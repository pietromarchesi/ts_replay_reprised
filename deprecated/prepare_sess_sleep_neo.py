import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(sys.path[0]))))
import neo
import pickle
import numpy as np
import distutils.util
import quantities as pq
import matplotlib.pyplot as plt
from loadmat import loadmat, nan_helper
import warnings
from constants import iPix2Cm

try:
    from utils import clean_position_segment
    can_clean_position = True
except FileNotFoundError:
    print('Could not import position cleaning function, probably the maze'
          'file cannot be found')
    can_clean_position = False

'''
This is the script used to package the data following the Neo format. 
'''


### PARAMETERS ###

try:
    __IPYTHON__
except NameError:
    print("Running not in IPython")
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sess_ind', type=int)
    parser.add_argument('-d', '--data_folder', type=str)
    parser.add_argument('-t', '--save_to', type=str, default='.')

    ARGS = parser.parse_args()

    sess_ind             = ARGS.sess_ind
    data_folder          = ARGS.data_folder
    save_to              = ARGS.save_to
    save_block           = True
    save_as              = 'pickle'

else:
    print("Running in IPython interactive console")
    data_folder          = '/media/pietro/bigdata/neuraldata/touch_see/'
    sess_ind             = 1 # Python index - sess_ind 0 is session 1
    save_block           = True
    save_as              = 'pickle'
    save_to              = '/media/pietro/bigdata/neuraldata/touch_see/neo/'


print('\n\n\nPackaging Touch & See Session {}'.format(sess_ind))

### EXTRACTING AND PREPARING THE DATA FROM ORIGINAL FILES ###
F = loadmat(data_folder + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]

recording_day = session[0:10]
recording_time = session[11:]

session_path = os.path.join(data_folder, 'SWSPeriods', rat, day, session, 'spikeTrl.mat')
session_path_raw = os.path.join(data_folder, 'SWSPeriods', rat, day, session, 'swsleep.mat')

unit_path = os.path.join(data_folder, 'unit.mat')
wave_class_path = os.path.join(data_folder, 'wave_class.mat')

try:
    S  = loadmat(session_path)
    S_raw  = loadmat(session_path_raw)
except FileNotFoundError:
    print('No sleep file for session {}'.format(sess_ind))
    sys.exit(0)

U  = loadmat(unit_path)
W  = loadmat(wave_class_path)

periods_raw = S_raw['swsleep']['indxSWSTs'] / 1e6
spike_trains_pre = S['spikeTrlPre']['timestamp']  / 1e6
periods_pre = S['spikeTrlPre']['cfg']['trl'] / 1e6

spike_trains_post = S['spikeTrlPost']['timestamp'] / 1e6
periods_post = S['spikeTrlPost']['cfg']['trl'] / 1e6


sleep_data = {'pre' : {'spikes' : spike_trains_pre,
                       'periods' : periods_pre},
              'post' : {'spikes' : spike_trains_post,
                        'periods' : periods_post}}

original_files = [session_path, unit_path]

total_time_post = ((periods_post[:, 1] - periods_post[:, 0]).sum()*pq.s).rescale(pq.min)
total_time_pre = ((periods_pre[:, 1] - periods_pre[:, 0]).sum()*pq.s).rescale(pq.min)

print('\nTotal time pre-sleep: {:.2f}\nTotal time post sleep: {:.2f} min.'.format(total_time_post,
                                                                                  total_time_pre))

assert periods_pre[-1, 1] < periods_post[0, 0]
### PUTATIVE NEURON TYPE ###

# unit-level information
unit_index    = U['unit']['sess'] == sess_ind + 1
unit_area     = U['unit']['area'][unit_index]
unit_depth    = U['unit']['unitDepth'][unit_index]
unit_layer    = U['unit']['unitLayer'][unit_index]
unit_rat      = U['unit']['ratName'][unit_index]
unit_sess     = U['unit']['sess'][unit_index]
unit_sess_num = U['unit']['unitSessNum'][unit_index]
unit_label    = U['unit']['label'][unit_index]
n_units       = unit_area.shape[0]

neuron_type_labels = W['R']['neuronTypeLabel']
neuron_type_code   = W['R']['neuronType']-1
neuron_type        = neuron_type_labels[neuron_type_code]
neuron_type_sess   = neuron_type[unit_index]
is_pyr_sess        = np.array([0 if t=='narrow_int' else 1 for t in
                               neuron_type_sess]).astype(bool)

# extract the tetrode and channel number from the label
unit_tetrode  = np.zeros_like(unit_label)
for i in range(unit_tetrode.shape[0]):
    tet_num = int(unit_label[i][unit_label[i].find('TT')+2])
    unit_tetrode[i] = tet_num

# Channel number (was not actually used later on)
# unit_channel = np.zeros_like(unit_label)
# for i in range(unit_channel.shape[0]):
#     chan_num = unit_label[i].split('_')[1]
#     if not chan_num in ['A', 'B', 'C', 'D']:
#         warnings.warn('Could not extract unit channel.')

# same number of units and spike timestamp vectors
np.testing.assert_equal(unit_area.shape, spike_trains_pre.shape)
np.testing.assert_equal(unit_area.shape, spike_trains_post.shape)

### SETTING UP THE NEO DATASET ###

# CREATE EMPTY BLOCK
bl = neo.Block(name = 'Sleep session %s' %(sess_ind + 1),
               animal_ID = rat,
               recording_day = recording_day,
               recording_time = recording_time,
               day_folder = day,
               session_folder = session)


# CREATE CHANNELS
Hp     = neo.ChannelIndex(name='Hippocampus', index=None)
Pr     = neo.ChannelIndex(name='Perirhinal', index=None)
Br     = neo.ChannelIndex(name='Barrel', index=None)
V1     = neo.ChannelIndex(name='V1', index=None)
bl.channel_indexes = [Hp, Pr, Br, V1] #, Pr35, Pr36, Pr3536]
# the index in the ChannelIndex is the index of the units belonging to the
# channel in the analog signal, so that you can use it to slice it and recover

# CREATE UNITS
for u in range(n_units):

    if unit_area[u] != 'tea':
        np.testing.assert_equal(unit_rat[u], rat)
        np.testing.assert_equal(unit_sess[u], sess_ind + 1)
        np.testing.assert_equal(unit_sess_num[u], u+1)

        general_unit_ind = (np.where(unit_index)[0]+1)[u]
        unit = neo.Unit(name='unit {}'.format(general_unit_ind),
                        area=None,
                        depth=unit_depth[u],
                        layer=unit_layer[u],
                        sess_ind=u,
                        tetrode=unit_tetrode[u],
                        general_unit_ind=general_unit_ind,
                        neuron_type=neuron_type_sess[u],
                        is_pyramidal=is_pyr_sess[u])

        # ASSIGN UNITS TO AREAS
        if unit_area[u] == 'Br':
            unit.annotations['area'] = 'Barrel'
            unit.annotations['sub_area'] = ''
            Br.units.append(unit)
            unit.channel_index = Br

        if unit_area[u] == 'Hp':
            unit.annotations['area'] = 'Hippocampus'
            unit.annotations['sub_area'] = ''
            Hp.units.append(unit)
            unit.channel_index = Hp

        # perirhinal units from different sub-areas are grouped together,
        # information about the sub-areas is moved to the annotations.
        if unit_area[u] == '35':
            unit.annotations['area'] = 'Perirhinal'
            unit.annotations['sub_area'] = 'Perirhinal - Area 35'
            Pr.units.append(unit)
            unit.channel_index = Pr

        if unit_area[u] == '36':
            unit.annotations['area'] = 'Perirhinal'
            unit.annotations['sub_area'] = 'Perirhinal - Area 36'
            Pr.units.append(unit)
            unit.channel_index = Pr

        if unit_area[u] == 'b3536':
            unit.annotations['area'] = 'Perirhinal'
            unit.annotations['sub_area'] = 'Perirhinal - Area 35-36'
            Pr.units.append(unit)
            unit.channel_index = Pr

        if unit_area[u] == 'V':
            unit.annotations['area'] = 'V1'
            unit.annotations['sub_area'] = ''
            V1.units.append(unit)
            unit.channel_index = V1


for sleep_epoch in ['pre', 'post']:

    spike_times = sleep_data[sleep_epoch]['spikes']
    periods     = sleep_data[sleep_epoch]['periods']

    t_start = periods[0, 0] * pq.s
    t_stop = periods[-1, 1] * pq.s

    # t_start = np.hstack(spike_times).min() * pq.s
    # t_stop = np.hstack(spike_times).max() * pq.s

    event_times = periods[:, :-1].flatten()
    assert (np.diff(event_times)>0).all()

    event_labels = np.array(['period_start', 'period_end'] * periods.shape[0])

    ev = neo.Event(event_times * pq.s,
                   labels=event_labels)

    seg = neo.Segment(name='{}-sleep'.format(sleep_epoch),
                      t_start=t_start,
                      t_stop=t_stop)

    seg.events.append(ev)

    for un in bl.list_units:
        # extract the spike train for the specific unit
        un_ind = un.annotations['sess_ind']
        spike_train = (spike_times[un_ind])
        if isinstance(spike_train, float):
            spike_train = np.array(spike_train)
        # beginning and end of the trial

        # get the spike times within the trial
        # train_trial is the spike train of unit `un` during trial `t`
        train_epoch = spike_train[(spike_train >= t_start) & (spike_train < t_stop)].copy()
        # GENERATE SPIKETRAIN OBJECT
        sptr_name = 'Unit {} in {}-sleep'.format(un.annotations['general_unit_ind'], sleep_epoch)
        train = neo.SpikeTrain(train_epoch * pq.s,
                               t_start=t_start,
                               t_stop=t_stop,
                               name=sptr_name,
                               general_unit_ind=un.annotations['general_unit_ind'],
                               neuron_type=un.annotations['neuron_type'],
                               is_pyramidal=un.annotations['is_pyramidal'],
                               area=un.annotations['area'],
                               sub_area=un.annotations['sub_area'])

        # ASSIGN THE SPIKE TRAIN TO THE TRIAL (SEGMENT)
        seg.spiketrains.append(train)
        # ASSIGN THE SPIKE TRAIN TO THE UNIT
        un.spiketrains.append(train)
        # ASSIGN THE SEGMENT AND UNIT TO THE SPIKE TRAIN
        train.segment = seg
        train.unit = un
    bl.segments.append(seg)


if save_block:

    dir = os.path.join(save_to, rat, session)

    if not os.path.isdir(dir):
        os.makedirs(dir)

    name = 'TS_sleep_session_{}_{}_{}'.format(sess_ind + 1, rat, recording_day)

    if save_as == 'pickle':

        format = '.pkl'
        full_destination = os.path.join(dir, name+format)
        print('Saving output to: {}'.format(full_destination))
        with open(full_destination, 'wb') as f:
            pickle.dump(bl, f)

    if save_as == 'numpy':
        format = '.npy'
        full_destination = os.path.join(dir, name+format)
        print('Saving output to: {}'.format(full_destination))
        np.save(full_destination, bl)




