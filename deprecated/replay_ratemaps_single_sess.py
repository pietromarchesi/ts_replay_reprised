import matplotlib.pyplot as plt
from constants import *
from utils import *
import quantities as pq
from plotting_style import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from replaydetector.replayratemaps import ReplayRatemaps

sess_ind                 = 7

ratemap_area             = 'Perirhinal'
PBEs_area                = 'Hippocampus'
dec_area                 = 'Hippocampus'
PBEs_settings_name       = 'july8'
PBEs_epoch               = 'task'
dec_settings_name        = 'dkwjuly4'


# filter pbes parameters
shuffles = ['column_cycle_shuffle',
            'place_field_rotation_shuffle']
p_vals   = [1, 1]


only_pyramidal           = True
time_bin_size_task_in_ms = 200
min_speed                = 5

# --- LOAD TASK DATA ----------------------------------------------------------
# Needed to build the ratemaps of the area during normal locomotion

session_data = unpack_session(sess_ind, data_folder=DATA_FOLDER,
                              area=ratemap_area, binsize_in_ms=time_bin_size_task_in_ms)

speed_real = session_data['speed']
binned_spikes_task = session_data['spikes']
binned_position_task = session_data['bin']

# --- LOAD REPLAY DATA --------------------------------------------------------
# Extract the spiketrains which will be binned according to the times of the
# decoded PBEs. If the PBEs are computed during the task, we simply get the
# trains from session_data, otherwise we load a sleep block.

if PBEs_epoch == 'task':
    trains = session_data['trains']

else:
    bl = get_session_block(DATA_FOLDER, sess_ind, PBEs_epoch)
    segment = bl.segments[0]
    trains = segment.spiketrains
    assert segment.name == PBEs_epoch.replace('-', '_')

# --- LOAD PBEs ---------------------------------------------------------------


PBEs_folder = os.path.join(REPLAY_FOLDER, 'results', 'decoded_PBEs',
                           'pbe_setting_{}_{}'.format(PBEs_settings_name,
                                                      PBEs_area),
                           'dec_setting_{}_{}'.format(dec_settings_name,
                                                      dec_area))

PBEs_file = os.path.join(PBEs_folder, 'decoded_PBEs_sess_{:02}_{}_'
                                      '{}.pkl'.format(sess_ind, dec_area,
                                                      PBEs_epoch))

pbes_data = pickle.load(open(PBEs_file, 'rb'))

pbes_df = pbes_data['pbes']
sig_df = pbes_data['sig_df']

bin_size_pbe_in_ms = pbes_data['decode_pars']['bin_size_pbe_in_ms']
slide_by_pbe_in_ms = pbes_data['decode_pars']['slide_by_pbe_in_ms']

# --- FILTER PBEs -------------------------------------------------------------

sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffles, p_vals=p_vals)
pbes_df = pbes_df[np.isin(pbes_df.index, sel_pbes_sig)]

# --- BIN PBEs ----------------------------------------------------------------

PBEs_times = [(row['start_time_in_ms'] * pq.ms, row['end_time_in_ms'] * pq.ms)
               for i, row in pbes_df.iterrows()]

binned_position_replay = pbes_df['trajectory'].tolist()
binned_pbes = bin_PBEs(trains, PBEs_times,
                       bin_size_in_ms=bin_size_pbe_in_ms,
                       sliding_binning=True,
                       slide_by_in_ms=slide_by_pbe_in_ms,
                       pyramidal_only=False)

binned_spikes_replay = binned_pbes['PBEs_binned']

for sp, pos in zip(binned_spikes_replay, binned_position_replay):
    print(sp.shape[0], pos.shape[0])


# --- BUILD RATEMAPS ----------------------------------------------------------


rr = ReplayRatemaps(bootstrap=False, unit_labels=range(len(trains)))

rr.build_real_ratemaps(binned_spikes_task=binned_spikes_task,
                       binned_position_task=binned_position_task,
                       time_bin_size_task_in_ms=time_bin_size_task_in_ms)

rr.build_event_ratemaps(binned_spikes_replay=binned_spikes_replay,
                        binned_position_replay=binned_position_replay,
                        time_bin_size_replay_in_ms=bin_size_pbe_in_ms)

rr.plot_full_ratemap_simple('event')

corr_out = rr.correlate_ratemaps()


p_val_thr_rateplots = 0.1
minmax_scale_plots = True


p_vals = rr.corr['p_values']
corrcoef_debiased = rr.corr['corrcoef_debiased']

ind_plot = np.where(p_vals < p_val_thr_rateplots)[0]


n_plots = len(ind_plot)
n_rows = int(np.ceil(n_plots / 4))
n_cols = 4

palette = real_event_palette[ratemap_area]
f, axes = plt.subplots(n_rows, n_cols, figsize=[n_cols * 3, n_rows * 1.5],
                       sharex=True)

for kk, unit in enumerate(ind_plot):

    try:
        ax = axes.flatten()[kk]
    except TypeError:
        ax = axes

    ax.set_yticks([])

    for i, rm in enumerate([rr.ratemap['real'], rr.ratemap['event']]):
        curve = rm[unit, :]
        if minmax_scale_plots:
            mm = MinMaxScaler()
            curve = mm.fit_transform(curve.reshape(-1, 1))[:, 0]

        line = ax.plot(rr.bins, curve, color=palette[i])
        fillcolor = line[0].get_color()
        ax.fill_between(rr.bins, curve, alpha=0.3,
                        color=fillcolor,
                        zorder=int(10 + 2 * rr.n_neurons - 2 * unit - 1))
        ax.set_title('c = {:.03}, p = {:.03}'.format(corrcoef_debiased[unit],
                                                     p_vals[unit]))

for kk in range(n_plots, n_rows * n_cols):
    print(kk)
    axes.flatten()[kk].axis('off')

sns.despine()
plt.tight_layout()
