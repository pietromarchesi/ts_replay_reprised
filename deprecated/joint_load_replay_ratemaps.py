import os
from constants import *
import pickle
import statsmodels.stats.descriptivestats
from statsmodels.stats.multitest import multipletests
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
from plotting_style import *
from sklearn.preprocessing import MinMaxScaler
from scipy.ndimage import gaussian_filter1d
import matplotlib.patches as mpatches
from utils import *

#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'jul4_DecSet2_smooth2_morelages'
#ratemap_settings = 'feb10_DecSet26_taskphase'
ratemap_areas = ['Perirhinal', 'Barrel']

save_plots = True
plot_format = 'png'
dpi = 400

test_values = [50, 0] # [0, 0]
nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
subselect_sessions = None
subselect_shifts = False
min_shift = -8
max_shift = 8

variable = 'corrcoef_obs'

# SWARMS
p_val_thr = 0.05
bonferroni_correct = False
null_dist_method_for_plot_both_shuff = 'rotate_spikes'


# RATEMAPS
smooth_ratemaps = True
p_val_thr_rateplots = 0.05
minmax_scale_plots = True
bins = np.arange(-29, 30)
n_rows = 2
n_cols = 5
n_plots = n_rows * n_cols
shift_amount = 0
shift_amount_ratemaps = 0



# --- SET UP PATHS ------------------------------------------------------------
plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# --- LOAD RESULTS ------------------------------------------------------------

dfs = []
for ratemap_area in ratemap_areas:
    res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)
    file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
    res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
    df = res['df']
    pars = res['pars']
    shift_amounts = pars['shift_amounts']
    smoothing_sigma = pars['smooth_sigma']


    if subselect_sessions is not None:
        df = df[np.isin(df['sess_ind'], subselect_sessions)]

    if subselect_shifts:
        df = df[df['shift_amount'] >= min_shift]
        df = df[df['shift_amount'] <= max_shift]
        shift_amounts = np.arange(min_shift, max_shift+1)

    df['area'] = ratemap_area
    dfs.append(df)

df = pd.concat(dfs)
df['unit_id'] = ['{}_{}_{}'.format(r['sess_ind'], r['unit'], r['area']) for i, r in df.iterrows()]
    #shift_amounts = np.array([-8, -6, -4, -2, 0, 2, 4, 6, 8])


len(df[df['area'] == 'Perirhinal']['sess_ind'].unique())
len(df[df['area'] == 'Barrel']['sess_ind'].unique())


# --- MAKE RATEMAP LEGEND ------------------------------------------------------


for ratemap_area in ratemap_areas:
    edgecolor1 = real_event_palette[ratemap_area][0]
    facecolor1 = make_rgb_transparent(real_event_palette[ratemap_area][0], (1, 1, 1), 0.3)

    edgecolor2 = real_event_palette[ratemap_area][1]
    facecolor2 = make_rgb_transparent(real_event_palette[ratemap_area][1], (1, 1, 1), 0.3)


    pos_patch = mpatches.Patch(edgecolor=edgecolor1,
                               facecolor=facecolor1, linewidth=2.5,
                               label='Real rate map')
    neg_patch = mpatches.Patch(edgecolor=edgecolor2,
                               facecolor=facecolor2, linewidth=2.5,
                               label='Replay event\nrate map')
    f, ax = plt.subplots(1, 1, figsize=[3, 1])
    leg = ax.legend(handles=[pos_patch, neg_patch], frameon=False,
                    title=ratemap_area_sigla[ratemap_area])
    leg.set_title(ratemap_area_sigla[ratemap_area], prop={'size' : 12})

    leg._legend_box.align = "left"
    ax.axis('off')
    plt.tight_layout()

    plot_name = 'legend_ratemaps_{}.{}'.format(ratemap_area, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# --- PLOT RATEMAPS COMBINED SHUFFLES ------------------------------------------

for ratemap_area in ratemap_areas:

    sig_ids = []
    n_total = []
    for null_distribution_method in nulldistmet_plots :
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'all') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_distribution_method) &
                 (df['shift_amount'] == 0) &
                 (df['area'] == ratemap_area)]

        if bonferroni_correct:
            p = p_val_thr / dfx.shape[0]
        else:
            p = p_val_thr

        #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
        unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
        sig_ids.append(unit_ids)
        n_total.append(dfx.shape[0])
    sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

    dfx = df[(df['phase'] == 'all') &
             (df['direction'] == 'all') &
             (df['has_ripple'] == 'all') &
             (df['null_method'] == null_dist_method_for_plot_both_shuff) &
             (df['shift_amount'] == shift_amount_ratemaps) &
             (np.isin(df['unit_id'], sig_both))]


    dfx = dfx.nlargest(n=n_plots, columns=variable)
    dfx = dfx.reset_index()
    n_neurons = dfx.shape[0]

    palette = real_event_palette[ratemap_area]



    f, axes = plt.subplots(n_rows, n_cols, figsize=[n_cols*1.6, n_rows * 1.1],
                           sharex=False)

    if n_rows == 1:
        axes = axes[np.newaxis, :]

    for kk, row in dfx.iterrows():

        real_ratemap = row['ratemap_real']
        event_ratemap = row['ratemap_event']

        try:
            ax = axes.flatten()[kk]
        except TypeError:
            ax = axes

        ax.set_yticks([])

        for i, rm in enumerate([real_ratemap, event_ratemap]):
            curve = rm

            if smooth_ratemaps:
                curve  = gaussian_filter1d(curve, smoothing_sigma)

            if minmax_scale_plots:
                mm = MinMaxScaler()
                curve = mm.fit_transform(curve.reshape(-1, 1))[:, 0]
            line = ax.plot(bins, curve, color=palette[i], lw=2)
            fillcolor = line[0].get_color()
            ax.fill_between(bins, curve,
                            color=fillcolor, alpha=0.3,
                            zorder=int(10 + 2 * n_neurons - 2 * kk - 1))
            ax.set_title('c = {:.03}'.format(row[variable]),
                         fontsize=10)

    axes.flatten()[0].set_xticks([-29, -reward_bin, 0, reward_bin, 29])
    axes.flatten()[0].set_xticklabels(['', 'REW', 'IMG', 'REW', ''], fontsize=8)
    #axes.flatten()[0].set_xticks([-29, -15, 0, 15, 29])
    #axes.flatten()[0].set_xticklabels([-29, '', 'Location', '', 29])
    #axes.flatten()[0].set_xlabel('Location')
    sns.despine(left=True,offset=2, ax=axes.flatten()[0], trim=True)

    for kk in range(1, n_plots):
        print(kk)
        axes.flatten()[kk].axis('off')

    plt.tight_layout()


    #plt.rcParams['savefig.dpi'] = dpi

    plot_name = 'sig_replay_ratemaps_{}_{}.{}'.format(ratemap_area, ratemap_settings, plot_format)
    plt.draw()
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- SWARMPLOTS ---------------------------------------------------------------

# for null_distribution_method in nulldistmet_plots :
#     dfx = df[(df['phase'] == 'all') &
#              (df['direction'] == 'all') &
#              (df['has_ripple'] == 'all') &
#              (df['null_method'] == null_distribution_method) &
#              (df['shift_amount'] == 0)]
#
#     if bonferroni_correct:
#         p = p_val_thr / dfx.shape[0]
#     else:
#         p = p_val_thr
#
#     print(p)
#     dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
#
#
#     violin_color = sns.xkcd_rgb['lightish red']
#     f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#
#     sns.violinplot(x='area', y='corrcoef_debiased', data=dfx,
#                   color='white', inner='quartile', lw=4, edgecolor='red')
#     sns.swarmplot(x='area', y='corrcoef_debiased', data=dfx, hue='is_sig', size=3,
#                   palette=[sns.xkcd_rgb['dark grey'], sns.xkcd_rgb['grey']],
#                   hue_order=['Coordinated', 'Not coordinated'])
#
#
#     ax.collections[0].set_edgecolor(area_palette['Perirhinal'])
#     ax.collections[1].set_edgecolor(area_palette['Barrel'])
#
#     for i in range(0, 3):
#         ax.lines[i].set_color(area_palette['Perirhinal'])
#         #ax.lines[i].set_alpha(0.7)
#         ax.lines[i].set_linewidth(2.5)
#
#     for i in range(3, 6):
#         ax.lines[i].set_color(area_palette['Barrel'])
#         #ax.lines[i].set_alpha(0.7)
#         ax.lines[i].set_linewidth(2.5)
#     ax.lines[1].set_linestyle('-')
#     ax.lines[4].set_linestyle('-')
#
#     #ax.set_title(null_distribution_method)
#     sns.despine()
#     ax.set_ylabel('Debiased correlation coefficient')
#     ax.set_xlabel('')
#     ax.set_ylim([-1, 1])
#     ax.set_yticks([-1, -0.5, 0, 0.5, 1])
#     ax.set_xticklabels([ratemap_area_sigla[area] for area in ['Perirhinal', 'Barrel']])
#     ax.get_legend().remove()
#     plt.tight_layout()
#
#
#     if save_plots :
#         plot_name = 'sig_swarm_{}.{}'.format(null_distribution_method, plot_format)
#         f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#


# --- SWARMPLOTS COMBINE SHUFFLES ----------------------------------------------

sig_ids = []
n_total = []
for null_distribution_method in nulldistmet_plots :
    dfx = df[(df['phase'] == 'all') &
             (df['direction'] == 'all') &
             (df['has_ripple'] == 'all') &
             (df['null_method'] == null_distribution_method) &
             (df['shift_amount'] == 0)]

    if bonferroni_correct:
        p = p_val_thr / dfx.shape[0]
    else:
        p = p_val_thr

    #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
    unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
    sig_ids.append(unit_ids)
    n_total.append(dfx.shape[0])

sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

dfx = df[(df['phase'] == 'all') &
         (df['direction'] == 'all') &
         (df['has_ripple'] == 'all') &
         (df['null_method'] == null_dist_method_for_plot_both_shuff) &
         (df['shift_amount'] == 0)]

dfx['is_sig'] = ['Coordinated' if f else 'Not coordinated' for f in np.isin(dfx['unit_id'], sig_both)]



f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
sns.swarmplot(x='area', y='corrcoef_debiased', data=dfx, hue='is_sig', size=3,
              palette=[sns.xkcd_rgb['dark grey'], sns.xkcd_rgb['grey']],
              hue_order=['Coordinated', 'Not coordinated'])
#ax.set_title(null_distribution_method)
sns.despine()
ax.set_ylabel('Debiased correlation coefficient')
ax.set_xlabel('')
ax.set_ylim([-1, 1])
ax.set_yticks([-1, -0.5, 0, 0.5, 1])
ax.get_legend().remove()
plt.tight_layout()


if save_plots :
    plot_name = 'sig_swarm_comb_shuffle.{}'.format(plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- DONUT COMBINE SHUFFLES ---------------------------------------------------

f, axes = plt.subplots(1, 2, figsize=[6, 3])

for ax, ratemap_area in zip(axes, ratemap_areas):

    sig_ids = []
    n_total = []
    for null_distribution_method in nulldistmet_plots :
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'all') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_distribution_method) &
                 (df['shift_amount'] == 0) &
                 (df['area'] == ratemap_area)]

        if bonferroni_correct:
            p = p_val_thr / dfx.shape[0]
        else:
            p = p_val_thr

        #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
        unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
        sig_ids.append(unit_ids)
        n_total.append(dfx.shape[0])

    sig_both = set(sig_ids[0]).intersection(set(sig_ids[1]))

    n_tot = n_total[0]
    n_sig = len(sig_both)
    n_not_sig = n_tot - n_sig

    binom_p = scipy.stats.binom_test(n_sig, n=n_tot, p=0.05)
    print('{}, both methods: {} out of {} significant units, ({}%) - p = {:.0e}'.format(
        ratemap_area, n_sig, n_tot, round(100 * n_sig / n_tot, 1), binom_p))

    size_of_groups = [n_sig, n_not_sig]
    labels = ['{}%'.format(round(100 * n_sig / n_tot, 1)),
              '{}%'.format(round(100 * n_not_sig / n_tot, 1))]

    circle_color = make_rgb_transparent(area_palette[ratemap_area], (1, 1, 1), 0.7)
    sig_info_colors_list = [sns.xkcd_rgb['dark grey'], circle_color]

    ax.pie(size_of_groups, colors=sig_info_colors_list, labels=labels,
           wedgeprops=dict(width=0.5), textprops={'fontsize' : 14})
    ax.axis('equal')
    #ax.set_title('{}: {} out of {}\ncoordinated units'.format(ratemap_area_sigla[ratemap_area], n_sig, n_tot))
    ax.set_title('{}'.format(ratemap_area_sigla[ratemap_area]), fontsize=16)
    #ax.set_title('{}: {} out of {} units\nbinomial p={:.3f}'.format(ratemap_area, n_sig, n_tot, binom_p))
    plt.tight_layout()

if save_plots :
    plot_name = 'sig_donut_comb_shuffle.{}'.format(plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- Number of significant units in positive vs negative ----------------------------
#
# for ratemap_area in ratemap_areas:
#
#     sig_dict = {}
#     for replay_direction in ['positive', 'negative']:
#         sig_ids = []
#         n_total = []
#         for null_distribution_method in nulldistmet_plots :
#             dfx = df[(df['phase'] == 'all') &
#                      (df['direction'] == replay_direction) &
#                      (df['has_ripple'] == 'all') &
#                      (df['null_method'] == null_distribution_method) &
#                      (df['shift_amount'] == 0) &
#                      (df['area'] == ratemap_area)]
#
#             if bonferroni_correct:
#                 p = p_val_thr / dfx.shape[0]
#             else:
#                 p = p_val_thr
#
#             #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
#             unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
#             sig_ids.append(unit_ids)
#             n_total.append(dfx.shape[0])
#
#         sig_both = set(sig_ids[0]).intersection(set(sig_ids[1]))
#         sig_dict[replay_direction] = len(sig_both)
#     print(sig_dict)


# --- Corr debiased in positive versus negative --------------------------------
# Take the units which are significant with all events, and look at
# their correlation coefficient in positive versus negative.

dfs = []

for ratemap_area in ratemap_areas:

    sig_ids = []
    for null_distribution_method in nulldistmet_plots :
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'all') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_distribution_method) &
                 (df['shift_amount'] == 0) &
                 (df['area'] == ratemap_area)]

        if bonferroni_correct:
            p = p_val_thr / dfx.shape[0]
        else:
            p = p_val_thr
        #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
        unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
        sig_ids.append(unit_ids)

    sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

    dfx = df[(df['phase'] == 'all') &
             (df['direction'] == 'negative') &
             (df['has_ripple'] == 'all') &
             (df['null_method'] == null_dist_method_for_plot_both_shuff) &
             (df['shift_amount'] == 0) &
             (df['area'] == ratemap_area) &
             (np.isin(df['unit_id'], sig_both))]

    dfx2 = df[(df['phase'] == 'all') &
             (df['direction'] == 'positive') &
             (df['has_ripple'] == 'all') &
             (df['null_method'] == null_dist_method_for_plot_both_shuff) &
             (df['shift_amount'] == 0) &
             (df['area'] == ratemap_area) &
             (np.isin(df['unit_id'], sig_both))]

    df3 = pd.concat([dfx, dfx2])
    dfs.append(df3)

    nhig = (dfx2['corrcoef_debiased'] > dfx['corrcoef_debiased']).sum()
    ntot = dfx.shape[0]
    binom_p = scipy.stats.binom_test(nhig, n=ntot, p=0.5)
    print('{} - {} units out of {} ({}%), p={:.2f} for which the debiased correlation is higher in pos'
          ' than neg events'.format(ratemap_area, nhig, ntot, np.round(100*nhig/ntot, 2), binom_p))
    t, p = scipy.stats.wilcoxon(dfx2['corrcoef_debiased'], dfx['corrcoef_debiased'])
    print('wilcoxon p={:.2f}'.format(p))



dg = pd.concat(dfs)

f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
sns.boxplot(data=dg, x='area', y='corrcoef_debiased', hue='direction',
              order=['Perirhinal', 'Barrel'], dodge=True,
              color='white')
plt.setp(ax.artists, edgecolor=sns.xkcd_rgb['grey'], facecolor='w')
plt.setp(ax.lines, color=sns.xkcd_rgb['grey'])

#
# ax.collections[0].set_edgecolor(violin_color)
# ax.collections[1].set_edgecolor(violin_color)
# for i in range(0, 6) :
#     ax.lines[i].set_color(violin_color)
#     # ax.lines[i].set_alpha(0.7)
#     ax.lines[i].set_linewidth(2.5)
# ax.lines[1].set_linestyle('-')
# ax.lines[4].set_linestyle('-')

sns.swarmplot(data=dg, x='area', y='corrcoef_debiased', hue='direction',
              order=['Perirhinal', 'Barrel'], dodge=True,
              palette=replay_direction_palette)
ax.set_ylabel(variable_label['corrcoef_debiased']+'\n of significant units')
ax.set_xlabel('')
ax.get_legend().remove()
sns.despine()
plt.tight_layout()


if save_plots :
    plot_name = 'corrcoef_posneg_of_sig_units_{}.{}'.format(null_dist_method_for_plot_both_shuff, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# # --- COMPARE TIMES ------------------------------------------------------------
# # Here we take the units which are significant at 0 for both shuffles
# # (no distinction of direction) and look how their correlation coefficient
# # changes with the shift
# # TODO: the problem here is that we select neurons which are significant at 0
# # so that introduces a bias towards having larger values at 0
#
#
# dfs = []
#
# for ratemap_area in ratemap_areas:
#
#     sig_ids = []
#     for null_distribution_method in nulldistmet_plots :
#         dfx = df[(df['phase'] == 'all') &
#                  (df['direction'] == 'all') &
#                  (df['has_ripple'] == 'all') &
#                  (df['null_method'] == null_distribution_method) &
#                  (df['shift_amount'] == 0) &
#                  (df['area'] == ratemap_area)]
#
#         if bonferroni_correct:
#             p = p_val_thr / dfx.shape[0]
#         else:
#             p = p_val_thr
#         #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
#         unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
#         sig_ids.append(unit_ids)
#
#     sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))
#
#
#     for shifts in np.arange(-10, 11):
#         dfx = df[(df['phase'] == 'all') &
#                  (df['direction'] == 'all') &
#                  (df['has_ripple'] == 'all') &
#                  (df['null_method'] == null_dist_method_for_plot_both_shuff) &
#                  (df['shift_amount'] == shifts) &
#                  (df['area'] == ratemap_area) &
#                  (np.isin(df['unit_id'], sig_both))]
#
#
#
#         dfs.append(dfx)
#
# dg = pd.concat(dfs)
#
# f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
# sns.lineplot(data=dg, x='shift_amount', y='corrcoef_debiased',
#              hue='area', palette=area_palette)
# ax.set_ylabel(variable_label['corrcoef_debiased']+'\n of significant units')
# ax.set_xlabel('Time shift [ms]')
# ax.set_xticks([-10, -5, 0, 5, 10])
# ax.set_xticklabels([-100, -50, 0, 50, 100])
# sns.despine()
# leg = ax.get_legend()
# ax.get_legend().remove()
# plt.tight_layout()
#
#
# if save_plots :
#     plot_name = 'corrcoef_of_sig_at_0_units_shift_{}.{}'.format(null_dist_method_for_plot_both_shuff, plot_format)
#     f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- COMPARE TIMES ------------------------------------------------------------

# here for every time shift we look only at the neurons which are significant
# for that time shift

# dfs = []
#
# for ratemap_area in ratemap_areas:
#
#     for shifts in np.arange(-5, 6):
#
#         sig_ids = []
#         for null_distribution_method in nulldistmet_plots :
#             dfx = df[(df['phase'] == 'all') &
#                      (df['direction'] == 'all') &
#                      (df['has_ripple'] == 'all') &
#                      (df['null_method'] == null_distribution_method) &
#                      (df['shift_amount'] == shifts) &
#                      (df['area'] == ratemap_area)]
#
#             if bonferroni_correct:
#                 p = p_val_thr / dfx.shape[0]
#             else:
#                 p = p_val_thr
#             #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
#             unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
#             sig_ids.append(unit_ids)
#
#         sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))
#
#
#         dfx = df[(df['phase'] == 'all') &
#                  (df['direction'] == 'all') &
#                  (df['has_ripple'] == 'all') &
#                  (df['null_method'] == null_dist_method_for_plot_both_shuff) &
#                  (df['shift_amount'] == shifts) &
#                  (df['area'] == ratemap_area) &
#                  (np.isin(df['unit_id'], sig_both))]
#
#         dfs.append(dfx)
#
# dg = pd.concat(dfs)
#
# f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
# sns.lineplot(data=dg, x='shift_amount', y='percentile_rank',
#              hue='area', palette=area_palette)
# ax.set_ylabel(variable_label['corrcoef_debiased']+'\n of significant units')
# ax.set_xlabel('Time shift [ms]')
# ax.set_xticks([-10, -5, 0, 5, 10])
# ax.set_xticklabels([-100, -50, 0, 50, 100])
# sns.despine()
# leg = ax.get_legend()
# ax.get_legend().remove()
# plt.tight_layout()
#
# if save_plots :
#     plot_name = 'corrcoef_of_sig_per_shift_units_shift_{}.{}'.format(null_dist_method_for_plot_both_shuff, plot_format)
#     f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

# --- COMPARE N OF SIG UNITS PER TIME ------------------------------------------
# here we look at every time point how many units are significant

# for direction in ['positive', 'all']:
#
#     dg = pd.DataFrame(columns=['shift', 'area', 'n_sig', 'perc_sig'])
#
#     for ratemap_area in ratemap_areas:
#
#         for shifts in np.arange(-10, 11):
#
#             sig_ids = []
#             for null_distribution_method in nulldistmet_plots :
#                 dfx = df[(df['phase'] == 'all') &
#                          (df['direction'] == direction) &
#                          (df['has_ripple'] == 'all') &
#                          (df['null_method'] == null_distribution_method) &
#                          (df['shift_amount'] == shifts) &
#                          (df['area'] == ratemap_area)]
#
#                 if bonferroni_correct:
#                     p = p_val_thr / dfx.shape[0]
#                 else:
#                     p = p_val_thr
#                 #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
#                 unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
#                 sig_ids.append(unit_ids)
#                 n_total = dfx.shape[0]
#
#             sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))
#             n_sig = len(sig_both)
#             perc_sig = 100*n_sig/n_total
#             dg.loc[dg.shape[0], :] = [shifts, ratemap_area, n_sig, perc_sig]
#
#
#
#     dg['n_sig'] = pd.to_numeric(dg['n_sig'])
#     dg['perc_sig'] = pd.to_numeric(dg['perc_sig'])
#
#     f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#     sns.lineplot(data=dg, x='shift', y='perc_sig',
#                  hue='area', palette=area_palette)
#     ax.set_ylabel('% of significant units')
#     ax.set_xlabel('Time shift [ms]')
#     ax.set_xticks([-10, -5, 0, 5, 10])
#     ax.set_xticklabels([-100, -50, 0, 50, 100])
#     sns.despine()
#     leg = ax.get_legend()
#     ax.get_legend().remove()
#     plt.tight_layout()
#
#
#     if save_plots :
#         plot_name = 'perc_of_sig_units_per_shift_{}_{}.{}'.format(direction, null_dist_method_for_plot_both_shuff, plot_format)
#         f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#




# --- PLOT AREA LEGEND ---------------------------------------------------------

# leg.set_title('')
# f, ax = plt.subplots(1, 1, figsize=[3, 1])
# ax.legend(handles=leg.legendHandles, frameon=False, title='')
# ax.axis('off')
#
# if save_plots :
#     plot_name = 'areas_line_legend.{}'.format(plot_format)
#     f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#


# # --- PLOT RSI INDEX ---------------------------------------------------------
#
#
# sig_ids = []
# n_total = []
# for null_distribution_method in nulldistmet_plots :
#     dfx = df[(df['phase'] == 'all') &
#              (df['direction'] == 'all') &
#              (df['has_ripple'] == 'all') &
#              (df['null_method'] == null_distribution_method) &
#              (df['shift_amount'] == 0)]
#
#     if bonferroni_correct:
#         p = p_val_thr / dfx.shape[0]
#     else:
#         p = p_val_thr
#
#     #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
#     unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
#     sig_ids.append(unit_ids)
#     n_total.append(dfx.shape[0])
#
# sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))
#
# dfx = df[(df['phase'] == 'all') &
#          (df['direction'] == 'all') &
#          (df['has_ripple'] == 'all') &
#          (df['null_method'] == null_dist_method_for_plot_both_shuff) &
#          (df['shift_amount'] == 0)]
#
# dfx['is_sig'] = ['Coordinated' if f else 'Not coordinated' for f in np.isin(dfx['unit_id'], sig_both)]
#
#
#
# f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
# sns.swarmplot(x='area', y='rsi', data=dfx, hue='is_sig', size=3,
#               palette=[sns.xkcd_rgb['dark grey'], sns.xkcd_rgb['grey']],
#               hue_order=['Coordinated', 'Not coordinated'])
# #ax.set_title(null_distribution_method)
# sns.despine()
# ax.set_ylabel('RSI index')
# ax.set_xlabel('')
# ax.get_legend().remove()
# plt.tight_layout()
#



# --- ACROSS TASK PHASES -------------------------------------------------------

phases = ['iti', 'img', 'run', 'reward']

null_dist_method = 'rotate_spikes'
bonferroni_correct=False
p_val_thr=0.05
nulldistmethods=['rotate_ratemap', 'rotate_spikes']

sig_ids = []
n_total = []
for null_distribution_method in nulldistmethods :
    dfx = df[(df['phase'] == 'all') &
             (df['direction'] == 'all') &
             (df['has_ripple'] == 'all') &
             (df['null_method'] == null_distribution_method) &
             (df['shift_amount'] == 0)]

    if bonferroni_correct :
        p = p_val_thr / dfx.shape[0]
    else :
        p = p_val_thr

    # dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
    unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
    sig_ids.append(unit_ids)
    n_total.append(dfx.shape[0])

sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))



# --- ACROSS TASK PHASES BOXPLOTS ----------------------------------------------
# import itertools
# import scipy.stats
# variable = 'corrcoef_debiased'
# replay_direction = 'all'
# shift_amount = 0
# null_dist_method = 'rotate_spikes'
#
# phases = ['iti', 'img', 'run', 'reward']
# combos = list(itertools.combinations(phases,2))
# for ratemap_area in ratemap_areas:
#     print('\n{}'.format(ratemap_area))
#     for phase1, phase2 in combos :
#         dfx = df[#(np.isin(df['unit_id'], sig_both)) &
#                  (df['direction'] == replay_direction) &
#                  (df['has_ripple'] == 'all') &
#                  (df['null_method'] == null_dist_method) &
#                  (df['shift_amount'] == shift_amount) &
#                  (df['area'] == ratemap_area)]
#         df1 = dfx[dfx['phase'] == phase1]
#         df2 = dfx[dfx['phase'] == phase2]
#         #np.testing.assert_array_equal(df1['unit_id'], df2['unit_id'])
#         x = df1[variable].values
#         y = df2[variable].values
#
#         stat, p = scipy.stats.mannwhitneyu(x, y)
#         is_sig =  p < 0.05 / len(combos)
#         print('{} - {}, {} and {} samples: p = {:.3f} - sig: {}'.format(phase1, phase2,
#                                                                         x.shape[0], y.shape[0], p, is_sig))
#
#
# f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#
# dfx = df[(np.isin(df['phase'], phases)) &
#          #(np.isin(df['unit_id'], sig_both)) &
#          (df['direction'] == replay_direction) &
#          (df['has_ripple'] == 'all') &
#          (df['null_method'] == null_dist_method) &
#          (df['shift_amount'] == shift_amount)]
#
# sns.boxplot(data=dfx, x='phase', y=variable, order=phases, hue='area',
#             palette=area_palette, hue_order=['Perirhinal', 'Barrel'])
# ax.set_xlabel('')
# ax.set_ylabel('Debiased correlation coefficient')
# ax.set_xticklabels([task_phase_labels_nobreak[l._text] for l in ax.get_xticklabels()],
#                    rotation=30)
# leg = ax.get_legend()
# ax.get_legend().remove()
# sns.despine()
# plt.tight_layout()
#
#
# if save_plots :
#     plot_name = 'corrcoef_per_task_phase_{}_{}_shift_{}.{}'.format(null_dist_method, replay_direction, shift_amount, plot_format)
#     f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#
# f, ax = plt.subplots(1, 1, figsize=[3, 1])
# ax.legend(handles=leg.legendHandles, frameon=False, title='')
# ax.get_legend().get_texts()[0].set_text('PRH')
# ax.get_legend().get_texts()[1].set_text('S1BF')
# ax.axis('off')
#
# if save_plots :
#     plot_name = 'corrcoef_per_task_phase_legend.{}'.format(plot_format)
#     f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#



# --- PERC SIG. UNITS PER TASK PHASE -------------------------------------------
#
# if df['phase'].unique().shape[0] > 1:
#     dn = pd.DataFrame(columns=['phase', 'area', 'perc_sig'])
#
#     phases = ['all', 'iti', 'img', 'run', 'reward']
#
#     for ratemap_area in ratemap_areas:
#         for phase in phases:
#             sig_ids = []
#             n_total = []
#             for null_distribution_method in nulldistmet_plots :
#                 dfx = df[(df['phase'] == phase) &
#                          (df['direction'] == 'all') &
#                          (df['has_ripple'] == 'all') &
#                          (df['null_method'] == null_distribution_method) &
#                          (df['shift_amount'] == 0) &
#                          (df['area'] == ratemap_area)]
#
#                 if bonferroni_correct:
#                     p = p_val_thr / dfx.shape[0]
#                 else:
#                     p = p_val_thr
#
#                 #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
#                 unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
#                 sig_ids.append(unit_ids)
#                 n_total.append(dfx.shape[0])
#
#             sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))
#             n_total = n_total[0]
#             perc_sig = 100 * len(sig_both) / n_total
#
#             dn.loc[dn.shape[0], :] = [phase, ratemap_area, perc_sig]
#
#     f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#     sns.barplot(data=dn, x='phase', y='perc_sig', hue='area', ax=ax,
#                 palette=area_palette)
#     ax.set_ylabel('% of coordinated units')
#     ax.set_xticklabels([task_phase_labels_nobreak[t._text] for t in
#                         ax.get_xticklabels()], rotation=45)
#     ax.set_xlabel('')
#     ax.get_legend().remove()
#     sns.despine()
#     plt.tight_layout()
#
#     if save_plots :
#         plot_name = 'perc_sig_units_per_task_phase_legend.{}'.format(plot_format)
#         f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#

        # dfx = df[(df['phase'] == 'all') &
        #          (df['direction'] == 'all') &
        #          (df['has_ripple'] == 'all') &
        #          (df['null_method'] == null_dist_method_for_plot_both_shuff) &
        #          (df['shift_amount'] == shift_amount_ratemaps) &
        #          (np.isin(df['unit_id'], sig_both))]



# for null_distribution_method in nulldistmet_plots :
#
#
#
#     f, axes = plt.subplots(1, 2, figsize=[smaller_panel_size[0]*2,
#                                           smaller_panel_size[0]])
#
#     for ax, area in zip(axes, ratemap_areas):
#
#         dfx = df[(df['phase'] == 'all') &
#                  (df['direction'] == 'all') &
#                  (df['has_ripple'] == 'all') &
#                  (df['null_method'] == null_distribution_method) &
#                  (df['shift_amount'] == 0) &
#                  (df['area'] == area)]
#
#         if bonferroni_correct:
#             p = p_val_thr / dfx.shape[0]
#         else:
#             p = p_val_thr
#
#         n_tot = dfx.shape[0]
#         n_sig = dfx[dfx['p_values'] <= p].shape[0]
#         n_not_sig = n_tot - n_sig
#
#         print('{}, {}: {} out of {} significant units, ({}%)'.format(ratemap_area,
#              null_distribution_method, n_sig, n_tot, round(100*n_sig/n_tot, 1)))
#
#         binom_p = scipy.stats.binom_test(n_sig, n=n_tot, p=0.05)
#
#         size_of_groups = [n_sig, n_not_sig]
#         labels=['{}%'.format(round(100 * n_sig / n_tot, 1)),
#                 '{}%'.format(round(100 * n_not_sig / n_tot, 1))]
#         sig_info_colors_list = [sns.xkcd_rgb['dark grey'], sns.xkcd_rgb['grey']]
#
#         ax.pie(size_of_groups, colors=sig_info_colors_list, labels=labels,
#                wedgeprops=dict(width=0.5), textprops={'fontsize':14})
#         ax.axis('equal')
#         ax.set_title('{}: {} units\nbinomial p={:.3f}'.format(ratemap_area, n_tot,
#                                                      binom_p))
#         plt.tight_layout()
#
#     if save_plots :
#         plot_name = 'sig_donut_{}.{}'.format(null_distribution_method, plot_format)
#         f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
