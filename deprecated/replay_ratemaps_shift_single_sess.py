import matplotlib.pyplot as plt
from constants import *
from utils import *
import quantities as pq
import copy
from plotting_style import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from replaydetector.replayratemaps import ReplayRatemaps
import statsmodels

sess_ind                 = 9

ratemap_area             = 'Perirhinal'
PBEs_area                = 'Hippocampus'
dec_area                 = 'Hippocampus'
PBEs_settings_name       = 'june28'
PBEs_epoch               = 'task'
dec_settings_name        = 'dkw_june28'


# filter pbes parameters
shuffles = ['column_cycle_shuffle',
            'place_field_rotation_shuffle']
p_vals   = [1, 1]

only_pyramidal           = True
time_bin_size_task_in_ms = 200
min_speed                = 5

# --- LOAD TASK DATA ----------------------------------------------------------
# Needed to build the ratemaps of the area during normal locomotion

session_data = unpack_session(sess_ind, data_folder=DATA_FOLDER,
                              area=ratemap_area, binsize_in_ms=time_bin_size_task_in_ms)

speed_real = session_data['speed']
binned_spikes_task = session_data['spikes']
binned_position_task = session_data['bin']

# --- LOAD REPLAY DATA --------------------------------------------------------
# Extract the spiketrains which will be binned according to the times of the
# decoded PBEs. If the PBEs are computed during the task, we simply get the
# trains from session_data, otherwise we load a sleep block.

if PBEs_epoch == 'task':
    trains = session_data['trains']

else:
    bl = get_session_block(DATA_FOLDER, sess_ind, PBEs_epoch)
    segment = bl.segments[0]
    trains = segment.spiketrains
    assert segment.name == PBEs_epoch.replace('-', '_')

# --- LOAD PBEs ---------------------------------------------------------------

PBEs_folder = os.path.join(REPLAY_FOLDER, 'results', 'decoded_PBEs',
                           'pbe_setting_{}_{}'.format(PBEs_settings_name, PBEs_area),
                           'dec_setting_{}_{}'.format(dec_settings_name, dec_area))


PBEs_file = os.path.join(PBEs_folder, 'decoded_PBEs_sess_{:02}_{}_'
                                      '{}.pkl'.format(sess_ind, dec_area, PBEs_epoch))

pbes_data = pickle.load(open(PBEs_file, 'rb'))

pbes_df = pbes_data['pbes']
sig_df = pbes_data['sig_df']

bin_size_pbe_in_ms = pbes_data['decode_pars']['bin_size_pbe_in_ms']
slide_by_pbe_in_ms = pbes_data['decode_pars']['slide_by_pbe_in_ms']

# --- FILTER PBEs -------------------------------------------------------------

sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffles, p_vals=p_vals)
pbes_df = pbes_df[np.isin(pbes_df.index, sel_pbes_sig)]

# --- BIN PBEs ----------------------------------------------------------------

PBEs_times = [(row['start_time_in_ms'] * pq.ms, row['end_time_in_ms'] * pq.ms)
               for i, row in pbes_df.iterrows()]

binned_position_replay = pbes_df['trajectory'].tolist()
binned_pbes = bin_PBEs(trains, PBEs_times,
                       bin_size_in_ms=bin_size_pbe_in_ms,
                       sliding_binning=True,
                       slide_by_in_ms=slide_by_pbe_in_ms,
                       pyramidal_only=False)

binned_spikes_replay = binned_pbes['PBEs_binned']

# --- SHIFT -------------------------------------------------------------------

shift_amounts = np.arange(-4, 5)
shift_amounts_in_ms = shift_amounts * slide_by_pbe_in_ms

shift_rr = {}

for sa in shift_amounts:

    if sa > 0:
        shifted_bs_replay = [s[sa:] for s in binned_spikes_replay]
        shifted_pos_replay = [p[:-sa] for p in binned_position_replay]

    elif sa < 0:
        shifted_bs_replay = [s[:sa] for s in binned_spikes_replay]
        shifted_pos_replay = [p[-sa:] for p in binned_position_replay]

    elif sa == 0:
        shifted_bs_replay = copy.copy(binned_spikes_replay)
        shifted_pos_replay = copy.copy(binned_position_replay)
# --- BUILD RATEMAPS ----------------------------------------------------------


    rr = ReplayRatemaps(bootstrap=False, unit_labels=range(len(trains)))

    rr.build_real_ratemaps(binned_spikes_task=binned_spikes_task,
                           binned_position_task=binned_position_task,
                           time_bin_size_task_in_ms=time_bin_size_task_in_ms)

    rr.build_event_ratemaps(binned_spikes_replay=shifted_bs_replay,
                            binned_position_replay=shifted_pos_replay,
                            time_bin_size_replay_in_ms=bin_size_pbe_in_ms)

    rr.correlate_ratemaps()

    shift_rr[sa] = rr


pal = sns.color_palette("RdBu", n_colors=shift_amounts.shape[0])
boxplot_vars = ['percentile_rank', 'corrcoef_debiased']

for variable in boxplot_vars:

    shifts, vals = [], []
    for sa, sa_ms in zip(shift_amounts, shift_amounts_in_ms):
        y = shift_rr[sa].corr[variable]
        vals.append(y)
        shifts.append(np.repeat(sa_ms, y.shape[0]))

    df = pd.DataFrame(columns=['shift_amount', variable])
    df['shift_amount'] = np.hstack(shifts)
    df[variable] = np.hstack(vals)

    f, ax = plt.subplots(1, 1, figsize=[6, 5])
    #ax.set_ylim([-0.25, 0.25])
    sns.boxplot(data=df, x='shift_amount', y=variable, ax=ax, palette=pal)
    # not_signif = percentile_p_val_sign_test > 0.05
    # height = ax.get_ylim()[0] + 0.1
    # signif_line = np.ma.masked_where(not_signif, np.repeat(height, len(shift_amounts)))
    # ax.plot(np.arange(len(shift_amounts)), signif_line, color='k', lw=2, alpha=0.5)
    ax.set_ylabel(replay_ratemap_labels[variable])
    ax.set_xlabel('Shift amount (ms)')
    ax.tick_params(axis='x', rotation=90)
    plt.tight_layout()
    sns.despine()

#
# m_perc, p_val_sign_perc = statsmodels.stats.descriptivestats.sign_test(
#     perc_score, 50)
# m_cor, p_val_sign_cor = statsmodels.stats.descriptivestats.sign_test(
#     corrcoef_debiased, 0)
#
# out['p_val_percentile_rank'] = p_val_sign_perc
# out['p_val_corrcoef_debiased'] = p_val_sign_cor
# # t, p_val_t = scipy.stats.ttest_1samp(corrcoef_debiased, 0)
# # t, p_val_t = scipy.stats.ttest_1samp(percentile_rank, 50)
