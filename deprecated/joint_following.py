import os
from constants import *
import pickle
import statsmodels.stats.descriptivestats
from statsmodels.stats.multitest import multipletests
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
from plotting_style import *
from sklearn.preprocessing import MinMaxScaler
from scipy.ndimage import gaussian_filter1d
import matplotlib.patches as mpatches
from utils import *

#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'jul4_DecSet2_smooth2_morelages'
#ratemap_settings = 'feb10_DecSet26_taskphase'
ratemap_areas = ['Perirhinal', 'Barrel']

save_plots = True
plot_format = 'png'
dpi = 400

test_values = [50, 0] # [0, 0]
nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
subselect_sessions = None
subselect_shifts = False
min_shift = -8
max_shift = 8

# SWARMS
p_val_thr = 0.05
bonferroni_correct = False
null_dist_method_for_plot_both_shuff = 'rotate_spikes'


# RATEMAPS
smooth_ratemaps = True
p_val_thr_rateplots = 0.05
minmax_scale_plots = True
bins = np.arange(-29, 30)
n_rows = 2
n_cols = 5
n_plots = n_rows * n_cols
shift_amount = 0
shift_amount_ratemaps = 0



# --- SET UP PATHS ------------------------------------------------------------
plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# --- LOAD RESULTS ------------------------------------------------------------

dfs = []
for ratemap_area in ratemap_areas:
    res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)
    file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
    res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
    df = res['df']
    pars = res['pars']
    shift_amounts = pars['shift_amounts']
    smoothing_sigma = pars['smooth_sigma']


    if subselect_sessions is not None:
        df = df[np.isin(df['sess_ind'], subselect_sessions)]

    if subselect_shifts:
        df = df[df['shift_amount'] >= min_shift]
        df = df[df['shift_amount'] <= max_shift]
        shift_amounts = np.arange(min_shift, max_shift+1)

    df['area'] = ratemap_area
    dfs.append(df)

df = pd.concat(dfs)
df['unit_id'] = ['{}_{}_{}'.format(r['sess_ind'], r['unit'], r['area']) for i, r in df.iterrows()]
    #shift_amounts = np.array([-8, -6, -4, -2, 0, 2, 4, 6, 8])



# --- STATS --------------------------------------------------------------------







