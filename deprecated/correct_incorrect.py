import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from utils import load_bin_centers
from utils import prepare_bins
from utils import plot_maze, mark_locations
from utils import get_position_bin
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from utils import is_on_central_arm, merge_duplicate_left_right_bins
from utils import add_ripples_to_pbes_df
from session_selection import sessions_HC_replay
from utils import load_decoded_PBEs
from utils import get_time_spent_per_task_phase
import quantities as pq
import matplotlib.patches as mpatches
from scipy.ndimage import gaussian_filter1d
import itertools
from utils import *

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

group_iti_and_return = True

# PLOTTING PARAMETERS

plot_settings = ''
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms    = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# # --- SET UP PLOT PATHS -------------------------------------------------------
#
# plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'hippocampal_replay',
#                              'pbe_setting_{}_{}_{}_dec_setting_{}_{}_plot_setting_{}'.format(PBEs_settings_name, PBEs_area,
#                               data_version, dec_settings_name, dec_area, plot_settings))
#
# if not os.path.isdir(plot_folder):
#     os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

pbes_dfs, sig_dfs = [], []

for sess_ind in sessions:
    print(sess_ind)
    for epoch in epochs:
        try:
            pbes = load_decoded_PBEs(sess_ind, PBEs_settings_name, PBEs_area,
                                     data_version, dec_settings_name, dec_area,
                                     load_light=True)
            pbes_dfs.append(pbes['pbes'])
            sig_dfs.append(pbes['sig_df'])
        except FileNotFoundError:
            print('No decoded PBEs for session {}'.format(sess_ind))

df = pd.concat(pbes_dfs)
sig_df = pd.concat(sig_dfs)
df = add_ripples_to_pbes_df(df)

if group_iti_and_return:
    df['phase'] = df['phase'].replace(to_replace='return', value='iti')

df = df[df['event_duration_in_ms'] >= min_pbe_duration_in_ms]

# If location of the animal during img has artifacts fix that
img_bins = [1, -1, 2, -2, 26, 27, 28, 29, 0, -26, -27, -28, -29]
x = df['pbe_loc_bin'].values
ind = np.logical_and(~np.isin(df['pbe_loc_bin'], img_bins), df['phase'] == 'img')
x[ind] = 0
df['pbe_loc_bin'] = x

sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types,
                                        p_vals=shuffle_p_vals)
df_not_sig = df[~np.isin(df.index, sel_pbes_sig)]
df = df[np.isin(df.index, sel_pbes_sig)]

for i, row in df.iterrows():
    if is_on_central_arm(row['dec_start_bin']) and is_on_central_arm(row['dec_end_bin']):
        df.loc[i, 'pbe_side'] = 'central'

for col in ['percentage_active', 'dist_traversed_cm', 'max_jump_dist',
            'mean_jump_dist', 'sharpness']:
    df[col] = pd.to_numeric(df[col])



# dfs = []
# for sess_ind in sessions:
#     F = loadmat(DATA_FOLDER + '/F/F_units/F_All.mat')['F']
#     rat, day, session = F[sess_ind].dir.split('/')[-3:]
#     recording_day = session[0:10]
#     recording_time = session[11:]
#     file_name = 'TS_session_{}_{}_{}.pkl'.format(sess_ind + 1, rat, recording_day)
#     dir = os.path.join(os.path.join(DATA_FOLDER, 'neo', data_version), rat, session)
#     bl = pickle.load(open(os.path.join(dir, file_name), 'rb'))
#
#     tuples = [(sess_ind, s.annotations['trial_number'], s.annotations['trial_outcome'],
#       s.annotations['trial_side'])  for s in bl.segments]
#
#     dx = pd.DataFrame(tuples, columns=['sess_ind', 'trial_n', 'trial_outcome', 'trial_side'])
#     dfs.append(dx)
#
# tdf = pd.concat(dfs)
#

# sess_trial_pbes = ['{}{}'.format(r['sess_ind'], r['trial_n']) for i, r in df.iterrows()]
# sess_trial_neo = ['{}{}'.format(r['sess_ind'], r['trial_n']) for i, r in tdf.iterrows()]
#
# print(len(set(sess_trial_pbes) ^ set(sess_trial_neo)))
#
#
#
#
for i, row in df.iterrows():
    si = row['sess_ind']
    tn = row['trial_n']
    sel = tdf[(tdf['sess_ind'] == si) & (tdf['trial_n'] == tn)]
    if sel.shape[0] == 0:
        print('yo')
        break


dfs = []
for sess_ind in sessions:
    bl = get_session_block(DATA_FOLDER, sess_ind, epoch, data_version=data_version)

    trial_start_times = [s.t_start.rescale(pq.ms) for s in bl.segments[:-1]]
    trial_sides = [s.annotations['trial_side'] for s in bl.segments[:-1]]
    trial_outcomes = [s.annotations['trial_outcome'] for s in bl.segments[:-1]]

    # add current and previous trial side
    trial_n, current_sides, previous_sides = [], [], []
    current_trial_outcomes, previous_trial_outcomes = [], []

    for time in df[df['sess_ind'] == sess_ind]['start_time_in_ms']:
        idx = find_index_nearest_preceding(trial_start_times, time)
        current_side = trial_sides[idx]
        current_trial_outcome = trial_outcomes[idx]
        if idx == 0:
            previous_side = np.nan
            previous_trial_outcome = np.nan
        else:
            previous_side = trial_sides[idx-1]
            previous_trial_outcome = trial_outcomes[idx-1]
        trial_n.append(idx)
        current_sides.append(current_side)
        previous_sides.append(previous_side)
        current_trial_outcomes.append(current_trial_outcome)
        previous_trial_outcomes.append(previous_trial_outcome)

    dx = pd.DataFrame(data={'sess_ind' : np.repeat(sess_ind, len(trial_n)),
                            'trial_n' : trial_n,
                            'current_trial_outcome' : current_trial_outcomes,
                            'previous_trial_outcome' : previous_trial_outcomes,
                            'current_side' : current_side})
    dfs.append(dx)


tdf = pd.concat(dfs)



sess_trial_pbes = ['{}{}'.format(r['sess_ind'], r['trial_n']) for i, r in df.iterrows()]
sess_trial_neo = ['{}{}'.format(r['sess_ind'], r['trial_n']) for i, r in tdf.iterrows()]

print(len(set(sess_trial_pbes) ^ set(sess_trial_neo)))

df['current_trial_outcome'] = None
df['previous_trial_outcome'] = None

for i, row in df.iterrows():
    si = row['sess_ind']
    tn = row['trial_n']
    sel = tdf[(tdf['sess_ind'] == si) & (tdf['trial_n'] == tn)]
    df.loc[i, 'current_trial_outcome'] = sel['current_trial_outcome'].iloc[0]
    df.loc[i, 'previous_trial_outcome'] = sel['previous_trial_outcome'].iloc[0]

