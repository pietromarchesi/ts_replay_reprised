import neo
import os
import pandas as pd
import numpy as np
from utils import get_session_block
from constants import *
import quantities as pq

data_version = 'dec16'
session_inds = range(46)

df = pd.DataFrame(columns=['sess_ind', 'animal', 'day', 'session', 'n_trials',
                           'nHC', 'nPR', 'nV1', 'nBR',
                           'has_sleep', 'pre_sleep_min', 'post_sleep_min'])


for sess_ind in session_inds:
    bl = get_session_block(data_folder=DATA_FOLDER, sess_ind=sess_ind,
                           epoch='task', data_version=data_version)
    animal = bl.annotations['animal_ID']
    day = bl.annotations['day_folder']
    session = bl.annotations['session_folder']
    segment = bl.segments[0]
    n_trials = len(bl.segments)

    n_hc = len(segment.filter(targdict={'area': 'Hippocampus'}, objects=neo.SpikeTrain))
    n_pr = len(segment.filter(targdict={'area': 'Perirhinal'}, objects=neo.SpikeTrain))
    n_br = len(segment.filter(targdict={'area': 'Barrel'}, objects=neo.SpikeTrain))
    n_v1 = len(segment.filter(targdict={'area': 'V1'}, objects=neo.SpikeTrain))

    assert n_hc + n_pr + n_br + n_v1 == len(bl.list_units)

    try:
        bls = get_session_block(data_folder=DATA_FOLDER, sess_ind=sess_ind,
                               epoch='pre_sleep', data_version=data_version)
        has_sleep = True
        # this is just the total time of the sleep session
        # pre_sleep_min = (bls.segments[0].t_stop - bls.segments[0].t_start).rescale(pq.min)
        # post_sleep_min = (bls.segments[1].t_stop - bls.segments[1].t_start).rescale(pq.min)
        pre_sleep_min = np.diff(bls.segments[0].events[0].times)[::2].sum().rescale(pq.min)
        post_sleep_min = np.diff(bls.segments[1].events[0].times)[::2].sum().rescale(pq.min)
        pre_sleep_min = np.round(pre_sleep_min.item(), 2)
        post_sleep_min = np.round(post_sleep_min.item(), 2)

    except FileNotFoundError:
        has_sleep = False
        pre_sleep_min = 0
        post_sleep_min = 0

    row = [sess_ind, animal, day, session, n_trials, n_hc, n_pr, n_v1, n_br, has_sleep,
           pre_sleep_min, post_sleep_min]

    df.loc[df.shape[0], :] = row
    assert df.loc[sess_ind, 'nBR'] == n_br
    assert df.loc[sess_ind, 'nPR'] == n_pr
    assert df.loc[sess_ind, 'nHC'] == n_hc
    assert df.loc[sess_ind, 'nV1'] == n_v1


df.index = df['sess_ind']
df.drop('sess_ind', axis=1, inplace=True)
output_path = os.path.join(DATA_FOLDER, 'sessions_overview.pkl')
df.to_pickle(output_path)

from constants import *

