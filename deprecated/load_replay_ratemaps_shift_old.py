import os
from constants import *
import pickle
import statsmodels.stats.descriptivestats
from statsmodels.stats.multitest import multipletests
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
from plotting_style import *
from sklearn.preprocessing import MinMaxScaler
from scipy.ndimage import gaussian_filter1d
from utils import *


ratemap_settings = 'may11_DecSet2_smooth2_morelages'
#ratemap_settings = 'jul4_DecSet2_smooth2_morelages'

#ratemap_settings = 'feb10_DecSet26_taskphase'
ratemap_area = 'Perirhinal'

save_plots = True
plot_format = 'png'
dpi = 400

stat_test = 'sign_test' #'sign_test', 't-test'
sign_alpha = 0.01
p_val_correction = 'fdr_bh'
variables = ['percentile_rank', 'corrcoef_debiased']  # ['corrcoef_obs', 'corrcoef_debiased']
test_values = [50, 0] # [0, 0]
nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
cols_to_check = ['phase', 'direction', 'has_ripple', 'null_method']
subselect_sessions = None
null_dist_method_for_plot_both_shuff = 'rotate_spikes'




# PEAK TIME DISTRIBUTION
p_val_thr_peak_time_dist = 0.00001


subselect_shifts = False
min_shift = -8
max_shift = 8

# --- SET UP PATHS ------------------------------------------------------------
plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# --- LOAD RESULTS ------------------------------------------------------------
res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)
file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
df = res['df']
pars = res['pars']
shift_amounts = pars['shift_amounts']
smoothing_sigma = pars['smooth_sigma']


if subselect_sessions is not None:
    df = df[np.isin(df['sess_ind'], subselect_sessions)]

if subselect_shifts:
    df = df[df['shift_amount'] >= min_shift]
    df = df[df['shift_amount'] <= max_shift]
    shift_amounts = np.arange(min_shift, max_shift+1)

    #shift_amounts = np.array([-8, -6, -4, -2, 0, 2, 4, 6, 8])

# --- SET UP UTILITIES --------------------------------------------------------



def get_significance_mask(dfx, variable='corrcoef_debiased', test_value=0,
                          stat_test='t-test', correction='fdr_by', sign_alpha=0.05):
    p_vals = []
    for shift_amount in np.sort(dfx['shift_amount'].unique()):
        ccd = dfx[dfx['shift_amount'] == shift_amount][variable]
        if stat_test == 'sign_test':
            #m, p_val = statsmodels.stats.descriptivestats.sign_test(ccd, test_value)
            m, p_val = scipy.stats.wilcoxon(ccd - test_value)
        elif stat_test == 't-test':
            t, p_val = scipy.stats.ttest_1samp(ccd, test_value)
        p_vals.append(p_val)
    print(p_vals)
    mask, pvals_corr, alphacs, alphacb = multipletests(p_vals, alpha=sign_alpha,
                                                       method=correction)
    return mask, pvals_corr



def get_significance_mask_split(df1, df2, variable='corrcoef_debiased',
                                stat_test='t-test', correction='fdr_by',
                                sign_alpha=0.05):

    p_vals = []
    for shift_amount in np.sort(df1['shift_amount'].unique()):
        var1 = df1[df1['shift_amount'] == shift_amount][variable]
        var2 = df2[df2['shift_amount'] == shift_amount][variable]
        # TODO CHECK IF THESE ARE PAIRED
        if stat_test == 'sign_test':
            m, p_val = scipy.stats.mannwhitneyu(var1, var2)
        elif stat_test == 't-test':
            t, p_val = scipy.stats.ttest_ind(var1, var2)
        p_vals.append(p_val)
    print(p_vals)
    mask, pvals_corr, alphacs, alphacb = multipletests(p_vals, alpha=sign_alpha,
                                                       method=correction)

    return mask, pvals_corr



# --- STATISTICS NO SHIFT ------------------------------------------------------

for variable, test_value in zip(variables, test_values):
    for null_method in nulldistmet_plots:
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'all') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_method)]

        ccd = dfx[dfx['shift_amount'] == 0][variable]


        if stat_test == 'sign_test' :
            #m, p_val = statsmodels.stats.descriptivestats.sign_test(ccd, test_value)
            m, p_val = scipy.stats.wilcoxon(ccd - test_value)
        elif stat_test == 't-test' :
            t, p_val = scipy.stats.ttest_1samp(ccd, test_value)

        q50, q25, q75 = ccd.quantile([0.5, 0.25, 0.75])
        print('{} {}: {:.2f}, {:.2f}-{:.2f}'.format(variable, null_method,
                                                    q50, q25, q75))
        print('{} different from {} with {} distribution: p={:.3}'.format(
            variable, test_value, null_method, p_val
        ))


# --- GLOBAL SHIFT COMBINED ----------------------------------------------------


for variable, test_value in zip(variables, test_values):

    # Create the significance mask using both shuffles
    masks = []
    for null_distribution_method in nulldistmet_plots:
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'all') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_distribution_method)]

        for col in cols_to_check:
            assert dfx[col].unique().shape[0] == 1

        mask, pvals_corr = get_significance_mask(dfx, correction=p_val_correction,
                                                 variable=variable,
                                                 stat_test=stat_test,
                                                 test_value=test_value,
                                                 sign_alpha=sign_alpha)
        masks.append(mask)
    mask = np.logical_and(masks[0], masks[1])


    # get the values to plot based on one shuffle
    dfx = df[(df['phase'] == 'all') &
             (df['direction'] == 'all') &
             (df['has_ripple'] == 'all') &
             (df['null_method'] == null_dist_method_for_plot_both_shuff)]

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

    line_color = make_rgb_transparent(area_palette[ratemap_area], (1, 1, 1), 0.9)

    sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax,
                 markers=True, color=line_color, style='null_method',
                 markersize=5, estimator=np.median)
    ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))
    height = ax.get_ylim()[0] + 0.01

    signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
    ax.scatter(shift_amounts, signif_line, lw=1, alpha=1, c=line_color)
    ax.set_ylabel('{} replay coordination strength'.format(ratemap_area_sigla[ratemap_area]))
    ax.set_xlabel('Time shift [ms]')
    ax.set_xticks([-10, -5, 0, 5, 10])
    ax.set_xticklabels([-100, -50, 0, 50, 100])
    sns.despine()
    ax.axhline(test_value, c='grey', ls='--', lw=2, zorder=-1)
    ax.get_legend().remove()
    plt.tight_layout()

    if save_plots:
        plot_name = 'ratemap_shift_BOTHSHUF_{}_{}_{}.' \
                    '{}'.format(ratemap_area, variable, null_dist_method_for_plot_both_shuff, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- GLOBAL SHIFT SPLIT BY REPLAY DIRECTION BOTH SHUFFLES ---------------------

for variable, test_value in zip(variables, test_values):

    # make the masks using both shuffles
    masks = {'positive' : [],
             'negative' : [],
             'difference' : []}

    for null_distribution_method in nulldistmet_plots:

        for direction in ['positive', 'negative']:
            dfx = df[(df['phase'] == 'all') &
                     (df['direction'] == direction) &
                     (df['has_ripple'] == 'all') &
                     (df['null_method'] == null_distribution_method)]

            for col in cols_to_check:
                assert dfx[col].unique().shape[0] == 1

            mask, pvals_corr = get_significance_mask(dfx, correction=p_val_correction,
                                                     variable=variable,
                                                     stat_test=stat_test,
                                                     test_value=test_value,
                                                     sign_alpha=sign_alpha)

            masks[direction].append(mask)


    # find the difference mask using again both shuffles
    for null_distribution_method in nulldistmet_plots:

        df_split = {}
        for direction in ['positive', 'negative']:
            dfx = df[(df['phase'] == 'all') &
                     (df['direction'] == direction) &
                     (df['has_ripple'] == 'all') &
                     (df['null_method'] == null_distribution_method)]
            df_split[direction] = dfx

        mask, pvals_corr = get_significance_mask_split(df_split['positive'],
                                                       df_split['negative'],
                                                 correction=p_val_correction,
                                                 variable=variable,
                                                 stat_test=stat_test,
                                                 sign_alpha=sign_alpha)
        masks['difference'].append(mask)

    # combine masks
    masks['positive'] = np.logical_and(masks['positive'][0], masks['positive'][1])
    masks['negative'] = np.logical_and(masks['negative'][0], masks['negative'][1])
    masks['difference'] = np.logical_and(masks['difference'][0], masks['difference'][1])


    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

    for direction in ['positive', 'negative'] :
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == direction) &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_dist_method_for_plot_both_shuff)]
        sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax,
                     color=replay_direction_palette[direction], label=direction,
                     markers=True, style='null_method', markersize=5)



    #ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))

    for i, (dir, mask) in enumerate(masks.items()):
        height = ax.get_ylim()[0] + 0.02 * i
        signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
        ax.scatter(shift_amounts, signif_line, color=replay_direction_palette[dir],
                lw=1,alpha=1)

    sns.despine()
    ax.legend(frameon=False)
    ax.set_ylabel('{} replay coordination strength'.format(ratemap_area_sigla[ratemap_area]))
    ax.set_xlabel('Time shift [ms]')
    ax.set_xticks([-10, -5, 0, 5, 10])
    ax.set_xticklabels([-100, -50, 0, 50, 100])
    ax.get_legend().remove()
    ax.axhline(test_value, c='grey', ls='--', lw=2, zorder=-1)
    plt.tight_layout()

    if save_plots:
        plot_name = 'ratemap_shift_by_replay_direction_BOTHSHUF_{}_{}_{}.' \
                    '{}'.format(ratemap_area, variable, null_dist_method_for_plot_both_shuff, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- GLOBAL SHIFT SPLIT BY LOW VS HIGH RSI -----------------------------------


# df['rsi_bin'] = ['RSI above thr.' if i > 1 else
#                  'RSI below thr.' for i in df['rsi']]
#
#
# for variable, test_value in zip(variables, test_values):
#     for null_distribution_method in nulldistmet_plots:
#
#         f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#         masks = {}
#         df_split = []
#
#         for rsi in ['RSI above thr.', 'RSI below thr.']:
#
#             dfx = df[(df['phase'] == 'all') &
#                      (df['direction'] == 'all') &
#                      (df['rsi_bin'] == rsi) &
#                      (df['has_ripple'] == 'all') &
#                      (df['null_method'] == null_distribution_method)]
#
#             for col in cols_to_check:
#                 assert dfx[col].unique().shape[0] == 1
#
#             mask, pvals_corr = get_significance_mask(dfx, correction='fdr_bh',
#                                                      variable=variable,
#                                                      stat_test=stat_test,
#                                                      test_value=test_value,
#                                                      sign_alpha=sign_alpha)
#
#             sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax,
#                          color=rsi_palette[rsi], label=rsi)
#
#             masks[rsi] = mask
#             df_split.append(dfx)
#
#         mask, pvals_corr = get_significance_mask_split(df_split[0], df_split[1],
#                                                  correction=p_val_correction,
#                                                  variable=variable,
#                                                  stat_test=stat_test,
#                                                  sign_alpha=sign_alpha)
#         masks['difference'] = mask
#
#         ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))
#
#         for i, (rsi, mask) in enumerate(masks.items()):
#             print(rsi, mask)
#             height = ax.get_ylim()[0] + 0.02 * i
#             y = np.repeat(height, len(shift_amounts))
#
#             #sa_diff = np.diff(shift_amounts)
#             #assert np.unique(sa_diff).shape[0] == 1
#             #sa_diff = sa_diff[0]
#             #x = np.append(shift_amounts, shift_amounts[-1]+sa_diff) - sa_diff / 2
#             signif_line = np.ma.masked_where(~mask, y)
#             ax.scatter(shift_amounts, signif_line, color=rsi_palette[rsi],
#                     lw=1,alpha=1)
#
#         sns.despine()
#         ax.set_ylabel(variable_label[variable])
#         ax.set_xlabel('Time shift [ms]')
#         ax.set_xticks([-10, -5, 0, 5, 10])
#         ax.set_xticklabels([-100, -50, 0, 50, 100])
#         ax.legend(frameon=False)
#         plt.tight_layout()
#
#
#         if save_plots:
#             plot_name = 'ratemap_shift_by_RSI_score_{}_{}_{}.' \
#                         '{}'.format(ratemap_area, variable, null_distribution_method, plot_format)
#             f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#


# --- HIGH RSI COMBINED ----------------------------------------------------

#
# df['rsi_bin'] = ['RSI above thr.' if i > 0.6 else
#                  'RSI below thr.' for i in df['rsi']]
#
# for variable, test_value in zip(variables, test_values):
#
#     # Create the significance mask using both shuffles
#     masks = []
#     for null_distribution_method in nulldistmet_plots:
#         dfx = df[(df['phase'] == 'all') &
#                  (df['direction'] == 'all') &
#                  (df['has_ripple'] == 'all') &
#                  (df['null_method'] == null_distribution_method) &
#                  (df['rsi_bin'] == 'RSI above thr.')]
#
#         for col in cols_to_check:
#             assert dfx[col].unique().shape[0] == 1
#
#         mask, pvals_corr = get_significance_mask(dfx, correction=p_val_correction,
#                                                  variable=variable,
#                                                  stat_test=stat_test,
#                                                  test_value=test_value,
#                                                  sign_alpha=sign_alpha)
#         masks.append(mask)
#     mask = np.logical_and(masks[0], masks[1])
#
#
#     # get the values to plot based on one shuffle
#     dfx = df[(df['phase'] == 'all') &
#              (df['direction'] == 'all') &
#              (df['has_ripple'] == 'all') &
#              (df['null_method'] == null_dist_method_for_plot_both_shuff)]
#
#     f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#     sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax)
#     ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))
#     height = ax.get_ylim()[0] + 0.01
#     signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
#     ax.scatter(shift_amounts, signif_line, lw=1, alpha=1)
#     ax.set_ylabel(variable_label[variable])
#     ax.set_xlabel('Time shift [ms]')
#     ax.set_xticks([-10, -5, 0, 5, 10])
#     ax.set_xticklabels([-100, -50, 0, 50, 100])
#     sns.despine()
#     plt.tight_layout()
#
#     if save_plots:
#         plot_name = 'ratemap_shift_high_RSI_BOTHSHUF_{}_{}.' \
#                     '{}'.format(ratemap_area, variable, plot_format)
#         f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#
#



# --- DISTRIBUTION OF PEAK CORRELATION SHIFTS ----------------------------------

#
#
# bins = np.arange(pars['shift_amounts'][0]*10, pars['shift_amounts'][-1]*10+1, 5)
#
#
# for variable in ['corrcoef_debiased']:
#     for direction in ['all']:
#         for null_distribution_method in nulldistmet_plots:
#
#             dfx = df[(df['phase'] == 'all') &
#                      (df['direction'] == direction) &
#                      (df['has_ripple'] == 'all') &
#                      (df['null_method'] == null_distribution_method) &
#                      (df['p_values'] <= p_val_thr_rateplots)]
#
#             #dfx = dfx[(dfx['shift_amount'] >= -5) & (dfx['shift_amount'] < 5)]
#
#             dfx = dfx.reset_index().drop('index', axis=1)
#             # dfsel = dfx.sort_values(variable, ascending=False).drop_duplicates(['sess_ind', 'unit'])
#             # dfsel['shift_amount_ms'] = dfsel['shift_amount'] * 10
#             # peak_shifts = dfsel['shift_amount_ms']
#             #
#             dfx['unit_id'] = ['{}_{}'.format(r['sess_ind'], r['unit']) for i, r
#                               in dfx.iterrows()]
#
#             peak_shifts = []
#             for unit_id in dfx['unit_id'].unique() :
#                 y = dfx[dfx['unit_id'] == unit_id].sort_values(by='shift_amount')[
#                     'corrcoef_debiased'].values
#                 peak_shifts.append(shift_amounts[y.argmax()]*10)
#
#             f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#             sns.distplot(peak_shifts, hist=True, bins=bins,
#                          kde=False, norm_hist=True, color='gray')
#             plt.tight_layout()
#             ax.set_xlabel('Time of peak correlation (ms)')
#             ax.set_xlim([shift_amounts[0]*10, shift_amounts[-1]*10])
#             sns.despine()
#
#             if save_plots :
#                 plot_name = 'distribution_of_peak_times_of_highest_{}_{}_{}_{}.' \
#                             '{}'.format(variable, ratemap_area,
#                                         null_distribution_method, direction, plot_format)
#                 f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#
#
#
#
#
# dfx = df[(df['phase'] == 'all') &
#          (df['direction'] == direction) &
#          (df['has_ripple'] == 'all') &
#          (df['null_method'] == null_distribution_method)]
#
#
# dfx['unit_id'] = ['{}_{}'.format(r['sess_ind'], r['unit']) for i, r in dfx.iterrows()]
#
# f, ax = plt.subplots(1, 1)
# for unit_id in dfx['unit_id'].unique()[0:20]:
#
#     y = dfx[dfx['unit_id'] == unit_id].sort_values(by='shift_amount')
#     y = y['corrcoef_obs'].values
#     print(y.argmax())
#     if y.mean() > 0:
#         ax.plot(y)
#


# for null_distribution_method in nulldistmet_plots:
#
#     dfx = df[(df['phase'] == 'all') &
#              (df['direction'] == 'all') &
#              (df['has_ripple'] == 'all') &
#              (df['null_method'] == null_distribution_method) &
#              (df['shift_amount'] == 0)]
#              #(df['p_values'] <= 0.05)]
#
#     f, ax = plt.subplots(1, 1, figsize=[4, 4])
#     sns.regplot(data=dfx, x='rsi', y='percentile_rank', color='m', ax=ax)
#
#

# --- GLOBAL SHIFT SPLIT BY RIPPLES -------------------------------------------
#
# for variable, test_value in zip(variables, test_values):
#     for null_distribution_method in nulldistmet_plots:
#
#
#         f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#
#         masks = {}
#         df_split = []
#
#         for has_ripple in ['True', 'False']:
#             dfx = df[(df['phase'] == 'all') &
#                      (df['direction'] == 'all') &
#                      (df['has_ripple'] == has_ripple) &
#                      (df['null_method'] == null_distribution_method)]
#
#             for col in cols_to_check:
#                 assert dfx[col].unique().shape[0] == 1
#
#             mask, pvals_corr = get_significance_mask(dfx, correction='fdr_bh',
#                                                      variable=variable,
#                                                      stat_test=stat_test,
#                                                      test_value=test_value,
#                                                      sign_alpha=sign_alpha)
#
#             sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax,
#                          color=has_ripple_palette[has_ripple], label=has_ripple)
#
#             #mask[3:6] = True
#             masks[direction] = mask
#             df_split.append(dfx)
#
#         mask, pvals_corr = get_significance_mask_split(df_split[0], df_split[1],
#                                                  correction=p_val_correction,
#                                                  variable=variable,
#                                                  stat_test=stat_test,
#                                                  sign_alpha=sign_alpha)
#         masks['difference'] = mask
#
#         ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))
#
#         for i, (dir, mask) in enumerate(masks.items()):
#             height = ax.get_ylim()[0] + 0.01 * i
#             signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
#             ax.scatter(shift_amounts, signif_line, color=replay_direction_palette[dir],
#                     lw=1,alpha=1)
#
#         sns.despine()
#         ax.set_ylabel('Correlation coefficient')
#         ax.set_xlabel('Time shift [ms]')
#         ax.set_xticks([-10, -5, 0, 5, 10])
#         ax.set_xticklabels([-100, -50, 0, 50, 100])
#         ax.legend(frameon=False)
#         plt.tight_layout()
#
#         if save_plots:
#             plot_name = 'ratemap_shift_by_ripples_{}_{}_{}.' \
#                         '{}'.format(ratemap_area, variable, null_distribution_method, plot_format)
#             f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#
#
#
#
#
#
#

if df['phase'].unique().shape[0] > 1 and False:

    for variable, test_value in zip(variables, test_values):
        for null_distribution_method in nulldistmet_plots:


            f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

            masks = {}
            df_split = []

            for phase in ['reward', 'iti', 'img']:
                dfx = df[(df['phase'] == phase) &
                         (df['direction'] == 'all') &
                         (df['has_ripple'] == 'all') &
                         (df['null_method'] == null_distribution_method)]

                for col in cols_to_check:
                    assert dfx[col].unique().shape[0] == 1

                mask, pvals_corr = get_significance_mask(dfx, correction='fdr_bh',
                                                         variable=variable,
                                                         stat_test=stat_test,
                                                         test_value=test_value,
                                                         sign_alpha=sign_alpha)

                sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax,
                             color=task_phase_palette[phase], label=phase)

                #mask[3:6] = True
                masks[phase] = mask
                df_split.append(dfx)

            mask, pvals_corr = get_significance_mask_split(df_split[0], df_split[1],
                                                     correction=p_val_correction,
                                                     variable=variable,
                                                     stat_test=stat_test,
                                                     sign_alpha=sign_alpha)
            masks['difference'] = mask

            ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))

            for i, (phase, mask) in enumerate(masks.items()):
                height = ax.get_ylim()[0] + 0.01 * i
                signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
                ax.scatter(shift_amounts, signif_line, color=task_phase_palette[phase],
                        lw=1,alpha=1)

            sns.despine()
            ax.set_ylabel('Correlation coefficient')
            ax.set_xlabel('Time shift [ms]')
            ax.set_xticks([-10, -5, 0, 5, 10])
            ax.set_xticklabels([-100, -50, 0, 50, 100])
            ax.legend(frameon=False)
            ax.axhline(test_value, c='grey', ls='--', lw=2, zorder=-1)
            plt.tight_layout()

            if save_plots:
                plot_name = 'ratemap_shift_by_phase_{}_{}_{}.' \
                            '{}'.format(ratemap_area, variable, null_distribution_method, plot_format)
                f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)





# --- PHASE BY DIRECTION COMBINE-------------------------------------------------

if df['phase'].unique().shape[0] > 1 and False:
    for phase in ['iti', 'img', 'reward']:

        for variable, test_value in zip(variables, test_values):

            # make the masks using both shuffles
            masks = {'positive' : [],
                     'negative' : [],
                     'difference' : []}

            for null_distribution_method in nulldistmet_plots:

                for direction in ['positive', 'negative']:
                    dfx = df[(df['phase'] == phase) &
                             (df['direction'] == direction) &
                             (df['has_ripple'] == 'all') &
                             (df['null_method'] == null_distribution_method)]

                    if dfx.shape[0] > 0:
                        for col in cols_to_check:
                            assert dfx[col].unique().shape[0] == 1

                        mask, pvals_corr = get_significance_mask(dfx, correction=p_val_correction,
                                                                 variable=variable,
                                                                 stat_test=stat_test,
                                                                 test_value=test_value,
                                                                 sign_alpha=sign_alpha)

                        masks[direction].append(mask)


            # find the difference mask using again both shuffles
            for null_distribution_method in nulldistmet_plots:

                df_split = {}
                for direction in ['positive', 'negative']:
                    dfx = df[(df['phase'] == phase) &
                             (df['direction'] == direction) &
                             (df['has_ripple'] == 'all') &
                             (df['null_method'] == null_distribution_method)]
                    df_split[direction] = dfx

                mask, pvals_corr = get_significance_mask_split(df_split['positive'],
                                                               df_split['negative'],
                                                         correction=p_val_correction,
                                                         variable=variable,
                                                         stat_test=stat_test,
                                                         sign_alpha=sign_alpha)
                masks['difference'].append(mask)

            # combine masks
            masks['positive'] = np.logical_and(masks['positive'][0], masks['positive'][1])
            masks['negative'] = np.logical_and(masks['negative'][0], masks['negative'][1])
            masks['difference'] = np.logical_and(masks['difference'][0], masks['difference'][1])


            f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

            for direction in ['positive', 'negative'] :
                dfx = df[(df['phase'] == phase) &
                         (df['direction'] == direction) &
                         (df['has_ripple'] == 'all') &
                         (df['null_method'] == null_dist_method_for_plot_both_shuff)]
                sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax,
                             color=replay_direction_palette[direction], label=direction,
                             markers=True, style='null_method', markersize=5)



            #ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))

            for i, (dir, mask) in enumerate(masks.items()):
                height = ax.get_ylim()[0] + 0.02 * i
                signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
                ax.scatter(shift_amounts, signif_line, color=replay_direction_palette[dir],
                        lw=1,alpha=1)

            sns.despine()
            ax.legend(frameon=False)
            ax.set_ylabel('{} replay coordination strength'.format(ratemap_area_sigla[ratemap_area]))
            ax.set_xlabel('Time shift [ms]')
            ax.set_xticks([-10, -5, 0, 5, 10])
            ax.set_xticklabels([-100, -50, 0, 50, 100])
            ax.get_legend().remove()
            ax.axhline(test_value, c='grey', ls='--', lw=2, zorder=-1)
            plt.tight_layout()

            if save_plots:
                plot_name = 'ratemap_shift_by_replay_direction_by_phase_BOTHSHUF_{}_{}_{}_{}.' \
                            '{}'.format(ratemap_area, variable, null_dist_method_for_plot_both_shuff, phase, plot_format)
                f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)





# for variable, test_value in zip(variables, test_values):
#     for null_method in nulldistmet_plots:
#         dfx = df[(df['phase'] == 'all') &
#                  (df['direction'] == 'all') &
#                  (df['has_ripple'] == 'all') &
#                  (df['null_method'] == null_method)]
#
#         ccd = dfx[dfx['shift_amount'] == 0][variable]
#         f, ax = plt.subplots(1, 1)
#         sns.violinplot(x=ccd)
#         ax.set_title('{}, {}'.format(variable, null_method))
#
#         if stat_test == 'sign_test' :
#             #m, p_val = statsmodels.stats.descriptivestats.sign_test(ccd, test_value)
#             m, p_val = scipy.stats.wilcoxon(ccd - test_value)
#
#         elif stat_test == 't-test' :
#             t, p_val = scipy.stats.ttest_1samp(ccd, test_value)
#
#         print('{} different from {} with {} distribution: p={:.3}'.format(
#             variable, test_value, null_method, p_val
#         ))







        #t, p = scipy.stats.wilcoxon(df1['corrcoef_debiased'], df2['corrcoef_debiased'])



# for null_distribution_method in nulldistmet_plots :
#     dfx = df[(df['phase'] == 'all') &
#              (df['direction'] == 'all') &
#              (df['has_ripple'] == 'all') &
#              (df['null_method'] == null_distribution_method) &
#              (df['shift_amount'] == 0)]
#
#     f, ax = plt.subplots(1, 1)
#     sns.distplot(dfx['p_values'])
#     ax.set_title(null_distribution_method)




# --- GLOBAL SHIFT -------------------------------------------------------------
# for variable, test_value in zip(variables, test_values):
#     for null_distribution_method in nulldistmet_plots:
#         dfx = df[(df['phase'] == 'all') &
#                  (df['direction'] == 'all') &
#                  (df['has_ripple'] == 'all') &
#                  (df['null_method'] == null_distribution_method)]
#
#         for col in cols_to_check:
#             assert dfx[col].unique().shape[0] == 1
#
#         mask, pvals_corr = get_significance_mask(dfx, correction=p_val_correction,
#                                                  variable=variable,
#                                                  stat_test=stat_test,
#                                                  test_value=test_value,
#                                                  sign_alpha=sign_alpha)
#
#         f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#         sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax,
#                      markers=True, color=line_plot_color, style='null_method',
#                      markersize=5)
#         ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))
#         height = ax.get_ylim()[0] + 0.01
#         signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
#         ax.scatter(shift_amounts, signif_line, lw=1, alpha=1, c=line_plot_color)
#         ax.set_ylabel(variable_label[variable])
#         ax.set_xlabel('Time shift [ms]')
#         ax.set_xticks([-10, -5, 0, 5, 10])
#         ax.set_xticklabels([-100, -50, 0, 50, 100])
#         sns.despine()
#         ax.get_legend().remove()
#         plt.tight_layout()
#
#         if save_plots:
#             plot_name = 'ratemap_shift_{}_{}_{}.' \
#                         '{}'.format(ratemap_area, variable, null_distribution_method, plot_format)
#             f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

