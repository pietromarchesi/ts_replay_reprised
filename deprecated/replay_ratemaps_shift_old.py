import matplotlib.pyplot as plt
from constants import *
from utils import *
import quantities as pq
from plotting_style import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from replaydetector.replayratemaps import ReplayRatemaps
import copy
import statsmodels.stats.descriptivestats
from utils import get_pbe_modulation

sessions                 = sessions_HC_PR_replay

ratemap_area             = 'Perirhinal'
PBEs_area                = 'Hippocampus'
dec_area                 = 'Hippocampus'
PBEs_settings_name       = 'july8'
PBEs_epoch               = 'task'
dec_settings_name        = 'dkwjuly4'


# filter pbes parameters
shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle',
                 'unit_identity_shuffle']
shuffle_p_vals = [0.1, 0.2, 0.3]


speed_thr_real_rm        = 3
only_pyramidal           = True
time_bin_size_task_in_ms = 200
min_speed                = 5


task_phase               = None
replay_direction         = 'positive'
null_distribution_method = 'rotate_spikes'

shift_amounts = np.arange(-7, 8)

bin_size_train           = 300

# --- LOAD ALL SESSIONS -------------------------------------------------------
# We first load and bin the data for all sessions (both task and pbe data)


sess_data = {}

for sess_ind in sessions:
    # --- LOAD TASK DATA ------------------------------------------------------
    # Needed to build the ratemaps of the area during normal locomotion

    session_data = unpack_session(sess_ind, data_folder=DATA_FOLDER,
                                  area=ratemap_area,
                                  binsize_in_ms=time_bin_size_task_in_ms)

    speed_real = session_data['speed'].flatten()
    binned_spikes_task = session_data['spikes']
    binned_position_task = session_data['bin']

    binned_spikes_task = binned_spikes_task[speed_real >= speed_thr_real_rm, :]
    binned_position_task = binned_position_task[speed_real >= speed_thr_real_rm]

    trains = session_data['trains']

    # --- COMUPTE UNIT PROPERTIES ---------------------------------------------

    # --- LOAD PBEs -----------------------------------------------------------

    PBEs_folder = os.path.join(REPLAY_FOLDER, 'results', 'decoded_PBEs',
                               'pbe_setting_{}_{}'.format(PBEs_settings_name,
                                                          PBEs_area),
                               'dec_setting_{}_{}'.format(dec_settings_name,
                                                          dec_area))

    PBEs_file = os.path.join(PBEs_folder, 'decoded_PBEs_sess_{:02}_{}_'
                                          '{}.pkl'.format(sess_ind, dec_area,
                                                          PBEs_epoch))

    pbes_data = pickle.load(open(PBEs_file, 'rb'))

    pbes_df = pbes_data['pbes']
    sig_df = pbes_data['sig_df']

    bin_size_pbe_in_ms = pbes_data['decode_pars']['bin_size_pbe_in_ms']
    slide_by_pbe_in_ms = pbes_data['decode_pars']['slide_by_pbe_in_ms']

    # --- FILTER PBEs ---------------------------------------------------------

    sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types,
                                            p_vals=shuffle_p_vals)
    pbes_df = pbes_df[np.isin(pbes_df.index, sel_pbes_sig)]

    if task_phase is not None:
        pbes_df = pbes_df[pbes_df['phase'] == task_phase]

    if replay_direction is not None:
        pbes_df = pbes_df[pbes_df['replay_direction'] == replay_direction]

    # --- BIN PBEs ------------------------------------------------------------

    PBEs_times = [(row['start_time_in_ms'] * pq.ms, row['end_time_in_ms'] * pq.ms)
        for i, row in pbes_df.iterrows()]

    binned_position_replay = pbes_df['trajectory'].tolist()

    binned_pbes = bin_PBEs(trains, PBEs_times,
                           bin_size_in_ms=bin_size_pbe_in_ms,
                           sliding_binning=True,
                           slide_by_in_ms=slide_by_pbe_in_ms,
                           pyramidal_only=False)

    binned_spikes_replay = binned_pbes['PBEs_binned']

    sess_data[sess_ind] = {}
    sess_data[sess_ind]['binned_spikes_task'] = binned_spikes_task
    sess_data[sess_ind]['binned_spikes_replay'] = binned_spikes_replay
    sess_data[sess_ind]['binned_position_task'] = binned_position_task
    sess_data[sess_ind]['binned_position_replay'] = binned_position_replay
    sess_data[sess_ind]['unit_labels'] = range(len(trains))
    sess_data[sess_ind]['time_bin_size_task_in_ms'] = time_bin_size_task_in_ms
    sess_data[sess_ind]['bin_size_pbe_in_ms'] = bin_size_pbe_in_ms






# --- BUILD RATEMAPS ----------------------------------------------------------
# Here we first loop over all shifts. For every shift, we go through the
# sessions, shift the data, and compute the ratemaps and their correlation.
# Then we aggregate across all the sessions for a given shift.

shiftratemaps = {}

for sa in shift_amounts:

    ratemaps = {}

    for sess_ind in sessions:

        binned_spikes_task = sess_data[sess_ind]['binned_spikes_task']
        binned_spikes_replay = sess_data[sess_ind]['binned_spikes_replay']
        binned_position_task = sess_data[sess_ind]['binned_position_task']
        binned_position_replay = sess_data[sess_ind]['binned_position_replay']
        unit_labels = sess_data[sess_ind]['unit_labels']
        time_bin_size_task_in_ms = sess_data[sess_ind]['time_bin_size_task_in_ms']
        bin_size_pbe_in_ms = sess_data[sess_ind]['bin_size_pbe_in_ms']

        if sa > 0:
            shifted_bs_replay = [s[sa:] for s in binned_spikes_replay]
            shifted_pos_replay = [p[:-sa] for p in binned_position_replay]

        elif sa < 0:
            shifted_bs_replay = [s[:sa] for s in binned_spikes_replay]
            shifted_pos_replay = [p[-sa:] for p in binned_position_replay]

        elif sa == 0:
            shifted_bs_replay = copy.copy(binned_spikes_replay)
            shifted_pos_replay = copy.copy(binned_position_replay)

        # --- MAKE SURE BINNED SPIKES DON'T HAVE ZERO SHAPE -------------------
        shifted_bs_replay_sel, shifted_pos_replay_sel = [], []

        for bs, bp in zip(shifted_bs_replay, shifted_pos_replay):
            if bs.shape[0] > 0 and bs.shape[1] > 0:
                shifted_bs_replay_sel.append(bs)
                shifted_pos_replay_sel.append(bp)

        shifted_bs_replay = shifted_bs_replay_sel
        shifted_pos_replay = shifted_pos_replay_sel

        # --- BUILD RATEMAPS --------------------------------------------------

        rr = ReplayRatemaps(bootstrap=False,
                            unit_labels=range(len(trains)))

        rr.build_real_ratemaps(binned_spikes_task=binned_spikes_task,
                               binned_position_task=binned_position_task,
                               time_bin_size_task_in_ms=time_bin_size_task_in_ms)

        rr.build_event_ratemaps(binned_spikes_replay=shifted_bs_replay,
                                binned_position_replay=shifted_pos_replay,
                                time_bin_size_replay_in_ms=bin_size_pbe_in_ms)

        rr.correlate_ratemaps(null_distribution_method=null_distribution_method)

        ratemaps[sess_ind] = rr


    real_ratemap = np.vstack([ratemaps[s].ratemap['real'] for s in sessions])
    event_ratemap = np.vstack([ratemaps[s].ratemap['event'] for s in sessions])
    p_vals = np.hstack([ratemaps[s].corr['p_values'] for s in sessions])
    corrcoef_obs = np.hstack([ratemaps[s].corr['corrcoef_obs'] for s in sessions])
    corrcoef_debiased = np.hstack([ratemaps[s].corr['corrcoef_debiased'] for s in sessions])
    percentile_rank = np.hstack([ratemaps[s].corr['percentile_rank'] for s in sessions])
    bins = ratemaps[sessions[0]].bins
    n_neurons = real_ratemap.shape[0]

    shiftratemaps[sa] = {}
    shiftratemaps[sa]['real_ratemap'] = real_ratemap
    shiftratemaps[sa]['event_ratemap'] = event_ratemap
    shiftratemaps[sa]['p_vals'] = p_vals
    shiftratemaps[sa]['corrcoef_obs'] = corrcoef_obs
    shiftratemaps[sa]['corrcoef_debiased'] = corrcoef_debiased
    shiftratemaps[sa]['percentile_rank'] = percentile_rank
    shiftratemaps[sa]['n_neurons'] = n_neurons
    shiftratemaps[sa]['bins'] = bins




for shift_amount in shift_amounts:

    percentile_rank = shiftratemaps[shift_amount]['percentile_rank']
    corrcoef_debiased = shiftratemaps[shift_amount]['corrcoef_debiased']

    # if subselect_units:
    #     percentile_rank = percentile_rank[sel_ind]
    #     corrcoef_debiased = corrcoef_debiased[sel_ind]

    m, p_val_sign = statsmodels.stats.descriptivestats.sign_test(percentile_rank, 50)
    t, p_val_t = scipy.stats.ttest_1samp(percentile_rank, 50)

    shiftratemaps[shift_amount]['percentile_p_val_sign_test'] = p_val_sign
    shiftratemaps[shift_amount]['percentile_p_val_t_test'] = p_val_t

    m, p_val_sign = statsmodels.stats.descriptivestats.sign_test(corrcoef_debiased, 0)
    t, p_val_t = scipy.stats.ttest_1samp(corrcoef_debiased, 0)

    shiftratemaps[shift_amount]['corrcoef_p_val_sign_test'] = p_val_sign
    shiftratemaps[shift_amount]['corrcoef_p_val_t_test'] = p_val_t

percentile_p_val_sign_test = np.array([shiftratemaps[s]['percentile_p_val_sign_test'] for s in shift_amounts])
percentile_p_val_t_test = np.array([shiftratemaps[s]['percentile_p_val_t_test'] for s in shift_amounts])
corrcoef_p_val_sign_test = np.array([shiftratemaps[s]['corrcoef_p_val_sign_test'] for s in shift_amounts])
corrcoef_p_val_t_test = np.array([shiftratemaps[s]['corrcoef_p_val_t_test'] for s in shift_amounts])


shift_amounts_in_ms = shift_amounts * slide_by_pbe_in_ms

pal = sns.color_palette("RdBu", n_colors=shift_amounts.shape[0])
#boxplot_vars = ['percentile_rank', 'corrcoef_debiased']
#boxplot_vars = ['percentile_rank', 'corrcoef_debiased', 'corrcoef_obs']
boxplot_vars = ['corrcoef_debiased']

plot_type = 'lineplot'

for variable in boxplot_vars:

    shifts, vals, units = [], [], []
    for sa, sa_ms in zip(shift_amounts, shift_amounts_in_ms):
        y = shiftratemaps[sa][variable]
        vals.append(y)
        shifts.append(np.repeat(sa_ms, y.shape[0]))
        units.append(np.arange(y.shape[0]))

    df = pd.DataFrame(columns=['shift_amount', variable, 'unit'])
    df['shift_amount'] = np.hstack(shifts)
    df[variable] = np.hstack(vals)
    df['unit'] = np.hstack(units)

    f, ax = plt.subplots(1, 1, figsize=[6, 5])
    ax.set_xticks(shift_amounts_in_ms)

    #ax.set_ylim([-0.25, 0.25])
    if plot_type == 'boxplot':
        sns.boxplot(data=df, x='shift_amount', y=variable, ax=ax, palette=pal)
    elif plot_type == 'lineplot':
        sns.lineplot(data=df, x='shift_amount', y=variable, ax=ax, palette=pal)
    else:
        raise ValueError

    if variable == 'percentile_rank':
        not_signif = percentile_p_val_t_test > 0.01
    elif variable == 'corrcoef_debiased':
        not_signif = corrcoef_p_val_t_test > 0.01

    #ax.set_xticklabels(10*shift_amounts)
    height = ax.get_ylim()[0] + 0.1
    signif_line = np.ma.masked_where(not_signif, np.repeat(height, len(shift_amounts)))
    if plot_type == 'boxplot':
        ax.plot(np.arange(len(shift_amounts)), signif_line, color='k', lw=2, alpha=0.5)
    elif plot_type == 'lineplot':
        ax.plot(shift_amounts_in_ms, signif_line, color='k', lw=2, alpha=0.5)
    else:
        raise ValueError
    ax.set_ylabel(replay_ratemap_labels[variable])
    ax.set_xlabel('Shift amount (ms)')
    ax.tick_params(axis='x', rotation=90)
    plt.tight_layout()
    sns.despine()
