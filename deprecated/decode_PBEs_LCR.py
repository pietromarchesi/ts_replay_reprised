import numpy as np
import pickle
import neo
import pandas as pd
import quantities as pq
import os
from sklearn.metrics import roc_auc_score
from sklearn.ensemble import RandomForestClassifier
from replaydetector.DKW_decoder import DKW_decoder
from utils import get_decoding_data
from utils import coarsify_bins
from utils import get_session_block
from utils import merge_segments
from utils import bin_PBEs
from utils import min_max_scale
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.gridspec as gridspec
from sklearn.metrics import confusion_matrix
from constants import DATA_FOLDER, REPLAY_FOLDER
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_predict
from plotting_utils import plot_replay_panel
from replaydetector.replay_utils import reindex_pbes_dfs
from constants import *

"""
Plot and decode windows of Perirhinal activity and check their correspondence
to trial side. The window that is plotted can be extended beyond the PBE
event to look at the vicinity of the event as well. 

"""


try:
    __IPYTHON__

except NameError:

    import argparse
    import distutils.util
    parser = argparse.ArgumentParser()

    parser.add_argument('--sess_ind', type=int)
    parser.add_argument('--epoch', type=str, choices=['task', 'pre_sleep', 'post_sleep'])
    parser.add_argument('--PBEs_settings_name', type=str)
    parser.add_argument('--PBEs_area', type=str)
    parser.add_argument('--dec_settings_name', type=str)
    parser.add_argument('--dec_area', type=str)
    parser.add_argument('--plot_events',  type=distutils.util.strtobool, default=0)
    parser.add_argument('--data_folder', type=str, default=DATA_FOLDER)
    parser.add_argument('--replay_folder', type=str, default=REPLAY_FOLDER)


    ARGS = parser.parse_args()

    sess_ind           = int(ARGS.sess_ind)
    epoch              = ARGS.epoch
    PBEs_settings_name = ARGS.PBEs_settings_name
    PBEs_area          = ARGS.PBEs_area
    dec_settings_name  = ARGS.dec_settings_name
    dec_area           = ARGS.dec_area
    plot_events        = ARGS.plot_events
    data_folder        = ARGS.data_folder
    replay_folder      = ARGS.replay_folder

else:

    sessions                   = sessions_HC_PR_replay
    epoch                      = 'task'
    PBEs_settings_name         = 'july8'
    PBEs_area                  = 'Hippocampus'
    dec_settings_name          = 'logreg_lcr' #logreg, dkw, randfor
    dec_area                   = 'Perirhinal'
    plot_events                = False
    data_folder                = DATA_FOLDER
    replay_folder              = REPLAY_FOLDER


bin_size_train                 = 300
bin_size_pbe_in_ms             = 20
slide_by_pbe_in_ms             = 10
min_spike_sample_pbe           = 1
min_pbe_bins                   = 3
min_total_spikes_pbe           = 4 # if PR on HP defined PBE, make sure there are PR spikes in it!
remove_pbe_samples_w_no_spikes = True
min_speed_train                = 4
min_spikes_sample_train        = 1
min_spikes_neuron_train        = 1
extra_time_in_ms               = 0

save_plots                     = True
plots_format                   = 'png'


for sess_ind in sessions:
    # --- SET UP PATHS ------------------------------------------------------------

    PBEs_folder   = os.path.join(replay_folder, 'results', 'PBEs',
                                 'pbe_setting_{}_{}'.format(PBEs_settings_name, PBEs_area))
    output_folder = os.path.join(replay_folder, 'results', 'decoded_PBEs',
                                 'pbe_setting_{}_{}'.format(PBEs_settings_name, PBEs_area),
                                 'dec_setting_{}_{}'.format(dec_settings_name, dec_area))
    plots_folder  = os.path.join(replay_folder, 'plots',
                                 'pbe_setting_{}_{}'.format(PBEs_settings_name, PBEs_area),
                                 'dec_setting_{}_{}'.format(dec_settings_name, dec_area),
                                 'session_{}'.format(sess_ind))

    if not os.path.isdir(plots_folder):
        os.makedirs(plots_folder)

    event_plots_folder = os.path.join(plots_folder, 'PBEs_PR')
    if not os.path.isdir(event_plots_folder):
        os.makedirs(event_plots_folder)


    # --- SET UP PARAMETERS DICT --------------------------------------------------

    decode_pars = {'area'     : dec_area,
                   'bin_size_train' : bin_size_train,
                   'bin_size_pbe_in_ms' : bin_size_pbe_in_ms,
                   'slide_by_pbe_in_ms' : slide_by_pbe_in_ms,
                   'min_spike_sample_pbe' : min_spike_sample_pbe,
                   'remove_pbe_samples_w_no_spikes' : remove_pbe_samples_w_no_spikes,
                   'min_pbe_bins' : min_pbe_bins,
                   'min_speed_train' : min_speed_train,
                   'min_spikes_sample_train' : min_spikes_sample_train,
                   'min_spikes_neuron_train' : min_spikes_neuron_train}




    # --- LOAD PBEs ---------------------------------------------------------------
    print('\n- Loading PBEs')
    PBEs_file = os.path.join(PBEs_folder, 'PBEs_sess_{:02}_{}.pkl'.format(sess_ind, epoch))
    pbes = pickle.load(open(PBEs_file, 'rb'))
    #pbes['pbes'] = pbes['pbes'][0:15]
    PBEs_indices = pbes['pbes'].index
    PBEs_times = list(zip(pbes['pbes']['start_time_in_ms'] * pq.ms, pbes['pbes']['end_time_in_ms'] * pq.ms))

    # --- LOAD BLOCK --------------------------------------------------------------
    bl = get_session_block(data_folder, sess_ind, epoch)
    if epoch == 'task':
        segment = merge_segments(bl.segments[:-1], bl.list_units)
        rat_speed_signal = segment.irregularlysampledsignals[1].rescale(pq.cm / pq.s)

    elif epoch == 'pre_sleep':
        segment = bl.segments[0]
        assert segment.name == 'pre-sleep'

    elif epoch == 'post_sleep':
        segment = bl.segments[1]
        assert segment.name == 'post-sleep'

    # --- BIN PBEs ----------------------------------------------------------------
    extra_time = extra_time_in_ms * pq.ms

    print('\n- Preparing PBEs data')
    trains = segment.filter(targdict={'area': dec_area}, objects=neo.SpikeTrain)
    bp = bin_PBEs(trains, PBEs_times,
                  bin_size_in_ms=bin_size_pbe_in_ms,
                  sliding_binning=True,
                  slide_by_in_ms=slide_by_pbe_in_ms,
                  pyramidal_only=False,
                  extra_time_before=extra_time,
                  extra_time_after=extra_time)
    binned_pbes = bp['PBEs_binned']
    in_pbe_mask = bp['time_point_in_pbe_mask']


    # --- PREPARING TASK DATA -----------------------------------------------------

    dec_data = get_decoding_data(sess_ind=sess_ind, data_folder=data_folder,
                                 area=dec_area,
                                 binsize=bin_size_train, remove_nans=True,
                                 min_speed=min_speed_train,
                                 min_spikes_per_sample=min_spikes_sample_train,
                                 min_spikes_per_neuron=min_spikes_neuron_train,
                                 pyramidal_only=True)

    # rescaling data to firing rate in Hertz
    X = (dec_data['X'] / (bin_size_train * pq.ms)).rescale(1/pq.s).__array__()
    y = dec_data['y']
    bins = dec_data['bins']
    neuron_sel = dec_data['neuron_sel']

    y, bins = coarsify_bins(y, mode='leftmidright')
    y = y.astype(int)
    # --- TRAIN DECODER ------------------------------------------------------------

    if dec_settings_name == 'dkw':
        dec = DKW_decoder(bin_size_train=bin_size_train,
                          bin_size_test=bin_size_pbe_in_ms)

    elif dec_settings_name == 'randfor':
        dec = RandomForestClassifier(n_estimators=200, random_state=1)
        #dec = GaussianNB()

    elif dec_settings_name == 'logreg_lcr':
        dec = LogisticRegression(solver='lbfgs', random_state=1)


    dec.fit(X, y)


    # --- REMOVE COLUMNS FROM PBES DATAFRAME --------------------------------------
    # the pbes dataframe contains columns which refer to population activity
    # in the area used to detect the pbes (whereas in this script we decode the
    # activity of a potentially different area). to avoid confusion, we drop
    # those columns

    if dec_area != PBEs_area:
        for col in ['percentage_active', 'n_active', 'n_spikes']:
            try:
                pbes['pbes'] = pbes['pbes'].drop(col, axis=1)
            except ValueError:
                pass

    # --- DECODE PBES -------------------------------------------------------------

    new_cols = ['pbe_prob_sum', 'pbe_prob_mean', 'pbe_n_bins',
                'side_score']

    for col in new_cols:
        pbes['pbes'][col] = None


    for pbe_ind, pbe_number in enumerate(PBEs_indices):

        spikes = binned_pbes[pbe_ind]
        ip     = in_pbe_mask[pbe_ind]

        print('Processing PBE event {}'.format(pbe_number))
        spikes = spikes[:, neuron_sel] # this is important!

        print(spikes.sum(), spikes[ip, :].sum())

        if spikes[ip, :].sum() > min_total_spikes_pbe:

            if dec_settings_name == 'dkw_decoder':
                spikes_decode = spikes

            else:
                if remove_pbe_samples_w_no_spikes:
                    nonzero_ind = spikes.sum(axis=1)>0
                    spikes = spikes[nonzero_ind, :]
                    ip = ip[nonzero_ind]

                spikes_decode = (spikes / (bin_size_pbe_in_ms * pq.ms)).rescale(1/pq.s).__array__()
                #spikes_decode = spikes * (bin_size_train / bin_size_pbe_in_ms)

            pbe_n_bins   = ip.sum()
            y_pred = dec.predict(spikes_decode)

            side_label = pd.value_counts(y_pred).sort_values(ascending=False).index[0].astype(int)

            pbes['pbes'].loc[pbe_number, 'pbe_side_pred'] = side_label

        else:
            print('       - Skipping PBE {}: Not enough spikes - REMOVING'.format(pbe_number))
            pbes['pbes'] = pbes['pbes'].drop(pbe_number, axis=0)

    ppm = pbes['pbes']['pbe_side_pred']

    pbes['pbes']['pbe_side'] = None
    pbes['pbes']['pbe_side'][ppm == 0] = 'left'
    pbes['pbes']['pbe_side'][ppm == 1] = 'mid'
    pbes['pbes']['pbe_side'][ppm == 2] = 'right'

    cols = ['pbe_n_bins']

    for col in cols:
        pbes['pbes'][col] = pd.to_numeric(pbes['pbes'][col])
        pbes['pbes'] = pbes['pbes'].round({col: 3})


    # --- REINDEX DF WITH UNIQUE PBE IDs ------------------------------------------

    pbes['pbes'] = reindex_pbes_dfs(pbes['pbes'])

    # --- SAVE OUTPUT -------------------------------------------------------------

    pbes['decode_pars'] = decode_pars

    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)

    output_file = os.path.join(output_folder,
                           'decoded_PBEs_sess_{:02}_{}_{}.pkl'.format(sess_ind, dec_area, epoch))

    print('Saving output to {}'.format(output_file))

    pickle.dump(pbes, open(output_file, 'wb'))






