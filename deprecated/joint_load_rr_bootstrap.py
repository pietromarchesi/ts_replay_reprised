import os
from constants import *
import pickle
import statsmodels.stats.descriptivestats
from statsmodels.stats.multitest import multipletests
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
from plotting_style import *
from sklearn.preprocessing import MinMaxScaler
from scipy.ndimage import gaussian_filter1d
import matplotlib.patches as mpatches
from utils import *

ratemap_settings = 'may11_DecSet2_smooth2_morelages'
#ratemap_settings = 'feb10_DecSet26_taskphase'
ratemap_areas = ['Perirhinal', 'Barrel']

save_plots = True
plot_format = 'png'
dpi = 400

test_values = [50, 0] # [0, 0]
nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
subselect_sessions = None
subselect_shifts = False
min_shift = -8
max_shift = 8

# SWARMS
p_val_thr = 0.05
bonferroni_correct = False
null_dist_method_for_plot_both_shuff = 'rotate_ratemap'


# RATEMAPS
smooth_ratemaps = True
p_val_thr_rateplots = 0.05
minmax_scale_plots = True
bins = np.arange(-29, 30)
n_rows = 8
n_cols = 2
n_plots = n_rows * n_cols
shift_amount = 0
shift_amount_ratemaps = 0



# --- SET UP PATHS ------------------------------------------------------------
plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# --- LOAD RESULTS ------------------------------------------------------------

dfs = []
for ratemap_area in ratemap_areas:
    res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)
    file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
    res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
    df = res['df']
    pars = res['pars']
    shift_amounts = pars['shift_amounts']
    smoothing_sigma = pars['smooth_sigma']


    if subselect_sessions is not None:
        df = df[np.isin(df['sess_ind'], subselect_sessions)]

    if subselect_shifts:
        df = df[df['shift_amount'] >= min_shift]
        df = df[df['shift_amount'] <= max_shift]
        shift_amounts = np.arange(min_shift, max_shift+1)

    df['area'] = ratemap_area
    dfs.append(df)

df = pd.concat(dfs)
df['unit_id'] = ['{}_{}_{}'.format(r['sess_ind'], r['unit'], r['area']) for i, r in df.iterrows()]
    #shift_amounts = np.array([-8, -6, -4, -2, 0, 2, 4, 6, 8])









# --- COMPARE N OF SIG UNITS PER TIME WITH BOOTSTRAP ---------------------------
#here we look at every time point how many units are significant

n_bootstraps = 50

all_unit_ids = df['unit_id'].unique()

for direction in ['all']:

    dg = pd.DataFrame(columns=['shift', 'area', 'n_sig', 'perc_sig', 'mean_corr', 'total_corr'])

    for ratemap_area in ratemap_areas:

        for shifts in np.arange(-10, 11):

            for n_boot in range(n_bootstraps):
                print('boot: {} of {}'.format(n_boot, n_bootstraps))

                sampled_ids = np.random.choice(all_unit_ids,
                                               size=all_unit_ids.shape[0],
                                               replace=True)

                # sampled_ids = np.random.choice(all_unit_ids,
                #                                size=int(all_unit_ids.shape[0] / 2),
                #                                replace=False)

                sig_ids = []
                for null_distribution_method in nulldistmet_plots :
                    dfx = df[(df['phase'] == 'all') &
                             (df['direction'] == direction) &
                             (df['has_ripple'] == 'all') &
                             (df['null_method'] == null_distribution_method) &
                             (df['shift_amount'] == shifts) &
                             (df['area'] == ratemap_area) &
                             (np.isin(df['unit_id'], sampled_ids))]

                    if bonferroni_correct:
                        p = p_val_thr / dfx.shape[0]
                    else:
                        p = p_val_thr
                    #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
                    unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values



                    sig_ids.append(unit_ids)
                    n_total = dfx.shape[0]

                sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

                # this way it's gonna be using the se
                dfx_sig = dfx[np.isin(dfx['unit_id'], sig_both)]
                mean_corr = dfx_sig['corrcoef_debiased'].mean()
                total_corr = dfx_sig['corrcoef_debiased'].sum()
                n_sig = len(sig_both)
                perc_sig = 100*n_sig/n_total
                dg.loc[dg.shape[0], :] = [shifts, ratemap_area, n_sig, perc_sig,
                                          mean_corr, total_corr]


    for col in ['n_sig', 'perc_sig', 'mean_corr', 'total_corr']:
        dg[col] = pd.to_numeric(dg[col])

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
    sns.lineplot(data=dg, x='shift', y='perc_sig',
                 hue='area', palette=area_palette)
    ax.set_ylabel('% of coordinated units')
    ax.set_xlabel('Time shift [ms]')
    ax.set_xticks([-10, -5, 0, 5, 10])
    ax.set_xticklabels([-100, -50, 0, 50, 100])
    sns.despine()
    leg = ax.get_legend()
    ax.get_legend().remove()
    plt.tight_layout()

    if save_plots :
        plot_name = 'n_of_sig_units_per_shift_boot_{}_{}.{}'.format(direction, null_dist_method_for_plot_both_shuff, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
    sns.lineplot(data=dg, x='shift', y='mean_corr',
                 hue='area', palette=area_palette)
    ax.set_ylabel('Total coordination')
    ax.set_xlabel('Time shift [ms]')
    ax.set_xticks([-10, -5, 0, 5, 10])
    ax.set_xticklabels([-100, -50, 0, 50, 100])
    sns.despine()
    leg = ax.get_legend()
    ax.get_legend().remove()
    plt.tight_layout()


    if save_plots :
        plot_name = 'total_coordination_per_shift_boot_{}_{}.{}'.format(direction, null_dist_method_for_plot_both_shuff, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




