import matplotlib.pyplot as plt
from constants import *
from utils import *
import quantities as pq
from plotting_style import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from replaydetector.replayratemaps import ReplayRatemaps
from scipy.ndimage import gaussian_filter1d
import statsmodels.stats.descriptivestats
from session_selection import ratemap_sessions

"""
Generates replay and event ratemaps for all sessions by looping over the 
sessions and then aggregating the results. 

You can pick a shift_amount if you want the ratemaps to be computed and plotted
with a certain shift in time (between replay spikes and replay location).

Real ratemaps are compute on moving data with a settable threshold
called speed_thr_real_rm (in cm/s).

Ratemaps can be smoothed (in which case the correlation is also computed
with the smoothed version).
"""

ratemap_settings         = 'may8_DecSet2'

data_version             = 'dec16'
ratemap_area             = 'Barrel'
PBEs_area                = 'Hippocampus'
dec_area                 = 'Hippocampus'
PBEs_settings_name       = 'dec19k20'
PBEs_epoch               = 'task'
dec_settings_name        = 'DecSet2'

correlation_method       = 'pearson'

# FILTER PBES PARS
shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle',
                 'unit_identity_shuffle']
shuffle_p_vals = [0.05, 0.05, 0.05]


speed_thr_real_rm        = 3
shift_amount             = 0
time_bin_size_task_in_ms = 200
min_speed                = 5
smooth_ratemaps          = True
smoothing_sigma          = 2
task_phase               = None
replay_direction         = None #'positive'

# PLOTTING PARAMETERS
p_val_thr_rateplots = 0.05
minmax_scale_plots = True
plot_format = 'png'
dpi = 400
save_plots = True

plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

sessions = ratemap_sessions[ratemap_area]

#TODO remove this
sessions = sessions[np.isin(sessions, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 27,
                                       28, 29, 30])]
# --- COMPUTE -----------------------------------------------------------------

ratemaps = {}

for sess_ind in sessions:
    # --- LOAD TASK DATA ------------------------------------------------------
    # Needed to build the ratemaps of the area during normal locomotion

    session_data = unpack_session(sess_ind, data_folder=DATA_FOLDER,
                                  area=ratemap_area, binsize_in_ms=time_bin_size_task_in_ms,
                                  data_version=data_version)

    speed_real = session_data['speed'].flatten()
    binned_spikes_task = session_data['spikes']
    binned_position_task = session_data['bin']

    binned_spikes_task = binned_spikes_task[speed_real >= speed_thr_real_rm, :]
    binned_position_task = binned_position_task[speed_real >= speed_thr_real_rm]

    # --- LOAD REPLAY DATA ----------------------------------------------------
    # Extract the spiketrains which will be binned according to the times of the
    # decoded PBEs. If the PBEs are computed during the task, we simply get the
    # trains from session_data, otherwise we load a sleep block.

    if PBEs_epoch == 'task':
        trains = session_data['trains']

    else:
        bl = get_session_block(DATA_FOLDER, sess_ind, PBEs_epoch)
        segment = bl.segments[0]
        trains = segment.spiketrains
        assert segment.name == PBEs_epoch.replace('-', '_')

    # --- LOAD PBEs -----------------------------------------------------------

    pbes_data = load_decoded_PBEs(sess_ind=sess_ind,
                                   PBEs_settings_name=PBEs_settings_name,
                                   PBEs_area=PBEs_area,
                                   data_version=data_version,
                                   dec_settings_name=dec_settings_name,
                                   dec_area=dec_area)

    pbes_df = pbes_data['pbes']
    sig_df = pbes_data['sig_df']

    bin_size_pbe_in_ms = pbes_data['decode_pars']['bin_size_pbe_in_ms']
    slide_by_pbe_in_ms = pbes_data['decode_pars']['slide_by_pbe_in_ms']

    # --- FILTER PBEs ---------------------------------------------------------

    sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types, p_vals=shuffle_p_vals)
    pbes_df = pbes_df[np.isin(pbes_df.index, sel_pbes_sig)]

    if task_phase is not None:
        pbes_df = pbes_df[pbes_df['phase'] == task_phase]

    if replay_direction != 'all' and replay_direction is not None:
        pbes_df = pbes_df[pbes_df['replay_direction'] == replay_direction]

    # --- BIN PBEs ------------------------------------------------------------
    if pbes_df.shape[0] > 0:

        PBEs_times = [(row['start_time_in_ms'] * pq.ms, row['end_time_in_ms'] * pq.ms)
                       for i, row in pbes_df.iterrows()]

        binned_position_replay = pbes_df['trajectory'].tolist()
        binned_pbes = bin_PBEs(trains, PBEs_times,
                               bin_size_in_ms=bin_size_pbe_in_ms,
                               sliding_binning=True,
                               slide_by_in_ms=slide_by_pbe_in_ms,
                               pyramidal_only=False)

        binned_spikes_replay = binned_pbes['PBEs_binned']

        # --- POTENTIALLY SHIFT ---------------------------------------------------

        if shift_amount > 0:
            binned_spikes_replay = [s[shift_amount:] for s in binned_spikes_replay]
            binned_position_replay = [p[:-shift_amount] for p in binned_position_replay]

        elif shift_amount < 0:
            binned_spikes_replay = [s[:shift_amount] for s in binned_spikes_replay]
            binned_position_replay = [p[-shift_amount:] for p in binned_position_replay]

        elif shift_amount == 0:
            pass


        # --- BUILD RATEMAPS ------------------------------------------------------


        rr = ReplayRatemaps(unit_labels=np.arange(len(trains)))

        rr.build_real_ratemaps(binned_spikes_task=binned_spikes_task,
                               binned_position_task=binned_position_task,
                               time_bin_size_task_in_ms=time_bin_size_task_in_ms)

        rr.build_event_ratemaps(binned_spikes_replay=binned_spikes_replay,
                                binned_position_replay=binned_position_replay,
                                time_bin_size_replay_in_ms=bin_size_pbe_in_ms)

        corr_out = rr.correlate_ratemaps(smooth=smooth_ratemaps,
                                         sigma=smoothing_sigma,
                                         correlation_method=correlation_method)

        ratemaps[sess_ind] = rr

sessions = list(ratemaps.keys())
real_ratemap = np.vstack([ratemaps[s].ratemap['real'] for s in sessions])
event_ratemap = np.vstack([ratemaps[s].ratemap['event'] for s in sessions])
p_vals = np.hstack([ratemaps[s].corr['p_values'] for s in sessions])
corrcoef_debiased = np.hstack([ratemaps[s].corr['corrcoef_debiased'] for s in sessions])
percentile_rank = np.hstack([ratemaps[s].corr['percentile_rank'] for s in sessions])
bins = ratemaps[sessions[0]].bins
n_neurons = real_ratemap.shape[0]
ind_plot = np.where(p_vals < p_val_thr_rateplots)[0]

# f, ax = plt.subplots(1, 2)
# sns.swarmplot(corrcoef_debiased, ax=ax[0])
# sns.swarmplot(percentile_rank, ax=ax[1])

m, p_val_sign = statsmodels.stats.descriptivestats.sign_test(percentile_rank, 50)
print('Sign test on percentile rank {:.3f}'.format(p_val_sign))
t, p_val_t = scipy.stats.ttest_1samp(percentile_rank, 50)
print('t-test on percentile rank {:.3f}'.format(p_val_t))

m, p_val_sign = statsmodels.stats.descriptivestats.sign_test(corrcoef_debiased, 0)
print('Sign test on corrleation coefficient {:.3f}'.format(p_val_sign))
t, p_val_t = scipy.stats.ttest_1samp(corrcoef_debiased, 0)
print('t-test on corrleation coefficient {:.3f}'.format(p_val_t))

perc = 100*(len(ind_plot) / n_neurons)
print('# of neurons with p-value below {}: {} out of {} ({:.2f}%)'.format(p_val_thr_rateplots,
                                                       len(ind_plot), n_neurons,
                                                        perc))


n_plots = len(ind_plot)
n_rows = int(np.ceil(n_plots / 4))
n_cols = 4

palette = real_event_palette[ratemap_area]
f, axes = plt.subplots(n_rows, n_cols, figsize=[n_cols * 3, n_rows * 1.5],
                       sharex=True)

for kk, unit in enumerate(ind_plot):

    try:
        ax = axes.flatten()[kk]
    except TypeError:
        ax = axes

    ax.set_yticks([])

    for i, rm in enumerate([real_ratemap, event_ratemap]):
        curve = rm[unit, :]

        if smooth_ratemaps:
            curve  = gaussian_filter1d(curve, smoothing_sigma)

        if minmax_scale_plots:
            mm = MinMaxScaler()
            curve = mm.fit_transform(curve.reshape(-1, 1))[:, 0]

        line = ax.plot(bins, curve, color=palette[i])
        fillcolor = line[0].get_color()
        ax.fill_between(bins, curve, alpha=0.3,
                        color=fillcolor,
                        zorder=int(10 + 2 * n_neurons - 2 * unit - 1))
        ax.set_title('c = {:.03}, p = {:.03}'.format(corrcoef_debiased[unit],
                                                     p_vals[unit]))

for kk in range(n_plots, n_rows * n_cols):
    print(kk)
    axes.flatten()[kk].axis('off')

sns.despine()
plt.tight_layout()
#plt.rcParams['savefig.dpi'] = dpi

plot_name = 'sig_replay_ratemaps_{}_{}.{}'.format(ratemap_area, ratemap_settings, plot_format)
plt.draw()
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- PLOT SELECTIVELY --------------------------------------------------------
selected_inds = ind_plot[np.array([0, 1, 3, 12, 27, 29])]
f, axes = plt.subplots(len(selected_inds), 1, figsize=[4, len(selected_inds)*1],
                       sharex=True)

for kk, unit in enumerate(selected_inds):

    ax = axes[kk]
    ax.set_yticks([])

    for i, rm in enumerate([real_ratemap, event_ratemap]):
        curve = rm[unit, :]
        if smooth_ratemaps:
            curve = gaussian_filter1d(curve, smoothing_sigma)

        if minmax_scale_plots:
            mm = MinMaxScaler()
            curve = mm.fit_transform(curve.reshape(-1, 1))[:, 0]

        line = ax.plot(bins, curve, color=palette[i])
        fillcolor = line[0].get_color()
        ax.fill_between(bins, curve, alpha=0.3,
                        color=fillcolor,
                        zorder=int(10 + 2 * n_neurons - 2 * unit - 1))
        ax.set_title('c = {:.02}, p = {:.02}'.format(corrcoef_debiased[unit],
                                                     p_vals[unit]))

axes[-1].set_xlabel('Spatial bin')
sns.despine(left=True)
plt.tight_layout()

plot_name = 'sig_replay_ratemaps_selected_{}_{}.{}'.format(ratemap_area, ratemap_settings, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


