

## TODO: chop return to central arm
The animal spends time sitting in the central arm both at the beginning
of the trial (trial start coincides with ITI) and at the end (after the
return to the central arm, there is still some time before the new ITI
starts). This means that PBEs that we see in the central arm could be
before the animal starts the current trial or after it has finished it:
this creates a confusion when comparing decoded PBEs with current/previous
trial side. Solution: when merging segments to define PBEs, chop trials
at return to central arm. This is implemented in `PBEs_make.py`, if
`chop_return_central_arm` is set to True, all PBEs in the central arm are
before the animal runs the current trial.
__DONE__: now we save the trial phase of every event, so there's no
need to chop anymore.

# IDEA: SPLIT ITI and POST-IMAGE ON EVENTS
The ITI events will have no information about the upcoming (current) trial,
whereas after image on they will!
__DONE__: in PBEs_make you also get for each event the phase in which it occurred.
This is something that will have to be added on top on the PBEsDetector class.

# TODO: perirhinal discard units which are not spatially selective,
maybe you get more significant coordination between real and event ratemaps
__DONE__: currently, three single-cell attributes to subselect cells for the
real/event ratemap correlation, namely AUC score, spatial selectivity,
and PBE up-down modulation

# Can I find forward/reverse replay by looking at the fitted line?
Because normally to do forward/reverse you would make a ratemap for each
run direction and then look at whether the one that correlates the most
in absolute value has a positive or negative correlation.
__DONE__: not really, in our experiment the animal runs almost exclusively
in one direction, so we probably cannot build two separate ratemaps (
the run in the opposite direction will probably not have enough data).

# Compute a replay score for the hippocampus
__DONE__: in the sig_df of the replay detector you get the z-score for
every shuffle type (you could average it to get one single z-score)