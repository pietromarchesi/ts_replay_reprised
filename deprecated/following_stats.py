import os
from constants import *
import pickle
import statsmodels.stats.descriptivestats
from statsmodels.stats.multitest import multipletests
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
from plotting_style import *
from sklearn.preprocessing import MinMaxScaler
from scipy.ndimage import gaussian_filter1d
import matplotlib.patches as mpatches
from utils import *

ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'jul4_DecSet2_smooth2_morelages'
#ratemap_settings = 'feb10_DecSet26_taskphase'
ratemap_areas = ['Perirhinal', 'Barrel']

save_plots = True
plot_format = 'png'
dpi = 400

#variables = ['percentile_rank']  # ['corrcoef_obs', 'corrcoef_debiased']
test_values = [50, 0] # [0, 0]
nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
subselect_sessions = None
subselect_shifts = False
min_shift = -8
max_shift = 8

ntimes = [5, 9]


# --- SET UP PATHS ------------------------------------------------------------
plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# --- LOAD RESULTS ------------------------------------------------------------

dfs = []
for ratemap_area in ratemap_areas:
    res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)
    file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
    res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
    df = res['df']
    pars = res['pars']
    shift_amounts = pars['shift_amounts']
    smoothing_sigma = pars['smooth_sigma']


    if subselect_sessions is not None:
        df = df[np.isin(df['sess_ind'], subselect_sessions)]

    if subselect_shifts:
        df = df[df['shift_amount'] >= min_shift]
        df = df[df['shift_amount'] <= max_shift]
        shift_amounts = np.arange(min_shift, max_shift+1)

    df['area'] = ratemap_area
    dfs.append(df)

df = pd.concat(dfs)
df['unit_id'] = ['{}_{}_{}'.format(r['sess_ind'], r['unit'], r['area']) for i, r in df.iterrows()]
    #shift_amounts = np.array([-8, -6, -4, -2, 0, 2, 4, 6, 8])





# --- PEAK --------------------------------------------------------------------


print('Computing the peak of the percentile rank time shift plot for Perirhinal\n')
variable = 'percentile_rank'

for estimator in ['mean']:
    for direction in ['all', 'positive']:
        print('\nDirection: {}'.format(direction))
        for null_distribution_method in nulldistmet_plots:
            dfx = df[(df['area'] == 'Perirhinal') &
                     (df['phase'] == 'all') &
                     (df['direction'] == direction) &
                     (df['has_ripple'] == 'all') &
                     (df['null_method'] == null_distribution_method)]

            dfx = dfx.drop(['ratemap_real', 'ratemap_event',
                            'phase', 'direction', 'null_method',
                            'has_ripple'], axis=1)

            if estimator == 'median':
                dfg = dfx.groupby('shift_amount').median().reset_index()
                best_shift = dfg.iloc[dfg[variable].idxmax()]['shift_amount']
                rank = dfx[dfx['shift_amount'] == best_shift][variable]
                rank_mean = rank.median()
                rank_q1 = rank.quantile(q=0.25)
                rank_q2 = rank.quantile(q=0.75)

                print('\nFor {} distribution {}\nHighest average percentile rank found at shfit = {}'
                      '\nwith median rank {}, {} - {}'.format(variable,null_distribution_method,
                                                       best_shift, rank_mean,
                                                       rank_q1, rank_q2))

            elif estimator == 'mean':
                dfg = dfx.groupby('shift_amount').mean().reset_index()
                best_shift = dfg.iloc[dfg[variable].idxmax()]['shift_amount']
                rank = dfx[dfx['shift_amount'] == best_shift][variable]
                rank_mean = rank.mean()
                rank_std = rank.std()


                print('\nFor {} distribution {}\nHighest average percentile rank found at shfit = {}'
                      '\nwith mean rank {} +- {}'.format(variable, null_distribution_method,
                                                         best_shift, rank_mean, rank_std))






# ------------------------------------------------------------------------------

print('\n\nnComparing the average percentile rank per unit in the xx ms before'
      'and after 0 for Perirhinal')

variable = 'percentile_rank'

for direction in ['all'] :
    print('\nDirection: {}'.format(direction))
    for nt in ntimes :
        print('\n\n-> {} ms before and after'.format(nt*10))
        for null_distribution_method in nulldistmet_plots:
            times_before = np.arange(-nt, 0)
            times_after = np.arange(1, nt+1)
            dfx = df[(df['area'] == 'Perirhinal') &
                     (df['phase'] == 'all') &
                     (df['direction'] == direction) &
                     (df['has_ripple'] == 'all') &
                     (df['null_method'] == null_distribution_method)]

            dfx = dfx.drop(['ratemap_real', 'ratemap_event', 'phase',
                            'direction', 'null_method','has_ripple'], axis=1)

            df1 = dfx[np.isin(dfx['shift_amount'], times_before)].groupby('unit_id').mean().reset_index()
            df2 = dfx[np.isin(dfx['shift_amount'], times_after)].groupby('unit_id').mean().reset_index()
            np.testing.assert_array_equal(df1['unit_id'], df2['unit_id'])

            print('\ntesting {} with {}'.format(variable, null_distribution_method))
            v1 = (df1[variable].quantile(q=0.5),
                       df1[variable].quantile(q=0.25),
                       df1[variable].quantile(q=0.75))

            v2 = (df2[variable].quantile(q=0.5),
                       df2[variable].quantile(q=0.25),
                       df2[variable].quantile(q=0.75))

            print('Before: {:.2f}, {:.2f}-{:.2f}'
                  '\nAfter: {:.2f}, {:.2f}-{:.2f}'.format(v1[0], v1[1], v1[2], v2[0], v2[1], v2[2]))

            t, p = scipy.stats.wilcoxon(df1[variable], df2[variable])
            print('p = {:.3f}, significant: ----> {}'.format(p, p<0.05/2))



# ------------------------------------------------------------------------------


print('Testing the number of units which reach their max percenile rank '
      'before or after for Perirhinal\n')

variable = 'percentile_rank'

for direction in ['all', 'positive'] :
    print('\nDirection: {}'.format(direction))
    for nt in ntimes :
        print('\n\n-> {} ms before and after'.format(nt * 10))
        for null_distribution_method in nulldistmet_plots :
            times_before = np.arange(-nt, 0)
            times_after = np.arange(1, nt+1)
            dfx = df[(df['area'] == 'Perirhinal') &
                     (df['phase'] == 'all') &
                     (df['direction'] == direction) &
                     (df['has_ripple'] == 'all') &
                     (df['null_method'] == null_distribution_method)]

            dfx = dfx.drop(['ratemap_real', 'ratemap_event', 'phase',
                            'direction', 'null_method', 'has_ripple'], axis=1)

            df1 = dfx[np.isin(dfx['shift_amount'], times_before)].groupby('unit_id').max().reset_index()
            df2 = dfx[np.isin(dfx['shift_amount'], times_after)].groupby('unit_id').max().reset_index()

            n_higher = (df2[variable] >= df1[variable]).sum()
            tot = df1.shape[0]
            binom_p = scipy.stats.binom_test(n_higher, n=tot, p=0.5)

            print('{} - {} units out of {} ({}%), p={:.2f} for which the debiased correlation is higher after'
                  ' than before 0'.format(null_distribution_method, n_higher,
                                          tot, np.round(100*n_higher/tot, 2), binom_p))




# ------------------------------------------------------------------------------


variable = 'percentile_rank'

for direction in ['all', 'positive'] :
    print('\nDirection: {}'.format(direction))
    for nt in ntimes :
        print('\n\n-> {} ms before and after'.format(nt * 10))
        for null_distribution_method in nulldistmet_plots :
            times_before = np.arange(-nt, 0)
            times_after = np.arange(1, nt + 1)

            dfx = df[(df['phase'] == 'all') &
                     (df['direction'] == direction) &
                     (df['has_ripple'] == 'all') &
                     (df['null_method'] == null_distribution_method)]
            dfx = dfx.drop(['ratemap_real', 'ratemap_event', 'phase',
                            'direction', 'null_method', 'has_ripple'], axis=1)

            # PLOT
            df1 = dfx[np.isin(dfx['shift_amount'], times_before)].groupby(['unit_id', 'area']).mean().reset_index()
            df2 = dfx[np.isin(dfx['shift_amount'], times_after)].groupby(['unit_id', 'area']).mean().reset_index()

            is_sig = []
            for area in ratemap_areas:
                t, p = scipy.stats.wilcoxon(df1[df1['area'] == area][variable],
                                            df2[df2['area'] == area][variable])
                print('{} - {} :p = {:.3f}, significant: ----> '
                      '{}'.format(null_distribution_method, area, p, p<0.05/2))
                is_sig.append(p<0.05/2)

            df1['when'] = 'before'
            df2['when'] = 'after'
            dfsx = pd.concat([df1, df2])

            f, ax = plt.subplots(1, 1, figsize=[2, 3])
            sns.barplot(data=dfsx, x='when', y=variable, palette=area_palette,
                        #edgecolor=barplot_color, facecolor='white',
                        errcolor='0.2', linewidth=3.5, hue='area',
                        order=['before', 'after'])
            # sns.boxplot(data=dfsx, x='when', y=variable,
            #             color=barplot_color)
            ax.set_xticklabels(['{} ms\nbefore'.format(nt*10), '{} ms\nafter'.format(nt*10)])
            ax.set_ylabel('Mean unit coordination')
            ax.set_xlabel('')
            ax.legend().remove()
            sns.despine()
            plt.tight_layout()

            if save_plots :
                plot_name = '{}_before_after_{}_ms_direction_{}_{}_sigp_{}_sigb_{}.{}'.format(variable, nt*10,
                            direction, null_distribution_method, is_sig[0], is_sig[1], plot_format)
                f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

            plt.close()


# --- HISTOGRAMS ---------------------------------------------------------------

bins = np.arange(pars['shift_amounts'][0]*10, pars['shift_amounts'][-1]*10+1, 20)

for variable in ['corrcoef_debiased']:
    for phase in ['all'] :
        for direction in ['all', 'positive']:
            for null_distribution_method in nulldistmet_plots:

                dfx = df[(df['phase'] == phase) &
                         (df['direction'] == direction) &
                         (df['has_ripple'] == 'all') &
                         (df['null_method'] == null_distribution_method)]

                # dfg = dfx.groupby(['unit_id'])[variable].max()
                # sel_ids = dfg[dfg > 0].index
                # dfx = dfx[np.isin(dfx['unit_id'], sel_ids)]

                #dfx = dfx[(dfx['shift_amount'] >= -10) & (dfx['shift_amount'] < 10)]
                shift_amounts = np.unique(dfx['shift_amount'])
                dfx = dfx.reset_index().drop('index', axis=1)
                # dfsel = dfx.sort_values(variable, ascending=False).drop_duplicates(['sess_ind', 'unit'])
                # dfsel['shift_amount_ms'] = dfsel['shift_amount'] * 10
                # peak_shifts = dfsel['shift_amount_ms']
                #

                peak_shifts = {'Perirhinal' : [], 'Barrel' : []}
                #peak_shifts = {'Perirhinal' : []}
                for area in ratemap_areas:
                    for unit_id in dfx[dfx['area'] == area]['unit_id'].unique() :
                        y = dfx[dfx['unit_id'] == unit_id].sort_values(by='shift_amount')[variable].values
                        peak_shifts[area].append(shift_amounts[y.argmax()]*10)
                    peak_shifts[area] = np.array(peak_shifts[area])
                    # peak_shifts[area] = peak_shifts[area][(peak_shifts[area] >= -90)
                    #                                     & (peak_shifts[area] < 90)]

                    ps = np.array(peak_shifts[area])
                    x1, x2 = ps[ps > 0].shape[0], ps[ps < 0].shape[0]
                    #print(x1, x2)
                    p = scipy.stats.binom_test(x=(x1, x2), p=0.5)
                    print('{} vs {} - {} - {} - {} - {} - p={:.3f} - {}'.format(x1, x2,
                                                     phase, direction,
                                                     null_distribution_method,
                                                     area, p, p<=0.05))

                f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

                for area in ratemap_areas:
                    sns.distplot(peak_shifts[area], hist=True, bins=bins,
                                 kde=False, norm_hist=True,
                                 hist_kws={'ec':'black', 'rwidth':0.8,
                                           'facecolor':area_palette[area]},
                                 label=ratemap_area_sigla[area])

                for patch in ax.patches :
                    patch.set_edgecolor('white')

                ax.set_xlabel('Time of peak coordination\nof individual units (ms)')
                ax.set_ylabel('Density')
                ax.set_xticks([-100, -50, 0, 50, 100])
                ax.set_xticklabels([-100, -50, 0, 50, 100])
                #ax.set_xlim([shift_amounts[0]*10+10, shift_amounts[-1]*10-10])
                ax.legend()
                leg = ax.get_legend()
                ax.get_legend().remove()
                sns.despine()
                plt.tight_layout()

                if save_plots :
                    plot_name = 'sel_distribution_of_peak_times_of_highest_{}_both_areas_{}_{}_{}_{}.' \
                                '{}'.format(variable, ratemap_area,
                                            null_distribution_method, direction, phase, plot_format)
                    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


                f, ax = plt.subplots(1, 1, figsize=[3, 1])
                ax.legend(handles=leg.legendHandles, frameon=False, title='')
                ax.axis('off')

                if save_plots :
                    plot_name = 'distribution_of_peak_times_legend.{}'.format(plot_format)
                    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




