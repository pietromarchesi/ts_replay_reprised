import numpy as np
import pickle
import neo
import pandas as pd
import quantities as pq
import os
from sklearn.metrics import roc_auc_score
from sklearn.ensemble import RandomForestClassifier
from replaydetector.DKW_decoder import DKW_decoder
from utils import get_decoding_data
from utils import coarsify_bins
from utils import get_session_block
from utils import merge_segments
from utils import bin_PBEs
from utils import min_max_scale
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.gridspec as gridspec
from sklearn.metrics import confusion_matrix
from constants import DATA_FOLDER, REPLAY_FOLDER
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_predict
from plotting_utils import plot_replay_panel
from replaydetector.replay_utils import reindex_pbes_dfs
from constants import *

"""
Plot and decode windows of Perirhinal activity and check their correspondence
to trial side. The window that is plotted can be extended beyond the PBE
event to look at the vicinity of the event as well. 

"""


try:
    __IPYTHON__

except NameError:

    import argparse
    import distutils.util
    parser = argparse.ArgumentParser()

    parser.add_argument('--sess_ind', type=int)
    parser.add_argument('--epoch', type=str, choices=['task', 'pre_sleep', 'post_sleep'])
    parser.add_argument('--PBEs_settings_name', type=str)
    parser.add_argument('--PBEs_area', type=str)
    parser.add_argument('--dec_settings_name', type=str)
    parser.add_argument('--dec_area', type=str)
    parser.add_argument('--plot_events',  type=distutils.util.strtobool, default=0)
    parser.add_argument('--data_folder', type=str, default=DATA_FOLDER)
    parser.add_argument('--replay_folder', type=str, default=REPLAY_FOLDER)


    ARGS = parser.parse_args()

    sess_ind           = int(ARGS.sess_ind)
    epoch              = ARGS.epoch
    PBEs_settings_name = ARGS.PBEs_settings_name
    PBEs_area          = ARGS.PBEs_area
    dec_settings_name  = ARGS.dec_settings_name
    dec_area           = ARGS.dec_area
    plot_events        = ARGS.plot_events
    data_folder        = ARGS.data_folder
    replay_folder      = ARGS.replay_folder

else:

    sessions                   = sessions_HC_PR_replay
    epoch                      = 'task'
    PBEs_settings_name         = 'july8'
    PBEs_area                  = 'Hippocampus'
    dec_settings_name          = 'logreg' #logreg, dkw, randfor
    dec_area                   = 'Perirhinal'
    plot_events                = False
    data_folder                = DATA_FOLDER
    replay_folder              = REPLAY_FOLDER


bin_size_train                 = 300
bin_size_pbe_in_ms             = 20
slide_by_pbe_in_ms             = 10
min_spike_sample_pbe           = 1
min_pbe_bins                   = 3
min_total_spikes_pbe           = 4 # if PR on HP defined PBE, make sure there are PR spikes in it!
remove_pbe_samples_w_no_spikes = True
min_speed_train                = 4
min_spikes_sample_train        = 1
min_spikes_neuron_train        = 1
extra_time_in_ms               = 0

save_plots                     = True
plots_format                   = 'png'


for sess_ind in sessions:
    # --- SET UP PATHS ------------------------------------------------------------

    PBEs_folder   = os.path.join(replay_folder, 'results', 'PBEs',
                                 'pbe_setting_{}_{}'.format(PBEs_settings_name, PBEs_area))
    output_folder = os.path.join(replay_folder, 'results', 'decoded_PBEs',
                                 'pbe_setting_{}_{}'.format(PBEs_settings_name, PBEs_area),
                                 'dec_setting_{}_{}'.format(dec_settings_name, dec_area))
    plots_folder  = os.path.join(replay_folder, 'plots',
                                 'pbe_setting_{}_{}'.format(PBEs_settings_name, PBEs_area),
                                 'dec_setting_{}_{}'.format(dec_settings_name, dec_area),
                                 'session_{}'.format(sess_ind))

    if not os.path.isdir(plots_folder):
        os.makedirs(plots_folder)

    event_plots_folder = os.path.join(plots_folder, 'PBEs_PR')
    if not os.path.isdir(event_plots_folder):
        os.makedirs(event_plots_folder)


    # --- SET UP PARAMETERS DICT --------------------------------------------------

    decode_pars = {'area'     : dec_area,
                   'bin_size_train' : bin_size_train,
                   'bin_size_pbe_in_ms' : bin_size_pbe_in_ms,
                   'slide_by_pbe_in_ms' : slide_by_pbe_in_ms,
                   'min_spike_sample_pbe' : min_spike_sample_pbe,
                   'remove_pbe_samples_w_no_spikes' : remove_pbe_samples_w_no_spikes,
                   'min_pbe_bins' : min_pbe_bins,
                   'min_speed_train' : min_speed_train,
                   'min_spikes_sample_train' : min_spikes_sample_train,
                   'min_spikes_neuron_train' : min_spikes_neuron_train}




    # --- LOAD PBEs ---------------------------------------------------------------
    print('\n- Loading PBEs')
    PBEs_file = os.path.join(PBEs_folder, 'PBEs_sess_{:02}_{}.pkl'.format(sess_ind, epoch))
    pbes = pickle.load(open(PBEs_file, 'rb'))
    #pbes['pbes'] = pbes['pbes'][0:15]
    PBEs_indices = pbes['pbes'].index
    PBEs_times = list(zip(pbes['pbes']['start_time_in_ms'] * pq.ms, pbes['pbes']['end_time_in_ms'] * pq.ms))

    # --- LOAD BLOCK --------------------------------------------------------------
    bl = get_session_block(data_folder, sess_ind, epoch)
    if epoch == 'task':
        segment = merge_segments(bl.segments[:-1], bl.list_units)
        rat_speed_signal = segment.irregularlysampledsignals[1].rescale(pq.cm / pq.s)

    elif epoch == 'pre_sleep':
        segment = bl.segments[0]
        assert segment.name == 'pre-sleep'

    elif epoch == 'post_sleep':
        segment = bl.segments[1]
        assert segment.name == 'post-sleep'

    # --- BIN PBEs ----------------------------------------------------------------
    extra_time = extra_time_in_ms * pq.ms

    print('\n- Preparing PBEs data')
    trains = segment.filter(targdict={'area': dec_area}, objects=neo.SpikeTrain)
    bp = bin_PBEs(trains, PBEs_times,
                  bin_size_in_ms=bin_size_pbe_in_ms,
                  sliding_binning=True,
                  slide_by_in_ms=slide_by_pbe_in_ms,
                  pyramidal_only=False,
                  extra_time_before=extra_time,
                  extra_time_after=extra_time)
    binned_pbes = bp['PBEs_binned']
    in_pbe_mask = bp['time_point_in_pbe_mask']


    # --- PREPARING TASK DATA -----------------------------------------------------

    dec_data = get_decoding_data(sess_ind=sess_ind, data_folder=data_folder,
                                 area=dec_area,
                                 binsize=bin_size_train, remove_nans=True,
                                 min_speed=min_speed_train,
                                 min_spikes_per_sample=min_spikes_sample_train,
                                 min_spikes_per_neuron=min_spikes_neuron_train,
                                 pyramidal_only=True)

    # rescaling data to firing rate in Hertz
    X = (dec_data['X'] / (bin_size_train * pq.ms)).rescale(1/pq.s).__array__()
    y = dec_data['y']
    bins = dec_data['bins']
    neuron_sel = dec_data['neuron_sel']

    y, bins = coarsify_bins(y, mode='leftmidright')

    # keeping only left and right
    X = X[y != 1, :]
    y = y[y != 1]
    y[y == 2] = 1

    # --- TRAIN DECODER ------------------------------------------------------------

    if dec_settings_name == 'dkw':
        dec = DKW_decoder(bin_size_train=bin_size_train,
                          bin_size_test=bin_size_pbe_in_ms)

    elif dec_settings_name == 'randfor':
        dec = RandomForestClassifier(n_estimators=200, random_state=1)
        #dec = GaussianNB()

    elif dec_settings_name == 'logreg':
        dec = LogisticRegression(solver='lbfgs', random_state=1)


    dec.fit(X, y)

    # # --- SHOW DECODING ON TASK ---------------------------------------------------
    #
    # y_pred = cross_val_predict(dec, X, y, cv=5)
    # cm = confusion_matrix(y, y_pred)
    # cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    #
    # f, ax = plt.subplots(1, 1, figsize=[5, 5])
    #
    # sns.heatmap(cm,
    #             xticklabels=['Left', 'Right'],
    #             yticklabels=['Left', 'Right'],
    #             ax=ax, square=True,
    #             annot=True,
    #             cmap='Blues')
    # ax.set_ylabel('True side')
    # ax.set_xlabel('Decoded side')
    #
    #
    # if save_plots :
    #     f.savefig(os.path.join(plots_folder, 'PR_task_decoding_sess{:02}.png'.format(sess_ind)), dpi=300)
    #


    # --- COMPUTE AUC SCORES -------------------------------------------------------

    df = pd.DataFrame(columns=['preferred_side', 'voter_score'])
    for i in range(X.shape[1]):
        auc = roc_auc_score(y, X[:, i])
        if auc > 0.5:
            df.loc[i, 'preferred_side'] = 'right'
            df.loc[i, 'voter_score'] = auc
        elif auc < 0.5:
            df.loc[i, 'preferred_side'] = 'left'
            df.loc[i, 'voter_score'] = 1-auc
        else:
            print('Unit {} has no preferred side'.format(i))

    df = df.sort_values(by=['preferred_side', 'voter_score'], ascending=False)

    left_ind = df.index[df['preferred_side'] == 'left']
    right_ind = df.index[df['preferred_side'] == 'right']

    left_scores = df.loc[df['preferred_side'] == 'left', 'voter_score']
    left_scores = -(left_scores - 0.5)
    left_scores = left_scores.as_matrix().astype(float)

    right_scores = df.loc[df['preferred_side'] == 'right', 'voter_score']
    right_scores = right_scores - 0.5
    right_scores = right_scores.as_matrix().astype(float)


    # --- REMOVE COLUMNS FROM PBES DATAFRAME --------------------------------------
    # the pbes dataframe contains columns which refer to population activity
    # in the area used to detect the pbes (whereas in this script we decode the
    # activity of a potentially different area). to avoid confusion, we drop
    # those columns

    if dec_area != PBEs_area:
        for col in ['percentage_active', 'n_active', 'n_spikes']:
            try:
                pbes['pbes'] = pbes['pbes'].drop(col, axis=1)
            except ValueError:
                pass

    # --- DECODE PBES -------------------------------------------------------------

    new_cols = ['pbe_prob_sum', 'pbe_prob_mean', 'pbe_n_bins',
                'left_score_active', 'right_score_active', 'side_score']

    for col in new_cols:
        pbes['pbes'][col] = None


    for pbe_ind, pbe_number in enumerate(PBEs_indices):

        spikes = binned_pbes[pbe_ind]
        ip     = in_pbe_mask[pbe_ind]

        print('Processing PBE event {}'.format(pbe_number))
        spikes = spikes[:, neuron_sel] # this is important!

        print(spikes.sum(), spikes[ip, :].sum())

        if spikes[ip, :].sum() > min_total_spikes_pbe:

            if dec_settings_name == 'dkw_decoder':
                spikes_decode = spikes

            else:
                if remove_pbe_samples_w_no_spikes:
                    nonzero_ind = spikes.sum(axis=1)>0
                    spikes = spikes[nonzero_ind, :]
                    ip = ip[nonzero_ind]

                spikes_decode = (spikes / (bin_size_pbe_in_ms * pq.ms)).rescale(1/pq.s).__array__()
                #spikes_decode = spikes * (bin_size_train / bin_size_pbe_in_ms)

            pbe_n_bins   = ip.sum()
            y_pred_proba = dec.predict_proba(spikes_decode)
            # the second column is the probability of the 1 class (which is the right)
            # Since they sum to 1, we subtract 0.5 so that if they are perfectly balanced
            # we are at zero, positive is more probability to the right, negative to the left
            pbe_side_proba = y_pred_proba[:, 1][np.newaxis, :] - 0.5
            proba_in_pbe = np.ma.masked_where(~ip[None, :], pbe_side_proba)

            # Side score per side as the fraction of the total voter score (voter score
            # summed over all neurons of each side) that gets activated (at least 1 spikes)
            left_neurons_active = spikes[:, left_ind].sum(axis=0) > 0
            right_neurons_active = spikes[:, right_ind].sum(axis=0) > 0

            left_score_active = left_scores[left_neurons_active].sum() / left_scores.sum()
            right_score_active = right_scores[right_neurons_active].sum() / right_scores.sum()

            side_score = (right_score_active - left_score_active) \
                         / (right_score_active + left_score_active)

            assert 0 <= left_score_active <= 1
            assert 0 <= right_score_active <= 1
            assert  -1 <= side_score <= 1

            pbes['pbes'].loc[pbe_number, 'pbe_prob_sum'] = proba_in_pbe.sum()
            pbes['pbes'].loc[pbe_number, 'pbe_prob_mean'] = proba_in_pbe.mean()
            pbes['pbes'].loc[pbe_number, 'pbe_n_bins'] = pbe_n_bins
            pbes['pbes'].loc[pbe_number, 'left_score_active'] = left_score_active
            pbes['pbes'].loc[pbe_number, 'right_score_active'] = right_score_active
            pbes['pbes'].loc[pbe_number, 'side_score'] = side_score

            # if plot_events:
            #
            #     scored_left = spikes[:, left_ind].copy()
            #     scored_left = scored_left * left_scores[np.newaxis, :]
            #
            #     scored_right = spikes[:, right_ind].copy()
            #     scored_right = scored_right * right_scores[np.newaxis, :]
            #
            #     scored_all = np.hstack((scored_left, scored_right))
            #     scored_all = np.ma.masked_where(scored_all == 0, scored_all)
            #
            #     ipmat = ip[:, None].repeat(scored_all.shape[1], axis=1)
            #     scored_all_in_pbe = np.ma.masked_where(~ipmat, scored_all)
            #     scored_all_not_in_pbe = np.abs(np.ma.masked_where(ipmat, scored_all))
            #     proba_not_in_pbe = np.abs(np.ma.masked_where(ip[None, :], pbe_side_proba))
            #
            #     cmap = plt.cm.RdBu
            #     cmap.set_bad(color='white')
            #
            #     f = plt.figure()
            #     gs = gridspec.GridSpec(2, 1,height_ratios=[6, 1])
            #
            #     ax1 = plt.subplot(gs[0])
            #     ax2 = plt.subplot(gs[1])
            #
            #     f.subplots_adjust(right=0.75)
            #
            #     im = ax1.imshow(scored_all_in_pbe.T, origin='lower', cmap=cmap,
            #                     aspect='auto', vmin=-0.5, vmax=0.5)
            #     im2 = ax1.imshow(scored_all_not_in_pbe.T, origin='lower', cmap='Greys',
            #                     aspect='auto', vmin=0, vmax=1)
            #     cax = f.add_axes([0.85, 0.15, 0.05, 0.7])
            #     cbar = f.colorbar(im, cax=cax,ticks=[-0.5, 0, 0.5])
            #     ax1.axhline(scored_all.shape[1]- scored_right.shape[1] - 0.5,
            #                ls = ':', c='grey', alpha=0.7)
            #     cbar.ax.set_yticklabels(['Left', '0', 'Right'])  # vertically oriented colorbar
            #     ax1.set_yticks(range(scored_all.shape[1]))
            #     ax1.set_yticklabels([])
            #     ax1.set_xticklabels([])
            #     ax1.set_ylabel('Neuron index (sorted)')
            #     ax2.set_ylabel('Decoded\nside')
            #     ax2.set_xlabel('Time bins')
            #     ax2.set_yticks([])
            #     ax2.imshow(proba_in_pbe, origin='lower', cmap=cmap,
            #                  aspect='auto', vmin=-0.5, vmax=0.5)
            #     ax2.imshow(np.abs(proba_not_in_pbe), origin='lower', cmap='Greys',
            #                  aspect='auto', vmin=-0, vmax=1)
            #     sns.despine(f)
            #
            #     if save_plots:
            #         f.savefig(os.path.join(event_plots_folder, 'sess{:02}_pbe_{:03}.{}'.format(sess_ind, pbe_number, plots_format)), dpi=300)

        else:
            print('       - Skipping PBE {}: Not enough spikes - REMOVING'.format(pbe_number))
            pbes['pbes'] = pbes['pbes'].drop(pbe_number, axis=0)

    ppm = pbes['pbes']['pbe_prob_mean']

    pbes['pbes']['pbe_side'] = None
    pbes['pbes']['pbe_side'][ppm > 0] = 'right'
    pbes['pbes']['pbe_side'][ppm <= 0] = 'left'

    thr = ppm.quantile(0.5)
    pbes['pbes']['pbe_side_balanced'] = None
    pbes['pbes']['pbe_side_balanced'][ppm > thr] = 'right'
    pbes['pbes']['pbe_side_balanced'][ppm <= thr] = 'left'

    confidence = np.abs(proba_in_pbe.mean())
    pbes['pbes']['confidence_naive'] = ppm
    pbes['pbes']['confidence_balanced'] = ppm - thr

    pbes['pbes']['pbe_side_voters'] = None

    for pbe_number, l, r in zip(pbes['pbes'].index,
                                pbes['pbes']['left_score_active'],
                                pbes['pbes']['right_score_active']):
        if l > r:
            pbes['pbes'].loc[pbe_number, 'pbe_side_voters'] = 'left'
        elif r > l:
            pbes['pbes'].loc[pbe_number, 'pbe_side_voters'] = 'right'
        else:
            pbes['pbes'].loc[pbe_number, 'pbe_side_voters'] = 'tie'


    cols = ['pbe_prob_sum', 'pbe_prob_mean', 'pbe_n_bins',
                'left_score_active', 'right_score_active', 'side_score',
                'confidence_naive', 'confidence_balanced']

    for col in cols:
        pbes['pbes'][col] = pd.to_numeric(pbes['pbes'][col])
        pbes['pbes'] = pbes['pbes'].round({col: 3})


    # --- REINDEX DF WITH UNIQUE PBE IDs ------------------------------------------

    pbes['pbes'] = reindex_pbes_dfs(pbes['pbes'])


    # --- SAVE OUTPUT -------------------------------------------------------------

    pbes['decode_pars'] = decode_pars

    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)

    output_file = os.path.join(output_folder,
                           'decoded_PBEs_sess_{:02}_{}_{}.pkl'.format(sess_ind, dec_area, epoch))

    print('Saving output to {}'.format(output_file))

    pickle.dump(pbes, open(output_file, 'wb'))






