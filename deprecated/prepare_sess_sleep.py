import os

import neo
import numpy as np
import quantities as pq
import elephant
import matplotlib.pyplot as plt
import scipy.signal
from loadmat import loadmat
import pickle
import scipy.io

#--------SET PARAMETERS -------------------------------------------------------

try:
    __IPYTHON__
except NameError:
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sess_ind', type=int)
    parser.add_argument('-d', '--data_folder', type=str, default='/media/pietro/bigdata/neuraldata/touch&see/')
    parser.add_argument('-t', '--save_to', type=str, default='.')
    parser.add_argument('-k', '--kernel_sigma', type=int, default=20)
    parser.add_argument('-p', '--sampling_period', type=int, default=10)
    parser.add_argument('-e', '--sleep_epoch', type=str)
    parser.add_argument('-a', '--area', type=str)

    ARGS = parser.parse_args()

    data_folder          = ARGS.data_folder
    save_to              = ARGS.save_to
    sess_ind             = ARGS.sess_ind
    kernel_sigma         = ARGS.kernel_sigma
    sampling_period      = ARGS.sampling_period
    sleep_epoch          = ARGS.sleep_epoch
    area                 = ARGS.area

    plot      = False
    save_data = True

else:
    data_folder     = '/media/pietro/bigdata/neuraldata/touch&see/'
    save_to         = '/media/pietro/bigdata/neuraldata/ts_replay/'
    sess_ind        = 22 # Python index - sess_ind 0 is session 1
    kernel_sigma    = 20
    sampling_period = 10
    sleep_epoch     = 'post' # or 'post'
    area            = 'both' # set to 'all' to keep all neurons

    # interactive parameters
    plot            = False
    save_data       = False

kernel_size     = kernel_sigma * 8

pars = {'sess_ind'        : sess_ind,
        'kernel_sigma'    : kernel_sigma,
        'kernel_size'     : kernel_size,
        'sampling_period' : sampling_period,
        'sleep_epoch'     : sleep_epoch,
        'area'            : area}



#-------- LOAD MATLAB FILES ---------------------------------------------------
print('Preparing and smoothing Touch & See sleep session {}'.format(sess_ind))

F = loadmat(data_folder + '/F/F_units/F_All.mat')['F']

rat, day, session = F[sess_ind].dir.split('/')[-3:]
session_path = data_folder + 'SWSPeriods/' + rat + '/' + day + '/' + session + '/spikeTrl.mat'

try:
    S = loadmat(session_path)
except TypeError:
    raise ValueError('No spikeTrl.mat for session {}'.format(sess_ind))

U = loadmat(data_folder + 'unit.mat')

unit_index    = U['unit']['sess'] == sess_ind + 1
unit_area     = U['unit']['area'][unit_index]
unit_depth    = U['unit']['unitDepth'][unit_index]
unit_layer    = U['unit']['unitLayer'][unit_index]
unit_rat      = U['unit']['ratName'][unit_index]
unit_sess     = U['unit']['sess'][unit_index]
unit_sess_num = U['unit']['unitSessNum'][unit_index]
unit_label    = U['unit']['label'][unit_index]
n_units       = unit_area.shape[0]

global_unit_ind = np.where(unit_index)[0]+1

# map the unit area labels for easier selection
map = {'Br' : 'Barrel', 'Hp' : 'Hippocampus', 'b3536' : 'Perirhinal',
       '35' : 'Perirhinal', '36' : 'Perirhinal', 'V' : 'V', 'tea' : 'tea'}

unit_area = np.array([map[u] for u in unit_area])


if sleep_epoch == 'pre':
    spike_trains = S['spikeTrlPre']['timestamp']
    periods = S['spikeTrlPre']['cfg']['trl']

elif sleep_epoch == 'post':
    spike_trains = S['spikeTrlPost']['timestamp']
    periods = S['spikeTrlPost']['cfg']['trl']

# --- LOAD UNIT TYPE ----------------------------------------------------------

U = loadmat(data_folder + 'unit.mat')
W = loadmat(data_folder + 'wave_class.mat')

unit_sess = U['unit']['sess'] - 1
unit_area2 = U['unit']['area']
unit_sess_ind = np.where(unit_sess == sess_ind)[0]

if area == 'both':
    sel = np.isin(unit_area2[unit_sess_ind], ['Hp', '35', '36', 'b3536'])

elif area == 'Hippocampus':
    sel = np.isin(unit_area2[unit_sess_ind], ['Hp'])

elif area == 'Perirhinal':
    sel = np.isin(unit_area2[unit_sess_ind], ['35', '36', 'b3536'])

unit_sess_ind = unit_sess_ind[sel]

neuron_type_labels = W['R']['neuronTypeLabel']
neuron_type_code = W['R']['neuronType']-1
neuron_type = neuron_type_labels[neuron_type_code]

neuron_type_sess = neuron_type[unit_sess_ind]
not_interneuron   = [0 if t=='narrow_int' else 1 for t in neuron_type_sess]


#-------- CONVERT TIMES TO SECONDS --------------------------------------------
periods = periods / 1e6
spike_trains = spike_trains / 1e6


#-------- BIN SPIKES IN 1 MS BINS FOR EACH SLEEP EPOCH ------------------------
if area == 'both':
    area_list = ['Hippocampus', 'Perirhinal']
elif area == 'all':
    area_list = ['Barrel', 'Hippocampus', 'Perirhinal', 'V']
else:
    area_list = [area]

spike_arrays = []
for period_n, period in enumerate(periods):

    all_spike_trains_period = []
    t_start, t_stop = period[0], period[1]

    for j, spike_train in enumerate(spike_trains):

        if np.isin(unit_area[j], area_list):
            try:
                spike_train_period = spike_train[
                    (spike_train >= t_start) & (spike_train < t_stop)].copy()
            except TypeError:
                spike_train = np.array([spike_train])
                spike_train_period = spike_train[
                    (spike_train >= t_start) & (spike_train < t_stop)].copy()

            neo_train = neo.SpikeTrain(spike_train_period*pq.s,
                                       t_start=t_start*pq.s,
                                       t_stop=t_stop*pq.s)
            all_spike_trains_period.append(neo_train)

    bs = elephant.conversion.BinnedSpikeTrain(all_spike_trains_period,
                                              binsize=1*pq.ms,
                                              t_start=t_start * pq.s,
                                              t_stop=t_stop * pq.s)

    spike_array_period = bs.to_array()

    # if two or more spikes occur in a 1 ms bin, clip to 1
    spike_array_period = np.clip(spike_array_period, a_min=0, a_max=1)

    spike_arrays.append(spike_array_period)
    print('Period {} - max {} spikes per bin - {} samples'
          ''.format(period_n, spike_array_period.max(),
                    spike_array_period.shape[1]))



#-------- SMOOTH WITH GAUSSIAN KERNEL -----------------------------------------
kernel = scipy.signal.gaussian(kernel_size, kernel_sigma, sym=True)
kernel = 1000 * kernel / kernel.sum()

rate_arrays = []
for spike_array in spike_arrays:

    rate_array = np.apply_along_axis(
        lambda m: np.convolve(m, kernel, mode='same'),
        axis=1, arr=spike_array)

    rate_array = rate_array[:, int(kernel_size / 2):-int(kernel_size / 2)]
    rate_array = rate_array[:, ::sampling_period]
    rate_arrays.append(rate_array)

#full_rate_array = np.hstack(rate_arrays)


# --- SAVE DATA ---------------------------------------------------------------
if save_data:

    if not os.path.isdir(save_to):
        os.makedirs(save_to)

    pars['animal'] = rat
    pars['day'] = day
    pars['session'] = session

    out = {'data' : rate_arrays,
           'pars' : pars,
           'unit_type' : neuron_type_sess,
           'not_interneuron' : not_interneuron}

    if area == 'Perirhinal':
        area_init = 'PR'
    elif area == 'Hippocampus':
        area_init = 'HP'
    elif area == 'both':
        area_init = 'HPPR'

    file_name = 'TS_{}_sleep_s{}_{}'.format(sleep_epoch, sess_ind + 1, area_init)

    pickle.dump(out, open(os.path.join(save_to, '{}.pkl'.format(file_name)), 'wb'))

    out['data'] = np.hstack(rate_arrays)
    scipy.io.savemat(os.path.join(save_to, '{}.mat'.format(file_name)), out)

if plot:
    # raster plot
    f, ax = plt.subplots(1, 1)
    ax.imshow(spike_arrays[0], aspect='auto')

    period = 2
    neuron = 10

    # single neuron spikes and estimated firing rate
    f, ax = plt.subplots(1, 1)
    time_axis_full = np.arange(spike_arrays[period].shape[1])
    time_axis_smooth = time_axis_full[int(kernel_size / 2):
                                      -int(kernel_size / 2)]
    time_axis_smooth = time_axis_smooth[::sampling_period]
    ax.plot(time_axis_full, 10*spike_arrays[period][neuron, :])
    ax.plot(time_axis_smooth, rate_arrays[period][neuron, :] ,c='g')

    # heatmap
    r = np.hstack(rate_arrays[0:10])
    f, ax = plt.subplots(1, 1)
    im = ax.imshow(r, aspect='auto')
    f.colorbar(im)

    # heatmap scaled
    min, max = 0, 1
    r = r.T
    r_std = (r - r.min(axis=0)) / (r.max(axis=0) - r.min(axis=0))
    r_scaled = r_std * (max - min) + min

    r = np.hstack(rate_arrays[0:10])
    f, ax = plt.subplots(1, 1)
    im = ax.imshow(r_scaled.T, aspect='auto')
    f.colorbar(im)


