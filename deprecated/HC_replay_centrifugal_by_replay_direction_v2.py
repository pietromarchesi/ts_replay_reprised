import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
import matplotlib.patches as mpatches
import itertools
from utils import *
from statsmodels.stats.proportion import proportions_ztest
from constants import *
from scipy.stats import wilcoxon, mannwhitneyu


data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']
group_iti_and_return = True

# PLOTTING PARAMETERS
#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'dec6'
phases = ['all', 'iti', 'img', 'reward']

n_bootstraps = 300
min_pbe_duration_in_ms    = 50
shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

save_plots = False
plot_format = 'png'
dpi = 400

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, 'HC_replay')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

contrast = 'radial_direction'
col_name = 'replay_direction'
col_vals = ['positive', 'negative']

# df = load_replay(sessions=sessions,
#                  PBEs_settings_name=PBEs_settings_name,
#                  PBEs_area=PBEs_area,
#                  data_version=data_version,
#                  dec_settings_name=dec_settings_name,
#                  dec_area=dec_area,
#                  min_pbe_duration_in_ms=min_pbe_duration_in_ms,
#                  shuffle_types=shuffle_types,
#                  shuffle_p_vals=shuffle_p_vals,
#                  group_iti_and_return=group_iti_and_return)

df = pd.read_csv(os.path.join(REPLAY_FOLDER, 'df_final_selection.csv'))


dfxy = add_radial_direction(df.copy())


dg = pd.DataFrame(columns=['sess_ind', 'phase', 'replay_direction', 'perc_centr', 'ntot'])
for phase in phases :
    for sess_ind in sessions :
        for dir in ['positive', 'negative']:
            if phase == 'all' :
                dk = dfxy[
                    (dfxy['sess_ind'] == sess_ind) & (dfxy['replay_direction'] == dir)]
            else :
                dk = dfxy[(dfxy['sess_ind'] == sess_ind) & (dfxy['replay_direction'] == dir)
                          & (dfxy['phase'] == phase)]
            if dk.shape[0] >= 5 :
                npos = dk[dk['radial_direction'] == 'centrifugal'].shape[0]
                nneg = dk[dk['radial_direction'] == 'centripetal'].shape[0]
                perc = (100 * npos / (npos + nneg))
                ntot = npos + nneg
                dg.loc[dg.shape[0], :] = [sess_ind, phase, dir, perc, ntot]

dg['perc_centr'] = pd.to_numeric(dg['perc_centr'])

f, ax = plt.subplots(1, 1, figsize=slightly_vertical_panel)

# sns.barplot(x=['all'] + phases, y=[100] * len(['all'] + phases),
#             edgecolor=radial_direction_palette['centripetal'],
#             facecolor='white', linewidth=3.5)

g = sns.barplot(data=dg, x='phase', y='perc_centr', hue='replay_direction',
                estimator=median, linewidth=3.5, errcolor='0.2')

inx = np.array([0, 1, 2, 3])
for i, patch in enumerate(ax.patches) :

    if np.isin(i, inx) :
        patch.set_edgecolor(replay_direction_palette['positive'])
        patch.set_facecolor('white')

inx = np.array([4, 5, 6, 7])
for i, patch in enumerate(ax.patches) :

    if np.isin(i, inx) :
        patch.set_edgecolor(replay_direction_palette['negative'])
        patch.set_facecolor('white')

ax.legend().remove()
sns.despine()
ax.set_ylabel('% of centrifugal replay events')
ax.set_xticklabels(
    [task_phase_labels_nobreak[l._text] for l in ax.get_xticklabels()],
    rotation=45)
ax.set_xlabel('')
ax.axhline(50, c=maze_color, ls='--', lw=2)
plt.tight_layout()

plot_name = 'perc_centrifugal_events_per_task_phase_per_direction.{}'.format(
                                                                  plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#plt.close(fig=f)

# --- STATISTICS ---------------------------------------------------------------

n_tests = len(phases)
for phase in phases:

    df1 = dg[(dg['phase'] == phase) & (dg['replay_direction'] == 'positive')]
    df2 = dg[(dg['phase'] == phase) & (dg['replay_direction'] == 'negative')]

    x = df1['perc_centr']
    y = df2['perc_centr']

    st, p = mannwhitneyu(x, y)

    adjusted_p = p * n_tests
    is_sig = adjusted_p < 0.05
    print('{} - p={:.1g}, is_sig={}'.format(phase, p,is_sig))
