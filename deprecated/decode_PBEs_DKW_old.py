import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(sys.path[0]))))
import numpy as np
import pickle
import neo
import pandas as pd
from replaydetector.DKW_decoder import DKW_decoder
from utils import get_decoding_data
from utils import get_session_block
from utils import merge_segments
from utils import bin_PBEs, load_PBEs
from utils import load_bin_centers, prepare_bins
from utils import get_average_running_speed
from utils import get_position_bin
from replaydetector.replaydetector import ReplayDetector
from constants import DATA_FOLDER, REPLAY_FOLDER, mean_distance_between_bins
import quantities as pq
from utils import get_maze_arm_of_point
from constants import mean_distance_between_bins
from replaydetector.replay_utils import replay_wrap_bins_and_direction
from replaydetector.replay_utils import reindex_pbes_dfs, replay_wrap_traversed_bins


try:
    __IPYTHON__
except NameError:

    import argparse
    import distutils.util
    parser = argparse.ArgumentParser()

    parser.add_argument('--sess_ind', type=int)
    parser.add_argument('--epoch', type=str, default='task',
                        choices=['task', 'pre_sleep', 'post_sleep'])
    parser.add_argument('--data_version', type=str)
    parser.add_argument('--PBEs_settings_name', type=str)
    parser.add_argument('--PBEs_area', type=str, default='Hippocampus')
    parser.add_argument('--dec_settings_name', type=str)
    parser.add_argument('--dec_area', type=str, default='Hippocampus')

    ARGS = parser.parse_args()

    sess_ind           = ARGS.sess_ind
    epoch              = ARGS.epoch
    PBEs_settings_name = ARGS.PBEs_settings_name
    PBEs_area          = ARGS.PBEs_area
    data_version       = ARGS.data_version
    dec_settings_name  = ARGS.dec_settings_name
    dec_area           = ARGS.dec_area


else:
    sess_ind                = 30
    epoch                   = 'task'
    PBEs_settings_name      = 'dec16'
    PBEs_area               = 'Hippocampus'
    data_version            = 'dec16'
    dec_settings_name       = 'DecSetTest'
    dec_area                = 'Hippocampus'

data_folder             = DATA_FOLDER
replay_folder           = REPLAY_FOLDER

# parameters which we keep fixed
plot_replay_events = False
plot_events_both_side = False
save_results = True
min_traj_bins = 2 #with min_traj_bins=2 the shortest trajectory is 3 bins in total
min_traj_speed_cm_s = 100  # in cm / s
min_speed_train = 12
min_spikes_sample_train = 1
min_spikes_neuron_train = 15
subsample_fit_lines = 1
shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle',
                 'unit_identity_shuffle']


# parameters which we vary
if dec_settings_name == 'DecSetTest':
    n_surr                  = 4
    n_surr_side             = 2
    smooth_ratemap          = True
    smooth_sigma            = 2
    bin_size_train          = 300
    bin_size_pbe_in_ms      = 20
    slide_by_pbe_in_ms      = 10
    n_bins_proba            = 1
    bins_ext                = 10
    shuffle_type_side       = 'column_cycle_shuffle'  # shuffle type used to determined left vs right replay


elif dec_settings_name == 'DecSetClassic':
    n_surr                  = 100
    n_surr_side             = 50
    smooth_ratemap          = True
    smooth_sigma            = 2
    bin_size_train          = 300
    bin_size_pbe_in_ms      = 20
    slide_by_pbe_in_ms      = 10
    n_bins_proba            = 2
    bins_ext                = 8
    shuffle_type_side       = 'column_cycle_shuffle'  # shuffle type used to determined left vs right replay





# --- SET UP PATHS ------------------------------------------------------------

output_folder = os.path.join(replay_folder, 'results', 'decoded_PBEs',
                             'pbe_setting_{}_{}_{}_dec_setting_{}_{}'.format(PBEs_settings_name, PBEs_area,
                              data_version, dec_settings_name, dec_area))

plots_folder  = os.path.join(replay_folder, 'plots',
                             'pbe_setting_{}_{}_{}_dec_setting_{}_{}'.format(PBEs_settings_name, PBEs_area,
                              data_version, dec_settings_name, dec_area),
                             'session_{}'.format(sess_ind))

detectors_folder = os.path.join(output_folder, 'detectors')

if not os.path.isdir(output_folder):
    os.makedirs(output_folder, exist_ok=True)



# --- SET UP PARAMETERS DICT --------------------------------------------------

decode_pars = {'area' : dec_area,
               'dec_settings' : dec_settings_name,
               'n_surr' : n_surr,
               'n_surr_side' : n_surr_side,
               'smooth_ratemap' : smooth_ratemap,
               'bin_size_train' : bin_size_train,
               'bin_size_pbe_in_ms' : bin_size_pbe_in_ms,
               'slide_by_pbe_in_ms' : slide_by_pbe_in_ms,
               'n_bins_proba' : n_bins_proba,
               'bins_ext' : bins_ext,
               'subsample_fit_lines' : subsample_fit_lines,
               'min_traj_bins' : min_traj_bins,
               'min_traj_speed_cm_s' : min_traj_speed_cm_s,
               'min_speed_train' : min_speed_train,
               'min_spikes_sample_train' : min_spikes_sample_train,
               'min_spikes_neuron_train' : min_spikes_neuron_train,
               'shuffle_type_side' : shuffle_type_side,
               'shuffle_types' : shuffle_types}



# --- PRINT SOME INFO ---------------------------------------------------------
print('Running DKW PBE decoding for \n - session {} \n'
      ' - PBE settings {}, area {} \n - decoding settings {}'
      '\n - decoding area {}'.format(sess_ind, PBEs_settings_name,
                                     PBEs_area, dec_settings_name, dec_area))

# --- LOAD PBEs ---------------------------------------------------------------
print('\n- Loading PBEsmooth_sigmas')
pbes = load_PBEs(sess_ind, PBEs_settings_name, PBEs_area, data_version=data_version)
#pbes['pbes'] = pbes['pbes'][0:15]
PBEs_indices = pbes['pbes'].index
PBEs_times = list(zip(pbes['pbes']['start_time_in_ms'] * pq.ms, pbes['pbes']['end_time_in_ms'] * pq.ms))

PBEs_times = list(zip([i*pq.ms for i in pbes['pbes']['start_time_in_ms']],
                      [i*pq.ms for i in pbes['pbes']['end_time_in_ms']]))

# print(PBEs_times)
# print(PBEs_times[0][0].units)
# --- LOAD BLOCK --------------------------------------------------------------
bl = get_session_block(data_folder, sess_ind, epoch, data_version=data_version)
if epoch == 'task':
    segment = merge_segments(bl.segments[:-1], bl.list_units)
    rat_speed_signal = segment.irregularlysampledsignals[1].rescale(pq.cm / pq.s)

elif epoch == 'pre_sleep':
    segment = bl.segments[0]
    assert segment.name == 'pre-sleep'

elif epoch == 'post_sleep':
    segment = bl.segments[1]
    assert segment.name == 'post-sleep'

rat = bl.annotations['animal_ID']
day_folder = bl.annotations['day_folder']
session_folder = bl.annotations['session_folder']
bin_centers_combined = load_bin_centers(data_folder, rat, day_folder,
                                        session_folder)
bin_centers, bin_labels = prepare_bins(bin_centers_combined)

average_running_speed = get_average_running_speed(sess_ind, data_folder=data_folder,
                                                  data_version=data_version)

# --- BIN PBEs ----------------------------------------------------------------

print('\n- Preparing PBEs data')
trains = segment.filter(targdict={'area': dec_area}, objects=neo.SpikeTrain)
binned_pbes = bin_PBEs(trains, PBEs_times, bin_size_in_ms=bin_size_pbe_in_ms,
                       sliding_binning=True, slide_by_in_ms=slide_by_pbe_in_ms,
                       pyramidal_only=False)
# note: here it's important that pyramidal_only is set to False, because later
# the call to dec_data outputs dict with a 'neuron_sel' field which will be
# a combination of pyramidal-ness (if pyramidal_only is set to True there)
# and other criteria if they are applied

binned_pbes_dict = {ind : sp for ind, sp in zip(PBEs_indices, binned_pbes['PBEs_binned'])}


# --- PREPARING TASK DATA AND TRAINING LEFT AND RIGHT DECODERS ----------------

print('\n- Preparing task data')

dec_data = get_decoding_data(sess_ind=sess_ind, data_folder=data_folder,
                             area=dec_area,
                             binsize=bin_size_train, remove_nans=True,
                             min_speed=min_speed_train,
                             data_version=data_version,
                             min_spikes_per_sample=min_spikes_sample_train,
                             min_spikes_per_neuron=min_spikes_neuron_train,
                             pyramidal_only=True, side=None)

neuron_sel = dec_data['neuron_sel']


sides = ['left', 'right']
bins_dict = {s : None for s in sides}
decoders = {s : None for s in sides}

for side in sides:

    side_mask = dec_data['side'] == side
    X = dec_data['X'][side_mask, :]
    y = dec_data['y'][side_mask]
    bins = np.unique(y)
    print('Fitting decoder on {} side data'.format(side))

    dec  = DKW_decoder(bin_size_train=bin_size_train,
                       bin_size_test=bin_size_pbe_in_ms,
                       smooth_ratemap=smooth_ratemap,
                       smooth_sigma=smooth_sigma)

    dec.fit(X, y)
    decoders[side] = dec
    bins_dict[side] = bins


# --- REMOVE COLUMNS FROM PBES DATAFRAME --------------------------------------
# the pbes dataframe contains columns which refer to population activity
# in the area used to detect the pbes (whereas in this script we decode the
# activity of a potentially different area). to avoid confusion, we drop
# those columns

if dec_area != PBEs_area:
    for col in ['percentage_active', 'n_active', 'n_spikes']:
        try:
            pbes['pbes'] = pbes['pbes'].drop(col, axis=1)
        except ValueError:
            pass

# --- DECODE PBES -------------------------------------------------------------
sig_rows, detectors = [], []
pbes['pbes']['pbe_side'] = None
pbes['pbes']['replay_score'] = None
pbes['pbes']['dec_start_bin_nowrap'] = None
pbes['pbes']['dec_end_bin_nowrap'] = None
pbes['pbes']['dec_start_bin'] = None
pbes['pbes']['dec_end_bin'] = None
pbes['pbes']['traj_speed'] = None
pbes['pbes']['replay_direction'] = None
pbes['pbes']['av_running_speed'] = average_running_speed
pbes['pbes']['max_jump_dist'] = None
pbes['pbes']['mean_jump_dist'] = None
pbes['pbes']['sharpness'] = None
pbes['pbes']['trajectory'] = None
pbes['pbes']['n_time_bins'] = None

# pbes['trajectories'] = pd.DataFrame(index=pbes['pbes'].index,
#                                     columns=['trajectory'])


for st in shuffle_types:
    pbes['pbes']['replay_z_score_{}'.format(st)] = None

print('\n- Decoding PBEs')
for pbe_ind in PBEs_indices:

    spikes = binned_pbes_dict[pbe_ind]
    start_time = pbes['pbes'].loc[pbe_ind]['start_time_in_ms'] * pq.ms
    end_time = pbes['pbes'].loc[pbe_ind]['end_time_in_ms'] * pq.ms

    print('    > Decoding PBE #{}'.format(pbe_ind))

    # --- DETERMINE SIDE OF EVENT ---
    scores = {}
    for side in sides:
        dec = decoders[side]
        bins = bins_dict[side]
        spikes_sel = spikes[:, neuron_sel]

        RD = ReplayDetector(decoder=decoders[side],
                            distance_between_bins_in_cm=mean_distance_between_bins)

        RD.set_line_fitting_pars(n_bins_proba=n_bins_proba,
                                 bins_ext=bins_ext,
                                 subsample=subsample_fit_lines,
                                 min_traj_bins=min_traj_bins,
                                 min_traj_speed_cm_s=min_traj_speed_cm_s)
        RD.add_pbe(spikes_sel, start_time=start_time, end_time=end_time)

        print(side, RD.trajectory_speed, RD.start_location, RD.end_location)

        if RD.trajectory_speed >= min_traj_speed_cm_s:
            if n_surr_side > 0:
                sigdf = RD.compute_significance(n_surr=n_surr_side,
                                                shuffle_type=shuffle_type_side)
                scores[side] = RD.get_replay_z_score(shuffle_type_side)
            else:
                scores[side] = RD.get_replay_score()
        else:
            pass

        if plot_events_both_side:
            f = RD.plot_replay()
            f.suptitle('{}, score={:.2f}, speed={:.2f}'.format(side, scores[side],
                                                           RD.trajectory_speed))
            figname = 'pbe_{}_{}.png'.format(pbe_ind, side)
            #f.savefig(os.path.join(plots_folder, 'left_right', figname), dpi=400)

    try:
        side_best_fit = max(scores, key=scores.get)
    except ValueError:
        side_best_fit = None
    print(RD.trajectory_speed, scores, side_best_fit)

    if side_best_fit is None:
        print('       - Skipping PBE {}: only flat trajectories - REMOVING'.format(pbe_ind))
        pbes['pbes'] = pbes['pbes'].drop(pbe_ind, axis=0)
        #pbes['trajectories'] = pbes['trajectories'].drop(pbe_ind, axis=0)
        continue

    else:
        print('       - Computing trajectory and signficance for PBE {}'.format(pbe_ind))
        pbes['pbes'].loc[pbe_ind, 'pbe_side'] = side_best_fit

        dec = decoders[side_best_fit]
        bins = bins_dict[side_best_fit]
        spikes_sel = spikes[:, neuron_sel]

        RD = ReplayDetector(decoder=dec,
                            distance_between_bins_in_cm=mean_distance_between_bins)
        RD.set_line_fitting_pars(n_bins_proba=n_bins_proba, bins_ext=bins_ext,
                                 subsample=subsample_fit_lines)
        RD.add_pbe(spikes_sel, start_time=start_time, end_time=end_time)

        pbes['pbes'].loc[pbe_ind, 'replay_score'] = RD.get_replay_score()

        for shuffle_type in shuffle_types:
            print('                > Shuffle: {}'.format(shuffle_type))
            row = RD.compute_significance(n_surr=n_surr, shuffle_type=shuffle_type)
            pbes['pbes'].loc[pbe_ind, 'replay_z_score_{}'.format(shuffle_type)] = RD.get_replay_z_score(shuffle_type)

        start_location = RD.start_location
        end_location = RD.end_location
        max_jump_dist = RD.compute_maximum_jump_distance()
        mean_jump_dist = RD.compute_mean_jump_distance()
        sharpness = RD.compute_sharpness()

        pbes['pbes'].loc[pbe_ind, 'dec_start_bin_nowrap'] = start_location
        pbes['pbes'].loc[pbe_ind, 'dec_end_bin_nowrap'] = end_location
        pbes['pbes'].loc[pbe_ind, 'replay_speed'] = np.round(RD.trajectory_speed, 2)
        pbes['pbes'].loc[pbe_ind, 'max_jump_dist'] = max_jump_dist
        pbes['pbes'].loc[pbe_ind, 'mean_jump_dist'] = mean_jump_dist
        pbes['pbes'].loc[pbe_ind, 'sharpness'] = sharpness
        pbes['pbes'].loc[pbe_ind, 'n_time_bins'] = RD.y_pred_proba.shape[0]

        start_bin, end_bin, replay_direction = replay_wrap_bins_and_direction(side_best_fit,
                                                                              start_location,
                                                                              end_location)


        pbes['pbes'].loc[pbe_ind, 'dec_start_bin'] = start_bin
        pbes['pbes'].loc[pbe_ind, 'dec_end_bin'] = end_bin
        pbes['pbes'].loc[pbe_ind, 'replay_direction'] = replay_direction

        if plot_replay_events:
            RD.plot_replay(shuffle_type=shuffle_types)

        wrapped_trajectory = replay_wrap_traversed_bins(side_best_fit, RD.traversed_locations)

        #pbes['trajectories'].loc[pbe_ind, 'trajectory'] = wrapped_trajectory
        pbes['pbes'].at[pbe_ind, 'trajectory'] = wrapped_trajectory

        assert wrapped_trajectory.shape[0] == RD.y_pred_proba.shape[0] == spikes_sel.shape[0]
        print(wrapped_trajectory.shape[0], RD.y_pred_proba.shape[0], spikes_sel.shape[0])

        RD.sig_df['PBE_ind'] = pbe_ind

        sig_rows.append(RD.sig_df)
        detectors.append(RD)


# pbes['trajectories'] = pbes['trajectories'].iloc[0:8]
# pbes['pbes'][['pbe_side', 'dec_start_bin_nowrap', 'dec_end_bin_nowrap',
#               'dec_start_bin', 'dec_end_bin', 'replay_direction']]

# PBEs_times = [(row['start_time_in_ms'] * pq.ms, row['end_time_in_ms'] * pq.ms)
#                for i, row in pbes['pbes'].iterrows()]
#
# trains = segment.filter(targdict={'area': dec_area}, objects=neo.SpikeTrain)
# binned_pbes = bin_PBEs(trains, PBEs_times, bin_size_in_ms=bin_size_pbe_in_ms,
#                        sliding_binning=True, slide_by_in_ms=slide_by_pbe_in_ms,
#                        pyramidal_only=False)
# # note: here it's important that pyramidal_only is set to False, because later
# # the call to dec_data outputs dict with a 'neuron_sel' field which will be
# # a combination of pyramidal-ness (if pyramidal_only is set to True there)
# # and other criteria if they are applied
# for bp in binned_pbes['PBEs_binned']:
#     print(bp.shape)



pbes['pbes'] = pbes['pbes'].drop(['bin_index_start', 'bin_index_end'], axis=1)

pbes['sig_df'] = pd.concat(sig_rows)
pbes['sig_df'] = pbes['sig_df'].rename(columns={'PBE_ind': 'event_number'})
pbes['sig_df'].index = pbes['sig_df']['event_number']
pbes['sig_df'] = pbes['sig_df'].drop('event_number', axis=1)
pbes['sig_df']['sess_ind'] = sess_ind
pbes['sig_df']['epoch'] = epoch

pbes['detectors'] = detectors
pbes['decode_pars'] = decode_pars


# pbes['trajectories']['sess_ind'] = pbes['pbes']['sess_ind']
# pbes['trajectories']['epoch'] = pbes['pbes']['epoch']

cols = ['replay_z_score_{}'.format(st) for st in shuffle_types] + ['replay_score']
for col in cols:
    pbes['pbes'][col] = pd.to_numeric(pbes['pbes'][col])
    pbes['pbes'] = pbes['pbes'].round({col: 3})

for col in ['dec_start_bin', 'dec_end_bin']:
    pbes['pbes'][col] = pbes['pbes'][col].astype(int)

# --- ADD INFORMATION ABOUT DECODED LOCATION ----------------------------------


pos_dec_start = []
for bin in pbes['pbes']['dec_start_bin']:
    pos = get_position_bin(bin, bin_centers, bin_labels)
    pos_dec_start.append(pos)
pos_dec_start = np.vstack(pos_dec_start)


pos_dec_end = []
for bin in pbes['pbes']['dec_end_bin']:
    pos = get_position_bin(bin, bin_centers, bin_labels)
    pos_dec_end.append(pos)
pos_dec_end = np.vstack(pos_dec_end)

pbes['pbes']['x_pos_dec_start'] = pos_dec_start[:, 0]
pbes['pbes']['y_pos_dec_start'] = pos_dec_start[:, 1]

pbes['pbes']['x_pos_dec_end'] = pos_dec_end[:, 0]
pbes['pbes']['y_pos_dec_end'] = pos_dec_end[:, 1]

# add maze are
maze_arm_dec_start = []
for loc in pos_dec_start:
    if np.isnan(loc).sum() == 0:
        arm = get_maze_arm_of_point(loc)
    else:
        arm = 'nanpos'
    maze_arm_dec_start.append(arm)

maze_arm_dec_end = []
for loc in pos_dec_end:
    if np.isnan(loc).sum() == 0:
        arm = get_maze_arm_of_point(loc)
    else:
        arm = 'nanpos'
    maze_arm_dec_end.append(arm)

pbes['pbes']['maze_arm_dec_start'] = maze_arm_dec_start
pbes['pbes']['maze_arm_dec_end'] = maze_arm_dec_end


# add distance traversed and replay speed
compression_factor = pbes['pbes']['replay_speed'] / pbes['pbes']['av_running_speed']
n_bins_traversed = np.abs(pbes['pbes']['dec_end_bin_nowrap'] - pbes['pbes']['dec_start_bin_nowrap']).values
distance_traversed = n_bins_traversed * mean_distance_between_bins

pbes['pbes']['n_bins_traversed'] = n_bins_traversed
pbes['pbes']['dist_traversed_cm'] = distance_traversed
pbes['pbes']['compression_factor'] = compression_factor

# sanity check
replay_speed = distance_traversed / (pbes['pbes']['event_duration_in_ms'] / 1000)
pbes['pbes']['replay_speed_2'] = replay_speed
np.testing.assert_array_almost_equal(pbes['pbes']['replay_speed'], pbes['pbes']['replay_speed_2'], decimal=1)
pbes['pbes'] = pbes['pbes'].drop('replay_speed_2', axis=1)


# --- TO NUMERIC --------------------------------------------------------------

cols = ['x_pos', 'y_pos', 'replay_score', 'traj_speed',
        'av_running_speed', 'max_jump_dist', 'mean_jump_dist',
        'x_pos_dec_start', 'x_pos_dec_end', 'y_pos_dec_start', 'y_pos_dec_end',
        'n_bins_traversed', 'dist_traversed_cm', 'compression_factor']

for col in cols:
    pbes['pbes'][col] = pd.to_numeric(pbes['pbes'][col])


cols = ['p_val', 'obs_score', 'surr_mean', 'z_score']

for col in cols:
    pbes['sig_df'][col] = pd.to_numeric(pbes['sig_df'][col])

# --- REINDEX DATAFRAMES WITH UNIQUE PBE IDS ----------------------------------

pbes['pbes'] = reindex_pbes_dfs(pbes['pbes'])
pbes['sig_df'] = reindex_pbes_dfs(pbes['sig_df'])
# pbes['trajectories'] = reindex_pbes_dfs(pbes['trajectories'])


# --- SAVE OUTPUT -------------------------------------------------------------

if not os.path.isdir(output_folder):
    os.makedirs(output_folder)

output_file = os.path.join(output_folder,'decoded_PBEs_sess_{:02}_{}_{}.pkl'.format(sess_ind, dec_area, epoch))

if save_results:
    print('Saving output to {}'.format(output_file))
    pickle.dump(pbes, open(output_file, 'wb'))
