import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(sys.path[0]))))
import os
import neo
import numpy as np
import quantities as pq
import elephant
import scipy.signal
from loadmat import loadmat
import pickle
import scipy.io

try:
    __IPYTHON__
except NameError:
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sess_ind', type=int)
    parser.add_argument('-d', '--data_folder', type=str,
                        default='/media/pietro/bigdata/neuraldata/touch&see/')
    parser.add_argument('-n', '--neo_data_folder', type=str,
                        default='/media/pietro/bigdata/neuraldata/ts_replay/original_neo/')
    parser.add_argument('-t', '--save_to', type=str, default='.')
    parser.add_argument('-k', '--kernel_sigma', type=int, default=20)
    parser.add_argument('-p', '--sampling_period', type=int, default=10)
    parser.add_argument('-a', '--area', type=str)

    parser.add_argument('-m', '--time_selection_mode', type=str,
                        choices=['crop', 'align'], default='crop')

    # if time selection mode is align we pick an event and a time
    # before and after
    parser.add_argument('-e', '--align_event', type=str, default='Image on')
    parser.add_argument('-t1', '--time_before', type=float, default=10)
    parser.add_argument('-t2', '--time_after', type=float, default=10)

    # if the time selection mode is crop, we take two events
    parser.add_argument('-e1', '--trial_start_event', type=str,
                        default='Image on')
    parser.add_argument('-e2', '--trial_end_event', type=str,
                        default='Return to central arm')

    ARGS = parser.parse_args()

    sess_ind             = ARGS.sess_ind
    data_folder          = ARGS.data_folder
    neo_data_folder      = ARGS.neo_data_folder
    save_to              = ARGS.save_to
    kernel_sigma         = ARGS.kernel_sigma
    sampling_period      = ARGS.sampling_period
    area                 = ARGS.area
    time_selection_mode  = ARGS.time_selection_mode
    align_event          = ARGS.align_event.replace("_", " ") # turn underscores to spaces when passed from bash
    time_before          = ARGS.time_before
    time_after           = ARGS.time_after
    trial_start_event    = ARGS.trial_start_event.replace("_", " ")
    trial_end_event      = ARGS.trial_end_event.replace("_", " ")

else:
    sess_ind            = 5
    data_folder         = '/media/pietro/bigdata/neuraldata/touch&see/'
    neo_data_folder     = '/media/pietro/bigdata/neuraldata/ts_replay/original_neo/'
    save_to             = '/media/pietro/bigdata/neuraldata/ts_replay/test/'
    kernel_sigma        = 20
    sampling_period     = 10
    area                = 'both'
    time_selection_mode = 'align'
    align_event         = 'Image on'
    time_before         = 2
    time_after          = 2
    trial_start_event   = 'Image on'
    trial_end_event     = 'Return to central arm'

kernel_size       = kernel_sigma * 8

# --- LOAD SESSION BLOCK ------------------------------------------------------

F = loadmat(data_folder + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]
recording_day = session[0:10]
recording_time = session[11:]
file_name = 'TS_session_{}_{}_{}.pkl'.format(sess_ind + 1, rat, recording_day)
dir = os.path.join(neo_data_folder, rat, session)
bl = pickle.load(open(os.path.join(dir, file_name), 'rb'))

# --- LOAD UNIT TYPE ----------------------------------------------------------

U = loadmat(data_folder + 'unit.mat')
W = loadmat(data_folder + 'wave_class.mat')

unit_sess = U['unit']['sess'] - 1
all_unit_area = U['unit']['area']
unit_sess_ind = np.where(unit_sess == sess_ind)[0]

area_map = {'Hp'    : 'Hippocampus',
            '35'    : 'Perirhinal',
            '36'    : 'Perirhinal',
            'b3536' : 'Perirhinal'}

if area == 'both':
    sel = np.isin(all_unit_area[unit_sess_ind], ['Hp', '35', '36', 'b3536'])

elif area == 'Hippocampus':
    sel = np.isin(all_unit_area[unit_sess_ind], ['Hp'])

elif area == 'Perirhinal':
    sel = np.isin(all_unit_area[unit_sess_ind], ['35', '36', 'b3536'])


unit_sess_ind = unit_sess_ind[sel]
units_area = np.array([area_map[a] for a in all_unit_area[unit_sess_ind]])
is_hippocampus = np.array([1 if u == 'Hippocampus' else 0 for u in units_area])

neo_units_area = np.array([u.annotations['area'] for u in bl.list_units])

n_hc1 = neo_units_area[neo_units_area == 'Hippocampus'].shape[0]
n_pr1 = neo_units_area[neo_units_area == 'Perirhinal'].shape[0]

n_hc2 = units_area[units_area == 'Hippocampus'].shape[0]
n_pr2 = units_area[units_area == 'Perirhinal'].shape[0]

assert n_hc1 == n_hc2
assert n_pr1 == n_pr2

neuron_type_labels = W['R']['neuronTypeLabel']
neuron_type_code = W['R']['neuronType']-1
neuron_type = neuron_type_labels[neuron_type_code]

neuron_type_sess = neuron_type[unit_sess_ind]
not_interneuron   = [0 if t=='narrow_int' else 1 for t in neuron_type_sess]


# --- SET UP DICT WITH ALL PARAMETERS -----------------------------------------

pars = {'animal'              : rat,
        'day'                 : day,
        'session'             : session,
        'sess_ind'            : sess_ind,
        'area'                : area,
        'kernel_sigma'        : kernel_sigma,
        'kernel_size'         : kernel_size,
        'sampling_period'     : sampling_period,
        'time_selection_mode' : time_selection_mode,
        'align_event'         : align_event,
        'time_before'         : time_before,
        'time_after'          : time_after,
        'trial_start_event'   : trial_start_event,
        'trial_end_event'     : trial_end_event}

# --- SMOOTH SINGLE TRIALS ----------------------------------------------------

print('Smoothing Touch & See Session {}, area: {}'.format(sess_ind, area))

smoothed_trials, position, velocity   = [], [], []

kernel = scipy.signal.gaussian(kernel_size, kernel_sigma, sym=True)
kernel = 1000 * kernel / kernel.sum()

def interpolate_outputs(outputs, output_times, new_times):
    output_dim = outputs.shape[1]  # Number of output features
    outputs_binned = np.empty([new_times.shape[0], output_dim])  # Initialize matrix of binned outputs

    for k in range(output_dim):
        outputs_binned[:, k] = np.interp(new_times, output_times, outputs[:, k])
    return outputs_binned

for segment in bl.segments:

    if area == 'both':
        trains_hp = segment.filter(targdict={'area': 'Hippocampus'}, objects=neo.SpikeTrain)
        trains_pr = segment.filter(targdict={'area': 'Perirhinal'}, objects=neo.SpikeTrain)
        trains = trains_hp + trains_pr
    else:
        trains = segment.filter(targdict={'area': area}, objects=neo.SpikeTrain)

    if time_selection_mode == 'crop':
        t_start = [segment.events[0].times[i] for i in range(len(segment.events[0].labels))
                   if segment.events[0].labels[i] == trial_start_event][0]
        t_stop  = [segment.events[0].times[i] for i in range(len(segment.events[0].labels))
                   if segment.events[0].labels[i] == trial_end_event][0]

    elif time_selection_mode == 'align':
        t_event = [segment.events[0].times[i] for i in range(len(segment.events[0].labels))
                   if segment.events[0].labels[i] == align_event][0]
        t_start = t_event - time_before*pq.s
        t_stop  = t_event + time_after*pq.s


    bs = elephant.conversion.BinnedSpikeTrain(trains,
                                              binsize=1 * pq.ms,
                                              t_start=t_start.rescale(pq.ms),
                                              t_stop=t_stop.rescale(pq.ms))

    bin_centers = bs.bin_centers
    binned_spikes_trial = bs.to_array()
    binned_spikes_trial = np.clip(binned_spikes_trial, a_min=0, a_max=1)

    rate_array = np.apply_along_axis(
        lambda m: np.convolve(m, kernel, mode='same'),
        axis=1, arr=binned_spikes_trial)

    rate_array = rate_array[:, int(kernel_size / 2):-int(kernel_size / 2)]
    rate_array = rate_array[:, ::sampling_period]

    bin_centers = bin_centers[int(kernel_size / 2):-int(kernel_size / 2)]
    bin_centers = bin_centers[::sampling_period]

    assert bin_centers.shape[0] == rate_array.shape[1]


    smoothed_trials.append(rate_array)

    pos = segment.analogsignals[0]
    pos = interpolate_outputs(pos.__array__(), pos.times, bin_centers)
    vel = segment.analogsignals[1]
    vel = interpolate_outputs(vel.__array__(), vel.times, bin_centers)
    assert pos.shape[0] == rate_array.shape[1]
    assert vel.shape[0] == rate_array.shape[1]

    position.append(pos)
    velocity.append(vel)


#full_task_smooth = np.hstack(smoothed_trials)
assert np.isnan(np.hstack(smoothed_trials)).sum() == 0

# --- SAVE SMOOTHED SINGLE TRIALS ---------------------------------------------

if not os.path.isdir(save_to):
    os.makedirs(save_to)

out = {'data' : smoothed_trials,
       'position' : position,
       'velocity' : velocity,
       'pars' : pars,
       'neuron_type' : neuron_type_sess,
       'not_interneuron' : not_interneuron,
       'units_area' : units_area,
       'is_hippocampus' : is_hippocampus,
       'unit_sess_ind' : unit_sess_ind}

if area == 'Perirhinal':
    area_init = 'PR'
elif area == 'Hippocampus':
    area_init = 'HP'
elif area == 'both':
    area_init = 'HPPR'

# in the pickled version, trials are kept as a list
file_name = 'TS_task_s{}_{}'.format(sess_ind+1, area_init)
pickle.dump(out, open(os.path.join(save_to, '{}.pkl'.format(file_name)), 'wb'))

# for the mat file, we stack all trials together
out['data'] = np.hstack(smoothed_trials)
out['position'] = np.vstack(position)
out['velocity'] = np.vstack(velocity)
scipy.io.savemat(os.path.join(save_to, '{}.mat'.format(file_name)), out)

