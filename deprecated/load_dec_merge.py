import os
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *
from plotting_style import *

"""
Here we load the decoding results of two different area to figure out
which ones are good for both
"""


setting_names = ['july1', 'july1']
areas = ['Hippocampus', 'Perirhinal']


# PLOTTING PARS
plot_format = 'png'
dpi = 400
save_plots = True
# to determine which confusion matrices to plot
score_thresholds = [25, 0.7]

score_labels = ['dec_score_{}'.format(area_code_labels[area]) for area in areas]


# --- LOAD RESULTS ------------------------------------------------------------

dfs = []

for settings_name, area in zip(setting_names, areas):
    output_folder = os.path.join(REPLAY_FOLDER, 'results', 'dec',
                                 settings_name)
    output_file = 'dec_rs_{}_{}.pkl'.format(settings_name, area)
    out = pickle.load(open(os.path.join(output_folder, output_file), 'rb'))

    # load variables
    sess_info = out['sess_info']
    sess_info['sess_ind'] = sess_info.index
    dfs.append(sess_info)


sess1 = dfs[0][dfs[0][score_labels[0]] <= score_thresholds[0]].index
print('Session for Hippocampal replay: {}'.format(sess1.values))

sess2 = dfs[1][dfs[1][score_labels[1]] > score_thresholds[1]].index

merge = set(sess1).intersection(set(sess2))
print('Session for Hippocampal-Perirhinal replay match: {}'.format(merge))
