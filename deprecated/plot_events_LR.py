
if plot_events:

    scored_left = spikes[:, left_ind].copy()
    scored_left = scored_left * left_scores[np.newaxis, :]

    scored_right = spikes[:, right_ind].copy()
    scored_right = scored_right * right_scores[np.newaxis, :]

    scored_all = np.hstack((scored_left, scored_right))
    scored_all = np.ma.masked_where(scored_all == 0, scored_all)

    ipmat = ip[:, None].repeat(scored_all.shape[1], axis=1)
    scored_all_in_pbe = np.ma.masked_where(~ipmat, scored_all)
    scored_all_not_in_pbe = np.abs(np.ma.masked_where(ipmat, scored_all))
    proba_not_in_pbe = np.abs(np.ma.masked_where(ip[None, :], pbe_side_proba))

    cmap = plt.cm.RdBu
    cmap.set_bad(color='white')

    f = plt.figure()
    gs = gridspec.GridSpec(2, 1 ,height_ratios=[6, 1])

    ax1 = plt.subplot(gs[0])
    ax2 = plt.subplot(gs[1])

    f.subplots_adjust(right=0.75)

    im = ax1.imshow(scored_all_in_pbe.T, origin='lower', cmap=cmap,
                    aspect='auto', vmin=-0.5, vmax=0.5)
    im2 = ax1.imshow(scored_all_not_in_pbe.T, origin='lower', cmap='Greys',
                     aspect='auto', vmin=0, vmax=1)
    cax = f.add_axes([0.85, 0.15, 0.05, 0.7])
    cbar = f.colorbar(im, cax=cax ,ticks=[-0.5, 0, 0.5])
    ax1.axhline(scored_all.shape[1 ]- scored_right.shape[1] - 0.5,
                ls = ':', c='grey', alpha=0.7)
    cbar.ax.set_yticklabels \
        (['Left', '0', 'Right'])  # vertically oriented colorbar
    ax1.set_yticks(range(scored_all.shape[1]))
    ax1.set_yticklabels([])
    ax1.set_xticklabels([])
    ax1.set_ylabel('Neuron index (sorted)')
    ax2.set_ylabel('Decoded\nside')
    ax2.set_xlabel('Time bins')
    ax2.set_yticks([])
    ax2.imshow(proba_in_pbe, origin='lower', cmap=cmap,
               aspect='auto', vmin=-0.5, vmax=0.5)
    ax2.imshow(np.abs(proba_not_in_pbe), origin='lower', cmap='Greys',
               aspect='auto', vmin=-0, vmax=1)
    sns.despine(f)

    if save_plots:
        f.savefig(os.path.join(event_plots_folder, 'sess{:02}_pbe_{:03}.{}'.format(sess_ind, pbe_number, plots_format)), dpi=300)
