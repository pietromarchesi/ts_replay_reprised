
import numpy as np



traj = [-23, -25, -27, -29,  -2,  -4,  -6,  -8, -10, -12, -14, -17, -19,
       -21, -23]

side = 'left'

direction = 'positive'


flip_dict1 = {i : j for i, j in zip(np.arange(29, -1, -1), np.arange(-1, -30, -1))}

flip_dict2 = {i : j for i, j in zip(np.arange(-29, 1, 1), np.arange(1, 30, 1))}
flip_dict3 = {0:0}

flip_dict3.update(flip_dict1)
flip_dict3.update(flip_dict2)


if side == 'right' and direction == 'positive' and traj[0] >= traj[-1] :
    #print('case 1')
    ind = np.where(np.diff(traj) < 0)[0][0]

    for i in range(ind + 1) :
        traj[i] = flip_dict3[traj[i]]

elif side == 'right' and direction == 'negative' and traj[-1] >= traj[0] :
    #print('case 2')
    ind = np.where(np.diff(traj) > 0)[0][0]

    for i in range(ind + 1, len(traj)) :
        # print(i)
        traj[i] = flip_dict3[traj[i]]

elif side == 'left' and direction == 'positive' and traj[0] <= traj[-1] :
    #print('case 3')
    ind = np.where(np.diff(traj) > 0)[0][0]

    for i in range(ind + 1) :
        traj[i] = flip_dict3[traj[i]]

elif side == 'left' and direction == 'negative' and traj[-1] <= traj[0] :
    #print('case 4')
    ind = np.where(np.diff(traj) < 0)[0][0]

    for i in range(ind + 1, len(traj)) :
        # print(i)
        traj[i] = flip_dict3[traj[i]]

