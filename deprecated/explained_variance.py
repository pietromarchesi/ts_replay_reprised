import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(sys.path[0]))))
import numpy as np
import pickle
import neo
import pandas as pd
import matplotlib.pyplot as plt
from elephant.conversion import BinnedSpikeTrain
import quantities as pq
from replaydetector.DKW_decoder import DKW_decoder
from utils import get_decoding_data
from utils import coarsify_bins
from utils import get_session_block
from utils import merge_segments
from utils import bin_PBEs
from replaydetector.replaydetector import ReplayDetector
from constants import DATA_FOLDER, REPLAY_FOLDER
import quantities as pq

"""
Selection criteria:
- Only pyramidal neurons can be tweaked
- Neurons which fire at least n spikes in total

"""

# --- PARAMETERS --------------------------------------------------------------

sess_ind           = 6
data_folder        = DATA_FOLDER
replay_folder      = REPLAY_FOLDER
epoch              = 'task'
area_1             = 'Hippocampus'
area_2             = 'Perirhinal'
binsize_in_ms      = 200
correlation_method = 'pearson'
min_spikes         = 10
pyramidal_only     = True


# --- LOAD DATA ---------------------------------------------------------------
areas  = [area_1, area_2]
epochs = ['task', 'pre_sleep', 'post_sleep']

data = {a : {} for a in [area_1, area_2]}

for area in areas:
    for epoch in epochs:
        bl = get_session_block(data_folder, sess_ind, epoch)
        if epoch == 'task':
            segment = merge_segments(bl.segments[:-1], bl.list_units)

        elif epoch == 'pre_sleep':
            segment = bl.segments[0]
            assert segment.name == 'pre-sleep'

        elif epoch == 'post_sleep':
            segment = bl.segments[1]
            assert segment.name == 'post-sleep'

        trains = segment.filter(targdict={'area': area}, objects=neo.SpikeTrain)

        if pyramidal_only:
            trains = [train for train in trains if train.annotations['is_pyramidal']]

        if correlation_method == 'pearson':
            bs = BinnedSpikeTrain(trains,binsize=binsize_in_ms * pq.ms,
                                  t_start=segment.t_start.rescale(pq.ms),
                                  t_stop=segment.t_stop.rescale(pq.ms))
            data[area][epoch] = bs.to_array()


for area in areas:
    min_spikes_task = data[area]['task'].sum(axis=1) > min_spikes
    min_spikes_pre  = data[area]['pre_sleep'].sum(axis=1) > min_spikes
    min_spikes_post = data[area]['post_sleep'].sum(axis=1) > min_spikes
    min_spikes_mask = np.logical_and(min_spikes_task,
                                     np.logical_and(min_spikes_pre, min_spikes_post))

    for epoch in epochs:
        data[area][epoch] = data[area][epoch][min_spikes_mask, :]

corr_matrices = {}

for epoch in epochs:

    spikes_1    = data[area_1][epoch]
    spikes_2    = data[area_2][epoch]
    n_neurons_1 = spikes_1.shape[0]
    n_neurons_2 = spikes_2.shape[0]

    cm = np.zeros([n_neurons_1, n_neurons_2])

    for i in range(n_neurons_1):
        for j in range(n_neurons_2):
            c = np.corrcoef(spikes_1[i, :], spikes_2[j, :])[0, 1]
            if np.isnan(c):
                raise ValueError
            else:
                cm[i,j] = c
    corr_matrices[epoch] = cm


f, ax = plt.subplots(1, 3, figsize=[9, 3])
for i, epoch in enumerate(epochs):
    ax[i].imshow(corr_matrices[epoch])
    ax[i].set_ylabel(area_1)
    ax[i].set_xlabel(area_2)
    ax[i].set_title(epoch)
plt.tight_layout()


# --- COMPUTE EXPLAINED VARIANCE ----------------------------------------------

def r(cm1, cm2):
    return np.corrcoef(cm1.flatten(), cm2.flatten())[0, 1]


def compute_EV(corr_matrices, REV=False):
    pre_sleep_mat  = corr_matrices['pre_sleep']
    task_mat       = corr_matrices['task']
    post_sleep_mat = corr_matrices['post_sleep']

    if REV:
        post_sleep_mat = corr_matrices['pre_sleep']
        pre_sleep_mat  = corr_matrices['post_sleep']

    r_TR2  = r(task_mat, post_sleep_mat)
    r_TR1  = r(task_mat, pre_sleep_mat)
    r_R1R2 = r(pre_sleep_mat, post_sleep_mat)
    numerator = r_TR2 - r_TR1*r_R1R2
    denominator = np.sqrt((1-r_TR1**2)*(1-r_R1R2**2))
    EV =  (numerator / denominator) ** 2
    return EV


EV = compute_EV(corr_matrices)
REV = compute_EV(corr_matrices, REV=True)
print(EV)
print(REV)