#!/usr/bin/env bash

# Prepare a set of sessions for analysis.
# This means smoothing and sub-sampling data from the task phase.

sessions=( 0 1 2 3 4 5 6 7 8 14 15 )
data_folder=/media/pietro/bigdata/neuraldata/touch\&see/
neo_data_folder=/media/pietro/bigdata/neuraldata/ts_replay/original_neo/
save_to=/media/pietro/bigdata/neuraldata/ts_replay/sessions/taskzoom2/

kernel_sigma=20
sampling_period=2
area=Hippocampus  #Hippocampus, Perirhinal, both
time_selection_mode="align"
align_event="Image_on" # pass spaces as underscores
time_before=1
time_after=2

for s in "${sessions[@]}"
do
    python prepare_sess_task_neo.py -s $s \
                            -d $data_folder\
                            -i 0 \
                            -t $neo_data_folder

    python prepare_sess_task_smooth.py -s $s \
                            -d $data_folder \
                            -n $neo_data_folder \
                            -t $save_to \
                            -k $kernel_sigma \
                            -p $sampling_period \
                            -a $area \
                            -m $time_selection_mode \
                            -e $align_event \
                            -t1 $time_before \
                            -t2 $time_after

done
