import numpy as np

start_bin = -1
end_bin = 5
side = 'right'

if end_bin > start_bin:
    replay_direction = 'positive'
else:
    replay_direction = 'negative'

if start_bin > 29:
    start_bin = start_bin - 29
if end_bin > 29:
    end_bin = end_bin - 29
if start_bin < 0:
    start_bin = np.arange(29+1)[start_bin]
if end_bin < 0:
    end_bin = np.arange(29+1)[end_bin]

print(start_bin, end_bin)


start_bin = -1
end_bin = 1
side = 'left'

if end_bin < start_bin:
    replay_direction = 'positive'
else:
    replay_direction = 'negative'


if start_bin < -29:
    start_bin = start_bin + 29 + 1
if end_bin < -29:
    end_bin = end_bin + 29 + 1
if start_bin > 0:
    start_bin = np.arange(-29-1, 0)[start_bin]
if end_bin > 0:
    end_bin = np.arange(-29-1, 0)[end_bin]

print(start_bin, end_bin, replay_direction)



def replay_wrap_bins_and_direction(side, start_bin, end_bin):
    """
    The replay detector will hand us bins which are not necessarily between
    (0, 29) for right side pbes and (-29, 0) for left side pbes. This is
    because of the wrapping that we do when fitting trajectory lines. The
    difference between these bins allows us to calculate how many bins the
    trajectory spans, thus we can easily compute a trajectory speed, as is
    done in replaydetector. However, when we look at for example where the
    trajectories start and end, we need to bring back these additional bins
    to the standard ranges (i.e. 'wrap' them). By doing that, however, we lose
    the information about whether the trajectory goes in the same direction
    as a normal trial run or it goes backwards, so we also determine that
    (which is also a bit different for the two pbe sides because of how the
    bins are defined).
    """

    if side == 'right':

        if end_bin > start_bin:
            replay_direction = 'positive'
        else:
            replay_direction = 'negative'

        if start_bin > 29:
            start_bin = start_bin - 29
        if end_bin > 29:
            end_bin = end_bin - 29
        if start_bin < 0:
            start_bin = np.arange(29 + 1)[start_bin]
        if end_bin < 0:
            end_bin = np.arange(29 + 1)[end_bin]

    elif side == 'left':

        if end_bin < start_bin:
            replay_direction = 'positive'
        else:
            replay_direction = 'negative'

        if start_bin < -29:
            start_bin = start_bin + 29 + 1
        if end_bin < -29:
            end_bin = end_bin + 29 + 1
        if start_bin > 0:
            start_bin = np.arange(-29, 0)[start_bin]
        if end_bin > 0:
            end_bin = np.arange(-29, 0)[end_bin]

    return start_bin, end_bin, replay_direction