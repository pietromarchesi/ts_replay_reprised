#!/usr/bin/env bash

sessions=( 0 1 2 3 4 5 6 7 8 14 15 )
data_folder=/media/pietro/bigdata/neuraldata/touch_see/
neo_data_folder=/media/pietro/bigdata/neuraldata/ts_replay/original_neo/
save_to=/media/pietro/bigdata/neuraldata/ts_replay/sessions/
kernel_sigma=200
sampling_period=10
area=Perirhinal  #Hippocampus, Perirhinal, both

for s in "${sessions[@]}"
do
    python prepare_sess_task_neo.py -s $s \
                            -d $data_folder\
                            -i 0 \
                            -t $neo_data_folder

    python prepare_sess_task_smooth.py -s $s \
                            -d $data_folder \
                            -n $neo_data_folder \
                            -t $save_to \
                            -k $kernel_sigma \
                            -p $sampling_period \
                            -a $area

    python prepare_sess_sleep.py -s $s \
                            -d $data_folder \
                            -t $save_to \
                            -k $kernel_sigma \
                            -p $sampling_period \
                            -e pre \
                            -a $area

    python prepare_sess_sleep.py -s $s \
                            -d $data_folder \
                            -t $save_to \
                            -k $kernel_sigma \
                            -p $sampling_period \
                            -e post \
                            -a $area
done
