import os
import numpy as np
from loadmat import loadmat
import pickle
import pandas as pd
import elephant
import quantities as pq
import neo
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix
from utils import get_trajectory_side
from sklearn.preprocessing import LabelEncoder, LabelBinarizer
from constants import DATA_FOLDER, REPLAY_FOLDER
from plotting_utils import combine_pbes_two_areas
import matplotlib.gridspec as gridspec
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from utils import is_on_central_arm


"""
Load decoded PBEs of two areas and see whether they are decoded to the same
side of the maze. 

If the decoding is done on the same events, the match_mode should be set
to 'same_events' (then we just make sure that a given PBE was successfully
decoded in both areas). If PBEs are different for the two areas, we need a way
to pair them, and there are three possible modes:

- nearest_start: find the PBE in area 2 which has the start time closest to
the start time of the PBE of area 1 

- nearest_start_following: find the PBE in area 2 which has the start time 
closest to the start time of the area 1 PBE, constrained to start AFTER the
PBE in area 1

- nearest_start_preceding: same as previous, only PBEs in area 2 which precede
their match in area 1

"""


sessions                = sessions_HC_PR_replay
epoch                   = 'task'
PBEs_settings_names     = ['july8', 'july8']
PBEs_areas              = ['Hippocampus', 'Hippocampus']
dec_settings_names      = ['dkwjuly4', 'logreg']
dec_areas               = ['Hippocampus', 'Perirhinal']

match_mode              = 'same_events'
max_time_diff_in_ms     = 2000
data_folder             = DATA_FOLDER
replay_folder           = REPLAY_FOLDER


shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle',
                 'unit_identity_shuffle']
p_vals = [0.1, 0.2, 0.3]


save_plots          = True
plots_format        = 'png'
dpi                 = 400



if (PBEs_settings_names[0] == PBEs_settings_names[1]) and match_mode != 'same_events':
    raise ValueError('Match mode is set to {} but the PBE settings are the'
                     'same!'.format(match_mode))


plots_folder = os.path.join(REPLAY_FOLDER, 'plots', 'match_areas')


if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)


# --- LOAD PBEs ---------------------------------------------------------------

pbes = {}
sig_df = []

for pbe_set, pbe_area, dec_set, dec_area in zip(PBEs_settings_names, PBEs_areas,
                                                dec_settings_names, dec_areas):

    pbes[dec_area] = []

    for sess_ind in sessions:
        decoded_PBEs_folder = os.path.join(replay_folder, 'results', 'decoded_PBEs',
                                     'pbe_setting_{}_{}'.format(pbe_set, pbe_area),
                                     'dec_setting_{}_{}'.format(dec_set, dec_area))
        PBEs_file = os.path.join(decoded_PBEs_folder, 'decoded_PBEs_sess_{:02}_{}_{}.pkl'.format(sess_ind, dec_area, epoch))
        pbe_data_area = pickle.load(open(PBEs_file, 'rb'))
        pbes[dec_area].append(pbe_data_area['pbes'])
        if dec_area == 'Hippocampus':
            sig_df.append(pbe_data_area['sig_df'])

    pbes[dec_area] = pd.concat(pbes[dec_area])

sig_df = pd.concat(sig_df)

# --- FILTER PBEs -------------------------------------------------------------

sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types, p_vals=p_vals)
pbes['Hippocampus'] = pbes['Hippocampus'][np.isin(pbes['Hippocampus'].index, sel_pbes_sig)]


for i, row in pbes['Hippocampus'].iterrows():
    if is_on_central_arm(row['dec_start_bin']) and is_on_central_arm(row['dec_end_bin']):
        pbes['Hippocampus'].loc[i, 'pbe_side'] = 'central'

pbes['Hippocampus'] = pbes['Hippocampus'][pbes['Hippocampus']['pbe_side'] != 'central']
# --- Match events ------------------------------------------------------------

pbes_match = {dec_areas[0]: [],
              dec_areas[1]: []}

if match_mode == 'same_events':
    events = pbes[dec_areas[0]].index.intersection(pbes[dec_areas[1]].index).__array__()
    pbes_match[dec_areas[0]] = list(events)
    pbes_match[dec_areas[1]] = list(events)

else:
    for a1_ind, pbe in pbes[dec_areas[0]].iterrows():
        # otherwise match
        print(a1_ind)
        a1_start = pbe['start_time_in_ms']
        a1_end   = pbe['start_time_in_ms']
        a2_start = pbes[dec_areas[1]]['start_time_in_ms']

        if match_mode == 'nearest_start':
            diff = a2_start - a1_start
            a2_ind = diff.abs().argmin()
            print(a2_ind)

        elif match_mode == 'nearest_start_following':
            diff = a2_start - a1_start
            try:
                a2_ind = diff[diff > 0].argmin()
            except ValueError:
                print('No PBEs in area 2 following PBE {} of area 1: skipping'.format(a1_ind))
                continue

        elif match_mode == 'nearest_start_preceding':
            diff = a2_start - a1_start
            try:
                a2_ind = np.abs(diff[diff < 0]).argmin()
            except ValueError:
                print('No PBEs in area 2 preceding PBE {} of area 1: skipping'.format(a1_ind))
                continue

        time_diff = pbes[dec_areas[1]].loc[a2_ind, 'start_time_in_ms'] - a1_start
        if time_diff < max_time_diff_in_ms:
            pbes_match[dec_areas[0]].append(a1_ind)
            pbes_match[dec_areas[1]].append(a2_ind)


# --- FILTER PBEs -------------------------------------------------------------
# potentially filter pbes based on replay scores or whatnot


# --- EXTRACT SIDE AND BUILD CONFUSION MATRIX ---------------------------------

a1_side = pbes[dec_areas[0]].loc[pbes_match[dec_areas[0]], 'pbe_side']
a2_side = pbes[dec_areas[1]].loc[pbes_match[dec_areas[1]], 'pbe_side']

cm = confusion_matrix(a1_side, a2_side)
cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

cm_shuf = np.zeros([2, 2])
for ii in range(50):
    cm_shuf_boot = confusion_matrix(a1_side, np.random.permutation(a2_side))
    cm_shuf_boot = cm_shuf_boot.astype('float') / cm_shuf_boot.sum(axis=1)[:, np.newaxis]
    cm_shuf = cm_shuf + cm_shuf_boot
cm_shuf = cm_shuf / 50

cm = cm - cm_shuf

f, ax = plt.subplots(1, 1)
sns.heatmap(cm,
            xticklabels=['Left', 'Right'],
            yticklabels=['Left', 'Right'],
            ax=ax, square=True,
            annot=True,
            cmap='Greens',
            cbar=False)
ax.set_ylabel('')
ax.set_yticks([])
ax.set_xticks([])
ax.set_ylabel('{} decoded side'.format(dec_areas[0]))
ax.set_xlabel('{} decoded side'.format(dec_areas[1]))
ax.set_title('All sessions ({} PBEs)'.format(a1_side.shape[0]))


f, ax = plt.subplots(1, 1)
ax.plot(np.array(a1_side))
ax.plot(np.array(a2_side))

#ax.set_title('Sess. {} ({} PBEs)'.format(sess_ind, a1_side.shape[0]))

if save_plots:
    plot_name = 'Conf_matrix_{}_{}_{}_session_{}.{}'.format(dec_areas[0],
                                                            dec_areas[1],
                                                            match_mode,
                                                            sess_ind,
                                                            plots_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)



# if False:
#     area1_side_agg, area2_side_agg = [], []
#
#     for sess_ind in sessions:
#         pbes_sel = pbes[sess_ind]
#         area1_side = pbes_sel['side_{}'.format(dec_areas[0])]
#         area2_side = pbes_sel['side_{}'.format(dec_areas[1])]
#         area1_side_agg.append(area1_side)
#         area2_side_agg.append(area2_side)
#
#
#
#     f = plt.figure(figsize=(7*2, 3*2))
#     gs = gridspec.GridSpec(3, 7)
#     ax = plt.subplot(gs[0:3, 0:3])
#
#     cm = confusion_matrix(np.hstack(area1_side_agg), np.hstack(area2_side_agg))
#     cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
#
#     sns.heatmap(cm,
#                 xticklabels=['Left', 'Right'],
#                 yticklabels=['Left', 'Right'],
#                 ax=ax, square=True,
#                 annot=True,
#                 cmap='Greens')
#     ax.set_ylabel('{} decoded side'.format(dec_areas[0]))
#     ax.set_xlabel('{} decoded side'.format(dec_areas[1]))
#
#     plt_ind_mat = np.indices((3, 4))
#     plt_ind = list(zip(plt_ind_mat[0].flatten(), plt_ind_mat[1].flatten()+3))
#
#
#     for i, (sess_ind, area1_side, area2_side) in enumerate(zip(sessions,
#                                                              area1_side_agg,
#                                                              area2_side_agg)):
#         ax = plt.subplot(gs[plt_ind[i][0], plt_ind[i][1]])
#
#         if len(area1_side) > 0 and len(area2_side) > 0:
#
#             cm = confusion_matrix(area1_side, area2_side)
#             cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
#
#             sns.heatmap(cm,
#                         xticklabels=['Left', 'Right'],
#                         yticklabels=['Left', 'Right'],
#                         ax=ax, square=True,
#                         annot=True,
#                         cmap='Greens',
#                         cbar=False)
#             ax.set_ylabel('')
#             ax.set_yticks([])
#             ax.set_xticks([])
#             ax.set_xlabel('Sess. {} ({} PBEs)'.format(sess_ind, area1_side.shape[0]))
#         else:
#             ax.axis('off')
#     plot_name = 'Conf_matrix_{}_{}_{}.{}'.format(dec_areas[0], dec_areas[1], match_mode, plots_format)
#     f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)
#
#
