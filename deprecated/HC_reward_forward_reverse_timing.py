import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from plotting_style import *
from utils import load_bin_centers
from utils import prepare_bins
from scipy.stats import wilcoxon, mannwhitneyu
from utils import *
import itertools
import scipy
from numpy import median
from utils import bin_occurrences_for_chi_square_test, linearize_trajectory
from statsmodels.stats.proportion import proportions_ztest


data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'
#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]

group_iti_and_return = True

# PLOTTING PARAMETERS

save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, 'HC_replay')


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

# df = load_replay(sessions=sessions,
#                  PBEs_settings_name=PBEs_settings_name,
#                  PBEs_area=PBEs_area,
#                  data_version=data_version,
#                  dec_settings_name=dec_settings_name,
#                  dec_area=dec_area,
#                  min_pbe_duration_in_ms=min_pbe_duration_in_ms,
#                  shuffle_types=shuffle_types,
#                  shuffle_p_vals=shuffle_p_vals,
#                  group_iti_and_return=group_iti_and_return)
#
#
# if group_iti_and_return:
#     phases = ['iti', 'img', 'run', 'reward']
# else:
#     phases = ['img', 'iti', 'return', 'reward', 'run']
#
#
# df = add_correct_incorrect(df, data_version=data_version)

df = pd.read_csv(os.path.join(REPLAY_FOLDER, 'df_final_selection.csv'))


# --- MAKE EARLY REWARD --------------------------------------------------------

df = df[df['phase'] == 'reward']

rew_times = []
for sess_ind in sessions:
    bl = get_session_block(DATA_FOLDER, sess_ind, 'task', data_version=data_version)
    for seg in bl.segments:
        ind = np.where(seg.events[0].labels == 'Reward')[0][0]
        rewt = seg.events[0].times[ind]
        rew_times.append(rewt.rescale(pq.ms))
rew_times = np.array(rew_times) * pq.ms

df['time_after_reward'] = None
for i, row in df.iterrows():

    tstart = row['start_time_in_ms'] * pq.ms

    try:
        rew_before = rew_times[rew_times < tstart].max()
        diff = (tstart - rew_before).rescale(pq.ms)
    except ValueError:
        diff = np.nan
    df.loc[i, 'time_after_reward'] = diff



df_sel = df[df['current_trial_outcome'] == 'correct']
df_sel = df_sel[df_sel['time_after_reward'] < 20000]

f, ax = plt.subplots(figsize=[10, 10])
sns.distplot(df_sel[df_sel['replay_direction'] == 'positive']['time_after_reward'],
             hist=True, kde=False, norm_hist=True,
             color=replay_direction_palette['positive'])
sns.distplot(df_sel[df_sel['replay_direction'] == 'negative']['time_after_reward'],
             hist=True, kde=False, norm_hist=True,
             color=replay_direction_palette['negative'])
ax.set_xlabel('Spike position in RE')
sns.despine()

plt.tight_layout()
#sns.distplot(data=dg, y='perc_tim

x = df_sel[df_sel['replay_direction'] == 'positive']['time_after_reward']
y = df_sel[df_sel['replay_direction'] == 'negative']['time_after_reward']
print('Median time from rew. start for forward and reverse events: {:.2f}s, {:.2f}s'
      ''.format(x.median()/1000, y.median()/1000))
stat, p = mannwhitneyu(x, y)
print('Mann-whitney U-test pval={:.1g}'.format(p))


