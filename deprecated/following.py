import os
from constants import *
import pickle
import statsmodels.stats.descriptivestats
from statsmodels.stats.multitest import multipletests
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
from plotting_style import *
from sklearn.preprocessing import MinMaxScaler
from scipy.ndimage import gaussian_filter1d
from utils import *

ratemap_settings = 'may11_DecSet2_smooth2_morelages'
#ratemap_settings = 'feb10_DecSet26_taskphase'
ratemap_area = 'Barrel'

save_plots = True
plot_format = 'png'
dpi = 400

stat_test = 'sign_test' #'sign_test', 't-test'
sign_alpha = 0.01
p_val_correction = 'fdr_bh'
variables = ['percentile_rank', 'corrcoef_debiased']  # ['corrcoef_obs', 'corrcoef_debiased']
test_values = [50, 0] # [0, 0]
nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
cols_to_check = ['phase', 'direction', 'has_ripple', 'null_method']
subselect_sessions = None
null_dist_method_for_plot_both_shuff = 'rotate_spikes'


# PEAK TIME DISTRIBUTION
p_val_thr_peak_time_dist = 0.00001


subselect_shifts = False
min_shift = -8
max_shift = 8

# --- SET UP PATHS ------------------------------------------------------------
plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# --- LOAD RESULTS ------------------------------------------------------------
res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)
file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
df = res['df']
pars = res['pars']
shift_amounts = pars['shift_amounts']
smoothing_sigma = pars['smooth_sigma']


if subselect_sessions is not None:
    df = df[np.isin(df['sess_ind'], subselect_sessions)]

if subselect_shifts:
    df = df[df['shift_amount'] >= min_shift]
    df = df[df['shift_amount'] <= max_shift]
    shift_amounts = np.arange(min_shift, max_shift+1)
df['area'] = ratemap_area
df['unit_id'] = ['{}_{}_{}'.format(r['sess_ind'], r['unit'], r['area']) for i, r in df.iterrows()]




p_val_thr_rateplots = 0.05
bins = np.arange(pars['shift_amounts'][0]*10, pars['shift_amounts'][-1]*10+1, 20)


for variable in ['corrcoef_debiased']:
    for direction in ['all', 'positive']:
        for null_distribution_method in nulldistmet_plots:

            dfx = df[(df['phase'] == 'all') &
                     (df['direction'] == direction) &
                     (df['has_ripple'] == 'all') &
                     (df['null_method'] == null_distribution_method)]

            dfx = dfx[(dfx['shift_amount'] >= -10) & (dfx['shift_amount'] < 10)]
            shift_amounts = np.unique(dfx['shift_amount'])
            dfx = dfx.reset_index().drop('index', axis=1)
            # dfsel = dfx.sort_values(variable, ascending=False).drop_duplicates(['sess_ind', 'unit'])
            # dfsel['shift_amount_ms'] = dfsel['shift_amount'] * 10
            # peak_shifts = dfsel['shift_amount_ms']
            #

            peak_shifts = []
            for unit_id in dfx['unit_id'].unique() :
                y = dfx[dfx['unit_id'] == unit_id].sort_values(by='shift_amount')[variable].values
                peak_shifts.append(shift_amounts[y.argmax()]*10)
            peak_shifts = np.array(peak_shifts)
            peak_shifts = peak_shifts[(peak_shifts >= -90) & (peak_shifts < 90) ]
            f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
            sns.distplot(peak_shifts, hist=True, bins=bins,
                         kde=False, norm_hist=True,
                         hist_kws={'ec':'black', 'rwidth':0.8,
                                   'facecolor':area_palette[ratemap_area]})
            for patch in ax.patches:
                patch.set_edgecolor('white')

            plt.tight_layout()
            ax.set_xlabel('Time of peak coordination\nof individual units (ms)')
            ax.set_ylabel('Density')
            ax.set_xticks([-100, -50, 0, 50, 100])
            ax.set_xticklabels([-100, -50, 0, 50, 100])
            #ax.set_xlim([shift_amounts[0]*10+10, shift_amounts[-1]*10-10])
            sns.despine()
            plt.tight_layout()

            if save_plots :
                plot_name = 'distribution_of_peak_times_of_highest_{}_{}_{}_{}.' \
                            '{}'.format(variable, ratemap_area,
                                        null_distribution_method, direction, plot_format)
                f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
