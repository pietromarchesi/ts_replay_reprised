import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(sys.path[0]))))
import neo
import pickle
import numpy as np
import distutils.util
import quantities as pq
import matplotlib.pyplot as plt
from loadmat import loadmat, nan_helper
import warnings
from constants import iPix2Cm

try:
    from utils import clean_position_segment
    can_clean_position = True
except FileNotFoundError:
    print('Could not import position cleaning function, probably the maze'
          'file cannot be found')
    can_clean_position = False

'''
This is the script used to package the data following the Neo format. 
'''


### PARAMETERS ###

try:
    __IPYTHON__
except NameError:
    print("Running not in IPython")
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sess_ind', type=int)
    parser.add_argument('-d', '--data_folder', type=str)
    parser.add_argument('-t', '--save_to', type=str, default='.')

    ARGS = parser.parse_args()

    sess_ind             = ARGS.sess_ind
    data_folder          = ARGS.data_folder
    save_to              = ARGS.save_to
    save_block           = True
    save_as              = 'pickle'

else:
    print("Running in IPython interactive console")
    data_folder          = '/media/pietro/bigdata/neuraldata/touch_see/'
    sess_ind             = 1 # Python index - sess_ind 0 is session 1
    save_block           = True
    save_as              = 'pickle'
    save_to              = '/media/pietro/bigdata/neuraldata/touch_see/neo/'


print('\n\n\nPackaging Touch & See Session {}'.format(sess_ind))

### EXTRACTING AND PREPARING THE DATA FROM ORIGINAL FILES ###
F = loadmat(data_folder + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]

recording_day = session[0:10]
recording_time = session[11:]

session_path = os.path.join(data_folder, 'Sleepdata', rat, day, session, 'sleepdata.mat')
session_path_sws = os.path.join(data_folder, 'SWSPeriods', rat, day, session, 'spikeTrl.mat')

unit_path = os.path.join(data_folder, 'unit.mat')
wave_class_path = os.path.join(data_folder, 'wave_class.mat')

Sdata = loadmat(session_path)
S = loadmat(session_path_sws)


spike_trains_pre = S['spikeTrlPre']['timestamp']  / 1e6
