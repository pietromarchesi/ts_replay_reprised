import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from utils import load_decoded_PBEs
from session_selection import sessions_HC_replay
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *

data_version       = 'dec16'

PBEs_settings_name = 'dec19k20'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'
PBEs_area          = 'Hippocampus'

#sessions           = [8]#sessions_HC_replay
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]

min_pbe_duration_in_ms = 50
epochs             = ['task']
group_iti_and_return = True


shuffle_p_vals = [0.05, 0.05]
shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']


# --- LOAD DATA ---------------------------------------------------------------


pbes_dfs, sig_dfs = [], []

for sess_ind in sessions:
    print(sess_ind)
    for epoch in epochs:
        try:
            pbes = load_decoded_PBEs(sess_ind, PBEs_settings_name, PBEs_area,
                                     data_version, dec_settings_name, dec_area,
                                     load_light=True)
            pbes_dfs.append(pbes['pbes'])
            sig_dfs.append(pbes['sig_df'])
        except FileNotFoundError:
            print('No decoded PBEs for session {}'.format(sess_ind))

df = pd.concat(pbes_dfs)
sig_df = pd.concat(sig_dfs)
#df = add_ripples_to_pbes_df(df)

if group_iti_and_return:
    df['phase'] = df['phase'].replace(to_replace='return', value='iti')

df = df[df['event_duration_in_ms'] >= min_pbe_duration_in_ms]


# --- STATISTICS ---------------------------------------------------------------

sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types,
                                        p_vals=shuffle_p_vals)
df_not_sig = df[~np.isin(df.index, sel_pbes_sig)]
df_sig = df[np.isin(df.index, sel_pbes_sig)]


print('')


# --- 1. BOXPLOT OF P-VALUES BY SHUFFLE TYPE ----------------------------------
f, ax = plt.subplots(1, 1)
sns.boxplot(data=sig_df, x='shuffle_type', y='p_val', ax=ax)
sns.despine()


# --- 2. CUMULATIVE DISTRIBUTION BY SHUFFLE TYPE ------------------------------
f, ax = plt.subplots(1, 1)
ax.set_xscale('log')

for shuffle_type in shuffle_types:
    pvals = sig_df.loc[sig_df['shuffle_type'] == shuffle_type, 'p_val']
    # plt.hist(pvals, normed=True, cumulative=True, label=shuffle_type,
    #          histtype='step', alpha=0.8, color='k', bins=1000)

    values, base = np.histogram(pvals, bins=100)
    #evaluate the cumulative
    cumulative = np.cumsum(values) / pvals.shape[0]
    # plot the cumulative function
    ax.plot(base[:-1], cumulative, label=shuffle_type)
ax.legend()
sns.despine()


# --- 3. PLOT NUMBER OF PBEs AND PERCENTAGE SIGNIFICANT FOR EACH SHUFFLE ------
# per session


f, ax = plt.subplots(1, 1+len(shuffle_types), sharey=False, figsize=[8, 3])
sns.countplot(data=df, x='sess_ind', ax=ax[0],
              color=sns.xkcd_rgb['light grey blue'])

for i, shuffle_type, p_val_threshold in enumerate(shuffle_types, shuffle_p_vals):

    dfsel = sig_df[sig_df['shuffle_type'] == shuffle_type]
    counts = dfsel.groupby('sess_ind')['sess_ind'].value_counts().values

    sigsel = dfsel[dfsel['p_val'] <= p_val_threshold]
    sig_counts = sigsel.groupby('sess_ind')['sess_ind'].value_counts().values

    perc = 100* sig_counts / counts

    sns.barplot(x=sessions, y=perc, ax=ax[i+1],
                color=sns.xkcd_rgb['light grey blue'])
    ax[i+1].set_ylabel('% PBEs significant for\n{}'.format(shuffle_type))
    ax[i+1].set_ylim([0, 100])

for axx in ax:
    axx.set_xlabel('Session')
ax[0].set_ylabel('# PBEs')
sns.despine()
plt.tight_layout()


# # --- PLOT PERCENTAGE AND ABSOLUTE NUMBER OF PBEs SIG. FOR ALL SHUFFLES -------
#
# p_vals = [0.1] * len(shuffle_types)
#
# f, ax = plt.subplots(1, 2, sharey=False, figsize=[6, 3])
# filtered_id = filter_pbes_significance(sig_df, shuffles=shuffle_types, p_vals=p_vals)
# sel_df = df.loc[filtered_id]
#
# counts = df.groupby('sess_ind')['sess_ind'].value_counts().values
# sig_counts = sel_df.groupby('sess_ind')['sess_ind'].value_counts().values
# perc = 100 * sig_counts / counts
# sns.barplot(x=sessions, y=perc, ax=ax[0], color=sns.xkcd_rgb['light grey blue'])
# ax[0].set_ylabel('% of significant PBEs\n(all shuffles)')
# ax[0].set_ylim([0, 100])
# sns.barplot(x=sessions, y=sig_counts, ax=ax[1],  color=sns.xkcd_rgb['light grey blue'])
# ax[1].set_ylabel('# of significant PBEs\n(all shuffles)')
# sns.despine()
# plt.tight_layout()
#

