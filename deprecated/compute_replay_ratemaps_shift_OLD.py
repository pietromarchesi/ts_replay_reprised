import matplotlib.pyplot as plt
from constants import *
from utils import *
import quantities as pq
from plotting_style import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from replaydetector.replayratemaps import ReplayRatemaps
import copy
import statsmodels.stats.descriptivestats
from utils import add_ripples_to_pbes_df
from utils import get_RSI
from session_selection import ratemap_sessions

data_version              = 'dec16'
PBEs_settings_name        = 'dec19k20'
dec_settings_name         = 'DecSet0'
ratemap_settings          = 'apr20_DecSet0_strict'
ratemap_area              = 'Barrel'
PBEs_area                 = 'Hippocampus'
dec_area                  = 'Hippocampus'

sessions = ratemap_sessions[ratemap_area]

# TODO da rimuovere
sessions = sessions[np.isin(sessions, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 27,
                                       28, 29, 30])]

# filter pbes parameters
shuffle_types             = ['column_cycle_shuffle', 'place_field_rotation_shuffle',
                             'unit_identity_shuffle']
shuffle_p_vals            = [0.05, 0.05, 0.05]
speed_thr_real_rm         = 3
time_bin_size_task_in_ms  = 200
min_speed                 = 3
rsi_thr                   = 0.5
phase_direction_ripples   = [('all', 'all', 'all'),
                             ('all', 'positive', 'all'),
                             ('all', 'negative', 'all'),
                             ('all', 'all', True),
                             ('all', 'all', False),
                             ('iti', 'all', 'all'),
                             ('img', 'all', 'all'),
                             ('reward', 'all', 'all'),
                             ('iti', 'positive', 'all'),
                             ('iti', 'negative', 'all'),
                             ('reward', 'positive', 'all'),
                             ('reward', 'negative', 'all')]

correlation_method        = 'pearson'
null_distribution_methods = ['rotate_ratemap']#['rotate_ratemap', 'rotate_spikes']
n_surrogates              = 30
shift_amounts             = np.arange(-10, 10)
bin_size_train            = 300

save_results              = True
# plot settings
save_plots                = False
plot_format               = 'png'
dpi                       = 400



# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

output_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(output_folder):
    os.makedirs(output_folder, exist_ok=True)

# --- RUN ---------------------------------------------------------------------

dfs = []

for sess_ind in sessions:
    # --- LOAD TASK DATA ------------------------------------------------------
    # Needed to build the ratemaps of the area during normal locomotion

    session_data = unpack_session(sess_ind, data_folder=DATA_FOLDER,
                                  area=ratemap_area,
                                  binsize_in_ms=time_bin_size_task_in_ms,
                                  data_version=data_version)

    speed_real = session_data['speed'].flatten()
    binned_spikes_task = session_data['spikes']
    binned_position_task = session_data['bin']

    binned_spikes_task = binned_spikes_task[speed_real >= speed_thr_real_rm, :]
    binned_position_task = binned_position_task[speed_real >= speed_thr_real_rm]

    trains = session_data['trains']
    n_trains = len(trains)
    # --- COMUPTE UNIT PROPERTIES ---------------------------------------------

    rsi = get_RSI(sess_ind, ratemap_area)

    # --- LOAD PBEs -----------------------------------------------------------


    pbes_data = load_decoded_PBEs(sess_ind=sess_ind,
                                   PBEs_settings_name=PBEs_settings_name,
                                   PBEs_area=PBEs_area,
                                   data_version=data_version,
                                   dec_settings_name=dec_settings_name,
                                   dec_area=dec_area,
                                   load_light=True)
    pbes_df_all = pbes_data['pbes']
    pbes_df_all = add_ripples_to_pbes_df(pbes_df_all)

    sig_df = pbes_data['sig_df']

    bin_size_pbe_in_ms = pbes_data['decode_pars']['bin_size_pbe_in_ms']
    slide_by_pbe_in_ms = pbes_data['decode_pars']['slide_by_pbe_in_ms']
    sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types,
                                            p_vals=shuffle_p_vals)
    pbes_df_all = pbes_df_all[np.isin(pbes_df_all.index, sel_pbes_sig)]

    for task_phase, replay_direction, has_ripple in phase_direction_ripples:

        print('Running for session {} task phase: {}, replay direction: {} and with ripple: '
              '{}'.format(sess_ind, task_phase, replay_direction, has_ripple))
        # if task_phase == 'all' and replay_direction == 'all':
        #     pass
        # elif task_phase == 'all' and replay_direction != 'all':
        #     pbes_df = pbes_df[pbes_df['replay_direction'] == replay_direction]
        # elif task_phase != 'all' and replay_direction == 'all':
        #     print('ieufh')
        #     pbes_df = pbes_df[pbes_df['phase'] == task_phase]
        # elif task_phase != 'all' and replay_direction != 'all':
        #     pbes_df = pbes_df[(pbes_df['phase'] == task_phase) &
        #                       (pbes_df['replay_direction'] == replay_direction)]

        # TODO check if this works

        if task_phase != 'all':
            pbes_df = pbes_df_all[pbes_df_all['phase'] == task_phase]
            print('def')
        if replay_direction != 'all':
            pbes_df = pbes_df_all[pbes_df_all['replay_direction'] == replay_direction]
            print('def')
        if has_ripple != 'all':
            pbes_df = pbes_df_all[pbes_df_all['has_ripple'] == has_ripple]
            print('def')
        if task_phase == 'all' and replay_direction == 'all' and has_ripple == 'all':
            pbes_df = pbes_df_all.copy()
            print('def')

        # TODO check this
        if pbes_df.shape[0] < 20:
            continue

        PBEs_times = [(row['start_time_in_ms'] * pq.ms, row['end_time_in_ms'] * pq.ms)
                      for i, row in pbes_df.iterrows()]

        binned_position_replay = pbes_df['trajectory'].tolist()

        binned_pbes = bin_PBEs(trains, PBEs_times,
                               bin_size_in_ms=bin_size_pbe_in_ms,
                               sliding_binning=True,
                               slide_by_in_ms=slide_by_pbe_in_ms,
                               pyramidal_only=False)

        binned_spikes_replay = binned_pbes['PBEs_binned']

        for sa in shift_amounts:

            if sa > 0:
                shifted_bs_replay = [s[sa:] for s in binned_spikes_replay]
                shifted_pos_replay = [p[:-sa] for p in binned_position_replay]

            elif sa < 0:
                shifted_bs_replay = [s[:sa] for s in binned_spikes_replay]
                shifted_pos_replay = [p[-sa:] for p in binned_position_replay]

            elif sa == 0:
                shifted_bs_replay = copy.copy(binned_spikes_replay)
                shifted_pos_replay = copy.copy(binned_position_replay)

            # --- MAKE SURE BINNED SPIKES DON'T HAVE ZERO SHAPE ---------------
            shifted_bs_replay_sel, shifted_pos_replay_sel = [], []

            for bs, bp in zip(shifted_bs_replay, shifted_pos_replay):
                if bs.shape[0] > 0 and bs.shape[1] > 0:
                    shifted_bs_replay_sel.append(bs)
                    shifted_pos_replay_sel.append(bp)

            shifted_bs_replay = shifted_bs_replay_sel
            shifted_pos_replay = shifted_pos_replay_sel


            # --- BUILD RATEMAPS ----------------------------------------------

            # TODO qua si dovrebbe porre un limite minimo effettivo sul numero di
            # PBEs che vengono usate per generare la ratemap

            # if shifted_bs_replay.__len__() < 10:
            #     continue

            rr = ReplayRatemaps(bootstrap=False,
                                unit_labels=range(len(trains)))

            rr.build_real_ratemaps(binned_spikes_task=binned_spikes_task,
                                   binned_position_task=binned_position_task,
                                   time_bin_size_task_in_ms=time_bin_size_task_in_ms)

            rr.build_event_ratemaps(binned_spikes_replay=shifted_bs_replay,
                                    binned_position_replay=shifted_pos_replay,
                                    time_bin_size_replay_in_ms=bin_size_pbe_in_ms)

            for null_distribution_method in null_distribution_methods:
                corr = rr.correlate_ratemaps(null_distribution_method=null_distribution_method,
                                             n_surrogates=n_surrogates,
                                             correlation_method=correlation_method)



                co = corr['corrcoef_obs']
                cd = corr['corrcoef_debiased']
                pr = corr['percentile_rank']
                pv = corr['p_values']
                sh = np.repeat(sa, co.shape[0])
                si = np.repeat(sess_ind, co.shape[0])
                un = np.arange(co.shape[0])
                ph = np.repeat(task_phase, co.shape[0]).astype(str)
                di = np.repeat(replay_direction, co.shape[0]).astype(str)
                hr = np.repeat(has_ripple, co.shape[0]).astype(str)
                nm = np.repeat(null_distribution_method, co.shape[0]).astype(str)

                columns = ['sess_ind', 'unit', 'shift_amount', 'phase',
                           'direction', 'null_method', 'rsi', 'corrcoef_obs',
                           'corrcoef_debiased', 'percentile_rank', 'p_values']

                dfx = pd.DataFrame(columns=columns)
                dfx['sess_ind'] = si
                dfx['unit'] = un
                dfx['shift_amount'] = sh
                dfx['phase'] = ph
                dfx['direction'] = di
                dfx['has_ripple'] = hr
                dfx['null_method'] = nm
                dfx['rsi'] = rsi
                dfx['corrcoef_obs'] = co
                dfx['corrcoef_debiased'] = cd
                dfx['percentile_rank'] = pr
                dfx['p_values'] = pv

                dfs.append(dfx)


df = pd.concat(dfs)



pars = {'ratemap_settings' : ratemap_settings,
        'ratemap_area' : ratemap_area,
        'PBEs_area' : PBEs_area,
        'dec_area' : dec_area,
        'PBEs_settings_name' : PBEs_settings_name,
        'dec_settings_name' : dec_settings_name,
        'shuffle_types' : shuffle_types,
        'shuffle_p_vals' : shuffle_p_vals,
        'speed_thr_real_rm' : speed_thr_real_rm,
        'time_bin_size_task_in_ms' : time_bin_size_task_in_ms,
        'min_speed' : min_speed,
        'rsi_thr' : rsi_thr,
        'phase_direction_ripples' : phase_direction_ripples,
        'null_distribution_methods' : null_distribution_methods,
        'n_surrogates' : n_surrogates,
        'shift_amounts' : shift_amounts,
        'bin_size_train' : bin_size_train}


output = {'pars' : pars,
          'df' : df}

if save_results:
    file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
    full_path = os.path.join(output_folder, file_name)
    print('Saving output to {}'.format(full_path))
    pickle.dump(output, open(full_path, 'wb'))

# --- PLOTTING ----------------------------------------------------------------
#
# # todo split by phase
# from statsmodels.stats.multitest import multipletests
#
# def get_significance_mask(dfx, variable='corrcoef_debiased', test_value=0,
#                           stat_test='t-test', correction='fdr_by'):
#     p_vals = []
#     for shift_amount in np.sort(dfx['shift_amount'].unique()):
#         ccd = dfx[dfx['shift_amount'] == shift_amount][variable]
#         if stat_test == 'sign_test':
#             m, p_val = statsmodels.stats.descriptivestats.sign_test(ccd, test_value)
#         elif stat_test == 't-test':
#             t, p_val = scipy.stats.ttest_1samp(ccd, test_value)
#         p_vals.append(p_val)
#     print(p_vals)
#     mask, pvals_corr, alphacs, alphacb = multipletests(p_vals, alpha=0.05,
#                                                        method=correction)
#     return mask, pvals_corr
#
#
# # parameters
# stat_test = 't-test'
# variables = ['corrcoef_obs', 'percentile_rank']
# nulldistmet_plots = ['rotate_ratemap']
# test_values = [0, 50]
# cols_to_check = ['phase', 'direction', 'has_ripple', 'null_method']
#
# # --- GLOBAL SHIFT ------------------------------------------------------------
# for variable, test_value in zip(variables, test_values):
#     for null_distribution_method in nulldistmet_plots:
#         dfx = df[(df['phase'] == 'all') &
#                  (df['direction'] == 'all') &
#                  (df['has_ripple'] == 'all') &
#                  (df['null_method'] == null_distribution_method)]
#
#         for col in cols_to_check:
#             assert dfx[col].unique().shape[0] == 1
#
#         mask, pvals_corr = get_significance_mask(dfx, correction='fdr_bh',
#                                                  variable=variable,
#                                                  stat_test=stat_test,
#                                                  test_value=test_value)
#
#         f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#         sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax)
#         ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))
#         height = ax.get_ylim()[0] + 0.01
#         signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
#         ax.scatter(shift_amounts, signif_line, lw=1, alpha=1)
#         sns.despine()
#         plt.tight_layout()
#
# # --- GLOBAL SHIFT SPLIT BY REPLAY DIRECTION ----------------------------------
#
# for variable, test_value in zip(variables, test_values):
#     for null_distribution_method in nulldistmet_plots:
#
#
#         f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#         masks = {}
#         for direction in ['positive', 'negative']:
#             dfx = df[(df['phase'] == 'all') &
#                      (df['direction'] == direction) &
#                      (df['has_ripple'] == 'all') &
#                      (df['null_method'] == null_distribution_method)]
#
#             for col in cols_to_check:
#                 assert dfx[col].unique().shape[0] == 1
#
#             mask, pvals_corr = get_significance_mask(dfx, correction='fdr_bh',
#                                                      variable=variable,
#                                                      stat_test=stat_test,
#                                                      test_value=test_value)
#
#             sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax,
#                          color=replay_direction_palette[direction], label=direction)
#
#             #mask[3:6] = True
#             masks[direction] = mask
#
#         ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))
#
#         for i, (dir, mask) in enumerate(masks.items()):
#             height = ax.get_ylim()[0] + 0.01 * i
#             signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
#             ax.scatter(shift_amounts, signif_line, color=replay_direction_palette[dir],
#                     lw=1,alpha=1)
#
#         sns.despine()
#         ax.legend(frameon=False)
#         plt.tight_layout()
#
#         if save_plots:
#             plot_name = 'ratemap_shift_by_replay_direction_{}_{}.' \
#                         '{}'.format(variable, null_distribution_method, plot_format)
#             f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#
#
#
# # --- GLOBAL SHIFT SPLIT BY RIPPLES -------------------------------------------
#
# for variable, test_value in zip(variables, test_values):
#     for null_distribution_method in nulldistmet_plots:
#
#
#         f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#         masks = {}
#         for has_ripple in ['True', 'False']:
#             dfx = df[(df['phase'] == 'all') &
#                      (df['direction'] == 'all') &
#                      (df['has_ripple'] == has_ripple) &
#                      (df['null_method'] == null_distribution_method)]
#
#             for col in cols_to_check:
#                 assert dfx[col].unique().shape[0] == 1
#
#             mask, pvals_corr = get_significance_mask(dfx, correction='fdr_bh',
#                                                      variable=variable,
#                                                      stat_test=stat_test,
#                                                      test_value=test_value)
#
#             sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax,
#                          color=has_ripple_palette[has_ripple], label=has_ripple)
#
#             #mask[3:6] = True
#             masks[direction] = mask
#
#         ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))
#
#         for i, (dir, mask) in enumerate(masks.items()):
#             height = ax.get_ylim()[0] + 0.01 * i
#             signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
#             ax.scatter(shift_amounts, signif_line, color=replay_direction_palette[dir],
#                     lw=1,alpha=1)
#
#         sns.despine()
#         ax.legend(frameon=False)
#         plt.tight_layout()
#
#         if save_plots:
#             plot_name = 'ratemap_shift_by_replay_direction_{}_{}.' \
#                         '{}'.format(variable, null_distribution_method, plot_format)
#             f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#
#
#
# # --- GLOBAL SHIFT SPLIT BY LOW VS HIGH RSI -----------------------------------
#
#
# df['rsi_bin'] = ['RSI above thr.' if i > 0.5 else
#                  'RSI below thr.' for i in df['rsi']]
#
#
#
# for variable in variables:
#     for null_distribution_method in nulldistmet_plots:
#
#         f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#         masks = {}
#         for rsi in ['RSI above thr.', 'RSI below thr.']:
#
#             dfx = df[(df['phase'] == 'all') &
#                      (df['direction'] == 'all') &
#                      (df['rsi_bin'] == rsi) &
#                      (df['has_ripple'] == 'all') &
#                      (df['null_method'] == null_distribution_method)]
#
#             for col in cols_to_check:
#                 assert dfx[col].unique().shape[0] == 1
#
#             mask, pvals_corr = get_significance_mask(dfx, correction='fdr_bh',
#                                                      variable=variable,
#                                                      stat_test=stat_test,
#                                                      test_value=test_value)
#
#             sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax,
#                          color=rsi_palette[rsi], label=rsi)
#
#             #mask[3:12] = True
#             masks[rsi] = mask
#
#         ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))
#
#         for i, (rsi, mask) in enumerate(masks.items()):
#             print(rsi, mask)
#             height = ax.get_ylim()[0] + 0.02 * i
#             y = np.repeat(height, len(shift_amounts))
#
#             #sa_diff = np.diff(shift_amounts)
#             #assert np.unique(sa_diff).shape[0] == 1
#             #sa_diff = sa_diff[0]
#             #x = np.append(shift_amounts, shift_amounts[-1]+sa_diff) - sa_diff / 2
#             signif_line = np.ma.masked_where(~mask, y)
#             ax.scatter(shift_amounts, signif_line, color=rsi_palette[rsi],
#                     lw=1,alpha=1)
#
#         sns.despine()
#         ax.legend(frameon=False)
#         plt.tight_layout()
#
#
#         if save_plots:
#             plot_name = 'ratemap_shift_by_RSI_score_{}_{}.' \
#                         '{}'.format(variable, null_distribution_method, plot_format)
#             f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# --- O -----------------------------------------------------------------------
# ---- L ----------------------------------------------------------------------
# ----- D ---------------------------------------------------------------------

#dfx = df[(df['shift_amount'] == 0) & (df['phase'] == 'all') & (df['direction'] == 'all')]

#f, ax = plt.subplots(1, 1)
#ax.scatter(dfx['percentile_rank'], dfx['rsi'])

# for shift_amount in shift_amounts:
#
#     percentile_rank = shiftratemaps[shift_amount]['percentile_rank']
#     corrcoef_debiased = shiftratemaps[shift_amount]['corrcoef_debiased']
#
#     # if subselect_units:
#     #     percentile_rank = percentile_rank[sel_ind]
#     #     corrcoef_debiased = corrcoef_debiased[sel_ind]
#
#     m, p_val_sign = statsmodels.stats.descriptivestats.sign_test(percentile_rank, 50)
#     t, p_val_t = scipy.stats.ttest_1samp(percentile_rank, 50)
#
#     shiftratemaps[shift_amount]['percentile_p_val_sign_test'] = p_val_sign
#     shiftratemaps[shift_amount]['percentile_p_val_t_test'] = p_val_t
#
#     m, p_val_sign = statsmodels.stats.descriptivestats.sign_test(corrcoef_debiased, 0)
#     t, p_val_t = scipy.stats.ttest_1samp(corrcoef_debiased, 0)
#
#     shiftratemaps[shift_amount]['corrcoef_p_val_sign_test'] = p_val_sign
#     shiftratemaps[shift_amount]['corrcoef_p_val_t_test'] = p_val_t
#
# percentile_p_val_sign_test = np.array([shiftratemaps[s]['percentile_p_val_sign_test'] for s in shift_amounts])
# percentile_p_val_t_test = np.array([shiftratemaps[s]['percentile_p_val_t_test'] for s in shift_amounts])
# corrcoef_p_val_sign_test = np.array([shiftratemaps[s]['corrcoef_p_val_sign_test'] for s in shift_amounts])
# corrcoef_p_val_t_test = np.array([shiftratemaps[s]['corrcoef_p_val_t_test'] for s in shift_amounts])


# shift_amounts_in_ms = shift_amounts * slide_by_pbe_in_ms
#
# pal = sns.color_palette("RdBu", n_colors=shift_amounts.shape[0])
# #boxplot_vars = ['percentile_rank', 'corrcoef_debiased']
# #boxplot_vars = ['percentile_rank', 'corrcoef_debiased', 'corrcoef_obs']
# boxplot_vars = ['corrcoef_debiased']
#
# plot_type = 'lineplot'
#
# for variable in boxplot_vars:
#
#     shifts, vals, units = [], [], []
#     for sa, sa_ms in zip(shift_amounts, shift_amounts_in_ms):
#         y = shiftratemaps[sa][variable]
#         vals.append(y)
#         shifts.append(np.repeat(sa_ms, y.shape[0]))
#         units.append(np.arange(y.shape[0]))
#
#     df = pd.DataFrame(columns=['shift_amount', variable, 'unit'])
#     df['shift_amount'] = np.hstack(shifts)
#     df[variable] = np.hstack(vals)
#     df['unit'] = np.hstack(units)
#
#     f, ax = plt.subplots(1, 1, figsize=[6, 5])
#     ax.set_xticks(shift_amounts_in_ms)
#
#     #ax.set_ylim([-0.25, 0.25])
#     if plot_type == 'boxplot':
#         sns.boxplot(data=df, x='shift_amount', y=variable, ax=ax, palette=pal)
#     elif plot_type == 'lineplot':
#         sns.lineplot(data=df, x='shift_amount', y=variable, ax=ax, palette=pal)
#     else:
#         raise ValueError
#
#     if variable == 'percentile_rank':
#         not_signif = percentile_p_val_t_test > 0.01
#     elif variable == 'corrcoef_debiased':
#         not_signif = corrcoef_p_val_t_test > 0.01
#
#     #ax.set_xticklabels(10*shift_amounts)
#     height = ax.get_ylim()[0] + 0.1
#     signif_line = np.ma.masked_where(not_signif, np.repeat(height, len(shift_amounts)))
#     if plot_type == 'boxplot':
#         ax.plot(np.arange(len(shift_amounts)), signif_line, color='k', lw=2, alpha=0.5)
#     elif plot_type == 'lineplot':
#         ax.plot(shift_amounts_in_ms, signif_line, color='k', lw=2, alpha=0.5)
#     else:
#         raise ValueError
#     ax.set_ylabel(replay_ratemap_labels[variable])
#     ax.set_xlabel('Shift amount (ms)')
#     ax.tick_params(axis='x', rotation=90)
#     plt.tight_layout()
#     sns.despine()
