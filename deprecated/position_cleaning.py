import os
import numpy as np
import pickle
import scipy.spatial
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from loadmat import loadmat
from constants import iPix2Cm
from constants import DATA_FOLDER
from plotting_style import *




# THIS EXISTS ALSO IN UTILS
def get_position_bin(bin, bin_centers, bin_labels):
    if bin <= 0:
        ind = np.argwhere(bin_labels['left'] == bin)[0][0]
        pos = bin_centers['left'][ind]
    elif bin > 0:
        ind = np.argwhere(bin_labels['right'] == bin)[0][0]
        pos = bin_centers['right'][ind]
    return pos


def load_bin_centers(data_folder, rat, day_folder, session_folder):
    dir_path = os.path.join(data_folder, 'PositionInfo_30', rat, day_folder, session_folder)
    path = loadmat(os.path.join(dir_path, 'path.mat'))
    xbins = np.hstack((path['cPathxM'], path['cPathxL'], path['cPathxR'])) / iPix2Cm
    ybins = np.hstack((path['cPathyM'], path['cPathyL'], path['cPathyR'])) / iPix2Cm
    bin_centers = np.hstack((xbins[:, np.newaxis], ybins[:, np.newaxis]))
    return bin_centers


def plot_maze(ax,c=None, alpha=1):
    if c is None:
        c = maze_color
    ax.plot(mz['xaxMInL'], mz['yaxMInL'], c=c, alpha=1, zorder=-2)
    ax.plot(mz['xaxMInR'], mz['yaxMInR'], c=c, alpha=1, zorder=-2)
    ax.plot(mz['xaxMOut'], mz['yaxMOut'], c=c, alpha=1, zorder=-2)


def get_rat_day_session(data_folder, sess_ind):
    F = loadmat(data_folder + '/F/F_units/F_All.mat')['F']
    rat, day, session = F[sess_ind].dir.split('/')[-3:]
    return rat, day, session





def is_on_maze(point, margin):
    ext  = maze.exterior.distance(point) < margin
    int1 = maze.interiors[0].distance(point) < margin
    int2 = maze.interiors[1].distance(point) < margin
    return (ext or int1 or int2)


def is_on_side(point, side, margin):
    """
    This function only excludes only point that are on the other side. i.e.
    if a point is on the centeral arm, it will be considered True for both
    'left' and 'right'. So is_on_side should really be interepreted as: can
    this point have occurred in a trial of this side?
    """
    if side == 'left':
        b = not (right_arm.exterior.distance(point) < margin)
    elif side == 'right':
        b = not (left_arm.exterior.distance(point) < margin)
    return b


def is_on(point, side, margin=4):

    if not isinstance(point, Point):
        point = Point(point)

    if side == 'left':
        b = (left_arm.exterior.distance(point) < margin)
    elif side == 'right':
        b = (right_arm.exterior.distance(point) < margin)
    elif side == 'center':
        b = central_arm.exterior.distance(point) < margin
    return b


def get_maze_arm_of_point(point):

    if not isinstance(point, Point):
        point = Point(point)

    dist = {}
    dist['left'] = left_arm.exterior.distance(point)
    dist['right'] = right_arm.exterior.distance(point)
    dist['central'] = central_arm.exterior.distance(point)
    #print(dist)
    return min(dist, key=dist.get)


def clean_position_segment(segment, margin=10):

    x_pos = segment.analogsignals[0][:, 0]
    y_pos = segment.analogsignals[0][:, 1]
    speed = segment.analogsignals[1]
    pos_units = segment.analogsignals[0].units
    spe_units = segment.analogsignals[1].units
    side  = segment.annotations['trial_side']

    for ind, (x, y) in enumerate(zip(x_pos, y_pos)):
        point = Point(x, y)
        if is_on_maze(point, margin) and is_on_side(point, side, margin):
            pass
        else:
            x_pos[ind] = np.nan * pos_units
            y_pos[ind] = np.nan * pos_units
            speed[ind] = np.nan * spe_units




def plot_clean_position_segment(ax, segment, margin=10):

    x_pos = segment.analogsignals[0][:, 0]
    y_pos = segment.analogsignals[0][:, 1]
    side  = segment.annotations['trial_side']

    for ind, (x, y) in enumerate(zip(x_pos, y_pos)):
        point = Point(x, y)
        if is_on_maze(point, margin):

            if is_on_side(point, side, margin):
                ax.scatter(x, y, c='g')
            else:
                ax.scatter(x, y, c='r')
        else:
            ax.scatter(x, y, c='grey')



def prepare_bins(bin_centers):
    """
    Given the bin centers, which have to be loaded for every session as they
    shift a bit across sessions, create two dictionaries, one containing
    the bin positions and one containing the bin labels, each keyed for left
    and right trials.
    """
    right_bins_lab = np.hstack((np.arange(24, 30), np.arange(0, 24),
                                np.repeat(np.nan, 21)))
    left_bins_lab  = np.hstack((np.arange(-24, -30, -1),  np.arange(0, -3, -1),
                                np.repeat(np.nan, 21), np.arange(-3, -24, -1)))
    left_bins = bin_centers[~np.isnan(left_bins_lab)]
    right_bins = bin_centers[~np.isnan(right_bins_lab)]
    right_bins_lab = right_bins_lab[~np.isnan(right_bins_lab)].astype(int)
    left_bins_lab = left_bins_lab[~np.isnan(left_bins_lab)].astype(int)

    bins = {'left'  : left_bins,
            'right' : right_bins}

    bin_labels = {'left' : left_bins_lab,
                  'right' : right_bins_lab}

    return bins, bin_labels




def bin_position_linearized(pos, bin_centers):
    """
    Simple linearized bins (every position gets unique bin)
    """
    dist = scipy.spatial.distance.cdist(pos, bin_centers)
    bin_index = np.argmin(dist, axis=1)
    return bin_index





def bin_position_linearized_LR(pos, trial_side, bin_centers,
                               bin_labels, max_distance=12):

    """
    :param position: Array of position at each time point
    :param trial_side: Array of trial side ('left' or 'right' at each position)
    :param bin_centers:
    :param bin_labels:
    :param max_distance:
    :return:
    """
    binned_position = []
    unassigned = []

    for p, side, in zip(pos, trial_side):
        if isinstance(side, str):
            dist    = scipy.spatial.distance.cdist(p[np.newaxis, :], bin_centers[side])
            ind     = np.argmin(dist)
            bin     = bin_centers[side][ind]
            bin_lab = bin_labels[side][ind]

            if (np.isnan(p).sum()==0) and (np.min(dist) < max_distance):
                binned_position.append(bin_lab)
            else:
                binned_position.append(np.nan)
                unassigned.append((p, bin, np.min(dist), side))
        elif np.isnan(side):
            binned_position.append(np.nan)
        else:
            raise ValueError('Something\'s up!')
    return np.array(binned_position), unassigned



def mark_locations(ax, lw=4, color=None):

    if color is None:
        color = maze_color
    rat, day, session = get_rat_day_session(DATA_FOLDER, 10)
    bin_centers = load_bin_centers(DATA_FOLDER, rat, day, session)
    bin_centers, bin_labels = prepare_bins(bin_centers)

    # start of trial
    x, y = get_position_bin(2, bin_centers, bin_labels)
    ax.plot([x - 9, x + 9], [y, y], c=color, lw=lw, zorder=-1)

    # points of no return
    # x, y = get_position_bin(10, bin_centers, bin_labels)
    # ax.plot([x - 11, x + 7], [y + 2, y + 2], c=color, lw=lw, zorder=-1)
    #
    # x, y = get_position_bin(-10, bin_centers, bin_labels)
    # ax.plot([x - 7, x + 11], [y + 2, y + 2], c=color, lw=lw, zorder=-1)

    # return to central arm
    # x1, y1 = get_position_bin(25, bin_centers, bin_labels)
    # x2, y2 = get_position_bin(26, bin_centers, bin_labels)
    # x = (x1 + x2) / 2
    # y = (y1 + y2) / 2
    # ax.plot([x - 9, x + 9], [y, y], c=color, lw=lw, zorder=-1)

    # reward 1
    x1, y1 = get_position_bin(17, bin_centers, bin_labels)
    x2, y2 = get_position_bin(18, bin_centers, bin_labels)
    x = (x1 + x2) / 2
    y = (y1 + y2) / 2
    ax.plot([x - 7, x + 7], [y + 7, y - 7], c=color, lw=lw, zorder=-1)

    # reward 2
    x1, y1 = get_position_bin(-17, bin_centers, bin_labels)
    x2, y2 = get_position_bin(-18, bin_centers, bin_labels)
    x = (x1 + x2) / 2
    y = (y1 + y2) / 2
    ax.plot([x - 7, x + 7], [y - 7, y + 7], c=color, lw=lw, zorder=-1)
    ax.axis('off')


