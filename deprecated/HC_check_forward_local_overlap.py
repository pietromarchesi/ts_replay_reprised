import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from utils import load_bin_centers
from utils import prepare_bins
from utils import plot_maze, mark_locations
from utils import get_position_bin
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from utils import is_on_central_arm, merge_duplicate_left_right_bins
from utils import add_ripples_to_pbes_df
from session_selection import sessions_HC_replay
from utils import load_decoded_PBEs
from utils import get_time_spent_per_task_phase
import quantities as pq
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
from scipy.ndimage import gaussian_filter1d
import itertools
from utils import *



data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]

group_iti_and_return = True

# PLOTTING PARAMETERS

save_plots = False
plot_format = 'svg'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'HC_replay')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df, df_not_sig = load_replay(sessions=sessions,
                             PBEs_settings_name=PBEs_settings_name,
                             PBEs_area=PBEs_area,
                             data_version=data_version,
                             dec_settings_name=dec_settings_name,
                             dec_area=dec_area,
                             min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                             shuffle_types=shuffle_types,
                             shuffle_p_vals=shuffle_p_vals,
                             group_iti_and_return=group_iti_and_return,
                             return_not_sig=True)


if group_iti_and_return:
    phases = ['img', 'iti', 'reward', 'run']
else:
    phases = ['img', 'iti', 'return', 'reward', 'run']


df = add_local_remote_v2(df)

df_sel = df.copy()#df[np.isin(df['phase'], ['run', 'reward'])]

lrf = df_sel[df_sel['replay_direction'] == 'positive']['local_remote_v2'].value_counts()
lrr = df_sel[df_sel['replay_direction'] == 'negative']['local_remote_v2'].value_counts()

fraction_forward = lrf['local'] / (lrf['local'] + lrf['remote'])
fraction_reverse = lrr['local'] / (lrr['local'] + lrr['remote'])

print('Fraction of local events in forward: {}'.format(fraction_forward))
print('Fraction of local events in reverse: {}'.format(fraction_reverse))




dx = pd.DataFrame(columns=['sess_ind', 'direction', 'frac_local'])

for sess_ind in sessions:
    df_sel = df[(df['sess_ind'] == sess_ind)] #& (np.isin(df['phase'], ['run', 'reward', 'iti', 'image']))]
    if df_sel.shape[0] > 15:
        lrf = df_sel[df_sel['replay_direction'] == 'positive']['local_remote_v2'].value_counts()
        lrr = df_sel[df_sel['replay_direction'] == 'negative']['local_remote_v2'].value_counts()

        fraction_forward = lrf['local'] / (lrf['local'] + lrf['remote'])
        fraction_reverse = lrr['local'] / (lrr['local'] + lrr['remote'])

        dx.loc[dx.shape[0], :] = [sess_ind, 'positive', fraction_forward]
        dx.loc[dx.shape[0], :] = [sess_ind, 'negative', fraction_reverse]


f, ax = plt.subplots(1, 1)
sns.barplot(x='direction', y='frac_local', data=dx)
ax.set_ylim([0, 1])

x = dx[dx['direction'] == 'positive']['frac_local']
y = dx[dx['direction'] == 'negative']['frac_local']





dx = pd.DataFrame(columns=['sess_ind', 'phase', 'frac_local'])

for sess_ind in sessions:

    df_sel = df[(df['sess_ind'] == sess_ind)] #& (np.isin(df['phase'], ['run', 'reward', 'iti', 'image']))]
    if df_sel.shape[0] > 15:
        lrii = df_sel[np.isin(df_sel['phase'], ['iti', 'img'])]['local_remote_v2'].value_counts()
        lrrr = df_sel[np.isin(df_sel['phase'], ['run', 'reward'])]['local_remote_v2'].value_counts()

        if len(lrii ==2) and len(lrrr==2):
            fraction_itiimg = lrii['local'] / (lrii['local'] + lrii['remote'])
            fraction_runrew = lrrr['local'] / (lrrr['local'] + lrrr['remote'])

            dx.loc[dx.shape[0], :] = [sess_ind, 'itiimg', fraction_itiimg]
            dx.loc[dx.shape[0], :] = [sess_ind, 'runrew', fraction_runrew]


f, ax = plt.subplots(1, 1)
sns.barplot(x='phase', y='frac_local', data=dx)
ax.set_ylim([0, 1])

x = dx[dx['phase'] == 'itiimg']['frac_local']
y = dx[dx['phase'] == 'runrew']['frac_local']
wilcoxon(x, y)