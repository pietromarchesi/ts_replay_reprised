
- Check that velocity is actually in mm/s

- Issue: in the last trial, the position stops being recorded
but the trial continues (probably because we use as time stop
for the last trial the timestamp of the last spike). Quick hack:
exclude the last trial.

- Test PBEs

- Problem: interneurons are helpful for position decoding!

- HAD TO MANUALLY INVERT THE TRIAL SIDE in prepare_sess_task_neo,
and now there's also a flip when I make the polygons in the
position_cleaning.py because apparently the maze coordinates are not flipped.

- In the CCH calculation, there could be an effect of removed trials?

- Somatosensory area

- Interneurons?

# NOTES

## SCALING DATA FOR RANDOM FOREST
It could be that random forest doesn't handle replay very well
simply because we use different binning in train and test. What I could
do is scale the data: instead of simply spike count per bin, transform
the data to spike count divided by bin length.


# NOTE
In the DKW decoder, it can be that at a certain time bin all space bins
have probability zero, so when you divide by the sum of the probability
you get nans. Now for that time bin I set all probabilities to zero
(but you could also set to a uniform probability).

Speed threshold will have to be converted to cm

## PANDA VERSIONS PROBLEM
PBEs_decode (dkw style) breaks with newer pandas because of argmax
of the replay scores, had to put an older version (0.20.3)

## In sessions 27 and 29 very few PR pbes which have low velocity

# FLIP POSITION in MAZE PLOTS

# INSTEAD OF DISCRIMINATING REPLAY SIDE
AT ZERO, YOU CAN VARY THAT THRESHOLD
TO FIGHT THE IMBALANCE

# FIX THE CONFIDENCE THRESHOLDING NOW THAT YOU DON'T SPLIT AT ZERO

# PBEs in PBEs_compare_sessions
are now aggregated without first averaging the confusion matrix per session


# TASK DECODING OF ALL HIPPOCAMPUS


# IDEA: instead of simple correlation between real and event ratemap,
come up with a measure like the proportion of overlapping area between
the two curves over the sum of the two areas.

# INTRODUCE SLEEP

# EXPLAINED VARIANCE

# IN REPLAY DETECTOR, ADD FUNCTIONS TO COMPUTE THE SIX MEASURES
# OF SEQUENCE STRUCTURE
mentioned in "Trajectory events across hippocampal place cells
require previous experience"


# Currently, the bad trials are excluded right away in the creation of the
neo files, which means that when you merge all the segments, you have
chunks of artificial silence in the firing of the cells. This is taken
care of in the binning, as those times are removed (in unpack_session),
but if you just load the neo block and concatenate the trains, this
will be a problem. This could affect the computation of the PBE modulation,
because that computes the spikes/time for all time intervals which are not
in the PBEs, so it will include those periods of silence.


- Correlation of time of occurrence of PBEs
- Decoding with event ratemaps from Perirhinal vs decoding with real ratemap (??)
- Dimensionality reduction trajectory
Fit a dimensionality reduction method in the long (trial-averaged
is not strictly possible because you would have to stretch the length
of the trials) form, then project all the real trials, and the replay events.
- Finding a subselection criteria that works for both areas PR and BR, plus parametric testing
- Cross-correlation?


# COMPUTE THE CORRELATION BETWEEN ALL PAIRS OF HIPPOCAMPAL AND PERIRHINAL
# UNITS DURING TASK AND DURING PBEs

# LOOK AT REPLAY ONLY IN THE REWARD ZONES (So invert the exclude central arm)

# FIRST DETERMINE SIGNIFICANT REAL-EVENT RATEMAP CORRELATIONS,
and for those look at best shift (???)


# SHOW THAT THE REAL-EVENT RATEMAP CORRELATION IS HIGHER FOR SIGNIFICANT PBEs

# INTRODUCE SMOOTHING IN PERIRHINAL DECODING?

# NOTE
In the PBEs df, the n_active and percentage_active
refer to the area which was used for the PBE
detection, which may not be the same areas for which the decoding
is run

# ADAPT SESSIONS INFO AND CHECK THAT IT MATCHES THE NEO STUFF WE MAKE


1. Sleep
2. LFP states
3. Estimate a compression factor
4. Replay of current vs previous trial is significant
5. What parts of the maze are we replaying? Every trajectory has a
start and an end location
