import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from utils import load_bin_centers
from utils import prepare_bins
from utils import plot_maze, mark_locations
from utils import get_position_bin
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from utils import is_on_central_arm, merge_duplicate_left_right_bins
from utils import add_ripples_to_pbes_df
from session_selection import sessions_HC_replay
from utils import load_decoded_PBEs
from utils import *
import quantities as pq
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
from scipy.ndimage import gaussian_filter1d
import itertools
from utils import bin_occurrences_for_chi_square_test, linearize_trajectory



"""
PLOT
1. Barplot of replay direction (positive versus negative)
2. Distribution of event durations (kdeplot)
3. Plot where the PBEs occur colored by left/central/right arm
4. Plot distribution of start vs end locations
5. Distribution of bin of occurrence of PBE vs decoded start and decoded end
locations 
6. Distribution of compression factors 
7. Distributions of replay structure measures across epochs (are replay
events more structured in post-sleep/task than pre-sleep?)
8. Distributions of replay structure measures across locations / task phase 
(do replay events differ depending on when/where they occur?)
- replay (z) score
- max and mean jump distance
"""


data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

group_iti_and_return = True

# PLOTTING PARAMETERS

plot_settings = 'only_two_shufs'
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms    = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, 'HC_replay_stacked_trajectories_early_reward')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df, df_not_sig = load_replay(sessions=sessions,
                             PBEs_settings_name=PBEs_settings_name,
                             PBEs_area=PBEs_area,
                             data_version=data_version,
                             dec_settings_name=dec_settings_name,
                             dec_area=dec_area,
                             min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                             shuffle_types=shuffle_types,
                             shuffle_p_vals=shuffle_p_vals,
                             group_iti_and_return=group_iti_and_return,
                             return_not_sig=True)



# --- MAKE EARLY REWARD --------------------------------------------------------
rew_times = []
for sess_ind in sessions:
    bl = get_session_block(DATA_FOLDER, sess_ind, 'task', data_version=data_version)
    for seg in bl.segments:
        ind = np.where(seg.events[0].labels == 'Reward')[0][0]
        rewt = seg.events[0].times[ind]
        rew_times.append(rewt.rescale(pq.ms))
rew_times = np.array(rew_times) * pq.ms



for i, row in df.iterrows():

    tstart = row['start_time_in_ms'] * pq.ms

    try:
        rew_before = rew_times[rew_times < tstart].max()
        diff = (tstart - rew_before).rescale(pq.s)

    except ValueError:
        diff = 10 * pq.s

    if diff < 0.5 * pq.s :
        df.loc[i, 'phase'] = 'early_reward'



# --- STACKED TRAJECTORIES -----------------------------------------------------

max_n_traj = 300

phases = ['reward', 'run', 'early_reward']

for phase in phases:
    df_sel = df[df['epoch'] == 'task']
    df_sel = df_sel[df_sel['phase'] == phase]

    #df_sel = df_sel[~df_sel['pbe_loc_bin'].isna()]

    if df_sel.shape[0] > max_n_traj:
            ind = np.random.choice(np.arange(df_sel.shape[0]), max_n_traj, replace=False)
            df_sel = df_sel.iloc[ind]

    new_trajs = []
    for i in range(df_sel.shape[0]):
        row = df_sel.iloc[i]
        traj = row['trajectory']
        loc = row['pbe_loc_bin']
        side = row['pbe_side']
        direction = row['replay_direction']
        traj = linearize_trajectory(traj, side, direction)
        new_trajs.append(traj)
    df_sel['trajectory'] = new_trajs
    df_sel['new_dec_start_bin'] = [t[0] for t in df_sel['trajectory']]
    df_sel['new_dec_end_bin'] = [t[-1] for t in df_sel['trajectory']]


    df1 = df_sel[df_sel['replay_direction'] == 'negative']
    df2 = df_sel[df_sel['replay_direction'] == 'positive']
    df1 = df1.sort_values(by=['new_dec_start_bin', 'new_dec_end_bin'])
    df2 = df2.sort_values(by=['new_dec_start_bin', 'new_dec_end_bin'])
    df_sel = pd.concat((df1, df2))

    f, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios' : [1, 7]},
                         sharex=True, figsize=[2.5, 6.5])

    ax2 = ax[0].twinx()  # instantiate a second axes that shares the same x-axis

    # if phase == 'img':
    #     bw_loc = 0.5
    # else:
    #     bw_loc = 0.1
    # sns.kdeplot(df_sel['pbe_loc_bin'], ax=ax2, c=animal_loc_color,
    #             label='Animal location during REs', bw=bw_loc,
    #             cut=100, clip=[-29, 29])

    bw = 0.1
    sns.kdeplot(df_sel['new_dec_start_bin'], ax=ax[0], c=trajectory_cmap(0),
                label='Start of replay trajectory', bw=bw, cut=100, clip=[-29, 29])

    sns.kdeplot(df_sel['new_dec_end_bin'], ax=ax[0], c=trajectory_cmap(255),
                label='End of replay trajectory', bw=bw, cut=100, clip=[-29, 29])


    ytext = df_sel[df_sel['replay_direction'] == 'negative'].shape[0]

    ba = int(df_sel.shape[0] / 40)
    ax[1].plot([-31, -31], [1, ytext-ba], lw=4.5,
               c=replay_direction_palette['negative'])
    ax[1].plot([-31, -31], [ytext+ba, df_sel.shape[0]-1], lw=4.5,
               c=replay_direction_palette['positive'])

    if phase == 'iti' or phase == 'img':
        ax[1].axvspan(*location_bands[phase], alpha=0.5, color=animal_loc_color, lw=0)
    elif phase == 'run' or phase == 'reward':
        ax[1].axvspan(*location_bands[phase][0], alpha=0.5, color=animal_loc_color, lw=0)
        ax[1].axvspan(*location_bands[phase][1], alpha=0.5, color=animal_loc_color, lw=0)

    for i in range(df_sel.shape[0]):
        row = df_sel.iloc[i]
        traj = row['trajectory']
        loc = row['pbe_loc_bin']
        side = row['pbe_side']
        direction = row['replay_direction']

        if traj[-1] > traj[0]:
            traj = np.arange(traj[0], traj[-1]+0.1, 0.5)
        elif traj[-1] < traj[0]:
            #print('rev')
            traj = np.arange(traj[0], traj[-1]-0.1, -0.5)
        N = traj.shape[0]
        for j in range(traj.shape[0]-1):
            #print([traj[j], traj[j+1]])
            colors = trajectory_cmap(int(255 * j / N))
            ax[1].plot([traj[j], traj[j+1]], [i, i], color=colors, zorder=100)

        ax[1].scatter(traj[0], i, c=trajectory_cmap(0), s=6, zorder=101)
        ax[1].scatter(traj[-1], i, c=trajectory_cmap(255), s=6, zorder=101)
        # ax[1].scatter(loc, i, facecolor=animal_loc_color, marker='.', zorder=100, alpha=0.8,
        #               edgecolor='w', lw=0, s=40)
    sns.despine(left=True, bottom=True, ax=ax[0])
    sns.despine(left=True, bottom=True, ax=ax2)

    for axx in ax:
        axx.set_yticks([], [])
    ax2.set_yticks([], [])
    ax[0].tick_params(axis=u'both', which=u'both',length=0)
    ax[1].set_xlabel('Linearized location', labelpad=15)
    ax[0].legend(frameon=False)
    leg = ax[0].get_legend()
    ax[0].get_legend().remove()
    #ax2.get_legend().remove()
    ax[1].set_xlim([-31, 31])
    ax[1].set_xticks([-29, -reward_bin, 0, reward_bin, 29])
    ax[1].set_xticklabels(['IMG', 'REW', 'IMG', 'REW', 'IMG'], fontsize=8)
    sns.despine(left=True, ax=ax[1], trim=True)
    for loc in [0, reward_bin, -reward_bin]:
        ax[1].axvline(loc, c=maze_color, zorder=-10, ls='--')
    plt.tight_layout()

    plot_name = 'trajectory_stack_{}.{}'.format(phase, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

