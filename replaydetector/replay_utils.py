import numpy as np
import pandas as pd
import itertools
import copy
import quantities as pq

def fit_lines_old(y_pred_proba, bins=None, n_bins_proba=3, bins_ext=20, subsample=2):

    """

    :param y_pred_proba: shape time x bins (sklearn format)
    :param bins:
    :param n_bins_proba:
    :param bins_ext:
    :param subsample:
    :return:
    """

    if bins is None:
        bins = np.arange(y_pred_proba.shape[1]).astype(int)

    time_bins = np.array(range(y_pred_proba.shape[0]))

    bins_loc = np.arange(bins[0] - 0.5, bins[-1] + 1.5)
    bins_time = np.arange(-0.5, time_bins.shape[0] + 0.5)

    # pad the probability so that it wraps around
    padded_proba = np.pad(y_pred_proba,
                          pad_width=((0, 0), (bins_ext, bins_ext)),
                          mode='wrap').T

    # create a dataframe for the results
    line_fits = pd.DataFrame(columns=['start_bin', 'end_bin', 'replay_score'])

    # allow trajectories to start outside the range (equivalent to having
    # trajectories that start later or end earlier)
    bins_extended = np.arange(bins[0] - bins_ext, bins[-1]+bins_ext, 1).astype(int)

    bin_combinations = list(itertools.product(bins_extended[::subsample],
                                              bins_extended[::subsample]))

    for start_location, end_location in bin_combinations:

        # define line by location at first and last time bins and interpolate
        x = time_bins
        coefficients = np.polyfit([time_bins[0], time_bins[-1]],
                                  [start_location, end_location], 1)
        line = np.poly1d(coefficients)
        y = line(x)

        # find in which bins the line falls at every time bin
        H, xedges, yedges = np.histogram2d(x, y, bins=[bins_time, bins_loc])

        # pad on top and bottom
        mask = np.pad(H, pad_width=((0, 0), (bins_ext, bins_ext)),
                      mode='constant', constant_values=0)
        mask = mask.T

        # set entries to 1 for the probability bins around the line that
        # should be counted
        for i in range(mask.shape[1]):
            j = mask[:, i].argmax()
            mask[j + 1:j + n_bins_proba + 1, i] = 1
            mask[j - n_bins_proba:j, i] = 1

        # compute probability around the line
        replay_score = (padded_proba * mask).sum()

        line_fits.loc[line_fits.shape[0], :] = [start_location, end_location, replay_score]

    return line_fits, padded_proba



def fit_lines(y_pred_proba, bins=None, n_bins_proba=3, bins_ext=20, subsample=2):

    time_bins = np.array(range(y_pred_proba.shape[0]))
    total_probability = y_pred_proba.sum()

    bins_loc = np.arange(bins[0] - 0.5, bins[-1] + 1.5)
    bins_time = np.arange(-0.5, time_bins.shape[0] + 0.5)

    # pad the probability so that it wraps around
    # we pad y_pred_proba in the direction of axis 1 (the bins)
    padded_proba = np.pad(y_pred_proba,
                          pad_width=((0, 0), (bins_ext, bins_ext)),
                          mode='wrap').T

    # create a dataframe for the results
    line_fits = pd.DataFrame(columns=['start_bin', 'end_bin', 'replay_score'])

    # allow trajectories to start outside the range (equivalent to having
    # trajectories that start later or end earlier)
    bins_extended = np.arange(bins[0] - bins_ext, bins[-1] + bins_ext + 1,
                              1).astype(int)

    bin_combinations = list(itertools.product(bins_extended[::subsample],
                                              bins_extended[::subsample]))

    # bin_combinations = [(0, 0), (-5, 3), (0, -5)]

    for start_location, end_location in bin_combinations:

        # define line by location at first and last time bins and interpolate
        x = time_bins
        coefficients = np.polyfit([time_bins[0], time_bins[-1]],
                                  [start_location, end_location], 1)
        line = np.poly1d(coefficients)
        y = line(x)

        # find in which bins the line falls at every time bin
        histogram_bins = np.hstack((bins_extended - 0.5, bins_extended[-1] + 0.5))
        H, xedges, yedges = np.histogram2d(x, y, bins=[bins_time, histogram_bins])

        mask = H.T.copy()

        # set entries to 1 for the probability bins around the line that
        # should be counted
        for i in range(mask.shape[1]):
            j = mask[:, i].argmax()
            mask[j + 1:j + n_bins_proba + 1, i] = 1
            mask[j - n_bins_proba:j, i] = 1

        # compute probability around the line
        #replay_score = (padded_proba * mask).sum()

        replay_score = 100 * (padded_proba * mask).sum() / total_probability
        assert replay_score >= 0 and replay_score <= 100
        line_fits.loc[line_fits.shape[0], :] = [start_location, end_location,
                                                replay_score]

    line_fits['replay_score'] = pd.to_numeric(line_fits['replay_score'])
    assert np.isnan(line_fits['replay_score']).sum() == 0

    return line_fits, padded_proba



def get_best_fit(line_fits):

    ind_max = line_fits['replay_score'].idxmax()
    #bool_ind_max = line_fits.index.isin([ind_max])
    #mean_score = line_fits[~bool_ind_max]['replay_score'].mean()
    #mean_score = line_fits['replay_score'].mean()
    start_location = line_fits.loc[ind_max, 'start_bin']
    end_location = line_fits.loc[ind_max, 'end_bin']
    score = line_fits.loc[ind_max, 'replay_score']

    return start_location, end_location, score




def time_bin_shuffle(y_pred_proba, bins, n_surr, n_bins_proba=4):

    surr_scores = np.zeros(n_surr)

    for n in range(n_surr):
        y_pred_shuf = y_pred_proba.copy()
        np.random.shuffle(y_pred_shuf)

        line_fits = fit_lines(y_pred_shuf, bins, n_bins_proba=n_bins_proba)
        start, end, score = get_best_fit(line_fits)

        surr_scores[n] = score

    return surr_scores




def column_cycle_shuffle(y_pred_proba, bins, n_surr, n_bins_proba=4):

    """
    y_pred_proba is n_samples by n_bins so we really cycle the rows
    """

    surr_scores = np.zeros(n_surr)

    for n in range(n_surr):
        y_pred_shuf = y_pred_proba.copy()
        y_pred_shuf = cycle_rows(y_pred_shuf)

        line_fits = fit_lines(y_pred_shuf, bins, n_bins_proba=n_bins_proba)
        start, end, score = get_best_fit(line_fits)

        surr_scores[n] = score

    return surr_scores



def unit_identity_shuffle(spikes, dec, bins, n_surr, n_bins_proba=4):

    surr_scores = np.zeros(n_surr)

    for n in range(n_surr):

        dec_surr = copy.deepcopy(dec)
        np.random.shuffle(dec_surr.ratemap)

        y_pred_shuf = dec_surr.predict_proba(spikes)
        line_fits = fit_lines(y_pred_shuf, bins, n_bins_proba=n_bins_proba)
        start, end, score = get_best_fit(line_fits)
        surr_scores[n] = score
    return surr_scores



def place_field_rotation_shuffle(spikes, dec, bins, n_surr, n_bins_proba=4):

    surr_scores = np.zeros(n_surr)

    for n in range(n_surr):
        dec_surr = copy.deepcopy(dec)
        dec_surr.ratemap = cycle_rows(dec_surr.ratemap)
        y_pred_shuf = dec_surr.predict_proba(spikes)
        line_fits = fit_lines(y_pred_shuf, bins, n_bins_proba=n_bins_proba)
        start, end, score = get_best_fit(line_fits)
        surr_scores[n] = score

    return surr_scores



def cycle_rows(X):
    """
    Roll every row of X by a random amount.
    """
    X = X.copy()

    shift_amount = np.random.randint(0, X.shape[1], size=X.shape[0])
    for i in range(X.shape[0]):
        X[i, :] = np.roll(X[i, :],shift=shift_amount[i])

    return X



def compute_jump_distance(X):

    """
    Given a numpy array X of posterior probabiities, of shape n_times x n_bins,
    this function calculates the distance between peak probabilities at
    subsequent bins, considering that the bins wrap around, so the distance
    between two rows [1, 0, 0, 0] and [0, 0, 0, 1] is 1, not 3.
    """

    peak_prob_ind = X.argmax(axis=1)
    jump_dist = np.abs(np.diff(peak_prob_ind))

    n_bins = X.shape[1]
    max_dist = int(n_bins / 2)
    for j in range(jump_dist.shape[0]):
        if jump_dist[j] > max_dist:
            jump_dist[j] = n_bins - jump_dist[j]

    return jump_dist




def replay_wrap_bins_and_direction(side, start_bin, end_bin):
    """
    The replay detector will hand us bins which are not necessarily between
    (0, 29) for right side pbes and (-29, 0) for left side pbes. This is
    because of the wrapping that we do when fitting trajectory lines. The
    difference between these bins allows us to calculate how many bins the
    trajectory spans, thus we can easily compute a trajectory speed, as is
    done in replaydetector. However, when we look at for example where the
    trajectories start and end, we need to bring back these additional bins
    to the standard ranges (i.e. 'wrap' them). By doing that, however, we lose
    the information about whether the trajectory goes in the same direction
    as a normal trial run or it goes backwards, so we also determine that
    (which is also a bit different for the two pbe sides because of how the
    bins are defined).
    """

    if side == 'right':

        if end_bin > start_bin:
            replay_direction = 'positive'
        else:
            replay_direction = 'negative'

        if start_bin > 29:
            start_bin = start_bin - 29 - 1
        if end_bin > 29:
            end_bin = end_bin - 29 - 1
        if start_bin < 0:
            start_bin = np.arange(29 + 1)[start_bin]
        if end_bin < 0:
            end_bin = np.arange(29 + 1)[end_bin]

    elif side == 'left':

        if end_bin < start_bin:
            replay_direction = 'positive'
        else:
            replay_direction = 'negative'

        if start_bin < -29:
            start_bin = start_bin + 29 + 1
        if end_bin < -29:
            end_bin = end_bin + 29 + 1
        if start_bin > 0:
            start_bin = np.arange(-29-1, 0)[start_bin]
        if end_bin > 0:
            end_bin = np.arange(-29-1, 0)[end_bin]

    return start_bin, end_bin, replay_direction



def replay_wrap_traversed_bins(side, traversed_locations):
    """
    Similar to replay_wrap_bins_and_direction, corrects the bins
    outside of the bounds for an array of the full trajectory.
    """
    wrapped_bins = []

    if side == 'right':
        for bin in traversed_locations:
            if bin > 29:
                bin = bin - 29 - 1
            if bin < 0:
                bin = np.arange(29 + 1)[int(bin)]
            wrapped_bins.append(bin)

    elif side == 'left':
        for bin in traversed_locations:
            if bin < -29:
                bin = bin + 29 + 1
            if bin > 0:
                bin = np.arange(-29 - 1, 0)[int(bin)]
            wrapped_bins.append(bin)
    assert len(wrapped_bins) == len(traversed_locations)
    return np.array(wrapped_bins)


def filter_pbes_significance(sig_df, shuffles, p_vals):
    selected_pbes = []

    for shuffle, p_val in zip(shuffles, p_vals):
        dfs = sig_df[sig_df['shuffle_type'] == shuffle]
        mask = dfs['p_val'] <= p_val
        sel_pbes = dfs.loc[mask].index.tolist()
        selected_pbes.append(sel_pbes)

    if len(selected_pbes) > 0:
        selected_final = list(set.intersection(*map(set, selected_pbes)))
    else:
        selected_final = np.unique(sig_df.index).tolist()

    selected_final.sort()

    return selected_final



def filter_pbes(df, phase=None, epoch=None):
    """
    phase: 'iti', 'img', 'run', 'return' (or list)
    epoch: 'task', 'pre_sleep', 'post_sleep' (or list)
    """

    selected_pbes = []

    if phase is not None:
        if isinstance(phase, str):
            phase = [phase]
        mask = np.isin(df['phase'], phase)
        sel_pbes = df.loc[mask].index.tolist()
        selected_pbes.append(sel_pbes)

    if epoch is not None:
        if isinstance(epoch, str):
            epoch = [epoch]
        mask = np.isin(df['epoch'], epoch)
        sel_pbes = df.loc[mask].index.tolist()
        selected_pbes.append(sel_pbes)

    if len(selected_pbes) > 0:
        selected_final = list(set.intersection(*map(set, selected_pbes)))
    else:
        selected_final =df.index.tolist()

    selected_final.sort()

    return selected_final



def reindex_pbes_dfs(df):
    new_index = []
    for i, row in df.iterrows():

        if row['epoch'] == 'pre_sleep':
            a = 'pre'
        elif row['epoch'] == 'task':
            a = 'task'
        elif row['epoch'] == 'post_sleep':
            a = 'post'

        new = '{}_{}_{}'.format(row['sess_ind'], a, row.name)
        new_index.append(new)
    df.index = new_index
    return df




def hstack_quantity_arrays(arrays, units=None):
    """
    Stacks Quantity arrays (often spiketrains) by first converting them
    all to the same units and then readding the units to the stacked array.
    """
    if units is None:
        units = arrays[0].units

    assert isinstance(units, pq.Quantity)

    stacked_arr =  np.hstack([r.rescale(units).__array__() for r in arrays])
    return stacked_arr * units

