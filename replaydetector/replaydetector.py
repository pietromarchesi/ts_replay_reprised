import numpy as np
try:
    import matplotlib
    import matplotlib.pyplot as plt
    import seaborn as sns
    import matplotlib.gridspec as gridspec
except (ModuleNotFoundError, ImportError):
    print('Could not load matplotlib/seaborn. Plotting functionality of the'
          'Replay Detector will not work')
import pandas as pd
import quantities as pq
import copy
from replaydetector.replay_utils import fit_lines, get_best_fit, cycle_rows
from replaydetector.replay_utils import compute_jump_distance
import warnings



class ReplayDetector:

    def __init__(self, decoder, distance_between_bins_in_cm=None):

        self.dec = decoder
        self.bins = self.dec.bins
        self.bin_dist = distance_between_bins_in_cm
        self.available_shuffles = ['time_bin_shuffle',
                                   'column_cycle_shuffle',
                                   'unit_identity_shuffle',
                                   'place_field_rotation_shuffle']

        self.shuffles_labels = {'time_bin_shuffle' : 'Time bin\nshuffle',
                                'column_cycle_shuffle' : 'Column-cycle\nshuffle',
                                'unit_identity_shuffle' : 'Unit identity\nshuffle',
                                'place_field_rotation_shuffle' : 'Place field rotation\nshuffle'}


    def set_line_fitting_pars(self, n_bins_proba=2, bins_ext=20, subsample=3,
                              min_traj_bins=2, min_traj_speed_cm_s=100):
        self.n_bins_proba = n_bins_proba
        self.bins_ext = bins_ext
        self.subsample = subsample
        self.min_traj_bins = min_traj_bins
        self.min_traj_speed_cm_s = min_traj_speed_cm_s

        self.bins_extended = np.arange(self.bins[0] - self.bins_ext,
                             self.bins[-1] + self.bins_ext + 1, 1).astype(int)


    def add_pbe(self, spikes, start_time=None, end_time=None,
                bin_size_in_ms=None):
        # spikes are shape n_time_points x n_neurons
        # distance_between_bins_in_cm is used to compute a speed
        # of trajectory replay
        # start and end times are assumed to be quantities
        self.surr_scores = {}
        self.spikes = spikes
        self.time_bins = np.arange(0, spikes.shape[0]).astype(int)

        if start_time is not None:
            self.start_time = start_time
        else:
            self.start_time = self.time_bins[0]

        if end_time is not None:
            self.end_time = end_time
        else:
            self.end_time = self.time_bins[-1]

        if (start_time is not None) and (end_time is not None):
            self.traj_time = (self.end_time - self.start_time).rescale(pq.s)

        self.y_pred_proba = self.dec.predict_proba(self.spikes)
        no_spikes = np.where(self.spikes.sum(axis=1) == 0)[0]
        self.y_pred_proba[no_spikes, :] = 0
        print('Setting to zero prob. of {} time bins with no spikes'.format(no_spikes.shape[0]))


        #print(self.y_pred_proba.sum())
        self.binsize_in_ms=bin_size_in_ms

        try:
            self.n_bins_proba
        except AttributeError:
            warnings.warn('Using default line-fitting parameters', UserWarning)
            self.set_line_fitting_pars()

        line_fits, padded_y_pred_proba = fit_lines(self.y_pred_proba, self.bins,
                                     n_bins_proba=self.n_bins_proba,
                                     bins_ext=self.bins_ext,
                                     subsample=self.subsample)

        line_fits['n_bins_traversed'] = None
        line_fits['trajectory_speed'] = None

        for i, row in line_fits.iterrows():
            start_location, end_location = row['start_bin'], row['end_bin']

            n_bins_traversed = np.abs(end_location - start_location)
            trajectory_speed = self.bin_dist * n_bins_traversed * pq.cm / self.traj_time
            line_fits.loc[i, ['n_bins_traversed', 'trajectory_speed']] = [n_bins_traversed,
                                                                          trajectory_speed]

        self.line_fits_all = line_fits

        print('Excluding flat trajectories')
        line_fits = line_fits[line_fits['n_bins_traversed']>=self.min_traj_bins]
        # make sure units are cm/s before comparing to min_traj_speed_cm_s
        assert line_fits['trajectory_speed'].iloc[0].units == 1*pq.cm/pq.s
        line_fits = line_fits[line_fits['trajectory_speed']>=self.min_traj_speed_cm_s]

        start, end, score = get_best_fit(line_fits)

        self.line_fits = line_fits
        self.padded_y_pred_proba = padded_y_pred_proba
        self.start_location = start
        self.end_location   = end

        self.n_bins_traversed = np.abs(self.end_location - self.start_location)


        if self.bin_dist is not None:
            self.trajectory_speed = self.bin_dist * self.n_bins_traversed * pq.cm \
                                    / self.traj_time
        else:
            raise ValueError('Cannot compute trajectory distance')

        self.obs_score      = score

        self.trajectory_line = np.interp(x=self.time_bins,
                                        xp=[self.time_bins[0], self.time_bins[-1]],
                                        fp=[self.start_location, self.end_location])

        self.traversed_locations = self.trajectory_line.round().astype(int)

        assert self.traversed_locations.shape[0] == self.spikes.shape[0]

        self.sig_df = pd.DataFrame(columns=['shuffle_type',
                                            'p_val',
                                            'obs_score',
                                            'surr_mean',
                                            'z_score'])

    def get_y_pred_proba(self):
        return self.y_pred_proba

    def _compute_p_val(self, surr_scores):
        p_val = (surr_scores > self.obs_score).sum() / surr_scores.shape[0]
        return p_val

    def _compute_replay_z_score(self, surr_scores):
        z_score = (self.obs_score - surr_scores.mean()) / surr_scores.std()
        return z_score

    def get_replay_score(self):
        return self.obs_score


    def get_replay_z_score(self, shuffle_type):
        surr_scores = self.surr_scores[shuffle_type]
        return self._compute_replay_z_score(surr_scores)

    def compute_significance(self, n_surr, shuffle_type='time_bin_shuffle'):

        if shuffle_type == 'time_bin_shuffle':
            surr_scores = self._surr_scores_time_bin_shuffle(n_surr)
        elif shuffle_type == 'column_cycle_shuffle':
            surr_scores = self._surr_scores_column_cycle_shuffle(n_surr)
        elif shuffle_type == 'unit_identity_shuffle':
            surr_scores = self._surr_scores_unit_identity_shuffle(n_surr)
        elif shuffle_type == 'place_field_rotation_shuffle':
            surr_scores = self._surr_scores_place_field_rotation_shuffle(n_surr)
        else:
            raise ValueError('The specified shuffle type {} does not correspond'
                             'to any existing shuffle type.\nThe available'
                             'shuffle types are {}'.format(shuffle_type,
                                                           ', '.join(self.available_shuffles)))
        self.surr_scores[shuffle_type] = surr_scores
        p_val = self._compute_p_val(surr_scores)
        surr_mean = surr_scores.mean()
        z_score = self._compute_replay_z_score(surr_scores)

        ind = self.sig_df.shape[0]
        self.sig_df.loc[ind, :] = [shuffle_type, p_val, self.obs_score,
                                   surr_mean, z_score]
        return self.sig_df.loc[ind, :]


    def _plot_binned_spikes(self, ax, plot_cbar=True, vmin=None, vmax=None):

        sorting = self.dec.ratemap.argmax(axis=1).argsort()
        sorted_spikes = self.spikes[:, sorting]

        if vmax is not None:
            cmap = plt.get_cmap('Greys', vmax)
        else:
            cmap = plt.get_cmap('Greys', sorted_spikes.max() + 1)

        if vmin is not None and vmax is not None:
            cax = ax.imshow(sorted_spikes.T, origin='lower', cmap=cmap,
                            aspect='auto', vmin=vmin, vmax=vmax)
        else:
            cax = ax.imshow(sorted_spikes.T, origin='lower', cmap='Greys',
                               aspect='auto')
        if vmax is not None:
            cbar_max = vmax + 1
        else:
            cbar_max = sorted_spikes.max() + 1

        if plot_cbar:
            cbar = plt.colorbar(cax, ax=ax, pad=0.2,
                              ticks=np.arange(0, cbar_max).astype(int),
                                orientation='vertical')
            cbar.set_label('# of spikes', size=matplotlib.rcParams['font.size']-1)
        ax.set_yticklabels([])

        ax.set_ylabel('Neuron index (sorted)')

        if self.binsize_in_ms is not None:
            ticks = ax.get_xticks()

            ticklabels = (ticks * self.binsize_in_ms).astype(int)
            ax.set_xticklabels(ticklabels)
            ax.set_xlabel('Time [ms]')
        else:
            ax.set_xlabel('Time bins')

        return cax



    def _plot_decoded_pbe(self, ax, plot_cbar=True, vmin=None, vmax=None,
                          line_color='r'):

        #y = self.trajectory_line

        y = np.interp(x=self.time_bins,
                      xp=[self.time_bins[0], self.time_bins[-1]],
                      fp=[self.start_location, self.end_location])

        extent = [-0.5, self.time_bins.shape[0] - 0.5,
                  self.bins[0] - 0.5, self.bins[-1] + 0.5]

        if vmin is not None and vmax is not None:
            cax = ax.imshow(self.y_pred_proba.T, origin='lower', cmap='Blues',
                                aspect='auto', extent=extent, vmin=vmin, vmax=vmax)
        else:
            cax = ax.imshow(self.y_pred_proba.T, origin='lower', cmap='Blues',
                                aspect='auto',
                                extent=extent)

        if vmax is not None:
            cbar_max = vmax + 0.1
        else:
            cbar_max = self.y_pred_proba.max() + 0.1

        if plot_cbar:
            cbar = plt.colorbar(cax, ax=ax, pad=0.2,
                              ticks=np.arange(0, cbar_max, 0.2),
                                orientation='vertical')
            cbar.set_label('Probability', size=matplotlib.rcParams['font.size']-1)
        #cbar.ax.text(-0.05, 0.5, 'Probability', rotation=90)
        ax.plot(self.time_bins, y, c=line_color, ls=':', lw=3)

        ax.set_ylabel('Location')

        if self.binsize_in_ms is not None:
            ticks = ax.get_xticks()
            ticklabels = (ticks * self.binsize_in_ms).astype(int)
            ax.set_xticklabels(ticklabels)
            ax.set_xlabel('Time [ms]')
        else:
            ax.set_xlabel('Time bins')

        ax.set_ylim([self.bins[0] - 0.5, self.bins[-1]+0.5])

        return cax



    def _plot_decoded_pbe_padded(self, ax):

        #y = self.trajectory_line

        y = np.interp(x=self.time_bins,
                      xp=[self.time_bins[0], self.time_bins[-1]],
                      fp=[self.start_location, self.end_location])

        extent_ext = [-0.5, self.time_bins.shape[0] - 0.5,
                      self.bins_extended[0] - 0.5, self.bins_extended[-1] + 0.5]

        cax = ax.imshow(self.padded_y_pred_proba, extent=extent_ext, origin='lower',
                  cmap='Blues', aspect='auto')
        cbar = plt.colorbar(cax, ax=ax, pad=0.2,
                          ticks=np.arange(0, self.y_pred_proba.max() + 0.1, 0.2),
                            orientation='vertical')
        cbar.set_label('Probability', size=matplotlib.rcParams['font.size']-1)
        ax.plot(self.time_bins, y, c='r', ls=':')
        ax.axhline(self.bins[0] - 0.5, c='grey', lw=1)
        ax.axhline(self.bins[-1] + 0.5, c='grey', lw=1)
        ax.set_ylabel('Linearized bins [padded]')
        if self.binsize_in_ms is not None:
            ticks = ax.get_xticks()
            ticklabels = (ticks * self.binsize_in_ms).astype(int)
            ax.set_xticklabels(ticklabels)
            ax.set_xlabel('Time [ms]')
        else:
            ax.set_xlabel('Time bins')


    def _plot_decoded_pbe_all_lines(self, ax):

        for i, row in self.line_fits.iterrows():
            y = np.interp(x=self.time_bins,
                          xp=[self.time_bins[0], self.time_bins[-1]],
                          fp=[row['start_bin'], row['end_bin']])

        extent = [-0.5, self.time_bins.shape[0] - 0.5,
                  self.bins[0] - 0.5, self.bins[-1] + 0.5]

        cax = ax.imshow(self.y_pred_proba.T, origin='lower', cmap='Blues',
                            aspect='auto',
                            extent=extent)
        cbar = plt.colorbar(cax, ax=ax, pad=0.2,
                          ticks=np.arange(0, self.y_pred_proba.max() + 0.1, 0.2),
                            orientation='vertical')
        cbar.set_label('Probability', size=matplotlib.rcParams['font.size']-1)
        #cbar.ax.text(-0.05, 0.5, 'Probability', rotation=90)
        ax.plot(self.time_bins, y, c='r', ls=':')
        ax.set_xlabel('Time bins')
        ax.set_ylabel('Linearized bins')
        ax.set_ylim([self.bins[0] - 0.5, self.bins[-1]+0.5])



    def _plot_surrogate_distribution(self, ax, shuffle_type, plot_legend=False):
        surr_scores = self.surr_scores[shuffle_type]
        sns.distplot(surr_scores, color=sns.xkcd_rgb['steel'],
                     label='Shuffled distribution', ax=ax, norm_hist=True)
        ax.axvline(self.obs_score, c='r', ls=':', label='Observed score')
        if plot_legend:
            ax.legend(frameon=False, loc='upper left')
        ax.set_xlabel('{} scores'.format(self.shuffles_labels[shuffle_type]))
        #ax.set_yticks([])
        #ax.set_yticklabels('')
        ax.set_ylabel('Density')

    def plot_replay(self, shuffle_type=None, figsize_tweak_h=1,
                    figsize_tweak_w=1, padded=True):

        # TODO: does not work if shuffle_type is a list with only one element

        if shuffle_type is not None:
            if isinstance(shuffle_type, str):
                f, ax = plt.subplots(1, 3, figsize=[5*3*figsize_tweak_w, 4*figsize_tweak_h])
                self._plot_binned_spikes(ax[0])
                self._plot_decoded_pbe(ax[1])
                self._plot_surrogate_distribution(ax[2], shuffle_type)
            elif isinstance(shuffle_type, list):
                f = plt.figure(figsize=[5*figsize_tweak_w, 4*figsize_tweak_h])
                gs = gridspec.GridSpec(2, 6, height_ratios=[2, 1])
                ax1 = f.add_subplot(gs[0, 0:3])
                ax2 = f.add_subplot(gs[0, 3:])

                if len(shuffle_type) == 2:
                    ax3 = f.add_subplot(gs[1, 0:3])
                    ax4 = f.add_subplot(gs[1, 3:])
                    shuff_axis = [ax3, ax4]
                elif len(shuffle_type) == 3:
                    ax3 = f.add_subplot(gs[1, 0:2])
                    ax4 = f.add_subplot(gs[1, 2:4])
                    ax5 = f.add_subplot(gs[1, 4:6])
                    shuff_axis = [ax3, ax4, ax5]
                else:
                    raise ValueError('Plotting supports only 2 or 3 '
                                     'surrogate distributions')

                self._plot_binned_spikes(ax1)
                if padded:
                    self._plot_decoded_pbe_padded(ax2)
                else:
                    self._plot_decoded_pbe(ax2)
                for i, ax in enumerate(shuff_axis):
                    self._plot_surrogate_distribution(ax, shuffle_type[i])

        else:
            f, ax = plt.subplots(1, 2, figsize=[5 * 2 * figsize_tweak_w, 4*figsize_tweak_h])
            self._plot_binned_spikes(ax[0])
            self._plot_decoded_pbe(ax[1])
        plt.tight_layout()
        sns.despine(top=True, right=True)
        return f


    def _surr_scores_time_bin_shuffle(self, n_surr):

        surr_scores = np.zeros(n_surr)

        for n in range(n_surr):
            y_pred_shuf = self.y_pred_proba.copy()
            np.random.shuffle(y_pred_shuf)

            line_fits, _ = fit_lines(y_pred_shuf, self.bins,
                                  n_bins_proba=self.n_bins_proba,
                                  bins_ext=self.bins_ext,
                                  subsample=self.subsample)
            start, end, score = get_best_fit(line_fits)

            surr_scores[n] = score

        return surr_scores


    def _surr_scores_column_cycle_shuffle(self, n_surr):
        surr_scores = np.zeros(n_surr)

        for n in range(n_surr):
            y_pred_shuf = cycle_rows(self.y_pred_proba)

            line_fits, _ = fit_lines(y_pred_shuf, self.bins,
                                  n_bins_proba=self.n_bins_proba,
                                  bins_ext=self.bins_ext,
                                  subsample=self.subsample)
            start, end, score = get_best_fit(line_fits)
            surr_scores[n] = score
        return surr_scores


    def _surr_scores_unit_identity_shuffle(self, n_surr):
        surr_scores = np.zeros(n_surr)

        for n in range(n_surr):
            dec_surr = copy.deepcopy(self.dec)
            np.random.shuffle(dec_surr.ratemap)

            y_pred_shuf = dec_surr.predict_proba(self.spikes)
            line_fits, _ = fit_lines(y_pred_shuf, self.bins,
                                  n_bins_proba=self.n_bins_proba,
                                  bins_ext=self.bins_ext,
                                  subsample=self.subsample)
            start, end, score = get_best_fit(line_fits)
            surr_scores[n] = score
        return surr_scores


    def _surr_scores_place_field_rotation_shuffle(self, n_surr):
        surr_scores = np.zeros(n_surr)

        for n in range(n_surr):
            dec_surr = copy.deepcopy(self.dec)
            dec_surr.ratemap = cycle_rows(dec_surr.ratemap)
            y_pred_shuf = dec_surr.predict_proba(self.spikes)
            line_fits, _ = fit_lines(y_pred_shuf, self.bins,
                                  n_bins_proba=self.n_bins_proba,
                                  bins_ext=self.bins_ext,
                                  subsample=self.subsample)
            start, end, score = get_best_fit(line_fits)
            surr_scores[n] = score

        return surr_scores


    def compute_maximum_jump_distance(self):
        """
        Maximum distance between peak decoded positions in neighboring
        decoded time windows (skip the windows where you don't have spikes).
        """
        mask = self.spikes.sum(axis=1) > 0
        X = self.y_pred_proba[mask]
        jump_distance = compute_jump_distance(X)
        assert np.sum(np.isnan(jump_distance)) == 0
        return jump_distance.max() * self.bin_dist


    def compute_mean_jump_distance(self):
        """
        Average distance between peak decoded positions in neighboring
        decoded time windows (skip the windows where you don't have spikes).
        """
        mask = self.spikes.sum(axis=1) > 0
        X = self.y_pred_proba[mask]
        jump_distance = compute_jump_distance(X)
        assert np.sum(np.isnan(jump_distance)) == 0
        return jump_distance.mean() * self.bin_dist


    def compute_sharpness(self):

        return self.y_pred_proba.max()




