import numpy as np
from scipy.ndimage import gaussian_filter1d
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import scipy.stats
from utils import get_data_real_event_ratemaps
import statsmodels

"""
bins = numpy array of all the possible bins
time_bin_size_task_in_ms = size in ms of bins used to bin task data
time_bin_size_replay_in_ms = size in ms of bins used to bin replay events   
binned_spikes_task = numpy array of shape n_bins x n_neurons
binned_position_task = numpy array of bin labels
binned_spikes_event = list of numpy arrays or just numpy array
                      shape in n_bins x n_neurons
binned_position_event = list of numpy arrays of bin labels or just numpy array
                        shape of the array should be 1d
bootstrap = bool
bootstrap_n = int
bootstrap_perc = int

real_ratemaps = array of shape neurons x bins x bootstrap_n

RATEMAPS are in hertz (we convert the time in ms to s before dividing)
"""

"""
The ratemap in self.ratemap[ratemap_type] is never smoothed in place, 
it always remains the original one. 

"""

# TODO: make sure it works if you have just one neuron
# TODO smoothing seems to reduce the sd bands!

class ReplayRatemaps():


    def __init__(self, bins=None, bootstrap=False, bootstrap_n = 100,
                 bootstrap_perc=80, unit_labels=None):

        if bins is None:
            bins = np.arange(-29, 30)

        if isinstance(unit_labels, list):
            unit_labels = np.array(unit_labels)

        self.bins = bins
        self.unit_labels = unit_labels
        self.bootstrap = bootstrap
        self.bootstrap_n = bootstrap_n
        self.bootstrap_perc = bootstrap_perc
        self.colormap = {}
        self.colormap['real'] = sns.xkcd_rgb['green']
        self.colormap['event'] = sns.xkcd_rgb['blue']


    def build_real_ratemaps(self, binned_spikes_task, binned_position_task,
                            time_bin_size_task_in_ms):
        """
        binned_spikes_task should be either an array or a list of arrays
        of shape n_time_bins x n_neurons
        """

        self.binned_spikes_task = binned_spikes_task
        try:
            self.n_neurons = binned_spikes_task.shape[1]
        except AttributeError:
            self.n_neurons = binned_spikes_task[0].shape[1]
        self.binned_position_task = binned_position_task
        self.time_bin_size_task_in_ms = time_bin_size_task_in_ms

        self._build_real_event_ratemaps(ratemap_type='real')


    def build_event_ratemaps(self, binned_spikes_replay, binned_position_replay,
                             time_bin_size_replay_in_ms):

        self.binned_spikes_replay = binned_spikes_replay
        self.binned_position_replay = binned_position_replay
        self.time_bin_size_replay_in_ms = time_bin_size_replay_in_ms

        self._build_real_event_ratemaps(ratemap_type='event')


    def _sort_by_peak(self, ratemap_type, reverse=False):
        rm = self.ratemap[ratemap_type]

        if len(rm.shape) == 3:
            sorting = rm.mean(axis=2).argmax(axis=1).argsort()

        elif len(rm.shape) == 2:
            sorting = rm.argmax(axis=1).argsort()

        if reverse:
            sorting = sorting[::-1]

        self.ratemap[ratemap_type] = rm[sorting]
        self.unit_labels = self.unit_labels[sorting]


    def _smooth_ratemap(self, ratemap, sigma):

        ratemap = np.copy(ratemap)

        if len(ratemap.shape) == 3:
            for unit in range(ratemap.shape[0]):
                for boot in range(ratemap.shape[2]):
                    ratemap[unit, :, boot] = gaussian_filter1d(ratemap[unit, :, boot], sigma)

        elif len(ratemap.shape) == 2:
            for unit in zip(range(ratemap.shape[0])):
                ratemap[unit, :] = gaussian_filter1d(ratemap[unit, :], sigma)

        return ratemap

    def _build_real_event_ratemaps(self, ratemap_type):

        if not hasattr(self, 'ratemap'):
            self.ratemap = {}

        if ratemap_type == 'real':
            X = self.binned_spikes_task
            y = self.binned_position_task
            t = self.time_bin_size_task_in_ms
        elif ratemap_type == 'event':
            X = self.binned_spikes_replay
            y = self.binned_position_replay
            t = self.time_bin_size_replay_in_ms

        if self.bootstrap:
            ratemaps = []
            for n in range(self.bootstrap_n):
                spikes, pos = self._get_bootstrap_sample(X, y, self.bootstrap_perc)

                ratemap = self._build_ratemaps(spikes, pos, self.bins, t)
                ratemaps.append(ratemap)
            ratemap = np.stack(ratemaps, axis=2)

        else:
            X, y = self._prepare_data_for_ratemaps(X, y)
            ratemap = self._build_ratemaps(X, y, self.bins, t)

        self.ratemap[ratemap_type] = ratemap


    def _get_bootstrap_sample(self, X, y, percentage=80):

        # this function should be able to allow getting bootstrap samples
        # either from a list of arrays or from two arrays directly (if you want
        # to ignore trials and just bootstrap samples)

        # bootstrap always outputs numpy arrays which can be fed directly
        # into _build_ratemaps

        if isinstance(X, np.ndarray):
            n_samples = np.rint((percentage * X.shape[0]) / 100).astype(int)
            ind = np.random.choice(X.shape[0], n_samples)
            Xboot, yboot = X[ind, :], y[ind]

        elif isinstance(X, list) and isinstance(X[0], np.ndarray):
            n_samples = np.rint((percentage * len(X)) / 100).astype(int)
            ind = np.random.choice(len(X), n_samples)
            Xboot = [X[i] for i in ind]
            yboot = [y[i] for i in ind]
            Xboot, yboot = np.vstack(Xboot), np.hstack(yboot)

        return Xboot, yboot


    def _prepare_data_for_ratemaps(self, X, y):
        if isinstance(X, list) and isinstance(X[0], np.ndarray):
            X = np.vstack(X)
        if isinstance(y, list):
            if len(y[0].shape) > 1:
                raise ValueError('position vectors should be 1d')
            y = np.hstack(y)
        return X, y


    def _build_ratemaps(self, X, y, bins, bin_time_in_ms):
        # this function only operates on numpy arrays (not on list of arrays)
        # print(X.shape)
        # print(len(y))
        # print(np.array(y).astype(int))
        bins_visited = np.unique(y.copy()).shape[0]
        #print('Bins visited: {} out of {}'.format(bins_visited, bins.shape[0]))

        ratemap = np.zeros([X.shape[1], bins.shape[0]])

        for neuron_ind, neuron in enumerate(X.T):
            for bin_ind, bin in enumerate(bins):
                in_loc = y == bin
                total_spikes = neuron[in_loc].sum()
                total_time_in_s = (bin_time_in_ms / 1000 * neuron[in_loc].shape[0])
                ratemap[neuron_ind, bin_ind] = total_spikes / total_time_in_s

        nnan = np.isnan(ratemap).sum()
        if nnan > 0:
            # print('{} nans out of {} in the ratemap, some locations have not been visited'
            #       ' setting their ratemap value to 0'.format(nnan, len(bins)))
            ratemap[np.isnan(ratemap)] = 0
            assert np.isnan(ratemap).sum() == 0

        return ratemap


    def _normalize_ratemap(self, ratemap):

        if len(ratemap.shape) == 3:
            peak_firing_rates = np.nanmax(ratemap, axis=(1, 2))
        elif len(ratemap.shape) == 2:
            peak_firing_rates = np.nanmax(ratemap, axis=1)

        peak_firing_rates[peak_firing_rates == 0] = 1
        rm = (ratemap.T / peak_firing_rates).T

        return rm


    def plot_full_ratemap_simple(self, ratemap_type, smooth=False, sigma=1,
                                 color=None, shade_under_curve=True,
                                 pad_factor=3):

        rm = self.ratemap[ratemap_type]

        if smooth:
            rm = self._smooth_ratemap(rm, sigma)

        if len(rm.shape) == 3:
            rm = rm.mean(axis=2)

        n_units = rm.shape[0]

        rm = self._normalize_ratemap(rm)

        if color is None:
            palette = sns.color_palette("hls", n_units)
        else:
            palette = [color for _ in range(n_units)]

        # normalize ratemap [n_neurons, n_bins] (no bootstraps)
        peak_firing_rates = rm.max(axis=1)
        peak_firing_rates[peak_firing_rates == 0] = 1
        rm = (rm.T / peak_firing_rates).T
        pad = pad_factor * rm.mean()

        f, ax = plt.subplots(1, 1, figsize=[4, n_units * 0.2])
        for unit, curve in enumerate(rm):
            zorder = int(10 + 2 * n_units - 2 * unit)
            line = ax.plot(self.bins, unit * pad + curve, zorder=zorder, color=palette[unit])
            if shade_under_curve:
                fillcolor = line[0].get_color()
                ax.fill_between(self.bins, unit * pad, unit * pad + curve, alpha=0.3,
                                color=fillcolor, zorder=zorder-1)
        yticks = np.arange(n_units) * pad + 0.5 * pad
        ax.set_yticks(yticks)
        ax.set_yticklabels(self.unit_labels)
        ax.set_xticks(self.bins[::5])
        ax.set_xticklabels(self.bins[::5].astype(int))
        ax.set_ylabel('Neuron')
        ax.set_xlabel('Location')
        sns.despine(fig=f, top=True, right=True)

        # ax.axvspan(0, 3, alpha=0.1, facecolor='grey')
        # ax.axvspan(xvals[-6], xvals[-1], alpha=0.1, facecolor='grey',
        #            zorder=100)
        return f


    def plot_full_ratemap_bootstrap(self, ratemap_type, smooth=False, sigma=1,
                                    color=None, pad_factor=3, ci=95):

        if not self.bootstrap:
            raise ValueError('Boostrapping was set to False when initializing!')

        rm = self.ratemap[ratemap_type]

        if smooth:
            rm = self._smooth_ratemap(rm, sigma)

        rm = self._normalize_ratemap(rm)

        n_units = rm.shape[0]

        if color is None:
            palette = sns.color_palette("hls", n_units)
        else:
            palette = [color for _ in range(n_units)]

        pad = pad_factor * rm.mean()

        f, ax = plt.subplots(1, 1, figsize=[4, n_units * 0.2])

        for unit, curve_boots in enumerate(rm):
            dfs = []
            for i in range(self.bootstrap_n):
                df = pd.DataFrame(columns=['bin', 'fr', 'boot_ind'])
                df['bin'] = self.bins
                df['fr'] = unit * pad + curve_boots[:, i]
                df['boot_ind'] = i
                dfs.append(df)
            df = pd.concat(dfs)

            zorder = int(10 + 2 * n_units - 2 * unit)
            sns.lineplot(ax=ax, x='bin', y='fr', data=df, zorder=zorder,
                         color=palette[unit], err_style='band', ci=ci)

        yticks = np.arange(n_units) * pad + 0.5 * pad
        ax.set_yticks(yticks)
        ax.set_yticklabels(self.unit_labels)
        ax.set_xticks(self.bins[::5])
        ax.set_xticklabels(self.bins[::5].astype(int))
        ax.set_ylabel('Neuron')
        ax.set_xlabel('Location')
        sns.despine(fig=f, top=True, right=True)

        # ax.axvspan(0, 3, alpha=0.1, facecolor='grey')
        # ax.axvspan(xvals[-6], xvals[-1], alpha=0.1, facecolor='grey',
        #            zorder=100)
        return f



    def _correlate_ratemaps(self, rm1, rm2, correlation_method='pearson'):
        if correlation_method == 'pearson':
            corr_mat_full = np.corrcoef(rm1, rm2, rowvar=True)
        elif correlation_method == 'spearman':
            corr = scipy.stats.spearmanr(rm1, rm2, axis=1)
            corr_mat_full = corr.correlation
        corrcoef = corr_mat_full.diagonal(rm1.shape[0]).copy()
        assert corrcoef.shape[0] == rm2.shape[0]
        corrcoef[np.isnan(corrcoef)] = 0
        return corrcoef


    def random_roll(self, arr):
        """
        roll each row of r independently by a randomly drawn amount
        """
        roll = np.random.randint(0, arr.shape[1], size=arr.shape[0])
        rolled =  np.vstack([np.roll(row, x) for row, x in zip(arr, roll)])
        np.testing.assert_array_equal(arr.shape, rolled.shape)
        return rolled

    def correlate_ratemaps(self, correlation_method='pearson',
                           null_distribution_method='rotate_ratemap',
                           smooth=False, sigma=1, n_surrogates=100):

        """
        out['corrcoef_obs'] is an array of shape n_neurons with the correlation
        between real and event ratemaps for each neuron
        out['corrcoef_surr'] is an array of shape n_neurons x n_surrogates
        n_surrogates is determined automatically and it is all the possible
        shifts. It contains the correlation values obtained by shifting the
        real ratemap by all the possible shifts.
        out['percentile_rank'] is an array of shape n_neurons with the percentile
        rank of the observed correlation wrt the surrogate correlations
        (percentage of surrogate scores which are lower)
        out['p_values'] is an array of shape n_neurons which contains a p-value
        per neuron computed as the fraction of surrogate values which are
        higher than the observed ratemap correlation
        out['p_val_percentile_rank'] is a p-value obtained with a sign test
        comparing the percentile ranks of the population against
        n_surrogates: only used if null_distribution_method = 'rotate_spikes'
        """

        assert np.isin(null_distribution_method, ['rotate_ratemap', 'rotate_spikes'])

        rm1 = self.ratemap['real']
        rm2 = self.ratemap['event']

        if len(rm1.shape) == 3:
            rm1 = rm1.mean(axis=2)

        if len(rm2.shape) == 3:
            rm2 = rm2.mean(axis=2)

        if smooth:
            rm1 = self._smooth_ratemap(rm1, sigma)
            rm2 = self._smooth_ratemap(rm2, sigma)


        n_units = rm1.shape[0]

        corrcoef_obs = self._correlate_ratemaps(rm1, rm2,
                                        correlation_method=correlation_method)


        if null_distribution_method == 'rotate_ratemap':
            n_surrogates = rm1.shape[1] - 1
            corrcoef_surr = np.zeros([n_units, n_surrogates])

            shifts = range(1, rm1.shape[1] - 1)
            for i, shift in enumerate(shifts):
                surr_rm = np.roll(rm1, shift=shift, axis=1)
                corrcoef_surr[:, i] = self._correlate_ratemaps(surr_rm, rm2,
                                            correlation_method=correlation_method)

        elif null_distribution_method == 'rotate_spikes':
            #print('\nMaking spike rotation surrogates')
            corrcoef_surr = np.zeros([n_units, n_surrogates])
            for n_surr in range(n_surrogates):
                X = [self.random_roll(a) for a in self.binned_spikes_replay]
                y = self.binned_position_replay
                t = self.time_bin_size_replay_in_ms
                X, y = self._prepare_data_for_ratemaps(X, y)
                surr_rm = self._build_ratemaps(X, y, self.bins, t)

                corrcoef_surr[:, n_surr] = self._correlate_ratemaps(surr_rm, rm2,
                                            correlation_method=correlation_method)

        perc_score = np.zeros(n_units)
        for unit in range(n_units):
            perc_score[unit] = scipy.stats.percentileofscore(
                corrcoef_surr[unit, :],
                corrcoef_obs[unit],
                kind='weak')

        p_vals = np.zeros(n_units)
        for unit in range(n_units):
            obs_val = corrcoef_obs[unit]
            surr_vals = corrcoef_surr[unit, :]
            # >= so if all obs and surr values are the same we get p=1
            p_vals[unit] = (surr_vals >= obs_val).sum() / n_surrogates

        corrcoef_debiased = corrcoef_obs - corrcoef_surr.mean(axis=1)

        out = {}
        out['corrcoef_obs'] = corrcoef_obs
        out['corrcoef_surr'] = corrcoef_surr
        out['corrcoef_debiased'] = corrcoef_debiased
        out['percentile_rank'] = perc_score
        out['p_values'] = p_vals
        out['null_distribution_method'] = null_distribution_method

        self.corr = out

        return out


    def plot_ratemap_single_unit(self, unit_ind, smooth=True, sigma=1, normalize=True,
                                 unit_traces=True, ci=95,
                                 ratemap_type='both'):

        if ratemap_type == 'both':
            ratemap_types = ['real', 'event']
        else:
            ratemap_types = [ratemap_type]

        f, ax = plt.subplots(1, 1, figsize=[4, 4])

        for type in ratemap_types:
            rm = self.ratemap[type]
            if smooth:
                rm = self._smooth_ratemap(rm, sigma=sigma)
            if normalize:
                rm = self._normalize_ratemap(rm)

            dfs = []
            for i in range(self.bootstrap_n):
                df = pd.DataFrame(columns=['bin', 'fr', 'boot_ind'])
                df['bin'] = self.bins
                df['fr'] = rm[unit_ind, :, i]
                df['boot_ind'] = i
                dfs.append(df)
            df = pd.concat(dfs)

            if unit_traces:
                sns.lineplot(ax=ax, x='bin', y='fr', data=df, estimator=None,
                             color=self.colormap[type], units='boot_ind',
                             ci='sd', alpha=0.2)
            else:
                sns.lineplot(ax=ax, x='bin', y='fr', data=df,
                             color=self.colormap[type], err_style='band', ci=ci)

        if normalize:
            ax.set_ylabel('Normalized firing rate')
        else:
            ax.set_ylabel('Firing rate [Hz]')
        ax.set_xlabel('Location')
        ax.set_title('Unit {}'.format(unit_ind))
        sns.despine()
        plt.tight_layout()

        return f



if __name__ == '__main__':
    from constants import *

    # data = get_data_real_event_ratemaps(DATA_FOLDER, REPLAY_FOLDER,
    #                              sess_ind=7, area='Perirhinal', PBEs_settings='tue2_HP_chop',
    #                              PBEs_area='Hippocampus',
    #                              PBEs_decoding_settings='dkw',
    #                              shuffle_type='column_cycle_shuffle',
    #                              only_pyramidal=True,
    #                              p_val_threshold_hc=0.1,
    #                              time_bin_size_task_in_ms=200,
    #                              min_speed=5,
    #                              bin_size_pbe_in_ms='use_default',
    #                              slide_by_pbe_in_ms='use_default')
    #
    # bins = np.arange(-29, 30)
    #
    # rp = ReplayRatemaps(bins, bootstrap=True, bootstrap_perc=50)
    #
    # rp.build_real_ratemaps(data['binned_spikes_task'], data['binned_position_task'],
    #                     data['time_bin_size_task_in_ms'])
    #
    # rp.build_event_ratemaps(data['binned_spikes_pbe'], data['binned_position_pbe'],
    #                     data['time_bin_size_pbe_in_ms'])
    #
    # corr = rp.correlate_ratemaps()
    #
    # sig = corr['p_values'][corr['p_values'] < 0.1]
    # print(sig.__len__())


