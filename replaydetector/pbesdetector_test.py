import numpy as np
import neo
import elephant
import quantities as pq
import scipy.signal
import matplotlib.pyplot as plt
import seaborn as sns
from replaydetector.pbesdetector import PBEsDetector

t_start_in_ms = 0
t_stop_in_ms  = 70

times = [np.array([2, 3, 4, 5, 15, 16, 17, 18, 52, 53, 54, 55]) * pq.ms,
          np.array([4, 5, 6, 18, 19, 20, 21, 22]) * pq.ms,
          np.array([3, 4, 5, 6, 18]) * pq.ms,
          np.array([18, 25, 28, 30, 32, 55, 57, 58, 59]) * pq.ms,
          np.array([3, 4, 30, 31, 32, 50, 51, 52, 53, 54, 55, 56, 57]) * pq.ms,
          np.array([2, 3, 4, 5, 6, 7, 31, 32, 33]) * pq.ms,
          np.array([31, 33, 35, 47, 48, 52, 53, 56, 57]) * pq.ms,
          np.array([32, 33, 34, 38, 53, 55, 56, 59]) * pq.ms]


trains = []
for tm in times:
    trains.append(neo.SpikeTrain(times=tm, t_start=t_start_in_ms * pq.ms,
                                 t_stop=t_stop_in_ms * pq.ms))


speed_times = np.arange(t_start_in_ms, t_stop_in_ms, 5)
speed = np.zeros_like(speed_times)
speed[speed.shape[0] // 2 :] = 3

pbedec = PBEsDetector()
pbedec.set_detection_parameters(n_sd_above_the_mean=1,
                                min_duration_in_ms=6,
                                min_n_spikes_only_selected_cells=False,
                                min_n_spikes=15)

df = pbedec.detect_candidate_events(trains, t_start_in_ms, t_stop_in_ms,
                               speed=speed, speed_times=speed_times)

min_d = pbedec.min_duration_in_ms * pq.ms
max_d = pbedec.max_duration_in_ms * pq.ms
ind = pbedec.pbes.index[(pbedec.pbes['event_duration'].as_matrix() >= min_d) &
                        (pbedec.pbes['event_duration'].as_matrix() <= max_d)]

pbedec._detect_PBEs()

print(pbedec.pbes)
print(pbedec.smooth_mua)

f, ax = plt.subplots()
ax.bar(x=np.arange(pbedec.mua.shape[0]), height=pbedec.mua * 1000, alpha=0.2)
ax.plot(pbedec.smooth_mua)
ax.axhline(pbedec.mean, ls=':', c='b')
ax.axhline(pbedec.threshold, ls=':', c='r')

pbedec._filter_min_n_spikes()
pbedec._filter_average_speed()
print(pbedec.pbes)

index = pbedec._get_intersection_of_filters()

pbedec.pbes.loc[[0, 2]]


# pbe_modulation = []
#
# for i, train in enumerate(trains):
#     spikes = train.times
#
#     n_spikes_pbes, n_spikes_non_pbe = 0, 0
#     duration_pbes, duration_non_pbe = 0 * pq.s, 0 * pq.s
#
#     for t1, t2 in pbe_times:
#         n_spikes = np.logical_and(spikes >= t1, spikes <= t2).sum()
