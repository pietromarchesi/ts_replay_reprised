import numpy as np
import elephant
import pandas as pd
import quantities as pq
import scipy.signal
from replaydetector.replay_utils import hstack_quantity_arrays
# a list of spike trains
# t_start, t_stop
# speed vector
# speed vector times
# a boolean vector of selected neurons, to allow flexibility for if
# you want to select only pyramidal, or pyramidal + place cells, etc.


"""
PARAMETERS:

- `mua_only_selected_cells`:
PBE detection begins with computed the multi-unit activity (MUA) histogram, 
smoothing it, and finding epochs where it exceeds a certain number of standard 
deviations from the mean. The MUA can be computed using all cells (e.g. including
interneurons) or directly only looking at selected cells (e.g. pyramidal neurons). 
Note: if `mua_only_selected_cells` is `True`, and `adjust_bounds_to_first_last_spikes` 
is also `True`, then the start and end time of each PBE will automatically be 
adjusted based on the first and last spike of the selected cells only. On 
the other hand, if `mua_only_selected_cells` is `False`, the bounds will be 
adjusted to the first and last spike considering all cells.
"""

# TODO: right now does not support passing and x and y position and determining
# where the PBEs occurred

'''
All criteria are imposed as >= or <=

selected_cells is a list of indices or boolean mask

Currently if something is not imposed as a criterion, then it will not 
be added to the dataframe. 
'''

# TODO add in the df the number of total spikes and number of selected spikes



class PBEsDetector():

    def __init__(self):

        self.filters = ['average_speed', 'min_spikes', 'duration',
                        'percentage_active_selected_cells',
                        'minimum_n_active_selected_cells']
        self.filtered_index = {}

        pass


    def set_detection_parameters(self, **kwargs):

        prop_defaults = {'mua_kernel_sigma_in_ms'                    : 2,
                         'mua_kernel_size_in_ms'                     : 10,
                         'mua_only_selected_cells'                   : False,
                         'n_sd_above_the_mean'                       : 3,
                         'adjust_bounds_to_first_last_spikes'        : True,
                         'adjust_bounds_only_selected_cells'         : True,
                         'min_average_speed'                         : None,
                         'max_average_speed'                         : None,
                         'min_duration_in_ms'                        : None,
                         'max_duration_in_ms'                        : None,
                         'min_n_spikes_only_selected_cells'          : False, # bool
                         'min_n_spikes'                              : None,
                         'min_percentage_active_only_selected_cells' : True,
                         'min_percentage_active'                     : None,
                         'min_n_active_only_selected_cells'          : False,
                         'min_n_active'                              : None}


        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))


    def get_pars_dictionary(self):

        pars_dict = {}

        props = ['mua_kernel_sigma_in_ms', 'mua_kernel_size_in_ms',
                 'mua_only_selected_cells', 'n_sd_above_the_mean',
                 'adjust_bounds_to_first_last_spikes',
                 'adjust_bounds_only_selected_cells', 'min_average_speed',
                 'max_average_speed', 'min_duration_in_ms',
                 'max_duration_in_ms', 'min_n_spikes_only_selected_cells',
                 'min_n_spikes', 'min_percentage_active_only_selected_cells',
                 'min_percentage_active', 'min_n_active_only_selected_cells',
                 'min_n_active']

        for prop in props:
            pars_dict[prop] = getattr(self, prop)

        return pars_dict


    def detect_candidate_events(self, trains, t_start_in_ms=None, t_stop_in_ms=None,
                                selected_cells=None, speed=None,
                                speed_times=None):

        self.trains = [t.rescale(pq.ms) for t in trains]

        if t_start_in_ms is None:
            units = trains[0][0].units
            self.t_start = np.min([np.min(t) for t in trains if len(t)>0]) * units
        else:
            self.t_start = t_start_in_ms * pq.ms

        if t_stop_in_ms is None:
            units = trains[0][0].units
            self.t_stop = np.max([np.max(t) for t in trains if len(t)>0]) * units
        else:
            self.t_stop = t_stop_in_ms * pq.ms

        need_selected_cells = self.min_n_spikes_only_selected_cells or \
                              self.min_percentage_active_only_selected_cells or \
                              self.min_n_active_only_selected_cells

        if selected_cells is None and need_selected_cells:
            raise ValueError('The chosen settings require an index of selected '
                             'cells which has not been provided')

        # convert to array of indices
        selected_cells = np.array(selected_cells)
        if selected_cells.dtype == 'bool':
            selected_cells = np.arange(len(self.trains))[selected_cells]

        self.selected_cells = selected_cells
        self.speed = speed
        self.speed_times = speed_times

        self._detect_PBEs()
        self._apply_filters()
        # get selected events gets the intersection of all the applied filters
        self.selected_pbes = self._get_selected_events()
        self.selected_pbes = self.selected_pbes.sort_values(by='start_time_in_ms')

        print('\n-> {} PBEs selected'.format(self.selected_pbes.shape[0]))

        self._run_checks()

        return self.selected_pbes


    def _adjust_dataframe(self, dataframe):

        # remove quantities
        quantity_columns = ['start_time_in_ms', 'end_time_in_ms', 'event_duration_in_ms']

        for col in quantity_columns:
            if np.isin(col, dataframe.columns.tolist()):
                dataframe[col] = dataframe[col].__array__().astype(float)

        # make numeric
        numeric_columns = ['bin_index_start', 'bin_index_end', 'start_time_in_ms',
                           'end_time_in_ms', 'percentage_active', 'n_active',
                           'event_duration_in_ms', 'average_speed']

        for col in numeric_columns:
            if np.isin(col, dataframe.columns.tolist()):
                dataframe[col] = pd.to_numeric(dataframe[col])

        # round columns
        round_dict = {'start_time_in_ms'     : 2,
                      'end_time_in_ms'       : 2,
                      'percentage_active'    : 1,
                      'event_duration_in_ms' : 2,
                      'average_speed'        : 3}

        for col in round_dict.keys():
            if np.isin(col, dataframe.columns.tolist()):
                dataframe = dataframe.round({col : round_dict[col]})

        return dataframe


    def _apply_filters(self):
        """
        This function applies the filters, the resulting selected indices are
        stored in self.filtered_index, then self._get_intersection_of_filters
        is used to combined them.
        """
        print('\n-> Filtering PBEs')

        if self.min_duration_in_ms is not None or self.max_duration_in_ms is not None:
            self._filter_event_duration()
            self._log_filter('duration', 'duration')

        if self.min_percentage_active is not None and self.min_percentage_active > 0:
            self._filter_min_percentage_active()
            self._log_filter('min % active neuron', 'min_percentage_active')

        if self.min_n_active is not None and self.min_n_active > 0:
            self._filter_min_n_active()
            self._log_filter('min n. active neuron', 'min_n_active')

        if self.min_n_spikes is not None and self.min_n_spikes > 0:
            self._filter_min_n_spikes()
            self._log_filter('min n. of spikes', 'min_n_spikes')

        if self.min_average_speed is not None or self.max_average_speed is not None:
            self._filter_average_speed()
            self._log_filter('average speed', 'average_speed')


    def _log_filter(self, filter_string, filter_name):
        n_pass = self.filtered_index[filter_name].shape[0]
        n_total = self.pbes.shape[0]
        percentage = 100 * n_pass / n_total
        print('\n---> Filtering {}'.format(filter_string))
        print('-----> {:.1f}% of PBEs satisfy the requirement'.format(percentage))


    def _run_checks(self):

        if np.isin('event_duration_in_ms', self.selected_pbes.columns.tolist()):
            print('Checking event duration')
            if self.min_duration_in_ms is not None:
                assert self.selected_pbes['event_duration_in_ms'].min() >= self.min_duration_in_ms
            if self.max_duration_in_ms is not None:
                assert self.selected_pbes['event_duration_in_ms'].min() <= self.max_duration_in_ms

        if np.isin('percentage_active', self.selected_pbes.columns.tolist()):
            print('Check percentage of active neurons')
            assert self.selected_pbes['percentage_active'].min() >= self.min_percentage_active

        if np.isin('n_active', self.selected_pbes.columns.tolist()):
            print('Check number of active neurons')
            assert self.selected_pbes['n_active'].min() >=self.min_n_active

        if np.isin('n_spikes', self.selected_pbes.columns.tolist()):
            print('Check number of spikes')
            assert self.selected_pbes['n_spikes'].min() >= self.min_n_spikes

        if np.isin('average_speed', self.selected_pbes.columns.tolist()):
            print('Checking average speed')
            if self.min_average_speed is not None:
                assert self.selected_pbes['average_speed'].min() >= self.min_average_speed
            if self.max_average_speed is not None:
                assert self.selected_pbes['average_speed'].min() <= self.max_average_speed



    def _get_intersection_of_filters(self):
        sets = [set(self.filtered_index[i].tolist())
                for i in self.filtered_index.keys()]
        intersection = list(set.intersection(*sets))
        return intersection


    def _get_selected_events(self):
        index = self._get_intersection_of_filters()
        return self.pbes.loc[index]


    def print_summary_of_events(self):
        pass

    def _get_trains(self, only_selected_cells=False):
        if only_selected_cells:
            trains =  [t for i, t in enumerate(self.trains)
                       if np.isin(i, self.selected_cells)]
        else:
            trains = self.trains
        return trains

    def _smooth_1d(self, array, kernel_size, kernel_sigma):
        kernel = scipy.signal.gaussian(M=kernel_size, std=kernel_sigma, sym=True)
        kernel = 1000 * kernel / kernel.sum()
        # TODO boundary effects can still be visible here
        return np.convolve(array, kernel, mode='same')

    def _detect_PBEs(self):

        print('-> Running PBE detection')
        trains = self._get_trains(only_selected_cells=False)

        bs = elephant.conversion.BinnedSpikeTrain(trains,
                                                  binsize=1 * pq.ms,
                                                  t_start=self.t_start,
                                                  t_stop=self.t_stop)

        # TODO horrible fix but I think BinnedSpikeTrain is fucked
        binned_spikes = bs.to_array()
        bin_centers = bs.bin_centers * 1000
        bin_edges = bs.bin_edges
        bin_edges = bin_edges.rescale(pq.ms) * 1000

        self.mua = binned_spikes.sum(axis=0)
        self.smooth_mua = self._smooth_1d(self.mua,
                                          kernel_size=self.mua_kernel_size_in_ms,
                                          kernel_sigma=self.mua_kernel_sigma_in_ms)

        if self.selected_cells is not None:
            # mua for selected cells is computed only if an index was passed
            # to detect_candidate_events
            self.mua_selected = binned_spikes[self.selected_cells, :].sum(axis=0)
            self.smooth_mua_selected = self._smooth_1d(self.mua_selected,
                                                       kernel_size=self.mua_kernel_size_in_ms,
                                                       kernel_sigma=self.mua_kernel_sigma_in_ms)


        if self.mua_only_selected_cells:
            mua = self.mua_selected
            smooth_mua = self.smooth_mua_selected
        else:
            mua = self.mua
            smooth_mua = self.smooth_mua

        mean = smooth_mua.mean()
        std  = smooth_mua.std()
        threshold = mean + self.n_sd_above_the_mean * std
        self.mua_threshold = threshold
        self.mua_mean = mean

        crossings_up = [i for i in range(1, smooth_mua.shape[0])
                        if (smooth_mua[i - 1] <= mean) and (smooth_mua[i] > mean)]

        crossings_down = [i for i in range(1, smooth_mua.shape[0])
                          if (smooth_mua[i - 1] > mean) and (smooth_mua[i] <= mean)]

        # if the first crossing is up, no problem. If it's down, remove it.
        if crossings_up[0] < crossings_down[0]:
            pbe_bounds_index = list(zip(crossings_up, crossings_down))
        else:
            pbe_bounds_index = list(zip(crossings_up, crossings_down[1:]))

        pbe_bounds_index = [t for t in pbe_bounds_index if
                            smooth_mua[t[0]: t[1]].max() > threshold]

        pbe_bounds_times = [[bin_edges[t[0]], bin_edges[t[1] + 1]] for t in
                            pbe_bounds_index]

        self.pbe_bounds_times = pbe_bounds_times
        if self.adjust_bounds_to_first_last_spikes:
            pbe_bounds_times = self._adjust_bounds_to_first_last_spikes(pbe_bounds_times,
                                   only_selected_cells=self.adjust_bounds_only_selected_cells)
            self.pbe_bounds_times_ajusted = pbe_bounds_times

        PBEs_start_bin = [pbe[0] for pbe in pbe_bounds_index]
        PBEs_end_bin = [pbe[1] for pbe in pbe_bounds_index]

        PBEs_start_times = [pbe[0] for pbe in pbe_bounds_times]
        PBEs_end_times = [pbe[1] for pbe in pbe_bounds_times]

        df = pd.DataFrame(columns=['bin_index_start', 'bin_index_end',
                                    'start_time_in_ms', 'end_time_in_ms'])

        df['bin_index_start']  = PBEs_start_bin
        df['bin_index_end']    = PBEs_end_bin
        df['start_time_in_ms'] = PBEs_start_times
        df['end_time_in_ms']   = PBEs_end_times
        df.index.name = 'event_number'

        self.mua  = mua
        self.smooth_mua = smooth_mua
        self.mean = mean
        self.threshold = threshold
        self.bin_centers = bin_centers
        self.bin_edges = bin_edges
        self.pbes = self._adjust_dataframe(df)

        print('---> {} PBEs detected'.format(self.pbes.shape[0]))



    def _adjust_bounds_to_first_last_spikes(self, pbe_bounds_times,
                                            only_selected_cells=False):

        trains = self._get_trains(only_selected_cells=only_selected_cells)
        all_spikes = hstack_quantity_arrays(trains, units=pq.ms)

        for times in pbe_bounds_times:
            t1 = times[0];
            t2 = times[1]

            spikes_in_event = all_spikes[(all_spikes >= t1) & (all_spikes <= t2)]

            first_spike_time = spikes_in_event[(spikes_in_event - t1).argmin()]
            last_spike_time = spikes_in_event[(t2 - spikes_in_event).argmin()]

            times[0] = first_spike_time - 2*pq.ms
            times[1] = last_spike_time + 2*pq.ms
        return pbe_bounds_times

    def _get_pbes_times(self):
        return list(zip(self.pbes['start_time_in_ms'], self.pbes['end_time_in_ms']))

    def _get_pbes_bin_indices(self):
        return list(zip(self.pbes['bin_index_start'], self.pbes['bin_index_end']))


    def _add_average_speed(self):
        pbe_times = self._get_pbes_times()
        speed = self.speed
        speed_times = self.speed_times
        average_speed = []
        for t1, t2 in pbe_times:
            ind = np.logical_and(speed_times >= t1, speed_times <= t2)
            mean_speed = np.nanmean(speed[ind])
            average_speed.append(mean_speed)

        self.pbes['average_speed'] =  average_speed
        n_nan_speeds = np.isnan(self.pbes['average_speed']).sum()
        print('Removing {} (of {}) PBE events because speed could not be '
              'determined'.format(n_nan_speeds, self.pbes.shape[0]))
        self.pbes = self.pbes[~np.isnan(self.pbes['average_speed'])]


    def _filter_average_speed(self):
        self._add_average_speed()
        if self.min_average_speed is None:
            self.min_average_speed = 0
        if self.max_average_speed is None:
            self.max_average_speed = 1e9
        ind = self.pbes.index[(self.pbes['average_speed'] >= self.min_average_speed) &
                              (self.pbes['average_speed'] <= self.max_average_speed)]
        self.filtered_index['average_speed'] = ind


    def _add_event_duration(self):
        self.pbes['event_duration_in_ms'] = self.pbes['end_time_in_ms'] - self.pbes['start_time_in_ms']


    def _filter_event_duration(self):
        self._add_event_duration()
        if self.min_duration_in_ms is None:
            self.min_duration_in_ms = 0
        if self.max_duration_in_ms is None:
            self.max_duration_in_ms = 1e9
        min_d = self.min_duration_in_ms
        max_d = self.max_duration_in_ms
        ind = self.pbes.index[(self.pbes['event_duration_in_ms'].values >= min_d) &
                              (self.pbes['event_duration_in_ms'].values <= max_d)]
        self.filtered_index['duration'] = ind


    def _compute_n_active(self, trains, t_start, t_stop, percentage=False):
        spikes_in_interval = [np.logical_and(t >= t_start, t <= t_stop).sum() for t in trains]
        n_active = (np.array(spikes_in_interval) > 0).sum()
        if percentage:
            n_active = 100 * n_active / len(trains)
        return n_active


    def _add_percentage_active(self, only_selected_cells):
        trains = self._get_trains(only_selected_cells=only_selected_cells)
        pbe_times = self._get_pbes_times()
        percentage_active = []
        for t1, t2 in pbe_times:
            pa = self._compute_n_active(trains, t1, t2, percentage=True)
            percentage_active.append(pa)

        self.pbes['percentage_active'] =  percentage_active


    def _filter_min_percentage_active(self):
        only_selected_cells = self.min_percentage_active_only_selected_cells
        self._add_percentage_active(only_selected_cells=only_selected_cells)
        ind = self.pbes.index[(self.pbes['percentage_active']) >= self.min_percentage_active]
        self.filtered_index['min_percentage_active'] = ind


    def _add_n_active(self, only_selected_cells):
        trains = self._get_trains(only_selected_cells=only_selected_cells)
        pbe_times = self._get_pbes_times()
        percentage_active = []
        for t1, t2 in pbe_times:
            pa = self._compute_n_active(trains, t1, t2, percentage=False)
            percentage_active.append(pa)
        self.pbes['n_active'] =  percentage_active


    def _filter_min_n_active(self):
        only_selected_cells = self.min_n_active_only_selected_cells
        self._add_n_active(only_selected_cells=only_selected_cells)
        ind = self.pbes.index[(self.pbes['n_active']) >= self.min_n_active]
        self.filtered_index['min_n_active'] = ind
        pass


    def _compute_n_spikes(self, trains, t_start, t_stop):
        spikes_in_interval = [np.logical_and(t >= t_start, t <= t_stop).sum() for t in trains]
        return np.sum(spikes_in_interval)


    def _add_n_spikes(self, only_selected_cells):
        trains = self._get_trains(only_selected_cells=only_selected_cells)
        pbe_times = self._get_pbes_times()
        n_spikes = []
        for t1, t2 in pbe_times:
            pa = self._compute_n_spikes(trains, t1, t2)
            n_spikes.append(pa)
        self.pbes['n_spikes'] =  n_spikes


    def _filter_min_n_spikes(self):
        self._add_n_spikes(only_selected_cells=self.min_n_spikes_only_selected_cells)
        ind = self.pbes.index[(self.pbes['n_spikes'] >= self.min_n_spikes)]
        self.filtered_index['min_n_spikes'] = ind




# smooth_mua = np.array([4, 1, 3, 1, 1, 1, 1, 2, 3, 3, 3, 2, 1])
#
# mean = 1.99
#
# crossings_up = [i for i in range(1, smooth_mua.shape[0])
#                 if (smooth_mua[i - 1] <= mean) and (smooth_mua[i] > mean)]
#
# crossings_down = [i for i in range(1, smooth_mua.shape[0])
#                   if (smooth_mua[i - 1] > mean) and (smooth_mua[i] <= mean)]
#
# if crossings_up[0] < crossings_down[0]:
#     pbe_bounds_index = list(zip(crossings_up, crossings_down))
# else:
#     pbe_bounds_index = list(zip(crossings_up, crossings_down[1:]))
#
# smooth_mua[pbe_bounds_index[0][0]:pbe_bounds_index[0][1]]
#
# smooth_mua[pbe_bounds_index[1][0]:pbe_bounds_index[1][1]]

# pbe_bounds_times = [[bin_edges[t[0]], bin_edges[t[1] + 1]] for t in
#                     pbe_bounds_index_0]
#
# all_spikes = hstack_quantity_arrays(trains, pq.ms)
#
#
#
# for times in pbe_bounds_times:
#     t1 = times[0]; t2 = times[1]
#
#     spikes_in_event = all_spikes[(all_spikes >= t1) & (all_spikes <= t2)]
#
#     first_spike_time = spikes_in_event[(spikes_in_event - t1).argmin()]
#     last_spike_time  = spikes_in_event[(t2 - spikes_in_event).argmin()]
#
#     times[0] = first_spike_time
#     times[1] = last_spike_time
#
