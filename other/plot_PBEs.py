import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(sys.path[0]))))
import numpy as np
from utils import bin_PBEs
import pandas as pd
import neo
import quantities as pq
import elephant
import pickle
import scipy
from loadmat import loadmat
from session_selection import DATA_FOLDER, REPLAY_FOLDER, sessions_HC_replay
from replaydetector.pbesdetector import PBEsDetector
from utils import find_index_nearest_value, find_index_nearest_preceding
from utils import get_session_block
from utils import merge_segments
from utils import load_bin_centers, prepare_bins
from utils import bin_position_linearized_LR
from utils import get_maze_arm_of_point
import matplotlib.pyplot as plt
import seaborn as sns

import warnings
warnings.filterwarnings("ignore")
warnings.filterwarnings("ignore", category=FutureWarning)

setting_name            = 'dec19k20'
data_version            = 'dec16'

area                    = 'Hippocampus'
epoch                   = 'task'   # sleep, pre_sleep, post_sleep, task

sessions                = sessions_HC_replay
data_folder             = DATA_FOLDER
output_folder           = os.path.join(REPLAY_FOLDER, 'results', 'PBEs',
                                      'pbe_setting_{}_{}_{}'.format(setting_name,
                                                             area, data_version))

# PBE DETECTION PARAMETERS ----------------------------------------------------
mua_kernel_sigma_in_ms                    = 20
mua_kernel_size_in_ms                     = mua_kernel_sigma_in_ms * 6
mua_only_selected_cells                   = True
n_sd_above_the_mean                       = 3 # peaks defined as n standard deviations above the mean
adjust_bounds_to_first_last_spikes        = True
adjust_bounds_only_selected_cells         = True
min_average_speed                         = None #0.3
max_average_speed                         = 12
min_duration_in_ms                        = 40
max_duration_in_ms                        = 2000
min_n_spikes_only_selected_cells          = True
min_n_spikes                              = None
min_percentage_active_only_selected_cells = True # considering all neurons
min_percentage_active                     = 10
min_n_active_only_selected_cells          = True
min_n_active                              = 4


for sess_ind in [7]:
    print('\n\n\n\n ~ PBE DETECTION FOR SESSION {}, AREA: {} ~   '.format(sess_ind, area))

    # --- GROUP OTHER PARAMETERS --------------------------------------------------

    pars = {'setting_name'            : setting_name,
            'area'                    : area,
            'epoch'                   : epoch,
            'sess_ind'                : sess_ind}

    # --- LOAD NEO BLOCK ----------------------------------------------------------

    bl = get_session_block(data_folder, sess_ind, epoch, data_version=data_version)

    rat = bl.annotations['animal_ID']
    day_folder = bl.annotations['day_folder']
    session_folder = bl.annotations['session_folder']
    bin_centers_combined = load_bin_centers(data_folder, rat, day_folder,
                                            session_folder)
    bin_centers, bin_labels = prepare_bins(bin_centers_combined)

    if epoch == 'task':
        segment = merge_segments(bl.segments[:-1], bl.list_units,
                                 chop_return_central_arm=False)
        rat_speed_signal = segment.irregularlysampledsignals[1].rescale(pq.cm / pq.s)
        rat_speed = rat_speed_signal.__array__().flatten()
        rat_speed_times = rat_speed_signal.times.rescale(pq.ms)

        rat_position_signal = segment.irregularlysampledsignals[0]
        rat_position = rat_position_signal.__array__()
        rat_position_times = rat_position_signal.times.rescale(pq.ms)

        event_times = np.hstack([s.events[0].times.rescale(pq.ms) for s in bl.segments[:-1]])
        event_labels = np.hstack([s.events[0].labels for s in bl.segments[:-1]])

        trial_start_times = [s.t_start.rescale(pq.ms) for s in bl.segments[:-1]]
        trial_sides = [s.annotations['trial_side'] for s in bl.segments[:-1]]

    elif epoch == 'pre_sleep':
        segment = bl.segments[0]
        assert segment.name == 'pre-sleep'
        rat_speed = None
        rat_speed_times = None

    elif epoch == 'post_sleep':
        segment = bl.segments[1]
        assert segment.name == 'post-sleep'
        rat_speed = None
        rat_speed_times = None

    trains = segment.filter(targdict={'area': area}, objects=neo.SpikeTrain)
    is_pyramidal = [i for i in range(len(trains)) if trains[i].annotations['is_pyramidal']]


    detector = PBEsDetector()

    detector.set_detection_parameters(mua_kernel_sigma_in_ms=mua_kernel_sigma_in_ms,
                                      mua_kernel_size_in_ms=mua_kernel_size_in_ms,
                                      mua_only_selected_cells=mua_only_selected_cells,
                                      n_sd_above_the_mean=n_sd_above_the_mean,
                                      adjust_bounds_to_first_last_spikes=False,
                                      adjust_bounds_only_selected_cells=adjust_bounds_only_selected_cells,
                                      min_average_speed=min_average_speed,
                                      max_average_speed=max_average_speed,
                                      min_duration_in_ms=min_duration_in_ms,
                                      max_duration_in_ms=max_duration_in_ms,
                                      min_n_spikes_only_selected_cells=min_n_spikes_only_selected_cells,
                                      min_n_spikes=min_n_spikes,
                                      min_percentage_active_only_selected_cells=min_percentage_active_only_selected_cells,
                                      min_percentage_active=min_percentage_active,
                                      min_n_active_only_selected_cells=min_n_active_only_selected_cells,
                                      min_n_active=min_n_active)

    sel_df = detector.detect_candidate_events(trains, selected_cells=is_pyramidal,
                                     speed=rat_speed, speed_times=rat_speed_times)



    PBEs_times = list(zip([i * pq.ms for i in sel_df['start_time_in_ms']],
                          [i * pq.ms for i in sel_df['end_time_in_ms']]))
    trains = segment.filter(targdict={'area': area}, objects=neo.SpikeTrain)
    binned_pbes = bin_PBEs(trains, PBEs_times,
                           bin_size_in_ms=20, sliding_binning=True, slide_by_in_ms=5,
                           pyramidal_only=True)


    trains = detector._get_trains(only_selected_cells=True)
    trains = np.hstack([t.times.rescale(pq.ms) for t in trains])

    f, ax = plt.subplots(1, 1)

    ax.plot(detector.bin_centers, detector.smooth_mua)
    ax.axhline(detector.mua_threshold, c='r')
    ax.axhline(detector.mua_mean, c='grey', ls='--')
    ax.scatter(trains, np.ones(trains.shape[0]), marker='|')
    for t1, t2 in zip(sel_df['start_time_in_ms'], sel_df['end_time_in_ms']):
        ax.axvspan(t1, t2, color='r', alpha=0.5)

    # for t1, t2 in zip(sel_df2['start_time_in_ms'], sel_df2['end_time_in_ms']):
    #     ax.axvspan(t1, t2, color='grey', alpha=0.5)

    for pbe in binned_pbes['bin_edges'][2:3]:
        for b1, b2 in pbe:
            ax.axvline(b1)
            ax.axvline(b2)

    for pbe, times in zip(binned_pbes['PBEs_binned'][2:3], binned_pbes['time_points'][2:3]):
        ax.plot(times, pbe.T.sum(axis=0), c='g')


    # f, ax = plt.subplots(1, 1)
    # ax.imshow(binned_pbes['PBEs_binned'][2].T)
    #
    # binned_pbes['PBEs_binned'][2].sum()
    # print(binned_pbes['time_points'][2])
    #
    # sel_df['epoch'] = epoch
    # sel_df['animal'] = bl.annotations['animal_ID']
    # sel_df['sess_ind'] = sess_ind

    #
    #
    #

    # detector2 = PBEsDetector()
    # detector2.set_detection_parameters(mua_kernel_sigma_in_ms=mua_kernel_sigma_in_ms,
    #                                   mua_kernel_size_in_ms=mua_kernel_size_in_ms,
    #                                   mua_only_selected_cells=mua_only_selected_cells,
    #                                   n_sd_above_the_mean=n_sd_above_the_mean,
    #                                   adjust_bounds_to_first_last_spikes=False,
    #                                   adjust_bounds_only_selected_cells=adjust_bounds_only_selected_cells,
    #                                   min_average_speed=min_average_speed,
    #                                   max_average_speed=max_average_speed,
    #                                   min_duration_in_ms=min_duration_in_ms,
    #                                   max_duration_in_ms=max_duration_in_ms,
    #                                   min_n_spikes_only_selected_cells=min_n_spikes_only_selected_cells,
    #                                   min_n_spikes=min_n_spikes,
    #                                   min_percentage_active_only_selected_cells=min_percentage_active_only_selected_cells,
    #                                   min_percentage_active=min_percentage_active,
    #                                   min_n_active_only_selected_cells=min_n_active_only_selected_cells,
    #                                   min_n_active=min_n_active)
    #
    # sel_df2 = detector2.detect_candidate_events(trains, selected_cells=is_pyramidal,
    #                                  speed=rat_speed, speed_times=rat_speed_times)



    # trains = segment.filter(targdict={'area': area},
    #                             objects=neo.SpikeTrain)
    #
    # detector = PBEsDetector()
    #
    # detector.set_detection_parameters(mua_kernel_sigma_in_ms=20,
    #                                   mua_kernel_size_in_ms=mua_kernel_size_in_ms,
    #                                   mua_only_selected_cells=mua_only_selected_cells,
    #                                   n_sd_above_the_mean=n_sd_above_the_mean,
    #                                   adjust_bounds_to_first_last_spikes=True,
    #                                   adjust_bounds_only_selected_cells=adjust_bounds_only_selected_cells,
    #                                   min_average_speed=min_average_speed,
    #                                   max_average_speed=max_average_speed,
    #                                   min_duration_in_ms=min_duration_in_ms,
    #                                   max_duration_in_ms=max_duration_in_ms,
    #                                   min_n_spikes_only_selected_cells=min_n_spikes_only_selected_cells,
    #                                   min_n_spikes=min_n_spikes,
    #                                   min_percentage_active_only_selected_cells=min_percentage_active_only_selected_cells,
    #                                   min_percentage_active=min_percentage_active,
    #                                   min_n_active_only_selected_cells=min_n_active_only_selected_cells,
    #                                   min_n_active=min_n_active)
    #
    # sel_df = detector.detect_candidate_events(trains, selected_cells=is_pyramidal,
    #                                  speed=rat_speed, speed_times=rat_speed_times)
    #
    # detector2 = PBEsDetector()
    # detector2.set_detection_parameters(mua_kernel_sigma_in_ms=10,
    #                                   mua_kernel_size_in_ms=mua_kernel_size_in_ms,
    #                                   mua_only_selected_cells=mua_only_selected_cells,
    #                                   n_sd_above_the_mean=n_sd_above_the_mean,
    #                                   adjust_bounds_to_first_last_spikes=True,
    #                                   adjust_bounds_only_selected_cells=adjust_bounds_only_selected_cells,
    #                                   min_average_speed=min_average_speed,
    #                                   max_average_speed=max_average_speed,
    #                                   min_duration_in_ms=min_duration_in_ms,
    #                                   max_duration_in_ms=max_duration_in_ms,
    #                                   min_n_spikes_only_selected_cells=min_n_spikes_only_selected_cells,
    #                                   min_n_spikes=min_n_spikes,
    #                                   min_percentage_active_only_selected_cells=min_percentage_active_only_selected_cells,
    #                                   min_percentage_active=min_percentage_active,
    #                                   min_n_active_only_selected_cells=min_n_active_only_selected_cells,
    #                                   min_n_active=min_n_active)
    #
    # sel_df2 = detector2.detect_candidate_events(trains, selected_cells=is_pyramidal,
    #                                  speed=rat_speed, speed_times=rat_speed_times)
    #
    #
    # f, ax = plt.subplots(1, 1)
    #
    # ax.plot(detector.bin_centers, detector.smooth_mua, alpha=0.5)
    # ax.plot(detector2.bin_centers, detector2.smooth_mua, c='g', alpha=0.5)
    # ax.axhline(detector.mua_threshold, c='r')
    # ax.axhline(detector.mua_mean, c='r', ls='--')
    # ax.axhline(detector2.mua_threshold, c='green')
    # ax.axhline(detector2.mua_mean, c='green', ls='--')
    # for t1, t2 in zip(sel_df['start_time_in_ms'], sel_df['end_time_in_ms']):
    #     ax.axvspan(t1, t2, color='r', alpha=0.5)
    # for t1, t2 in zip(sel_df2['start_time_in_ms'], sel_df2['end_time_in_ms']):
    #     ax.axvspan(t1, t2, color='green', alpha=0.5)
    #


