import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import seaborn as sns
import matplotlib.pyplot as plt
from plotting_style import *
from utils import *
from statsmodels.stats.proportion import proportions_ztest


data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]

group_iti_and_return = True

# PLOTTING PARAMETERS
restrict_to_outcome = None

plot_settings = 'only_two_shufs'
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms    = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'hippocampal_replay_posneg_overrep',
                             'pbe_setting_{}_{}_{}_dec_setting_{}_{}_plot_setting_{}'.format(PBEs_settings_name, PBEs_area,
                              data_version, dec_settings_name, dec_area, plot_settings))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return)

df = match_previous_trial_side(df.copy())
df = match_current_trial_side(df.copy())

if np.isin(restrict_to_outcome, ['correct', 'incorrect']):
    df = add_correct_incorrect(df.copy(), data_version)
    df = df[df['current_trial_outcome'] == restrict_to_outcome]


# --- STATS Z-TEST -------------------------------------------------------------
dg = pd.DataFrame(columns=['phase', 'match', 'direction', 'perc', 'n1', 'n2', 'ntot'])

phases = ['iti', 'reward', 'img', 'run']

for phase in phases:
    for match in ['match_previous_trial_side', 'match_current_trial_side']:
        for direction in ['positive', 'negative', 'both']:

            if direction != 'both':
                iti_pos = df[(df['phase'] == phase) & (df['replay_direction'] == direction)]
            elif direction == 'both':
                iti_pos = df[(df['phase'] == phase)]


            counts = iti_pos[match].value_counts()
            try:
                npos = counts['match']
            except KeyError:
                npos = 0
            nneg = counts['nomatch']

            if npos+nneg < 5:
                pass
            perc = (100 * npos / (npos+nneg))
            ntot = npos+nneg
            dg.loc[dg.shape[0], :] = [phase, match, direction, perc, npos, nneg, ntot]

for col in ['perc', 'n1', 'n2', 'ntot']:
    dg[col] = pd.to_numeric(dg[col])

# combos = list(itertools.combinations(col_vals, 2))
# n_tests = len(col_vals) + len(combos)

for phase in phases:
    print('\n{}'.format(phase))
    dg_sel = dg[dg['phase'] == phase]

    for i, row in dg_sel.iterrows():

        n1 = row['n1']
        nsamp = row['ntot']
        n2 = int(nsamp / 2)
        z, pval = proportions_ztest(count=[n1, n2], nobs=[nsamp, nsamp])
        is_sig = pval < (0.05)
        adjusted_p = pval
        print('{}, {}  different from 50%: {:.1f}%, {} of {}, z-test '
              'p={:.3f} - {}'.format(row['match'], row['direction'],
                                     n1/nsamp*100, n1, nsamp, adjusted_p, is_sig))



# difference between forward and reverse per phase/match
for phase in phases:

    dg_sel = dg[dg['phase'] == phase]
    # TEST INDIVIDUAL PHASE DIFFERENT FROM 50%
    for match in ['match_previous_trial_side', 'match_current_trial_side']:
        n1 = dg_sel[(dg_sel['match'] == match) & (dg_sel['direction'] == 'positive')]['n1'].iloc[0]
        n2 = dg_sel[(dg_sel['match'] == match) & (dg_sel['direction'] == 'negative')]['n1'].iloc[0]

        ntot1 = dg_sel[(dg_sel['match'] == match) & (dg_sel['direction'] == 'positive')]['ntot'].iloc[0]
        ntot2 = dg_sel[(dg_sel['match'] == match) & (dg_sel['direction'] == 'negative')]['ntot'].iloc[0]

        z, pval = proportions_ztest(count=[n1, n2], nobs=[ntot1, ntot2])
        is_sig = pval < (0.05)
        adjusted_p = pval
        print('\n\n{} {}'.format(phase, match))
        print('forward: {:.1f}%, nsamp = {}'.format(n1/ntot1*100, ntot1))
        print('reverse: {:.1f}%, nsamp = {}'.format(n2/ntot2*100, ntot2))
        print('z-test p={:.3f} - {}'.format( adjusted_p, is_sig))




# --- PLOT BARS (NO BOOTSTRAP) -------------------------------------------------

combos = [('img', 'match_current_trial_side'),
          ('reward', 'match_current_trial_side'),
          ('iti', 'match_previous_trial_side')]

for phase, match in combos:

    dg_sel = dg[(dg['phase'] == phase) & (dg['match'] == match)]

    f, ax = plt.subplots(1, 1, figsize=[2, 3])
    sns.barplot(data=dg_sel, x='direction', y='perc',
                linewidth=3.5, order=['both', 'positive', 'negative'])

    plt.setp(ax.patches[0], edgecolor=sns.xkcd_rgb['dark grey'],
             facecolor='w', linewidth=3.5)
    plt.setp(ax.patches[1], edgecolor=replay_direction_palette['positive'],
             facecolor='w', linewidth=3.5)
    plt.setp(ax.patches[2], edgecolor=replay_direction_palette['negative'],
             facecolor='w', linewidth=3.5)

    labls = [replay_direction_labels[t._text] for t in ax.get_xticklabels()]

    plt.setp(ax.lines, color=sns.xkcd_rgb['dark grey'])

    ax.set_xticklabels(labls, rotation=45)
    # ax.set_yticks([0, 20, 40])
    # ax.set_ylim([0, 40])
    ax.set_xlabel('')
    ax.set_ylabel('% of REs corresponding\nto future path')
    sns.despine()
    plt.tight_layout()

    plot_name = 'barplot_{}_{}_{}.{}'.format(phase, match, restrict_to_outcome, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- BOOTSTRAP STATISTICS -----------------------------------------------------

# combos = [('img', 'positive', 'current_trial_side'),
#           ('img', 'both', 'current_trial_side'),
#           ('reward', 'negative', 'previous_trial_side'),
#           ('reward', 'negative', 'current_trial_side'),
#           ('reward', 'positive', 'current_trial_side'),]


combos = [('img', 'positive', 'current_trial_side'),
          ('reward', 'negative', 'current_trial_side'),
          ('reward', 'positive', 'current_trial_side'),]

for phase, direction, match in combos:

    if direction == 'both':
        dx = df[(df['phase'] == phase)]
    else:
        dx = df[(df['phase'] == phase) & (df['replay_direction'] == direction)]

    ma_occ = dx[match]  # THIS IS DIFFERENT
    ma_end = dx['maze_arm_dec_end']
    bin_end = dx['dec_end_bin']

    # if phase == 'reward':
    #     new_ma_end = []
    #     for me, be in zip(ma_end, bin_end):
    #         if be < -20 or be > 20:
    #             new_ma_end.append('ahead')
    #         else:
    #             new_ma_end.append(me)
    #     ma_end = new_ma_end

    nmatch_obs = np.sum([False if a != b else True for a, b in zip(ma_occ, ma_end)])

    nboot = 1000
    nmatch_shuf = []
    for i in range(nboot):

        ma_end_shuff = np.random.permutation(ma_end)
        nmatch = np.sum([False if a != b else True for a, b in zip(ma_occ, ma_end_shuff)])
        nmatch_shuf.append(nmatch)

    nmatch_shuf = np.array(nmatch_shuf)
    pval = (nmatch_shuf > nmatch_obs).sum() / len(nmatch_shuf)
    print('\n\n{} {} {}, p={:.5f}'.format(phase, direction, match, pval))

    z, pval = proportions_ztest(count=[nmatch_obs, len(ma_occ)//2],
                                nobs=[len(ma_occ), len(ma_occ)])
    is_sig = pval < (0.05)
    print('z-test p={:.3f} - {}'.format(pval, is_sig))
