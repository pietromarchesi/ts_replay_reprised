import os
from neo.io import NeuralynxIO
import matplotlib.pyplot as plt
from neuralynx_io import neuralynx_io
import os, glob
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from utils import *
from irasa.IRASA import IRASA
import yasa

data_version         = 'dec16'
PBEs_settings_name   = 'dec19k20'
PBEs_area            = 'Hippocampus'
dec_settings_name    = 'DecSet2'
dec_area             = 'Hippocampus'
sessions             = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
group_iti_and_return = True

lfp_path = '/Users/pietro/data/touch_see/LFP'


SPEED_THRESHOLD      = 8

# PLOTTING PARAMETERS
save_plots = False
plot_format = 'png'
dpi = 400
min_pbe_duration_in_ms = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

#edge_at_the_sides_in_ms = 50
# TODO 2 cycles at 4 hz is 500 ms, so we take a bit extra
total_duration_extended = 700

theta_band = [5, 12]

settings_name = '5_12_1sec_only_peak'


# session 9
# event 1549

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'lfp_events_with_osc_{}'.format(settings_name))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df, df_not_sig = load_replay(sessions=sessions,
                             PBEs_settings_name=PBEs_settings_name,
                             PBEs_area=PBEs_area,
                             data_version=data_version,
                             dec_settings_name=dec_settings_name,
                             dec_area=dec_area,
                             min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                             shuffle_types=shuffle_types,
                             shuffle_p_vals=shuffle_p_vals,
                             group_iti_and_return=group_iti_and_return,
                             return_not_sig=True)

df = add_new_speed(df, time_before_in_ms=0, binsize_interp_pos=50,
                   time_after_in_ms=0, position_sigma=12,
                   data_version=data_version)

# df['speed_group'] = None
# df.loc[df['average_speed_new']>4, 'speed_group'] = 'Fast\nevents'
# df.loc[df['average_speed_new']<=4, 'speed_group'] = 'Slow\nevents'

seldf = df[df['average_speed_new'] <= SPEED_THRESHOLD]

# df[df['average_speed_new']>SPEED_THRESHOLD]['average_speed_new'].quantile(0.5)


for sess in sessions:
    rat, day, session = get_rat_day_session(sess)
    print(sess, rat, day, session)



def bandpower(data, sf, band, method='welch', window_sec=None, relative=False):
    """Compute the average power of the signal x in a specific frequency band.

    Requires MNE-Python >= 0.14.

    Parameters
    ----------
    data : 1d-array
      Input signal in the time-domain.
    sf : float
      Sampling frequency of the data.
    band : list
      Lower and upper frequencies of the band of interest.
    method : string
      Periodogram method: 'welch' or 'multitaper'
    window_sec : float
      Length of each window in seconds. Useful only if method == 'welch'.
      If None, window_sec = (1 / min(band)) * 2.
    relative : boolean
      If True, return the relative power (= divided by the total power of the signal).
      If False (default), return the absolute power.

    Return
    ------
    bp : float
      Absolute or relative band power.
    """
    from scipy.signal import welch
    from scipy.integrate import simps
    from mne.time_frequency import psd_array_multitaper

    band = np.asarray(band)
    low, high = band

    # Compute the modified periodogram (Welch)
    if method == 'welch':
        if window_sec is not None:
            nperseg = window_sec * sf
        else:
            nperseg = (2 / low) * sf

        freqs, psd = welch(data, sf, nperseg=nperseg)

    elif method == 'multitaper':
        psd, freqs = psd_array_multitaper(data, sf, adaptive=True,
                                          normalization='full', verbose=0)

    # Frequency resolution
    freq_res = freqs[1] - freqs[0]

    # Find index of band in frequency vector
    idx_band = np.logical_and(freqs >= low, freqs <= high)

    # Integral approximation of the spectrum using parabola (Simpson's rule)
    bp = simps(psd[idx_band], dx=freq_res)

    if relative:
        bp /= simps(psd, dx=freq_res)
    return bp


# --- COMPUTE THETA POWER ------------------------------------------------------

for sess in sessions:#sessions:

    rat, day, session = get_rat_day_session(sess)
    sess_path = os.path.join(lfp_path, rat, day, session)
    os.chdir(sess_path)

    os.getcwd()
    time_stamps = []
    channels = []

    session_path = 'SpikeTrl/'+rat+'/'+day+'/'+session+'/spikeTrl.mat'
    S  = loadmat(DATA_FOLDER + session_path)
    first_ts = S['spike']['hdr'][sess].FirstTimeStamp / 1000 #first ts in ms
    #print(sess, files)

    files = glob.glob("*.ncs")

    #timeaxis = loadmat(files[1])

    for eni, file in enumerate(files):
        ncs = neuralynx_io.load_ncs(os.path.join(sess_path, file))  # Load signal data into a dictionary
        channels.append(ncs['data'])
        if eni == 0:
            time_stamps.append(ncs['time'])

    ts = time_stamps[0] / 1000 # ts in ms
    sessdf = seldf[seldf['sess_ind'] == sess]

    for event_id, row in sessdf.iterrows():

        if row['event_duration_in_ms'] < total_duration_extended:
            edge_at_the_sides_in_ms = (total_duration_extended - row['event_duration_in_ms']) / 2
        else:
            edge_at_the_sides_in_ms = 0

        start_time = (row['start_time_in_ms'])+first_ts-edge_at_the_sides_in_ms
        end_time = (row['end_time_in_ms'])+first_ts+edge_at_the_sides_in_ms

        indx = [np.logical_and(ts > start_time, ts < end_time)][0]

        # IRASA
        sig = channels[0][indx]
        freqs = np.arange(2, 30, 1)
        irasa = IRASA(sig=sig, freqs=freqs,
                      samplerate = ncs['sampling_rate'])
        osc = np.log10(irasa.mixed) - np.log10(irasa.fractal)

        #theta_power = osc[(freqs>=theta_band[0]) & (freqs<=theta_band[1])].sum()
        # TODO sum only positive power in theta - makes sense?
        freqs_osc = osc[(freqs >= theta_band[0]) & (freqs <= theta_band[1])]
        freqs_osc[freqs_osc < 0] = 0
        theta_power = freqs_osc.sum()

        total_power = osc.sum()
        seldf.loc[event_id, 'theta_power'] = theta_power
        seldf.loc[event_id, 'theta_fraction'] = theta_power / total_power

        # BANDPOWER WELCH
        # sig = np.vstack([ch[indx] for ch in channels])
        #
        # ys = yasa.bandpower(sig, sf=int(ncs['sampling_rate']), relative=True,
        #                     win_sec=0.05)
        # ys['Theta'].mean()
        # seldf.loc[i, 'relative_theta_power_welch'] = theta_power

        # BANDPOWER MULTITAPER
        theta_mt = []
        for ch in channels:
            try:
                th = bandpower(ch[indx], band=theta_band, sf=int(ncs['sampling_rate']), relative=True)
                theta_mt.append(th)
            except IndexError:
                pass
        seldf.loc[event_id, 'relative_theta_power_mt'] = np.mean(theta_mt)

        seldf.loc[event_id, 'sess_ind'] = sess


seldf.to_csv(os.path.join(REPLAY_FOLDER, 'results', 'theta_power_{}.csv'.format(settings_name)), index=True,
             index_label='indx')




#
# # --- LOAD ------------
# seldf = pd.read_csv(
#     os.path.join(REPLAY_FOLDER, 'results', 'theta_power_{}.csv'.format(settings_name)),
#     index_col='indx')
#
# seldf['sig_theta'] = False
# seldf.loc[seldf['theta_power'] >= seldf['theta_power'].quantile(0.95), 'sig_theta'] = True
#
# f, ax = plt.subplots(1, 1, figsize=[3, 3])
# sns.kdeplot(seldf['theta_power'], label='')
# ax.axvline(seldf['theta_power'].quantile(0.95), c='r', ls=':')
# ax.set_xlabel('Theta power')
# ax.set_ylabel('Density')
# sns.despine()
# plt.tight_layout()
#
# plot_name = 'theta_distribution_{}.{}'.format(settings_name, plot_format)
# f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#
# # --- PLOT ONLY EVENTS WITH HIGH THETA POWER -----------------------------------
#
# #seldf_th = seldf.sort_values(by='theta_power', ascending=False).iloc[0:30]
#
# # seldf['sig_theta'] = False
# # seldf.loc[seldf['theta_power'] >= seldf['theta_power'].quantile(0.95), 'sig_theta'] = True
#
# plot_non_significant = True
#
#
#
# for sess in [2]:
#
#     rat, day, session = get_rat_day_session(sess)
#     sess_path = os.path.join(lfp_path, rat, day, session)
#     os.chdir(sess_path)
#
#     time_stamps = []
#     channels = []
#
#     session_path = 'SpikeTrl/' + rat + '/' + day + '/' + session + '/spikeTrl.mat'
#     S = loadmat(DATA_FOLDER + session_path)
#     first_ts = S['spike']['hdr'][sess].FirstTimeStamp / 1000  # first ts in ms
#
#     for i, file in enumerate(glob.glob("*.ncs")) :
#         ncs = neuralynx_io.load_ncs(
#             os.path.join(sess_path, file))  # Load signal data into a dictionary
#         channels.append(ncs['data'])
#         if i == 0 :
#             time_stamps.append(ncs['time'])
#
#     ts = time_stamps[0] / 1000  # ts in ms
#
#     sessdf = seldf[(seldf['sess_ind'] == sess)]
#
#     if plot_non_significant:
#         sessdf = sessdf[(sessdf['sig_theta'] == False)]
#     sessdf = sessdf.sort_values(by='theta_power', ascending=False).iloc[0:5]
#
#     for event_id, row in sessdf.iterrows() :
#
#         edge_at_the_sides_in_ms = (total_duration_extended - row['event_duration_in_ms']) / 2
#
#
#         start_time = (row['start_time_in_ms'])+first_ts-edge_at_the_sides_in_ms
#         end_time = (row['end_time_in_ms'])+first_ts+edge_at_the_sides_in_ms
#
#         indx = [np.logical_and(ts > start_time, ts < end_time)][0]
#
#         sig = channels[0][indx]
#         freqs = np.arange(2, 30, 1)
#         irasa = IRASA(sig=sig, freqs=freqs,
#                       samplerate=ncs['sampling_rate'])
#
#         # plt.figure(figsize=(10, 4))
#         # plt.subplot(121)
#         # irasa.psdplot(fit=True)
#         # plt.subplot(122)
#         # irasa.loglogplot(fit=True)
#
#         osc = np.log10(irasa.mixed) - np.log10(irasa.fractal)
#
#         theta_power = osc[(freqs >= theta_band[0]) & (freqs <= theta_band[1])].sum()
#         seldf.loc[event_id, 'theta_power'] = theta_power
#
#         sns.set_style('ticks')
#
#         f, ax = plt.subplots(1, 3, figsize=[8, 3])
#
#         # --- PLOT SIGNAL ---
#         for kk, ch in enumerate(channels) :
#             if kk == 0 :
#                 ax[0].plot(ts[indx] - start_time, ch[indx], c='k', lw=2,
#                            zorder=10)
#             else :
#                 ax[0].plot(ts[indx] - start_time, ch[indx], lw=1, zorder=-10)
#
#         ax[0].axvspan((start_time + edge_at_the_sides_in_ms - start_time),
#                       (end_time - edge_at_the_sides_in_ms - start_time),
#                       color='grey', alpha=0.5,
#                       zorder=-10)
#
#         # ax[0].axvline((start_time + edge_at_the_sides_in_ms - start_time),
#         #               ls='-')
#         # ax[0].axvline((end_time - edge_at_the_sides_in_ms - start_time), ls='-')
#
#         sns.despine()
#         ax[0].set_xlabel('Time [ms]')
#         ax[0].set_title('All LFP channels'.format(sess))
#         ax[1].set_title('Selected LFP channel'.format(sess))
#
#         # --- PLOT DETRENDED SIGNAL ---
#         ax[1].plot(ts[indx] - start_time, irasa.sig_filtered, c='k', lw=2, zorder=10)
#
#         # irasa.plot_oscillatory()
#         ax[2].plot(irasa.freqs, osc)
#         ax[2].axhline(0, c='k')
#         ax[2].set_xlabel('Frequency')
#         ax[2].set_ylabel('Residual power')
#         ax[2].set_xticks([0, 5, 10, 15, 20, 25, 30])
#         ax[2].axvspan(theta_band[0], theta_band[1], color=sns.xkcd_rgb['light red'])
#
#         plt.tight_layout()
#
#         if plot_non_significant:
#             plot_name = 'event_{}_sess_notsig.{}'.format(event_id, sess, plot_format)
#         else:
#             plot_name = 'event_{}_sess.{}'.format(event_id, sess, plot_format)
#         f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#
#         plt.close()