import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from session_selection import sessions_HC_replay
import quantities as pq
import matplotlib.patches as mpatches
from scipy.ndimage import gaussian_filter1d
import itertools
from utils import *
from numpy import median
from statsmodels.stats.proportion import proportions_ztest



data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

group_iti_and_return = True

contrasts                 = ['coordination_perirhinal',
                             'coordination_barrel',
                             'coordination_both']

#contrasts = ['local_remote']
# PLOTTING PARAMETERS
#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'jul4_DecSet2_smooth2_morelages'


plot_settings = 'only_two_shufs'
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms    = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'hippocampal_replay',
                             'pbe_setting_{}_{}_{}_dec_setting_{}_{}_plot_setting_{}'.format(PBEs_settings_name, PBEs_area,
                              data_version, dec_settings_name, dec_area, plot_settings))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------


df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return)



df['phase'] = df['phase'].replace(to_replace='img', value='itiimg')
df['phase'] = df['phase'].replace(to_replace='iti', value='itiimg')



# df_mod_1 = add_coordination_per_event(df.copy(), ratemap_area='Perirhinal',
#                                 ratemap_settings=ratemap_settings,
#                                 data_version=data_version)
#
#
# df_mod_2 = add_coordination_per_event(df.copy(), ratemap_area='Barrel',
#                                 ratemap_settings=ratemap_settings,
#                                 data_version=data_version)

df_mod = add_coordination_both_areas(df.copy(),
                                ratemap_settings=ratemap_settings,
                                data_version=data_version)


df_mod = df_mod.rename({'coordination_both' : 'coordination'}, axis=1)

contrast = 'coordination'

name1, name2 = np.sort(df_mod[contrast].unique())


if group_iti_and_return :
    phases = ['itiimg', 'run', 'reward']
else :
    phases = ['itiimg', 'return', 'reward', 'run']





# --- PLOT V1 - Z TEST ---------------------------------------------------------

#df_mod = df_mod_3

dg = pd.DataFrame(columns=['phase', 'direction', 'perc_centr', 'ncentrif', 'ntot'])
for phase in ['all'] + phases:
    for dir in ['positive', 'negative', 'both']:
        if phase == 'all':
            dk = df_mod.copy()
        else:
            dk = df_mod[df_mod['phase'] == phase].copy()

        if dir == 'both':
            dk = dk.copy()
        else:
            dk = dk[dk['replay_direction'] == dir].copy()

        if dk.shape[0] > 5:
            npos = dk[dk[contrast] == name1].shape[0]
            nneg = dk[dk[contrast] == name2].shape[0]
            perc = (100 * npos / (npos+nneg))
            ntot = npos+nneg
            dg.loc[dg.shape[0], :] = [phase, dir ,perc, npos, ntot]
        else:
            pass

dg['perc_centr'] = pd.to_numeric(dg['perc_centr'])
dg['ncentrif'] = pd.to_numeric(dg['ncentrif'])
dg['ntot'] = pd.to_numeric(dg['ntot'])


allphases = ['all'] + phases
combos = list(itertools.combinations(phases, 2))
n_tests = len(allphases) + len(combos)

# TEST INDIVIDUAL PHASE DIFFERENT FROM 50%
# for ph1 in ['all'] + phases:
#     if ph1 == 'all':
#         ncentrif = dg['ncentrif'].values[0]
#         nsamp = dg['ntot'].values[0]
#     else:
#         ncentrif = dg[dg['phase'] == ph1]['ncentrif'].values[0]
#         nsamp = dg[dg['phase'] == ph1]['ntot'].values[0]
#
#     ncentrif2 = int(nsamp / 2)
#
#     z, pval = proportions_ztest(count=[ncentrif, ncentrif2], nobs=[nsamp, nsamp])
#     is_sig = pval < (0.05 / n_tests)
#     adjusted_p = pval
#     print('{} different from 50%, {} of {}, z-test p={:.3f} - {}'.format(ph1, ncentrif, nsamp, adjusted_p, is_sig))
#
# # TEST DIFFERENCE BETWEEN PHASES
# for ph1, ph2 in combos:
#     ncentrif1 = dg[dg['phase'] == ph1]['ncentrif'].values[0]
#     nsamp1 = dg[dg['phase'] == ph1]['ntot'].values[0]
#
#     ncentrif2 = dg[dg['phase'] == ph2]['ncentrif'].values[0]
#     nsamp2 = dg[dg['phase'] == ph2]['ntot'].values[0]
#
#     z, pval = proportions_ztest(count=[ncentrif1, ncentrif2], nobs=[nsamp1, nsamp2])
#     is_sig = pval < (0.05 / n_tests)
#     adjusted_p = pval
#     print('{} vs {} z-test p={:.3f} - {}'.format(ph1, ph2, adjusted_p, is_sig))


f, ax = plt.subplots(1, 1, figsize=slightly_vertical_panel)

# sns.barplot(x=['all'] + phases, y=[100] * len(['all']+phases),
#             edgecolor=sns.xkcd_rgb['grey'],
#             facecolor='white', linewidth=1.5)

g = sns.barplot(data=dg, x='phase', y='perc_centr', hue='direction',
                hue_order=['positive', 'negative', 'both'], estimator=median,
                palette=replay_direction_palette_2,
               linewidth=3.5, errcolor='0.2')

ax.legend().remove()
ax.set_yticks([0, 20, 40, 60, 80, 100])
ax.set_ylim([0, 100])

sns.despine()
ax.set_ylabel('% of REs coordinated with PRH and S1BF')
ax.set_xticklabels([task_phase_labels_nobreak_v2[l._text] for l in ax.get_xticklabels()],
                   rotation=45)
ax.set_xlabel('')
plt.tight_layout()

plot_name = 'perc_{}_events_forward_rev_per_task_phase_v2_both_areas.{}'.format(contrast, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#plt.close(fig=f)



pos_patch = mpatches.Patch(facecolor=replay_direction_palette_2['positive'],
                       edgecolor='white', linewidth=2.5,
                       label='Forward REs')
neg_patch = mpatches.Patch(facecolor=replay_direction_palette_2['negative'],
                          edgecolor='white', linewidth=2.5,
                       label='Reverse REs')
bo_patch = mpatches.Patch(facecolor=replay_direction_palette_2['both'],
                          edgecolor='white', linewidth=2.5,
                       label='Forward and reverse REs')

f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=[pos_patch, neg_patch, bo_patch], frameon=False)
ax.axis('off')
plt.tight_layout()

plot_name = 'replay_{}_legend_forward_rev_both_areas.{}'.format(contrast, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


