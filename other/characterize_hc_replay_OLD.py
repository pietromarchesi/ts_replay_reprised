import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from utils import load_bin_centers
from utils import prepare_bins
from utils import plot_maze, mark_locations
from utils import get_position_bin
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from utils import is_on_central_arm, merge_duplicate_left_right_bins
from utils import add_ripples_to_pbes_df
from session_selection import sessions_HC_replay
from utils import load_decoded_PBEs
from utils import get_time_spent_per_task_phase
import quantities as pq
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
from scipy.ndimage import gaussian_filter1d
import itertools
from utils import *



"""
PLOT
1. Barplot of replay direction (positive versus negative)
2. Distribution of event durations (kdeplot)
3. Plot where the PBEs occur colored by left/central/right arm
4. Plot distribution of start vs end locations
5. Distribution of bin of occurrence of PBE vs decoded start and decoded end
locations 
6. Distribution of compression factors 
7. Distributions of replay structure measures across epochs (are replay
events more structured in post-sleep/task than pre-sleep?)
8. Distributions of replay structure measures across locations / task phase 
(do replay events differ depending on when/where they occur?)
- replay (z) score
- max and mean jump distance
"""


data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]

group_iti_and_return = True

# PLOTTING PARAMETERS

plot_settings = 'only_two_shufs'
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms    = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'hippocampal_replay',
                             'pbe_setting_{}_{}_{}_dec_setting_{}_{}_plot_setting_{}'.format(PBEs_settings_name, PBEs_area,
                              data_version, dec_settings_name, dec_area, plot_settings))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return)




# --- STACKED TRAJECTORIES -----------------------------------------------------


max_n_traj = 300

if group_iti_and_return:
    phases = ['img', 'iti', 'reward', 'run']
else:
    phases = ['img', 'iti', 'return', 'reward', 'run']


for phase in phases:
    df_sel = df[df['epoch'] == 'task']
    df_sel = df_sel[df_sel['phase'] == phase]

    #df_sel = df_sel[~df_sel['pbe_loc_bin'].isna()]

    if df_sel.shape[0] > max_n_traj:
            ind = np.random.choice(np.arange(df_sel.shape[0]), max_n_traj, replace=False)
            df_sel = df_sel.iloc[ind]

    new_trajs = []
    for i in range(df_sel.shape[0]):
        row = df_sel.iloc[i]
        traj = row['trajectory']
        loc = row['pbe_loc_bin']
        side = row['pbe_side']
        direction = row['replay_direction']
        traj = linearize_trajectory(traj, side, direction)
        new_trajs.append(traj)
    df_sel['trajectory'] = new_trajs
    df_sel['new_dec_start_bin'] = [t[0] for t in df_sel['trajectory']]
    df_sel['new_dec_end_bin'] = [t[-1] for t in df_sel['trajectory']]


    df1 = df_sel[df_sel['replay_direction'] == 'negative']
    df2 = df_sel[df_sel['replay_direction'] == 'positive']
    df1 = df1.sort_values(by=['new_dec_start_bin', 'new_dec_end_bin'])
    df2 = df2.sort_values(by=['new_dec_start_bin', 'new_dec_end_bin'])
    df_sel = pd.concat((df1, df2))

    f, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios' : [1, 7]},
                         sharex=True, figsize=[2.5, 6.5])

    ax2 = ax[0].twinx()  # instantiate a second axes that shares the same x-axis

    # if phase == 'img':
    #     bw_loc = 0.5
    # else:
    #     bw_loc = 0.1
    # sns.kdeplot(df_sel['pbe_loc_bin'], ax=ax2, c=animal_loc_color,
    #             label='Animal location during REs', bw=bw_loc,
    #             cut=100, clip=[-29, 29])

    bw = 0.1
    sns.kdeplot(df_sel['new_dec_start_bin'], ax=ax[0], c=trajectory_cmap(0),
                label='Start of replay trajectory', bw=bw, cut=100, clip=[-29, 29])

    sns.kdeplot(df_sel['new_dec_end_bin'], ax=ax[0], c=trajectory_cmap(255),
                label='End of replay trajectory', bw=bw, cut=100, clip=[-29, 29])


    ytext = df_sel[df_sel['replay_direction'] == 'negative'].shape[0]

    ba = int(df_sel.shape[0] / 40)
    ax[1].plot([-31, -31], [1, ytext-ba], lw=4.5,
               c=replay_direction_palette['negative'])
    ax[1].plot([-31, -31], [ytext+ba, df_sel.shape[0]-1], lw=4.5,
               c=replay_direction_palette['positive'])

    if phase == 'iti' or phase == 'img':
        ax[1].axvspan(*location_bands[phase], alpha=0.5, color=animal_loc_color, lw=0)
    elif phase == 'run' or phase == 'reward':
        ax[1].axvspan(*location_bands[phase][0], alpha=0.5, color=animal_loc_color, lw=0)
        ax[1].axvspan(*location_bands[phase][1], alpha=0.5, color=animal_loc_color, lw=0)

    for i in range(df_sel.shape[0]):
        row = df_sel.iloc[i]
        traj = row['trajectory']
        loc = row['pbe_loc_bin']
        side = row['pbe_side']
        direction = row['replay_direction']

        if traj[-1] > traj[0]:
            traj = np.arange(traj[0], traj[-1]+0.1, 0.5)
        elif traj[-1] < traj[0]:
            #print('rev')
            traj = np.arange(traj[0], traj[-1]-0.1, -0.5)
        N = traj.shape[0]
        for j in range(traj.shape[0]-1):
            #print([traj[j], traj[j+1]])
            colors = trajectory_cmap(int(255 * j / N))
            ax[1].plot([traj[j], traj[j+1]], [i, i], color=colors, zorder=100)

        ax[1].scatter(traj[0], i, c=trajectory_cmap(0), s=6, zorder=101)
        ax[1].scatter(traj[-1], i, c=trajectory_cmap(255), s=6, zorder=101)
        # ax[1].scatter(loc, i, facecolor=animal_loc_color, marker='.', zorder=100, alpha=0.8,
        #               edgecolor='w', lw=0, s=40)
    sns.despine(left=True, bottom=True, ax=ax[0])
    sns.despine(left=True, bottom=True, ax=ax2)

    for axx in ax:
        axx.set_yticks([], [])
    ax2.set_yticks([], [])
    ax[0].tick_params(axis=u'both', which=u'both',length=0)
    ax[1].set_xlabel('Linearized location', labelpad=15)
    ax[0].legend(frameon=False)
    leg = ax[0].get_legend()
    ax[0].get_legend().remove()
    #ax2.get_legend().remove()
    ax[1].set_xlim([-31, 31])
    ax[1].set_xticks([-29, -reward_bin, 0, reward_bin, 29])
    ax[1].set_xticklabels(['IMG', 'REW', 'IMG', 'REW', 'IMG'], fontsize=8)
    sns.despine(left=True, ax=ax[1], trim=True)
    for loc in [0, reward_bin, -reward_bin]:
        ax[1].axvline(loc, c=maze_color, zorder=-10, ls='--')
    plt.tight_layout()

    plot_name = 'trajectory_stack_{}.{}'.format(phase, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# graph_data = [5, 8, 7, 9]
# x = range(len(graph_data))
# y = graph_data
# f, ax = plt.subplots()
#
# ax.plot(x, y, '-ob', markersize=6, c=animal_loc_color,
#          label='Animal location during REs')
# ax.plot(x, y, '-ob', markersize=6, c=trajectory_cmap(0),
#         label='Start of replay trajectory')
# ax.plot(x, y, '-ob', markersize=6, c=trajectory_cmap(255),
#         label='End of replay trajectory')
# ax.legend(frameon=False, bbox_to_anchor=(1.05, 1))
# #leg = ax.get_legend()
# plt.tight_layout(rect=(0, 0, 0.7, 1))
#
# #ax.get_legend().remove()
# # f, ax = plt.subplots(1, 1, figsize=[3, 1])
# # ax.legend(handles=leg.legendHandles, frameon=False, title='')
# # ax.axis('off')
# plot_name = 'stack_legend.{}'.format(plot_format)
# f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


legend_elements = [mlines.Line2D([0], [0], color=trajectory_cmap(0), lw=3,
                                 label='Start of replay trajectory'),
                   mlines.Line2D([0], [0], color=trajectory_cmap(255), lw=3,
                                 label='End of replay trajectory'),
                   mpatches.Patch(facecolor=animal_loc_color, lw=0, alpha=0.5,
                         label='Animal location during REs')]

# Create the figure
f, ax = plt.subplots()
ax.legend(handles=legend_elements, loc='center', frameon=False)
ax.axis('off')
plt.show()
plot_name = 'stack_legend.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


#
# # TODO need to linearize the trajectory!
#
# side = 'right'
# direction = 'positive'
# traj = [27, 29, 2, 4, 6, 8]
#
#
# side = 'right'
# direction = 'negative'
# traj = [8, 6, 4, 2, 29, 27, 25]
#
#
# side = 'left'
# direction = 'positive'
# traj = [-27, -29, -2, -4, -6, -8]
#
#
#
# side = 'left'
# direction = 'negative'
# traj = [-8, -6, -5, -4, -2, 0, -29, -27]
#
# side = 'right'
# direction = 'positive'
# traj = [ 6,  7,  8,  8,  9, 10, 11, 12, 12, 13, 14, 15, 16, 16, 17, 18, 19,
#        20, 20, 21, 22, 23, 24, 24, 25, 26, 27, 28, 28, 29,  0]
#
#
# flip_dict = {29:-1, 28:-2, 27:-3,
#              26:-4, 25:-5, 24:-6,
#              23:-7, 22:-8,
#              -29:1, -28:2, -27:3,
#              -26:4, -25:5, -24:6,
#              -23:7, -22:8}
#
# if side == 'right' and direction == 'positive' and traj[0] > traj[-1]:
#     ind = np.where(np.diff(traj) < 0)[0][0]
#
#     for i in range(ind+1):
#         traj[i] = flip_dict[traj[i]]
#
#
# if side == 'right' and direction == 'negative' and traj[-1] > traj[0]:
#     ind = np.where(np.diff(traj) > 0)[0][0]
#
#     for i in range(ind+1, len(traj)):
#         #print(i)
#         traj[i] = flip_dict[traj[i]]
#
#
# if side == 'left' and direction == 'positive' and traj[0] < traj[-1]:
#     ind = np.where(np.diff(traj) > 0)[0][0]
#
#     for i in range(ind+1):
#         traj[i] = flip_dict[traj[i]]
#
#
# if side == 'left' and direction == 'negative' and traj[-1] < traj[0]:
#     ind = np.where(np.diff(traj) < 0)[0][0]
#
#     for i in range(ind+1, len(traj)):
#         #print(i)
#         traj[i] = flip_dict[traj[i]]


# --- IN FRONT OR BEHIND? ------------------------------------------------------

for phase in phases:
    for replay_direction in ['positive', 'negative']:
        df_sel = df[df['epoch'] == 'task']
        df_sel = df_sel[df_sel['replay_direction'] == replay_direction]

        df_sel = df_sel[df_sel['phase'] == phase]

        df_sel = df_sel[df_sel['current_trial_side'] == df_sel['pbe_side']]

        loc = df_sel['pbe_loc_bin']

        start = df_sel['dec_start_bin']

        end = df_sel['dec_end_bin']

        hist_bins = np.arange(0, 31, 5)

        f, ax = plt.subplots(1, 1, figsize=small_panel_size)
        sns.distplot(np.abs(loc-start), hist=True, norm_hist=True,
                     color=trajectory_cmap(0), ax=ax, bins=hist_bins,
                     label='Distance from start of replay traj.')
        sns.distplot(np.abs(loc-end), hist=True, norm_hist=True,
                     color=trajectory_cmap(255), ax=ax, bins=hist_bins,
                     label='Distance from end of replay traj.')
        ax.legend()
        plot_name = 'distance_between_loc_and_traj_{}_{}.{}'.format(phase, replay_direction, plot_format)
        ax.set_ylabel('Density')
        ax.set_xlabel('Location')
        # ax.set_xlim([-29, 29])
        # ax.set_xticks([-29, -15, 0, 15, 29])
        # ax.set_xticklabels([-29, -15, 0, 15, 29])
        #
        ax.set_xlim([0, 29])
        ax.set_xticks([0, 15, 29])
        ax.set_xticklabels([0, 15, 29])
        sns.despine()
        plt.tight_layout()
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

# --- STATISTICS ---------------------------------------------------------------

n_sig_events = df.shape[0]
n_not_sig_events = df_not_sig.shape[0]

n_totale_events = n_sig_events + n_not_sig_events

print('{} events detected, of which {} ({}%) were significant'.format(
    n_totale_events, n_sig_events, np.round(100*n_sig_events/n_totale_events, 2)))


df['phase'].value_counts()


# --- MAKE LEGENDS ------------------------------------------------------------


colors = [replay_direction_palette['positive'],
          replay_direction_palette['negative']]
texts = [replay_direction_labels['positive'], replay_direction_labels['negative']]
patches = [ plt.plot([],[], marker="o", ms=10, ls="", mec=None, color=colors[i],
            label="{:s}".format(texts[i]) )[0]  for i in range(len(texts)) ]
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=patches, frameon=False)
ax.axis('off')
plt.tight_layout()
plot_name = 'replay_direction_legend_1.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



colors = [replay_direction_palette['positive'],
          replay_direction_palette['negative']]
texts = [replay_direction_labels['positive'], replay_direction_labels['negative']]
patches = [ plt.plot([],[], marker="o", ms=10, ls="", mec=None, color=colors[i],
            label="{:s}".format(texts[i]) )[0]  for i in range(len(texts)) ]
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=patches, frameon=False)
ax.axis('off')
plt.tight_layout()
plot_name = 'replay_direction_legend_2.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




pos_patch = mpatches.Patch(color=replay_direction_palette['positive'],
                           label='Task-direction replay')
neg_patch = mpatches.Patch(color=replay_direction_palette['negative'],
                           label='Opposite-direction replay')
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=[pos_patch, neg_patch], frameon=False)
ax.axis('off')
plt.tight_layout()

plot_name = 'replay_direction_legend_3.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



pos_patch = mpatches.Patch(edgecolor=replay_direction_palette['positive'],
                           facecolor='white', linewidth=2.5,
                           label=replay_direction_labels['positive'])
neg_patch = mpatches.Patch(edgecolor=replay_direction_palette['negative'],
                           facecolor='white', linewidth=2.5,
                           label=replay_direction_labels['negative'])
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=[pos_patch, neg_patch], frameon=False)
ax.axis('off')
plt.tight_layout()

plot_name = 'replay_direction_legend_4.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


colors = [beginning_end_palette['dec_start_bin'],
          beginning_end_palette['dec_end_bin']]
texts = ["Trajectory start", "Trajectory end"]
patches = [ plt.plot([],[], marker="o", ms=10, ls="", mec=None, color=colors[i],
            label="{:s}".format(texts[i]) )[0]  for i in range(len(texts)) ]
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=patches, frameon=False)
ax.axis('off')
plt.tight_layout()
plot_name = 'start_end_legend_1.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


pos_patch = mpatches.Patch(color=beginning_end_palette['dec_start_bin'],
                           label='Trajectory start')
neg_patch = mpatches.Patch(color=beginning_end_palette['dec_end_bin'],
                           label='Trajectory end')
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=[pos_patch, neg_patch], frameon=False)
ax.axis('off')
plt.tight_layout()

plot_name = 'start_end_legend_2.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- DISTRIBUTION OF REPLAY SCORE ---------------------------------------------

df_sel = df[df['epoch'] == 'task']

bins = np.arange(0, df['replay_z_score_column_cycle_shuffle'].max()+1)
f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
sns.distplot(df_sel['replay_z_score_column_cycle_shuffle'], ax=ax,
             bins=bins, label=epoch)
ax.set_xlabel('Replay score')
ax.set_ylabel('Density')
ax.set_xlim([df_sel['replay_z_score_column_cycle_shuffle'].min(), ax.get_xlim()[1]])
sns.despine()
plt.tight_layout()

plot_name = 'replay_score_kde.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- DISTRIBUTION OF COMPRESSION FACTOR ---------------------------------------

df_sel = df[df['epoch'] == 'task']

bins = np.arange(0, df['compression_factor'].max()+1)
f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
sns.distplot(df_sel['compression_factor'], ax=ax,
             bins=bins, label=epoch)
ax.set_xlabel('Replay compression factor')
ax.set_ylabel('Density')
ax.set_xlim([df_sel['compression_factor'].min(), ax.get_xlim()[1]])
sns.despine()
plt.tight_layout()

plot_name = 'compression_factor_kde.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# # --- PERCENTAGE OF POSITIVE/NEGATIVE DIRECTION EVENTS AVERAGED ACROSS SESSIONS
# df_sel = df[df['epoch'] == 'task']
#
# dfy = pd.DataFrame(columns=['sess_ind', 'replay_direction', 'perc_pos', 'perc_neg'])
# for sess_ind in df_sel['sess_ind'].unique():
#
#     dfx = df_sel[df_sel['sess_ind'] == sess_ind]
#
#     npos = dfx[dfx['replay_direction'] == 'positive'].shape[0]
#     nneg = dfx[dfx['replay_direction'] == 'negative'].shape[0]
#
#     perc_pos = 100 * npos / (npos+nneg)
#     dfy.loc[dfy.shape[0], :] = sess_ind
#
#
# #print('Difference between % of forward and backward events: p={}'.format(p_val))
# directions = ['positive', 'negative']
# f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
# sns.barplot(data=dfy, x='replay_direction', y='percentage',
#             edgecolor=[replay_direction_palette[d] for d in directions],
#             facecolor='white', linewidth=3.5,
#             errcolor='0.2')
# ax.set_xticklabels(['Task\ndirection', 'Opposite\ndirection'])
# ax.set_ylabel('Percentage')
# ax.set_xlabel('')
# #ax.legend(frameon=False, loc='upper right')
# sns.despine()
# plt.tight_layout()
#
# plot_name = 'forward_backward_barplot.{}'.format(plot_format)
# f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# --- VISITED LOCATIONS SCATTER ------------------------------------------------

df_sel = df[(df['epoch'] == 'task')]

traj = np.hstack(df_sel['trajectory'])


traj = merge_duplicate_left_right_bins(traj)
bin_counts = pd.value_counts(traj, normalize=True)

f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

for bin, counts in bin_counts.iteritems():
    #print(bin, counts)
    x, y = get_position_bin(bin, bin_centers, bin_labels)
    ax.scatter(x, y, s=counts*1000, c='grey')
plot_maze(ax)
mark_locations(ax)
plt.tight_layout()
ax.axis('off')

plot_name = 'locations_representation_in_decoded_traj.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- DIFFERENCE BETWEEN PHASES IN VISITED LOCATIONS ---------------------------

if group_iti_and_return:
    phase_pairs = [('iti', 'reward'),
                   ('iti', 'img')]

else:
    phase_pairs = [('iti', 'reward'),
                   ('iti', 'return'),
                   ('reward', 'return'),
                   ('iti', 'img')]

for phase1, phase2 in phase_pairs:
    df_phase1 = df[(df['epoch'] == 'task') & (df['phase'] == phase1)]
    df_phase2 = df[(df['epoch'] == 'task') & (df['phase'] == phase2)]

    phase1_traj = np.hstack(df_phase1['trajectory'])
    phase2_traj = np.hstack(df_phase2['trajectory'])

    phase1_traj = merge_duplicate_left_right_bins(phase1_traj)
    phase2_traj = merge_duplicate_left_right_bins(phase2_traj)

    phase1_series = pd.Series(phase1_traj, dtype='category')
    phase1_series = phase1_series.cat.set_categories(np.arange(-29, 30))

    phase2_series = pd.Series(phase2_traj, dtype='category')
    #check = np.array(phase2_series)
    phase2_series = phase2_series.cat.set_categories(np.arange(-29, 30))
    #chheck = np.array(phase2_series)
    #np.testing.assert_array_equal(check, chheck)

    bin_counts_phase1 = pd.value_counts(phase1_series, normalize=True).sort_index()
    bin_counts_phase2 = pd.value_counts(phase2_series, normalize=True).sort_index()

    bin_counts_diff = bin_counts_phase1 - bin_counts_phase2

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

    for bin, counts in bin_counts_diff.iteritems():
        #print(bin, counts)
        x, y = get_position_bin(bin, bin_centers, bin_labels)
        if counts > 0:
            ax.scatter(x, y, s=counts*dot_scaling, c=sns.xkcd_rgb['lightish blue'])
        elif counts < 0:
            ax.scatter(x, y, s=-counts*dot_scaling, c=sns.xkcd_rgb['lightish red'])
    plot_maze(ax)
    mark_locations(ax)
    #ax.set_title('{}-{}'.format(phase1, phase2))
    plt.tight_layout()
    ax.axis('off')

    plot_name = 'difference_in_loc_representation_{}-{}.{}'.format(phase1,phase2,
                  plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- DIFFERENCE BETWEEN PHASES IN VISITED LOCATIONS VERSION 2 ----------------
"""
This is the same as the one before but instead of comparing one task phase against
another we compare a phase against all other phases
"""

if group_iti_and_return:
    phase_pairs = [('iti', ['reward', 'img', 'run']),
                   ('reward', ['iti', 'img', 'run']),
                   ('img', ['iti', 'reward', 'run'])]
else:
    phase_pairs = [('iti', ['reward', 'return', 'img', 'run']),
                   ('reward', ['iti', 'return', 'img', 'run']),
                   ('img', ['iti', 'return', 'reward', 'run'])]

for phase1, phases2 in phase_pairs:
    df_phase1 = df[(df['epoch'] == 'task') & (df['phase'] == phase1)]
    df_phase2 = df[(df['epoch'] == 'task') & (np.isin(df['phase'], phases2))]

    phase1_traj = np.hstack(df_phase1['trajectory'])
    phase2_traj = np.hstack(df_phase2['trajectory'])

    phase1_traj = merge_duplicate_left_right_bins(phase1_traj)
    phase2_traj = merge_duplicate_left_right_bins(phase2_traj)

    phase1_series = pd.Series(phase1_traj, dtype='category')
    phase1_series = phase1_series.cat.set_categories(np.arange(-29, 30))

    phase2_series = pd.Series(phase2_traj, dtype='category')
    #check = np.array(phase2_series)
    phase2_series = phase2_series.cat.set_categories(np.arange(-29, 30))
    #chheck = np.array(phase2_series)
    #np.testing.assert_array_equal(check, chheck)

    bin_counts_phase1 = pd.value_counts(phase1_series, normalize=True).sort_index()
    bin_counts_phase2 = pd.value_counts(phase2_series, normalize=True).sort_index()

    bin_counts_diff = bin_counts_phase1 - bin_counts_phase2

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

    for bin, counts in bin_counts_diff.iteritems():
        #print(bin, counts)
        x, y = get_position_bin(bin, bin_centers, bin_labels)
        if counts > 0:
            ax.scatter(x, y, s=counts*dot_scaling, c=sns.xkcd_rgb['lightish blue'])
        elif counts < 0:
            ax.scatter(x, y, s=-counts*dot_scaling, c=sns.xkcd_rgb['lightish red'])
    plot_maze(ax)
    mark_locations(ax)
    #ax.set_title('{}-{}'.format(phase1, phase2))
    plt.tight_layout()
    ax.set_title(phase1)
    ax.axis('off')

    plot_name = 'difference_in_loc_representation_vs_all_others_{}.{}'.format(phase1,
                  plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# --- PBE LOCATION, START AND END OF TRAJ. FOR FORWARD / BACKWARD -------------

for event in ['pbe_loc_bin', 'dec_start_bin', 'dec_end_bin']:

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
    plot_maze(ax, c='grey')
    mark_locations(ax, color=maze_color)

    for replay_direction in ['positive', 'negative']:

        df_sel = df[(df['epoch'] == 'task') & (df['replay_direction'] == replay_direction)]

        bins = merge_duplicate_left_right_bins(np.array(df_sel[event]))
        bin_counts = pd.value_counts(bins, normalize=True)
        for bin, counts in bin_counts.iteritems():
            #print(bin, counts)
            x, y = get_position_bin(bin, bin_centers, bin_labels)
            if replay_direction == 'positive':
                x = x - 4
            if replay_direction == 'negative':
                x = x + 3
            ax.scatter(x, y, s=counts * dot_scaling,
                       c=replay_direction_palette[replay_direction],
                       zorder=10)
        ax.axis('off')
        #plt.tight_layout()
        #ax.set_title(event)
        plot_name = 'difference_in_representation_of_{}_forward_vs_backward.{}'.format(event,
                                                                       plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- VISITED LOCATIONS BY REPLAY DIRECTION ------------------------------------

f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

for replay_direction in ['positive', 'negative'] :
    df_sel = df[(df['epoch'] == 'task') & (df['replay_direction'] == replay_direction)]
    traj = np.hstack(df_sel['trajectory'])
    traj = merge_duplicate_left_right_bins(traj)
    bin_counts = pd.value_counts(traj, normalize=True)

    for bin, counts in bin_counts.iteritems() :
        # print(bin, counts)
        x, y = get_position_bin(bin, bin_centers, bin_labels)

        if replay_direction == 'positive' :
            x = x - 4
        if replay_direction == 'negative' :
            x = x + 3
        ax.scatter(x, y, s=counts * dot_scaling,
                   c=replay_direction_palette[replay_direction],
                   zorder=10)
        ax.scatter(x, y, s=counts * 1000, c='grey')
    plot_maze(ax, c=maze_color)
    mark_locations(ax, color=maze_color)
    plt.tight_layout()
    ax.axis('off')

plot_name = 'locations_representation_in_decoded_traj_positive_negative.{}'.format(
    plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# for event in ['dec_start_bin', 'dec_end_bin']:
#
#     f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#
#     for task_phase in ['iti', 'reward', 'return']:
#
#         df_sel = df[(df['epoch'] == 'task') & (df['phase'] == task_phase)
#                     & (df['replay_direction'] == 'positive')]
#         bin_counts = pd.value_counts(df_sel[event], normalize=True)
#         for bin, counts in bin_counts.iteritems():
#             #print(bin, counts)
#             x, y = get_position_bin(bin, bin_centers, bin_labels)
#             if task_phase == 'iti':
#                 x = x
#             elif task_phase == 'return':
#                 x = x + 7
#             elif task_phase == 'reward':
#                 x = x - 7
#             ax.scatter(x, y, s=counts * 1000, c=task_phase_palette[task_phase],
#                        zorder=10)
#         plot_maze(ax, c='grey')
#         plt.tight_layout()
#         ax.axis('off')
#         ax.set_title(event)


# --- CHI SQUARED TEST FOR SPATIAL DISTRIBUTION --------------------------------

if group_iti_and_return:
    phases = ['img', 'iti', 'reward', 'run']
else:
    phases = ['img', 'iti', 'return', 'reward', 'run']



bin_counts_per_phase = {}
for task_phase in phases:

    # df_sel = df[(df['epoch'] == 'task') & (df['phase'] == task_phase)
    #             & (df['replay_direction'] == 'positive')]

    df_sel = df[(df['epoch'] == 'task') & (df['phase'] == task_phase)]
    #print(df_sel.shape[0])

    #bins = np.hstack(df_sel['trajectory'])
    bins = df_sel['dec_start_bin']
    # TODO CHECK THIS, replacement or not?
    #bins = np.random.choice(bins, 400, replace=True) #downsampling should be required
    # but we try it anyways

    bin_freq = bin_occurrences_for_chi_square_test(bins)

    bin_counts_per_phase[task_phase] = bin_freq


import scipy.stats
import itertools
for phase1, phase2 in list(itertools.combinations(phases,2)):

    rs = scipy.stats.chisquare(f_obs=bin_counts_per_phase[phase1],
                               f_exp=bin_counts_per_phase[phase2])
    print('Difference in spatial distribution between {} and {}: p={:.2e}'.format(
        phase1, phase2, rs[1]
    ))




x = np.arange(8)
y = bin_counts_per_phase['img']
y2 = bin_counts_per_phase['reward']

f, ax = plt.subplots(1, 1)
plt.bar(x, y)
plt.bar(x+0.2, y2)


# --- START AND END OF TRAJ. FOR DIFFERENT PHASES -----------------------------
if group_iti_and_return:
    phases = ['img', 'iti', 'reward', 'run']
else:
    phases = ['img', 'iti', 'return', 'reward', 'run']

for task_phase in phases:

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

    for event in ['dec_start_bin', 'dec_end_bin']:

        df_sel = df[(df['epoch'] == 'task') & (df['phase'] == task_phase)
                    & (df['replay_direction'] == 'positive')]

        bins = merge_duplicate_left_right_bins(np.array(df_sel[event]))
        bin_counts = pd.value_counts(bins, normalize=True)

        for bin, counts in bin_counts.iteritems():
            #print(bin, counts)
            x, y = get_position_bin(bin, bin_centers, bin_labels)
            if event == 'dec_start_bin':
                x = x - 5
            elif event == 'dec_end_bin':
                x = x + 5
            ax.scatter(x, y, s=counts * dot_scaling, c=[beginning_end_palette[event]],
                       zorder=10)
        plot_maze(ax, c=maze_color)
        mark_locations(ax, color=maze_color)
        plt.tight_layout()
        ax.axis('off')
        #ax.set_title(task_phase)

    plot_name = 'start_and_end_traj_{}.{}'.format(task_phase, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- DISTANCE BETWEEN BIN OF PBE AND START DEC BIN ---------------------------

df_sel = df[df['epoch'] == 'task']

dist_dict = {}
for xlab, ylab, lab in [('x_pos_dec_start', 'y_pos_dec_start', 'Beginning'),
                   ('x_pos_dec_end', 'y_pos_dec_end', 'End')]:
    pbe_loc = np.vstack((df_sel['x_pos'].values, df_sel['y_pos'])).T
    dec_start = np.vstack((df_sel[xlab].values, df_sel[ylab])).T

    #dec_start = dec_start[np.random.permutation(np.arange(dec_start.shape[0]))]

    distance = np.linalg.norm((pbe_loc-dec_start), axis=1)
    dist_dict[lab] = distance

t, p = wilcoxon(dist_dict['Beginning'], dist_dict['End'])
print('distance of pbe location to beginning/end of trajectory is different'
      ' p={:.3}'.format(p))

# --- DISTANCE BETWEEN BIN OF PBE AND START DEC BIN ---------------------------


hist_bins = np.linspace(0, 120, num=15)

f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

for dictlab, xlab, ylab, lab in [('dec_start_bin', 'x_pos_dec_start', 'y_pos_dec_start', 'Beginning'),
                   ('dec_end_bin','x_pos_dec_end', 'y_pos_dec_end', 'End')]:
    pbe_loc = np.vstack((df_sel['x_pos'].values, df_sel['y_pos'])).T
    dec_start = np.vstack((df_sel[xlab].values, df_sel[ylab])).T

    #dec_start = dec_start[np.random.permutation(np.arange(dec_start.shape[0]))]

    distance = np.linalg.norm((pbe_loc-dec_start), axis=1)

    sns.distplot(distance, hist=True, bins=hist_bins, label=lab,
                 color=beginning_end_palette[dictlab])
ax.set_xlabel('Distance from actual\nanimal location during PBE')
ax.set_ylabel('Density')
leg = ax.legend(frameon=False)
leg.get_texts()[0].set_text('Start of\nreplay\ntrajectory')
leg.get_texts()[1].set_text('End of\nreplay\ntrajectory')

#ax.get_legend().remove()
ax.set_xlim([0, ax.get_xlim()[1]])
sns.despine()
plt.tight_layout()



plot_name = 'distance_between_pbe_loc_and_beginning_end_of_traj.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- BARPLOT DISTANCE BETWEEN BIN OF PBE AND START DEC BIN --------------------

for direction in ['all', 'positive', 'negative']:

    print('\n{}'.format(direction))
    df_sel = df[df['epoch'] == 'task']

    if direction != 'all':
        df_sel = df_sel[df_sel['replay_direction'] == direction]

    df_sel = df_sel[~df_sel['pbe_loc_bin'].isna()]

    phase = df_sel['phase']
    pbe_loc = np.vstack((df_sel['x_pos'].values, df_sel['y_pos'])).T
    dec_start = np.vstack((df_sel['x_pos_dec_start'].values, df_sel['y_pos_dec_start'])).T
    dec_end = np.vstack((df_sel['x_pos_dec_end'].values, df_sel['y_pos_dec_end'])).T

    dist_start = np.linalg.norm((pbe_loc - dec_start), axis=1)
    dist_end = np.linalg.norm((pbe_loc - dec_end), axis=1)

    df1 = pd.DataFrame(columns=['phase', 'which', 'dist'])
    df1['phase'] = phase
    df1['which'] = 'start'
    df1['dist'] = dist_start

    df2 = pd.DataFrame(columns=['phase', 'which', 'dist'])
    df2['phase'] = phase
    df2['which'] = 'end'
    df2['dist'] = dist_end

    dfxx = pd.concat([df1, df2])
    dfxx['dist'] = pd.to_numeric(dfxx['dist'])

    trajectory_cmap = plt.cm.winter_r

    for phase in ['iti', 'img', 'run', 'reward']:
        dfy = dfxx[dfxx['phase'] == phase]
        d1 = dfy[dfy['which'] == 'start']
        d2 = dfy[dfy['which'] == 'end']
        np.testing.assert_array_equal(d1.index.values, d2.index.values)
        x = d1['dist']
        y = d2['dist']
        s, p = mannwhitneyu(x, y)
        print('{}, p={:.1}'.format(phase, p*4))

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
    sns.boxplot(data=dfxx, x='phase', hue='which', y='dist',
                order=['iti', 'img', 'run', 'reward'], hue_order=['start', 'end'],
                palette={'start' : trajectory_cmap(0), 'end' : trajectory_cmap(255)})

    sns.despine()
    ax.set_ylabel('Distance from actual\nanimal location during PBE [cm]')
    ax.set_xlabel('')
    #ax.set_ylabel('Density')
    ax.set_xticklabels([task_phase_labels[l._text] for l in ax.get_xticklabels()],
                       rotation=30)
    leg = ax.legend(frameon=False)

    ax.get_legend().remove()
    plt.tight_layout()

    plot_name = 'distance_per_phase_between_pbe_loc_and_beginning_end_of_traj_{}.{}'.format(direction, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



f, ax = plt.subplots(1, 1, figsize=[3, 1])
leg = ax.legend(handles=leg.legendHandles, frameon=False, title='')
leg.get_texts()[0].set_text('Start of replay trajectory')
leg.get_texts()[1].set_text('End of replay trajectory')
ax.axis('off')
plot_name = 'distance_between_leg.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)





# --- EVENT DURATION ----------------------------------------------------------
df_sel = df[df['epoch'] == 'task']
f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
sns.distplot(df_sel['event_duration_in_ms'], ax=ax)
ax.set_xlabel('Event duration [ms]')
ax.set_ylabel('Density')
ax.set_xlim([df_sel['event_duration_in_ms'].min(), ax.get_xlim()[1]])
#ax.legend(frameon=False, loc='upper right')
sns.despine()
plt.tight_layout()

plot_name = 'event_duration_kde.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- BOXPLOTS ----------------------------------------------------------------
#
# vars = ['percentage_active', 'dist_traversed_cm', 'max_jump_dist',
#         'mean_jump_dist', 'replay_z_score_column_cycle_shuffle',
#         'percentage_active']
#
# vars = ['replay_score', 'dist_traversed_cm',
#               'sharpness']
#
# #hues = ['replay_direction', 'phase', 'has_ripple']
# hues = ['replay_direction']
#
# df_sel = df[df['epoch'] == 'task']
#
# for var in vars:
#     for hue in hues:
#         f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#         sns.boxplot(data=df_sel, y=var, x=hue, ax=ax)
#
#         ax.set_ylabel(var)
#         #ax.legend(frameon=False, loc='upper right')
#         sns.despine()
#         plt.tight_layout()
#
#         plot_name = 'barplot_{}_across_{}.{}'.format(var, hue, plot_format)
#         f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#


# --- STATISTICAL COMPARISON OF FORWARD AND BACKWARD EVENT --------------------

metrics = ['replay_score', 'dist_traversed_cm', 'event_duration_in_ms',
           'percentage_active', 'max_jump_dist', 'mean_jump_dist',
              'av_running_speed', 'sharpness', 'compression_factor']



for metric in metrics:
    df_pos = df[(df['epoch'] == 'task')
                & (df['replay_direction'] == 'positive')]
    df_neg = df[(df['epoch'] == 'task')
                & (df['replay_direction'] == 'negative')]


    x = df_pos[metric].values
    y = df_neg[metric].values
    st,pval = mannwhitneyu(x, y)
    print('\n{}'.format(metric))
    print('task-direction: {:.2f}, {:.2f}-{:.2f}'.format(
        df_pos[metric].quantile(0.5),
        df_pos[metric].quantile(0.25),
        df_pos[metric].quantile(0.75)))
    print('opposite-direction: {:.2f}, {:.2f}-{:.2f}'.format(
        df_neg[metric].quantile(0.5),
        df_neg[metric].quantile(0.25),
        df_neg[metric].quantile(0.75)))
    print('mann-whitney U: p={:.2e}'.format(pval))

    df_sel = df[(df['epoch'] == 'task')]
    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
    sns.violinplot(data=df_sel, x='replay_direction', y=metric,
                   palette=replay_direction_palette, inner='quartiles')
    ax.set_title('{}\np={:.2e}'.format(metric, pval))
    sns.despine()
    plt.tight_layout()
    plot_name = 'task_vs_opposite_direction_barplot_{}.{}'.format(metric, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
    plt.close(fig=f)






# --- SHARPNESS ---------------------------------------------------------------
df_sel = df[df['epoch'] == 'task']
f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
sns.distplot(pd.to_numeric(df_sel['sharpness']), ax=ax)
ax.set_xlabel('Sharpness')
ax.set_ylabel('Density')
#ax.legend(frameon=False, loc='upper right')
sns.despine()
plt.tight_layout()
plt.close(fig=f)



# --- PLOT ACTUAL TRAJECTORIES ------------------------------------------------
trajectory_cmap = plt.cm.winter_r

f, ax = plt.subplots(1, 1, figsize=[3, 1])
x = np.arange(0, 100)
y = np.repeat(0, x.shape[0])

N = y.shape[0]
for i in range(N) :
    colors = trajectory_cmap(int(255 * i / N))
    ax.plot(x[i:i+2], y[i:i+2], color=colors, lw=3)

c_start = trajectory_cmap(int(255 * 0 / N))
c_end = trajectory_cmap(int((255 * N - 1) / N))
ax.scatter(x[0], y[0], c=c_start, zorder=10, s=20)
ax.scatter(x[-1], y[-1], c=c_end, zorder=10, s=20)
ax.text(x[0]-30, y[0]+0.05, 'Trajectory start')
ax.text(x[-1]-30, y[0]+0.05, 'Trajectory end')
plt.tight_layout()
ax.axis('off')

plot_name = 'trajectories_example_line.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
plt.close(fig=f)



trajectory_cmap = plt.cm.winter_r
#df_sel = df[(df['epoch'] == 'task') & (df['replay_direction'] == 'positive')]

df_sel_1 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'iti') & (df['replay_direction'] == 'positive')]

df_sel_2 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'img') & (df['replay_direction'] == 'positive')]

df_sel_3 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'run') & (df['replay_direction'] == 'positive')]

df_sel_4 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'reward') & (df['replay_direction'] == 'positive')]


df_sel_5 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'iti')]

df_sel_6 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'img')]

df_sel_7 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'run')]

df_sel_8 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'reward')]



dfs = [df_sel_1, df_sel_2, df_sel_3, df_sel_4,
       df_sel_5, df_sel_6, df_sel_7, df_sel_8]
labels = ['forward_during_iti', 'forward_during_img',
          'forward_during_run', 'forward_during_reward',
          'during_iti', 'during_img',
          'during_run', 'during_reward']

n_traj = 8
offsets = np.linspace(-4, 4, n_traj)
for df_sel, label in zip(dfs, labels):
    trajectories=  np.random.choice(df_sel['trajectory'].values, min(n_traj, df_sel.shape[0]),
                                    replace=False)

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

    for i, trajectory in enumerate(trajectories):
        xy_pos = np.vstack([get_position_bin(bin, bin_centers, bin_labels) for bin in trajectory])

        # xy_pos[1:-1, 0] += np.random.normal(loc=0, scale=3)
        # xy_pos[1:-1, 1] += np.random.normal(loc=0, scale=2)
        # xy_pos[:, 0] += np.random.uniform(low=-5, high=5)
        # xy_pos[:, 1] += np.random.uniform(low=-5, high=5)

        xy_pos[:, 0] += offsets[i]
        xy_pos[:, 1] += offsets[i]

        xy_pos[:, 0] = gaussian_filter1d(xy_pos[:, 0], sigma=1)
        xy_pos[:, 1] = gaussian_filter1d(xy_pos[:, 1], sigma=1)

        N = xy_pos.shape[0]-1
        for i in range(N):
            colors = trajectory_cmap(int(255 * i / N))
            ax.plot(xy_pos[i:i+2, 0], xy_pos[i:i+2, 1],color=colors)
        #ax.plot(xy_pos[:, 0], xy_pos[:, 1], c='blue', alpha=0.6)
        c_start = trajectory_cmap(int(255 * 0 / N))
        c_end = trajectory_cmap(int((255 * N - 1) / N))
        ax.scatter(*xy_pos[0, :], c=c_start, zorder=10, s=12)
        ax.scatter(*xy_pos[-1, :], c=c_end, zorder=10, s=12)

    plot_maze(ax, c=maze_color)
    mark_locations(ax, color=maze_color)
    ax.axis('off')
    plt.tight_layout()

    plot_name = 'trajectories_{}.{}'.format(label, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
    plt.close(fig=f)



# --- PREDICTING CURRENT/PREVIOUS TRIAL ---------------------------------------

side_col = 'maze_arm_dec_end'

#side_col = 'pbe_side'



if group_iti_and_return:
    phases = ['img', 'iti', 'reward', 'run']
else:
    phases = ['img', 'iti', 'return', 'reward', 'run']

for task_phase in phases:
    print('\n\nTask phase: {}'.format(task_phase))
    for trial in ['previous_trial_side', 'current_trial_side']:

        df_sel = df[(df['epoch'] == 'task') & (df[side_col] != 'central')
                    & (df['phase'] == task_phase)]


        mask = np.logical_and(np.isin(df_sel[side_col], ['left', 'right']),
                              np.isin(df_sel[trial], ['left', 'right']))
        df_sel = df_sel[mask]
        a1_side = df_sel[side_col][1:-1]
        a2_side = df_sel[trial][1:-1]

        from sklearn.metrics import accuracy_score, balanced_accuracy_score

        accuracy = balanced_accuracy_score(a1_side, a2_side)
        accuracy_shuff = []

        for ii in range(50):
            accuracy_shuff.append(balanced_accuracy_score(a1_side, np.random.permutation(a2_side)))
        accuracy_shuff = np.array(accuracy_shuff)

        p_val = (accuracy_shuff > accuracy).sum() / accuracy_shuff.shape[0]
        print('\n- Match with {}'.format(trial))
        print('   - # of events: {}'.format(a1_side.shape[0]))
        print('   - accuracy: {}, average accuracy shuff {}'.format(accuracy, np.mean(accuracy_shuff)))
        print('   - p_val = {}'.format(p_val))





# --- PERCENTAGE OF NEGATIVE EVENTS PER TASK PHASE ----------------------------

df_sel = df[df['epoch'] == 'task']

for col in ['replay_direction']:
    group1 = df.groupby(['phase', col]).agg({col:'count'})
    group2 = df.groupby(['phase']).agg({col:'count'})
    perc_col_per_set = group1.div(group2, level='phase') * 100
    print(perc_col_per_set)

if group_iti_and_return:
    phases = ['img', 'iti', 'reward', 'run']
else:
    phases = ['img', 'iti', 'return', 'reward', 'run']

percs = []
for phase in phases:
    df_sel = df[(df['epoch'] == 'task') & (df['phase'] == phase)]
    npos = df_sel[df['replay_direction'] == 'positive'].shape[0]
    nneg = df_sel[df['replay_direction'] == 'negative'].shape[0]
    percs.append(100 * npos / (npos+nneg))

f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
sns.barplot(x=phases, y=percs, color=barplot_color)
sns.despine()
ax.set_ylabel('% of task-direction replay')
ax.set_xticklabels([task_phase_labels[l._text] for l in ax.get_xticklabels()])
plt.tight_layout()

plot_name = 'perc_neg_events_per_task_phase.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
plt.close(fig=f)




# --- NUMBER OF EVENTS PER TASK PHASE -----------------------------------------

if group_iti_and_return:
    phases = ['img', 'iti', 'reward', 'run']
else:
    phases = ['img', 'iti', 'return', 'reward', 'run']

nevents = []
for phase in phases:
    nevents.append(df[(df['epoch'] == 'task') & (df['phase'] == phase)].shape[0])

f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
sns.barplot(x=phases, y=nevents, color=barplot_color)
sns.despine()
ax.set_ylabel('# of replay events')
plt.tight_layout()

plot_name = 'number_of_events_per_task_phase.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
plt.close(fig=f)

# --- PERCENTAGE OF EVENTS PER TASK PHASE -------------------------------------

if group_iti_and_return:
    phases = ['img', 'iti', 'reward', 'run']
else:
    phases = ['img', 'iti', 'return', 'reward', 'run']

percevents = []
for phase in phases:
    ne = df[(df['epoch'] == 'task') & (df['phase'] == phase)].shape[0]
    ntot = df[(df['epoch'] == 'task')].shape[0]
    percevents.append(100*ne/ntot)

f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
sns.barplot(x=phases, y=percevents, color=barplot_color)
sns.despine()
ax.set_ylabel('% of replay events')
plt.tight_layout()

plot_name = 'perc_of_events_per_task_phase.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
plt.close(fig=f)


# --- REPLAY RATE -------------------------------------------------------------

# if group_iti_and_return:
#     phases = ['all', 'img', 'iti', 'reward', 'run']
# else:
#     phases = ['all', 'img', 'iti', 'return', 'reward', 'run']
#
# rs = pd.DataFrame(columns=['sess_ind', 'phase', 'replay_rate'])
#
# for sess_ind in df['sess_ind'].unique():
#
#     timespent = get_time_spent_per_task_phase(sess_ind, data_version=data_version)
#     df_sel = df[(df['epoch'] == 'task') & (df['sess_ind'] == sess_ind)]
#
#     for phase in phases:
#
#         if phase == 'all':
#             nreplay = df_sel.shape[0]
#         else:
#             nreplay = df_sel[df_sel['phase'] == phase].shape[0]
#
#         replayrate = nreplay / timespent[phase].rescale(pq.min)
#
#         rs.loc[rs.shape[0], :] = [sess_ind, phase, replayrate]
#
#
# rs['replay_rate'] = pd.to_numeric([i.item() for i in rs['replay_rate']])
#
# f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
#
# sns.barplot(x='phase', y='replay_rate', data=rs,
#             edgecolor=barplot_color, facecolor='white', errcolor='0.2',
#             linewidth=3.5)
# ax.set_ylabel('Replay events per minute')
# ax.set_xlabel('')
# ax.set_xticklabels([task_phase_labels[l._text] for l in ax.get_xticklabels()])
# sns.despine()
# plt.tight_layout()




if group_iti_and_return:
    phases = ['iti', 'img', 'run', 'reward']
else:
    phases = ['all', 'img', 'iti', 'return', 'reward', 'run']

rs = pd.DataFrame(columns=['sess_ind', 'phase', 'replay_rate'])

for sess_ind in df['sess_ind'].unique():

    timespent = get_time_spent_per_task_phase(sess_ind,
                                              data_version=data_version,
                                              speed_threshold=12)
    df_sel = df[(df['epoch'] == 'task') & (df['sess_ind'] == sess_ind)]

    for phase in phases:

        if phase == 'all':
            nreplay = df_sel.shape[0]
        else:
            nreplay = df_sel[df_sel['phase'] == phase].shape[0]

        replayrate = nreplay / timespent[phase].rescale(pq.min)

        rs.loc[rs.shape[0], :] = [sess_ind, phase, replayrate]


rs['replay_rate'] = pd.to_numeric([i.item() for i in rs['replay_rate']])

f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

sns.barplot(x='phase', y='replay_rate', data=rs,
            edgecolor=sns.xkcd_rgb['dark grey'], facecolor='white', errcolor='0.2',
            linewidth=1.5, order=phases)
ax.set_ylabel('Replay events per minute')
ax.set_xlabel('')
ax.set_xticklabels([task_phase_labels[l._text] for l in ax.get_xticklabels()],
                   rotation=30)
# plt.setp(ax.artists, edgecolor=sns.xkcd_rgb['dark grey'], facecolor='w')
# plt.setp(ax.lines, color=sns.xkcd_rgb['dark grey'])
sns.despine()
plt.tight_layout()

plot_name = 'replay_rate_per_task_phase.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

plt.close(fig=f)


#combos = list(itertools.combinations(phases, 2))

combos = [('iti', 'img'),
          ('iti', 'reward'),
          ('iti', 'run')]

for phase1, phase2 in combos:

    df1 = rs[(rs['phase'] == phase1)]
    df2 = rs[(rs['phase'] == phase2)]

    #print(df1[col].hasnans, df2[col].hasnans)
    x = df1['replay_rate'].values
    y = df2['replay_rate'].values
    #print(np.median(x), np.median(y))
    st,pval = wilcoxon(x, y)
    print('{}, {} vs {} medians: {:.2f} vs {:.2f}: {}, p_val = {:.1e}, sig: {}'.format('replay_rate',
          phase1, phase2, np.median(x), np.median(y), st, pval, pval<0.05/len(combos)))

    df1 = rs[(rs['phase'] == 'img')]
    df2 = rs[(rs['phase'] == 'run')]
    df3 = rs[(rs['phase'] == 'iti')]


# --- AVERAGE COMPRESSION FACTOR ---

av_cf = df[(df['epoch'] == 'task')]['compression_factor'].mean()
print('Average compression factor: {}'.format(av_cf))


# --- REPLAY METRICS -----------------------------------------------------------


if group_iti_and_return:
    phases = ['iti', 'img', 'run', 'reward']
else:
    phases = ['img', 'iti', 'return', 'reward', 'run']


cols = ['replay_score', 'event_duration_in_ms',
        'dist_traversed_cm', 'compression_factor', 'max_jump_dist',
        'mean_jump_dist', 'sharpness', 'n_active']

combos = list(itertools.combinations(phases, 2))

for col in cols:
    print('\n')

    for phase1, phase2 in combos:
        df1 = df[(df['epoch'] == 'task') & (df['phase'] == phase1)]
        df2 = df[(df['epoch'] == 'task') & (df['phase'] == phase2)]

        #print(df1[col].hasnans, df2[col].hasnans)
        x = df1[col].values
        y = df2[col].values
        #print(np.median(x), np.median(y))
        st,pval = mannwhitneyu(x, y)
        print('{}, {} vs {} medians: {:.2f} vs {:.2f}: {}, p_val = {:.3e}, sig: {}'.format(col,
              phase1, phase2, np.median(x), np.median(y), st, pval, pval<0.05/len(combos)))



df_sel = df[df['epoch'] == 'task']

cols = ['replay_score', 'event_duration_in_ms',
        'dist_traversed_cm', 'compression_factor', 'max_jump_dist',
        'mean_jump_dist', 'sharpness', 'n_active']

for col in cols:

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
    sns.boxplot(data=df_sel, x='phase', y=col, color='white',
                fliersize=2, order=phases)
    plt.setp(ax.artists, edgecolor=sns.xkcd_rgb['dark grey'], facecolor='w')
    plt.setp(ax.lines, color=sns.xkcd_rgb['dark grey'])

    if col == 'compression_factor':
        ax.set_yscale("log")
        ax.set_ylim(1, 100)

    #sns.swarmplot(data=df_sel, x='phase', y=col, color=sns.xkcd_rgb['dark grey'])

    ax.set_ylabel(replay_metric_labels[col])
    ax.set_xlabel('')
    ax.set_xticklabels([task_phase_labels_nobreak[l._text] for l in ax.get_xticklabels()],
                       rotation=30)
    sns.despine()
    plt.tight_layout()

    plot_name = 'replay_{}_per_task_phase.{}'.format(col, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

    #plt.close(fig=f)



sns.boxplot(data=dg, x='area', y='corrcoef_debiased', hue='direction',
              order=['Perirhinal', 'Barrel'], dodge=True,
              color='white')
plt.setp(ax.artists, edgecolor=sns.xkcd_rgb['grey'], facecolor='w')
plt.setp(ax.lines, color=sns.xkcd_rgb['grey'])

#
# ax.collections[0].set_edgecolor(violin_color)
# ax.collections[1].set_edgecolor(violin_color)
# for i in range(0, 6) :
#     ax.lines[i].set_color(violin_color)
#     # ax.lines[i].set_alpha(0.7)
#     ax.lines[i].set_linewidth(2.5)
# ax.lines[1].set_linestyle('-')
# ax.lines[4].set_linestyle('-')

sns.swarmplot(data=dg, x='area', y='corrcoef_debiased', hue='direction',
              order=['Perirhinal', 'Barrel'], dodge=True,
              palette=replay_direction_palette)
ax.set_ylabel(variable_label['corrcoef_debiased']+'\n of significant units')
ax.set_xlabel('')
ax.get_legend().remove()
sns.despine()
plt.tight_layout()




f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

plot_maze(ax, c=maze_color)
mark_locations(ax, color=maze_color)
# ax.set_title('{}-{}'.format(phase1, phase2))
plt.tight_layout()
ax.axis('off')
plot_name = 'just_the_maze.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

#epoch_order = ['pre_sleep', 'task', 'post_sleep']

# epoch_order = ['task']
# g = sns.jointplot(x=df['pbe_loc_bin'].values, y=df['dec_start_bin'].values)
# g.ax_joint.set_xlabel('Bin where PBE event occurred')
# g.ax_joint.set_ylabel('Bin where decoded trajectory started')
# plt.tight_layout()
#
#
# g = sns.jointplot(x=df['dec_start_bin'].values, y=df['dec_end_bin'].values,
#                   marginal_kws=dict(bins=30))
# g.ax_joint.set_xlabel('Bin where decoded trajectory started')
# g.ax_joint.set_ylabel('Bin where decoded trajectory ended')
# plt.tight_layout()

# g = sns.jointplot(x=df['pbe_loc_bin'].values, y=df['dec_start_bin'].values)
# g.ax_joint.set_xlabel('Bin where PBE event occurred')
# g.ax_joint.set_ylabel('Bin where decoded trajectory started')


# --- TOTAL NUMBER OF POSITIVE NEGATIVE DIRECTION -----------------------------
# df_sel = df[df['epoch'] == 'task']
# f, ax = plt.subplots(1, 1, figsize=small_panel_size)
# sns.countplot(data=df_sel, x='replay_direction')


