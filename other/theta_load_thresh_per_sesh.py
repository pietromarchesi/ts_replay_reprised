import os
from neo.io import NeuralynxIO
import matplotlib.pyplot as plt
from neuralynx_io import neuralynx_io
import os, glob
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from utils import *
from irasa.IRASA import IRASA
import yasa

data_version         = 'dec16'
PBEs_settings_name   = 'dec19k20'
PBEs_area            = 'Hippocampus'
dec_settings_name    = 'DecSet2'
dec_area             = 'Hippocampus'
sessions             = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
group_iti_and_return = True

lfp_path = '/Users/pietro/data/touch_see/LFP'


# PLOTTING PARAMETERS
save_plots = False
plot_format = 'png'
dpi = 400
min_pbe_duration_in_ms = 50

# TODO CHECK THAT THESE ARE PRESERVED
total_duration_extended = 700
theta_band = [5, 12]


settings_name = '5_12_1sec_only_peak'

exclude_quantile = 0.10


# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'lfp_events_with_osc_{}_{}'.format(settings_name, exclude_quantile))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD ---------------------------------------------------------------------
seldf = pd.read_csv(
    os.path.join(REPLAY_FOLDER, 'results', 'theta_power_{}.csv'.format(settings_name)),
    index_col='indx')

seldf['sig_theta'] = False
seldf.loc[seldf['theta_power'] >= seldf['theta_power'].quantile(1-exclude_quantile), 'sig_theta'] = True

# all_dxx = []
# for sess in sessions:
#
#     dxx = seldf[seldf['sess_ind'] == sess]
#     quant = dxx['theta_power'].quantile(1-exclude_quantile)
#     dxx.loc[dxx['theta_power'] >= quant, 'sig_theta'] = True
#     dxx['thresh'] = quant
#     all_dxx.append(dxx)
#
# seldf = pd.concat(all_dxx)


# --- PLOT THETA DISTRIBUTION --------------------------------------------------

f, ax = plt.subplots(1, 1, figsize=[3, 3])
sns.kdeplot(seldf['theta_power'], label='')
ax.axvline(seldf['theta_power'].quantile(1-exclude_quantile), c='r', ls=':')
ax.set_xlabel('Theta power')
ax.set_ylabel('Density')
sns.despine()
plt.tight_layout()

plot_name = 'theta_distribution_{}.{}'.format(settings_name, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

# --- PLOT INCLUDED EVENTS WITH HIGH THETA POWER -------------------------------

# TODO sess 11 throws error
for sess in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 27, 28, 29, 30]:
    print(sess)
    for plot_excluded in [True, False]:
        rat, day, session = get_rat_day_session(sess)
        sess_path = os.path.join(lfp_path, rat, day, session)
        os.chdir(sess_path)

        time_stamps = []
        channels = []

        session_path = 'SpikeTrl/' + rat + '/' + day + '/' + session + '/spikeTrl.mat'
        S = loadmat(DATA_FOLDER + session_path)
        first_ts = S['spike']['hdr'][sess].FirstTimeStamp / 1000  # first ts in ms

        for i, file in enumerate(glob.glob("*.ncs")) :
            ncs = neuralynx_io.load_ncs(
                os.path.join(sess_path, file))  # Load signal data into a dictionary
            channels.append(ncs['data'])
            if i == 0 :
                time_stamps.append(ncs['time'])

        ts = time_stamps[0] / 1000  # ts in ms

        sessdf = seldf[(seldf['sess_ind'] == sess)]

        if plot_excluded:
            sessdf = sessdf[(sessdf['sig_theta'] == True)]
        else:
            sessdf = sessdf[(sessdf['sig_theta'] == False)]

        sessdf = sessdf.sort_values(by='theta_power', ascending=False).iloc[0:8]

        for i, row in sessdf.iterrows() :

            edge_at_the_sides_in_ms = (total_duration_extended - row['event_duration_in_ms']) / 2


            start_time = (row['start_time_in_ms'])+first_ts-edge_at_the_sides_in_ms
            end_time = (row['end_time_in_ms'])+first_ts+edge_at_the_sides_in_ms

            indx = [np.logical_and(ts > start_time, ts < end_time)][0]

            sig = channels[0][indx]
            freqs = np.arange(2, 30, 1)
            irasa = IRASA(sig=sig, freqs=freqs,
                          samplerate=ncs['sampling_rate'])

            # plt.figure(figsize=(10, 4))
            # plt.subplot(121)
            # irasa.psdplot(fit=True)
            # plt.subplot(122)
            # irasa.loglogplot(fit=True)

            osc = np.log10(irasa.mixed) - np.log10(irasa.fractal)

            freqs_osc = osc[(freqs >= theta_band[0]) & (freqs <= theta_band[1])]
            freqs_osc[freqs_osc < 0] = 0
            theta_power = freqs_osc.sum()
            sessdf.loc[i, 'theta_power'] = theta_power

            sns.set_style('ticks')

            f, ax = plt.subplots(1, 3, figsize=[8, 3])

            # --- PLOT SIGNAL ---
            for kk, ch in enumerate(channels) :
                if kk == 0 :
                    ax[0].plot(ts[indx] - start_time, ch[indx], c='k', lw=2,
                               zorder=10)
                else :
                    ax[0].plot(ts[indx] - start_time, ch[indx], lw=1, zorder=-10)

            ax[0].axvspan((start_time + edge_at_the_sides_in_ms - start_time),
                          (end_time - edge_at_the_sides_in_ms - start_time),
                          color='grey', alpha=0.5,
                          zorder=-10)

            # ax[0].axvline((start_time + edge_at_the_sides_in_ms - start_time),
            #               ls='-')
            # ax[0].axvline((end_time - edge_at_the_sides_in_ms - start_time), ls='-')

            sns.despine()
            ax[0].set_xlabel('Time [ms]')
            ax[0].set_title('All LFP channels'.format(sess))
            ax[1].set_title('Selected LFP channel'.format(sess))
            ax[2].set_title('Theta power = {:.2f}'.format(row['theta_power']))

            # --- PLOT DETRENDED SIGNAL ---
            ax[1].plot(ts[indx] - start_time, irasa.sig_filtered, c='k', lw=2, zorder=10)

            # irasa.plot_oscillatory()
            ax[2].plot(irasa.freqs, osc)
            ax[2].axhline(0, c='k')
            ax[2].set_xlabel('Frequency')
            ax[2].set_ylabel('Residual power')
            ax[2].set_xticks([0, 5, 10, 15, 20, 25, 30])
            ax[2].axvspan(theta_band[0], theta_band[1], color=sns.xkcd_rgb['light red'])

            plt.tight_layout()

            if plot_excluded:
                plot_name = 'EXCLUDED_event_{}_sess_{}.{}'.format(i, sess, plot_format)
            else:
                plot_name = 'INCLUDED_event_{}_sess_{}.{}'.format(i, sess, plot_format)
            f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

            plt.close()