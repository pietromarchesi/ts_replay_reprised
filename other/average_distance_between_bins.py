from constants import *
from utils import prepare_bins, load_bin_centers
from loadmat import loadmat
import numpy as np

sessions = range(20)
for sess_ind in sessions:
    F = loadmat(DATA_FOLDER + '/F/F_units/F_All.mat')['F']
    rat, day, session = F[0].dir.split('/')[-3:]

    bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, session)

    bin_centers, bin_labels = prepare_bins(bin_centers_combined)

    bcr = bin_centers['right']

    mean_dist = np.linalg.norm(bcr[:-1] - bcr[1:], axis=1).mean()
    print(mean_dist)