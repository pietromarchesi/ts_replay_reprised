
import neo
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle

file = '/media/pietro/bigdata/neuraldata/touch_see/neo/Taskmaster/2012-01-11_14-21-37/TS_session_7_Taskmaster_2012-01-11.pkl'


bl = pickle.load(open(file, 'rb'))


import matplotlib as mpl
mpl.rcParams['figure.dpi']= 200

maze_file = '/media/pietro/bigdata/neuraldata/touch_see/maze_outline.pkl'

mz = pickle.load(open(maze_file, 'rb'))

def plot_maze(ax):
    ax.plot(mz['xaxMInL'], mz['yaxMInL'], c='k')
    ax.plot(mz['xaxMInR'], mz['yaxMInR'], c='k')
    ax.plot(mz['xaxMOut'], mz['yaxMOut'], c='k')

vel_threshold = 0.2
selected_units = [449, 452, 457, 458, 463, 472]
spike_pos = {un : np.empty([0, 2]) for un in selected_units}
pos = np.empty([0, 2])

for unit_ind in selected_units:

    for segment in bl.segments:
        train = segment.filter(targdict={'general_unit_ind': unit_ind},
                               objects=neo.SpikeTrain)[0]
        trial_pos  = segment.analogsignals[0].__array__()
        trial_vel  = segment.analogsignals[1].__array__()
        time = segment.analogsignals[0].times

        for spike in train:
            idx_spike = np.abs(time - spike).argmin()
            # Filter using velocity
            if trial_vel[idx_spike] > vel_threshold:
                spike_pos[unit_ind] = np.append(spike_pos[unit_ind],
                                                trial_pos[idx_spike, None], axis=0)
        pos = np.append(pos, trial_pos, axis=0)


pal = sns.color_palette('husl', len(selected_units))
f, ax = plt.subplots(1, 1, figsize=[3,3])
plot_maze(ax)

#ax.plot(pos[:, 0], pos[:, 1], c='gray', zorder=1, alpha=0.4)
for i, unit_ind in enumerate(selected_units):
    if not np.isin(unit_ind, [452, 458]):
        unit = bl.filter(targdict={'general_unit_ind': unit_ind}, objects=neo.Unit)[0]
        area = unit.annotations['area']
        if unit_ind == 463:
            zorder = 20
        else:
            zorder = i
        ax.scatter(spike_pos[unit_ind][:, 0], spike_pos[unit_ind][:, 1], s=5, zorder=zorder,
                   label='Unit {} - {}'.format(unit_ind, area), facecolor=pal[i], alpha=0.5,
                   lw=0)
#ax.legend()
ax.set_axis_off()
plt.show()

f.savefig('/home/pietro/PyConSpikes.png', dpi=500)

for c in pal:
    print([col*255 for col in c])