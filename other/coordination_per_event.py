import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from utils import *
from session_selection import sessions_HC_replay
import quantities as pq
import matplotlib.patches as mpatches
from scipy.ndimage import gaussian_filter1d
import itertools


"""
#TODO

"""


data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

group_iti_and_return = True

# PLOTTING PARAMETERS

plot_settings = 'only_two_shufs'
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms    = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'hippocampal_replay',
                             'pbe_setting_{}_{}_{}_dec_setting_{}_{}_plot_setting_{}'.format(PBEs_settings_name, PBEs_area,
                              data_version, dec_settings_name, dec_area, plot_settings))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------


df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return)


sess_ind = 10
F = loadmat(DATA_FOLDER + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]
bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, session)
bin_centers, bin_labels = prepare_bins(bin_centers_combined)



# --- LOAD REPLAY RATEMAPS -----------------------------------------------------




ratemap_settings = 'may11_DecSet2_smooth2_morelages'
#ratemap_settings = 'jul4_DecSet2_smooth2_morelages'
#ratemap_settings = 'feb10_DecSet26_taskphase'
ratemap_areas = ['Perirhinal', 'Barrel']


test_values = [50, 0] # [0, 0]
nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
subselect_sessions = None
subselect_shifts = False
min_shift = -8
max_shift = 8
bonferroni_correct = False
variable = 'corrcoef_obs'
p_val_thr = 0.05
null_dist_method_for_plot_both_shuff = 'rotate_spikes'



# --- SET UP PATHS ------------------------------------------------------------
plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# --- LOAD RESULTS ------------------------------------------------------------

dfs = []
for ratemap_area in ratemap_areas:
    res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)
    file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
    res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
    df = res['df']
    pars = res['pars']
    shift_amounts = pars['shift_amounts']
    smoothing_sigma = pars['smooth_sigma']


    if subselect_sessions is not None:
        df = df[np.isin(df['sess_ind'], subselect_sessions)]

    if subselect_shifts:
        df = df[df['shift_amount'] >= min_shift]
        df = df[df['shift_amount'] <= max_shift]
        shift_amounts = np.arange(min_shift, max_shift+1)

    df['area'] = ratemap_area
    dfs.append(df)

df = pd.concat(dfs)
df['unit_id'] = ['{}_{}_{}'.format(r['sess_ind'], r['unit'], r['area']) for i, r in df.iterrows()]
    #shift_amounts = np.array([-8, -6, -4, -2, 0, 2, 4, 6, 8])

sig_ids = []
n_total = []
for null_distribution_method in nulldistmet_plots :
    dfx = df[(df['phase'] == 'all') &
             (df['direction'] == 'all') &
             (df['has_ripple'] == 'all') &
             (df['null_method'] == null_distribution_method) &
             (df['shift_amount'] == 0)]

    if bonferroni_correct:
        p = p_val_thr / dfx.shape[0]
    else:
        p = p_val_thr

    #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
    unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
    sig_ids.append(unit_ids)
    n_total.append(dfx.shape[0])

sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

dfx = df[(df['phase'] == 'all') &
         (df['direction'] == 'all') &
         (df['has_ripple'] == 'all') &
         (df['null_method'] == null_dist_method_for_plot_both_shuff) &
         (df['shift_amount'] == 0)]

dfx = dfx[np.isin(dfx['unit_id'], sig_both)]

# ---

dr['coordinated_Perirhinal'] = None
dr['coordinated_Barrel'] = None

for sess_ind in sessions:

    dr_sess = dr[dr['sess_ind'] == sess_ind]
    bl = get_session_block(DATA_FOLDER, sess_ind, 'task', data_version=data_version)
    segment = merge_segments(bl.segments[:-1], bl.list_units, chop_return_central_arm=False)

    for area in ratemap_areas:

        trains = segment.filter(targdict={'area': area}, objects=neo.SpikeTrain)
        units = dfx[(dfx['sess_ind'] == sess_ind) & (dfx['area'] == area)]['unit']

        trains = [trains[k] for k in units]

        for i, row in dr_sess.iterrows():
            t_start = (row['start_time_in_ms'] * pq.ms).rescale(pq.s)
            t_end = (row['end_time_in_ms'] * pq.ms).rescale(pq.s)

            n_spikes = 0
            for train in trains:
                n_spikes += train.time_slice(t_start, t_end).times.__len__()

            #print(n_spikes)
            if n_spikes > 0:
                dr.loc[i, 'coordinated_{}'.format(area)] = 'coordinated'
            else:
                dr.loc[i, 'coordinated_{}'.format(area)] = 'uncoordinated'



dr['coordinated_Perirhinal'].value_counts()
dr['coordinated_Barrel'].value_counts()


