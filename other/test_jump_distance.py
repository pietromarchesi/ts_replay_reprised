import numpy as np



X = np.array([[0, 1, 1, 1, 0, 0],
              [0, 0, 0, 0, 0, 1],
              [0, 0, 0, 0, 1, 0],
              [1, 0, 0, 0, 0, 0]]).T


def _jump_distance(sefl, X):
    """
    Given a numpy array X of posterior probabiities, of shape n_times x n_bins,
    this function calculates the distance between peak probabilities at
    subsequent bins, considering that the bins wrap around, so the distance
    between two rows [1, 0, 0, 0] and [0, 0, 0, 1] is 1, not 3.
    """

    peak_prob_ind = X.argmax(axis=1)
    jump_dist = np.abs(np.diff(peak_prob_ind))

    n_bins = X.shape[1]
    max_dist = int(n_bins / 2)
    for j in range(jump_dist.shape[0]):
        if jump_dist[j] > max_dist:
            jump_dist[j] = n_bins - jump_dist[j]

    return jump_dist


