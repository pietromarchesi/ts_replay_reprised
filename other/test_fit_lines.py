import numpy as np
import matplotlib.pyplot as plt
from replaydetector.replay_utils import fit_lines
import itertools
import pandas as pd


plot_all_fits = False

n_times = 10
n_bins = 30
y_pred_proba = np.random.randint(0, 4, size=[n_times, n_bins])

y_pred_proba[2, 0] = 10
y_pred_proba[3, 4] = 10
y_pred_proba[4, 6] = 10


bins_ext = 10
n_bins_proba = 1
subsample = 1


bins = np.arange(y_pred_proba.shape[1]).astype(int)

bins = np.arange(-y_pred_proba.shape[1]+1, 1).astype(int)


time_bins = np.array(range(y_pred_proba.shape[0]))

bins_loc = np.arange(bins[0] - 0.5, bins[-1] + 1.5)
bins_time = np.arange(-0.5, time_bins.shape[0] + 0.5)


# pad the probability so that it wraps around
# we pad y_pred_proba in the direction of axis 1 (the bins)
padded_proba = np.pad(y_pred_proba,
                      pad_width=((0, 0), (bins_ext, bins_ext)),
                      mode='wrap').T


# we transpose it so that padded_proba is of shape
# (n_bins + 2 * extented_bins x n_times)

# create a dataframe for the results
line_fits = pd.DataFrame(columns=['start_bin', 'end_bin', 'replay_score'])

# allow trajectories to start outside the range (equivalent to having
# trajectories that start later or end earlier)
bins_extended = np.arange(bins[0] - bins_ext, bins[-1] + bins_ext + 1, 1).astype(int)

bin_combinations = list(itertools.product(bins_extended[::subsample],
                                          bins_extended[::subsample]))

#bin_combinations = [(0, 0), (-5, 3), (0, -5)]

for start_location, end_location in bin_combinations:

    # define line by location at first and last time bins and interpolate
    x = time_bins
    coefficients = np.polyfit([time_bins[0], time_bins[-1]],
                              [start_location, end_location], 1)
    line = np.poly1d(coefficients)
    y = line(x)

    # find in which bins the line falls at every time bin
    histogram_bins = np.hstack((bins_extended - 0.5, bins_extended[-1]+0.5))
    H, xedges, yedges = np.histogram2d(x, y, bins=[bins_time, histogram_bins])

    mask = H.T.copy()

    # set entries to 1 for the probability bins around the line that
    # should be counted
    for i in range(mask.shape[1]):
        j = mask[:, i].argmax()
        mask[j + 1:j + n_bins_proba + 1, i] = 1
        mask[j - n_bins_proba:j, i] = 1

    # compute probability around the line
    replay_score = 100 * (padded_proba * mask).sum() / len(time_bins)

    line_fits.loc[line_fits.shape[0], :] = [start_location, end_location,
                                            replay_score]

    if plot_all_fits:
        f, ax = plt.subplots(1, 4)
        extent = [-0.5, time_bins.shape[0] - 0.5,
                  bins[0] - 0.5, bins[-1] + 0.5]
        extent_ext = [-0.5, time_bins.shape[0] - 0.5,
                      bins_extended[0] - 0.5, bins_extended[-1] + 0.5]

        ax[0].imshow(np.flipud(y_pred_proba.T), extent=extent)
        ax[1].imshow(np.flipud(padded_proba), extent=extent_ext)
        ax[2].imshow(np.flipud(H.T), extent=extent_ext)
        ax[3].imshow(np.flipud(mask), extent=extent_ext)

        plt.tight_layout()


line_fits['replay_score'] = pd.to_numeric(line_fits['replay_score'])

import quantities as pq
bin_dist = 8
traj_time = 0.2 * pq.s

line_fits['n_bins_traversed'] = None
line_fits['trajectory_speed'] = None


for i, row in line_fits.iterrows():
    start_location, end_location = row['start_bin'], row['end_bin']

    n_bins_traversed = np.abs(end_location - start_location)
    trajectory_speed = bin_dist * n_bins_traversed * pq.cm / traj_time
    line_fits.loc[i, ['n_bins_traversed', 'trajectory_speed']] = [n_bins_traversed, trajectory_speed]





ind_max = line_fits['replay_score'].idxmax()
#bool_ind_max = line_fits.index.isin([ind_max])
#mean_score = line_fits[~bool_ind_max]['replay_score'].mean()
#mean_score = line_fits['replay_score'].mean()
start_location = line_fits.loc[ind_max, 'start_bin']
end_location = line_fits.loc[ind_max, 'end_bin']
score = line_fits.loc[ind_max, 'replay_score']


# --- PLOT BEST FIT -----------------------------------------------------------

print('Best fit: start {} end {}'.format(start_location, end_location))

# define line by location at first and last time bins and interpolate
x = time_bins
coefficients = np.polyfit([time_bins[0], time_bins[-1]],
                          [start_location, end_location], 1)
line = np.poly1d(coefficients)
y = line(x)

# find in which bins the line falls at every time bin
histogram_bins = np.hstack((bins_extended - 0.5, bins_extended[-1] + 0.5))
H, xedges, yedges = np.histogram2d(x, y, bins=[bins_time, histogram_bins])

mask = H.T.copy()

# set entries to 1 for the probability bins around the line that
# should be counted
for i in range(mask.shape[1]):
    j = mask[:, i].argmax()
    mask[j + 1:j + n_bins_proba + 1, i] = 1
    mask[j - n_bins_proba:j, i] = 1

f, ax = plt.subplots(1, 4, figsize=[8, 8])
extent = [-0.5, time_bins.shape[0] - 0.5,
          bins[0] - 0.5, bins[-1] + 0.5]
extent_ext = [-0.5, time_bins.shape[0] - 0.5,
              bins_extended[0] - 0.5, bins_extended[-1] + 0.5]

ax[0].imshow(np.flipud(y_pred_proba.T), extent=extent)
ax[1].imshow(np.flipud(padded_proba), extent=extent_ext)
ax[1].plot(time_bins, y, c='r', ls=':', lw=3)
ax[2].imshow(np.flipud(H.T), extent=extent_ext)
ax[3].imshow(np.flipud(mask), extent=extent_ext)

for i in [1, 2, 3]:
    ax[i].axhline(bins[0] - 0.5, c='r')
    ax[i].axhline(bins[-1] + 0.5, c='r')

for axx in ax:
    axx.set_xlabel('Time bins')

ax[0].set_title('Posterior\nprobabilities')
ax[1].set_title('Padded posterior\nprobabilities')
ax[2].set_title('Histogram')
ax[3].set_title('Mask used to compute\nline score')
plt.tight_layout()
