import matplotlib.pyplot as plt
from constants import *
from utils import *
import quantities as pq
from plotting_style import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from replaydetector.replayratemaps import ReplayRatemaps
import copy
import statsmodels.stats.descriptivestats
from utils import *
from session_selection import ratemap_sessions

data_version              = 'dec16'
PBEs_settings_name        = 'dec19k20'
dec_settings_name         = 'DecSet2'
ratemap_settings          = 'newspeedFIXED_THETA_MANUAL_THETAFALSE'#'newspeedFIXED_12_oct19'
ratemap_areas             = ['Barrel', 'Perirhinal']
PBEs_area                 = 'Hippocampus'
dec_area                  = 'Hippocampus'

# filter pbes parameters
shuffle_types             = ['column_cycle_shuffle', 'place_field_rotation_shuffle',
                             'unit_identity_shuffle']
shuffle_p_vals            = [0.05, 0.05, 1]
speed_thr_real_rm         = 3
time_bin_size_task_in_ms  = 200
min_pbe_duration_in_ms    = 50
min_pbe_events_per_group  = 10

smooth_ratemaps           = True
smooth_sigma              = 2
correlation_method        = 'pearson'
null_distribution_methods = ['rotate_ratemap', 'rotate_spikes']#['rotate_ratemap', 'rotate_spikes']
n_surrogates              = 100
shift_amounts             = np.arange(-10, 11)

only_remote_events        = False
only_local_events         = False
group_iti_and_return      = True

new_speed                 = True
speed_threshold_pbes      = 8
time_before_in_ms         = 0
time_after_in_ms          = 0
position_sigma            = 10
binsize_interp_pos        = 50

theta_power               = False

phase_direction_ripples   = [('all', 'all', 'all'),
                             ('all', 'positive', 'all'),
                             ('all', 'negative', 'all')]

# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

output_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(output_folder):
    os.makedirs(output_folder, exist_ok=True)



# --- RUN ---------------------------------------------------------------------

if theta_power :
    # theta = pd.read_csv(
    #     os.path.join(REPLAY_FOLDER, 'results', 'theta_power_48.csv'),
    #     index_col='indx')
    #
    # theta['sig_theta'] = False
    # theta.loc[theta['theta_power'] >= theta['theta_power'].quantile(0.9), 'sig_theta'] = True

    from constants import excluded_ids


for ratemap_area in ratemap_areas:

    dfs = []
    #df_n_events = pd.DataFrame(columns=['all', 'pos', 'neg'])

    sessions = ratemap_sessions[ratemap_area]

    # TODO da rimuovere
    sessions = sessions[np.isin(sessions, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 27, 28, 29, 30])]

    for sess_ind in sessions:
        print('\n {} \n'.format(sess_ind))
        #pass
        # --- LOAD TASK DATA ------------------------------------------------------
        # Needed to build the ratemaps of the area during normal locomotion
        session_data = unpack_session(sess_ind, data_folder=DATA_FOLDER,
                                      area=ratemap_area,
                                      binsize_in_ms=time_bin_size_task_in_ms,
                                      data_version=data_version)

        speed_real = session_data['speed'].flatten()
        binned_spikes_task = session_data['spikes']
        binned_position_task = session_data['bin']

        binned_spikes_task = binned_spikes_task[speed_real >= speed_thr_real_rm, :]
        binned_position_task = binned_position_task[speed_real >= speed_thr_real_rm]

        trains = session_data['trains']
        n_trains = len(trains)
        # --- COMUPTE UNIT PROPERTIES ---------------------------------------------

        rsi = get_RSI(sess_ind, ratemap_area)

        # --- LOAD PBEs -----------------------------------------------------------


        pbes_data = load_decoded_PBEs(sess_ind=sess_ind,
                                       PBEs_settings_name=PBEs_settings_name,
                                       PBEs_area=PBEs_area,
                                       data_version=data_version,
                                       dec_settings_name=dec_settings_name,
                                       dec_area=dec_area,
                                       load_light=True)

        pbes_df_all = pbes_data['pbes']

        if new_speed:
            pbes_df_all = add_new_speed(pbes_df_all, time_before_in_ms=time_before_in_ms,
                                        time_after_in_ms=time_after_in_ms,
                                        binsize_interp_pos=binsize_interp_pos,
                                        position_sigma=position_sigma,
                                        data_version=data_version,
                                        column_name='average_speed')

        if theta_power:
            # pbes_df_all = pd.merge(pbes_df_all, theta[['theta_power', 'sig_theta']], left_index=True,
            #          right_index=True, how='inner')

            # pbes_df_all = pbes_df_all[pbes_df_all['sig_theta'] == False]

            excluded_mask = pbes_df_all.index.isin(excluded_ids)

            pbes_df_all = pbes_df_all[~excluded_mask]