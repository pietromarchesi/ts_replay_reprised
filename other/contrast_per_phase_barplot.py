import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from session_selection import sessions_HC_replay
import quantities as pq
import matplotlib.patches as mpatches
from scipy.ndimage import gaussian_filter1d
import itertools
from utils import *
from numpy import median
from statsmodels.stats.proportion import proportions_ztest



data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

group_iti_and_return = True

contrasts                 = ['replay_direction',
                             'radial_direction',
                             'ahead_behind',
                             'local_remote',
                             'current_trial_outcome',
                             'previous_trial_outcome',
                             'coordination_perirhinal',
                             'coordination_barrel',
                             'coordination_both']


contrasts                 = ['match_previous_trial_side',
                             'match_current_trial_side']


#contrasts = ['local_remote']
# PLOTTING PARAMETERS
ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'jul3_DecSet2_smooth2_morelages'

restrict_to_direction = 'negative'

plot_settings = 'only_two_shufs'
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms    = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'hippocampal_replay',
                             'pbe_setting_{}_{}_{}_dec_setting_{}_{}_plot_setting_{}'.format(PBEs_settings_name, PBEs_area,
                              data_version, dec_settings_name, dec_area, plot_settings))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------


df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return)




for contrast in contrasts:
    # only ones that makes sense to look at here
    assert contrast in ['radial_direction', 'ahead_behind', 'local_remote',
                        'replay_direction', 'current_trial_outcome',
                        'previous_trial_outcome', 'coordination_perirhinal',
                        'coordination_barrel', 'coordination_both',
                        'match_current_trial_side', 'match_previous_trial_side']

    if contrast == 'radial_direction' :
        df_mod = add_radial_direction(df.copy())

    if contrast == 'ahead_behind' :
        df_mod = add_ahead_behind(df.copy())

    if contrast == 'local_remote' :
        df_mod = add_local_remote(df.copy())

    if contrast == 'current_trial_outcome' or contrast == 'previous_trial_outcome':
        df_mod = add_correct_incorrect(df.copy(), data_version)

        df_mod = df_mod[~df_mod[contrast].isna()]

    if contrast == 'replay_direction':
        df_mod = df.copy()

    if contrast == 'coordination_perirhinal':
        df_mod = add_coordination_per_event(df.copy(), ratemap_area='Perirhinal',
                                        ratemap_settings=ratemap_settings,
                                        data_version=data_version)

    if contrast == 'coordination_barrel':
        df_mod = add_coordination_per_event(df.copy(), ratemap_area='Barrel',
                                        ratemap_settings=ratemap_settings,
                                        data_version=data_version)
    if contrast == 'coordination_both':
        df_mod = add_coordination_both_areas(df.copy(),
                                        ratemap_settings=ratemap_settings,
                                        data_version=data_version)

    if contrast == 'match_previous_trial_side' :
        df_mod = match_previous_trial_side(df.copy())

    if contrast == 'match_current_trial_side' :
        df_mod = match_current_trial_side(df.copy())

    name1, name2 = np.sort(df_mod[contrast].unique())


    if group_iti_and_return :
        phases = ['iti', 'img', 'run', 'reward']
    else :
        phases = ['img', 'iti', 'return', 'reward', 'run']


    palette = {name1 : sns.xkcd_rgb['forest green'],
               name2 : sns.xkcd_rgb['ultramarine blue']}


    if restrict_to_direction is not None:
        df_mod = df_mod[df_mod['replay_direction'] == restrict_to_direction]


    # # CHECK LOCAL REMOTE
    #
    # for i, row in df_mod[df_mod['phase'] == 'run'].iterrows():
    #     print(row['maze_arm_occ'], row['pbe_loc_bin'], row['dec_end_bin'], row['local_remote'])



    # --- PLOT V1 - PER SESSION ----------------------------------------------------

    df_sel = df_mod[df_mod['epoch'] == 'task']


    dg = pd.DataFrame(columns=['sess_ind', 'phase', 'perc_cont1', 'ntot'])
    for phase in ['all'] + phases:
        for sess_ind in sessions:
            if phase == 'all':
                dk = df_sel[(df_sel['sess_ind'] == sess_ind) & (df_sel['epoch'] == 'task')]
            else:
                dk = df_sel[(df_sel['sess_ind'] == sess_ind) & (df_sel['epoch'] == 'task')
                              & (df_sel['phase'] == phase)]
            if dk.shape[0] > 5:
                npos = dk[dk[contrast] == name1].shape[0]
                nneg = dk[dk[contrast] == name2].shape[0]
                perc = (100 * npos / (npos+nneg))
                ntot = npos+nneg
                dg.loc[dg.shape[0], :] = [sess_ind, phase, perc, ntot]

    dg['perc_cont1'] = pd.to_numeric(dg['perc_cont1'])

    for phase in ['all'] + phases:
        try:
            m = dg.groupby('phase')['perc_cont1'].quantile(q=0.5).loc[phase]
            q1 = dg.groupby('phase')['perc_cont1'].quantile(q=0.25).loc[phase]
            q2 = dg.groupby('phase')['perc_cont1'].quantile(q=0.75).loc[phase]
            print('% {} events {}: {:.1f}, {:.1f} - {:.1f}'.format(name1, phase, m, q1, q2))
        except:
            pass

    x = dg[dg['phase'] == 'all']['perc_cont1']
    stat, wilp = wilcoxon(x-50)
    print('% events {} {:.2f}, {:.2f}-{:.2f}'.format(name1, x.quantile(0.5),
                                         x.quantile(0.25),
                                         x.quantile(0.75)))

    print('Perc {} larger than 50: p={:.2e}'.format(name1, wilp))


    allphases = ['all'] + phases
    combos = list(itertools.combinations(allphases, 2))

    for ph1, ph2 in combos:
        x = dg[dg['phase'] == ph1]['perc_cont1']
        y = dg[dg['phase'] == ph2]['perc_cont1']
        if x.shape[0] > 0 and y.shape[0] > 0:
            stat, pval = mannwhitneyu(x, y)
            is_sig = pval < (0.05 / len(combos))
            adjusted_p = pval * len(combos)
            print('{} vs {} mannwhitney p={:.3f} - {}'.format(ph1, ph2, adjusted_p, is_sig))



    f, ax = plt.subplots(1, 1, figsize=slightly_vertical_panel)


    sns.barplot(x=['all'] + phases, y=[100] * len(['all']+phases),
                edgecolor=palette[name2],
                facecolor='white', linewidth=3.5)

    g = sns.barplot(data=dg, x='phase', y='perc_cont1', estimator=median,
                    edgecolor=palette[name1],
                    facecolor='white', linewidth=3.5, errcolor='0.2')

    sns.despine()
    ax.set_ylabel('% of replay events')
    ax.set_xticklabels([task_phase_labels_nobreak[l._text] for l in ax.get_xticklabels()],
                       rotation=45)
    ax.set_xlabel('')
    ax.axhline(50, c=maze_color, ls='--', lw=2)
    plt.tight_layout()

    plot_name = 'perc_{}_events_per_task_phase.{}'.format(contrast, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
    #plt.close(fig=f)




    # --- PLOT V1 - Z TEST ---------------------------------------------------------

    df_sel = df_mod[df_mod['epoch'] == 'task']


    dg = pd.DataFrame(columns=['phase', 'perc_centr', 'ncentrif', 'ntot'])
    for phase in ['all'] + phases:
        if phase == 'all':
            dk = df_sel[(df_sel['epoch'] == 'task')]
        else:
            dk = df_sel[(df_sel['epoch'] == 'task') & (df_sel['phase'] == phase)]
        if dk.shape[0] > 5:
            npos = dk[dk[contrast] == name1].shape[0]
            nneg = dk[dk[contrast] == name2].shape[0]
            perc = (100 * npos / (npos+nneg))
            ntot = npos+nneg
            dg.loc[dg.shape[0], :] = [phase, perc, npos, ntot]

    dg['perc_centr'] = pd.to_numeric(dg['perc_centr'])
    dg['ncentrif'] = pd.to_numeric(dg['ncentrif'])
    dg['ntot'] = pd.to_numeric(dg['ntot'])


    allphases = ['all'] + phases
    combos = list(itertools.combinations(phases, 2))
    n_tests = len(allphases) + len(combos)

    # TEST INDIVIDUAL PHASE DIFFERENT FROM 50%
    for ph1 in ['all'] + phases:
        if ph1 == 'all':
            ncentrif = dg['ncentrif'].values[0]
            nsamp = dg['ntot'].values[0]
        else:
            ncentrif = dg[dg['phase'] == ph1]['ncentrif'].values[0]
            nsamp = dg[dg['phase'] == ph1]['ntot'].values[0]

        ncentrif2 = int(nsamp / 2)

        z, pval = proportions_ztest(count=[ncentrif, ncentrif2], nobs=[nsamp, nsamp])
        is_sig = pval < (0.05 / n_tests)
        adjusted_p = pval
        print('{} different from 50%, {} of {}, z-test p={:.3f} - {}'.format(ph1, ncentrif, nsamp, adjusted_p, is_sig))

    # TEST DIFFERENCE BETWEEN PHASES
    for ph1, ph2 in combos:
        ncentrif1 = dg[dg['phase'] == ph1]['ncentrif'].values[0]
        nsamp1 = dg[dg['phase'] == ph1]['ntot'].values[0]

        ncentrif2 = dg[dg['phase'] == ph2]['ncentrif'].values[0]
        nsamp2 = dg[dg['phase'] == ph2]['ntot'].values[0]

        z, pval = proportions_ztest(count=[ncentrif1, ncentrif2], nobs=[nsamp1, nsamp2])
        is_sig = pval < (0.05 / n_tests)
        adjusted_p = pval
        print('{} vs {} z-test p={:.3f} - {}'.format(ph1, ph2, adjusted_p, is_sig))


    f, ax = plt.subplots(1, 1, figsize=slightly_vertical_panel)

    sns.barplot(x=['all'] + phases, y=[100] * len(['all']+phases),
                edgecolor=palette[name2],
                facecolor='white', linewidth=3.5)

    g = sns.barplot(data=dg, x='phase', y='perc_centr', estimator=median,
                    edgecolor=palette[name1],
                    facecolor='white', linewidth=3.5, errcolor='0.2')


    sns.despine()
    ax.set_ylabel('% of replay events')
    ax.set_xticklabels([task_phase_labels_nobreak[l._text] for l in ax.get_xticklabels()],
                       rotation=45)
    ax.set_xlabel('')
    plt.tight_layout()

    plot_name = 'perc_{}_events_per_task_phase_v2.{}'.format(contrast, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
    plt.close(fig=f)



    pos_patch = mpatches.Patch(edgecolor=palette[name1],
                           facecolor='white', linewidth=2.5,
                           label=name1)
    neg_patch = mpatches.Patch(edgecolor=palette[name2],
                           facecolor='white', linewidth=2.5,
                           label=name2)
    f, ax = plt.subplots(1, 1, figsize=[3, 1])
    ax.legend(handles=[pos_patch, neg_patch], frameon=False)
    ax.axis('off')
    plt.tight_layout()

    plot_name = 'replay_{}_legend_4.{}'.format(contrast, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


