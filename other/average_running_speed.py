import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(sys.path[0]))))
import numpy as np
import quantities as pq
from constants import DATA_FOLDER, session_indices
from utils import get_session_block

av_speeds_all_sess = []

for sess_ind in session_indices:

    bl = get_session_block(DATA_FOLDER, sess_ind, 'task')

    rat = bl.annotations['animal_ID']
    day_folder = bl.annotations['day_folder']
    session_folder = bl.annotations['session_folder']



    av_speeds = []
    for segment in bl.segments[:-1]:

        ind1 = np.where(segment.events[0].labels == 'Block removed')[0][0]
        ind2 = np.where(segment.events[0].labels == 'Reward')[0][0]

        t1 = segment.events[0].times[ind1]
        t2 = segment.events[0].times[ind2]

        speed_times = segment.analogsignals[1].times
        mask = np.logical_and(speed_times >= t1, speed_times <= t2)
        speed_trial = segment.analogsignals[1].rescale(pq.cm/pq.s).__array__()[mask].flatten()
        av_speeds.append(np.nanmean(speed_trial))

    average_speed = np.nanmean(av_speeds)

    print('Session {}, Average speed: {:.4} cm/s'.format(sess_ind, average_speed))

    av_speeds_all_sess.append(average_speed)

global_av_speed = np.nanmean(av_speeds_all_sess)
print('Global average: {:.4}'.format(global_av_speed))










