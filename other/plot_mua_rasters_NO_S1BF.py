import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(sys.path[0]))))
import numpy as np
import quantities as pq
from utils import merge_segments
from constants import DATA_FOLDER, REPLAY_FOLDER
from utils import get_session_block
from utils import load_bin_centers, prepare_bins, load_PBEs
import matplotlib.pyplot as plt
import seaborn as sns
import neo
import elephant
import scipy
from replaydetector.pbesdetector import PBEsDetector
from utils import bin_position_linearized_LR
from plotting_style import *
from constants import *


data_version = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area = 'Hippocampus'

sess_ind = 10

save_plots = True
plot_format = 'svg'
dpi = 400


print('\n- Loading PBEsmooth_sigmas')
pbes = load_PBEs(sess_ind, PBEs_settings_name, PBEs_area, data_version=data_version)
#pbes['pbes'] = pbes['pbes'][0:15]
PBEs_indices = pbes['pbes'].index
PBEs_times = list(zip(pbes['pbes']['start_time_in_ms'] * pq.ms, pbes['pbes']['end_time_in_ms'] * pq.ms))

PBEs_times = list(zip([i*pq.ms for i in pbes['pbes']['start_time_in_ms']],
                      [i*pq.ms for i in pbes['pbes']['end_time_in_ms']]))

#[(t[1] - t[0]).rescale(pq.s) for t in PBEs_times]

# --- SET UP PATHS ------------------------------------------------------------
plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'MUA_rasters')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)


# --- LOAD BLOCK --------------------------------------------------------------
bl = get_session_block(DATA_FOLDER, sess_ind, 'task', data_version=data_version)
#segment = merge_segments(bl.segments[:-1], bl.list_units)
#rat_speed_signal = segment.irregularlysampledsignals[1].rescale(pq.cm / pq.s)

# rat = bl.annotations['animal_ID']
# day_folder = bl.annotations['day_folder']
# session_folder = bl.annotations['session_folder']
# bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day_folder,
#                                         session_folder)
# bin_centers, bin_labels = prepare_bins(bin_centers_combined)

# ------------------------------------------------------------------------------

print('-> Running PBE detection')
segment = merge_segments(bl.segments[:-1], bl.list_units,
                         chop_return_central_arm=False)

trains = segment.filter(targdict={'area' : 'Hippocampus'}, objects=neo.SpikeTrain)


mua_kernel_sigma_in_ms                    = 20
mua_kernel_size_in_ms                     = mua_kernel_sigma_in_ms * 6
mua_only_selected_cells                   = True
n_sd_above_the_mean                       = 3 # peaks defined as n standard deviations above the mean
adjust_bounds_to_first_last_spikes        = True
adjust_bounds_only_selected_cells         = True
min_average_speed                         = None #0.3
max_average_speed                         = 12
min_duration_in_ms                        = 40
max_duration_in_ms                        = 2000
min_n_spikes_only_selected_cells          = True
min_n_spikes                              = None
min_percentage_active_only_selected_cells = True # considering all neurons
min_percentage_active                     = 10
min_n_active_only_selected_cells          = True
min_n_active                              = 4
detector = PBEsDetector()
detector.set_detection_parameters(mua_kernel_sigma_in_ms=mua_kernel_sigma_in_ms,
                                  mua_kernel_size_in_ms=mua_kernel_size_in_ms,
                                  mua_only_selected_cells=mua_only_selected_cells,
                                  n_sd_above_the_mean=n_sd_above_the_mean,
                                  adjust_bounds_to_first_last_spikes=adjust_bounds_to_first_last_spikes,
                                  adjust_bounds_only_selected_cells=adjust_bounds_only_selected_cells,
                                  min_average_speed=min_average_speed,
                                  max_average_speed=max_average_speed,
                                  min_duration_in_ms=min_duration_in_ms,
                                  max_duration_in_ms=max_duration_in_ms,
                                  min_n_spikes_only_selected_cells=min_n_spikes_only_selected_cells,
                                  min_n_spikes=min_n_spikes,
                                  min_percentage_active_only_selected_cells=min_percentage_active_only_selected_cells,
                                  min_percentage_active=min_percentage_active,
                                  min_n_active_only_selected_cells=min_n_active_only_selected_cells,
                                  min_n_active=min_n_active)

is_pyramidal = [i for i in range(len(trains)) if
                trains[i].annotations['is_pyramidal']]

selected_cells = np.array(is_pyramidal)
if selected_cells.dtype == 'bool' :
    selected_cells = np.arange(len(detector.trains))[selected_cells]

detector.selected_cells = selected_cells

detector.trains = [t.rescale(pq.ms) for t in trains]

trains = detector._get_trains(only_selected_cells=False)
units = trains[0][0].units
detector.t_start = np.min([np.min(t) for t in trains if len(t) > 0]) * units

units = trains[0][0].units
detector.t_stop = np.max([np.max(t) for t in trains if len(t) > 0]) * units


bs = elephant.conversion.BinnedSpikeTrain(trains,
                                          binsize=1 * pq.ms,
                                          t_start=detector.t_start,
                                          t_stop=detector.t_stop)


# TODO horrible fix but I think BinnedSpikeTrain is fucked
binned_spikes = bs.to_array()
bin_centers = bs.bin_centers
bin_edges = bs.bin_edges
bin_edges = bin_edges.rescale(pq.ms) #* 1000

detector.mua = binned_spikes.sum(axis=0)
detector.smooth_mua = detector._smooth_1d(detector.mua,
                                  kernel_size=detector.mua_kernel_size_in_ms,
                                  kernel_sigma=detector.mua_kernel_sigma_in_ms)

if detector.selected_cells is not None :
    # mua for selected cells is computed only if an index was passed
    # to detect_candidate_events
    detector.mua_selected = binned_spikes[detector.selected_cells, :].sum(axis=0)
    detector.smooth_mua_selected = detector._smooth_1d(detector.mua_selected,
                                               kernel_size=detector.mua_kernel_size_in_ms,
                                               kernel_sigma=detector.mua_kernel_sigma_in_ms)

if detector.mua_only_selected_cells :
    mua = detector.mua_selected
    smooth_mua = detector.smooth_mua_selected
else :
    mua = detector.mua
    smooth_mua = detector.smooth_mua

mean = smooth_mua.mean()
std = smooth_mua.std()
threshold = mean + detector.n_sd_above_the_mean * std
detector.mua_threshold = threshold
detector.mua_mean = mean

# crossings_up = [i for i in range(1, smooth_mua.shape[0])
#                 if (smooth_mua[i - 1] <= mean) and (smooth_mua[i] > mean)]
#
# crossings_down = [i for i in range(1, smooth_mua.shape[0])
#                   if (smooth_mua[i - 1] > mean) and (smooth_mua[i] <= mean)]
#
# # if the first crossing is up, no problem. If it's down, remove it.
# if crossings_up[0] < crossings_down[0] :
#     pbe_bounds_index = list(zip(crossings_up, crossings_down))
# else :
#     pbe_bounds_index = list(zip(crossings_up, crossings_down[1 :]))
#
# pbe_bounds_index = [t for t in pbe_bounds_index if
#                     smooth_mua[t[0] : t[1]].max() > threshold]
#
# pbe_bounds_times = [[bin_edges[t[0]], bin_edges[t[1] + 1]] for t in
#                     pbe_bounds_index]

# ------------------------------------------------------------------------------

#for trial_number, segment in enumerate(bl.segments):
trial_number = 24
segment = bl.segments[trial_number]

ind = np.where(segment.events[0].labels == 'ITI')[0][0]
t_start = segment.events[0].times[ind]# - 1 * pq.s

ind2 = np.where(segment.events[0].labels == 'Reward')[0][0]
t_stop = segment.events[0].times[ind2]+ 2 * pq.s

pbes_trial = [t for t in PBEs_times if (t[0]>= t_start) & (t[0]<= t_stop)]

trains = segment.filter(targdict={'area' : 'Hippocampus'}, objects=neo.SpikeTrain)
trains_peri = segment.filter(targdict={'area' : 'Perirhinal'}, objects=neo.SpikeTrain)
trains_barrel = segment.filter(targdict={'area' : 'Barrel'}, objects=neo.SpikeTrain)

trains = [t.time_slice(t_start = t_start, t_stop=t_stop) for t in trains]
trains_peri = [t.time_slice(t_start = t_start, t_stop=t_stop) for t in trains_peri]
trains_barrel = [t.time_slice(t_start = t_start, t_stop=t_stop) for t in trains_barrel]

speed = segment.analogsignals[1]
speed = speed.time_slice(t_start, t_stop)

xypos = segment.analogsignals[0]
xypos = xypos.time_slice(t_start, t_stop)


from loadmat import loadmat
sess_ind = 10
F = loadmat(DATA_FOLDER + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]
bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, session)
spatial_bin_centers, spatial_bin_labels = prepare_bins(bin_centers_combined)
binned_pos, unassigned = bin_position_linearized_LR(xypos,
                                                    [segment.annotations['trial_side']]*xypos.shape[0],
                                                    spatial_bin_centers,
                                                    spatial_bin_labels, max_distance=10)

# bs = elephant.conversion.BinnedSpikeTrain(trains,
#                                           binsize=1 * pq.ms,
#                                           t_start=t_start,
#                                           t_stop=t_stop)
#
# # TODO horrible fix but I think BinnedSpikeTrain is fucked
# binned_spikes = bs.to_array()
# bin_centers = bs.bin_centers * 1000
# bin_edges = bs.bin_edges
# bin_edges = bin_edges.rescale(pq.ms) * 1000
#
# #binned_spikes, bin_centers = bin_spikes(trains, 1*pq.ms)
#
# mua = binned_spikes.sum(axis=0)
# kernel = scipy.signal.gaussian(M=mua_kernel_size_in_ms, std=mua_kernel_sigma_in_ms, sym=True)
# kernel = 1000 * kernel / kernel.sum()
# # TODO boundary effects can still be visible here
# smooth_mua = np.convolve(mua, kernel, mode='same')
bin_centers = bin_centers.rescale(pq.s)
bin_centers_plot = bin_centers[np.logical_and(bin_centers>= t_start, bin_centers<=t_stop)]
smooth_mua_plot = smooth_mua[np.logical_and(bin_centers>= t_start, bin_centers<=t_stop)]


f, ax = plt.subplots(4, 1, sharex=True, figsize=[6, 6])

for i, train in enumerate(trains):
    ax[0].scatter(train, [i]*len(train), c='k', s=4, marker='|')

for pbe in pbes_trial:
    ax[0].axvspan(pbe[0].rescale(pq.s), pbe[1].rescale(pq.s), facecolor='r', alpha=0.3)
    ax[1].axvspan(pbe[0].rescale(pq.s), pbe[1].rescale(pq.s), facecolor='r', alpha=0.3)
    ax[2].axvspan(pbe[0].rescale(pq.s), pbe[1].rescale(pq.s), facecolor='r', alpha=0.3)
    #ax[3].axvspan(pbe[0].rescale(pq.s), pbe[1].rescale(pq.s), facecolor='r', alpha=0.3)


sns.despine(bottom=True, left=True)

ym1 = np.ma.masked_where(smooth_mua_plot > detector.mua_threshold, smooth_mua_plot)
ym2 = np.ma.masked_where(smooth_mua_plot <= detector.mua_threshold, smooth_mua_plot)


ax[1].plot(bin_centers_plot, ym1, linewidth=1, c='k')
ax[1].plot(bin_centers_plot, ym2, linewidth=1, c='r')
ax[1].axhline(detector.mua_threshold, c='r', linestyle=':')
#ax[1].axhline(detector.mua_mean, c=sns.xkcd_rgb['dark grey'], linestyle=':')



for i, train in enumerate(trains_peri):
    ax[2].scatter(train, [i]*len(train), c='k', s=4, marker='|')


# for i, train in enumerate(trains_barrel):
#     ax[3].scatter(train, [i]*len(train), c='k', s=4, marker='|')


#ax[4].plot(xypos.times, xypos[:, 0], label='X position', c=sns.xkcd_rgb['dark grey'])
#ax[4].plot(xypos.times, xypos[:, 1], label='Y position', c=sns.xkcd_rgb['dark grey'], linestyle='--')
ax[3].plot(xypos.times, binned_pos, c=sns.xkcd_rgb['dark grey'])

ax[3].legend()
leg = ax[3].get_legend()
ax[3].get_legend().remove()

xtext = xypos.times[100]+ (xypos.times[100]+1*pq.s - xypos.times[100])/2

ylim = np.nanmin(binned_pos.__array__())#ax[4].get_ylim()[0]
yline = ylim - 10
ytext = ylim - 20

ax[3].plot([xypos.times[100], xypos.times[100]+1*pq.s], [yline, yline], linewidth=4,
           c='k')
ax[3].text(xtext, ytext, '1 s', horizontalalignment='center',
      verticalalignment='center')
ax[3].yaxis.set_ticks_position("right")
ax[3].set_yticks([2, 20])
ax[3].set_yticklabels(['IMG', 'REW'])
for bn in [2, 20]:
    ax[3].axhline(bn, ls=':', c='grey', linewidth=1)

for axx in ax[:-1]:
    axx.set_yticks([], [])

labels_lines = ['ITI', 'Image on', 'Block removed', 'Reward']
sel_times = [s for s, lab in zip(segment.events[0], segment.events[0].labels)
            if np.isin(lab, labels_lines)]
for axx in ax:
    axx.tick_params(axis=u'both', which=u'both', length=0)
    for t in sel_times:
        axx.axvline(t, linestyle='--', c='grey')

labels_lines_xax = ['iti', 'img', 'run', 'reward']
ax[-1].set_xticks(sel_times)
ax[-1].set_xticklabels([task_phase_labels[t] for t in labels_lines_xax],
                       rotation=35)
ax[-1].tick_params(axis=u'both', which=u'both',length=0)
ax[0].set_ylabel('CA1')
ax[1].set_ylabel('CA1 MUA')
ax[2].set_ylabel('PRH')
ax[3].set_ylabel('Rat position')

plt.tight_layout()


if save_plots :
    plot_name = 'raster_sess_{}_trial_{}.{}'.format(sess_ind, trial_number, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

# leg.set_title('')
# f, ax = plt.subplots(1, 1, figsize=[3, 1])
# ax.legend(handles=leg.legendHandles, frameon=False, title='')
# ax.axis('off')
#
# if save_plots :
#     plot_name = 'raster_legend.{}'.format(plot_format)
#     f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

# average_running_speed = get_average_running_speed(sess_ind, data_folder=data_folder,
#                                                   data_version=data_version)


#
# def bin_spikes(spiketrains, binsize):
#
#     spiketrains = [sp.time_slice(t_start=t_start, t_stop=t_stop) for sp
#                    in spiketrains]
#
#     left_edges = np.arange(t_start.rescale('us'), t_stop.rescale('us'),
#                            binsize.rescale('us')) * pq.us
#     bin_edges = [(e, e + binsize.rescale('us')) for e in left_edges]
#
#     # make sure that all the bin centers are within the event
#     bin_edges = [b for b in bin_edges if
#                  (b[0] + b[1]) / 2 <= t_stop.rescale('us')]
#     # make sure that bins are fully within
#     # bin_edges = [b for b in bin_edges if b[1] <= t_stop.rescale('us')]
#
#     # prepare the bin centers (to return)
#     bin_centers = [(b1 + b2) / 2 for b1, b2 in bin_edges]
#     bin_centers = np.array(bin_centers) * pq.us
#
#     num_bins = len(bin_edges)  # Number of bins
#     num_neurons = len(spiketrains)  # Number of neuron_inds
#     binned_spikes = np.empty([num_neurons, num_bins])
#
#     for i, train in enumerate(spiketrains) :
#         spike_times = train.times.rescale('us')
#         for t, edges in enumerate(bin_edges) :
#             binned_spikes[i, t] = np.histogram(spike_times, edges)[0]
#
#     binned_spikes = binned_spikes.astype(int)
#
#     return binned_spikes, bin_centers