import os
from neo.io import NeuralynxIO
import matplotlib.pyplot as plt
from neuralynx_io import neuralynx_io
import os, glob
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from utils import *

data_version         = 'dec16'
PBEs_settings_name   = 'dec19k20'
PBEs_area            = 'Hippocampus'
dec_settings_name    = 'DecSet2'
dec_area             = 'Hippocampus'
sessions             = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
group_iti_and_return = True

lfp_path = '/Users/pietro/data/touch_see/LFP'


SPEED_THRESHOLD      = 4

# PLOTTING PARAMETERS
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'lfp_events')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df, df_not_sig = load_replay(sessions=sessions,
                             PBEs_settings_name=PBEs_settings_name,
                             PBEs_area=PBEs_area,
                             data_version=data_version,
                             dec_settings_name=dec_settings_name,
                             dec_area=dec_area,
                             min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                             shuffle_types=shuffle_types,
                             shuffle_p_vals=shuffle_p_vals,
                             group_iti_and_return=group_iti_and_return,
                             return_not_sig=True)

df = add_new_speed(df, time_before_in_ms=0, binsize_interp_pos=50,
                   time_after_in_ms=0, position_sigma=12,
                   data_version=data_version)

df['speed_group'] = None
df.loc[df['average_speed_new']>4, 'speed_group'] = 'Fast\nevents'
df.loc[df['average_speed_new']<=4, 'speed_group'] = 'Slow\nevents'

fastdf = df[df['average_speed_new']>4]


df[df['average_speed_new']>4]['average_speed_new'].quantile(0.5)


for sess in sessions:
    rat, day, session = get_rat_day_session(sess)
    print(sess, rat, day, session)


lfp_path = '/Users/pietro/data/touch_see/LFP'

for sess in sessions:

    rat, day, session = get_rat_day_session(sess)

    print(rat, day, '      ', session)

    sess_path = os.path.join(lfp_path, day, session)
    try:
        os.chdir(sess_path)
    except FileNotFoundError:
        continue

    time_stamps = []
    channels = []


    session_path = 'SpikeTrl/'+rat+'/'+day+'/'+session+'/spikeTrl.mat'
    S  = loadmat(DATA_FOLDER + session_path)
    first_ts = S['spike']['hdr'][sess].FirstTimeStamp

    for i, file in enumerate(glob.glob("*.ncs")):
        ncs = neuralynx_io.load_ncs(os.path.join(sess_path, file))  # Load signal data into a dictionary
        channels.append(ncs['data'])
        if i == 0:
            time_stamps.append(ncs['time'])

    ts = time_stamps[0]
    sessdf = fastdf[fastdf['sess_ind'] == sess]

    for i, row in sessdf.iterrows():

        start_time = (row['start_time_in_ms'])*1000+first_ts
        end_time = (row['end_time_in_ms'])*1000+first_ts

        indx = [np.logical_and(ts > start_time, ts < end_time)][0]

        f, ax = plt.subplots(1, 1, figsize=[3, 3])
        for ch in channels:
            ax.plot(ch[indx])
        sns.despine()
        ax.set_xlabel('Time [$\mu$s]')
        ax.set_title('Session {}'.format(sess))
        plt.tight_layout()

        plot_name = 'event_{}_sess_{}.{}'.format(i, sess, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



