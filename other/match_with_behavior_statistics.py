from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from scipy.stats import wilcoxon, mannwhitneyu
import matplotlib.pyplot as plt
from utils import *
import scipy.stats
import itertools
from plotting_style import *
import matplotlib.patches as mpatches
from sklearn.metrics import balanced_accuracy_score, accuracy_score



data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'
#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

group_iti_and_return = True

# PLOTTING PARAMETERS

plot_settings = 'only_two_shufs'
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms    = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'hippocampal_replay',
                             'pbe_setting_{}_{}_{}_dec_setting_{}_{}_plot_setting_{}'.format(PBEs_settings_name, PBEs_area,
                              data_version, dec_settings_name, dec_area, plot_settings))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return)

# --- LOAD BIN CENTERS --------------------------------------------------------
sess_ind = 10
F = loadmat(DATA_FOLDER + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]
bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, session)
bin_centers, bin_labels = prepare_bins(bin_centers_combined)



# --- MATCH WITH CURRENT/PREVIOUS TRIAL CONFUSION MAT --------------------------
import matplotlib

# # Say, "the default sans-serif font is COMIC SANS"
# matplotlib.rcParams['font.sans-serif'] = "Keyboard"
# # Then, "ALWAYS use sans-serif fonts"
# matplotlib.rcParams['font.family'] = "sans-serif"
#


if group_iti_and_return :
    phases = ['iti', 'img', 'run', 'reward']
else :
    phases = ['img', 'iti', 'return', 'reward', 'run']

side_col = 'maze_arm_dec_end'
shuffle = True

if False:
    # df_sel = df[(df['epoch'] == 'task') & (df['pbe_side'] != 'central')
    #             & (df['phase'] == 'img')]

    for phase in phases:
        for match_with in ['current_trial_side', 'previous_trial_side']:
            for replay_direction in ['positive', 'negative', 'all']:

                if replay_direction == 'all':
                    df_sel = df[(df['epoch'] == 'task') & (df[side_col] != 'central')
                            & (df['phase'] == phase)]
                else:
                    df_sel = df[(df['epoch'] == 'task') & (df[side_col] != 'central')
                            & (df['replay_direction'] == replay_direction)
                            & (df['phase'] == phase)]
                #
                # side_col = 'pbe_side'
                #
                # df_sel = df[(df['epoch'] == 'task') & (df[side_col] != 'central')
                #             & (df['replay_direction'] == 'positive')
                #             & (df['phase'] == 'img')]

                a1_side = df_sel[side_col][1:-1]
                a2_side = df_sel[match_with][1:-1]

                ind = np.where(~a2_side.isna())[0]
                a1_side = a1_side[ind]
                a2_side = a2_side[ind]
                #a2_side = df_sel['previous_trial_side'][1:]

                cm = confusion_matrix(a1_side, a2_side)
                cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

                if shuffle:
                    cm_shuf = np.zeros([2, 2])
                    for ii in range(30):
                        cm_shuf_boot = confusion_matrix(a1_side, np.random.permutation(a2_side))
                        cm_shuf_boot = cm_shuf_boot.astype('float') / cm_shuf_boot.sum(axis=1)[:, np.newaxis]
                        cm_shuf = cm_shuf + cm_shuf_boot
                    cm_shuf = cm_shuf / 30

                    cm = cm - cm_shuf


                #f, ax2 = plt.subplots(1, 1)


                f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
                sns.heatmap(cm,
                            xticklabels=['Left arm', 'Right arm'],
                            yticklabels=['Left arm', 'Right arm'],
                            ax=ax, square=True,
                            annot=True,
                            cmap='RdBu_r', center=0,
                            cbar=False, #cbar_ax=ax2,
                            vmin=-0.2, vmax=0.7)
                ax.set_ylabel('')
                #ax.set_yticks([])
                #ax.set_xticks([])
                ax.tick_params(axis='both', which='both', length=0)

                ax.set_ylabel('End of replay trajectory')
                ax.set_xlabel('Side of {} trial'.format(heatmap_trial_labels[match_with]))
                ax.set_title('{}'.format(task_phase_labels_nobreak[phase]))
                # ax.set_title('{} ({} REs)'.format(task_phase_labels_nobreak[phase],
                #                                    a1_side.shape[0]))

                ax.tick_params(axis='both', which='major', labelsize=9)
                ax.tick_params(axis='both', which='minor', labelsize=10)
                plt.tight_layout()

                plot_name = 'match_with_{}_{}_{}_{}_events.{}'.format(match_with, phase, replay_direction, a1_side.shape[0], plot_format)
                f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
                plt.close(fig=f)
                #plt.close(fig=f)






replay_direction = 'all'
side_col = 'maze_arm_dec_end'
#shuffle = True
n_boot = 50


if False:
    for match_with in ['current_trial_side', 'previous_trial_side']:
        for replay_direction in ['positive', 'negative', 'all']:
            dac = pd.DataFrame(columns=['phase', 'accuracy', 'n_samp'])

            for phase in phases :
                if replay_direction == 'all' :
                    df_sel = df[(df['epoch'] == 'task')
                                & (df[side_col] != 'central') & (df['phase'] == phase)]
                else :
                    df_sel = df[(df['epoch'] == 'task') &
                                (df[side_col] != 'central') & (df['replay_direction'] == replay_direction)
                                & (df['phase'] == phase)]

                a1_side = df_sel[side_col][1:-1]
                a2_side = df_sel[match_with][1:-1]

                ind = np.where(~a2_side.isna())[0]
                a1_side = a1_side[ind]
                a2_side = a2_side[ind]

                for b in range(n_boot):
                    ind = np.random.choice(np.arange(a1_side.shape[0]), a1_side.shape[0],
                                           replace=True)
                    a1_side_use = a1_side[ind]
                    a2_side_use = a2_side[ind]

                    acc = balanced_accuracy_score(a1_side_use, a2_side_use)

                    dac.loc[dac.shape[0], :] = [phase, acc, a1_side.shape[0]]

            f, ax = plt.subplots(1, 1, figsize=[2.5,4.5])
            sns.barplot(data=dac, x='phase', y='accuracy', facecolor='white',
                        order=phases,
                        edgecolor=sns.xkcd_rgb['dark grey'], linewidth=2.5, errcolor='0.2')
            plt.setp(ax.artists, edgecolor=sns.xkcd_rgb['grey'], facecolor='w')
            plt.setp(ax.lines, color=sns.xkcd_rgb['dark grey'], lw=4)
            ax.axhline(0.5, c='grey', ls='--')
            ax.set_yticks([0, 0.25, 0.5, 0.75, 1])
            ax.set_xticklabels([task_phase_labels[p] for p in phases], rotation=45)
            ax.set_ylabel('Balanced accuracy score')
            ax.set_xlabel('')
            sns.despine()
            plt.tight_layout()

            plot_name = 'barplot_match_with_{}_{}_{}_events.{}'.format(match_with,
                                                                  replay_direction,
                                                                  a1_side.shape[0],
                                                                  plot_format)
            f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
            plt.close(fig=f)


side_col = 'maze_arm_dec_end'
n_boot = 500
replay_direction = 'all'
dac = pd.DataFrame(columns=['phase', 'accuracy', 'match_with', 'n_samp'])

for match_with in ['current_trial_side', 'previous_trial_side'] :

    for phase in phases :
        if replay_direction == 'all' :
            df_sel = df[(df['epoch'] == 'task')
                        & (df[side_col] != 'central')
                        & (df['phase'] == phase)]
        else :
            df_sel = df[(df['epoch'] == 'task') &
                        (df[side_col] != 'central')
                        & (df['replay_direction'] == replay_direction)
                        & (df['phase'] == phase)]
        print('phase: {}, n_samp={}'.format(phase, df_sel.shape))
        a1_side = df_sel[side_col][1 :-1]
        a2_side = df_sel[match_with][1 :-1]

        ind = np.where(~a2_side.isna())[0]
        a1_side = a1_side[ind]
        a2_side = a2_side[ind]

        for b in range(n_boot) :
            ind = np.random.choice(np.arange(a1_side.shape[0]),
                                   a1_side.shape[0],
                                   replace=True)
            a1_side_use = a1_side[ind]
            a2_side_use = a2_side[ind]

            acc = balanced_accuracy_score(a1_side_use, a2_side_use)

            dac.loc[dac.shape[0], :] = [phase, acc, match_with, a1_side.shape[0]]


for phase in phases:

    for match_with in ['current_trial_side', 'previous_trial_side'] :

        print('\n{} {}'.format(match_with, phase))
        stats = dac[(dac['match_with'] == match_with) & (dac['phase'] == phase)]['accuracy']

        alpha = 0.95
        p = ((1.0 - alpha) / 2.0) * 100
        lower = max(0.0, np.percentile(stats, p))
        p = (alpha + ((1.0 - alpha) / 2.0)) * 100
        upper = min(1.0, np.percentile(stats, p))
        print('%.1f confidence interval %.1f%% and %.1f%%' % ( alpha * 100, lower * 100, upper * 100))


# CANNOT USE BOOTSTRAPS ON BOOTSTRAPS!


y = []
err = []
for phase in phases:
    for match_with in ['previous_trial_side', 'current_trial_side'] :
        print('\n{} {}'.format(match_with, phase))
        stats = dac[(dac['match_with'] == match_with) & (dac['phase'] == phase)]['accuracy']
        obs_val = stats.median()
        y.append(obs_val)
        alpha = 0.95
        p = ((1.0 - alpha) / 2.0) * 100
        lower = max(0.0, np.percentile(stats, p))
        p = (alpha + ((1.0 - alpha) / 2.0)) * 100
        upper = min(1.0, np.percentile(stats, p))
        err.append([obs_val - lower, upper - obs_val])
        print('%.1f confidence interval %.1f%% and %.1f%%' % ( alpha * 100, lower * 100, upper * 100))



f, ax = plt.subplots(1, 1, figsize=[2.5, 4])

ax.bar(x=[0, 1, 3, 4, 6, 7, 9, 10],
       height=y,
       yerr=np.array(err).T,
       linewidth=2.5,
       ecolor='0.2')

for i, patch in enumerate(ax.patches):
    if i%2==0:
        patch.set_edgecolor(sns.xkcd_rgb['grey'])
        patch.set_facecolor('white')

    else:
        patch.set_edgecolor(sns.xkcd_rgb['grey'])
        patch.set_facecolor(sns.xkcd_rgb['grey'])

ax.axhline(0.5, c=maze_color, ls='--', lw=2)
ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1])
ax.set_xticks([0.5, 3.5, 6.5, 9.5])
ax.set_xticklabels([task_phase_labels[p] for p in phases],
                   rotation=45)
ax.set_ylabel('Match with trajectory side')
ax.set_xlabel('')
ax.legend().remove()
sns.despine()
plt.tight_layout()

plot_name = 'barplot_match_with_both_{}_events.{}'.format(
    replay_direction,
    plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
plt.close(fig=f)



pos_patch = mpatches.Patch(edgecolor=sns.xkcd_rgb['grey'],
                           facecolor='white', linewidth=2.5,
                           label='Previous trial side')
neg_patch = mpatches.Patch(edgecolor=sns.xkcd_rgb['grey'],
                           facecolor=sns.xkcd_rgb['grey'], linewidth=2.5,
                           label='Current trial side')
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=[pos_patch, neg_patch], frameon=False)
ax.axis('off')
plt.tight_layout()

plot_name = 'barplot_match_trial_side_legend.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

