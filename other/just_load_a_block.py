import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(sys.path[0]))))
import numpy as np
import quantities as pq
from utils import merge_segments
from constants import DATA_FOLDER
from utils import get_session_block
from utils import load_bin_centers, prepare_bins


sess_ind = 29

data_version = 'dec16'

bl = get_session_block(DATA_FOLDER, sess_ind, 'task', data_version=data_version)

rat = bl.annotations['animal_ID']
day_folder = bl.annotations['day_folder']
session_folder = bl.annotations['session_folder']
bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day_folder,
                                        session_folder)
bin_centers, bin_labels = prepare_bins(bin_centers_combined)

segment = merge_segments(bl.segments[:-1], bl.list_units,
                         chop_return_central_arm=False)
rat_speed_signal = segment.irregularlysampledsignals[1].rescale(pq.cm / pq.s)
rat_speed = rat_speed_signal.__array__().flatten()
rat_speed_times = rat_speed_signal.times.rescale(pq.ms)

rat_position_signal = segment.irregularlysampledsignals[0]
rat_position = rat_position_signal.__array__()
rat_position_times = rat_position_signal.times.rescale(pq.ms)

event_times = np.hstack([s.events[0].times.rescale(pq.ms) for s in bl.segments[:-1]])
event_labels = np.hstack([s.events[0].labels for s in bl.segments[:-1]])

trial_start_times = [s.t_start.rescale(pq.ms) for s in bl.segments[:-1]]
trial_sides = [s.annotations['trial_side'] for s in bl.segments[:-1]]



