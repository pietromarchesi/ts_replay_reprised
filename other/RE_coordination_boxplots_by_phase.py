import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from session_selection import sessions_HC_replay
from utils import *
import quantities as pq
import matplotlib.patches as mpatches
from scipy.ndimage import gaussian_filter1d
import itertools



"""

"""


data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

ratemap_areas = ['Both']

group_iti_and_return = True

# PLOTTING PARAMETERS
#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'jul4_DecSet2_smooth2_morelages'


plot_settings = 'only_two_shufs'
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms    = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'hippocampal_replay',
                             'pbe_setting_{}_{}_{}_dec_setting_{}_{}_ratemaps_{}_plot_setting_{}'.format(PBEs_settings_name, PBEs_area,
                              data_version, dec_settings_name, dec_area, ratemap_settings, plot_settings))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return)


df_pre = df.copy()


# --- ITERATE OVER AREAS -------------------------------------------------------


for area in ratemap_areas:

    if area == 'Perirhinal' or area == 'Barrel':
        df = add_coordination_per_event(df.copy(), ratemap_area=area,
                                        ratemap_settings=ratemap_settings,
                                        data_version=data_version)
    elif area == 'Both':
        df = add_coordination_both_areas(df.copy(),
                                        ratemap_settings=ratemap_settings,
                                        data_version=data_version)

    df['phase'] = df['phase'].replace(to_replace='img', value='iti')


    col_name = 'coordination_{}'.format(area.lower())

    # --- BOXPLOTS -----------------------------------------------------------------

    if group_iti_and_return:
        phases = ['iti', 'run', 'reward']
    else:
        phases = ['iti', 'return', 'reward', 'run']


    cols = ['replay_score', 'event_duration_in_ms',
            'dist_traversed_cm', 'compression_factor', 'max_jump_dist',
            'mean_jump_dist', 'sharpness', 'n_active']

    for phase in phases:

        combos = list(itertools.combinations(phases, 2))

        df_sel = df[df['epoch'] == 'task']
        df_sel = df_sel[df_sel['phase'] == phase]

        for col in cols:
            print('\n')

            df1 = df_sel[(df_sel['epoch'] == 'task') & (df_sel[col_name] == 'coordinated')]
            df2 = df_sel[(df_sel['epoch'] == 'task') & (df_sel[col_name] == 'uncoordinated')]
            x = df1[col].values
            y = df2[col].values
            #print(np.median(x), np.median(y))
            st,pval = mannwhitneyu(x, y)
            print('{}, {} vs {} medians: {:.2f} vs {:.2f}: {}, p_val = {:.3e}, sig: {}'.format(col,
                  'coordinated', 'uncoordinated', np.median(x), np.median(y), st, pval, pval<0.05/len(combos)))



        cols = ['replay_score', 'event_duration_in_ms',
                'dist_traversed_cm', 'compression_factor', 'max_jump_dist',
                'mean_jump_dist', 'sharpness', 'n_active']

        order = ['coordinated', 'uncoordinated']
        for col in cols:

            f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
            sns.boxplot(data=df_sel, x=col_name, y=col, color='white',
                        fliersize=2, order=order)
            plt.setp(ax.artists, edgecolor=sns.xkcd_rgb['dark grey'], facecolor='w')
            plt.setp(ax.lines, color=sns.xkcd_rgb['dark grey'])

            if col == 'compression_factor':
                ax.set_yscale("log")
                ax.set_ylim(1, 100)

            #sns.swarmplot(data=df_sel, x='phase', y=col, color=sns.xkcd_rgb['dark grey'])

            ax.set_ylabel(replay_metric_labels[col])
            ax.set_xlabel('')
            # ax.set_xticklabels([task_phase_labels_nobreak[l._text] for l in ax.get_xticklabels()],
            #                    rotation=30)
            sns.despine()
            plt.tight_layout()

            plot_name = 'replay_{}_per_coordination_{}.{}'.format(col, phase, plot_format)
            f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

            #plt.close(fig=f)