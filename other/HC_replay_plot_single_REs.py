import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from session_selection import sessions_HC_replay
from utils import load_decoded_PBEs, load_replay


data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'
plot_settings      = 'base'

sessions           = [0,1,2,3,4,6,7,8,9,10,11,28,29,30] #sessions_HC_replay
epochs             = ['task']


# FILTER PBES PARAMETERS
phase = None  # 'iti', 'img', 'run', 'return' (or list)
epoch = None # 'task', 'pre_sleep', 'post_sleep' (or list)

# shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle',
#                  'unit_identity_shuffle']
# p_vals = [0.05, 0.1, 0.1]

shuffle_types  = ['column_cycle_shuffle', 'place_field_rotation_shuffle',
                  'unit_identity_shuffle']
shuffle_p_vals = [0.017, 0.017, 0.017]

shuffle_p_vals = [0.003, 0.003, 0.003]



min_pbe_duration_in_ms = 50

SPEED_THRESHOLD = 3


# PLOTTING PARAMETERS
save_plots = True
plot_format = 'png'
dpi = 400


plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'decoded_PBEs',
                             'pbe_setting_{}_{}_{}_dec_setting_{}_{}_plot_setting_{}'.format(PBEs_settings_name, PBEs_area,
                              data_version, dec_settings_name, dec_area, plot_settings))


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD REPLAY ---

df, df_not_sig = load_replay(sessions=sessions,
                             PBEs_settings_name=PBEs_settings_name,
                             PBEs_area=PBEs_area,
                             data_version=data_version,
                             dec_settings_name=dec_settings_name,
                             dec_area=dec_area,
                             min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                             shuffle_types=shuffle_types,
                             shuffle_p_vals=shuffle_p_vals,
                             group_iti_and_return=True,
                             return_not_sig=True,
                             max_average_speed=SPEED_THRESHOLD,
                             NEWSPEED=True)

# --- LOAD DATA ---------------------------------------------------------------

rds = {}

for sess_ind in sessions:
    for epoch in epochs:

        pbes = load_decoded_PBEs(sess_ind, PBEs_settings_name, PBEs_area,
                                 data_version, dec_settings_name, dec_area)

        for pbe_id, det in zip(pbes['pbes'].index, pbes['detectors']):
            rds[pbe_id] = det




#shuffle_types = sig_df['shuffle_type'].unique().tolist()

# --- FILTER PBES -------------------------------------------------------------



fast_events = ['0_task_123', '0_task_443', '0_task_952', '0_task_1611', '1_task_965',
 '2_task_204', '3_task_111', '3_task_273', '3_task_486', '3_task_889',
 '3_task_1148', '3_task_1343', '3_task_1588', '3_task_1870', '4_task_317',
 '5_task_243', '5_task_810', '6_task_309', '6_task_1263', '7_task_135',
 '7_task_1342', '8_task_1099', '9_task_618', '10_task_978', '27_task_26',
 '29_task_90', '29_task_476', '29_task_887', '30_task_494', '30_task_637']

fast_events = ['6_task_309', '6_task_1263', '7_task_135',
 '7_task_1342', '8_task_1099', '9_task_618', '10_task_978',
 '29_task_90', '29_task_476', '29_task_887', '30_task_494', '30_task_637']




# --- GET GOOD EVENTS ---------------------------------------------------------

df_sort = df.sort_values(by=['replay_score'], ascending=False)
top_events = df_sort.iloc[0:100].index

# df_sort = df.sort_values(by=['mean_jump_dist'], ascending=True)
# df_sort = df_sort.sort_values(by=['dist_traversed_cm', 'percentage_active'], ascending=False)

for id in top_events:
    pbe = rds[id]
    pbe.binsize_in_ms = 10
    f = pbe.plot_replay(shuffle_type=shuffle_types,
                                       figsize_tweak_w=1.5, figsize_tweak_h=1,
                                       padded=False)
    plot_name = 'pbe_panel_{}.{}'.format(id, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

df_sort = df.sort_values(by=['percentage_active', 'dist_traversed_cm', 'average_speed'], ascending=False)
df_sort = df_sort[df_sort['mean_jump_dist'] < df_sort['mean_jump_dist'].quantile(0.40)]

n_events = 6
event_ids = df_sort.iloc[0:n_events].index
figsize=[tiny_panel_side*2, tiny_panel_side*n_events]


f, ax = plt.subplots(n_events, 2, figsize=figsize)

for row, id in enumerate(event_ids):
    pbe = rds[id]
    pbe.binsize_in_ms = 10

    pbe._plot_binned_spikes(ax[row, 0], plot_cbar=False)

    pbe._plot_decoded_pbe(ax[row, 1], plot_cbar=False)
    #ax[row, 0].axis('off')
    #ax[row, 1].axis('off')
    # ax[row, 0].set_ylabel('Neurons (sorted)')
    # ax[row, 1].set_ylabel('Decoded probability')

    for ii in [0, 1]:
        ax[row, ii].set_xlabel('')
        ax[row, ii].set_ylabel('')
        ax[row, ii].set_xticks([])
        ax[row, ii].set_xticklabels('')
        ax[row, ii].set_yticks([])
        ax[row, ii].set_yticklabels('')

# ax[-1, 0].set_xlabel('Time')
# ax[-1, 1].set_xlabel('Time')

sns.despine(top=True, right=True, offset=0, trim=False)
plt.subplots_adjust(wspace=0.2, hspace=0.2)

plot_name = 'pbe_panel_combined_{}.{}'.format(id, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

# for pbe_id in sel_pbes_sig:
#
#     #f = rds[pbe_id].plot_replay(shuffle_type=shuffle_types)
#
#     pbe = rds[pbe_id]
#     pbe.binsize_in_ms = 10
#
#     shuffle_type = shuffle_types
#
#     if shuffle_type is not None:
#         if isinstance(shuffle_type, str):
#             f, ax = plt.subplots(1, 3, figsize=[5 * 3 * figsize_tweak,
#                                                 4 * figsize_tweak])
#             pbe._plot_decoded_pbe(ax[1])
#             pbe._plot_surrogate_distribution(ax[2], shuffle_type)
#         elif isinstance(shuffle_type, list):
#             f = plt.figure(figsize=[5 * figsize_tweak, 4 * figsize_tweak])
#             gs = gridspec.GridSpec(2, 6, height_ratios=[3, 1])
#             ax1 = f.add_subplot(gs[0, 0:3])
#             ax2 = f.add_subplot(gs[0, 3:])
#
#             if len(shuffle_type) == 2:
#                 ax3 = f.add_subplot(gs[1, 0:3])
#                 ax4 = f.add_subplot(gs[1, 3:])
#                 shuff_axis = [ax3, ax4]
#             elif len(shuffle_type) == 3:
#                 ax3 = f.add_subplot(gs[1, 0:2])
#                 ax4 = f.add_subplot(gs[1, 2:4])
#                 ax5 = f.add_subplot(gs[1, 4:6])
#                 shuff_axis = [ax3, ax4, ax5]
#             else:
#                 raise ValueError('Plotting supports only 2 or 3 '
#                                  'surrogate distributions')
#
#             pbe._plot_binned_spikes(ax1)
#             if padded:
#                 pbe._plot_decoded_pbe_padded(ax2)
#             else:
#                 pbe._plot_decoded_pbe(ax2)
#             for i, ax in enumerate(shuff_axis):
#                 pbe._plot_surrogate_distribution(ax, shuffle_type[i])
#
#     else:
#         f, ax = plt.subplots(1, 2, figsize=[5 * 2 * figsize_tweak,
#                                             4 * figsize_tweak])
#         pbe._plot_binned_spikes(ax[0])
#         pbe._plot_decoded_pbe(ax[1])
#     plt.tight_layout()
#     sns.despine(top=True, right=True)
#
#     plot_name = 'pbe_{}.{}'.format(pbe_id, plot_format)
#
#     f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
