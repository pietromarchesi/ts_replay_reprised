import seaborn as sns
import matplotlib.pyplot as plt
from utils import lighten_color
from matplotlib import rc

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

small_panel_size = [4, 4]
medium_panel_size = [3.5, 3.5]

smaller_panel_size = [3, 3]
smaller_than_smaller_panel_size = [2.7, 2.7]
smaller_but_wider_panel_size = [3, 2.7]


slightly_vertical_panel = [2.5, 4]
slightly_vertical_panel_small = [2.4, 3.2]
thin_vertical_panel = [2, 4]

tiny_panel_side = 2

labels = {'task' : 'Task',
          'pre_sleep' : 'Pre-sleep',
          'post_sleep' : 'Post-sleep'}


epoch_palette = {'task' : None}


task_phase_labels = {'img' : 'Image\n',
                     'reward' : 'Reward',
                     'iti' : 'ITI',
                     'run' : 'pre-Rew.',
                     'all' : 'All'}

task_phase_labels_nobreak = {'img' : 'Image',
                             'reward' : 'Reward',
                             'iti' : 'ITI',
                             'run' : 'pre-Rew.',
                             'all' : 'All'}

task_phase_labels_nobreak_v2 = {'img' : 'Image',
                             'reward' : 'Reward',
                             'iti' : 'ITI',
                             'run' : 'pre-Rew.',
                             'all' : 'All',
                             'itiimg' : 'ITI+Img.'}

coordination_labels_nobreak = {'coordinated' : 'Coord.',
                               'uncoordinated' : 'Uncoord.'}


heatmap_trial_labels = {'current_trial_side' : 'current',
                        'previous_trial_side' : 'previous'}

variable_label = {'corrcoef_debiased' : 'Debiased correlation coefficient',
                  'corrcoef_obs' : 'Correlation coefficient',
                  'percentile_rank' : 'Percentile rank'}



replay_metric_labels = {'replay_score' : 'Replay score',
                        'event_duration_in_ms' : 'Event duration (ms)',
                        'dist_traversed_cm' : 'Distance covered by RE (cm)',
                        'compression_factor' : 'Compression factor',
                        'max_jump_dist' : 'Maximum jump distance',
                        'sharpness' : 'Sharpness',
                        'mean_jump_dist' : 'Mean jump distance',
                        'n_active' : '# neurons participating in RE',
                        'percentage_active' : '% of active neurons'}


replay_direction_labels = {'both'     : 'All',
                           'positive' : 'Forward',
                           'negative' : 'Reverse'}

replay_direction_labels_break = {'positive' : 'Forward',
                                'negative' : 'Reverse'}



animal_loc_color = sns.xkcd_rgb['electric purple']
trajectory_cmap = plt.cm.winter_r

location_bands = {'img' : [-2, 2],
                  'iti' : [-5, 5],
                  'run' : [[-16, -18], [16, 18]],
                  'reward' : [[-16, -18], [16, 18]],
                  'early_reward' : [[-16, -18], [16, 18]],
                  'late_reward' : [[-16, -18], [16, 18]],
                  'reward_edges' : [[-16, -18], [16, 18]],
                  'reward_central' : [[-16, -18], [16, 18]]}

circle_locations = {'img' : [0],
                    'iti' : [29],
                    'reward' : [-18, 18],
                    'run' : [-17, 17]}

circle_size = {'img' : 15,
               'iti' : 30,
               'reward' : 15,
               'run' : 15}

reward_bin = 17

line_plot_color = sns.xkcd_rgb['slate']
barplot_color = sns.xkcd_rgb['grey']


area_palette = {'Hippocampus' : sns.xkcd_rgb['light blue'],
                'Perirhinal' : sns.xkcd_palette(['dark teal'])[0],
                'Barrel' : sns.xkcd_palette(['dark orange'])[0]}

area_palette_2 = {'PRH' : sns.xkcd_palette(['dark teal'])[0],
                  'S1BF' : sns.xkcd_palette(['dark orange'])[0],
                  'Both' : sns.xkcd_rgb['grey']}

ratemap_area_sigla = {'Perirhinal' : 'PER',
                      'Barrel' : 'S1BF',
                      'Hippocampus' : 'CA1'}


real_event_palette = {'Perirhinal' : [sns.xkcd_palette(['light teal'])[0],
                                        area_palette['Perirhinal']],
                                      #lighten_color(area_palette['Perirhinal'], 0.5)],

                      'Barrel' :     [sns.xkcd_palette(['light orange'])[0],
                                        area_palette['Barrel']],
                                      #lighten_color(area_palette['Barrel'], 0.5)],

                      'V1' : [sns.xkcd_rgb['periwinkle blue'],
                              sns.xkcd_rgb['lighter purple']]}

coordinated_palette = {'coordinated' : sns.xkcd_rgb['aquamarine'],
                       'uncoordinated' : sns.xkcd_rgb['bluish purple']}

replay_ratemap_labels = {'percentile_rank' : 'Percentile rank',
                         'corrcoef_debiased' : 'Debiased correlation coefficient',
                         'corrcoef_obs' : 'Correlation coefficient'}


area_code_labels = {'Hippocampus' : 'HC',
                    'Perirhinal' : 'PR',
                    'Barrel' : 'BR',
                    'V1' : 'V1'}


contrast_labels = {'local_remote' : {'local' : 'Local',
                                     'remote' : 'Remote'},
                   'joint_phase' : {'runrew' : 'PR + REW',
                                    'itiimg' : 'ITI + IMG'},
                   'replay_direction' : {'positive' : 'Forward',
                                         'negative' : 'Reverse'},
                   'ahead_behind' : {'ahead' : 'Ahead',
                                         'behind' : 'Behind'},
                   'current_trial_outcome' : {'correct' : 'Correct',
                                              'incorrect' : 'Incorrect'},
                   'radial_direction' : {'centrifugal' : 'Centrifugal',
                                         'centripetal' : 'Centripetal'},
                   'traj_start_reward' : {'yes' : 'Replay starting at REW',
                                          'no' : 'Replay not starting at REW'},
                   'traj_end_reward' : {'yes' : 'Replay ending at REW',
                                          'no' : 'Replay not ending at REW'},
                   'traj_start_end_reward' : {'yes' : 'Replay starting or ending at REW',
                                          'no' : 'Replay not starting or ending at REW'}}


replay_direction_palette = {'positive' : sns.xkcd_rgb['kelly green'],
                            'negative' : sns.xkcd_rgb['darkish red'],
                            'all'      : sns.xkcd_rgb['orange'],
                            'difference' : sns.xkcd_rgb['grey']}

replay_direction_palette_2 = {'positive' : sns.xkcd_rgb['dark teal'],
                              'negative' : sns.xkcd_rgb['darkish red'],
                              'both'      : sns.xkcd_rgb['grey']}


radial_direction_palette = {'centrifugal' : sns.xkcd_rgb['fuchsia'],
                            'centripetal' : sns.xkcd_rgb['bright light blue']}


joint_phase_palette = {'itiimg' : sns.xkcd_rgb['dark forest green'],
                       'runrew' : sns.xkcd_rgb['ultramarine blue'],}


outcome_palette = {'correct' : sns.xkcd_rgb['light forest green'],
                   'incorrect' : sns.xkcd_rgb['pale red']}


rsi_palette = {'RSI above thr.' : sns.xkcd_rgb['dark teal'],
               'RSI below thr.' : sns.xkcd_rgb['darkish red'],
               'difference': sns.xkcd_rgb['grey']}

has_ripple_palette = {True : sns.xkcd_rgb['dark teal'],
                      False : sns.xkcd_rgb['darkish red'],
                      'True': sns.xkcd_rgb['dark teal'],
                      'False': sns.xkcd_rgb['darkish red'],
                      'difference': sns.xkcd_rgb['grey']}


task_phase_palette = {'iti' : sns.xkcd_rgb['greyish blue'],
                      'return' : sns.xkcd_rgb['greyish brown'],
                      'reward' : sns.xkcd_rgb['greyish purple'],
                      'img' : sns.xkcd_rgb['greyish green'],
                      'difference' : sns.xkcd_rgb['dark grey']}

beginning_end_palette = {'dec_start_bin' : plt.cm.winter_r(int(255 * 0/10)),
                         'dec_end_bin' : plt.cm.winter_r(int(255 * 10/10))}


maze_color = sns.xkcd_rgb['steel grey']