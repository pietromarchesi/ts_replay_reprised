import os
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *
from plotting_style import *
from sklearn.metrics import confusion_matrix
from scipy.ndimage.filters import gaussian_filter
from utils import make_confusion_matrix_wrt_chance
from sklearn.metrics import balanced_accuracy_score as classification_score
from scipy.stats import mannwhitneyu

data_version = 'dec16'
settings_name = 'jan3'

n_boot = 500

plot_format = 'png'
dpi = 400

# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, 'POS_dec', settings_name)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)


# --- LOAD RESULTS ------------------------------------------------------------
dfs = {}

for area in ['Hippocampus', 'Perirhinal', 'Barrel']:

    output_folder = os.path.join(REPLAY_FOLDER, 'results', 'dec')
    output_file = 'dec_rs_{}_{}_{}.pkl'.format(settings_name, area, data_version)
    out = pickle.load(open(os.path.join(output_folder, output_file), 'rb'))

    # load variables
    sess_info = out['sess_info']
    dec_rs = out['dec_rs']
    score_col = 'dec_score_{}'.format(area_code_labels[area])
    sel_sess_stat = sess_info[~np.isnan(sess_info[score_col])].index.values


    dfsig = pd.DataFrame(columns=['sess_ind', 'acc_obs', 'acc_deb', 'p_val'])

    for sess_ind in sel_sess_stat:

        #cm = dec_rs[sess_ind]['confusion_mat']
        y = dec_rs[sess_ind]['y']
        y_pred = dec_rs[sess_ind]['y_pred']

        acc_obs = classification_score(y, y_pred)

        acc_shuf = np.zeros(n_boot)
        for n in range(n_boot):
            acc_shuf[n] = classification_score(y, np.random.permutation(y_pred))

        p_val = (acc_shuf >= acc_obs).sum() / acc_shuf.shape[0]
        acc_deb = acc_obs - acc_shuf.mean()
        dfsig.loc[dfsig.shape[0], :] = [sess_ind, acc_obs, acc_deb, p_val]

    dfs[area] = dfsig


# --- STATISTICS ---------------------------------------------------------------

for dfsig, area in zip([dfs['Perirhinal'], dfs['Barrel']], ['Perirhinal', 'Barrel']):
    print('\n\n{}'.format(area))
    print('\nposition decoding better than chance for {} of {} sessions considered (p<0.001)'
          ''.format(dfsig[dfsig['p_val'] < (0.001/dfsig.shape[0])].shape[0], dfsig.shape[0]))

    print('\nbalanced classification of the {} sessions included: {:.2f}, {:.2f}-{:.2f}'.format(
          dfsig.shape[0], dfsig['acc_obs'].quantile(0.5),
            dfsig['acc_obs'].quantile(0.25), dfsig['acc_obs'].quantile(0.75)))

    print('\nbalanced DEBIASED classification of the {} sessions included: {:.2f}, {:.2f}-{:.2f}'.format(
          dfsig.shape[0], dfsig['acc_deb'].quantile(0.5),
            dfsig['acc_deb'].quantile(0.25), dfsig['acc_deb'].quantile(0.75)))


# x = dfs['Perirhinal']['acc_deb']
# y = dfs['Barrel']['acc_deb']
# stat, p = mannwhitneyu(x, y)
# print('Difference between debiased balanced accuracies Mann-Whitney U-test: '
#       'p={:.1g}'.format(p))

combos = [('Hippocampus', 'Perirhinal'),
          ('Hippocampus', 'Barrel'),
          ('Perirhinal', 'Barrel')]
for combo in combos:
    x = dfs[combo[0]]['acc_deb']
    y = dfs[combo[1]]['acc_deb']
    stat, p = mannwhitneyu(x, y)
    print(combo)
    p = p * len(combos)
    print('Difference between balanced accuracies Mann-Whitney U-test:'
          'p={:.1g}'.format(p))




# --- BARPLOT ------------------------------------------------------------------
dfs['Perirhinal']['area'] = 'Perirhinal'
dfs['Barrel']['area'] = 'Barrel'
dfs['Hippocampus']['area'] = 'Hippocampus'
dx = pd.concat([dfs['Hippocampus'], dfs['Perirhinal'], dfs['Barrel']])

f, ax = plt.subplots(figsize=[2.5, 3.5])
sns.barplot(data=dx, x='area', y='acc_deb', order=['Hippocampus', 'Perirhinal', 'Barrel'])
plt.setp(ax.patches[0], edgecolor=area_palette['Hippocampus'], facecolor='w',
         linewidth=3)
plt.setp(ax.patches[1], edgecolor=area_palette['Perirhinal'], facecolor='w',
         linewidth=3)
plt.setp(ax.patches[2], edgecolor=area_palette['Barrel'], facecolor='w',
         linewidth=3)

#ax.axhline(0.33, ls='--', linewidth=3, color='grey')
ax.set_ylabel('Maze arm decoding accuracy\nbeyond chance')
ax.set_xlabel('')
ax.set_xticklabels(['CA1', 'PRH', 'S1BF'])
ax.set_yticks([0, 0.1, 0.2, 0.3, 0.4])
sns.despine()
plt.tight_layout()

plot_name = 'barplot_pos_dec_PRH_S1BF_{}.{}'.format(settings_name, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)