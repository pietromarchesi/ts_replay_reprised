import os
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *
from plotting_style import *
from utils import get_decoding_data
from utils import bayesian_cross_validate
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_predict
from utils import load_bin_centers
from utils import prepare_bins, get_position_bin
from utils import coarsify_bins
from sklearn.metrics import confusion_matrix
from sklearn.metrics import balanced_accuracy_score
from sklearn.model_selection import StratifiedKFold


data_version = 'dec16'
settings_name = 'dec17'
sess_ind = None
area = 'Hippocampus'

min_units = 20
min_speed_train = 12
min_spikes_per_sample = 1
min_spikes_per_neuron = 30
pyramidal_only = True

# DECODING PARS
decoder = 'dkw_decoder'
n_splits = 3

decode_modality = 'position' # 'leftright', 'leftmidright', 'position'

bin_sizes_train = [300, 400, 500]
smooth_hyperpars = [(False, 0), (True, 1), (True, 2), (True, 3)]

# --- SET UP PATHS ------------------------------------------------------------

output_folder = os.path.join(REPLAY_FOLDER, 'results', 'dec')
output_file = 'dec_rs_{}_{}_{}_hyperpars.pkl'.format(settings_name, area, data_version)
output_full_path = os.path.join(output_folder, output_file)
# --- DECODE ------------------------------------------------------------------

sess_info = pd.read_pickle(os.path.join(DATA_FOLDER, 'sessions_overview.pkl'))
area_code = area_code_labels[area]
score_col = 'dec_score_{}'.format(area_code)
sess_info[score_col] = np.nan

# select sessions
if sess_ind is None:
    sel_sess = sess_info.loc[sess_info['n{}'.format(area_code)] >= min_units].index.values
else:
    sel_sess = [sess_ind]


df = pd.DataFrame(columns=['session', 'error', 'bin_size', 'smooth_ratemap', 'smooth_sigma'])

for bin_size_train in bin_sizes_train:
    for smooth_ratemap, smooth_sigma in smooth_hyperpars:
        for sess_ind in sel_sess:
            print('Running session {} for binsize {}, smooth ratemap {} with sigma {}'
                  ''.format(sess_ind, bin_size_train, smooth_ratemap, smooth_sigma))
            # load session data
            dec_data = get_decoding_data(sess_ind=sess_ind, data_folder=DATA_FOLDER,
                                         area=area,
                                         data_version=data_version,
                                         binsize=bin_size_train, remove_nans=True,
                                         min_speed=min_speed_train,
                                         min_spikes_per_sample=min_spikes_per_sample,
                                         min_spikes_per_neuron=min_spikes_per_neuron,
                                         pyramidal_only=pyramidal_only)

            X = dec_data['X']
            y = dec_data['y'].astype(int)
            bins = dec_data['bins']
            neuron_sel = dec_data['neuron_sel']


            # --- Get decoding inputs ---
            if decode_modality == 'leftright':
                y, bins = coarsify_bins(y, mode='leftmidright')
                bin_labels = ['Left', 'Right']
                X = X[y!=1]
                y = y[y!=1]

            elif decode_modality == 'leftmidright':
                y, bins = coarsify_bins(y, mode='leftmidright')
                bin_labels = ['Left', 'Mid', 'Right']

            elif decode_modality == 'position':
                rat, day, sess_id = sess_info.loc[sess_ind, ['animal', 'day', 'session']].values
                bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, sess_id)
                bin_centers, bin_labels = prepare_bins(bin_centers_combined)
                #bin_labels = np.arange(-29, 30)


            # --- Set up decoder and fit ---
            cv = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                 random_state=sess_ind)

            if decoder == 'dkw_decoder':
                y, y_pred = bayesian_cross_validate(X, y, bin_size=bin_size_train,
                                                    cv=cv, smooth_ratemap=smooth_ratemap,
                                                    smooth_sigma=smooth_sigma)
            else:
                if decoder == 'random_forest':
                    dec = RandomForestClassifier(n_estimators=200,
                                                 random_state=1)
                elif decoder == 'logistic_regression':
                    dec = LogisticRegression(solver='lbfgs', random_state=1)

                y_pred = cross_val_predict(dec, X, y, cv=cv)


            # --- Compute decoding metric ---
            if np.isin(decode_modality, ['leftright', 'leftmidright']):
                score = balanced_accuracy_score(y, y_pred)

            elif decode_modality == 'position':
                error = []
                for true_bin, pred_bin in zip(y, y_pred):
                    true_pos = get_position_bin(true_bin, bin_centers, bin_labels)
                    pred_pos = get_position_bin(pred_bin, bin_centers, bin_labels)
                    error.append(np.linalg.norm(true_pos - pred_pos))
                error = np.array(error)
                score = error.mean()


            df.loc[df.shape[0], :] = [sess_ind, score, bin_size_train,
                                      smooth_ratemap, smooth_sigma]


df['error'] = pd.to_numeric(df['error'])



# --- COLLECT PARS AND SAVE OUTPUT --------------------------------------------

pars = {'settings_name' : settings_name,
        'bin_sizes_train' : bin_sizes_train,
        'smooth_hyperpars' : smooth_hyperpars,
        'area' : area,
        'min_units' : min_units,
        'min_speed_train' : min_speed_train,
        'min_spikes_per_sample' : min_spikes_per_sample,
        'min_spikes_per_neuron' : min_spikes_per_neuron,
        'pyramidal_only' : pyramidal_only,
        'decoder' : decoder,
        'n_splits' : n_splits,
        'decode_modality' : decode_modality,
        'sel_sess' : sel_sess,
        'bin_labels' : bin_labels}


out = {'pars' : pars,
       'df' : df}


if not os.path.isdir(output_folder):
    os.makedirs(output_folder)

print('Saving output to {}'.format(output_full_path))
pickle.dump(out, open(output_full_path, 'wb'))


