import os
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *


data_version = 'dec16'
settings_name = 'dec17'
sess_ind = None
area = 'Hippocampus'

plot_format = 'png'
dpi = 400
save_plots = True
score_threshold_cm = 35 # to determine which confusion matrices to plot
min_units = 15

# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, 'POS_dec', settings_name)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD RESULTS ------------------------------------------------------------

output_folder = os.path.join(REPLAY_FOLDER, 'results', 'dec')
output_file = 'dec_rs_{}_{}_{}_hyperpars.pkl'.format(settings_name, area, data_version)
output_full_path = os.path.join(output_folder, output_file)

rs = pickle.load(open(output_full_path, 'rb'))

df = rs['df']



# --- PLOT --------------------------------------------------------------------

#df_sel = df[df['smooth_ratemap'] == True]
df_sel = df[(df['smooth_ratemap'] == True) & (df['smooth_sigma'] == 2)]
f, ax = plt.subplots(1, 1)
sns.lineplot(data=df_sel, x='bin_size', y='error', markers=True)



#df_sel = df[(df['smooth_ratemap'] == True) & (df['smooth_sigma'] == 1)]
df_sel = df[df['bin_size'] == 300]
f, ax = plt.subplots(1, 1)
sns.lineplot(data=df, x='smooth_sigma', y='error', markers=True)
