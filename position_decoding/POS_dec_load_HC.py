import os
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *
from plotting_style import *
from utils import make_confusion_matrix_wrt_chance, compute_euclidean_error
from utils import load_bin_centers, prepare_bins


data_version = 'dec16'
settings_name = 'jan3'
area = 'Hippocampus'


# PLOTTING PARS
plot_format = 'svg'
dpi = 400
save_plots = True
n_boot = 500
cm_smooth_sigma = 1
score_threshold_cm = 45 # to determine which confusion matrices to plot
min_units = 15
run_statistics = False

# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'POS_dec', settings_name)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD RESULTS ------------------------------------------------------------

output_folder = os.path.join(REPLAY_FOLDER, 'results', 'dec')
output_file = 'dec_rs_{}_{}_{}.pkl'.format(settings_name, area, data_version)
out = pickle.load(open(os.path.join(output_folder, output_file), 'rb'))

# load variables
sess_info = out['sess_info']
dec_rs = out['dec_rs']
decode_modality = out['pars']['decode_modality']
bin_labels = out['pars']['bin_labels']
score_col = 'dec_score_{}'.format(area_code_labels[area])


# --- SELECT SESSIONS ---------------------------------------------------------

sel_sess_plot = sess_info[sess_info[score_col] < score_threshold_cm]

area_code = area_code_labels[area]
sel_sess_plot = sel_sess_plot.loc[sel_sess_plot['n{}'.format(area_code)] >= min_units].index.values

sel_sess_stat = sess_info[~np.isnan(sess_info['dec_score_HC'])].index.values


# --- DECODING BEYOND CHANCE
# TODO statistics probably for all sessions?

if run_statistics:
    dfsig = pd.DataFrame(columns=['sess_ind', 'p_val'])

    for sess_ind in sel_sess_stat:
        print(sess_ind)
        rat, day, sess_id = sess_info.loc[sess_ind, ['animal', 'day', 'session']].values
        bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, sess_id)
        bin_centers, bin_labels = prepare_bins(bin_centers_combined)
        bin_labels_cm = np.arange(-29, 30)
        y = dec_rs[sess_ind]['y']
        y_pred = dec_rs[sess_ind]['y_pred']

        score_obs = compute_euclidean_error(y, y_pred, bin_centers, bin_labels).mean()

        score_shuf = np.zeros(n_boot)
        for n in range(n_boot):
            score_shuf[n] = compute_euclidean_error(y, np.random.permutation(y_pred),
                                                    bin_centers, bin_labels).mean()

        print(score_obs, score_shuf.mean())
        p_val = (score_shuf <= score_obs).sum() / score_shuf.shape[0]
        dfsig.loc[dfsig.shape[0], :] = [sess_ind, p_val]

# --- 1. AVERAGE CONFUSION MATRIX ---------------------------------------------


from scipy.ndimage.filters import gaussian_filter

bin_labels = np.arange(-29, 30)

cm_deb = []
for sess_ind in sel_sess_plot:

    y = dec_rs[sess_ind]['y']
    y_pred = dec_rs[sess_ind]['y_pred']

    cm_deb.append((make_confusion_matrix_wrt_chance(y, y_pred, bin_labels, n_boot)))

cm_av = np.sum(cm_deb, axis=0) / len(cm_deb)
cm_av = gaussian_filter(cm_av, sigma=cm_smooth_sigma)

f, ax = plt.subplots(1, 1, figsize=medium_panel_size)
sns.heatmap(cm_av,
            xticklabels=10,
            yticklabels=10,
            ax=ax, square=True,
            annot=False, cmap='RdBu_r', cbar=True, center=0,
            cbar_kws={"shrink": 0.55, 'label':'Prob. density - chance'})
bins = np.arange(-29, 30)
bins_to_tick = [-29, -reward_bin, 0, reward_bin, 29]
for _, spine in ax.spines.items():
    spine.set_visible(True)
sns.despine(top=True, right=True, offset=8, trim=False)

# for loc in [0, 10, -10, 20, -20] :
#     ax.axvline(np.where(bins==loc), c=maze_color, zorder=10, ls='--', lw=0.8)
#     ax.axhline(np.where(bins==loc), c=maze_color, zorder=10, ls='--', lw=0.8)


ax.set_xticks([np.where(bins==b)[0] for b in bins_to_tick])
ax.set_xticklabels(['IMG', 'REW', 'IMG', 'REW', 'IMG'], rotation=90)
ax.set_yticks([np.where(bins==b)[0] for b in bins_to_tick])
ax.set_yticklabels(['IMG', 'REW', 'IMG', 'REW', 'IMG'])
#ax.tick_params('both', length=0, width=0, which='major')
ax.set_ylabel('True location', labelpad=15)
ax.set_xlabel('Predicted location', labelpad=15)
plt.tight_layout()

plot_name = 'decode_cm_averaged_{}_{}_{}.{}'.format(decode_modality, area, settings_name,
                                        plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- 2. PLOT CONFUSION MATRICES OF SELECTED SESSIONS -------------------------


annot = False
bin_labels_cm = []

n_plots = len(sel_sess_plot)
n_cols = 5
n_rows = int(np.ceil(n_plots / n_cols))



f, axes = plt.subplots(n_rows, n_cols, figsize=[n_cols * 2, n_rows * 2],
                       sharex=True, sharey=True)

cbar_ax = f.add_axes([0.86, .3, .03, .4])


cms = []
for sess_ind, ax in zip(sel_sess_plot, axes.flatten()):

    #cm = dec_rs[sess_ind]['confusion_mat']
    y = dec_rs[sess_ind]['y']
    y_pred = dec_rs[sess_ind]['y_pred']

    cm = make_confusion_matrix_wrt_chance(y, y_pred, bin_labels, n_boot)
    cm = gaussian_filter(cm, sigma=1)
    cms.append(cm)

vmin = np.hstack(cms).min()
vmax = np.hstack(cms).max()


for i, (cm, sess_ind, ax) in enumerate(zip(cms, sel_sess_plot, axes.flatten())):

    sns.heatmap(cm,
                xticklabels=10,
                yticklabels=10,
                ax=ax, square=True,
                annot=False, cmap='RdBu_r', cbar=i==0, center=0,
                cbar_kws={'label':'Prob. density - chance'},
                vmin=vmin, vmax=vmax, cbar_ax=None if i else cbar_ax)
    # ax.set_title('Session {}\nMean error:\n{:.2f} cm'.format(sess_ind,
    #              dec_rs[sess_ind]['score']), fontsize=10)
    ax.set_title('Session {}'.format(sess_ind), fontsize=11, y=0.9)

for ax in axes.flatten():
    ax.axis('off')
for ax in axes.flatten():
    ax.xaxis.set_ticks_position('none')
    ax.yaxis.set_ticks_position('none')

f.tight_layout(rect=[0, 0, .85, 1])

plot_name = 'decode_cm_singlesess_{}_{}_{}.{}'.format(decode_modality, area, settings_name,
                                        plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# --- 3. SCATTER DECODING METRIC AS A FUNCTION OF NUMBER OF UNITS -------------

f, ax = plt.subplots(1, 1, figsize=[4, 4])
sns.regplot(data=sess_info, x='n{}'.format(area_code_labels[area]),
            y=score_col, color='m', ax=ax)
sns.despine()
plt.tight_layout()
ax.set_xlabel('Number of units ({})'.format(area))
if decode_modality == 'position':
    ax.set_ylabel('Average decoding error (cm)')
else:
    ax.set_ylabel('Balanced accuracy')

plot_name = 'decode_score_vs_nunits_{}_{}_{}.{}'.format(decode_modality, area, settings_name,
                                        plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# df = pd.DataFrame(columns=['true_bin', 'pred_bin', 'error'])
# df['true_bin'] = y
# df['pred_bin'] = y_pred
# df['error'] = error
#
# f, ax = plt.subplots(1, 1, figsize=[5, 5])
# sns.barplot(data=df, x='true_bin', y='error', ax=ax)
