import os
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *
from plotting_style import *
from sklearn.metrics import confusion_matrix
from scipy.ndimage.filters import gaussian_filter
from utils import make_confusion_matrix_wrt_chance

data_version = 'dec16'
settings_name = 'jan3'
area = 'Hippocampus'

# PLOTTING PARS
plot_format = 'svg'
dpi = 400
save_plots = True
score_threshold_acc = 0.3

# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, 'POS_dec', settings_name)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

for area in ['Hippocampus', 'Perirhinal']:
    # --- LOAD RESULTS ------------------------------------------------------------

    output_folder = os.path.join(REPLAY_FOLDER, 'results', 'dec')
    output_file = 'dec_rs_{}_{}_{}.pkl'.format(settings_name, area, data_version)
    out = pickle.load(open(os.path.join(output_folder, output_file), 'rb'))

    # load variables
    sess_info = out['sess_info']
    dec_rs = out['dec_rs']
    decode_modality = out['pars']['decode_modality']
    bin_labels = out['pars']['bin_labels']
    score_col = 'dec_score_{}'.format(area_code_labels[area])

    if decode_modality == 'leftright':
        ticklabels = ['Left\narm', 'Right\narm']
    elif decode_modality == 'leftmidright':
        ticklabels = ['Left\narm', 'Central\narm', 'Right\narm']


    # --- SELECT SESSIONS ---------------------------------------------------------

    sel_sess_plot = sess_info[sess_info[score_col] >= score_threshold_acc].index.values

    sel_sess_stat = sess_info[~np.isnan(sess_info[score_col])].index.values


    # --- 0. CLASSIFICATION REPORT ------------------------------------------------
    #
    # n_boot = 50
    #
    # from sklearn.metrics import classification_report, accuracy_score
    #
    # cm_deb = []
    # for sess_ind in sel_sess_plot:
    #
    #     cm = dec_rs[sess_ind]['confusion_mat']
    #     y = dec_rs[sess_ind]['y']
    #     y_pred = dec_rs[sess_ind]['y_pred']
    #
    #
    #     print(classification_report(y, y_pred, digits=2))

    # --- CLASSIFICATION IS BETTER THAN CHANCE ------------------------------------

    from sklearn.metrics import balanced_accuracy_score as classification_score

    n_boot = 500

    cm_deb = []

    dfsig = pd.DataFrame(columns=['sess_ind', 'acc_obs', 'acc_deb', 'p_val'])
    for sess_ind in sel_sess_stat:
        print(sess_ind)
        #cm = dec_rs[sess_ind]['confusion_mat']
        y = dec_rs[sess_ind]['y']
        y_pred = dec_rs[sess_ind]['y_pred']

        acc_obs = classification_score(y, y_pred)

        acc_shuf = np.zeros(n_boot)
        for n in range(n_boot):
            acc_shuf[n] = classification_score(y, np.random.permutation(y_pred))

        acc_deb = acc_obs - acc_shuf.mean()
        p_val = (acc_shuf >= acc_obs).sum() / acc_shuf.shape[0]
        dfsig.loc[dfsig.shape[0], :] = [sess_ind, acc_obs, acc_deb, p_val]
    print(dfsig)

    print('position decoding better than chance for {} of {} sessions considered'
          ''.format(dfsig[dfsig['p_val'] < (0.001/dfsig.shape[0])].shape[0], dfsig.shape[0]))

    print('balanced classification of the {} sessions included: {:.2f}, {:.2f}-{:.2f}'.format(
          dfsig.shape[0], dfsig['acc_obs'].quantile(0.5),
            dfsig['acc_obs'].quantile(0.25), dfsig['acc_obs'].quantile(0.75)))




    # y_all = []
    # y_pred_all = []
    # for sess_ind in sel_sess_plot:
    #
    #     #cm = dec_rs[sess_ind]['confusion_mat']
    #     y = dec_rs[sess_ind]['y']
    #     y_pred = dec_rs[sess_ind]['y_pred']
    #     y_all.append(y)
    #     y_pred_all.append(y_pred)
    #
    #
    # y = np.hstack(y_all)
    # y_pred = np.hstack(y_pred_all)
    # acc_obs = classification_score(y, y_pred)
    #
    # acc_shuf = np.zeros(n_boot)
    # for n in range(n_boot):
    #     acc_shuf[n] = classification_score(y, np.random.permutation(y_pred))
    #
    # p_val = (acc_shuf >= acc_obs).sum() / acc_shuf.shape[0]
    print(p_val)

    # --- 1. AVERAGE CONFUSION MATRIX ---------------------------------------------

    n_boot = 500

    cm_deb = []
    for sess_ind in sel_sess_plot:

        #cm = dec_rs[sess_ind]['confusion_mat']
        y = dec_rs[sess_ind]['y']
        y_pred = dec_rs[sess_ind]['y_pred']

        cm_deb.append((make_confusion_matrix_wrt_chance(y, y_pred, None, n_boot)))

        #cm_deb.append(confusion_matrix(y, y_pred, labels=None))
        # cm_shuf = np.zeros_like(cm)
        # for n in range(n_boot):
        #     cm_boot = confusion_matrix(y, np.random.permutation(y_pred))
        #     cm_boot = cm_boot.astype('float') / cm_boot.sum(axis=1)[:, np.newaxis]
        #     cm_shuf += cm_boot
        # cm_shuf = cm_shuf / n_boot
        #
        # cm_deb.append((cm-cm_shuf))


    cm_av = np.sum(cm_deb, axis=0) / len(cm_deb)


    f, ax = plt.subplots(1, 1, figsize=medium_panel_size)
    sns.heatmap(cm_av,
                xticklabels=ticklabels,
                yticklabels=ticklabels,
                ax=ax, square=True,
                annot=True, cmap='RdBu_r', cbar=True, center=0,fmt='.2f',
                cbar_kws={"shrink": 0.55, 'label':'Prob. density - chance'},
                vmin=-0.3, vmax=0.5)
    bins = np.arange(-29, 30)
    bins_to_tick = [-reward_bin, -10, 0, reward_bin, 20]
    for _, spine in ax.spines.items():
        spine.set_visible(True)
    sns.despine(top=True, right=True, offset=8, trim=False)

    #ax.tick_params('both', length=0, width=0, which='major')
    ax.set_ylabel('True location', labelpad=15)
    ax.set_xlabel('Predicted location', labelpad=15)
    plt.tight_layout()

    plot_name = 'decode_cm_averaged_{}_{}_{}.{}'.format(decode_modality, area, settings_name,
                                            plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

    # --- 2. PLOT CONFUSION MATRICES OF SELECTED SESSIONS -------------------------



    n_plots = len(sel_sess_stat)
    n_rows = int(np.ceil(n_plots / 4))
    n_cols = 4


    f, axes = plt.subplots(n_rows, n_cols, figsize=[n_cols * 2.4, n_rows * 2.4],
                           sharex=True, sharey=True)
    for sess_ind, ax in zip(sel_sess_stat, axes.flatten()):

        y = dec_rs[sess_ind]['y']
        y_pred = dec_rs[sess_ind]['y_pred']

        cm_deb = make_confusion_matrix_wrt_chance(y, y_pred, None, n_boot)

        sns.heatmap(cm_deb,
                    xticklabels=ticklabels,
                    yticklabels=ticklabels,
                    ax=ax, square=True,
                    annot=True, cmap='RdBu_r', cbar=False, center=0, fmt='.2f',
                    cbar_kws={"shrink": 0.55, 'label': 'Prob. density - chance'})
        ax.set_title('Session {}\nScore: {:.2f}'.format(sess_ind, dec_rs[sess_ind]['score']),
                     fontsize=10)

    for kk in range(n_plots, n_rows * n_cols):
        print(kk)
        axes.flatten()[kk].axis('off')
    # for ax in axes.flatten():
    #     ax.xaxis.set_ticks_position('none')
    #     ax.yaxis.set_ticks_position('none')

    for _, spine in ax.spines.items():
        spine.set_visible(True)
    sns.despine(top=True, right=True, offset=5, trim=False)
    plt.tight_layout()

    plt.subplots_adjust(wspace=0.5, hspace=0.5)

    plot_name = 'decode_cm__singlesess_{}_{}_{}.{}'.format(decode_modality, area, settings_name,
                                            plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




    # --- 3. SCATTER DECODING METRIC AS A FUNCTION OF NUMBER OF UNITS -------------

    f, ax = plt.subplots(1, 1, figsize=[4, 4])
    sns.regplot(data=sess_info, x='n{}'.format(area_code_labels[area]),
                y=score_col, color='m', ax=ax)
    sns.despine()
    plt.tight_layout()
    ax.set_xlabel('Number of units ({})'.format(area))
    if decode_modality == 'position':
        ax.set_ylabel('Average decoding error (cm)')
    else:
        ax.set_ylabel('Balanced accuracy')

    plot_name = 'decode_score_vs_nunits_{}_{}_{}.{}'.format(decode_modality, area, settings_name,
                                            plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# df = pd.DataFrame(columns=['true_bin', 'pred_bin', 'error'])
# df['true_bin'] = y
# df['pred_bin'] = y_pred
# df['error'] = error
#
# f, ax = plt.subplots(1, 1, figsize=[5, 5])
# sns.barplot(data=df, x='true_bin', y='error', ax=ax)
