import os
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *
from plotting_style import *
from utils import get_decoding_data
from utils import bayesian_cross_validate
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_predict
from utils import load_bin_centers
from utils import prepare_bins, get_position_bin
from utils import coarsify_bins
from utils import compute_euclidean_error
from sklearn.metrics import confusion_matrix
from sklearn.metrics import balanced_accuracy_score
from sklearn.model_selection import StratifiedKFold


data_version = 'dec16'
settings_name = 'jan3'
sess_ind = None
bin_size_train = 400
area = 'Hippocampus'

min_units = 20
min_speed_train = 12
min_spikes_per_sample = 1
min_spikes_per_neuron = 30
pyramidal_only = False
smooth_ratemap = True
smooth_sigma = 1

# DECODING PARS
decoder = 'dkw_decoder'
n_splits = 3

decode_modality = 'leftmidright' # 'leftright', 'leftmidright', 'position'

# --- SET UP PATHS ------------------------------------------------------------

output_folder = os.path.join(REPLAY_FOLDER, 'results', 'dec')
output_file = 'dec_rs_{}_{}_{}.pkl'.format(settings_name, area, data_version)
output_full_path = os.path.join(output_folder, output_file)
# --- DECODE ------------------------------------------------------------------

sess_info = pd.read_pickle(os.path.join(DATA_FOLDER, 'sessions_overview.pkl'))
area_code = area_code_labels[area]
score_col = 'dec_score_{}'.format(area_code)
sess_info[score_col] = np.nan

# select sessions
if sess_ind is None:
    sel_sess = sess_info.loc[sess_info['n{}'.format(area_code)] >= min_units].index.values
else:
    sel_sess = [sess_ind]


nhc = sess_info.loc[sess_info['nHC'] >= 1].shape[0]
npr = sess_info.loc[sess_info['nPR'] >= 1].shape[0]
nbr = sess_info.loc[sess_info['nBR'] >= 1].shape[0]
print('CA1: N={}'.format(nhc))
print('PRH: N={}'.format(npr))
print('S1BF: N={}'.format(nbr))


dec_rs = {}

for sess_ind in sel_sess:

    # load session data
    dec_data = get_decoding_data(sess_ind=sess_ind, data_folder=DATA_FOLDER,
                                 area=area,
                                 data_version=data_version,
                                 binsize=bin_size_train, remove_nans=True,
                                 min_speed=min_speed_train,
                                 min_spikes_per_sample=min_spikes_per_sample,
                                 min_spikes_per_neuron=min_spikes_per_neuron,
                                 pyramidal_only=pyramidal_only)

    X = dec_data['X']
    y = dec_data['y'].astype(int)
    bins = dec_data['bins']
    neuron_sel = dec_data['neuron_sel']


    # --- Get decoding inputs ---
    if decode_modality == 'leftright':
        y, bins = coarsify_bins(y, mode='leftmidright')
        bin_labels = ['Left', 'Right']
        X = X[y!=1]
        y = y[y!=1]

    elif decode_modality == 'leftmidright':
        y, bins = coarsify_bins(y, mode='leftmidright')
        bin_labels = ['Left', 'Mid', 'Right']

    elif decode_modality == 'position':
        rat, day, sess_id = sess_info.loc[sess_ind, ['animal', 'day', 'session']].values
        bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, sess_id)
        bin_centers, bin_labels = prepare_bins(bin_centers_combined)
        bin_labels_cm = np.arange(-29, 30)


    # --- Set up decoder and fit ---
    cv = StratifiedKFold(n_splits=n_splits, shuffle=True,
                         random_state=sess_ind)

    if decoder == 'dkw_decoder':
        y, y_pred = bayesian_cross_validate(X, y, bin_size=bin_size_train,
                                            cv=cv, smooth_ratemap=smooth_ratemap,
                                            smooth_sigma=smooth_sigma)
    else:
        if decoder == 'random_forest':
            dec = RandomForestClassifier(n_estimators=200,
                                         random_state=1)
        elif decoder == 'logistic_regression':
            dec = LogisticRegression(solver='lbfgs', random_state=1)

        y_pred = cross_val_predict(dec, X, y, cv=cv)


    # --- Compute decoding metric ---
    if np.isin(decode_modality, ['leftright', 'leftmidright']):
        score = balanced_accuracy_score(y, y_pred)

    elif decode_modality == 'position':
        score = compute_euclidean_error(y, y_pred, bin_centers, bin_labels).mean()

    # --- Store output ---
    sess_info.loc[sess_ind, score_col] = score
    if decode_modality == 'position':
        # it could be that a bin is missing in y and y_pred, so we force the
        # confusion matrix to always be of the same shape
        cm = confusion_matrix(y, y_pred, labels=bin_labels_cm)
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        cm[np.isnan(cm)] = 0
        assert cm.shape[0] == cm.shape[1] == 59
    else:
        cm = confusion_matrix(y, y_pred)
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    assert np.isnan(cm).sum() == 0

    dec_rs[sess_ind] = {}
    dec_rs[sess_ind]['y'] = y
    dec_rs[sess_ind]['y_pred'] = y_pred
    dec_rs[sess_ind]['score'] = score
    dec_rs[sess_ind]['confusion_mat'] = cm
    if decode_modality == 'position':
        dec_rs[sess_ind]['error'] = error



for col in ['n{}'.format(area_code), score_col]:
    sess_info[col] = pd.to_numeric(sess_info[col])




# --- COLLECT PARS AND SAVE OUTPUT --------------------------------------------

pars = {'settings_name' : settings_name,
        'bin_size_train' : bin_size_train,
        'area' : area,
        'min_units' : min_units,
        'min_speed_train' : min_speed_train,
        'min_spikes_per_sample' : min_spikes_per_sample,
        'min_spikes_per_neuron' : min_spikes_per_neuron,
        'pyramidal_only' : pyramidal_only,
        'decoder' : decoder,
        'n_splits' : n_splits,
        'decode_modality' : decode_modality,
        'sel_sess' : sel_sess,
        'bin_labels' : bin_labels}


out = {'pars' : pars,
       'sess_info' : sess_info,
       'dec_rs' : dec_rs}


if not os.path.isdir(output_folder):
    os.makedirs(output_folder)

print('Saving output to {}'.format(output_full_path))
pickle.dump(out, open(output_full_path, 'wb'))


