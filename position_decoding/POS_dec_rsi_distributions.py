from utils import get_RSI
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from plotting_style import *
import pandas as pd
from constants import *
from scipy.stats import mannwhitneyu

sessions = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]

dpi = 400
plot_format = 'png'

plot_folder = os.path.join(PLOTS_FOLDER, 'RSI')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

barrel, peri = [], []
for sess_ind in sessions:

    br = get_RSI(sess_ind=sess_ind, area='Barrel')
    pr = get_RSI(sess_ind=sess_ind, area='Perirhinal')
    barrel.append(br)
    peri.append(pr)


barrel = np.hstack(barrel)
peri = np.hstack(peri)

print('RSI - barrel: {:.2f}, {:.2f}-{:.2f}'.format(
    np.quantile(barrel, 0.5),
    np.quantile(barrel, 0.25),
    np.quantile(barrel, 0.75)))

print('RSI - perirhinal: {:.2f}, {:.2f}-{:.2f}'.format(
    np.quantile(peri, 0.5),
    np.quantile(peri, 0.25),
    np.quantile(peri, 0.75)))

stat, mannp = mannwhitneyu(barrel, peri)
print('mannwhitney: p={:.2e}'.format(mannp))



df1 = pd.DataFrame(columns=['area', 'RSI'])
df1['RSI'] = barrel
df1['area'] = 'Barrel'

df2 = pd.DataFrame(columns=['area', 'RSI'])
df2['RSI'] = peri
df2['area'] = 'Perirhinal'

df = pd.concat([df1, df2])

f, ax = plt.subplots(1, 1)
sns.distplot(barrel, ax=ax, kde=True,
             color=area_palette['Barrel'], norm_hist=True)
sns.distplot(peri, ax=ax, kde=True,
             color=area_palette['Perirhinal'], norm_hist=True)
sns.despine()
plt.tight_layout()


f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
sns.swarmplot(data=df, y='RSI', x='area', ax=ax,
             palette=area_palette, size=2.5)
ax.set_ylabel('RSI index')
ax.set_xlabel('')
sns.despine()
plt.tight_layout()

plot_name = 'rsi_swarm.{}'.format(plot_format)
plt.draw()
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
