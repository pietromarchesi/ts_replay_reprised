## TODO

<<<<<<< Updated upstream
=======

0. In the decoded events, threshold speed a bit more, cause some
trajectories look pretty flat.
>>>>>>> Stashed changes

2. Set up pipeline to make PBEs and decode them.
[DONE] Run the decoding of position for Hippocampus and select sessions with
good enough decoding.
[DONE] Adapt PBEs_make.py so it can run for a list of sessions, and generate
the PBEs for all the selected sessions.
[DONE] Adapt a slurm script which runs decode_PBEs_DKW.py
[DONE] Put the PBEs on the cluster to test the slurm script
- Download the results, make sure everything worked out, and start
developing the analysis a bit further.

3. Replay ratemaps: subselection of units. Previously we were doing
subselection based on 3 criteria
- AUC score: how much does the Perirhinal/Barrel unit distinguish between
the two arms?
- Upmodulation doing hippocampal PBEs: does the unit tend to increase
firing when a hippocampal pbe occurs?

4. Fit lines has been rewritten. Looks fine but double check.
NOTE: now that we know the average distance between bins, we can
reason how many bins to include in terms of cm.

5. Find a way to statistically test whether replay events are
more skewed towards current or previous trial.

6. Now ReplayDetector knows time in seconds, the plotting should be
updated (time on x axis), and probably we can get rid of the
compatibility with the case in which you don't know times and bin distance

7. Look if cells in peri/barrel which have good real/event ratemap
similarity are focused on parts of the maze.

8. In replay ratemaps, look at is_pyramidal: currently we compute ratemaps
for all Peri/Barrel units. is_pyramidal is a key of the output
of unpack_session so we could easily subselect, but no strict reason for now.



# LOW PRIORITY ITEMS
1. Single unit approach: for every unit, we see whether it fires more
in which arm of the maze, and we see whether it fires more during left or
right decoded Hippocampal events. We can restrict the event to the ones
occurring on the central arm to avoid confounds. This could be sort of
a coarse grained version of the replay ratemaps
1. When we create the combinations of bins to fit our lines, we could
exclude the pairs in which both bins fall outside the actual bins,
because those cases may give rise to situations in which the full line
falls in the padded region. However since we wrap the bins, this is not
a problem, it could just avoid fitting unnecessary lines.
3. Replay ratemaps: axis y label: normalized ratmap?
0. correlate_ratemaps takes a smooth parameter which defaults to False,
which can be used to smooth the ratempas BEFORE computing the correlation.
Think if this is a good idea or not. So far: if you smooth in the plot,
we also smooth for the correlation.
12. Explained variance script is not finished.
12. In replay detector, we should add functions to compute the 6 measures
of sequence structure introduced "Trajectory events across hippocampal
place cells require previous experience": we are missing only
- Weighted correlation: this may be a bit tricky because of the
circular structure.
0. Test if numba could make things faster (e.g. line fitting).
0. To look at the match between Hippocampal and Barrel: Barrel
has really only two sessions with more than 10 units, 1 and 16, in session
1 Hippocampus has decent but not really good decoding. I could add that
session in (make PBEs, run DKW decoding), but then it would still only
be just one session, so probably not event worth the effort.



## DONE
0. Write scripts to plot individual events (ideally: a decoded hippocampal
pbe together with a matching perirhinal pbe) [done for hippocampus]
0. Create new dataframe with session information
1. We need to add to the PBEs an indication of the __trial phase__ in which
it occurred (if we are in sleep we can just put 'nophase'). We can also
add a column to the df that indicates task vs sleep if we later need
to stack multiple dfs.
2. Need to add __animal position__ to the PBEs df. If sleep we insert
missing values.
3. Add trial side in PBEs_make, so we can directly compare with the side
of the decoded PBE.
4. In the PBEs df we have the position but it would be handy
to have the linearized bin, the problem is that we need the trial
side for that, which is not handy to access.
5. Convert speed threshold to cm / s. It was already in cm/s, because
we convert position to centimeters when we make the neo blocks,
and position is computed using position in cms.
6. In decode_PBEs_DKW I introduced a function to wrap the bins and
figure out the direction of the trajectory replay.
The replay detector will hand us bins which are not necessarily between
(0, 29) for right side pbes and (-29, 0) for left side pbes. This is
because of the wrapping that we do when fitting trajectory lines. The
difference between these bins allows us to calculate how many bins the
trajectory spans, thus we can easily compute a trajectory speed, as is
done in replaydetector. However, when we look at for example where the
trajectories start and end, we need to bring back these additional bins
to the standard ranges (i.e. 'wrap' them). By doing that, however, we lose
the information about whether the trajectory goes in the same direction
as a normal trial run or it goes backwards, so we also determine that
(which is also a bit different for the two pbe sides because of how the
bins are defined).
7. Check again that replay_wrap_bins_and_direction is doing what you
want it to do.
8. Changed the dataframes with the p-values of the decoded PBEs
such that it has the same index and we can join them
2. Once we have the decoded PBEs, we need to filter them before we
characterize them. We could inherit the select_pbes function, maybe call
it filter pbes
5. Estimate a compression factor. Calculate the average running
speed and compare with average speed in replay events.
1. In other/test_fit_lines.py I started looking again at the fit_lines
function. It seems like the padding was not actually done perfectly,
the amount of padding should probably be n_bins_ext and not n_bins_proba,
which is the number of bins around the line which we take into account.
Look back at it and fix it when you're smart.
1. ReplayDetector should know time in seconds, not in bins
7. In replay detector, we should add functions to compute the 6 measures
of sequence structure introduced "Trajectory events across hippocampal
place cells require previous experience"
- Replay score (now that we know the average distance between bins,
we can report that we accumulate probability around x cm from the fitted
line)
- Maximum jump distance. Outputs distance in cm
- Sharpness: maximum decoded posterior probability for each candidate
event
6. In the replay detector, you could add a function to plot the
replay with the padded y_proba, so we see better what trajectory
we are picking up
1. When we combine multiple dataframes of decoded pbes, the event_number
is no longer unique: reindex. Reindexing is more complicated for the
sig_df dataframe
2. [BUG] in replay_ratemaps.py we geneate the trajectory and save it
in decode_PBEs_DKW, but then when we load the data of a second area and
bin in replay_ratemaps, the shape of the binned PBE spikes don't match
the trajectory. Why? [SOLVED] because in the main decode_PBEs_DKW loop
I was indexing a list of binned PBEs with a PBE index which was more like
an id (did not start at zero). Now I put the binned_pbes in a dict
so I can access using that key.
2. Replay ratemaps: shift in time (replay_ratemaps.shift.py)
4. Replay ratemaps: aggregate across sessions!!
4. In replay ratemaps, set a speed threshold to compute real ratemaps
0. Test decode_PBEs_LR.py and the script to compare decoded side between
Hippocampus and Perirhinal. Seems to work for one session, have not tested
the part where we aggregate sessions.
1. Set up normal decoding of position (or left/right arm) for
Hippocampus (and Peri/Barrel/V1). For the Hippocampus, we need for every
bin to get its x y position and compute the euclidean distance to get
the error.
1. decode.py script: save decoding results with parameters.
1. Look back at Davidson 2009 for how they exclude flat trajectories to
apply in decode_PBEs_DKW.py
- Not found
- In the hippocampus-enthorinal paper: exclude trajectories with less
than 4 bins and speed of less than 200cm/s - we exclude 150 cm/s in the
computation,we can always increase later
3. Write load_dec_merge.py: load decoding results of Hippocampus and
Perirhinal and figure out in which we have decent decoding in both areas.
0. PBEs are not sorted by their start time in the df that we load, why??
Fixed, it was because computing the set intersections did not return
a sorted output.
1. Decide session inclusion criteria
- Sessions for hippocampal replay (TASK)
- Sessions for hippocampal replay (SLEEP): subset of the above with
enough sleep)
- Sessions for side matching between hippocampus and peri/barrel replay
(TASK): determined jointly from decoding scores
- Sessions for side matching (SLEEP): subset of the above with enough
sleep
0. In the replay ratemaps, if you have no firing during PBEs, you get a
p-value of zero!
0. Write a script in which we just load the hippocampal PBEs and we
figure out how many there are, how many are significant, etc.
[DONE]: quantify_hc_replay.py
1. Run replay again on cluster with a few more surrogates and all
shuffle types. Before doing that, fix a few things, namely:
- sig_df needs to be made to have numeric columns
0. compare_side_across_area: filter significant PBEs
1. Adapt compare_side_across_areas.py so it loads all wanted sessions
and aggregates the side matching, with the possibility to still show it
per session.
0. Figure out a way to plot the trajectory line so that it wraps:
for now we just plot the padded y_proba, much quicker
0. Adjusting event to first and last spike seems to not work perfectly
all the time
What is happening is that I determined the events using MUA with all cells,
then adjust the bounds using again all cells, and later select only pyramidal,
that's why I don't always see spikes in the first/last bins.
[Solution]: I added a parameter called adjust_bounds_only_selected_cells,
so that you can specify if you want to adjust bounds based on all cells
or only selected. This can break though: if I define a MUA based on all
cells, and then adjust the bounds based on pyramidal cells, it may be
that no pyramidal cell actually fires, and so how do you adjust the bounds?
You get an error. So in the end mua_only_selected_cells and
adjust_bounds_only_selected_cells should be the same.
0. Run again decoding on PBEs july8 in which we determine the candidate
PBEs based on pyramidal activity alone, and adjust the bounds to
pyramidal spikes [RUNNING]
1. [SLEEP]:
- Discuss with Cyrel if I can avoid it.
- I don't like the chosen periods cause I don't know how they
were determined and how they were equalized: can I access the raw data
and just take the time where the animal is not moving at all? What
I would want to do then is concatenate periods of no movement which are
at least n seconds. sleepdata.mat has movement and time, but time is
not in timestamps, it is in seconds, so I probably need the first time stamp
to convert so I can match with the spike times. The firstts is not there
but maybe it's the same for the whole session so I can get it in spikeTrl.mat
- In prepare_sess_task_neo.py, we put in all the data from the
beginning of the first SWS period to the end of the last SWS period.
But, when actually need to use that data (i.e. when we bin it), we should
take into account only the spikes in the SWS periods (which we have
in the events). The same thing also a bit applies to task data if I
clean it up from nans.
0. Quite some PBEs are not really left nor right but start and end in
the central arm. Spot those from the start and stop position and call
them central PBEs. We could exclude them when we look at trial matching
0. Figure out which bins are the walls / reward sites, add it to
plot_maze
0. Replay ratemaps: clean up, compute metrics per unit, correlate
ratemaps with the single unit metrics,
0. For the Hippocampal decoding you have to make the single sessions
also into prob. - chance