import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import quantities as pq
from sklearn.preprocessing import LabelBinarizer
from utils import plot_maze
from sklearn.metrics import confusion_matrix
from matplotlib.lines import Line2D
from utils import is_on

def plot_decoded_position(y_test, y_pred, bin_size=None):
    xvals = np.arange(y_pred.shape[0])

    f, ax = plt.subplots(1, 1, figsize=[6, 4])
    ax.scatter(xvals, y_pred, s=4, zorder=3)
    ax.plot(xvals, y_test, c=sns.color_palette()[1])
    if bin_size is not None:
        ax.set_xticklabels((ax.get_xticks() * bin_size.rescale(pq.ms).item() / 1000).astype(int))
        ax.set_xlabel('Time [s]')
    else:
        ax.set_xlabel('Time bin')
    ax.set_ylabel('Spatial bin')
    sns.despine(f, top=True, right=True)
    return f




def plot_confusion_matrix(y_test, y_pred, bins=None, annot=False):
    if bins is None:
        bins = np.unique(np.hstack((y_test, y_pred)))
    cm = confusion_matrix(y_test, y_pred)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    f, ax = plt.subplots(1, 1, figsize=[12,12])
    sns.heatmap(cm,
                xticklabels=bins.astype(int),
                yticklabels=bins.astype(int),
                ax=ax, square=True,
                annot=annot, cmap='Blues')
    ax.set_xlabel('Bin number')
    ax.set_ylabel('Bin number')

    return f



def plot_trajectory(time_bins, y_pred):
    f, ax = plt.subplots(1, 1, figsize=[8, 6])
    ax.axhspan(-29, 0, alpha=0.1, color=sns.color_palette()[0])
    ax.axhspan(0, 29, alpha=0.1, color=sns.color_palette()[2])
    ax.plot(time_bins, y_pred)
    ax.set_xticks(time_bins)

    ax.set_yticks(np.arange(-29, 30, 2))
    ax.scatter(time_bins, y_pred)
    ax.hlines([0, 19, -19, 24, -24], *ax.get_xlim(), color='k', linestyle=':', alpha=0.2)

    ax.set_ylim([-29, 30])
    sns.despine(f, top=True, right=True)

    return f


def plot_conf_matrix(pbes, only_selected=False, only_central_arm=False,
                      p_val_threshold=0.05,
                      shuffle_type='column_cycle_shuffle',
                      conf_quantile=0.5):
    pass




def _plot_confusion_matrix_side_decoded(ax, trial_side, pbe_side, match_with,
                                        vmin=0, vmax=1, axisoff=False):
    if match_with == 'current_trial_side':
        match_with_str = 'current'
    elif match_with == 'previous_trial_side':
        match_with_str = 'previous'

    cm = confusion_matrix(trial_side, pbe_side)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    if axisoff:
        cbar = False
    else:
        cbar = True
    sns.heatmap(cm,
                xticklabels=['Left', 'Right'],
                yticklabels=['Left', 'Right'],
                ax=ax, square=True,
                annot=True,
                cmap='Blues',
                vmin=vmin, vmax=vmax,
                cbar=cbar)
    ax.set_ylabel('{} trial side'.format(match_with_str.capitalize()))
    ax.set_xlabel('Decoded PBE side')

    if axisoff:
        ax.set_ylabel('')
        ax.set_yticks([])
        ax.set_xticks([])



def plot_confusion_matrix_side_decoded(ax, pbes, match_with='current_trial_side',
                                       only_selected=False,
                                       only_central_arm=False,
                                       p_val_threshold=0.05,
                                       shuffle_type='column_cycle_shuffle',
                                       conf_quantile=0.5):

    out = select_pbe_info(pbes, only_selected=only_selected,
                          only_central_arm=only_central_arm,
                          p_val_threshold=p_val_threshold,
                          shuffle_type=shuffle_type,
                          conf_quantile=conf_quantile)
    current_trial_side = out['current_trial_side']
    previous_trial_side = out['previous_trial_side']
    pbe_side = out['pbe_side']


    if match_with == 'current_trial_side':
        trial_side = current_trial_side
    elif match_with == 'previous_trial_side':
        trial_side = previous_trial_side

    _plot_confusion_matrix_side_decoded(ax, trial_side, pbe_side,
                                        match_with = match_with)




def plot_replay_panel(pbes, only_selected=False, only_central_arm=False,
                      only_phase=False,
                      p_val_threshold=0.05,
                      shuffle_type='column_cycle_shuffle',
                      conf_quantile=0.5,
                      phase='iti'):

    out = select_pbe_info(pbes, only_selected=only_selected,
                          only_central_arm=only_central_arm,
                          only_phase=only_phase,
                          p_val_threshold=p_val_threshold,
                          shuffle_type=shuffle_type,
                          conf_quantile=conf_quantile,
                          phase=phase)
    current_trial_side = out['current_trial_side']
    previous_trial_side = out['previous_trial_side']
    pbe_side = out['pbe_side']
    score = out['score']
    xpos  = out['xpos']
    ypos  = out['ypos']

    if only_selected or only_central_arm or only_phase:
        xpos_exc = out['xpos_exc']
        ypos_exc = out['ypos_exc']
        score_exc = out['score_exc']

    match_with_modes = ['current_trial_side', 'previous_trial_side']

    # binarize labels
    le = LabelBinarizer()
    le.fit(current_trial_side)
    current_trial_side = le.transform(current_trial_side)[:, 0]
    previous_trial_side = le.transform(previous_trial_side)[:, 0]
    pbe_side = le.transform(pbe_side)[:, 0]


    # --- START MAIN PLOT ---

    f = plt.figure(figsize=(16, 8))
    ax1 = plt.subplot2grid((2, 4), (0, 0), colspan=2, rowspan=2)
    ax2 = plt.subplot2grid((2, 4), (0, 2))
    ax3 = plt.subplot2grid((2, 4), (1, 2))
    ax4 = plt.subplot2grid((2, 4), (0, 3))
    ax5 = plt.subplot2grid((2, 4), (1, 3))
    cm_axes = [ax2, ax3]
    maze_axes = [ax4, ax5]


    # plot
    ax = ax1
    plot_maze(ax)
    ind_col = pbe_side == 0
    colors = [sns.xkcd_rgb['red'] if t else sns.xkcd_rgb['blue'] for t in ind_col]
    sizes  = (600 * score).astype(int)

    ax.scatter(xpos, ypos, c=colors, s=sizes, zorder=10, edgecolors='k')
    if only_selected or only_central_arm or only_phase:
        sizes = (600 * score_exc).astype(int)
        ax.scatter(xpos_exc, ypos_exc, c='grey', s=sizes, zorder=10,
                   edgecolors='k', alpha=0.5)

    legend_elements = [Line2D([0], [0], marker='o', markerfacecolor=sns.xkcd_rgb['red'],
                              label='Decoded left',
                              color='w', markersize=12),
                       Line2D([0], [0], marker='o', markerfacecolor=sns.xkcd_rgb['blue'],
                              label='Decoded right',
                              color='w', markersize=12)]
    ax.legend(handles=legend_elements, loc='lower right')
    ax.axis('off')

    # SECONDARY PLOTS
    for i, match_with in enumerate(match_with_modes):

        if match_with == 'current_trial_side':
            match_with_str = 'current'
            trial_side = current_trial_side
        elif match_with == 'previous_trial_side':
            match_with_str = 'previous'
            trial_side = previous_trial_side

        _plot_confusion_matrix_side_decoded(cm_axes[i], trial_side, pbe_side,
                                            match_with = match_with)

        # PLOT SCATTER
        ax = maze_axes[i]
        plot_maze(ax)

        ind = trial_side == pbe_side
        colors = [sns.xkcd_rgb['green'] if t else sns.xkcd_rgb['red'] for t in ind]
        sizes = (400 * score).astype(int)

        ax.scatter(xpos, ypos, c=colors, s=sizes, zorder=10, edgecolors='k')

        legend_elements = [
            Line2D([0], [0], marker='o', markerfacecolor=sns.xkcd_rgb['green'],
                   label='Match with {} trial side'.format(match_with_str),
                   color='w', markersize=12),
            Line2D([0], [0], marker='o', markerfacecolor=sns.xkcd_rgb['red'],
                   label='No match with {} trial side'.format(match_with_str),
                   color='w', markersize=12)]
        ax.legend(handles=legend_elements, loc='lower right')
        ax.axis('off')

    return f




def plot_replay_panel(pbes, exc_pbes=None):

    current_trial_side = pbes['current_trial_side']
    previous_trial_side = pbes['previous_trial_side']
    pbe_side = pbes['pbe_side']
    xpos  = pbes['mean_xpos']
    ypos  = pbes['mean_ypos']

    try:
        score = pbes['side_confidence']
    except KeyError:
        try:
            score = pbes['replay_score']
        except KeyError:
            score = 0.15 * np.ones(pbes.shape[0])

    match_with_modes = ['current_trial_side', 'previous_trial_side']

    # binarize labels
    le = LabelBinarizer()
    le.fit(current_trial_side)
    current_trial_side = le.transform(current_trial_side)[:, 0]
    previous_trial_side = le.transform(previous_trial_side)[:, 0]
    pbe_side = le.transform(pbe_side)[:, 0]


    # --- START MAIN PLOT ---

    f = plt.figure(figsize=(16, 8))
    ax1 = plt.subplot2grid((2, 4), (0, 0), colspan=2, rowspan=2)
    ax2 = plt.subplot2grid((2, 4), (0, 2))
    ax3 = plt.subplot2grid((2, 4), (1, 2))
    ax4 = plt.subplot2grid((2, 4), (0, 3))
    ax5 = plt.subplot2grid((2, 4), (1, 3))
    cm_axes = [ax2, ax3]
    maze_axes = [ax4, ax5]


    # plot
    ax = ax1
    plot_maze(ax)
    ind_col = pbe_side == 0
    colors = [sns.xkcd_rgb['red'] if t else sns.xkcd_rgb['blue'] for t in ind_col]
    sizes  = (600 * score).astype(int)

    ax.scatter(xpos, ypos, c=colors, s=sizes, zorder=10, edgecolors='k')
    if exc_pbes is not None and exc_pbes.shape[0] > 0:

        try:
            score_exc = exc_pbes['side_confidence']
        except KeyError:
            try:
                score_exc = exc_pbes['replay_score']
            except KeyError:
                score_exc = 0.15 * np.ones(pbe_side.shape[0])

        sizes = (600 * score_exc).astype(int)
        ax.scatter(exc_pbes['mean_xpos'], exc_pbes['mean_ypos'], c='grey', s=sizes, zorder=10,
                   edgecolors='k', alpha=0.5)

    legend_elements = [Line2D([0], [0], marker='o', markerfacecolor=sns.xkcd_rgb['red'],
                              label='Decoded left',
                              color='w', markersize=12),
                       Line2D([0], [0], marker='o', markerfacecolor=sns.xkcd_rgb['blue'],
                              label='Decoded right',
                              color='w', markersize=12)]
    ax.legend(handles=legend_elements, loc='lower right')
    ax.axis('off')

    # SECONDARY PLOTS
    for i, match_with in enumerate(match_with_modes):

        if match_with == 'current_trial_side':
            match_with_str = 'current'
            trial_side = current_trial_side
        elif match_with == 'previous_trial_side':
            match_with_str = 'previous'
            trial_side = previous_trial_side

        _plot_confusion_matrix_side_decoded(cm_axes[i], trial_side, pbe_side,
                                            match_with = match_with)

        # PLOT SCATTER
        ax = maze_axes[i]
        plot_maze(ax)

        ind = trial_side == pbe_side
        colors = [sns.xkcd_rgb['green'] if t else sns.xkcd_rgb['red'] for t in ind]
        sizes = (400 * score).astype(int)

        ax.scatter(xpos, ypos, c=colors, s=sizes, zorder=10, edgecolors='k')

        legend_elements = [
            Line2D([0], [0], marker='o', markerfacecolor=sns.xkcd_rgb['green'],
                   label='Match with {} trial side'.format(match_with_str),
                   color='w', markersize=12),
            Line2D([0], [0], marker='o', markerfacecolor=sns.xkcd_rgb['red'],
                   label='No match with {} trial side'.format(match_with_str),
                   color='w', markersize=12)]
        ax.legend(handles=legend_elements, loc='lower right')
        ax.axis('off')

    return f




def combine_pbes_two_areas(pbes, match_mode, max_time_diff_in_ms=None):

    """
    pbes is a dict with the two areas as keys

    match_mode can be same_events ->
    """

    areas = list(pbes.keys())

    # --- Match events ------------------------------------------------------------

    pbes_df = pd.DataFrame(columns=['ind_{}'.format(areas[0]),
                                    'ind_{}'.format(areas[1]),
                                    'score_{}'.format(areas[0]),
                                    'score_{}'.format(areas[1]),
                                    'start_time_{}'.format(areas[0]),
                                    'start_time_{}'.format(areas[1]),
                                    'side_{}'.format(areas[0]),
                                    'side_{}'.format(areas[1])])

    for a1_ind, pbe in pbes[areas[0]]['info'].iterrows():

        if match_mode == 'same_events':
            # If the same PBEs times are used, simply use the same index
            a2_ind = a1_ind

        else:
            # otherwise match
            a1_start = pbe['start_time']
            a1_end = pbe['start_time']

            a2_start = pbes[areas[1]]['info']['start_time']
            a2_end = pbes[areas[1]]['info']['end_time']

            if match_mode == 'nearest_start':
                diff = a2_start - a1_start
                a2_ind = diff.abs().argmin()

            elif match_mode == 'nearest_start_following':
                diff = a2_start - a1_start
                try:
                    a2_ind = diff[diff > 0].argmin()
                except ValueError:
                    print(
                        'No PBEs in area 2 following PBE {} of area 1: skipping'.format(
                            a1_ind))
                    continue

            elif match_mode == 'nearest_start_preceding':
                diff = a2_start - a1_start
                try:
                    a2_ind = np.abs(diff[diff < 0]).argmin()
                except ValueError:
                    print('No PBEs in area 2 preceding PBE {} of area 1: skipping'.format(a1_ind))
                    continue

        try:
            pbe2 = pbes[areas[1]]['info'].loc[a2_ind, :]
        except KeyError:
            print(
                'PBE {} in area {} has been removed. Skipping.'.format(a2_ind,
                                                                       areas[
                                                                           1]))
            continue

        pbes_match = {areas[0]: pbe,
                      areas[1]: pbe2}

        for area in areas:

            pbes_df.loc[a1_ind, 'ind_{}'.format(area)] = pbes_match[area].name
            pbes_df.loc[a1_ind, 'start_time_{}'.format(area)] = pbes_match[area]['start_time']
            pbes_df.loc[a1_ind, 'side_{}'.format(area)] = pbes_match[area][
                'pbe_side']

            try:
                if area == 'Hippocampus':
                    key = 'replay_score'
                elif area == 'Perirhinal':
                    key = 'side_confidence'
                score = pbes_match[area][key]
            except KeyError:
                score = np.nan

            pbes_df.loc[a1_ind, 'score_{}'.format(area)] = score

    pbes_df['start_time_diff'] = pbes_df['start_time_{}'.format(areas[1])] - \
                                 pbes_df['start_time_{}'.format(areas[0])]

    if match_mode == 'nearest_start_following':
        if not np.all(pbes_df['start_time_diff'] >= 0):
            raise ValueError
    elif match_mode == 'nearest_start_preceding':
        if not np.all(pbes_df['start_time_diff'] <= 0):
            raise ValueError

    # Select to avoid null entries

    pbes_sel = pbes_df.loc[(~pd.isnull(pbes_df['side_{}'.format(areas[0])]))
                           & (~pd.isnull(pbes_df['side_{}'.format(areas[1])])),:].copy()

    # select for significance

    if max_time_diff_in_ms is not None:
        pbes_sel = pbes_sel[pbes_sel['start_time_diff'] < max_time_diff_in_ms]

    return pbes_sel






def select_pbe_info(pbes, only_selected=False, only_central_arm=False,
                    only_phase=False,
                      p_val_threshold=0.05,
                      shuffle_type='column_cycle_shuffle',
                      conf_quantile=0.5,
                      phase='iti'):

    if (only_central_arm and only_phase) or (only_central_arm and only_selected):
        raise ValueError('This selection option is not yet supported')

    area = pbes['decode_pars']['area']

    # extract data from dataframe
    pbe_side = pbes['info'].loc[:, 'pbe_side']
    if area == 'Perirhinal':
        score = pbes['info'].loc[:, 'side_confidence']
    elif area == 'Hippocampus':
        try:
            score = pbes['info'].loc[:, 'replay_score'].as_matrix()
        except KeyError:
            score = 0.15 * np.ones(pbe_side.shape[0])

    xpos = pbes['info']['mean_xpos']
    ypos = pbes['info']['mean_ypos']
    current_trial_side = pbes['info'].loc[:, 'current_trial_side']
    previous_trial_side = pbes['info'].loc[:, 'previous_trial_side']

    # Exclude nans and low confidence
    ind1 = ~np.array(pd.isnull(current_trial_side))
    ind2 = ~np.array(pd.isnull(previous_trial_side))
    ind3 = ~np.array(pd.isnull(pbe_side))

    ind = np.logical_and(np.logical_and(ind1, ind2), ind3)  # some PBEs may not have any samples with enough spikes

    if only_selected:
        if area == 'Perirhinal':
            ind4 = score > score.quantile(conf_quantile)

        elif area == 'Hippocampus':
            sig_df = pbes['sig_df'][pbes['sig_df']['shuffle_type'] == shuffle_type]
            sig_pbes_ind = sig_df.PBE_ind[sig_df.p_val <= p_val_threshold]
            ind4 = np.isin(pbes['info'].index, sig_pbes_ind)

        if only_phase:
            ind5 = pbes['info']['trial_phase'] == phase
            ind4 = np.logical_and(ind4, ind5)


    if only_central_arm:
        on_center_ind = []
        for row in pbes['info'].itertuples():
            on_center = is_on((row.mean_xpos, row.mean_ypos), 'center', margin=7)
            if on_center:
                on_center_ind.append(row.Index)

        ind4 = np.isin(pbes['info'].index, on_center_ind)

    if not only_selected and only_phase:
        ind4 = pbes['info']['trial_phase'] == phase


    if only_selected or only_central_arm or only_phase:
        excluded_ind = np.logical_and(ind, ~ind4)
        ind = np.logical_and(ind, ind4)
        xpos_exc = xpos[excluded_ind]
        ypos_exc = ypos[excluded_ind]
        score_low = score[excluded_ind]

    current_trial_side = current_trial_side[ind]
    previous_trial_side = previous_trial_side[ind]
    pbe_side = pbe_side[ind]
    score = score[ind]
    xpos = xpos[ind]
    ypos = ypos[ind]

    out = {'index' : pbes['info'].index[ind],
           'current_trial_side' : current_trial_side,
           'previous_trial_side' : previous_trial_side,
           'pbe_side' : pbe_side,
           'score' : score,
           'xpos' : xpos,
           'ypos' : ypos}

    if only_selected or only_central_arm or only_phase:
        out['xpos_exc'] = xpos_exc
        out['ypos_exc'] = ypos_exc
        out['score_exc'] = score_low

    return out


def select_pbes(pbes, only_selected_score=False, only_central_arm=False,
               only_phase=False, only_significant=True,
               phase='iti',
               score_quantile=0.5,
               p_val_threshold=0.05,
               shuffle_type='column_cycle_shuffle',
               return_excluded=True):

    area = pbes['decode_pars']['area']
    df = pbes['info']

    included_index = {}

    if only_phase:
        ind = df.index[pbes['info']['trial_phase'] == phase]
        included_index['phase'] = ind

    if only_central_arm:
        ind = []
        for row in pbes['info'].itertuples():
            on_center = is_on((row.mean_xpos, row.mean_ypos), 'center', margin=7)
            if on_center:
                ind.append(row.Index)
        included_index['central_arm'] = pd.Index(ind)

    if only_selected_score:
        if area == 'Perirhinal':
            score = 'side_confidence'
        elif area == 'Hippocampus':
            score = 'replay_score'

        ind = df.index[df[score] > df[score].quantile(score_quantile)]
        included_index['score'] = ind


    if only_significant:
        if area == 'Perirhinal':
            raise ValueError('No significance for perirhinal replays')
        elif area == 'Hippocampus':
            sig_df = pbes['sig_df'][pbes['sig_df']['shuffle_type'] == shuffle_type]
            ind = sig_df.PBE_ind[sig_df.p_val <= p_val_threshold]
        included_index['significant'] = ind


    ind1 = ~np.array(pd.isnull(df['current_trial_side']))
    ind2 = ~np.array(pd.isnull(df['previous_trial_side']))
    ind3 = ~np.array(pd.isnull(df['pbe_side']))
    ind = np.logical_and(np.logical_and(ind1, ind2), ind3)
    included_index['not_null'] = df.index[ind]

    sets = [set(included_index[i].tolist()) for i in included_index.keys()]
    intersection = list(set.intersection(*sets))

    excluded_ind = set(df.index.tolist()).difference(set(intersection))
    excluded_ind = set.intersection(excluded_ind, set(included_index['not_null'].tolist()))

    if return_excluded:
        return df.loc[intersection], df.loc[excluded_ind]
    else:
        return df.loc[intersection]