import os
from utils import *
from constants import *
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *

data_version = 'dec16'
sessions = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
speed_thr = 12
binsize = 50
plots_format = 'png'
plots_dir = os.path.join(PLOTS_FOLDER, plots_format, 'position_and_speed_smoothed_100')

if not os.path.isdir(plots_dir):
    os.makedirs(plots_dir, exist_ok=True)


position_sigma = 10

for sess_ind in sessions:

    out = unpack_session(sess_ind, DATA_FOLDER, 'Hippocampus',
                         data_version=data_version, binsize_in_ms=binsize,
                         remove_nans=True,
                         interp_and_smooth_speed=False,
                         speed_sigma=2)

    pos = out['position']
    speed = out['speed']
    times = out['times']

    # pos, times = get_position(data_folder=DATA_FOLDER,
    #                           data_version=data_version,
    #                           sess_ind=sess_ind)

    print(np.isnan(pos).sum())

    xpos     = scipy.ndimage.filters.gaussian_filter1d(pos[:, 0], sigma=position_sigma)
    ypos     = scipy.ndimage.filters.gaussian_filter1d(pos[:, 1], sigma=position_sigma)

    xy = np.hstack((xpos[:, None], ypos[:, None]))
    a = xy[:-1]
    b = np.roll(xy, -1, axis=0)[:-1]
    dxy = np.linalg.norm(a - b, axis=1)
    smooth_speed = np.divide(dxy, np.diff(times).rescale(pq.s))

    nsamp = 20000

    t = times.rescale(pq.min) - times[0].rescale(pq.min)
    f, ax = plt.subplots(4, 1, figsize=[8, 12], sharex=True)
    ax[0].scatter(t[0:nsamp], pos[:nsamp, 0], s=1, c=sns.xkcd_rgb['blue'], label='X moving')
    ax[0].scatter(t[0:nsamp], pos[:nsamp, 1], s=1, c=sns.xkcd_rgb['green'], label='Y moving')

    ax[1].scatter(t[0:nsamp], xpos[:nsamp], s=1, c=sns.xkcd_rgb['blue'], label='X moving')
    ax[1].scatter(t[0:nsamp], ypos[:nsamp], s=1, c=sns.xkcd_rgb['green'], label='Y moving')

    ax[2].scatter(t[0:nsamp], speed[:nsamp], s=1, c='grey')
    ax[2].axhline(12, ls=':', c='r')
    ax[2].axhline(4, ls='--', c='r')
    ax[2].axhline(0, ls=':', c='r')

    ax[3].scatter(t[0:nsamp], smooth_speed[:nsamp], s=1, c='grey')
    ax[3].axhline(12, ls=':', c='r')
    ax[3].axhline(4, ls='--', c='r')
    ax[3].axhline(0, ls=':', c='r')


    ax[0].set_ylabel('X and Y position')
    ax[1].set_ylabel('X and Y position smoothed')

    ax[2].set_ylabel('Speed (cm/s)')
    ax[3].set_ylabel('Speed on smoothed pos. (cm/s)')

    ax[3].set_xlabel('Time (m)')
    sns.despine()
    plt.tight_layout()

    plot_name = os.path.join(plots_dir, 'possigma_what_{}_binsize{}_long_sess_{}.{}'.format(position_sigma, binsize,
                                                                         sess_ind, plots_format))
    f.savefig(plot_name, dpi=400)






