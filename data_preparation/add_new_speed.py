import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from utils import *



data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]

group_iti_and_return = True

SPEED_THRESHOLD = 4


# PLOTTING PARAMETERS
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------
if SPEED_THRESHOLD is not None:
    PLOTS_FOLDER = os.path.join(PLOTS_FOLDER, 'speed{}'.format(SPEED_THRESHOLD))

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'HC_replay')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df, df_not_sig = load_replay(sessions=sessions,
                             PBEs_settings_name=PBEs_settings_name,
                             PBEs_area=PBEs_area,
                             data_version=data_version,
                             dec_settings_name=dec_settings_name,
                             dec_area=dec_area,
                             min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                             shuffle_types=shuffle_types,
                             shuffle_p_vals=shuffle_p_vals,
                             group_iti_and_return=group_iti_and_return,
                             return_not_sig=True)

#speed_sigma = 2



# speed_sess = {}
# for sess_ind in df['sess_ind'].unique():
#
#     bl = get_session_block(DATA_FOLDER, sess_ind, epoch='task',
#                            data_version=data_version)
#
#     bl.segments = bl.segments[:-1]
#
#     # EXTRACT SPIKES AND POSITION
#     segment = merge_segments(bl.segments, bl.list_units)
#
#     rat_position_signal = segment.irregularlysampledsignals[0]
#     rat_position_times = rat_position_signal.times.rescale(pq.ms)
#
#     rat_speed_signal = segment.irregularlysampledsignals[1].rescale(pq.cm / pq.s)
#     rat_speed_times = rat_speed_signal.times.rescale(pq.ms)
# #
#     speed = scipy.ndimage.filters.gaussian_filter1d(rat_speed_signal.flatten(),
#                                                     sigma=speed_sigma)
#
#     speed_sess[sess_ind] = {}
#     speed_sess[sess_ind]['speed'] = speed.flatten()
#     speed_sess[sess_ind]['times'] = rat_speed_times
#
#
# df['average_speed_new'] = None
#
# for i, row in df.iterrows():
#
#     speed = speed_sess[row['sess_ind']]['speed']
#     times = speed_sess[row['sess_ind']]['times']
#
#     t1, t2 = row['start_time_in_ms']-2000, row['end_time_in_ms']
#
#     df.loc[i, 'average_speed_new'] = np.nanmean(speed[np.logical_and(times>=t1, times <t2)])

n4 = []
n5 = []
n6 = []
for sigma in [6, 8, 10, 12, 14, 16]:
    df = add_new_speed(df, time_before_in_ms=0, binsize_interp_pos=50,
                       time_after_in_ms=0, position_sigma=sigma,
                       data_version=data_version)

    n4.append([sigma, df[df['average_speed_new']>4].shape[0]])

    n5.append([sigma, df[df['average_speed_new']>5].shape[0]])
    n6.append([sigma, df[df['average_speed_new']>6].shape[0]])

print(n)
# dx = df[df['sess_ind'] == 0]
# dx['average_speed_new']
#
df[df['average_speed_new']>4]['phase'].value_counts()
#
#
# df[['average_speed', 'average_speed_new']]
#
# f, ax = plt.subplots(1, 1)
# ax.scatter(df['average_speed'], df['average_speed_new'], s=3)
#
# (df['average_speed_new']>4).sum()
#
# df[df['average_speed_new']>4]['phase'].value_counts()
#
# df[df['average_speed_new']>4]['sess_ind'].value_counts()


df['speed_group'] = None
df.loc[df['average_speed_new']>4, 'speed_group'] = 'Fast\nevents'
df.loc[df['average_speed_new']<=4, 'speed_group'] = 'Slow\nevents'


df['speed_group'].value_counts()

# df[df['speed_group']=='Fast\nevents']['replay_score'].quantile([0.25, 0.5, 0.75])
# df[df['speed_group']=='Slow\nevents']['replay_score'].quantile([0.25, 0.5, 0.75])

pal = {'Fast\nevents' : sns.xkcd_rgb['bright teal'],
       'Slow\nevents' : sns.xkcd_rgb['reddish orange']}

pal2 = {'fast' : sns.xkcd_rgb['bright teal'],
       'slow' : sns.xkcd_rgb['reddish orange']}


f,ax = plt.subplots(1, 1, figsize=[3, 3])
sns.countplot(data=df, x='speed_group', hue='phase')
ax.set_xlabel('')
ax.set_ylabel('Count')
sns.despine()
plt.tight_layout()

plot_name = 'count_per_phase.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



f,ax = plt.subplots(1, 1, figsize=[2, 3])
sns.barplot(data=df, x='speed_group', y='replay_score', palette=pal)
ax.set_xlabel('')
ax.set_ylabel('Replay score')
sns.despine()
plt.tight_layout()

plot_name = 'replay_score.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




f,ax = plt.subplots(1, 1, figsize=[2, 3])
sns.barplot(data=df, x='speed_group', y='event_duration_in_ms', palette=pal)
ax.set_xlabel('')
ax.set_ylabel('Event duration [ms]')
sns.despine()
plt.tight_layout()

plot_name = 'event_duration.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



f,ax = plt.subplots(1, 1, figsize=[2, 3])
sns.barplot(data=df, x='speed_group', y='average_speed_new', palette=pal)
ax.set_xlabel('')
ax.set_ylabel('Average speed')
sns.despine()
plt.tight_layout()

plot_name = 'average_speed_new.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




f,ax = plt.subplots(1, 1, figsize=[3, 3])
sns.countplot(data=df, x='speed_group', hue='has_ripple')
ax.set_xlabel('')
ax.set_ylabel('Count')
sns.despine()
plt.tight_layout()

plot_name = 'has_ripple.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




f,ax = plt.subplots(1, 1, figsize=[2, 3])
sns.barplot(data=df, x='speed_group', y='compression_factor', palette=pal)
ax.set_xlabel('')
ax.set_ylabel('Compression factor')
sns.despine()
plt.tight_layout()

plot_name = 'compression_factor.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)





f,ax = plt.subplots(1, 1, figsize=[2, 3])
sns.barplot(data=df, x='speed_group', y='dist_traversed_cm', palette=pal)
ax.set_xlabel('')
ax.set_ylabel('Distance covered by\nreplay trajectory [cm]')
sns.despine()
plt.tight_layout()

plot_name = 'distance_traversed.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# LOAD ACTUAL PBEs

sessions = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]


rds = {}

for sess_ind in sessions:

    pbes = load_decoded_PBEs(sess_ind, PBEs_settings_name, PBEs_area,
                             data_version, dec_settings_name, dec_area)

    for pbe_id, det in zip(pbes['pbes'].index, pbes['detectors']):
        rds[pbe_id] = det

#df = pd.concat(pbes_dfs)


proba = {'slow' : {'left' : None, 'right' : None},
         'fast' : {'left' : None, 'right' : None}}
for ev in ['slow', 'fast']:
    if ev == 'slow':
        ids = df[df['speed_group']=='Slow\nevents'].index
    elif ev == 'fast':
        ids = df[df['speed_group']=='Fast\nevents'].index

    left = []
    right = []

    for pbe_id in ids:
        if rds[pbe_id].bins.sum() < 10:
            left.append(rds[pbe_id].y_pred_proba.sum(axis=0))

        elif rds[pbe_id].bins.sum() > 10:
            right.append(rds[pbe_id].y_pred_proba.sum(axis=0))

        else:
            raise ValueError
    proba[ev]['left'] = left
    proba[ev]['right'] = right

f, ax = plt.subplots(1, 1, figsize=[3, 2])

for ev in ['slow', 'fast']:
    ax.plot(np.arange(30), np.array(proba[ev]['left']).mean(axis=0), c=pal2[ev])
    ax.plot(np.arange(30)+31, np.array(proba[ev]['right']).mean(axis=0), c=pal2[ev])
ax.set_ylabel('Bayesian prob.')
ax.set_xticks([])
ax.set_xlabel('<- Left arm - Right arm ->')
sns.despine()
plt.tight_layout()

plot_name = 'proba.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)






df['speed_group'] = None
df.loc[df['average_speed_new']>4, 'speed_group'] = 'fast'
df.loc[df['average_speed_new']<=4, 'speed_group'] = 'slow'


# --- START AND END OF TRAJ. FOR DIFFERENT PHASES -----------------------------

start = df['dec_start_bin'] # THIS IS DIFFERENT
end = df['dec_end_bin'] # THIS IS DIFFERENT

# ma_start = df['maze_arm_dec_start']
# ma_end = df['maze_arm_dec_end']
# df['local_remote'] = ['local' if a==b and a==c else 'remote' for a, b, c in
#                        zip(ma_occ, ma_start, ma_end)]

rew_loc = [15, 16, 17, 18, 19, -15, -16, -17, -18, -19]
df['traj_start_reward'] = ['yes' if np.isin(s, rew_loc) else 'no' for s in start]
df['traj_end_reward'] = ['yes' if np.isin(e, rew_loc) else 'no' for e in end]
df['traj_start_end_reward'] = ['yes' if a=='yes' or b=='yes' else 'no' for a, b in
                               zip(df['traj_start_reward'], df['traj_end_reward'])]

# --- LOAD BIN CENTERS --------------------------------------------------------
from plotting_style import *

sess_ind = 10
F = loadmat(DATA_FOLDER + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]
bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, session)
bin_centers, bin_labels = prepare_bins(bin_centers_combined)


for speed in ['fast', 'slow']:

    f, ax = plt.subplots(1, 1, figsize=[3, 3])

    for event in ['dec_start_bin', 'dec_end_bin']:

        df_sel = df[(df['epoch'] == 'task') & (df['speed_group'] == speed)
                    & (df['replay_direction'] == 'positive')]

        bins = merge_duplicate_left_right_bins(np.array(df_sel[event]))
        bin_counts = pd.value_counts(bins, normalize=True)

        for bin, counts in bin_counts.iteritems():
            #print(bin, counts)
            x, y = get_position_bin(bin, bin_centers, bin_labels)
            if event == 'dec_start_bin':
                x = x - 5
            elif event == 'dec_end_bin':
                x = x + 5
            ax.scatter(x, y, s=counts * dot_scaling, c=[beginning_end_palette[event]],
                       zorder=10)
    plot_maze(ax, c=maze_color)
    mark_locations(ax, color=maze_color)

    # for loc in circle_locations[task_phase]:
    #     x, y = get_position_bin(loc, bin_centers, bin_labels)
    #     circle = plt.Circle((x, y), circle_size[task_phase], edgecolor='w',
    #                         facecolor=animal_loc_color, linewidth=0,
    #                         zorder=10, alpha=0.2)
    #     ax.add_artist(circle)
    plt.tight_layout()
    ax.axis('off')
        #ax.set_title(task_phase)

    plot_name = 'start_and_end_traj_{}.{}'.format(speed, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)










val = df[df['speed_group'] == 'fast']['has_ripple'].value_counts()
print('fast, perc of ripples: {:.1f}%'.format(100 * val[True] / (val[False]+val[False])))


val = df[df['speed_group'] == 'slow']['has_ripple'].value_counts()
print('slow, perc of ripples: {:.1f}%'.format(100 * val[True] / (val[False]+val[False])))

