import os
import neo
import pickle
import numpy as np
import distutils.util
from quantities import s
import matplotlib.pyplot as plt
from loadmat import loadmat
from constants import iPix2Cm
from constants import DATA_FOLDER

save_as = 'pickle'

maze = loadmat(os.path.join(DATA_FOLDER, 'mazeOutlineNew.mat'))

new_maze = {}

for key in ['xaxMInL', 'xaxMInR', 'xaxMOut', 'yaxMInL', 'yaxMInR', 'yaxMOut']:
    new_maze[key] = maze[key] / iPix2Cm

pickle.dump(new_maze, open(os.path.join(DATA_FOLDER, 'maze_outline.pkl'), 'wb'))




