import os
import numpy as np
from loadmat import loadmat
import pickle
import elephant
import quantities as pq
import neo
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
import scipy.spatial
import seaborn as sns
from plotting_style import *
from utils import merge_segments
from utils import interpolate_outputs
from utils import load_bin_centers
from utils import bin_position_linearized
from utils import clean_position_segment
from utils import plot_maze
from utils import prepare_bins
from utils import bin_position_linearized_LR
from constants import DATA_FOLDER as data_folder
from utils import mark_locations
from constants import REPLAY_FOLDER
from constants import *


sess_ind            = 12
data_version        = 'dec16'
area                = 'Hippocampus'

trial_start_event   = 'ITI'
trial_end_event     = 'Return to central arm'

plot_format = 'svg'
dpi = 400
plots_folder = os.path.join(PLOTS_FOLDER, plot_format, 'POS_dec')

if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)

# --- LOAD SESSION DATA -------------------------------------------------------
F = loadmat(data_folder + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]
recording_day = session[0:10]
recording_time = session[11:]
file_name = 'TS_session_{}_{}_{}.pkl'.format(sess_ind + 1, rat, recording_day)
dir = os.path.join(os.path.join(data_folder, 'neo', data_version), rat, session)
bl = pickle.load(open(os.path.join(dir, file_name), 'rb'))


# --- SELECT POSITION ONLY BETWEEN CERTAIN EVENTS -----------------------------
data = {'position' : [],
        'raw_position' : [],
        'position_times' : [],
        'trial_side' : [],
        'speed' : []}

for segment in bl.segments:
    t_start = [segment.events[0].times[i] for i in range(len(segment.events[0].labels))
               if segment.events[0].labels[i] == trial_start_event][0]
    t_stop  = [segment.events[0].times[i] for i in range(len(segment.events[0].labels))
               if segment.events[0].labels[i] == trial_end_event][0]

    # extract position and speed signals
    pos_trial      = segment.analogsignals[0].__array__()
    postimes       = segment.analogsignals[0].times
    raw_pos_trial  = segment.analogsignals[2].__array__()
    speed_trial    = segment.analogsignals[1].__array__()

    # chop them based on the desired t_start and t_stop
    pos_trial      = pos_trial[(postimes > t_start) & (postimes < t_stop), :]
    raw_pos_trial  = raw_pos_trial[(postimes > t_start) & (postimes < t_stop), :]
    postimes_trial = postimes[(postimes > t_start) & (postimes < t_stop)]
    speed_trial    = speed_trial[(postimes > t_start) & (postimes < t_stop)]
    side_trial     = ['left' if segment.annotations['trial_side'] == 'left'
                      else 'right' for _ in range(pos_trial.shape[0])]

    # append
    data['position'].append(pos_trial)
    data['position_times'].append(postimes_trial)
    data['raw_position'].append(raw_pos_trial)
    data['trial_side'].append(side_trial)
    data['speed'].append(speed_trial)

for key in ['position', 'raw_position', 'speed']:
    data[key] = np.vstack(data[key])
for key in ['position_times', 'trial_side']:
    data[key] = np.hstack(data[key])


pos = data['position']
postimes = data['position_times']
trial_side = data['trial_side']
raw_pos = data['raw_position']

bin_centers_combined = load_bin_centers(data_folder, rat, day, session)

bin_centers, bin_labels = prepare_bins(bin_centers_combined)

binned_pos, unassigned = bin_position_linearized_LR(pos,
                                                    trial_side,
                                                    bin_centers,
                                                    bin_labels, max_distance=10)

# --- PLOT POSITION -----------------------------------------------------------

if False:
    f, ax = plt.subplots(2, 1, sharex=True)
    ax[0].scatter(postimes, raw_pos[:, 0], label='Raw position', s=5)
    ax[0].scatter(postimes, pos[:, 0], label='Smoothed position', s=5, c=sns.color_palette()[2])
    ax[1].scatter(postimes, binned_pos, color=sns.color_palette()[3], label='Binned position', s=5)
    ax[0].legend()
    ax[1].legend()

    f, ax = plt.subplots(1, 1, sharex=True)
    ax.scatter(raw_pos[:, 0], raw_pos[:, 1], label='Raw position', s=5)
    ax.scatter(pos[:, 0], pos[:, 1], label='Smoothed position', s=5, c=sns.color_palette()[2])
    plot_maze(ax)
    #ax[1].scatter(postimes, binned_pos, color=sns.color_palette()[3], label='Binned position', s=5)
    #ax[0].legend()
    #ax[1].legend()


# --- PLOT NEW LINEARIZED BINNING ---------------------------------------------

f, ax = plt.subplots(1, 2, figsize=[10, 5])

for i, side in enumerate(['right', 'left']):
    pos_side = pos[trial_side == side, :]
    ax[i].scatter(pos_side[:, 0], pos_side[:, 1], s=4, alpha=0.1,
                  color=sns.xkcd_rgb['sky'])
    ax[i].scatter(bin_centers[side][:, 0], bin_centers[side][:, 1],
                  color=sns.xkcd_rgb['scarlet'])

    for j, coord in enumerate(bin_centers[side]):
        ax[i].text(coord[0] + 2, coord[1], s=bin_labels[side][j])

sns.despine(fig=f, top=True, right=True)
ax[0].axis('off')
ax[1].axis('off')
plt.subplots_adjust(wspace=-0.01)
f.savefig(os.path.join(plots_folder, 'binning.{}'.format(plot_format)), dpi=dpi)



# --- PLOT NEW LINEARIZED BINNING ONLY BINS -----------------------------------

f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

for i, side in enumerate(['right', 'left']):

    ax.scatter(bin_centers[side][:, 0], bin_centers[side][:, 1],
                  edgecolor=sns.xkcd_rgb['scarlet'], facecolor='')

    # if side == 'right':
    #     for j, (coord, lab) in enumerate(zip(bin_centers[side][0::2],
    #                                          bin_labels[side][0::2])):
    #         if np.isin(lab, [0, 2, 28, 26, 24]):
    #             ax.text(coord[0] - 14, coord[1], s=lab ,rotation=180)
    #         elif np.isin(lab, [22]):
    #             ax.text(coord[0], coord[1]+12, s=lab ,rotation=180)
    #         elif np.isin(lab, [4]):
    #             ax.text(coord[0] - 14, coord[1]-12, s=lab ,rotation=180)
    #
    #         else:
    #             ax.text(coord[0] - 18, coord[1], s=lab ,rotation=180)
    #
    # elif side == 'left':
    #     for j, (coord, lab) in enumerate(zip(bin_centers[side][0::2],
    #                                          bin_labels[side][0::2])):
    #         ax.text(coord[0] + 4, coord[1], s=lab, rotation=180)

plot_maze(ax, c=maze_color)
mark_locations(ax, color=maze_color)
sns.despine(fig=f, top=True, right=True)
ax.axis('off')
plt.tight_layout()
f.savefig(os.path.join(plots_folder, 'bin_without_numbers.{}'.format(plot_format)), dpi=dpi)



# PLOT ONLY THE MAZE
f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)


plot_maze(ax, c=maze_color)
mark_locations(ax, color=maze_color)
sns.despine(fig=f, top=True, right=True)
ax.axis('off')
plt.tight_layout()
f.savefig(os.path.join(plots_folder, 'maze_with_marks.{}'.format(plot_format)), dpi=dpi)



# --- PLOT UNASSIGNED POINTS --------------------------------------------------
if False:
    """
    Plot points that were not assigned to a bin because the closest bin is too far, 
    connected to the bin that they would have been assigned to.
    """
    color = {'left' : sns.color_palette()[0], 'right' : sns.color_palette()[1]}
    f, ax = plt.subplots(1, 1)
    plot_maze(ax, c=maze_color)
    for point, bin, dist, side in unassigned:
        #ax.plot([point[0], bin[0]], [point[1], bin[1]], c=color[side])
        ax.scatter(point[0], point[1], c=color[side])

    for sd in ['left', 'right']:
        ind = np.where(trial_side == sd)[0]
        ax.scatter(pos[ind, 0], pos[ind, 1], c=color[sd], alpha=0.1, s=5)
        ax.scatter(bin_centers[sd][:, 0], bin_centers[sd][:, 1], c='k')


