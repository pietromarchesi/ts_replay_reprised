import os
from utils import *
from constants import *
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *

data_version = 'dec16'
sessions = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
speed_thr = 12
binsize = 100
plots_format = 'png'
plots_dir = os.path.join(PLOTS_FOLDER, plots_format, 'position_and_speed_smoothed_100')

if not os.path.isdir(plots_dir):
    os.makedirs(plots_dir, exist_ok=True)

for sess_ind in sessions:

    out = unpack_session(sess_ind, DATA_FOLDER, 'Hippocampus',
                         data_version=data_version, binsize_in_ms=binsize,
                         remove_nans=True,
                         interp_and_smooth_speed=True,
                         speed_sigma=2)

    speed = out['speed']
    pos = out['position']

    speed = speed[0:2000]
    pos = pos[0:2000, :]

    movingx = np.ma.masked_where(speed < speed_thr, pos[:, 0])
    movingy = np.ma.masked_where(speed < speed_thr, pos[:, 1])
    notmovingx = np.ma.masked_where(speed >= speed_thr, pos[:, 0])
    notmovingy = np.ma.masked_where(speed >= speed_thr, pos[:, 1])

    xx = np.arange(pos.shape[0])
    f, ax = plt.subplots(2, 1, sharex=True)
    ax[0].scatter(xx, movingx, s=3, c=sns.xkcd_rgb['blue'], label='X moving')
    ax[0].scatter(xx, movingy, s=3, c=sns.xkcd_rgb['green'], label='Y moving')
    ax[0].scatter(xx, notmovingx, s=2, c='grey', label = 'X not moving')
    ax[0].scatter(xx, notmovingy, s=2, c='grey', label = 'Y not moving')
    ax[1].scatter(xx, np.hstack(speed), s=2, c='grey')
    ax[0].set_ylabel('X and Y position')
    ax[1].set_ylabel('Smoothed speed (cm/s)')
    ax[1].axhline(12, ls=':', c='r')
    ax[1].axhline(4, ls='--', c='r')
    sns.despine()
    ax[0].legend()
    plt.tight_layout()

    plot_name = os.path.join(plots_dir, 'thr_{}_ms_{}_sess_{}.{}'.format(speed_thr,
                                                                         binsize,
                                                                         sess_ind, plots_format))
    f.savefig(plot_name, dpi=400)

