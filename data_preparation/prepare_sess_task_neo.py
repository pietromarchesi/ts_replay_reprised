import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(sys.path[0]))))
import neo
import pickle
import numpy as np
import distutils.util
from quantities import s
import matplotlib.pyplot as plt
from loadmat import loadmat, nan_helper
import warnings
from constants import iPix2Cm
import pandas as pd

try:
    from utils import clean_position_segment
    can_clean_position = True
except FileNotFoundError:
    raise ValueError('Could not import position cleaning function, probably the maze'
                      'file cannot be found')

settings_name                = 'dec16'
data_folder                  = '/media/pietro/bigdata/neuraldata/touch_see/'
save_to                      = os.path.join(data_folder, 'neo', settings_name)

save_block                   = True
save_as                      = 'pickle'
save_pars                    = True
interpolate_and_smooth_speed = True
speed_sigma                  = 4
interp_pos_nans              = False
smooth_position              = False
clean_position               = False
downsample_pos               = False
sigma                        = 2    # to smooth position
downsample_period            = 25


sessions                     = range(46) # range(46) - Python index - sess_ind 0 is session 1



if not os.path.isdir(save_to):
    os.makedirs(save_to, exist_ok=True)

for sess_ind in sessions:
    print('Packaging Touch & See Session {}'.format(sess_ind))

    ### EXTRACTING AND PREPARING THE DATA FROM ORIGINAL FILES ###
    F = loadmat(data_folder + '/F/F_units/F_All.mat')['F']
    rat, day, session = F[sess_ind].dir.split('/')[-3:]

    recording_day = session[0:10]
    recording_time = session[11:]

    session_path = 'SpikeTrl/'+rat+'/'+day+'/'+session+'/spikeTrl.mat'
    binned_data_path = 'PositionInfo_30/'+rat+'/'+day+'/'+session+'/binnedData.mat'
    binned_mov_path = 'PositionInfo_30/'+rat+'/'+day+'/'+session+'/binnedMovementDirection.mat'
    unit_path = 'unit.mat'

    S  = loadmat(data_folder + session_path)
    B  = loadmat(data_folder + binned_data_path)
    U  = loadmat(data_folder + unit_path)
    M  = loadmat(data_folder + binned_mov_path)
    W  = loadmat(data_folder + 'wave_class.mat')


    original_files = [session_path, binned_data_path, unit_path]

    first_ts = S['spike']['hdr'][sess_ind].FirstTimeStamp
    spike_ts = S['spike']['timestamp']

    last_spike_ts = np.array([sp.max() for sp in spike_ts])
    last_spike_s  = (last_spike_ts.max() - first_ts)/ 1e6

    # trial information
    trl_imo = S['spikeTrl_imgon']['cfg']['trl']
    trl_rew = S['spikeTrl_rwd']['cfg']['trl']
    trl_ret = S['spikeTrl_returnBeam']['cfg']['trl']
    trl_pon = S['spikeTrl_pon']['cfg']['trl']
    trl_iti = S['spikeTrl_iti']['cfg']['trl']
    trl_pub = S['spikeTrl_pickUpBlock']['cfg']['trl']

    imo_times = (trl_imo[:, 1] - first_ts) / 1e6 + trl_imo[:, 2] / 1e6
    rew_times = (trl_rew[:, 1] - first_ts) / 1e6 + trl_rew[:, 2] / 1e6
    ret_times = (trl_ret[:, 1] - first_ts) / 1e6 + trl_ret[:, 2] / 1e6
    pon_times = (trl_pon[:, 1] - first_ts) / 1e6 + trl_pon[:, 2] / 1e6
    iti_times = (trl_iti[:, 1] - first_ts) / 1e6 + trl_iti[:, 2] / 1e6
    pub_times = (trl_pub[:, 1] - first_ts) / 1e6 + trl_pub[:, 2] / 1e6


    # select only the trials for which the time the animal took between trial start
    # and point of no return is below 15 seconds
    trials_ok     = trl_imo[:, -1] / 1e6 < 15
    trial_outcome = trl_imo[:, 4] - 1
    trial_side    = trl_imo[:, 5] - 1  # where the animal actually went
    trial_type    = trl_imo[:, 6] - 1
    n_trials      = imo_times.shape[0]


    frame_times   = B['placedataNanJump']['time']

    ### PUTATIVE NEURON TYPE ###

    # unit-level information
    unit_index    = U['unit']['sess'] == sess_ind + 1
    unit_area     = U['unit']['area'][unit_index]
    unit_depth    = U['unit']['unitDepth'][unit_index]
    unit_layer    = U['unit']['unitLayer'][unit_index]
    unit_rat      = U['unit']['ratName'][unit_index]
    unit_sess     = U['unit']['sess'][unit_index]
    unit_sess_num = U['unit']['unitSessNum'][unit_index]
    unit_label    = U['unit']['label'][unit_index]
    n_units       = unit_area.shape[0]

    neuron_type_labels = W['R']['neuronTypeLabel']
    neuron_type_code   = W['R']['neuronType']-1
    neuron_type        = neuron_type_labels[neuron_type_code]
    neuron_type_sess   = neuron_type[unit_index]
    is_pyr_sess        = np.array([0 if t=='narrow_int' else 1 for t in
                                   neuron_type_sess]).astype(bool)



    # extract the tetrode and channel number from the label
    unit_tetrode  = np.zeros_like(unit_label)
    for i in range(unit_tetrode.shape[0]):
        tet_num = int(unit_label[i][unit_label[i].find('TT')+2])
        unit_tetrode[i] = tet_num

    # Channel number (was not actually used later on)
    # unit_channel = np.zeros_like(unit_label)
    # for i in range(unit_channel.shape[0]):
    #     chan_num = unit_label[i].split('_')[1]
    #     if not chan_num in ['A', 'B', 'C', 'D']:
    #         warnings.warn('Could not extract unit channel.')


    # run some checks:
    # preprocessed img_on times are equal to the ones extracted from spikeTrl.event
    np.testing.assert_array_equal(imo_times, imo_times)
    # same number of units and spike timestamp vectors
    np.testing.assert_equal(unit_area.shape, spike_ts.shape)


    import scipy.ndimage
    ### EXTRACT AND PROCESS POSITION DATA ###

    # _rawraw is the really original position tracking
    # _raw will potentially be interpolated if interp_pos_nan is True
    xpos_rawraw  = B['placedataNanJump']['xpos'] / iPix2Cm
    ypos_rawraw  = B['placedataNanJump']['ypos'] / iPix2Cm
    postimes     = B['placedataNanJump']['time']
    position_sampling_period = np.diff(postimes)[0]*s

    if interp_pos_nans:
        raise ValueError('Let\'s not do this anymore')
        print('Interpolating nans')
        # xpos_raw = xpos_rawraw.copy()
        # ypos_raw = ypos_rawraw.copy()
        #
        # nans, x= nan_helper(xpos_raw)
        # xpos_raw[nans]= np.interp(x(nans), x(~nans), xpos_raw[~nans])
        # nans, y= nan_helper(ypos_raw)
        # ypos_raw[nans]= np.interp(y(nans), y(~nans), ypos_raw[~nans])

    else:
        xpos_raw = xpos_rawraw.copy()
        ypos_raw = ypos_rawraw.copy()


    if downsample_pos:
        raise ValueError('Let\'s not do this anymore')
        # print('Downsampling position with a period of {} frames, and '
        #       'interpolating'.format(downsample_period))
        # xpos_ds = xpos_raw[::downsample_period]
        # ypos_ds = ypos_raw[::downsample_period]
        # postimes_ds = postimes[::downsample_period]
        #
        # xpos = np.interp(postimes, postimes_ds, xpos_ds)
        # ypos = np.interp(postimes, postimes_ds, ypos_ds)


    elif smooth_position:
        raise ValueError('Let\'s not do this anymore')
        # print('Smoothing position with sigma {}'.format(sigma))
        # xpos     = scipy.ndimage.filters.gaussian_filter1d(xpos_raw, sigma=sigma)
        # ypos     = scipy.ndimage.filters.gaussian_filter1d(ypos_raw, sigma=sigma)

    else:
        xpos = xpos_raw
        ypos = ypos_raw


    # import seaborn as sns
    # f, ax = plt.subplots(1, 1, sharex=True)
    # ax.scatter(xpos_raw, ypos_raw, label='Raw position', s=5)
    # ax.scatter(xpos, ypos, label='Smoothed position', s=5, c=sns.color_palette()[2])
    # plot_maze(ax)

    ### COMPUTE SPEED ###
    print('Computing speed')
    xy = np.hstack((xpos[:, None], ypos[:, None]))
    a = xy[:-1]
    b = np.roll(xy, -1, axis=0)[:-1]
    dxy = np.linalg.norm(a - b, axis=1)
    speed = np.divide(dxy, np.diff(postimes))
    # TODO: terrible hack to make speed as long as postimes

    speed = np.hstack((speed, speed[-1]))
    if interpolate_and_smooth_speed:
        speed = pd.Series(speed.flatten()).interpolate().get_values()
        speed = scipy.ndimage.filters.gaussian_filter1d(speed, sigma=speed_sigma)

    # xplot, yplot, speedplot = xpos_raw, ypos_raw, interpolated_speed_nonan
    # xxx = np.arange(xplot.shape[0])
    #
    # f, ax = plt.subplots(2, 1, sharex=True)
    # ax[0].scatter(xxx, xplot, s=5)
    # ax[0].scatter(xxx, yplot, s=5)
    # #ax[0].plot(xpos[0:1000])
    # ax[1].plot(speedplot)
    # ax[1].plot(scipy.ndimage.filters.gaussian_filter1d(speedplot, sigma=4))
    # ax[0].set_ylabel('X and Y position')
    # ax[1].set_ylabel('Speed [cm/s]')

    ### SETTING UP THE NEO DATASET ###

    # CREATE EMPTY BLOCK
    bl = neo.Block(name = 'session %s' %(sess_ind + 1),
                   animal_ID = rat,
                   recording_day = recording_day,
                   recording_time = recording_time,
                   day_folder = day,
                   session_folder = session)

    # EXPLAIN THE CODES USED IN THE ANNOTATIONS
    bl.annotations['trial_outcome'] = '0=bad trial, 1=good trial'
    bl.annotations['trial_side']   = '0=left, 1=right'
    bl.annotations['trial_type']   = '0=normal trial, 1=memory trial (image goes' \
                                     'off before movement starts)'

    trial_outcome_string = {0 : 'incorrect', 1 : 'correct'}
    trial_side_string    = {1 : 'left', 0: 'right'}
    trial_type_string    = {0 : 'normal', 1 : 'memory'}

    # CREATE CHANNELS
    Hp     = neo.ChannelIndex(name='Hippocampus', index=None)
    Pr     = neo.ChannelIndex(name='Perirhinal', index=None)
    Br     = neo.ChannelIndex(name='Barrel', index=None)
    V1     = neo.ChannelIndex(name='V1', index=None)
    bl.channel_indexes = [Hp, Pr, Br, V1] #, Pr35, Pr36, Pr3536]
    # the index in the ChannelIndex is the index of the units belonging to the
    # channel in the analog signal, so that you can use it to slice it and recover

    # CREATE UNITS
    for u in range(n_units):

        if unit_area[u] != 'tea':
            np.testing.assert_equal(unit_rat[u], rat)
            np.testing.assert_equal(unit_sess[u], sess_ind + 1)
            np.testing.assert_equal(unit_sess_num[u], u+1)

            general_unit_ind = (np.where(unit_index)[0]+1)[u]
            unit = neo.Unit(name='unit {}'.format(general_unit_ind),
                            area=None,
                            depth=unit_depth[u],
                            layer=unit_layer[u],
                            sess_ind=u,
                            tetrode=unit_tetrode[u],
                            general_unit_ind=general_unit_ind,
                            neuron_type=neuron_type_sess[u],
                            is_pyramidal=is_pyr_sess[u])

            # ASSIGN UNITS TO AREAS
            if unit_area[u] == 'Br':
                unit.annotations['area'] = 'Barrel'
                unit.annotations['sub_area'] = ''
                Br.units.append(unit)
                unit.channel_index = Br

            if unit_area[u] == 'Hp':
                unit.annotations['area'] = 'Hippocampus'
                unit.annotations['sub_area'] = ''
                Hp.units.append(unit)
                unit.channel_index = Hp

            # perirhinal units from different sub-areas are grouped together,
            # information about the sub-areas is moved to the annotations.
            if unit_area[u] == '35':
                unit.annotations['area'] = 'Perirhinal'
                unit.annotations['sub_area'] = 'Perirhinal - Area 35'
                Pr.units.append(unit)
                unit.channel_index = Pr

            if unit_area[u] == '36':
                unit.annotations['area'] = 'Perirhinal'
                unit.annotations['sub_area'] = 'Perirhinal - Area 36'
                Pr.units.append(unit)
                unit.channel_index = Pr

            if unit_area[u] == 'b3536':
                unit.annotations['area'] = 'Perirhinal'
                unit.annotations['sub_area'] = 'Perirhinal - Area 35-36'
                Pr.units.append(unit)
                unit.channel_index = Pr

            if unit_area[u] == 'V':
                unit.annotations['area'] = 'V1'
                unit.annotations['sub_area'] = ''
                V1.units.append(unit)
                unit.channel_index = V1



    for t in range(n_trials):
        if trials_ok[t]:

            if not t == n_trials - 1:
                t_start, t_stop = iti_times[t], iti_times[t + 1]
            else:
                t_start, t_stop = iti_times[t], last_spike_s

            # ADD EVENTS


            event_times  = np.array([iti_times[t],
                                     imo_times[t],
                                     pub_times[t],
                                     pon_times[t],
                                     rew_times[t],
                                     ret_times[t]])

            event_labels = np.array(['ITI','Image on', 'Block removed',
                            'Point of no return',
                            'Reward', 'Return to central arm'])


            ev = neo.Event(event_times * s,
                           labels=event_labels)

            seg = neo.Segment(name='Trial %s' %(t+1),
                              index=t+1,
                              trial_number=t+1,
                              trial_outcome=trial_outcome_string[trial_outcome[t]],
                              trial_side=trial_side_string[trial_side[t]],
                              trial_type=trial_type_string[trial_type[t]],
                              t_start=t_start * s,
                              t_stop=t_stop * s)

            ev.segment = seg
            seg.events.append(ev)

            for un in bl.list_units:
                # extract the spike train for the specific unit
                un_ind = un.annotations['sess_ind']
                spike_train = (spike_ts[un_ind] - first_ts) / 1e6
                # beginning and end of the trial

                # get the spike times within the trial
                # train_trial is the spike train of unit `un` during trial `t`
                train_trial = spike_train[(spike_train>=t_start)&(spike_train<t_stop)].copy()
                # GENERATE SPIKETRAIN OBJECT
                sptr_name = 'Unit {} in Trial {}'.format(un.annotations['general_unit_ind'],
                                                                         t + 1)
                train = neo.SpikeTrain(train_trial*s,
                                       t_start=t_start*s,
                                       t_stop=t_stop*s,
                                       name= sptr_name,
                                       general_unit_ind=un.annotations['general_unit_ind'],
                                       neuron_type=un.annotations['neuron_type'],
                                       is_pyramidal=un.annotations['is_pyramidal'],
                                       area=un.annotations['area'],
                                       sub_area=un.annotations['sub_area'],
                                       index=t + 1,
                                       trial_number=t + 1,
                                       trial_outcome=trial_outcome_string[trial_outcome[t]],
                                       trial_side=trial_side_string[trial_side[t]],
                                       trial_type=trial_type_string[trial_type[t]],
                                       trial_start_timestamp=imo_times[t])

                # ASSIGN THE SPIKE TRAIN TO THE TRIAL (SEGMENT)
                seg.spiketrains.append(train)
                # ASSIGN THE SPIKE TRAIN TO THE UNIT
                un.spiketrains.append(train)
                # ASSIGN THE SEGMENT AND UNIT TO THE SPIKE TRAIN
                train.segment = seg
                train.unit = un

            x_trial = xpos[(postimes >= t_start) & (postimes < t_stop)]
            y_trial = ypos[(postimes >= t_start) & (postimes < t_stop)]

            xy_trial = np.hstack((x_trial[:, None],y_trial[:, None]))

            pos_trial = neo.AnalogSignal(xy_trial,
                                         units='cm',
                                         t_start=t_start * s,
                                         t_stop=t_stop * s,
                                         sampling_period=position_sampling_period,
                                         name='xy position of Trial {}'.format(t + 1),
                                         description='first column: x position'
                                                    'second column: y position',
                                         trial_number=t + 1,
                                         trial_outcome=trial_outcome_string[trial_outcome[t]],
                                         trial_side=trial_side_string[trial_side[t]],
                                         trial_type=trial_type_string[trial_type[t]],
                                         trial_start_timestamp=imo_times[t])

            # keep raw (the truly raw original tracking) position just in case
            x_raw_trial = xpos_rawraw[(postimes >= t_start) & (postimes < t_stop)]
            y_raw_trial = ypos_rawraw[(postimes >= t_start) & (postimes < t_stop)]

            xy_raw_trial = np.hstack((x_raw_trial[:, None], y_raw_trial[:, None]))

            raw_pos_trial = neo.AnalogSignal(xy_raw_trial,
                                         units='cm',
                                         t_start=t_start * s,
                                         t_stop=t_stop * s,
                                         sampling_period=position_sampling_period,
                                         name='RAW xy position of Trial {}'.format(
                                             t + 1),
                                         description='first column: x position'
                                                     'second column: y position',
                                         trial_number=t + 1,
                                         trial_outcome=trial_outcome_string[trial_outcome[t]],
                                         trial_side=trial_side_string[trial_side[t]],
                                         trial_type=trial_type_string[trial_type[t]],
                                         trial_start_timestamp=imo_times[t])


            speed_trial = speed[(postimes >= t_start) & (postimes < t_stop)]

            speed_trial = neo.AnalogSignal(speed_trial,
                                           units='cm/s',
                                           t_start=t_start * s,
                                           t_stop=t_stop * s,
                                           sampling_period=position_sampling_period,
                                           name='Speed of locomotion of the animal of Trial {}'.format(t + 1),
                                           trial_number=t + 1,
                                           trial_outcome=trial_outcome_string[trial_outcome[t]],
                                           trial_side=trial_side_string[trial_side[t]],
                                           trial_type=trial_type_string[trial_type[t]],
                                           trial_start_timestamp=imo_times[t])


            seg.analogsignals.append(pos_trial)
            seg.analogsignals.append(speed_trial)
            seg.analogsignals.append(raw_pos_trial)
            # ADD SEGMENT TO THE BLOCK
            bl.segments.append(seg)


    ### CLEAN POSITION ###
    if clean_position and can_clean_position:
        raise ValueError('Let\'s not do this anymore')
        print('Cleaning position')
        for segment in bl.segments:
            clean_position_segment(segment)

    if save_block:

        dir = os.path.join(save_to, rat, session)

        if not os.path.isdir(dir):
            os.makedirs(dir)

        name = 'TS_session_{}_{}_{}'.format(sess_ind + 1, rat, recording_day)


        if save_as == 'pickle':

            format = '.pkl'
            full_destination = os.path.join(dir, name+format)
            print('Saving output to: {}'.format(full_destination))
            with open(full_destination, 'wb') as f:
                pickle.dump(bl, f)

        if save_as == 'numpy':
            format = '.npy'
            full_destination = os.path.join(dir, name+format)
            print('Saving output to: {}'.format(full_destination))
            np.save(full_destination, bl)



if save_pars:

    pars = {'settings_name' : settings_name,
            'data_folder' : data_folder,
            'save_to' : save_to,
            'interpolate_and_smooth_speed' : interpolate_and_smooth_speed,
            'speed_sigma' : speed_sigma,
            'interp_pos_nans' : interp_pos_nans,
            'smooth_position' : smooth_position,
            'clean_position' : clean_position,
            'downsample_pos' : downsample_pos,
            'sigma' : sigma,
            'downsample_period' : downsample_period}


    pickle.dump(pars, open(os.path.join(save_to, 'pars.pkl'), 'wb'))