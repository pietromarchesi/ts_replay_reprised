import os
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import neo
from loadmat import loadmat
import elephant
import quantities as pq
import pickle
import scipy.stats
from replaydetector.DKW_decoder import DKW_decoder
from constants import DATA_FOLDER, REPLAY_FOLDER
from constants import iPix2Cm
import pandas as pd
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from sklearn.metrics import confusion_matrix
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from statsmodels.stats.multitest import multipletests


try:
    from plotting_style import *
except (ModuleNotFoundError, ImportError):
    print('Could not load matplotlib/seaborn. Plotting functionality of the'
          'Replay Detector will not work')


try:
    maze_file = os.path.join(DATA_FOLDER, 'maze_outline.pkl')

    mz = pickle.load(open(maze_file, 'rb'))

    ext = list(zip(mz['xaxMOut'], mz['yaxMOut']))
    intL = list(zip(mz['xaxMInL'], mz['yaxMInL']))
    intR = list(zip(mz['xaxMInR'], mz['yaxMInR']))

    maze = Polygon(ext, [intL, intR])

    central_arm_x = np.hstack((mz['xaxMInL'][0], mz['xaxMInL'][7],
                               mz['xaxMInR'][0], mz['xaxMInR'][7]))
    central_arm_y = np.hstack((mz['yaxMInL'][0], mz['yaxMInL'][7],
                               mz['yaxMInR'][0], mz['yaxMInR'][7]))
    central_arm = Polygon(list(zip(central_arm_x, central_arm_y)))

    left_arm_x = np.hstack((mz['xaxMOut'][9:-2], mz['xaxMInR'][2:-3]))
    left_arm_y = np.hstack((mz['yaxMOut'][9:-2], mz['yaxMInR'][2:-3]))
    left_arm = Polygon(list(zip(left_arm_x, left_arm_y)))

    right_arm_x  = np.hstack((mz['xaxMOut'][2:6], mz['xaxMInL'][2:-3]))
    right_arm_y  = np.hstack((mz['yaxMOut'][2:6], mz['yaxMInL'][2:-3]))
    right_arm    = Polygon(list(zip(right_arm_x, right_arm_y)))

except FileNotFoundError:
    print('Could not load the maze outline. Some functionality may not be'
          'available.')





def shorthand_area(area):
    if area == 'Hippocampus':
        short = 'HC'
    elif area == 'Perirhinal':
        short = 'PR'
    return short

def z_score(X, neuron_axis=1):
    if neuron_axis == 1:
        X_mean = np.mean(X, axis=0)
        X_std  = np.std(X, axis=0)
        X_std[X_std == 0] = 1
        Xz = (X - X_mean) / X_std
    elif neuron_axis == 0:
        X_mean = np.mean(X, axis=1)[:, np.newaxis]
        X_std  = np.std(X, axis=1)[:, np.newaxis]
        X_std[X_std == 0] = 1
        Xz = (X - X_mean) / X_std
    return Xz


def min_max_scale(X, min=0, max=1, neuron_axis=1):
    mm = MinMaxScaler(feature_range=(min, max))

    if neuron_axis == 1:
        Xm = mm.fit_transform(X)
    elif neuron_axis == 0:
        Xm = mm.fit_transform(X.T).T
    return Xm


def z_score_tensor(X):
    # experimental function
    Xf = X.reshape(X.shape[0], X.shape[1]*X.shape[2], order='F')

    Xf_mean = np.mean(Xf, axis=1)[:, np.newaxis]
    Xf_std = np.std(Xf, axis=1)[:, np.newaxis]
    Xf_std[Xf_std == 0] = 1
    Xfz= (Xf - Xf_mean) / Xf_std
    Xz = Xfz.reshape(X.shape[0], X.shape[1], X.shape[2], order='F')

    return Xz


def compute_percentage_active(arr, interval):
    n_active = (arr[:, interval[0]:interval[1]].sum(axis=1) > 0).sum()
    percentage_active = 100 * n_active / arr.shape[0]
    return percentage_active

def threshold_percentage_active(arr, interval, percentage):
    n_active = (arr[:, interval[0]:interval[1]].sum(axis=1) > 0).sum()
    percentage_active = 100 * n_active / arr.shape[0]
    return percentage_active >= percentage

def interpolate_outputs(outputs, output_times, new_times):
    output_dim = outputs.shape[1]  # Number of output features
    outputs_binned = np.empty([new_times.shape[0], output_dim])  # Initialize matrix of binned outputs

    for k in range(output_dim):
        outputs_binned[:, k] = np.interp(new_times, output_times, outputs[:, k])
    return outputs_binned



def merge_segments(segments, list_units, chop_return_central_arm = False):
    merged_trains = []

    if chop_return_central_arm:
        for s in segments:
            t_chop = s.events[0][s.events[0].labels == 'Return to central arm'].times[0]
            s.spiketrains = [t.time_slice(t_start = s.t_start, t_stop=t_chop)
                             for t in s.spiketrains]
            s.analogsignals = [a.time_slice(t_start = s.t_start, t_stop=t_chop)
                               for a in s.analogsignals]

    for i, un in enumerate(list_units):
        trains = [s.spiketrains[i] for s in segments]


        spiketimes = np.hstack([t.times for t in trains]) * trains[0].times.units

        train = neo.SpikeTrain(spiketimes,
                               t_start=trains[0].t_start,
                               t_stop=trains[-1].t_stop,
                               name='Unit {}'.format(
                                   un.annotations['general_unit_ind']),
                               general_unit_ind=un.annotations['general_unit_ind'],
                               neuron_type=un.annotations['neuron_type'],
                               is_pyramidal=un.annotations['is_pyramidal'],
                               area=un.annotations['area'],
                               sub_area=un.annotations['sub_area'])

        merged_trains.append(train)

    # stack position data
    position_units = segments[0].analogsignals[0].units
    time_units = segments[0].analogsignals[0].times.units
    position = np.vstack([s.analogsignals[0].__array__() for s in
                          segments]) * position_units
    position_times = np.hstack([s.analogsignals[0].times for s in
                                segments]) * time_units

    # stack velocity data
    velocity_units = segments[0].analogsignals[1].units
    time_units = segments[0].analogsignals[1].times.units

    velocity = np.vstack([s.analogsignals[1].__array__() for s in
                          segments]) * velocity_units
    velocity_times = np.hstack([s.analogsignals[1].times for s in
                                segments]) * time_units


    # stack raw position data
    raw_position_units = segments[0].analogsignals[2].units
    raw_time_units = segments[0].analogsignals[2].times.units
    raw_position = np.vstack([s.analogsignals[2].__array__() for s in
                          segments]) * raw_position_units
    raw_position_times = np.hstack([s.analogsignals[2].times for s in
                                segments]) * raw_time_units

    # if there are missing trials or other interruptions, the position and
    # velocity will not be regularly sampled anymore, so we use this
    position_signal = neo.IrregularlySampledSignal(signal=position,
                                                   times=position_times,
                                                   description='first column: x position'
                                                               'second column: y position')

    velocity_signal = neo.IrregularlySampledSignal(signal=velocity,
                                                   times=velocity_times,
                                                   description='Velocity')

    raw_position_signal = neo.IrregularlySampledSignal(signal=raw_position,
                                                   times=raw_position_times,
                                                   description='Velocity')


    seg = neo.Segment(name='Merged segment',
                      t_start=trains[0].t_start,
                      t_stop=trains[-1].t_stop)

    seg.spiketrains = merged_trains
    seg.irregularlysampledsignals.append(position_signal)
    seg.irregularlysampledsignals.append(velocity_signal)
    seg.irregularlysampledsignals.append(raw_position_signal)

    return seg


class AreaNotRecordedInSession(Exception):
    pass


def unpack_session(sess_ind, data_folder, area, data_version, binsize_in_ms=100,
                   remove_nans=True, interp_and_smooth_speed=False, speed_sigma=5):
    """
    Starts from the Neo block.

    Unpack session does the following actions:
    1. Merge spike trains and position/velocity signals across segments
    2. Bin spikes and interpolate position/velocity
    3. Remove data from bad trials (set to nan at this stage)
    4. Create a vector indicating trial side at each time point
    5. Bin position to the linearized LR bins
    6. If remove_nans is True, clean up all nans (also in the time and side vectors)
    7. Remove the last trial by default
    """

    bl = get_session_block(data_folder, sess_ind, epoch='task',
                           data_version=data_version)

    bl.segments = bl.segments[:-1]

    # EXTRACT SPIKES AND POSITION
    segment = merge_segments(bl.segments, bl.list_units)
    trains = segment.filter(targdict={'area': area}, objects=neo.SpikeTrain)

    if len(trains) == 0:
        raise AreaNotRecordedInSession('No spike trains of the selected area. Exiting.')

    rat_position_signal = segment.irregularlysampledsignals[0]
    rat_position_times = rat_position_signal.times.rescale(pq.ms)

    rat_speed_signal = segment.irregularlysampledsignals[1].rescale(pq.cm / pq.s)
    rat_speed_times = rat_speed_signal.times.rescale(pq.ms)

    is_pyramidal = [i for i in range(len(trains))
                    if trains[i].annotations['is_pyramidal']]

    # BIN SPIKES AND INTERPOLATE POSITION AND VELOCITY ------------------------
    bs = elephant.conversion.BinnedSpikeTrain(trains,
                                              binsize=binsize_in_ms * pq.ms,
                                              t_start=segment.t_start.rescale(
                                                  pq.ms),
                                              t_stop=segment.t_stop.rescale(
                                                  pq.ms))
    X = bs.to_array().T.astype(float)
    times = bs.bin_centers
    t_start = segment.t_start.rescale(pq.ms)
    t_stop  = segment.t_stop.rescale(pq.ms)

    interpolated_position = interpolate_outputs(rat_position_signal.__array__(),
                              rat_position_times, times)
    interpolated_speed = interpolate_outputs(rat_speed_signal.__array__(),
                              rat_speed_times, times)

    # REMOVE DATA FROM BAD TRIALS ---------------------------------------------
    # which would otherwise have no spikes and flat position
    removed_trials = []
    for seg1, seg2 in zip(bl.segments[:-1], bl.segments[1:]):
        if (seg2.t_start - seg1.t_stop) > 5 * pq.s:
            removed_trials.append((seg1.t_stop, seg2.t_start))
    print('Session: {} - Removed {} trials'.format(sess_ind, len(removed_trials)))

    removed = np.repeat(False, bs.bin_centers.shape[0])
    for rt in removed_trials:
        in_removed_trial = np.logical_and(bs.bin_centers >= rt[0],
                                          bs.bin_centers <= rt[1])
        removed = np.logical_or(removed, in_removed_trial)

    X[removed, :] = np.nan
    interpolated_position[removed, :] = np.nan
    interpolated_speed[removed] = np.nan

    # CREATE TRIAL SIDE VECTOR
    trial_side = np.repeat(np.nan, bs.bin_centers.shape[0])
    for tr in bl.segments:
        in_trial = np.logical_and(bs.bin_centers >= tr.t_start,
                                  bs.bin_centers <= tr.t_stop)
        if tr.annotations['trial_side'] == 'left':
            ts = 1
        elif tr.annotations['trial_side'] == 'right':
            ts = 0
        else:
            ts = -1
        trial_side[in_trial] = ts

    trial_side_label = ['left' if s == 1 else 'right' if s == 0
    else np.nan for s in trial_side]

    # BIN POSITION
    rat = bl.annotations['animal_ID']
    day_folder = bl.annotations['day_folder']
    session_folder = bl.annotations['session_folder']

    bin_centers_combined = load_bin_centers(data_folder, rat, day_folder,
                                            session_folder)

    bin_centers, bin_labels = prepare_bins(bin_centers_combined)


    binned_pos, unassigned = bin_position_linearized_LR(interpolated_position,
                                                        trial_side_label,
                                                        bin_centers,
                                                        bin_labels,
                                                        max_distance=10)

    # REMOVE TIME POINTS FOR WHICH POSITION IS NAN
    if remove_nans:
        ind = ~np.isnan(binned_pos)
        X = X[ind, :]
        binned_pos = binned_pos[ind]
        interpolated_position = interpolated_position[ind, :]
        interpolated_speed = interpolated_speed[ind]
        trial_side = np.array(trial_side)[ind]
        trial_side_label = np.array(trial_side_label)[ind]
        times = times[ind]

    # INTERPOLATE SPEED
    # at this point speed will still have nans potentially, we can first
    # interpolated it then smooth
    print(np.sum(np.isnan(interpolated_speed)))
    if interp_and_smooth_speed:
        print('resmoothing speed')
        #raise ValueError('Don\'t resmooth!')
        interp_speed_nonan = pd.Series(interpolated_speed.flatten()).interpolate().values
        speed = scipy.ndimage.filters.gaussian_filter1d(interp_speed_nonan,
                                                               sigma=speed_sigma)
    else:
        speed = interpolated_speed.flatten()

    out = {'spikes': X,
           'trains' : trains,
           'is_pyramidal' : is_pyramidal,
           'bin': binned_pos,
           'speed': speed,
           'position' : interpolated_position,
           'speed_units' : rat_speed_signal.units,
           'trial_side' : trial_side,
           'trial_side_label' : trial_side_label,
           'times' : times,
           'left_bins' : bin_labels['left'],
           'right_bins' : bin_labels['right'],
           't_start' : t_start,
           't_stop' : t_stop}

    return out


def get_decoding_data(sess_ind, data_folder, area, data_version, binsize, remove_nans,
                      min_speed, min_spikes_per_sample=1, min_spikes_per_neuron=1,
                      pyramidal_only=True, side=None):
    """
    get_decoding_data is a shortcut function which first prepares the data using
    unpack_session, then does a few extra things to return data which can readily
    be used to train a decoder.

    Specifically, it does the following things:
    1. You can select only data from one side of the maze
    2. Select time bins with enough running speed of the animal
    3. Select time bins with enough spikes
    4. Select neurons with enough total spikes
    5. Select neurons which are pyramidal

    Importantly, it returns a boolean vector representing the neurons which
    have been kept from the original ones (selected based on pyramidalness and/or
    number of total spikes. )

    """

    data = unpack_session(sess_ind, data_folder=data_folder,
                          area=area, data_version=data_version,
                          binsize_in_ms=binsize, remove_nans=remove_nans)

    if side is not None:
        print('Selecting only trial side {}'.format(side))
        ind = data['trial_side_label'] == side
        X = data['spikes'][ind, :]
        y = data['bin'][ind]
        speed = data['speed'][ind]
        side_list = data['trial_side_label'][ind]
    else:
        X = data['spikes']
        y = data['bin']
        speed = data['speed']
        side_list = data['trial_side_label']

    # ENOUGH SPEED
    X = X[speed >= min_speed, :]
    y = y[speed >= min_speed]
    side_list = side_list[speed >= min_speed]

    # ENOUGH SPIKES PER SAMPLE
    ind1 = X.sum(axis=1) >= min_spikes_per_sample
    X = X[ind1, :]
    y = y[ind1]
    side_list = side_list[ind1]
    # PYRAMIDAL
    pyr_mask = np.isin(list(range(X.shape[1])), data['is_pyramidal'])

    # ENOUGH SPIKES PER NEURON
    ind2 = X.sum(axis=0) >= min_spikes_per_neuron

    # COMBINE NEURON SELECTION: PYRAMIDAL AND MIN SPIKES
    if pyramidal_only:
        neuron_sel = np.logical_and(pyr_mask, ind2)
    else:
        neuron_sel = ind2

    X = X[:, neuron_sel]

    bins = np.unique(y)

    out = {'X' : X,
           'y' : y,
           'bins' : bins,
           'neuron_sel' : neuron_sel,
           'is_pyramidal' : pyr_mask,
           'side' : side_list}

    return out





def bin_spikes_sliding_window(trains, t_start, t_stop, wdw_size, slide_by,
                              fully_within=False):
    """
    If fully_within is set to True, all bins must be within t_start and
    t_stop. Otherwise we allow for the last bin to end past t_stop
    (we only require that the CENTER of the bin is within the event)
    """

    t_start = t_start.rescale(pq.ms)
    t_stop = t_stop.rescale(pq.ms)
    wdw_size = wdw_size.rescale(pq.ms)
    slide_by = slide_by.rescale(pq.ms)

    left_edges = np.arange(t_start, t_stop, slide_by) * pq.ms
    bins = [(e, e + wdw_size) for e in left_edges]

    # make sure that all the bin CENTERS are within the event
    bins = [b for b in bins if (b[0] + b[1]) / 2 <= t_stop]

    # make sure that all bins are fully within the event
    if fully_within:
        bins = [b for b in bins if b[1] <= t_stop]

    # prepare the bin centers (to return)
    bin_centers = [(b1 + b2) / 2 for b1, b2 in bins]

    num_bins = len(bins)  # Number of bins
    num_neurons = len(trains)  # Number of neurons
    neural_data = np.empty([num_bins, num_neurons])

    for i, train in enumerate(trains):
        spike_times = train.times.rescale(pq.ms)
        for t, bin in enumerate(bins):
            neural_data[t, i] = np.histogram(spike_times, bin)[0]
    return neural_data, bin_centers, bins



def bayesian_cross_validate(X, y, bin_size, cv, smooth_ratemap, smooth_sigma):
    y_pred_all, y_true_all = [], []
    for train_index, test_index in cv.split(X, y):
        X_train, X_test = X[train_index, :], X[test_index, :]
        y_train, y_test = y[train_index], y[test_index]

        dec = DKW_decoder(bin_size_train=bin_size,
                          bin_size_test=bin_size,
                          smooth_ratemap=smooth_ratemap,
                          smooth_sigma=smooth_sigma)

        dec.fit(X_train, y_train)
        y_pred = dec.predict(X_test)
        y_pred_all.append(y_pred)
        y_true_all.append(y_test)

    y_pred = np.hstack(y_pred_all)
    y_true = np.hstack(y_true_all)

    return y_true, y_pred



def coarsify_bins(y, mode='7bins'):
    if mode == 'leftmidright':
        left_ind = np.isin(y, np.arange(-22, -3))
        right_ind = np.isin(y, np.arange(4, 23))
        mid_ind = ~np.logical_or(left_ind, right_ind)

        y = np.zeros(y.shape[0])
        y[left_ind] = 0
        y[mid_ind] = 1
        y[right_ind] = 2

    elif mode == '7bins':
        left_ind1 = np.isin(y, np.arange(-22, -3)[0:6])
        left_ind2 = np.isin(y, np.arange(-22, -3)[6:12])
        left_ind3 = np.isin(y, np.arange(-22, -3)[12:19])

        right_ind1 = np.isin(y, np.arange(4, 23)[0:6])
        right_ind2 = np.isin(y, np.arange(4, 23)[6:12])
        right_ind3 = np.isin(y, np.arange(4, 23)[12:19])

        left_ind = np.logical_or.reduce((left_ind1, left_ind2, left_ind3))
        right_ind = np.logical_or.reduce(
            (right_ind1, right_ind2, right_ind3))

        mid_ind = ~np.logical_or(left_ind, right_ind)

        y = np.zeros(y.shape[0])
        y[left_ind3] = -1
        y[left_ind2] = -2
        y[left_ind1] = -3

        y[mid_ind] = 0
        y[right_ind1] = 1
        y[right_ind2] = 2
        y[right_ind3] = 3
    bins = np.unique(y).astype(int)
    return y, bins



def get_session_block(data_folder, sess_ind, epoch, data_version):
    F = loadmat(os.path.join(data_folder, 'F', 'F_units', 'F_All.mat'))['F']
    rat, day, session = F[sess_ind].dir.split('/')[-3:]
    recording_day = session[0:10]
    #recording_time = session[11:]
    if epoch == 'task':
        file_name = 'TS_session_{}_{}_{}.pkl'.format(sess_ind + 1, rat,
                                                     recording_day)
    elif np.isin(epoch, ['pre_sleep', 'post_sleep']):
        file_name = 'TS_sleep_session_{}_{}_{}.pkl'.format(sess_ind + 1, rat,
                                                     recording_day)
    dir = os.path.join(data_folder, 'neo', data_version, rat, session)
    full_file_path = os.path.join(dir, file_name)
    #print('Loading {}'.format(full_file_path))
    #print(full_file_path)
    bl = pickle.load(open(full_file_path, 'rb'))
    return bl


def get_animal_name(data_folder, sess_ind):
    F = loadmat(data_folder + '/F/F_units/F_All.mat')['F']
    rat, day, session = F[sess_ind].dir.split('/')[-3:]
    return rat


def get_rat_day_session(sess_ind):
    F = loadmat(os.path.join(DATA_FOLDER, 'F', 'F_units', 'F_All.mat'))['F']
    rat, day, session = F[sess_ind].dir.split('/')[-3:]
    return rat, day, session


def bin_PBEs(trains, PBEs_times, bin_size_in_ms, sliding_binning=True,
             slide_by_in_ms=10, pyramidal_only=False, extra_time_before=0,
             extra_time_after=0):

    try:
        extra_time_after.units
    except AttributeError:
        extra_time_after = extra_time_after * pq.ms


    try:
        extra_time_before.units
    except AttributeError:
        extra_time_before = extra_time_before * pq.ms

    out = {'PBEs_binned' : [],
           'time_points' : [],
           'time_point_in_pbe_mask' : [],
           'start_end_times' : PBEs_times,
           'bin_edges' : []}

    for pbe_start, pbe_stop in PBEs_times:

        t1 = pbe_start - extra_time_before
        t2 = pbe_stop + extra_time_after

        if pyramidal_only:
            trains = [t for t in trains if t.annotations['is_pyramidal']]

        if not sliding_binning:
            pbe_bs = elephant.conversion.BinnedSpikeTrain(trains,
                                                          binsize=bin_size_in_ms * pq.ms,
                                                          t_start=t1,
                                                          t_stop=t2)
            bin_centers = pbe_bs.bin_centers
            pbe_bs = pbe_bs.to_array().T
            bins = pbe_bs.bin_edges

        else:
            pbe_bs, bin_centers, bins = bin_spikes_sliding_window(trains, wdw_size=bin_size_in_ms * pq.ms,
                                               t_start=t1,
                                               t_stop=t2,
                                               slide_by=slide_by_in_ms * pq.ms,
                                               fully_within=False)

        in_pbe = np.logical_and(bin_centers > pbe_start, bin_centers <= pbe_stop)

        out['PBEs_binned'].append(pbe_bs)
        out['time_points'].append(bin_centers)
        out['time_point_in_pbe_mask'].append(in_pbe)
        out['bin_edges'].append(bins)

    for key in out.keys():
        assert len(out[key]) == len(out['start_end_times'])

    return out





def get_trajectory_side(y_pred_proba, area='Hippocampus', coarsify_mode='leftmidright'):

    if area == 'Hippocampus':
        prob_left = y_pred_proba[:, 0:29].sum() / y_pred_proba.sum()
        prob_right = y_pred_proba[:, 30:].sum() / y_pred_proba.sum()

    elif area == 'Perirhinal':
        if coarsify_mode == 'leftmidright':
            prob_left = y_pred_proba[:, 0].sum() / y_pred_proba.sum()
            prob_right = y_pred_proba[:, 2].sum() / y_pred_proba.sum()
        elif coarsify_mode == '7bins':
            prob_left = y_pred_proba[:, 0:3].sum() / y_pred_proba.sum()
            prob_right = y_pred_proba[:, 4:7].sum() / y_pred_proba.sum()

    if prob_left > prob_right:
        side = 'left'
    else:
        side = 'right'

    return side




def build_ratemap(X, y, bins, bin_size):
    ratemap = np.zeros([X.shape[1], bins.shape[0]])

    for neuron_ind, neuron in enumerate(X.T):

        for bin_ind, bin in enumerate(bins):
            in_loc = y == bin
            ratemap[neuron_ind, bin_ind] = neuron[in_loc].sum() / (bin_size * neuron[in_loc].shape[0])

    return ratemap



def correlate_ratemaps(ratemap1, ratemap2, method='pearson'):
    if method == 'pearson':
        corr_mat_full = np.corrcoef(ratemap1, ratemap2, rowvar=True)
    elif method == 'spearman':
        corr = scipy.stats.spearmanr(ratemap1, ratemap2, axis=1)
        corr_mat_full = corr.correlation
    corrcoef = corr_mat_full.diagonal(ratemap1.shape[0]).copy()
    assert corrcoef.shape[0] == ratemap1.shape[0]
    corrcoef[np.isnan(corrcoef)] = 0
    return corrcoef


def ratemap_correlation_with_shift_surrogates(real_ratemap, event_ratemap,
                                              correlation_method='pearson'):

    n_units = real_ratemap.shape[0]
    n_surrogates = event_ratemap.shape[1] - 1

    corrcoef_obs = correlate_ratemaps(real_ratemap, event_ratemap,
                                      method=correlation_method)

    corrcoef_surr = np.zeros([n_units, n_surrogates])

    shifts = range(1, real_ratemap.shape[1] - 1)
    for i, shift in enumerate(shifts):
        surr_ratemap = np.roll(real_ratemap, shift=shift, axis=1)
        corrcoef_surr[:, i] = correlate_ratemaps(surr_ratemap,
                                                 event_ratemap,
                                                 method=correlation_method)

    perc_score = np.zeros(n_units)
    for unit in range(n_units):
        perc_score[unit] = scipy.stats.percentileofscore(corrcoef_surr[unit, :],
                                                        corrcoef_obs[unit],
                                                         kind='weak')

    p_vals = np.zeros(n_units)
    for unit in range(n_units):
        obs_val = corrcoef_obs[unit]
        surr_vals = corrcoef_surr[unit, :]
        p_vals[unit] = (surr_vals >= obs_val).sum() / n_surrogates

    out = {}
    out['corrcoef_obs'] = corrcoef_obs
    out['corrcoef_surr'] = corrcoef_surr
    out['percentile_rank'] = perc_score
    out['p_values'] = p_vals

    return out



def get_pbe_modulation(trains, pbes_df):

    """
    This function determines whether a unit is upmodulated or downmodulated
    during times specified by pbes_df (compare to its normal activity), and it
    does so for a list of spike trains, returning a list of modulation values.
    A modulation value larger than 1 means that the cell fires more during
    the intervals than outside of them (firing computed as number of spikes
    divided by time).
    """

    # treat everything as unitless scalars first
    pbe_times = list(zip(pbes_df['start_time_in_ms'], pbes_df['end_time_in_ms']))

    t_start = trains[0].t_start.rescale(pq.ms).item()
    t_stop = trains[0].t_stop.rescale(pq.ms).item()

    non_pbe_times_0 = tuple((t_start, pbes_df['start_time_in_ms'].iloc[0]))

    non_pbe_times_1 = list(zip(pbes_df['end_time_in_ms'].iloc[:-1],
                               pbes_df['start_time_in_ms'].iloc[1:]))

    non_pbe_times_3 = tuple((pbes_df['end_time_in_ms'].iloc[-1], t_stop))

    non_pbe_times = []
    non_pbe_times.append(non_pbe_times_0)
    non_pbe_times = non_pbe_times + non_pbe_times_1
    non_pbe_times.append(non_pbe_times_3)

    # add units and rescale to seconds to match the spike trains
    pbe_times = (pbe_times * pq.ms).rescale(pq.s)
    non_pbe_times = (non_pbe_times * pq.ms).rescale(pq.s)

    pbe_modulation = []

    for i, train in enumerate(trains):
        spikes = train.times

        n_spikes_pbes, n_spikes_non_pbe = 0, 0
        duration_pbes, duration_non_pbe = 0 * pq.s, 0 * pq.s

        for t1, t2 in pbe_times:
            n_spikes = np.logical_and(spikes >= t1, spikes <= t2).sum()
            n_spikes_pbes = n_spikes_pbes + n_spikes
            duration_pbes = duration_pbes + (t2 - t1)

        for t1, t2 in non_pbe_times:
            n_spikes = np.logical_and(spikes > t1, spikes < t2).sum()
            n_spikes_non_pbe = n_spikes_non_pbe + n_spikes
            duration_non_pbe = duration_non_pbe + (t2 - t1)

        fr_pbe = n_spikes_pbes / duration_pbes
        fr_non_pbe = n_spikes_non_pbe / duration_non_pbe
        mod = fr_pbe / fr_non_pbe
        pbe_modulation.append(mod)

    return pbe_modulation





def get_data_real_event_ratemaps(data_folder, replay_folder,
                                 sess_ind, area, PBEs_settings,
                                 PBEs_area='Hippocampus',
                                 PBEs_decoding_settings='dkw',
                                 shuffle_type='column_cycle_shuffle',
                                 only_pyramidal=True,
                                 p_val_threshold_hc=0.2,
                                 time_bin_size_task_in_ms=200,
                                 min_speed=5,
                                 bin_size_pbe_in_ms='use_default',
                                 slide_by_pbe_in_ms='use_default'):

    # data_folder              = DATA_FOLDER
    #
    # sess_ind                 = 7
    # area                     = 'Perirhinal'
    # PBEs_settings            = 'tue2_HP_chop'
    # PBEs_area                = 'Hippocampus'
    # PBEs_decoding_settings   = 'dkw'
    #
    # shuffle_type             = 'column_cycle_shuffle'
    # only_pyramidal           = True
    # p_val_threshold_hc       = 0.2
    # time_bin_size_task_in_ms = 200
    # min_speed                = 5
    # bin_size_pbe_in_ms       = 20 # or 'use_default'
    # slide_by_pbe_in_ms       = 10 # or 'use_default'

    # LOAD DATA :
    area2_data = unpack_session(sess_ind, data_folder=data_folder,
                                area=area, binsize_in_ms=time_bin_size_task_in_ms)
    PBEs_folder = os.path.join(replay_folder, 'decoded_PBEs', PBEs_settings, PBEs_area, PBEs_decoding_settings)
    PBEs_file = os.path.join(PBEs_folder, 'decoded_PBEs_sess_{:02}_{}.pkl'.format(sess_ind, 'Hippocampus'))
    pbes_data = pickle.load(open(PBEs_file, 'rb'))


    # EXTRACT FIELD
    trains = area2_data['trains']
    X = area2_data['spikes']
    y = area2_data['bin']
    speed = area2_data['speed'][:, 0]
    is_pyramidal = area2_data['is_pyramidal']
    if bin_size_pbe_in_ms == 'use_default':
        bin_size_pbe_in_ms = pbes_data['decode_pars']['bin_size_pbe_in_ms']
    if slide_by_pbe_in_ms == 'use_default':
        slide_by_pbe_in_ms = pbes_data['decode_pars']['slide_by_pbe_in_ms']


    # SELECT DATA (SPEED AND PYRAMIDAL)
    X = X[speed > min_speed, :]
    y = y[speed > min_speed]

    if only_pyramidal:
        X = X[:, is_pyramidal]
        trains = [t for i, t in enumerate(trains) if np.isin(i, is_pyramidal)]


    # BIN PBES
    X_pbe = []
    y_pbe = []

    for i, RD in enumerate(pbes_data['detectors']):

        RD.sig_df.index = RD.sig_df['shuffle_type']
        RD.sig_df.drop('shuffle_type', axis=1)

        if RD.sig_df.loc[shuffle_type, 'p_val'] <= p_val_threshold_hc:

            pbe_ind = RD.sig_df['PBE_ind'][0]
            t1 = pbes_data['info'].loc[pbe_ind, 'start_time']
            t2 = pbes_data['info'].loc[pbe_ind, 'end_time']
            PBE_times = [[t1, t2]]

            event_area2 = bin_PBEs(trains, PBE_times,
                                   bin_size_in_ms=bin_size_pbe_in_ms,
                                   sliding_binning=True,
                                   slide_by_in_ms=slide_by_pbe_in_ms,
                                   pyramidal_only=False,
                                   extra_time_before=0 * pq.ms,
                                   extra_time_after=0 * pq.ms)

            pbe_spikes = event_area2['PBEs_binned'][0]

            y_event = np.interp(x=RD.time_bins,
                                xp=[RD.time_bins[0], RD.time_bins[-1]],
                                fp=[RD.start_location, RD.end_location]).round().astype(int)

            X_pbe.append(pbe_spikes)
            y_pbe.append(y_event)



    out = {'binned_spikes_task' : X,
           'binned_position_task' : y,
           'time_bin_size_task_in_ms' : time_bin_size_task_in_ms,
           'binned_spikes_pbe' : X_pbe,
           'binned_position_pbe' : y_pbe,
           'time_bin_size_pbe_in_ms' : bin_size_pbe_in_ms}

    return out


def find_index_nearest_value(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


def find_index_nearest_preceding(array, value):
    array = np.asarray(array)
    diff = value - array
    idx = diff[diff > 0].argmin()
    return idx


def get_average_running_speed(sess_ind, data_version, data_folder=DATA_FOLDER):
    bl = get_session_block(data_folder, sess_ind, 'task', data_version=data_version)

    av_speeds = []
    for segment in bl.segments[:-1]:
        ind1 = np.where(segment.events[0].labels == 'Block removed')[0][0]
        ind2 = np.where(segment.events[0].labels == 'Reward')[0][0]

        t1 = segment.events[0].times[ind1]
        t2 = segment.events[0].times[ind2]

        speed_times = segment.analogsignals[1].times
        mask = np.logical_and(speed_times >= t1, speed_times <= t2)
        speed_trial = segment.analogsignals[1].rescale(pq.cm / pq.s).__array__()[mask].flatten()
        av_speeds.append(np.nanmean(speed_trial))

    average_speed = np.nanmean(av_speeds)

    return average_speed


# add decoded positions
def get_position_bin(bin, bin_centers, bin_labels):
    if bin <= 0:
        ind = np.argwhere(bin_labels['left'] == bin)[0][0]
        pos = bin_centers['left'][ind]
    elif bin > 0:
        ind = np.argwhere(bin_labels['right'] == bin)[0][0]
        pos = bin_centers['right'][ind]
    return pos

def is_on_central_arm(bin):
    central_right = np.hstack((np.arange(24, 30), np.arange(0, 3)))
    central_left  = np.hstack((np.arange(-29, -23), np.arange(-2, 1)))
    return bool(np.isin(bin, central_left) or np.isin(bin, central_right))



def merge_duplicate_left_right_bins(bins):
    to_flip = np.hstack((np.arange(-29, -23), [-1, -2]))
    bins_flipped = bins.copy()
    bins_flipped[np.isin(bins_flipped, to_flip)] = -bins_flipped[np.isin(bins_flipped, to_flip)]
    return bins_flipped


def get_RSI(sess_ind, area):
    U = loadmat(os.path.join(DATA_FOLDER, 'unit.mat'))
    SS = loadmat(os.path.join(DATA_FOLDER, 'Sustained_stats',
                              'Stats_per_bin_sidearms_temporal_l_vs_r.mat'))
    unit_index = U['unit']['sess'] == sess_ind + 1
    unit_area = U['unit']['area'][unit_index]

    assert unit_area.shape[0] == SS['avgRsi1'][sess_ind].shape[0]

    if area == 'Perirhinal':
        ind = np.where(np.isin(unit_area, ['35', '36', 'b3536']))
    elif area == 'Barrel':
        ind = np.where(np.isin(unit_area, ['Br']))
    elif area == 'V1':
        ind = np.where(np.isin(unit_area, ['V', 'V1']))

    rsi = SS['avgRsi1'][sess_ind][ind]
    return rsi


def add_ripples_to_pbes_df(df):

    session_inds = df['sess_ind'].unique()
    dfs = []

    for sess_ind in session_inds:

        pbes_df = df[df['sess_ind'] == sess_ind]

        rat, day, session = get_rat_day_session(sess_ind)
        ripples_path = os.path.join(DATA_FOLDER, 'Ripples', rat, day, session, 'ripples.mat')
        session_path = os.path.join(DATA_FOLDER, 'SpikeTrl', rat, day, session, 'spikeTrl.mat')

        U = loadmat(os.path.join(DATA_FOLDER, 'unit.mat'))
        S = loadmat(session_path)

        first_ts = S['spike']['hdr'][sess_ind].FirstTimeStamp

        # select tetrode which has the most units
        unit_index = U['unit']['sess'] == sess_ind + 1
        unit_label = U['unit']['label'][unit_index]
        unit_tetrode = np.zeros_like(unit_label)
        for i in range(unit_tetrode.shape[0]):
            tet_num = int(unit_label[i][unit_label[i].find('TT') + 2])
            unit_tetrode[i] = tet_num
        tetrode_index = unit_tetrode - 1
        tetrode_ind_selected = pd.value_counts(tetrode_index).index[0]

        # get ripples
        ripples = loadmat(ripples_path)['ripples']
        ripple_selected = ripples[tetrode_ind_selected]
        ripple_times_in_ms = (ripple_selected.__getattribute__('indxRipplesTs') - first_ts) / 1e3

        ripple_all_times = np.hstack((ripple_times_in_ms[:, 0], ripple_times_in_ms[:, 1]))

        contains_ripple = []
        for pbe_start, pbe_stop in zip(pbes_df['start_time_in_ms'],
                                       pbes_df['end_time_in_ms']):
            n = np.logical_and(ripple_all_times >= pbe_start,
                               ripple_all_times <= pbe_stop).sum()
            contains_ripple.append(n)

        contains_ripple = np.array(contains_ripple)
        has_ripple = contains_ripple > 0
        pbes_df['has_ripple'] = has_ripple
        dfs.append(pbes_df)

    df_w_ripples = pd.concat(dfs)

    return df_w_ripples



def load_PBEs(sess_ind, PBEs_settings_name, PBEs_area, data_version, epoch='task'):
    PBEs_folder = os.path.join(REPLAY_FOLDER, 'results', 'PBEs',
                  'pbe_setting_{}_{}_{}'.format(PBEs_settings_name, PBEs_area,
                                                data_version))
    PBEs_file = os.path.join(PBEs_folder, 'PBEs_sess_{:02}_{}.pkl'.format(sess_ind, epoch))
    pbes = pickle.load(open(PBEs_file, 'rb'))
    return pbes



def load_decoded_PBEs(sess_ind, PBEs_settings_name, PBEs_area, data_version,
                      dec_settings_name, dec_area, epoch='task', load_light=False):
    output_folder = os.path.join(REPLAY_FOLDER, 'results', 'decoded_PBEs',
                                 'pbe_setting_{}_{}_{}_dec_setting_{}_{}'.format(
                                  PBEs_settings_name, PBEs_area, data_version, dec_settings_name, dec_area))
    if load_light:
        file_name = 'decoded_PBEs_sess_{:02}_{}_{}_light.pkl'.format(sess_ind, dec_area, epoch)
    else:
        file_name = 'decoded_PBEs_sess_{:02}_{}_{}.pkl'.format(sess_ind, dec_area, epoch)
    PBEs_file = os.path.join(output_folder, file_name)
    pbes = pickle.load(open(PBEs_file, 'rb'))
    return pbes





def load_bin_centers(data_folder, rat, day_folder, session_folder):
    dir_path = os.path.join(data_folder, 'PositionInfo_30', rat, day_folder, session_folder)
    path = loadmat(os.path.join(dir_path, 'path.mat'))
    xbins = np.hstack((path['cPathxM'], path['cPathxL'], path['cPathxR'])) / iPix2Cm
    ybins = np.hstack((path['cPathyM'], path['cPathyL'], path['cPathyR'])) / iPix2Cm
    bin_centers = np.hstack((xbins[:, np.newaxis], ybins[:, np.newaxis]))
    return bin_centers


def plot_maze(ax,c=None, alpha=1):
    if c is None:
        c = maze_color
    ax.plot(mz['xaxMInL'], mz['yaxMInL'], c=c, alpha=1, zorder=-2)
    ax.plot(mz['xaxMInR'], mz['yaxMInR'], c=c, alpha=1, zorder=-2)
    ax.plot(mz['xaxMOut'], mz['yaxMOut'], c=c, alpha=1, zorder=-2)




def is_on_maze(point, margin):
    ext  = maze.exterior.distance(point) < margin
    int1 = maze.interiors[0].distance(point) < margin
    int2 = maze.interiors[1].distance(point) < margin
    return (ext or int1 or int2)


def is_on_side(point, side, margin):
    """
    This function only excludes only point that are on the other side. i.e.
    if a point is on the centeral arm, it will be considered True for both
    'left' and 'right'. So is_on_side should really be interepreted as: can
    this point have occurred in a trial of this side?
    """
    if side == 'left':
        b = not (right_arm.exterior.distance(point) < margin)
    elif side == 'right':
        b = not (left_arm.exterior.distance(point) < margin)
    return b


def is_on(point, side, margin=4):

    if not isinstance(point, Point):
        point = Point(point)

    if side == 'left':
        b = (left_arm.exterior.distance(point) < margin)
    elif side == 'right':
        b = (right_arm.exterior.distance(point) < margin)
    elif side == 'center':
        b = central_arm.exterior.distance(point) < margin
    return b


def get_maze_arm_of_point(point):

    if not isinstance(point, Point):
        point = Point(point)

    dist = {}
    dist['left'] = left_arm.exterior.distance(point)
    dist['right'] = right_arm.exterior.distance(point)
    dist['central'] = central_arm.exterior.distance(point)
    #print(dist)
    return min(dist, key=dist.get)


def get_maze_arm_of_bin(bin):
    central_bins = np.hstack([np.arange(0, 3),
                              np.arange(0, -3, -1),
                              np.arange(24, 30),
                              np.arange(-24, -30, -1)])

    if np.isin(bin, central_bins):
        arm = 'central'
    elif ~np.isin(bin, central_bins) and bin > 0:
        arm = 'right'
    else:
        arm = 'left'
    return arm



def clean_position_segment(segment, margin=10):

    x_pos = segment.analogsignals[0][:, 0]
    y_pos = segment.analogsignals[0][:, 1]
    speed = segment.analogsignals[1]
    pos_units = segment.analogsignals[0].units
    spe_units = segment.analogsignals[1].units
    side  = segment.annotations['trial_side']

    for ind, (x, y) in enumerate(zip(x_pos, y_pos)):
        point = Point(x, y)
        if is_on_maze(point, margin) and is_on_side(point, side, margin):
            pass
        else:
            x_pos[ind] = np.nan * pos_units
            y_pos[ind] = np.nan * pos_units
            speed[ind] = np.nan * spe_units




def plot_clean_position_segment(ax, segment, margin=10):

    x_pos = segment.analogsignals[0][:, 0]
    y_pos = segment.analogsignals[0][:, 1]
    side  = segment.annotations['trial_side']

    for ind, (x, y) in enumerate(zip(x_pos, y_pos)):
        point = Point(x, y)
        if is_on_maze(point, margin):

            if is_on_side(point, side, margin):
                ax.scatter(x, y, c='g')
            else:
                ax.scatter(x, y, c='r')
        else:
            ax.scatter(x, y, c='grey')



def prepare_bins(bin_centers):
    """
    Given the bin centers, which have to be loaded for every session as they
    shift a bit across sessions, create two dictionaries, one containing
    the bin positions and one containing the bin labels, each keyed for left
    and right trials.
    """
    right_bins_lab = np.hstack((np.arange(24, 30), np.arange(0, 24),
                                np.repeat(np.nan, 21)))
    left_bins_lab  = np.hstack((np.arange(-24, -30, -1),  np.arange(0, -3, -1),
                                np.repeat(np.nan, 21), np.arange(-3, -24, -1)))
    left_bins = bin_centers[~np.isnan(left_bins_lab)]
    right_bins = bin_centers[~np.isnan(right_bins_lab)]
    right_bins_lab = right_bins_lab[~np.isnan(right_bins_lab)].astype(int)
    left_bins_lab = left_bins_lab[~np.isnan(left_bins_lab)].astype(int)

    bins = {'left'  : left_bins,
            'right' : right_bins}

    bin_labels = {'left' : left_bins_lab,
                  'right' : right_bins_lab}

    return bins, bin_labels




def bin_position_linearized(pos, bin_centers):
    """
    Simple linearized bins (every position gets unique bin)
    """
    dist = scipy.spatial.distance.cdist(pos, bin_centers)
    bin_index = np.argmin(dist, axis=1)
    return bin_index





def bin_position_linearized_LR(pos, trial_side, bin_centers,
                               bin_labels, max_distance=12):

    """
    :param position: Array of position at each time point
    :param trial_side: Array of trial side ('left' or 'right' at each position)
    :param bin_centers:
    :param bin_labels:
    :param max_distance:
    :return:
    """
    binned_position = []
    unassigned = []

    for p, side, in zip(pos, trial_side):
        if isinstance(side, str):
            dist    = scipy.spatial.distance.cdist(p[np.newaxis, :], bin_centers[side])
            ind     = np.argmin(dist)
            bin     = bin_centers[side][ind]
            bin_lab = bin_labels[side][ind]

            if (np.isnan(p).sum()==0) and (np.min(dist) < max_distance):
                binned_position.append(bin_lab)
            else:
                binned_position.append(np.nan)
                unassigned.append((p, bin, np.min(dist), side))
        elif np.isnan(side):
            binned_position.append(np.nan)
        else:
            raise ValueError('Something\'s up!')
    return np.array(binned_position), unassigned



def mark_locations(ax, lw=4, color=None, zorder=-1):

    if color is None:
        color = maze_color
    rat, day, session = get_rat_day_session(10)
    bin_centers = load_bin_centers(DATA_FOLDER, rat, day, session)
    bin_centers, bin_labels = prepare_bins(bin_centers)

    # start of trial
    x, y = get_position_bin(2, bin_centers, bin_labels)
    ax.plot([x - 10, x + 11], [y, y], c=color, lw=lw, zorder=zorder)

    # points of no return
    # x, y = get_position_bin(10, bin_centers, bin_labels)
    # ax.plot([x - 11, x + 7], [y + 2, y + 2], c=color, lw=lw, zorder=zorder)
    #
    # x, y = get_position_bin(-10, bin_centers, bin_labels)
    # ax.plot([x - 7, x + 11], [y + 2, y + 2], c=color, lw=lw, zorder=zorder)

    # return to central arm
    # x1, y1 = get_position_bin(25, bin_centers, bin_labels)
    # x2, y2 = get_position_bin(26, bin_centers, bin_labels)
    # x = (x1 + x2) / 2
    # y = (y1 + y2) / 2
    # ax.plot([x - 9, x + 9], [y, y], c=color, lw=lw, zorder=zorder)

    # reward 1
    x1, y1 = get_position_bin(17, bin_centers, bin_labels)
    x2, y2 = get_position_bin(18, bin_centers, bin_labels)
    x = (x1 + x2) / 2
    y = (y1 + y2) / 2
    ax.plot([x - 7, x + 7], [y + 7, y - 7], c=color, lw=lw, zorder=zorder)

    # reward 2
    x1, y1 = get_position_bin(-17, bin_centers, bin_labels)
    x2, y2 = get_position_bin(-18, bin_centers, bin_labels)
    x = (x1 + x2) / 2
    y = (y1 + y2) / 2
    ax.plot([x - 7, x + 7], [y - 7, y + 7], c=color, lw=lw, zorder=zorder)
    ax.axis('off')


def make_confusion_matrix_wrt_chance(y, y_pred, bin_labels, n_boot):
    cm = confusion_matrix(y, y_pred, labels=bin_labels)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    cm[np.isnan(cm)] = 0

    cm_shuf = np.zeros_like(cm)
    for n in range(n_boot):
        cm_boot = confusion_matrix(y, np.random.permutation(y_pred),
                                   labels=bin_labels)
        cm_boot = cm_boot.astype('float') / cm_boot.sum(axis=1)[:, np.newaxis]
        cm_boot[np.isnan(cm_boot)] = 0

        cm_shuf += cm_boot
    cm_shuf = cm_shuf / n_boot

    return cm - cm_shuf



def get_time_spent_per_task_phase(sess_ind, data_version, speed_threshold):

    bl = get_session_block(DATA_FOLDER, sess_ind, 'task', data_version=data_version)


    time_spent = {'iti' : 0 * pq.s,
                  'img' : 0 * pq.s,
                  'run' : 0 * pq.s,
                  'reward' : 0 * pq.s,
                  'return' : 0 * pq.s}

    for i in range(bl.segments[:-1].__len__()):

        segment = bl.segments[i]
        next_trial = bl.segments[i+1]
        #print(segment.events)

        rat_speed_signal  = segment.analogsignals[1].rescale(pq.cm / pq.s)
        rat_speed = rat_speed_signal.__array__().flatten()
        rat_speed_times = rat_speed_signal.times.rescale(pq.ms)

        labels = ['ITI', 'Image on', 'Block removed', 'Reward',
                  'Return to central arm']
        labels_short = ['iti', 'img', 'run', 'reward', 'return']

        for i, lab in enumerate(labels):

            ev1 = segment.events[0].times[np.where(segment.events[0].labels == lab)[0][0]]
            try:
                ev2 = segment.events[0].times[np.where(segment.events[0].labels == labels[i+1])[0][0]]
            except IndexError:
                ev2 = next_trial.events[0].times[np.where(next_trial.events[0].labels == 'ITI')[0][0]]

            mask = np.logical_and(rat_speed_times >= ev1, rat_speed_times < ev2)
            time_spent[labels_short[i]] += (rat_speed[mask] <= speed_threshold).sum() * 40 * pq.ms

    time_spent['all'] = np.sum([time_spent[l] for l in labels_short]) * pq.s

    return time_spent



def get_time_spent_per_task_phase_outcome(sess_ind, data_version, speed_threshold):

    bl = get_session_block(DATA_FOLDER, sess_ind, 'task', data_version=data_version)


    time_spent = {'correct': {'iti' : 0 * pq.s,
                              'img' : 0 * pq.s,
                              'run' : 0 * pq.s,
                              'reward' : 0 * pq.s,
                              'return' : 0 * pq.s},
                  'incorrect': {'iti' : 0 * pq.s,
                              'img' : 0 * pq.s,
                              'run' : 0 * pq.s,
                              'reward' : 0 * pq.s,
                              'return' : 0 * pq.s}}

    for i in range(bl.segments[:-1].__len__()):

        segment = bl.segments[i]
        next_trial = bl.segments[i+1]
        outc = segment.annotations['trial_outcome']
        #print(segment.events)

        rat_speed_signal  = segment.analogsignals[1].rescale(pq.cm / pq.s)
        rat_speed = rat_speed_signal.__array__().flatten()
        rat_speed_times = rat_speed_signal.times.rescale(pq.ms)

        labels = ['ITI', 'Image on', 'Block removed', 'Reward',
                  'Return to central arm']
        labels_short = ['iti', 'img', 'run', 'reward', 'return']

        for i, lab in enumerate(labels):

            ev1 = segment.events[0].times[np.where(segment.events[0].labels == lab)[0][0]]
            try:
                ev2 = segment.events[0].times[np.where(segment.events[0].labels == labels[i+1])[0][0]]
            except IndexError:
                ev2 = next_trial.events[0].times[np.where(next_trial.events[0].labels == 'ITI')[0][0]]

            mask = np.logical_and(rat_speed_times >= ev1, rat_speed_times < ev2)
            time_spent[outc][labels_short[i]] += (rat_speed[mask] <= speed_threshold).sum() * 40 * pq.ms

    time_spent['correct']['all'] = np.sum([time_spent['correct'][l] for l in labels_short]) * pq.s
    time_spent['incorrect']['all'] = np.sum([time_spent['incorrect'][l] for l in labels_short]) * pq.s

    return time_spent




def select_pbes(pbes_df_all, task_phase=None, replay_direction=None,
                has_ripple=None) :
    indices = []
    if task_phase != 'all' and task_phase is not None :
        indx = pbes_df_all[pbes_df_all['phase'] == task_phase].index
        indices.append(indx)
    if replay_direction != 'all' and replay_direction is not None :
        indx = pbes_df_all[
            pbes_df_all['replay_direction'] == replay_direction].index
        indices.append(indx)
    if has_ripple != 'all' and has_ripple is not None :
        indx = pbes_df_all[pbes_df_all['has_ripple'] == has_ripple].index
        indices.append(indx)
    if (task_phase == 'all' or task_phase is None) and \
       (replay_direction == 'all' or replay_direction is None) and \
       (has_ripple == 'all' or has_ripple is None):
        indx = pbes_df_all.index
        indices.append(indx)

    selected_final = list(set.intersection(*map(set, indices)))

    return pbes_df_all.loc[selected_final]





def compute_euclidean_error(y, y_pred, bin_centers, bin_labels):
    error = []
    for true_bin, pred_bin in zip(y, y_pred) :
        true_pos = get_position_bin(true_bin, bin_centers, bin_labels)
        pred_pos = get_position_bin(pred_bin, bin_centers, bin_labels)
        error.append(np.linalg.norm(true_pos - pred_pos))
    error = np.array(error)
    return error




def darken(color, ratio=0.5):
    """Creates a darker version of a color given by an RGB triplet.

    This is done by mixing the original color with black using the given
    ratio. A ratio of 1.0 will yield a completely black color, a ratio
    of 0.0 will yield the original color. The alpha values are left intact.
    """
    ratio = 1.0 - ratio
    red, green, blue, alpha = color
    return (red * ratio, green * ratio, blue * ratio, alpha)




def lighten_color(color, amount=0.5):
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])





def make_rgb_transparent(rgb, bg_rgb, alpha):
    return [alpha * c1 + (1 - alpha) * c2
            for (c1, c2) in zip(rgb, bg_rgb)]


def get_sig_both(df, bonferroni_correct=False, p_val_thr=0.05,
                 nulldistmethods=['rotate_ratemap', 'rotate_spikes']):
    sig_ids = []
    n_total = []
    for null_distribution_method in nulldistmethods:
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'all') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_distribution_method) &
                 (df['shift_amount'] == 0)]

        if bonferroni_correct:
            p = p_val_thr / dfx.shape[0]
        else:
            p = p_val_thr

        #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
        unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
        sig_ids.append(unit_ids)
        n_total.append(dfx.shape[0])

    sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

    return sig_both



def bin_occurrences_for_chi_square_test(bins):

    # bins_groups = [[0,1,-1, 2,-2,29,-29,28,-28],
    #                [27, -27, 26, -26, 25, -25, 24, -24],
    #                [3, 4, 5, 6, 7],
    #                [-3, -4, -5, -6, -7],
    #                [8, 9, 10, 11, 12],
    #                [-8, -9, -10, -11, -12],
    #                [13, 14, 15, 16, 17],
    #                [-13, -14, -15, -16, -17],
    #                [18, 19, 20, 21, 22, 23],
    #                [-18, -19, -20, -21, -22, -23]]


    bins_groups = [[0,1,-1, 2,-2,29,-29,28,-28],
                   [27, -27, 26, -26, 25, -25, 24, -24],
                   [3, 4, 5, 6, 7, 8, 9, 10],
                   [-3, -4, -5, -6, -7, -8, -9, -10],
                   [11, 12, 13, 14, 15, 16, 17],
                   [-11, -12, -13, -14, -15, -16, -17],
                   [18, 19, 20, 21, 22, 23],
                   [-18, -19, -20, -21, -22, -23]]

    bins_groups = {str(i) : bins_groups[i] for i in range(len(bins_groups))}

    bin_counts = {str(i) : 0 for i in range(len(bins_groups))}

    for i, b in enumerate(bins):
        for k in bins_groups.keys():
            if np.isin(b, bins_groups[k]):
                bin_counts[k] += 1

    # total = np.sum([bin_counts[str(k)] for k in range(len(bins_groups))])
    # bin_freq = [bin_counts[str(k)]/total for k in range(len(bins_groups))]
    bin_counts = [bin_counts[str(k)] for k in range(len(bins_groups))]

    return bin_counts



def linearize_trajectory(traj, side, direction):
    flip_dict1 = {i : j for i, j in zip(np.arange(29, -1, -1), np.arange(-1, -30, -1))}

    flip_dict2 = {i : j for i, j in zip(np.arange(-29, 1, 1), np.arange(1, 30, 1))}
    flip_dict3 = {0:0}

    flip_dict3.update(flip_dict1)
    flip_dict3.update(flip_dict2)


    if side == 'right' and direction == 'positive' and traj[0] >= traj[-1] :
        #print('case 1')
        ind = np.where(np.diff(traj) < 0)[0][0]

        for i in range(ind + 1) :
            traj[i] = flip_dict3[traj[i]]

    elif side == 'right' and direction == 'negative' and traj[-1] >= traj[0] :
        #print('case 2')
        ind = np.where(np.diff(traj) > 0)[0][0]

        for i in range(ind + 1, len(traj)) :
            # print(i)
            traj[i] = flip_dict3[traj[i]]

    elif side == 'left' and direction == 'positive' and traj[0] <= traj[-1] :
        #print('case 3')
        ind = np.where(np.diff(traj) > 0)[0][0]

        for i in range(ind + 1) :
            traj[i] = flip_dict3[traj[i]]

    elif side == 'left' and direction == 'negative' and traj[-1] <= traj[0] :
        #print('case 4')
        ind = np.where(np.diff(traj) < 0)[0][0]

        for i in range(ind + 1, len(traj)) :
            # print(i)
            traj[i] = flip_dict3[traj[i]]

    else:
        #print('no change: {}'.format(traj))
        pass
    return traj



def add_radial_direction(df):
    df_sel = df[~df['pbe_loc_bin'].isna()]
    pbe_loc = np.vstack((df_sel['x_pos'].values, df_sel['y_pos'])).T
    dec_start = np.vstack((df_sel['x_pos_dec_start'].values, df_sel['y_pos_dec_start'])).T
    dec_end = np.vstack((df_sel['x_pos_dec_end'].values, df_sel['y_pos_dec_end'])).T

    dist_start = np.linalg.norm((pbe_loc - dec_start), axis=1)
    dist_end = np.linalg.norm((pbe_loc - dec_end), axis=1)

    is_centrifugal = dist_start <= dist_end
    radial_dir = ['centrifugal' if t else 'centripetal' for t in is_centrifugal]
    df_sel['radial_direction'] = radial_dir
    dfxy = df_sel.copy()
    return dfxy


def add_ahead_behind(df):
    iti_ahead = np.hstack((np.arange(2, 23), np.arange(-2, -23, -1)))
    img_ahead = np.hstack((np.arange(2, 23), np.arange(-2, -23, -1)))
    run_ahead = np.hstack((np.arange(18, 29), np.arange(0, 2),
                           np.arange(-18, -29, -1), np.arange(0, -2, -1)))
    rew_ahead = np.hstack((np.arange(18, 29), np.arange(0, 2),
                           np.arange(-18, -29, -1), np.arange(0, -2, -1)))

    bins_ahead = {'iti' : iti_ahead,
                  'img' : img_ahead,
                  'run' : run_ahead,
                  'reward' : rew_ahead}

    df['ahead_behind'] = None
    for i, row in df.iterrows() :

        phase = row['phase']
        # x = row['pbe_loc_bin']
        bs = row['dec_start_bin']
        be = row['dec_end_bin']

        bins = bins_ahead[phase]
        if np.isin(bs, bins) and np.isin(be, bins) :
            df.loc[i, 'ahead_behind'] = 'ahead'
        else :
            df.loc[i, 'ahead_behind'] = 'behind'

    return df

def add_local_remote(df):
    df = df[df['maze_arm_occ'] != 'nanpos']
    df = df[~df['pbe_loc_bin'].isna()]

    ma_occ = df['maze_arm_occ']
    # ma_start = df['maze_arm_dec_start']
    # ma_end = df['maze_arm_dec_end']
    # df['local_remote'] = ['local' if a==b and a==c else 'remote' for a, b, c in
    #                        zip(ma_occ, ma_start, ma_end)]
    ma_start = df['maze_arm_dec_start']
    ma_end = df['maze_arm_dec_end']
    df['local_remote'] = ['remote' if a!=b else 'local' for a, b in
                           zip(ma_occ, ma_end)]

    return df


def add_local_remote_start_end(df):
    df = df[df['maze_arm_occ'] != 'nanpos']
    df = df[~df['pbe_loc_bin'].isna()]

    ma_occ = df['maze_arm_occ']
    ma_start = df['maze_arm_dec_start']
    ma_end = df['maze_arm_dec_end']
    df['local_remote'] = ['local' if a==b or a==c else 'remote' for a, b, c in
                           zip(ma_occ, ma_start, ma_end)]
    return df

def add_local_remote_v2(df):
    #df = df[df['maze_arm_occ'] != 'nanpos']
    #df = df[~df['pbe_loc_bin'].isna()]

    ma_occ = df['current_trial_side'] # THIS IS DIFFERENT
    # ma_start = df['maze_arm_dec_start']
    # ma_end = df['maze_arm_dec_end']
    # df['local_remote'] = ['local' if a==b and a==c else 'remote' for a, b, c in
    #                        zip(ma_occ, ma_start, ma_end)]
    ma_start = df['maze_arm_dec_start']
    ma_end = df['maze_arm_dec_end']
    df['local_remote_v2'] = ['remote' if a!=b else 'local' for a, b in
                           zip(ma_occ, ma_end)]

    return df


def add_reward_represented(df):
    start = df['dec_start_bin']
    end = df['dec_end_bin']
    rew_loc = [15, 16, 17, 18, 19, -15, -16, -17, -18, -19]
    df['traj_start_reward'] = ['yes' if np.isin(s, rew_loc) else 'no' for s in start]
    df['traj_end_reward'] = ['yes' if np.isin(e, rew_loc) else 'no' for e in end]
    df['traj_start_end_reward'] = ['yes' if a == 'yes' or b == 'yes' else 'no'
                                   for a, b in  zip(df['traj_start_reward'], df['traj_end_reward'])]
    return df


def match_previous_trial_side(df):
    #df = df[df['maze_arm_occ'] != 'nanpos']
    #df = df[~df['pbe_loc_bin'].isna()]

    ma_occ = df['previous_trial_side'] # THIS IS DIFFERENT
    # ma_start = df['maze_arm_dec_start']
    # ma_end = df['maze_arm_dec_end']
    # df['local_remote'] = ['local' if a==b and a==c else 'remote' for a, b, c in
    #                        zip(ma_occ, ma_start, ma_end)]
    ma_start = df['maze_arm_dec_start']
    ma_end = df['maze_arm_dec_end']
    df['match_previous_trial_side'] = ['nomatch' if a!=b else 'match' for a, b in
                           zip(ma_occ, ma_end)]

    return df


def match_current_trial_side(df):
    #df = df[df['maze_arm_occ'] != 'nanpos']
    #df = df[~df['pbe_loc_bin'].isna()]

    ma_occ = df['current_trial_side'] # THIS IS DIFFERENT
    # ma_start = df['maze_arm_dec_start']
    # ma_end = df['maze_arm_dec_end']
    # df['local_remote'] = ['local' if a==b and a==c else 'remote' for a, b, c in
    #                        zip(ma_occ, ma_start, ma_end)]
    ma_start = df['maze_arm_dec_start']
    ma_end = df['maze_arm_dec_end']
    df['match_current_trial_side'] = ['nomatch' if a!=b else 'match' for a, b in
                           zip(ma_occ, ma_end)]

    return df



def add_binned_replay_score(df):
    df['binned_replay_score'] = pd.qcut(df['replay_score'], q=[0, 0.5, 1],
                                        labels=['low_score', 'high_score'])
    return df

def add_binned_event_duration(df):
    df['binned_event_duration'] = pd.qcut(df['event_duration_in_ms'], q=[0, 0.5, 1],
                                  labels=['short', 'long'])
    return df



def add_correct_incorrect(df, data_version):

    sessions = df['sess_ind'].unique()
    dfs = []
    for sess_ind in sessions :
        bl = get_session_block(DATA_FOLDER, sess_ind, 'task',
                               data_version=data_version)

        trial_start_times = [s.t_start.rescale(pq.ms) for s in bl.segments[:-1]]
        trial_sides = [s.annotations['trial_side'] for s in bl.segments[:-1]]
        trial_outcomes = [s.annotations['trial_outcome'] for s in
                          bl.segments[:-1]]

        # add current and previous trial side
        trial_n, current_sides, previous_sides = [], [], []
        current_trial_outcomes, previous_trial_outcomes = [], []

        for time in df[df['sess_ind'] == sess_ind]['start_time_in_ms'] :
            idx = find_index_nearest_preceding(trial_start_times, time)
            current_side = trial_sides[idx]
            current_trial_outcome = trial_outcomes[idx]
            if idx == 0 :
                previous_side = np.nan
                previous_trial_outcome = np.nan
            else :
                previous_side = trial_sides[idx - 1]
                previous_trial_outcome = trial_outcomes[idx - 1]
            trial_n.append(idx)
            current_sides.append(current_side)
            previous_sides.append(previous_side)
            current_trial_outcomes.append(current_trial_outcome)
            previous_trial_outcomes.append(previous_trial_outcome)

        dx = pd.DataFrame(data={'sess_ind' : np.repeat(sess_ind, len(trial_n)),
                                'trial_n' : trial_n,
                                'current_trial_outcome' : current_trial_outcomes,
                                'previous_trial_outcome' : previous_trial_outcomes,
                                'current_side' : current_side})
        dfs.append(dx)

    tdf = pd.concat(dfs)

    sess_trial_pbes = ['{}{}'.format(r['sess_ind'], r['trial_n']) for i, r in
                       df.iterrows()]
    sess_trial_neo = ['{}{}'.format(r['sess_ind'], r['trial_n']) for i, r in
                      tdf.iterrows()]

    if len(set(sess_trial_pbes) ^ set(sess_trial_neo)) > 0:
        raise ValueError

    df['current_trial_outcome'] = None
    df['previous_trial_outcome'] = None

    for i, row in df.iterrows() :
        si = row['sess_ind']
        tn = row['trial_n']
        sel = tdf[(tdf['sess_ind'] == si) & (tdf['trial_n'] == tn)]
        df.loc[i, 'current_trial_outcome'] = sel['current_trial_outcome'].iloc[0]
        df.loc[i, 'previous_trial_outcome'] = sel['previous_trial_outcome'].iloc[0]

    return df


def add_coordination_per_event_deprecated(df, data_version, ratemap_settings) :
    # TODO here we keep only sessions for which there are no coordinated units
    nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
    bonferroni_correct = False
    p_val_thr = 0.05
    null_dist_method_for_plot_both_shuff = 'rotate_spikes'
    ratemap_areas = ['Perirhinal', 'Barrel']

    # --- LOAD REPLAY RATEMAPS RESULTS -----------------------------------------

    dfs_rm = []
    for ratemap_area in ratemap_areas :
        res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps',
                                  ratemap_settings)
        file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings,
                                                      ratemap_area)
        res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
        df_rm = res['df']
        df_rm['area'] = ratemap_area
        dfs_rm.append(df_rm)

    df_rm = pd.concat(dfs_rm)
    df_rm['unit_id'] = ['{}_{}_{}'.format(r['sess_ind'], r['unit'], r['area']) for
                     i, r in df_rm.iterrows()]

    sig_ids = []
    n_total = []
    for null_distribution_method in nulldistmet_plots :
        dfx = df_rm[(df_rm['phase'] == 'all') &
                    (df_rm['direction'] == 'all') &
                    (df_rm['has_ripple'] == 'all') &
                    (df_rm['null_method'] == null_distribution_method) &
                    (df_rm['shift_amount'] == 0)]

        if bonferroni_correct :
            p = p_val_thr / dfx.shape[0]
        else :
            p = p_val_thr

        # dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
        unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
        sig_ids.append(unit_ids)
        n_total.append(dfx.shape[0])

    sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

    dfx = df_rm[(df_rm['phase'] == 'all') &
                (df_rm['direction'] == 'all') &
                (df_rm['has_ripple'] == 'all') &
                (df_rm['null_method'] == null_dist_method_for_plot_both_shuff) &
                (df_rm['shift_amount'] == 0)]

    dfx = dfx[np.isin(dfx['unit_id'], sig_both)]

    # --- ADD COORDINATION COLUMN ----------------------------------------------

    df['coordination_perirhinal'] = None
    df['coordination_barrel'] = None

    sessions = df['sess_ind'].unique()

    for sess_ind in sessions:

        bl = get_session_block(DATA_FOLDER, sess_ind, 'task',
                               data_version=data_version)
        segment = merge_segments(bl.segments[:-1], bl.list_units,
                                 chop_return_central_arm=False)

        for area in ratemap_areas :
            trains = segment.filter(targdict={'area' : area},
                                    objects=neo.SpikeTrain)
            units = dfx[(dfx['sess_ind'] == sess_ind) & (dfx['area'] == area)][
                'unit']
            trains = [trains[k] for k in units]

            for i, row in df[df['sess_ind'] == sess_ind].iterrows():
                t_start = (row['start_time_in_ms'] * pq.ms).rescale(pq.s)
                t_end = (row['end_time_in_ms'] * pq.ms).rescale(pq.s)

                n_spikes = 0
                for train in trains :
                    n_spikes += train.time_slice(t_start, t_end).times.__len__()

                if n_spikes > 0 :
                    df.loc[i, 'coordination_{}'.format(area.lower())] = 'coordinated'
                else :
                    df.loc[i, 'coordination_{}'.format(area.lower())] = 'uncoordinated'

    # add simultaneous coordination in both areas
    df['coordination_both'] = None
    for i, row in df.iterrows():
        if row['coordination_perirhinal'] == row['coordination_barrel'] == 'coordinated':
            df.loc[i, 'coordination_both'] = 'coordinated'
        else:
            df.loc[i, 'coordination_both'] = 'uncoordinated'


    return df


def add_coordination_per_event(df, ratemap_area, data_version, ratemap_settings,
                               min_spikes=1) :

    nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
    bonferroni_correct = False
    p_val_thr = 0.05
    null_dist_method_for_plot_both_shuff = 'rotate_spikes'

    # --- LOAD REPLAY RATEMAPS RESULTS -----------------------------------------

    res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps',
                              ratemap_settings)
    file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings,
                                                  ratemap_area)
    res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
    df_rm = res['df']
    df_rm['area'] = ratemap_area
    df_rm['unit_id'] = ['{}_{}_{}'.format(r['sess_ind'], r['unit'], r['area']) for
                     i, r in df_rm.iterrows()]

    sig_ids = []
    n_total = []
    for null_distribution_method in nulldistmet_plots :
        dfx = df_rm[(df_rm['phase'] == 'all') &
                    (df_rm['direction'] == 'all') &
                    (df_rm['has_ripple'] == 'all') &
                    (df_rm['null_method'] == null_distribution_method) &
                    (df_rm['shift_amount'] == 0)]

        if bonferroni_correct :
            p = p_val_thr / dfx.shape[0]
        else :
            p = p_val_thr

        # dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
        unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
        sig_ids.append(unit_ids)
        n_total.append(dfx.shape[0])

    sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

    dfx = df_rm[(df_rm['phase'] == 'all') &
                (df_rm['direction'] == 'all') &
                (df_rm['has_ripple'] == 'all') &
                (df_rm['null_method'] == null_dist_method_for_plot_both_shuff) &
                (df_rm['shift_amount'] == 0)]

    dfx = dfx[np.isin(dfx['unit_id'], sig_both)]

    # --- ADD COORDINATION COLUMN ----------------------------------------------

    df['coordination_{}'.format(ratemap_area.lower())] = None

    sessions = dfx['sess_ind'].unique()

    for sess_ind in sessions:

        bl = get_session_block(DATA_FOLDER, sess_ind, 'task',
                               data_version=data_version)
        segment = merge_segments(bl.segments[:-1], bl.list_units,
                                 chop_return_central_arm=False)

        trains = segment.filter(targdict={'area' : ratemap_area},
                                objects=neo.SpikeTrain)
        units = dfx[(dfx['sess_ind'] == sess_ind) & (dfx['area'] == ratemap_area)]['unit']
        trains = [trains[k] for k in units]

        for i, row in df[df['sess_ind'] == sess_ind].iterrows():
            t_start = (row['start_time_in_ms'] * pq.ms).rescale(pq.s)
            t_end = (row['end_time_in_ms'] * pq.ms).rescale(pq.s)

            n_spikes = 0
            for train in trains :
                n_spikes += train.time_slice(t_start, t_end).times.__len__()

            if n_spikes >= min_spikes :
                df.loc[i, 'coordination_{}'.format(ratemap_area.lower())] = 'coordinated'
            else :
                df.loc[i, 'coordination_{}'.format(ratemap_area.lower())] = 'uncoordinated'

    df = df[np.isin(df['coordination_{}'.format(ratemap_area.lower())], ['coordinated', 'uncoordinated'])]

    return df



def add_coordination_per_event_add_spike_times(df, ratemap_area, data_version,
                                               ratemap_settings, add_all_cells=False,
                                               randomize_time=False) :

    nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
    bonferroni_correct = False
    p_val_thr = 0.05
    if add_all_cells:
        p_val_thr = 1
    null_dist_method_for_plot_both_shuff = 'rotate_spikes'

    # --- LOAD REPLAY RATEMAPS RESULTS -----------------------------------------

    res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps',
                              ratemap_settings)
    file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings,
                                                  ratemap_area)
    res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
    df_rm = res['df']
    df_rm['area'] = ratemap_area
    df_rm['unit_id'] = ['{}_{}_{}'.format(r['sess_ind'], r['unit'], r['area']) for
                     i, r in df_rm.iterrows()]

    sig_ids = []
    n_total = []
    for null_distribution_method in nulldistmet_plots :
        dfx = df_rm[(df_rm['phase'] == 'all') &
                    (df_rm['direction'] == 'all') &
                    (df_rm['has_ripple'] == 'all') &
                    (df_rm['null_method'] == null_distribution_method) &
                    (df_rm['shift_amount'] == 0)]

        if bonferroni_correct :
            p = p_val_thr / dfx.shape[0]
        else :
            p = p_val_thr

        # dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
        unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
        sig_ids.append(unit_ids)
        n_total.append(dfx.shape[0])

    sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

    dfx = df_rm[(df_rm['phase'] == 'all') &
                (df_rm['direction'] == 'all') &
                (df_rm['has_ripple'] == 'all') &
                (df_rm['null_method'] == null_dist_method_for_plot_both_shuff) &
                (df_rm['shift_amount'] == 0)]

    dfx = dfx[np.isin(dfx['unit_id'], sig_both)]


    # --- ADD COORDINATION COLUMN ----------------------------------------------

    df['coordination_{}'.format(ratemap_area.lower())] = None
    df['coord_spikes_{}'.format(ratemap_area.lower())] = None

    sessions = dfx['sess_ind'].unique()

    for sess_ind in sessions:

        bl = get_session_block(DATA_FOLDER, sess_ind, 'task',
                               data_version=data_version)
        segment = merge_segments(bl.segments[:-1], bl.list_units,
                                 chop_return_central_arm=False)

        trains = segment.filter(targdict={'area' : ratemap_area},
                                objects=neo.SpikeTrain)
        units = dfx[(dfx['sess_ind'] == sess_ind) & (dfx['area'] == ratemap_area)]['unit']
        n_units = len(units)
        trains = [trains[k] for k in units]

        for i, row in df[df['sess_ind'] == sess_ind].iterrows():

            t_start = (row['start_time_in_ms'] * pq.ms).rescale(pq.s)
            t_end = (row['end_time_in_ms'] * pq.ms).rescale(pq.s)

            if randomize_time:
                rand_t = np.random.randint(2, 20)
                rand_sign = np.random.randint(2)
                if rand_sign == 0:
                    t_start = t_start + rand_t*pq.s
                    t_end = t_end + rand_t*pq.s
                elif rand_sign == 1:
                    t_start = t_start - rand_t*pq.s
                    t_end = t_end - rand_t*pq.s

                df.loc[i, 'start_time_in_ms'] = t_start * 1000
                df.loc[i, 'end_time_in_ms'] = t_end * 1000

            n_spikes = 0
            train_slices, n_spikes_all = [], []
            for train in trains :
                train_slice = train.time_slice(t_start, t_end).times
                n_spikes += train_slice.__len__()
                train_slices.append(train_slice)
                n_spikes_all.append(n_spikes)
            if n_spikes > 0 :
                df.loc[i, 'coordination_{}'.format(ratemap_area.lower())] = 'coordinated'
            else :
                df.loc[i, 'coordination_{}'.format(ratemap_area.lower())] = 'uncoordinated'

            best_ind = np.where(np.array(n_spikes_all) == max(n_spikes_all))[0][0]
            best_train = train_slices[best_ind]
            df.at[i, 'coord_spikes_{}'.format(ratemap_area.lower())] = best_train
            df.loc[i, 'n_units_{}'.format(ratemap_area.lower())] = n_units

    df = df[np.isin(df['coordination_{}'.format(ratemap_area.lower())], ['coordinated', 'uncoordinated'])]

    return df




def add_coordination_both_areas(df, data_version, ratemap_settings,
                                min_spikes=1):

    dp = add_coordination_per_event(df.copy(),
                                    ratemap_area="Perirhinal",
                                    ratemap_settings=ratemap_settings,
                                    data_version=data_version,
                                    min_spikes=min_spikes)

    db = add_coordination_per_event(df.copy(), ratemap_area="Barrel",
                                    ratemap_settings=ratemap_settings,
                                    data_version=data_version,
                                    min_spikes=min_spikes)

    cols_to_use = db.columns.difference(dp.columns)

    dfb = dp.merge(db[cols_to_use], left_index=True, right_index=True,
                  how='inner')

    dfb['coordination_both'] = ['coordinated' if x == y == 'coordinated'
                               else 'uncoordinated' for x, y in
                               zip(dfb['coordination_perirhinal'],
                                   dfb['coordination_barrel'])]
    return dfb


def get_pbes_contrast(df, contrast):
    if contrast == 'replay_direction' :
        df1 = df[(df['replay_direction'] == 'positive')]
        df2 = df[(df['replay_direction'] == 'negative')]
        name1, name2 = 'positive', 'negative'

    elif contrast == 'radial_direction':
        df1 = df[(df['radial_direction'] == 'centrifugal')]
        df2 = df[(df['radial_direction'] == 'centripetal')]
        name1, name2 = 'centrifugal', 'centripetal'

    elif contrast == 'joint_phase' :
        df1 = df[(np.isin(df['phase'], ['iti', 'img']))]
        df2 = df[(np.isin(df['phase'], ['run', 'reward']))]
        name1, name2 = 'itiimg', 'runrew'

    elif contrast == 'ahead_behind':
        df1 = df[(df['ahead_behind'] == 'ahead')]
        df2 = df[(df['ahead_behind'] == 'behind')]
        name1, name2 = 'ahead', 'behind'

    elif contrast == 'local_remote':
        df1 = df[(df['local_remote'] == 'local')]
        df2 = df[(df['local_remote'] == 'remote')]
        name1, name2 = 'local', 'remote'

    elif contrast == 'replay_score':
        df1 = df[(df['binned_replay_score'] == 'low_score')]
        df2 = df[(df['binned_replay_score'] == 'high_score')]
        name1, name2 = 'low_score', 'high_score'

    elif contrast == 'event_duration':
        df1 = df[(df['binned_event_duration'] == 'short')]
        df2 = df[(df['binned_event_duration'] == 'long')]
        name1, name2 = 'short', 'long'

    elif contrast == 'coordination_perirhinal':
        df1 = df[(df['coordination_perirhinal'] == 'coordinated')]
        df2 = df[(df['coordination_perirhinal'] == 'uncoordinated')]
        name1, name2 = 'coordinated', 'uncoordinated'

    elif contrast == 'coordination_barrel':
        df1 = df[(df['coordination_barrel'] == 'coordinated')]
        df2 = df[(df['coordination_barrel'] == 'uncoordinated')]
        name1, name2 = 'coordinated', 'uncoordinated'

    elif contrast == 'coordination_both':
        df1 = df[(df['coordination_both'] == 'coordinated')]
        df2 = df[(df['coordination_both'] == 'uncoordinated')]
        name1, name2 = 'coordinated', 'uncoordinated'

    elif contrast == 'current_trial_outcome':
        df1 = df[(df['current_trial_outcome'] == 'correct')]
        df2 = df[(df['current_trial_outcome'] == 'incorrect')]
        name1, name2 = 'correct', 'incorrect'

    elif contrast == 'previous_trial_outcome':
        df1 = df[(df['previous_trial_outcome'] == 'correct')]
        df2 = df[(df['previous_trial_outcome'] == 'incorrect')]
        name1, name2 = 'correct', 'incorrect'

    elif np.isin(contrast,['traj_start_reward', 'traj_end_reward', 'traj_start_end_reward']):
        df1 = df[(df[contrast] == 'yes')]
        df2 = df[(df[contrast] == 'no')]
        name1, name2 = 'yes', 'np'

    else:
        raise ValueError

    pbes_dfs = [df1, df2]
    names = [name1, name2]
    return pbes_dfs, names



def get_significance_mask(dfx, variable='corrcoef_debiased', test_value=0,
                          stat_test='t-test', correction='fdr_by', sign_alpha=0.05):
    p_vals = []
    for shift_amount in np.sort(dfx['shift_amount'].unique()):
        ccd = dfx[dfx['shift_amount'] == shift_amount][variable]
        if stat_test == 'sign_test':
            #m, p_val = statsmodels.stats.descriptivestats.sign_test(ccd, test_value)
            m, p_val = scipy.stats.wilcoxon(ccd - test_value, alternative='greater')
        elif stat_test == 't-test':
            t, p_val = scipy.stats.ttest_1samp(ccd, test_value)
        p_vals.append(p_val)
    #print(p_vals)
    mask, pvals_corr, alphacs, alphacb = multipletests(p_vals, alpha=sign_alpha,
                                                       method=correction)
    return mask, pvals_corr


def get_significance_mask_split(df1, df2, variable='corrcoef_debiased',
                                stat_test='t-test', correction='fdr_by',
                                sign_alpha=0.05):

    p_vals = []
    for shift_amount in np.sort(df1['shift_amount'].unique()):
        var1 = df1[df1['shift_amount'] == shift_amount][variable]
        var2 = df2[df2['shift_amount'] == shift_amount][variable]
        # TODO CHECK IF THESE ARE PAIRED
        if stat_test == 'sign_test':
            m, p_val = scipy.stats.mannwhitneyu(var1, var2)
        elif stat_test == 't-test':
            t, p_val = scipy.stats.ttest_ind(var1, var2)
        p_vals.append(p_val)
    #print(p_vals)
    mask, pvals_corr, alphacs, alphacb = multipletests(p_vals, alpha=sign_alpha,
                                                       method=correction)

    return mask, pvals_corr




def add_new_speed(df, data_version, time_before_in_ms, time_after_in_ms,
                  position_sigma=5, binsize_interp_pos=10,
                  column_name='average_speed_new'):

    # pos sigma gets multiplied by 50ms (video frame interpolated)



    speed_sess = {}
    for sess_ind in df['sess_ind'].unique():
        out = unpack_session(sess_ind, DATA_FOLDER, 'Hippocampus',
                             data_version=data_version, binsize_in_ms=binsize_interp_pos,
                             remove_nans=True,
                             interp_and_smooth_speed=False,
                             speed_sigma=None)
        pos = out['position']
        times = out['times']

        # pos, times = get_position(data_folder=DATA_FOLDER, data_version=data_version,
        #                           sess_ind=sess_ind)

        xpos = scipy.ndimage.filters.gaussian_filter1d(pos[:, 0],  sigma=position_sigma)
        ypos = scipy.ndimage.filters.gaussian_filter1d(pos[:, 1], sigma=position_sigma)

        xy = np.hstack((xpos[:, None], ypos[:, None]))
        a = xy[:-1]
        b = np.roll(xy, -1, axis=0)[:-1]
        dxy = np.linalg.norm(a - b, axis=1)
        smooth_speed = np.divide(dxy, np.diff(times).rescale(pq.s))
        smooth_speed = np.hstack((smooth_speed, smooth_speed[-1]))

        speed_sess[sess_ind] = {}
        speed_sess[sess_ind]['speed'] = smooth_speed.flatten()
        speed_sess[sess_ind]['times'] = times

    df[column_name] = None

    for i, row in df.iterrows() :
        speed = speed_sess[row['sess_ind']]['speed']
        times = speed_sess[row['sess_ind']]['times']

        t1, t2 = row['start_time_in_ms'] - time_before_in_ms, row['end_time_in_ms'] + time_after_in_ms

        speed_sel = speed[np.logical_and(times >= t1, times < t2)]
        #print(speed_sel)
        df.loc[i, column_name] = np.nanmean(speed_sel)

    return df




def get_position(data_folder, data_version, sess_ind):

    bl = get_session_block(data_folder, sess_ind, epoch='task',
                           data_version=data_version)

    bl.segments = bl.segments[:-1]

    # EXTRACT SPIKES AND POSITION
    segment = merge_segments(bl.segments, bl.list_units)

    rat_position_signal = segment.irregularlysampledsignals[0]
    rat_position_times = rat_position_signal.times.rescale(pq.ms)

    pos = rat_position_signal.__array__()
    times = rat_position_times

    # REMOVE DATA FROM BAD TRIALS ---------------------------------------------
    # which would otherwise have no spikes and flat position
    removed_trials = []
    for seg1, seg2 in zip(bl.segments[:-1], bl.segments[1:]):
        if (seg2.t_start - seg1.t_stop) > 5 * pq.s:
            removed_trials.append((seg1.t_stop, seg2.t_start))
    print(removed_trials)

    removed = np.repeat(False, times.shape[0])
    for rt in removed_trials:
        in_removed_trial = np.logical_and(times >= rt[0],
                                          times <= rt[1])
        removed = np.logical_or(removed, in_removed_trial)

    pos[removed, :] = np.nan

    # if remove_nans:
    #     ind = ~np.isnan(binned_pos)
    #     X = X[ind, :]
    #     interpolated_position = interpolated_position[ind, :]
    #     interpolated_speed = interpolated_speed[ind]
    #     trial_side = np.array(trial_side)[ind]
    #     trial_side_label = np.array(trial_side_label)[ind]
    #     times = times[ind]

    return pos, times



def load_replay(sessions, PBEs_settings_name, PBEs_area,
                data_version, dec_settings_name, dec_area,
                min_pbe_duration_in_ms, shuffle_types, shuffle_p_vals,
                group_iti_and_return, return_not_sig=False,
                max_average_speed=None, NEWSPEED=False,
                correct_incorrect=True,
                exclude_run_phase=True):

    pbes_dfs, sig_dfs = [], []

    for sess_ind in sessions :
        print(sess_ind)
        try :
            pbes = load_decoded_PBEs(sess_ind, PBEs_settings_name, PBEs_area,
                                     data_version, dec_settings_name, dec_area,
                                     load_light=True)
            pbes_dfs.append(pbes['pbes'])
            sig_dfs.append(pbes['sig_df'])



        except FileNotFoundError :
            print('No decoded PBEs for session {}'.format(sess_ind))

    df = pd.concat(pbes_dfs)
    sig_df = pd.concat(sig_dfs)

    if NEWSPEED :
        print('adding new speed')
        time_before_in_ms = 0
        time_after_in_ms = 0
        position_sigma = 10
        binsize_interp_pos = 50
        df = add_new_speed(df, time_before_in_ms=time_before_in_ms,
                           time_after_in_ms=time_after_in_ms,
                           binsize_interp_pos=binsize_interp_pos,
                           position_sigma=position_sigma,
                           data_version=data_version,
                           column_name='average_speed')

    df['average_speed'] = [i.item() for i in df['average_speed']]

    print(np.isnan(df['average_speed']).sum())

    df = add_ripples_to_pbes_df(df)

    if correct_incorrect :
        df = add_correct_incorrect(df, data_version=data_version,)

    # group ITI and return
    if group_iti_and_return :
        df['phase'] = df['phase'].replace(to_replace='return', value='iti')

    # filter event duration
    df = df[df['event_duration_in_ms'] >= min_pbe_duration_in_ms]

    # If location of the animal during img has artifacts fix that
    img_bins = [1, -1, 2, -2, 26, 27, 28, 29, 0, -26, -27, -28, -29]
    x = df['pbe_loc_bin'].values
    ind = np.logical_and(~np.isin(df['pbe_loc_bin'], img_bins),
                         df['phase'] == 'img')
    x[ind] = 0
    df['pbe_loc_bin'] = x

    # filter events by significance
    sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types,
                                            p_vals=shuffle_p_vals)
    print(sel_pbes_sig)
    df_not_sig = df[~np.isin(df.index, sel_pbes_sig)]
    df = df[np.isin(df.index, sel_pbes_sig)]

    # fix maze_arm_dec_start and maze_arm_dec_end
    for i, row in df.iterrows() :
        df.loc[i, 'maze_arm_dec_start'] = get_maze_arm_of_bin(
            row['dec_start_bin'])
        df.loc[i, 'maze_arm_dec_end'] = get_maze_arm_of_bin(row['dec_end_bin'])

    if max_average_speed is not None :
        print('filtering speed')
        df = df[df['average_speed'] <= max_average_speed]

    df['pbe_side_wcentral'] = df['pbe_side'].copy()
    for i, row in df.iterrows() :
        if is_on_central_arm(row['dec_start_bin']) and is_on_central_arm(
                row['dec_end_bin']) :
            df.loc[i, 'pbe_side_wcentral'] = 'central'

    for col in ['percentage_active', 'dist_traversed_cm', 'max_jump_dist',
                'mean_jump_dist', 'sharpness'] :
        df[col] = pd.to_numeric(df[col])

    if exclude_run_phase:
        df = df[np.isin(df['phase'], ['iti', 'img', 'reward'])]

    if return_not_sig :
        return df, df_not_sig
    else :
        return df