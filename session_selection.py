import pickle
from constants import *
from plotting_style import *

data_version = 'dec16'
settings_name = 'dec18'
area = 'Hippocampus'


# --------
score_threshold_cm = 1000 # to determine which confusion matrices to plot
min_units_hc = 20
# -----------

# --- LOAD RESULTS ------------------------------------------------------------

output_folder = os.path.join(REPLAY_FOLDER, 'results', 'dec')
output_file = 'dec_rs_{}_{}_{}.pkl'.format(settings_name, area, data_version)
out = pickle.load(open(os.path.join(output_folder, output_file), 'rb'))

sess_info = out['sess_info']
dec_rs = out['dec_rs']
decode_modality = out['pars']['decode_modality']
bin_labels = out['pars']['bin_labels']
score_col = 'dec_score_{}'.format(area_code_labels[area])

# --- DETERMINE HC SESSIONS BASED ON DECODING SCORE AND MIN UNITS -------------

sel_sess_hc = sess_info[sess_info[score_col] < score_threshold_cm]
sel_sess_hc = sel_sess_hc.loc[sel_sess_hc['nHC'] >= min_units_hc]

# --- FOR THE RATEMAPS, SELECT ALL SESSIONS WITH AT LEAST ONE UNIT ------------

sessions_HC_replay = sel_sess_hc.index.values

sessions_PR_ratemaps = sel_sess_hc[sel_sess_hc['nPR'] > 0].index.values

sessions_BR_ratemaps = sel_sess_hc[sel_sess_hc['nBR'] > 0].index.values

sessions_V1_ratemaps = sel_sess_hc[sel_sess_hc['nV1'] > 0].index.values

ratemap_sessions = {'Perirhinal' : sessions_PR_ratemaps,
                    'Barrel' : sessions_BR_ratemaps,
                    'V1' : sessions_V1_ratemaps}