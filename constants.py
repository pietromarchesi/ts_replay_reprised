import os
import pandas as pd
import platform

desired_width = 600
pd.set_option('display.width', desired_width)
pd.set_option("display.max_columns", 14)

user = os.environ['USER']

if user == 'pietro':

    if platform.system() == 'Linux':
        DATA_FOLDER = '/media/pietro/bigdata/neuraldata/touch_see/'
        REPLAY_FOLDER = '/media/pietro/bigdata/neuraldata/ts_replay_reprised/'
        PLOTS_FOLDER = '/media/pietro/bigdata/neuraldata/ts_replay_reprised/may23plots'


    elif platform.system() == 'Darwin':
        DATA_FOLDER = '/Users/pietro/data/touch_see/'
        REPLAY_FOLDER = '/Users/pietro/data/ts_replay_reprised/'
        PLOTS_FOLDER =  '/Users/pietro/data/ts_replay_reprised/finalplots'

elif os.environ['USER'] == 'pmarche1':
    DATA_FOLDER = '/data/pmarche1/touch_see/'
    REPLAY_FOLDER = '/data/pmarche1/ts_replay_reprised/'

iPix2Cm = 3.4573
session_indices = list(range(46))
good_sessions=[4,7,9,10,27,29]
mean_distance_between_bins = 8.43 # cm

# sessions_HC_replay = [5, 7, 8, 9, 10, 12, 27, 28, 29]
#
# sessions_BR_ratemaps = [5, 8, 9, 10, 12, 27, 29]
#
# sessions_PR_ratemaps = [7, 9, 10, 27, 28, 29]
#
# sessions_HC_PR_replay = [7, 9, 10, 27, 28, 29]


# --- REPLAY PARAMETERS ---
SPEED_THRESHOLD = 3
shuffle_types  = ['column_cycle_shuffle', 'place_field_rotation_shuffle',
                  'unit_identity_shuffle']

shuffle_p_vals = [0.05, 0.05, 0.05]
min_pbe_duration_in_ms    = 50
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
phases = ['iti', 'img', 'reward']
phases_plus_all = ['all', 'iti', 'img', 'reward']
group_iti_and_return = True


path = '/Users/pietro/data/ts_replay_reprised/new_plots/png/' \
       'lfp_events_with_osc_5_12_1sec_only_peak_0.2/EXCLUDED2/*'

import glob

excluded = glob.glob(path)

excluded_ids = ['_'.join(e.split('/')[-1].split('_')[3:6]) for e in excluded]

