import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(sys.path[0]))))
import numpy as np
import pandas as pd
import neo
import quantities as pq
import elephant
import pickle
import scipy
from loadmat import loadmat
from session_selection import DATA_FOLDER, REPLAY_FOLDER, sessions_HC_replay
from replaydetector.pbesdetector import PBEsDetector
from utils import find_index_nearest_value, find_index_nearest_preceding
from utils import get_session_block
from utils import merge_segments
from utils import load_bin_centers, prepare_bins
from utils import bin_position_linearized_LR
from utils import get_maze_arm_of_point

import warnings
warnings.filterwarnings("ignore")
warnings.filterwarnings("ignore", category=FutureWarning)

setting_name            = 'dec19k20'
data_version            = 'dec16'

area                    = 'Hippocampus'
epoch                   = 'task'   # sleep, pre_sleep, post_sleep, task

sessions                = sessions_HC_replay
data_folder             = DATA_FOLDER
output_folder           = os.path.join(REPLAY_FOLDER, 'results', 'PBEs',
                                      'pbe_setting_{}_{}_{}'.format(setting_name,
                                                             area, data_version))

# PBE DETECTION PARAMETERS ----------------------------------------------------
mua_kernel_sigma_in_ms                    = 20
mua_kernel_size_in_ms                     = mua_kernel_sigma_in_ms * 6
mua_only_selected_cells                   = True
n_sd_above_the_mean                       = 3 # peaks defined as n standard deviations above the mean
adjust_bounds_to_first_last_spikes        = True
adjust_bounds_only_selected_cells         = True
min_average_speed                         = None #0.3
max_average_speed                         = 12
min_duration_in_ms                        = 40
max_duration_in_ms                        = 2000
min_n_spikes_only_selected_cells          = True
min_n_spikes                              = None
min_percentage_active_only_selected_cells = True # considering all neurons
min_percentage_active                     = 10
min_n_active_only_selected_cells          = True
min_n_active                              = 4


for sess_ind in sessions:
    print('\n\n\n\n ~ PBE DETECTION FOR SESSION {}, AREA: {} ~   '.format(sess_ind, area))

    # --- GROUP OTHER PARAMETERS --------------------------------------------------

    pars = {'setting_name'            : setting_name,
            'area'                    : area,
            'epoch'                   : epoch,
            'sess_ind'                : sess_ind}

    # --- LOAD NEO BLOCK ----------------------------------------------------------

    bl = get_session_block(data_folder, sess_ind, epoch, data_version=data_version)

    rat = bl.annotations['animal_ID']
    day_folder = bl.annotations['day_folder']
    session_folder = bl.annotations['session_folder']
    bin_centers_combined = load_bin_centers(data_folder, rat, day_folder,
                                            session_folder)
    bin_centers, bin_labels = prepare_bins(bin_centers_combined)

    if epoch == 'task':
        segment = merge_segments(bl.segments[:-1], bl.list_units,
                                 chop_return_central_arm=False)
        rat_speed_signal = segment.irregularlysampledsignals[1].rescale(pq.cm / pq.s)
        rat_speed = rat_speed_signal.__array__().flatten()
        rat_speed_times = rat_speed_signal.times.rescale(pq.ms)

        rat_position_signal = segment.irregularlysampledsignals[0]
        rat_position = rat_position_signal.__array__()
        rat_position_times = rat_position_signal.times.rescale(pq.ms)

        event_times = np.hstack([s.events[0].times.rescale(pq.ms) for s in bl.segments[:-1]])
        event_labels = np.hstack([s.events[0].labels for s in bl.segments[:-1]])

        trial_start_times = [s.t_start.rescale(pq.ms) for s in bl.segments[:-1]]
        trial_sides = [s.annotations['trial_side'] for s in bl.segments[:-1]]
        trial_n2 = [s.annotations['trial_number'] for s in bl.segments[:-1]]


    elif epoch == 'pre_sleep':
        segment = bl.segments[0]
        assert segment.name == 'pre-sleep'
        rat_speed = None
        rat_speed_times = None

    elif epoch == 'post_sleep':
        segment = bl.segments[1]
        assert segment.name == 'post-sleep'
        rat_speed = None
        rat_speed_times = None

    trains = segment.filter(targdict={'area': area}, objects=neo.SpikeTrain)
    is_pyramidal = [i for i in range(len(trains)) if trains[i].annotations['is_pyramidal']]


    detector = PBEsDetector()

    detector.set_detection_parameters(mua_kernel_sigma_in_ms=mua_kernel_sigma_in_ms,
                                      mua_kernel_size_in_ms=mua_kernel_size_in_ms,
                                      mua_only_selected_cells=mua_only_selected_cells,
                                      n_sd_above_the_mean=n_sd_above_the_mean,
                                      adjust_bounds_to_first_last_spikes=adjust_bounds_to_first_last_spikes,
                                      adjust_bounds_only_selected_cells=adjust_bounds_only_selected_cells,
                                      min_average_speed=min_average_speed,
                                      max_average_speed=max_average_speed,
                                      min_duration_in_ms=min_duration_in_ms,
                                      max_duration_in_ms=max_duration_in_ms,
                                      min_n_spikes_only_selected_cells=min_n_spikes_only_selected_cells,
                                      min_n_spikes=min_n_spikes,
                                      min_percentage_active_only_selected_cells=min_percentage_active_only_selected_cells,
                                      min_percentage_active=min_percentage_active,
                                      min_n_active_only_selected_cells=min_n_active_only_selected_cells,
                                      min_n_active=min_n_active)

    sel_df = detector.detect_candidate_events(trains, selected_cells=is_pyramidal,
                                     speed=rat_speed, speed_times=rat_speed_times)

    print(sel_df[['start_time_in_ms', 'end_time_in_ms']])

    sel_df['epoch'] = epoch
    sel_df['animal'] = bl.annotations['animal_ID']
    sel_df['sess_ind'] = sess_ind

    if epoch == 'task':

        # add task phase
        phases = []
        for time in sel_df['start_time_in_ms']:
            idx = find_index_nearest_preceding(event_times, time)
            event = event_labels[idx]
            if event == 'ITI':
                phase = 'iti'
            elif event == 'Image on':
                phase = 'img'
            elif event == 'Block removed':
                phase = 'run'
            elif event == 'Return to central arm':
                phase = 'return'
            elif event == 'Reward':
                phase = 'reward'
            phases.append(phase)
        sel_df['phase'] = phases

        # add current and previous trial side
        trial_n, current_sides, previous_sides = [], [], []
        for time in sel_df['start_time_in_ms']:
            idx = find_index_nearest_preceding(trial_start_times, time)
            current_side = trial_sides[idx]
            if idx == 0:
                previous_side = np.nan
            else:
                previous_side = trial_sides[idx-1]
            trial_n.append(idx)
            current_sides.append(current_side)
            previous_sides.append(previous_side)
        sel_df['current_trial_side'] = current_sides
        sel_df['previous_trial_side'] = previous_sides
        sel_df['trial_n'] = trial_n

        # add position
        positions = []
        for time1, time2 in zip(sel_df['start_time_in_ms'], sel_df['end_time_in_ms']):
            idx = np.where(np.logical_and(rat_position_times >= time1, rat_position_times<= time2))[0]
            #idx = find_index_nearest_value(rat_position_times, time)
            position = rat_position[idx, :]
            position = np.nanmean(position, axis=0)
            positions.append(position)
        positions = np.vstack(positions)

        sel_df['x_pos'] = positions[:, 0]
        sel_df['y_pos'] = positions[:, 1]

        # add binned_position
        binned_positions, unassigned = bin_position_linearized_LR(positions, current_sides, bin_centers,
                                                                 bin_labels, max_distance=15)
        perc_pbe_no_pos = 100 * np.isnan(binned_positions).sum() / binned_positions.shape[0]
        print('{}% of PBEs do not have a location'.format(perc_pbe_no_pos))
        sel_df['pbe_loc_bin'] = binned_positions

        maze_arm_occ = []
        for loc in positions:
            if np.isnan(loc).sum() == 0:
                arm = get_maze_arm_of_point(loc)
            else:
                arm = 'nanpos'
            maze_arm_occ.append(arm)

        sel_df['maze_arm_occ'] = maze_arm_occ

    else:
        sel_df['phase'] = 'nophase'
        sel_df['x_pos'] = np.nan
        sel_df['y_pos'] = np.nan
        sel_df['current_trial_side'] = np.nan
        sel_df['previous_trial_side'] = np.nan
        sel_df['trial_n'] = np.nan

    pars_dict = detector.get_pars_dictionary()

    # SAVE OUTPUT -----------------------------------------------------------------

    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)

    output = {'pbes' : sel_df, 'detection_pars' : pars_dict, 'other_pars' : pars}

    output_file = os.path.join(output_folder, 'PBEs_sess_{:02}_{}.pkl'.format(sess_ind, epoch))

    print('Saving to {}'.format(output_file))
    pickle.dump(output, open(output_file, 'wb'))