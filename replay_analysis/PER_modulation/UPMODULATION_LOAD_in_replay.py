import matplotlib.pyplot as plt
from constants import *
from utils import *
import quantities as pq
from plotting_style import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from replaydetector.replayratemaps import ReplayRatemaps
import copy
import statsmodels.stats.descriptivestats
from utils import *
from session_selection import ratemap_sessions
import matplotlib


settings_name = 'dec20'
plot_format = 'svg'

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'upmodulation')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

min_time = -300
max_time = 300
only_top_half_rsi = False


# --- PLOT AREA AVERAGE ---
for area in ['Perirhinal']:

    file_name = 'upmodulation_{}_{}.pkl'.format(settings_name, area)
    full_path = os.path.join('/Users/pietro/data/ts_replay_reprised/results', file_name)
    df = pickle.load(open(full_path, 'rb'))
    dfx = df[df['area'] == area]
    dfx = dfx[~np.logical_or(dfx['time'] < min_time, dfx['time'] > max_time)]
    dfx = dfx.groupby(['unit', 'area', 'time']).mean().reset_index()

    f, ax = plt.subplots(1, 1, figsize=[4, 2])
    sns.lineplot(data=dfx, x='time', y='firing_rate_Hz', ax=ax,
                  color=area_palette[area], markers=True,
                 style='area')

    ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
    #ax.set_title(area)
    ax.set_xlabel('Time from replay onset [ms]')
    ax.set_ylabel('Mean PRH\nfiring rate (Hz)')
    ax.set_xlim([min_time, max_time])
    sns.despine()
    ax.legend().remove()
    #ax.set_ylim([0, 10])
    plt.tight_layout()

    plot_name = 'upmodulation_{}.svg'.format(area)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=400)



# --- FORWARD VS REVERSE ---
only_top_half = False
settings_name = 'feb17'

for area in ['Perirhinal']:

    file_name = 'upmodulation_{}_{}.pkl'.format(settings_name, area)
    full_path = os.path.join('/Users/pietro/data/ts_replay_reprised/results', file_name)
    df = pickle.load(open(full_path, 'rb'))

    dfx = df[df['area'] == area]
    dfx = dfx[~np.logical_or(dfx['time'] < min_time, dfx['time'] > max_time)]

    if only_top_half:
        dfk = dfx.groupby(['unit', 'area', 'replay_direction']).mean().reset_index()
        sel_ids = dfk[dfk['firing_rate_Hz'] > dfk['firing_rate_Hz'].median()]['unit']
        dfx = dfx.groupby(['unit', 'area', 'replay_direction', 'time']).mean().reset_index()
        dfx = dfx[np.isin(dfx['unit'], sel_ids)]
    else:
        dfx = dfx.groupby(['unit', 'area', 'time', 'replay_direction']).mean().reset_index()

    f, ax = plt.subplots(1, 1, figsize=[4, 2])
    sns.lineplot(data=dfx, x='time', y='firing_rate_Hz', ax=ax,
                  color=area_palette[area], hue='replay_direction',
                 style='area',
                 palette=replay_direction_palette, markers=True)

    ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
    #ax.set_title(area)
    ax.set_xlabel('Time from replay onset [ms]')
    ax.set_ylabel('Mean PRH\nfiring rate (Hz)')
    sns.despine()
    #ax.set_ylim([0, 10])
    plt.tight_layout()
    ax.legend().remove()

    plot_name = 'upmodulation_forward_reverse_{}_only_top{}.svg'.format(area, only_top_half)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=400)










# --- PLOT IMSHOW ---
settings_name = 'feb13'

for area in ['Perirhinal', 'Barrel']:

    file_name = 'upmodulation_{}_{}.pkl'.format(settings_name, area)
    full_path = os.path.join('/Users/pietro/data/ts_replay_reprised/results', file_name)
    df = pickle.load(open(full_path, 'rb'))
    dfx = df[df['area'] == area]
    dfx = dfx[~np.logical_or(dfx['time'] < min_time, dfx['time'] > max_time)]
    dfx = dfx.groupby(['unit', 'area', 'time']).mean().reset_index()

    n_time_bins = dfx['time'].unique().shape[0]
    unit_ids = dfx['unit'].unique()
    fr = np.zeros([len(unit_ids), n_time_bins])

    for indx, unit in enumerate(unit_ids):

        fr[indx ,:] = dfx[dfx['unit'] == unit]['firing_rate_Hz']

    indxmax = fr.argmax(axis=1)
    reindx = indxmax.argsort()

    f, ax = plt.subplots(figsize=[4, 6])

    ax.imshow(fr[reindx, :],
              extent=(min_time, max_time,
                      fr.shape[0] + 0.5, 0.5),
              aspect='auto',
              cmap= matplotlib.cm.get_cmap('magma'))
    ax.set_xlabel('Time from replay onset [ms]')
    ax.set_ylabel('Neuron')
    sns.despine()
    ax.axvline(0, c='grey', ls='--')
    plt.tight_layout()
    plot_name = 'upmodulation_imshow_{}.png'.format(area)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=400)


# --- PLOT SINGLE UNITS ---
for area in ['Perirhinal', 'Barrel']:

    file_name = 'upmodulation_{}_{}.pkl'.format(settings_name, area)
    full_path = os.path.join('/Users/pietro/data/ts_replay_reprised/results', file_name)
    df = pickle.load(open(full_path, 'rb'))


    dfx = df[df['area'] == area]

    dfx = dfx[~np.logical_or(dfx['time'] < min_time, dfx['time'] > max_time)]

    dfx = dfx.groupby(['unit', 'area', 'time']).mean().reset_index()

    f, ax = plt.subplots(1, 1, figsize=[8, 5])
    sns.lineplot(data=dfx, x='time', y='firing_rate_Hz', ax=ax,
                 units='unit', estimator=None, color=area_palette[area])
    #
    # sns.lineplot(data=dfx, x='time', y='firing_rate_Hz', ax=ax,
    #               color=area_palette[area])

    ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
    ax.set_title(area)
    ax.set_xlabel('Time from replay onset [ms]')
    ax.set_ylabel('Firing rate (Hz)')
    sns.despine()
    #ax.set_ylim([0, 10])
    plt.tight_layout()

    plot_name = 'upmodulation_single_units_{}.png'.format(area)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=400)


# --- TOP HALF OF CELLS WITH HIGHER FIRING ---
for area in ['Perirhinal', 'Barrel']:

    file_name = 'upmodulation_{}_{}.pkl'.format(settings_name, area)
    full_path = os.path.join('/Users/pietro/data/ts_replay_reprised/results', file_name)
    df = pickle.load(open(full_path, 'rb'))

    dfx = df[df['area'] == area]
    dfx = dfx[~np.logical_or(dfx['time'] < min_time, dfx['time'] > max_time)]
    dfk = dfx.groupby(['unit', 'area']).mean().reset_index()
    sel_ids = dfk[dfk['firing_rate_Hz'] > dfk['firing_rate_Hz'].median()]['unit']
    dfx = dfx.groupby(['unit', 'area', 'time']).mean().reset_index()
    dfx = dfx[np.isin(dfx['unit'], sel_ids)]

    f, ax = plt.subplots(1, 1, figsize=[6, 4])
    sns.lineplot(data=dfx, x='time', y='firing_rate_Hz', ax=ax,
                  color=area_palette[area], markers=True,
                 style='area')

    ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
    ax.set_title(area)
    ax.set_xlabel('Time from replay onset [ms]')
    ax.set_ylabel('Firing rate (Hz)')
    sns.despine()
    ax.legend().remove()
    #ax.set_ylim([0, 10])
    plt.tight_layout()

    plot_name = 'upmodulation_single_units_top_half_{}.png'.format(area)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=400)





min_time = -80
max_time = 80
settings_name = 'mar30_zoomed_in_forward_reverse'

for area in ['Perirhinal']:

    file_name = 'upmodulation_{}_{}.pkl'.format(settings_name, area)
    full_path = os.path.join('/Users/pietro/data/ts_replay_reprised/results', file_name)
    df = pickle.load(open(full_path, 'rb'))

    dfx = df[df['area'] == area]
    dfx = dfx[~np.logical_or(dfx['time'] < min_time, dfx['time'] > max_time)]
    dfx = dfx.groupby(['unit', 'area', 'time', 'replay_direction']).mean().reset_index()

    f, ax = plt.subplots(1, 1, figsize=[6, 4])
    sns.lineplot(data=dfx, x='time', y='firing_rate_Hz', ax=ax,
                  color=area_palette[area], hue='replay_direction',
                 style='area',
                 palette=replay_direction_palette, markers=True)

    ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
    ax.set_title(area)
    ax.set_xlabel('Time from replay onset [ms]')
    ax.set_ylabel('Firing rate (Hz)')
    sns.despine()
    #ax.set_ylim([0, 10])
    plt.tight_layout()

    plot_name = 'upmodulation_forward_reverse_{}_{}.png'.format(area, settings_name)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=400)


for area in ['Perirhinal'] :
    file_name = 'upmodulation_{}_{}.pkl'.format(settings_name, area)
    full_path = os.path.join('/Users/pietro/data/ts_replay_reprised/results', file_name)
    df = pickle.load(open(full_path, 'rb'))

    for replay_direction in ['positive', 'negative']:

        dfx = df[df['area'] == area]
        dfx = dfx[~np.logical_or(dfx['time'] < min_time, dfx['time'] > max_time)]
        dfx = dfx.groupby(['unit', 'area', 'time', 'replay_direction']).mean().reset_index()
        vmax = dfx['firing_rate_Hz'].max()
        dfx = dfx[dfx['replay_direction'] == replay_direction]

        n_time_bins = dfx['time'].unique().shape[0]
        unit_ids = dfx['unit'].unique()
        fr = np.zeros([len(unit_ids), n_time_bins])

        for indx, unit in enumerate(unit_ids) :
            fr[indx, :] = dfx[dfx['unit'] == unit]['firing_rate_Hz']

        indxmax = fr.argmax(axis=1)
        reindx = indxmax.argsort()

        f, ax = plt.subplots(figsize=[4, 6])

        ax.imshow(fr[reindx, :], vmin=0, vmax=vmax,
                  extent=(min_time, max_time,
                          fr.shape[0] + 0.5, 0.5),
                  aspect='auto',
                  cmap=matplotlib.cm.get_cmap('magma'))
        ax.set_xlabel('Time from replay onset [ms]')
        ax.set_ylabel('Neuron')
        sns.despine()
        ax.axvline(0, c='grey', ls='--')
        plt.tight_layout()
        plot_name = 'upmodulation_imshow_{}_{}.png'.format(area, replay_direction)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=400)




# --- RIPPLES ---

min_time = -200
max_time = 200
settings_name = 'mar30_ripple'

min_time = -350
max_time = 350
settings_name = 'mar30_ripple_ext'

min_time = -3000
max_time = 3000
settings_name = 'apr4_ripple_ext'

for area in ['Perirhinal', 'Barrel']:

    file_name = 'upmodulation_{}_{}.pkl'.format(settings_name, area)
    full_path = os.path.join('/Users/pietro/data/ts_replay_reprised/results', file_name)
    df = pickle.load(open(full_path, 'rb'))

    dfx = df[df['area'] == area]
    dfx = dfx[~np.logical_or(dfx['time'] < min_time, dfx['time'] > max_time)]
    dfx = dfx.groupby(['unit', 'area', 'time', 'replay_direction', 'ripple']).mean().reset_index()

    f, ax = plt.subplots(1, 1, figsize=[6, 4])
    sns.lineplot(data=dfx, x='time', y='firing_rate_Hz', ax=ax,
                  color=area_palette[area], hue='ripple',
                 style='area',
                 palette=has_ripple_palette, markers=True, ci=95)

    ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
    ax.set_title(area)
    ax.set_xlabel('Time from replay onset [ms]')
    ax.set_ylabel('Firing rate (Hz)')
    sns.despine()
    #ax.set_ylim([0, 10])
    plt.tight_layout()

    plot_name = 'upmodulation_ripple_{}_{}.png'.format(area, settings_name)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=400)






for area in ['Perirhinal', 'Barrel'] :
    file_name = 'upmodulation_{}_{}.pkl'.format(settings_name, area)
    full_path = os.path.join('/Users/pietro/data/ts_replay_reprised/results', file_name)
    df = pickle.load(open(full_path, 'rb'))

    for ripple in [True, False]:

        dfx = df[df['area'] == area]
        dfx = dfx[~np.logical_or(dfx['time'] < min_time, dfx['time'] > max_time)]
        dfx = dfx.groupby(['unit', 'area', 'time', 'replay_direction', 'ripple']).mean().reset_index()
        vmax = dfx['firing_rate_Hz'].max()
        dfx = dfx[dfx['ripple'] == ripple]

        n_time_bins = dfx['time'].unique().shape[0]
        unit_ids = dfx['unit'].unique()
        fr = np.zeros([len(unit_ids), n_time_bins])

        for indx, unit in enumerate(unit_ids) :
            fr[indx, :] = dfx[dfx['unit'] == unit]['firing_rate_Hz']

        indxmax = fr.argmax(axis=1)
        reindx = indxmax.argsort()

        f, ax = plt.subplots(figsize=[4, 6])

        ax.imshow(fr[reindx, :], vmin=0, vmax=vmax,
                  extent=(min_time, max_time,
                          fr.shape[0] + 0.5, 0.5),
                  aspect='auto',
                  cmap=matplotlib.cm.get_cmap('magma'))
        ax.set_xlabel('Time from replay onset [ms]')
        ax.set_ylabel('Neuron')
        sns.despine()
        ax.axvline(0, c='grey', ls='--')
        plt.tight_layout()
        plot_name = 'upmodulation_imshow_{}_ripple{}.png'.format(area, ripple)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=400)
