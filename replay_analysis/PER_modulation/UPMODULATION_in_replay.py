import matplotlib.pyplot as plt
from constants import *
from utils import *
import quantities as pq
from plotting_style import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from replaydetector.replayratemaps import ReplayRatemaps
import copy
import statsmodels.stats.descriptivestats
from utils import *
from session_selection import ratemap_sessions


settings_name = 'dec20'


data_version              = 'dec16'
PBEs_settings_name        = 'dec19k20'
dec_settings_name         = 'DecSet2'

cortical_areas            = ['Perirhinal', 'Barrel']#['Perirhinal', 'Barrel']
hippocampus               = 'Hippocampus'
dec_area                  = 'Hippocampus'


time_bin_size_task_in_ms  = 200
min_pbe_events_per_group  = 5

only_remote_events        = False
only_local_events         = False
group_iti_and_return      = True

new_speed                 = True
speed_threshold_pbes      = SPEED_THRESHOLD
time_before_in_ms         = 0
time_after_in_ms          = 0
position_sigma            = 10
binsize_interp_pos        = 50

theta_power               = False
time_before_after_in_ms   = 300
gaussian_kernel_sigma_in_ms = 50
downsample_every          = 1
binsize_in_ms             = 25


phase_direction_ripples   = [('all', 'all', 'all'),
                             ('all', 'positive', 'all'),
                             ('all', 'negative', 'all')]

# phase_direction_ripples   = [('all', 'all', True),
#                              ('all', 'all', False)]


# --- RUN ---------------------------------------------------------------------

if theta_power :

    from constants import excluded_ids

# LOAD REPLAY FINAL
df = pd.read_csv(os.path.join(REPLAY_FOLDER, 'df_final_selection.csv'))

for ratemap_area in cortical_areas:

    sessions = ratemap_sessions[ratemap_area]

    columns = ['sess_ind', 'unit', 'area', 'task_phase',
               'replay_direction', 'ripple',
               'time', 'firing_rate_Hz', 'rsi']

    df = pd.DataFrame(columns=columns)

    sessions = sessions[np.isin(sessions,[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 27, 28,
                                 29, 30])]

    for sess_ind in sessions:
        print('\n SESSION {} \n'.format(sess_ind))
        #pass
        # --- LOAD TASK DATA ------------------------------------------------------
        session_data = unpack_session(sess_ind, data_folder=DATA_FOLDER,
                                      area=ratemap_area,
                                      binsize_in_ms=time_bin_size_task_in_ms,
                                      data_version=data_version)

        cortical_trains = session_data['trains']

        rsi = get_RSI(sess_ind, ratemap_area)

        # --- LOAD REPLAY ------------------------------------------------------

        # pbes_data = load_decoded_PBEs(sess_ind=sess_ind,
        #                               PBEs_settings_name=PBEs_settings_name,
        #                               PBEs_area=hippocampus,
        #                               data_version=data_version,
        #                               dec_settings_name=dec_settings_name,
        #                               dec_area=dec_area,
        #                               load_light=True)
        #
        # pbes_df_all = pbes_data['pbes']
        #
        # if new_speed:
        #     pbes_df_all = add_new_speed(pbes_df_all, time_before_in_ms=time_before_in_ms,
        #                                 time_after_in_ms=time_after_in_ms,
        #                                 binsize_interp_pos=binsize_interp_pos,
        #                                 position_sigma=position_sigma,
        #                                 data_version=data_version,
        #                                 column_name='average_speed')
        #
        # if group_iti_and_return :
        #     pbes_df_all['phase'] = pbes_df_all['phase'].replace(to_replace='return', value='iti')
        #
        # pbes_df_all = add_ripples_to_pbes_df(pbes_df_all)
        #
        #
        #
        # sig_df = pbes_data['sig_df']
        #
        # bin_size_pbe_in_ms = pbes_data['decode_pars']['bin_size_pbe_in_ms']
        # slide_by_pbe_in_ms = pbes_data['decode_pars']['slide_by_pbe_in_ms']
        # sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types,
        #                                         p_vals=shuffle_p_vals)
        # pbes_df_all = pbes_df_all[np.isin(pbes_df_all.index, sel_pbes_sig)]
        # pbes_df_all = pbes_df_all[pbes_df_all['event_duration_in_ms'] >= min_pbe_duration_in_ms]
        #
        # pbes_df_all = pbes_df_all[pbes_df_all['average_speed'] <= speed_threshold_pbes]
        #
        # pbes_df_all = pbes_df_all[np.isin(pbes_df_all['phase'], phases)]

        pbes_df_all = df[df['sess_ind'] == sess_ind]


        for task_phase, replay_direction, has_ripple in phase_direction_ripples:

            print('\nRunning for session {} task phase: {}, replay direction: {} and with ripple: '
                  '{}'.format(sess_ind, task_phase, replay_direction, has_ripple))

            # --- SELECT REPLAY FOR GIVEN SUBSET -------------------------------
            pbes_df = select_pbes(pbes_df_all=pbes_df_all,
                                  task_phase=task_phase,
                                  replay_direction=replay_direction,
                                  has_ripple=has_ripple)

            if pbes_df.shape[0] < min_pbe_events_per_group:
                print('Skipping {} {} {} of session {}'.format(task_phase, replay_direction, has_ripple, sess_ind))
                continue

            print('#pbes: {}'.format(pbes_df.shape[0]))

            PBEs_times = [(row['start_time_in_ms'] * pq.ms, row['end_time_in_ms'] * pq.ms)
                          for i, row in pbes_df.iterrows()]

            from timeit import default_timer as timer
            from datetime import timedelta

            start = timer()


            # --- BIN CORTICAL SPIKES USING REPLAY TIMES -----------------------
            for unit_ind, train in enumerate(cortical_trains):

                unit_id  = '{}_{}_{}'.format(sess_ind, ratemap_area, unit_ind)
                print('cell {} of {}'.format(unit_ind, len(cortical_trains)))

                for t_start, t_stop in PBEs_times:

                    t_start_adj = t_start-time_before_after_in_ms*pq.ms
                    t_stop_adj = t_start+(time_before_after_in_ms+1)*pq.ms

                    # train_slice = train.time_slice(t_start=t_start_adj,
                    #                                t_stop=t_stop_adj)

                    #kernel = elephant.kernels.GaussianKernel(sigma=gaussian_kernel_sigma_in_ms*pq.ms)

                    # rate = elephant.statistics.instantaneous_rate(
                    #             spiketrain=train.rescale('us'),
                    #             sampling_period=1*pq.ms,
                    #             kernel=kernel,
                    #             t_start=t_start_adj,
                    #             t_stop=t_stop_adj)

                    # TODO z-scoreeeeeeeeeee

                    rate = elephant.statistics.time_histogram(
                        spiketrains=[train],
                        binsize=binsize_in_ms * pq.ms,
                        t_start=t_start_adj,
                        t_stop=t_stop_adj)

                    times = rate.times - t_start
                    rate = rate.magnitude.flatten()

                    for inst_rate, time in zip(rate, times):
                        row = [sess_ind, unit_id, ratemap_area, task_phase,
                               replay_direction, has_ripple, time, inst_rate, rsi[unit_ind]]

                        df.loc[df.shape[0], :] = row

            end = timer()
            print('\ntime:')
            print(timedelta(seconds=end-start))

    for col in ['time'] :
        df[col] = pd.to_numeric([t.item() for t in df[col]])
    for col in ['firing_rate_Hz']:
        df[col] = pd.to_numeric(df[col])


    file_name = 'upmodulation_{}_{}.pkl'.format(settings_name, ratemap_area)
    full_path = os.path.join('/Users/pietro/data/ts_replay_reprised/results', file_name)
    print('Saving output to {}'.format(full_path))
    pickle.dump(df, open(full_path, 'wb'))


#
# plot_folder = os.path.join(PLOTS_FOLDER, 'upmodulation_replay')
# if not os.path.isdir(plot_folder):
#     os.makedirs(plot_folder, exist_ok=True)
#
#
#
# for area in ['Perirhinal', 'Barrel']:
#     dfx = df[df['area'] == area]
#
#     dfx = dfx[~np.logical_or(dfx['time'] < -50, dfx['time'] > 50)]
#
#     dfx = dfx.groupby(['unit', 'area', 'time']).mean().reset_index()
#
#
#     f, ax = plt.subplots(1, 1, figsize=[8, 5])
#
#     sns.lineplot(data=dfx, x='time', y='firing_rate_Hz', ax=ax,
#                  units='unit', estimator=None, color=area_palette[area])
#     ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
#     ax.set_title(area)
#     ax.set_xlabel('Time from replay onset [ms]')
#     ax.set_ylabel('Firing rate (Hz)')
#     sns.despine()
#     ax.set_ylim([0, 10])
#     plt.tight_layout()
#
#     plot_name = 'firing_rate_replay_barplot_phase_direction.png'
#     f.savefig(os.path.join(plot_folder, plot_name), dpi=400)
#


