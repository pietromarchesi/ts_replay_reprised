import matplotlib.pyplot as plt
from constants import *
from utils import *
import quantities as pq
from plotting_style import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from replaydetector.replayratemaps import ReplayRatemaps
import copy
import statsmodels.stats.descriptivestats
from utils import *
from session_selection import ratemap_sessions
from sklearn.preprocessing import StandardScaler

settings_name = 'dec20zsc'


data_version              = 'dec16'
PBEs_settings_name        = 'dec19k20'
dec_settings_name         = 'DecSet2'

time_bin_size_task_in_ms  = 200
min_pbe_events_per_group  = 5

time_before_after_in_ms   = 1000
binsize_in_ms             = 25

baseline_t0               = -600
baseline_t1               = -100

phase_direction_ripples   = [('all', 'all', 'all'),
                             ('all', 'positive', 'all'),
                             ('all', 'negative', 'all')]

# phase_direction_ripples   = [('all', 'all', True),
#                              ('all', 'all', False)]


# --- RUN ---------------------------------------------------------------------

# LOAD REPLAY FINAL
pbes_df_all = pd.read_csv(os.path.join(REPLAY_FOLDER, 'df_final_selection.csv'))

for ratemap_area in ['Perirhinal']:

    sessions = ratemap_sessions[ratemap_area]

    columns = ['sess_ind', 'unit', 'area', 'task_phase',
               'replay_direction', 'ripple',
               'time', 'firing_rate_z_scored']

    df = pd.DataFrame(columns=columns)

    columns2 = ['sess_ind', 'unit', 'area', 'task_phase',
               'replay_direction', 'ripple', 'average_z_scored_resp',
                'significant_response',
                'perc_responsive_trials']
    df2 = pd.DataFrame(columns=columns2)

    sessions = sessions[np.isin(sessions,[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 27, 28,
                                 29, 30])]

    for sess_ind in sessions:
        print('\n SESSION {} \n'.format(sess_ind))
        #pass
        # --- LOAD TASK DATA ------------------------------------------------------
        session_data = unpack_session(sess_ind, data_folder=DATA_FOLDER,
                                      area=ratemap_area,
                                      binsize_in_ms=time_bin_size_task_in_ms,
                                      data_version=data_version)

        cortical_trains = session_data['trains']

        for task_phase, replay_direction, has_ripple in phase_direction_ripples:

            print('\nRunning for session {} task phase: {}, replay direction: {} and with ripple: '
                  '{}'.format(sess_ind, task_phase, replay_direction, has_ripple))

            # --- SELECT REPLAY FOR GIVEN SUBSET -------------------------------
            pbes_df = select_pbes(pbes_df_all=pbes_df_all[pbes_df_all['sess_ind'] == sess_ind],
                                  task_phase=task_phase,
                                  replay_direction=replay_direction,
                                  has_ripple=has_ripple)

            if pbes_df.shape[0] < min_pbe_events_per_group:
                print('Skipping {} {} {} of session {}'.format(task_phase, replay_direction, has_ripple, sess_ind))
                continue

            print('#pbes: {}'.format(pbes_df.shape[0]))

            PBEs_times = [(row['start_time_in_ms'] * pq.ms, row['end_time_in_ms'] * pq.ms)
                          for i, row in pbes_df.iterrows()]


            # --- BIN CORTICAL SPIKES USING REPLAY TIMES -----------------------
            for unit_ind, train in enumerate(cortical_trains):

                unit_id  = '{}_{}_{}'.format(sess_ind, ratemap_area, unit_ind)
                print('cell {} of {}'.format(unit_ind, len(cortical_trains)))

                # for every PBE
                rates, durations = [], []
                for t_start, t_stop in PBEs_times:

                    t_start_adj = t_start-time_before_after_in_ms*pq.ms
                    t_stop_adj = t_start+(time_before_after_in_ms+1)*pq.ms

                    rate = elephant.statistics.time_histogram(
                        spiketrains=[train],
                        binsize=binsize_in_ms * pq.ms,
                        t_start=t_start_adj,
                        t_stop=t_stop_adj,
                        output='rate')

                    times = rate.times - t_start
                    rate = rate.magnitude.flatten()
                    rates.append(rate)
                    durations.append(t_stop - t_start)


                # take the baseline
                baseline_i0 = np.where(times == baseline_t0)[0][0]
                baseline_i1 = np.where(times == baseline_t1)[0][0]

                # group all the baslines
                baselines = np.hstack([rate[baseline_i0 :baseline_i1] for rate in rates])

                # fit z-score scaler
                ss = StandardScaler().fit(baselines.reshape(-1, 1))

                z_scored_rates, responses = [], []
                for rate, duration in zip(rates, durations):
                    # z-score individual replay rate
                    z_scored_rate = ss.transform(rate.reshape(-1, 1)).flatten()
                    z_scored_rates.append(z_scored_rate)

                    # isolate the z-score during the replay
                    stim_i0 = np.where(times == 0)[0][0]
                    stim_i1 = np.abs(times-duration).argmin()
                    z_scored_rate_stim = z_scored_rate[stim_i0:stim_i1]

                    # get the max z-score during the replay: that's the response
                    response = z_scored_rate_stim.max()
                    responses.append(response)

                # compute the average response
                average_resp = np.mean(responses)
                # determine if significant
                significant_resp = average_resp >= 2
                # percentage of trials in which single-trial response is significant
                perc_responsive_trials = 100 * (np.array(responses) >= 2).sum() / len(responses)

                row = [sess_ind, unit_id, ratemap_area, task_phase,
                       replay_direction, has_ripple, np.round(average_resp, 3),
                       significant_resp, np.round(perc_responsive_trials, 3)]

                df2.loc[df2.shape[0], :] = row


                # average z-score over time
                mean_zscored_rate = np.vstack(rates).mean(axis=0)

                for inst_rate, time in zip(mean_zscored_rate, times):
                    row = [sess_ind, unit_id, ratemap_area, task_phase,
                           replay_direction, has_ripple, time, inst_rate]

                    df.loc[df.shape[0], :] = row

                # f, ax = plt.subplots()
                # ax.plot(times, mean_zscored_rates)

                # take the average firing rate at every point in time
                # mean_rate = np.vstack(rates).mean(axis=0)
                # # then z-score
                # ss = StandardScaler()
                # z_score_rate = ss.fit_transform((mean_rate.reshape(-1, 1))).flatten()


    for col in ['time'] :
        df[col] = pd.to_numeric([t.item() for t in df[col]])
    for col in ['firing_rate_z_scored']:
        df[col] = pd.to_numeric(df[col])

    for col in ['average_z_scored_resp',
                'significant_response',
                'perc_responsive_trials']:
        df2[col] = pd.to_numeric(df2[col])


    file_name = 'upmodulation_{}_{}.pkl'.format(settings_name, ratemap_area)
    full_path = os.path.join('/Users/pietro/data/ts_replay_reprised/results', file_name)
    print('Saving first dataframe to {}'.format(full_path))
    pickle.dump(df, open(full_path, 'wb'))

    file_name = 'upmodulation_units_{}_{}.pkl'.format(settings_name, ratemap_area)
    full_path = os.path.join('/Users/pietro/data/ts_replay_reprised/results', file_name)
    print('Saving second dataframe to {}'.format(full_path))
    pickle.dump(df2, open(full_path, 'wb'))




#
# plot_folder = os.path.join(PLOTS_FOLDER, 'upmodulation_replay')
# if not os.path.isdir(plot_folder):
#     os.makedirs(plot_folder, exist_ok=True)
#
#
#
# for area in ['Perirhinal', 'Barrel']:
#     dfx = df[df['area'] == area]
#
#     dfx = dfx[~np.logical_or(dfx['time'] < -50, dfx['time'] > 50)]
#
#     dfx = dfx.groupby(['unit', 'area', 'time']).mean().reset_index()
#
#
#     f, ax = plt.subplots(1, 1, figsize=[8, 5])
#
#     sns.lineplot(data=dfx, x='time', y='firing_rate_Hz', ax=ax,
#                  units='unit', estimator=None, color=area_palette[area])
#     ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
#     ax.set_title(area)
#     ax.set_xlabel('Time from replay onset [ms]')
#     ax.set_ylabel('Firing rate (Hz)')
#     sns.despine()
#     ax.set_ylim([0, 10])
#     plt.tight_layout()
#
#     plot_name = 'firing_rate_replay_barplot_phase_direction.png'
#     f.savefig(os.path.join(plot_folder, plot_name), dpi=400)
#


