import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from utils import *
from constants import *
from plotting_style import *

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

ratemap_areas = ['Both']

group_iti_and_return = True

# PLOTTING PARAMETERS
#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'dec6'

save_plots = False
plot_format = 'svg'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'RE_coordination_stacked_trajectories')


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return)


df_pre = df.copy()


# --- ITERATE OVER AREAS -------------------------------------------------------


for area in ratemap_areas:

    if area == 'Perirhinal' or area == 'Barrel':
        df = add_coordination_per_event(df.copy(), ratemap_area=area,
                                        ratemap_settings=ratemap_settings,
                                        data_version=data_version)
    elif area == 'Both':
        df = add_coordination_both_areas(df.copy(),
                                        ratemap_settings=ratemap_settings,
                                        data_version=data_version)

    df['phase'] = df['phase'].replace(to_replace='img', value='iti')


    col_name = 'coordination_{}'.format(area.lower())

    # --- COORDINATED/UNCOORDINATED STACKED TRAJECTORIES -----------------------

    max_n_traj = 400

    if group_iti_and_return:
        phases = ['iti', 'reward', 'run']
    else:
        phases = ['iti', 'return', 'reward', 'run']


    for current_trial_side in ['left', 'right']:
        for phase in phases:
            df_sel = df[df['epoch'] == 'task']
            df_sel = df_sel[df_sel['current_trial_side'] == current_trial_side]

            df_sel = df_sel[df_sel['phase'] == phase]
            #df_sel = df_sel[~df_sel['pbe_loc_bin'].isna()]

            if df_sel.shape[0] > max_n_traj:
                    ind = np.random.choice(np.arange(df_sel.shape[0]), max_n_traj, replace=False)
                    df_sel = df_sel.iloc[ind]

            new_trajs = []
            for i in range(df_sel.shape[0]):
                row = df_sel.iloc[i]
                traj = row['trajectory']
                loc = row['pbe_loc_bin']
                side = row['pbe_side']
                direction = row['replay_direction']
                traj = linearize_trajectory(traj, side, direction)
                new_trajs.append(traj)

            df_sel['trajectory'] = new_trajs
            df_sel['new_dec_start_bin'] = [t[0] for t in df_sel['trajectory']]
            df_sel['new_dec_end_bin'] = [t[-1] for t in df_sel['trajectory']]

            df1 = df_sel[df_sel[col_name] == 'coordinated']
            df2 = df_sel[df_sel[col_name] == 'uncoordinated']
            df1 = df1.sort_values(by=['new_dec_start_bin', 'new_dec_end_bin'])
            df2 = df2.sort_values(by=['new_dec_start_bin', 'new_dec_end_bin'])
            #df_sel = df_sel.sort_values(by=['replay_direction', 'dec_start_bin'])
            df_sel = pd.concat((df1, df2))

            f, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 7]}, sharex=True,
                                 figsize=[2.5, 3.5])
            ax2 = ax[0].twinx()  # instantiate a second axes that shares the same x-axis

            bw = 0.1
            if phase == 'run':
                bw = 0.2
            # if phase == 'img':
            #     bw_loc = 0.5
            # else:
            #     bw_loc = 0.1
            #
            # sns.kdeplot(df_sel['pbe_loc_bin'], ax=ax2, c=animal_loc_color,
            #             label='Animal location during REs', bw=bw_loc,
            #             cut=100, clip=[-29, 29])

            sns.kdeplot(df_sel['new_dec_start_bin'], ax=ax[0], c=trajectory_cmap(0),
                        label='Start of replay trajectory', bw=bw, cut=100, clip=[-29, 29])

            sns.kdeplot(df_sel['new_dec_end_bin'], ax=ax[0], c=trajectory_cmap(255),
                        label='End of replay trajectory', bw=bw, cut=100, clip=[-29, 29])


            ytext = df_sel[df_sel[col_name] == 'coordinated'].shape[0]

            if phase == 'iti' or phase == 'img' :
                ax[1].axvspan(*location_bands[phase], alpha=0.5, color=animal_loc_color, lw=0)
            elif phase == 'run' or phase == 'reward' :
                if current_trial_side == 'left':
                    ax[1].axvspan(*location_bands[phase][0], alpha=0.5, color=animal_loc_color, lw=0)
                elif current_trial_side == 'right':
                    ax[1].axvspan(*location_bands[phase][1], alpha=0.5, color=animal_loc_color, lw=0)

            # TODO CHANGE PALETTE
            ba = int(df_sel.shape[0] / 40)
            ax[1].plot([-31, -31], [1, ytext-ba], lw=4.5,
                       c=coordinated_palette['coordinated'])
            ax[1].plot([-31, -31], [ytext+ba, df_sel.shape[0]-1], lw=4.5,
                       c=coordinated_palette['uncoordinated'])

            for i in range(df_sel.shape[0]):
                row = df_sel.iloc[i]
                traj = row['trajectory']
                loc = row['pbe_loc_bin']
                side = row['pbe_side']

                if traj[-1] > traj[0]:
                    traj = np.arange(traj[0], traj[-1]+0.1, 0.5)
                elif traj[-1] < traj[0]:
                    traj = np.arange(traj[0], traj[-1]-0.1, -0.5)
                N = traj.shape[0]
                for j in range(traj.shape[0]-1):
                    colors = trajectory_cmap(int(255 * j / N))
                    ax[1].plot([traj[j], traj[j + 1]], [i, i], color=colors,
                               zorder=100)

                ax[1].scatter(traj[0], i, c=trajectory_cmap(0), s=6, zorder=101)
                ax[1].scatter(traj[-1], i, c=trajectory_cmap(255), s=6, zorder=101)

                # ax[1].scatter(loc, i, facecolor=animal_loc_color, marker='.', zorder=100, alpha=0.8,
                #               edgecolor='w', lw=0, s=40)
            sns.despine(left=True, bottom=True, ax=ax[0])
            sns.despine(left=True, bottom=True, ax=ax2)

            for axx in ax:
                axx.set_yticks([], [])
            ax2.set_yticks([], [])
            ax[0].tick_params(axis=u'both', which=u'both',length=0)
            ax[1].set_xlabel('Linearized location', labelpad=15, fontdict={'fontsize':13})
            ax[0].legend(frameon=False)
            leg = ax[0].get_legend()
            ax[0].get_legend().remove()
            #ax2.get_legend().remove()
            ax[1].set_xlim([-31, 31])
            ax[1].set_xticks([-29, -reward_bin, 0, reward_bin, 29])
            ax[1].set_xticklabels(['IMG', 'REW', 'IMG', 'REW', 'IMG'], fontsize=11)
            sns.despine(left=True, ax=ax[1], trim=True)
            for loc in [0, reward_bin, -reward_bin]:
                ax[1].axvline(loc, c=maze_color, zorder=-10, ls='--')

            # if current_trial_side == 'left':
            #     ax[1].set_xlabel('')
            #     ax[1].set_xticks([])
            #     ax[1].set_xtickslabels([])
            #     sns.despine(left=True, ax=ax[1], trim=True)

            plt.tight_layout()

            plot_name = 'trajectory_stack_{}_{}_{}.{}'.format(phase, col_name, current_trial_side, plot_format)
            f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

