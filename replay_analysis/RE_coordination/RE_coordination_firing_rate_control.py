import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import quantities as pq
from constants import *
import itertools
from utils import *
from numpy import median
from statsmodels.stats.proportion import proportions_ztest
from scipy.stats import mannwhitneyu, wilcoxon
from plotting_style import *

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

group_iti_and_return = True


#contrasts = ['local_remote']
# PLOTTING PARAMETERS
#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'jul4_DecSet2_smooth2_morelages'

restrict_to_direction = None

n_bootstraps = 300
save_plots = False
plot_format = 'svg'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms = 50

SPEED_THRESHOLD = 8

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

if SPEED_THRESHOLD is not None:
    PLOTS_FOLDER = os.path.join(PLOTS_FOLDER, 'speed{}'.format(SPEED_THRESHOLD))

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'RE_coordination')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)


# --- LOAD DATA ---------------------------------------------------------------


df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return,
                 max_average_speed=SPEED_THRESHOLD)

# df['phase'] = df['phase'].replace(to_replace='img', value='itiimg')
# df['phase'] = df['phase'].replace(to_replace='iti', value='itiimg')


if group_iti_and_return :
    phases = ['iti', 'img', 'run', 'reward']
else :
    phases = ['itiimg', 'return', 'reward', 'run']


# --- PERIRHINAL ALL CELLS ----------------------------------------------------------

dp = add_coordination_per_event_add_spike_times(df.copy(), ratemap_area='Perirhinal',
                                ratemap_settings=ratemap_settings,
                                data_version=data_version, add_all_cells=True)

for i, row in dp.iterrows():

    n_spikes_pr = len(row['coord_spikes_perirhinal'])
    n_units_pr = row['n_units_perirhinal']
    t = row['event_duration_in_ms'] / 1000

    dp.loc[i, 'rate_perirhinal'] = (n_spikes_pr / n_units_pr) / t

f, ax = plt.subplots(1, 1, sharey=True, figsize=[2.2, 3])

sns.barplot(data=dp, x='phase', y='rate_perirhinal', ax=ax,
            color=area_palette['Perirhinal'], order=phases, alpha=0.8)

ax.set_xticklabels([task_phase_labels_nobreak_v2[l] for l in phases],
                   rotation=45)
ax.set_xlabel('')
ax.set_ylabel('Average PER firing rate (Hz)')
sns.despine()
plt.tight_layout()

plot_name = 'firing_rate_control_all_spikes.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


combos = [['iti', 'run'],
          ['img', 'run'],
          ['iti', 'reward'],
          ['img', 'reward'],
          ['iti', 'img'],
          ['run', 'reward']]

for area in ['perirhinal'] :
    print('\n{}'.format(area))
    for ph1, ph2 in combos :
        x = dp[dp['phase'] == ph1]['rate_{}'.format(area)]
        y = dp[dp['phase'] == ph2]['rate_{}'.format(area)]

        stat, p = mannwhitneyu(x, y)
        is_sig = p < (0.05 / len(combos))
        print('Rate of {} vs {}: p={:1g} - {}'.format(ph1, ph2, p, is_sig))



if False:
    # --- ADD COORDINATED SPIKES ---------------------------------------------------

    dp = add_coordination_per_event_add_spike_times(df.copy(), ratemap_area='Perirhinal',
                                    ratemap_settings=ratemap_settings,
                                    data_version=data_version, )


    db = add_coordination_per_event_add_spike_times(df.copy(), ratemap_area='Barrel',
                                    ratemap_settings=ratemap_settings,
                                    data_version=data_version)

    cols_to_use = db.columns.difference(dp.columns)

    dfb = dp.merge(db[cols_to_use], left_index=True, right_index=True,
                   how='inner')

    dfb['coordination_both'] = ['coordinated' if x == y == 'coordinated'
                                else 'uncoordinated' for x, y in
                                zip(dfb['coordination_perirhinal'],
                                    dfb['coordination_barrel'])]



    for i, row in dfb.iterrows():

        n_spikes_br = len(row['coord_spikes_barrel'])
        n_spikes_pr = len(row['coord_spikes_perirhinal'])
        n_units_br = row['n_units_barrel']
        n_units_pr = row['n_units_perirhinal']
        t = row['event_duration_in_ms'] / 1000

        dfb.loc[i, 'rate_perirhinal'] = (n_spikes_pr / n_units_pr) / t
        dfb.loc[i, 'rate_barrel'] = (n_spikes_br / n_units_br)/ t

    f, ax = plt.subplots(1, 2, sharey=True, figsize=[3, 3])

    sns.barplot(data=dfb, x='phase', y='rate_perirhinal', ax=ax[0],
                color=area_palette['Perirhinal'], order=phases)
    sns.barplot(data=dfb, x='phase', y='rate_barrel', ax=ax[1],
                color=area_palette['Barrel'], order=phases)
    ax[0].set_xticklabels([task_phase_labels_nobreak_v2[l] for l in phases],
                       rotation=45)
    ax[1].set_xticklabels([task_phase_labels_nobreak_v2[l] for l in phases],
                       rotation=45)

    ax[0].set_xlabel('')
    ax[1].set_xlabel('')
    ax[0].set_ylabel('Firing rate')
    ax[1].set_ylabel('')
    sns.despine()
    plt.tight_layout()


    combos = [['itiimg', 'run'],
              ['itiimg', 'reward'],
              ['run', 'reward']]

    for area in ['perirhinal', 'barrel']:
        print('\n{}'.format(area))
        for ph1, ph2  in combos:
            x = dfb[dfb['phase'] == ph1]['rate_{}'.format(area)]
            y = dfb[dfb['phase'] == ph2]['rate_{}'.format(area)]

            stat, p = mannwhitneyu(x, y)
            print('Rate of {} vs {}: p={:1g}'.format(ph1, ph2, p))





    # --- ADD ALL SPIKES -----------------------------------------------------------

    dp = add_coordination_per_event_add_spike_times(df.copy(), ratemap_area='Perirhinal',
                                    ratemap_settings=ratemap_settings,
                                    data_version=data_version,
                                    add_all_cells=True)


    db = add_coordination_per_event_add_spike_times(df.copy(), ratemap_area='Barrel',
                                    ratemap_settings=ratemap_settings,
                                    data_version=data_version,
                                    add_all_cells=True)

    cols_to_use = db.columns.difference(dp.columns)

    dfz = dp.merge(db[cols_to_use], left_index=True, right_index=True,
                   how='inner')

    dfz['coordination_both'] = ['coordinated' if x == y == 'coordinated'
                                else 'uncoordinated' for x, y in
                                zip(dfz['coordination_perirhinal'],
                                    dfz['coordination_barrel'])]




    for i, row in dfz.iterrows():

        n_spikes_br = len(row['coord_spikes_barrel'])
        n_spikes_pr = len(row['coord_spikes_perirhinal'])
        n_units_br = row['n_units_barrel']
        n_units_pr = row['n_units_perirhinal']
        t = row['event_duration_in_ms'] / 1000

        dfz.loc[i, 'rate_perirhinal'] = (n_spikes_pr / n_units_pr) / t
        dfz.loc[i, 'rate_barrel'] = (n_spikes_br / n_units_br)/ t


    # --- PLOT AND STATS ACROSS ALL EVENTS -----------------------------------------

    f, ax = plt.subplots(1, 2, sharey=True, figsize=[3, 3])

    sns.barplot(data=dfz, x='phase', y='rate_perirhinal', ax=ax[0],
                color=area_palette['Perirhinal'], order=phases)
    sns.barplot(data=dfz, x='phase', y='rate_barrel', ax=ax[1],
                color=area_palette['Barrel'], order=phases)
    ax[0].set_xticklabels([task_phase_labels_nobreak_v2[l] for l in phases],
                       rotation=45)
    ax[1].set_xticklabels([task_phase_labels_nobreak_v2[l] for l in phases],
                       rotation=45)

    ax[0].set_xlabel('')
    ax[1].set_xlabel('')
    ax[0].set_ylabel('Firing rate')
    ax[1].set_ylabel('')
    sns.despine()
    plt.tight_layout()


    combos = [['itiimg', 'run'],
              ['itiimg', 'reward'],
              ['run', 'reward']]

    for area in ['perirhinal', 'barrel']:
        print('\n{}'.format(area))
        for ph1, ph2  in combos:
            x = dfz[dfz['phase'] == ph1]['rate_{}'.format(area)]
            y = dfz[dfz['phase'] == ph2]['rate_{}'.format(area)]

            stat, p = mannwhitneyu(x, y)
            print('Rate of {} vs {}: p={:1g}'.format(ph1, ph2, p))



    # --- PLOT AND STATS AVERAGE ACROSS SESSIONS -----------------------------------

    dfzg = dfz.groupby(['sess_ind', 'phase']).mean().reset_index()


    f, ax = plt.subplots(1, 2, sharey=True, figsize=[3, 3])

    sns.barplot(data=dfzg, x='phase', y='rate_perirhinal', ax=ax[0],
                color=area_palette['Perirhinal'], order=phases)
    sns.barplot(data=dfzg, x='phase', y='rate_barrel', ax=ax[1],
                color=area_palette['Barrel'], order=phases)
    ax[0].set_xticklabels([task_phase_labels_nobreak_v2[l] for l in phases],
                          rotation=45)
    ax[1].set_xticklabels([task_phase_labels_nobreak_v2[l] for l in phases],
                          rotation=45)

    ax[0].set_xlabel('')
    ax[1].set_xlabel('')
    ax[0].set_ylabel('Firing rate')
    ax[1].set_ylabel('')
    sns.despine()
    plt.tight_layout()

    combos = [['itiimg', 'run'],
              ['itiimg', 'reward'],
              ['run', 'reward']]

    for area in ['perirhinal', 'barrel'] :
        print('\n{}'.format(area))
        for ph1, ph2 in combos :
            x = dfzg[dfzg['phase'] == ph1]['rate_{}'.format(area)]
            y = dfzg[dfzg['phase'] == ph2]['rate_{}'.format(area)]

            stat, p = mannwhitneyu(x, y)
            print('Rate of {} vs {}: p={:1g}'.format(ph1, ph2, p))


