from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from scipy.stats import wilcoxon, mannwhitneyu
import matplotlib.pyplot as plt
from utils import *
import scipy.stats
import itertools
from plotting_style import *
import matplotlib.patches as mpatches
from sklearn.metrics import balanced_accuracy_score, accuracy_score
from constants import *

"""
Here we run the same code as HC_replay_match_with_real_path, 
except we select only coordinated events
"""


data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'
#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

group_iti_and_return = True

# PLOTTING PARAMETERS
coordination_area = 'Perirhinal'
ratemap_settings = 'dec6'

plot_settings = 'only_two_shufs'
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms    = 50

SPEED_THRESHOLD = 8

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------
if SPEED_THRESHOLD is not None:
    PLOTS_FOLDER = os.path.join(PLOTS_FOLDER, 'speed{}'.format(SPEED_THRESHOLD))

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'RE_coordination')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return,
                 max_average_speed=SPEED_THRESHOLD)

if coordination_area == 'Perirhinal' or coordination_area == 'Barrel' :
    df = add_coordination_per_event(df.copy(), ratemap_area=coordination_area,
                                    ratemap_settings=ratemap_settings,
                                    data_version=data_version)
elif coordination_area == 'Both' :
    df = add_coordination_both_areas(df.copy(),
                                     ratemap_settings=ratemap_settings,
                                     data_version=data_version)

col_name = 'coordination_{}'.format(coordination_area.lower())
col_vals = ['coordinated', 'uncoordinated']

df = df[df[col_name] == 'coordinated']

# --- LOAD BIN CENTERS --------------------------------------------------------
sess_ind = 10
F = loadmat(DATA_FOLDER + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]
bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, session)
bin_centers, bin_labels = prepare_bins(bin_centers_combined)


if group_iti_and_return :
    phases = ['iti', 'img', 'run', 'reward']
else :
    phases = ['img', 'iti', 'return', 'reward', 'run']


# --- BOOTSTRAP ----------------------------------------------------------------

side_col = 'maze_arm_dec_start'
n_boot = 500
for replay_direction in ['all']:

    dac = pd.DataFrame(columns=['phase', 'accuracy', 'match_with', 'n_samp'])

    for match_with in ['current_trial_side', 'previous_trial_side'] :

        for phase in phases :
            if replay_direction == 'all' :
                df_sel = df[(df['epoch'] == 'task')
                            & (df[side_col] != 'central')
                            & (df['phase'] == phase)]
            else :
                df_sel = df[(df['epoch'] == 'task') &
                            (df[side_col] != 'central')
                            & (df['replay_direction'] == replay_direction)
                            & (df['phase'] == phase)]
            print('phase: {}, n_samp={}'.format(phase, df_sel.shape))
            a1_side = df_sel[side_col][1 :-1]
            a2_side = df_sel[match_with][1 :-1]

            ind = np.where(~a2_side.isna())[0]
            a1_side = a1_side[ind]
            a2_side = a2_side[ind]

            for b in range(n_boot) :
                ind = np.random.choice(np.arange(a1_side.shape[0]),
                                       a1_side.shape[0],
                                       replace=True)
                a1_side_use = a1_side[ind]
                a2_side_use = a2_side[ind]
                acc = balanced_accuracy_score(a1_side_use, a2_side_use)
                dac.loc[dac.shape[0], :] = [phase, acc, match_with, a1_side.shape[0]]


    # --- MAKE CONFIDENCE INTERVALS (CANNOT USE BOOTSTRAPS ON BOOTSTRAPS!) ---------

    y = []
    err = []
    for phase in phases:
        for match_with in ['previous_trial_side', 'current_trial_side'] :
            print('\n{} {}'.format(match_with, phase))
            stats = dac[(dac['match_with'] == match_with) & (dac['phase'] == phase)]['accuracy']
            obs_val = stats.median()
            y.append(obs_val)
            alpha = 0.95
            p = ((1.0 - alpha) / 2.0) * 100
            lower = max(0.0, np.percentile(stats, p))
            p = (alpha + ((1.0 - alpha) / 2.0)) * 100
            upper = min(1.0, np.percentile(stats, p))
            err.append([obs_val - lower, upper - obs_val])
            print('%.1f confidence interval %.1f%% and %.1f%%' % ( alpha * 100, lower * 100, upper * 100))


    # --- PLOT BARS ----------------------------------------------------------------

    f, ax = plt.subplots(1, 1, figsize=[2.5, 4])

    ax.bar(x=[0, 1, 3, 4, 6, 7, 9, 10],
           height=y,
           yerr=np.array(err).T,
           linewidth=2.5,
           ecolor='0.2')

    for i, patch in enumerate(ax.patches):
        if i%2==0:
            patch.set_edgecolor(sns.xkcd_rgb['grey'])
            patch.set_facecolor('white')

        else:
            patch.set_edgecolor(sns.xkcd_rgb['grey'])
            patch.set_facecolor(sns.xkcd_rgb['grey'])

    ax.axhline(0.5, c=maze_color, ls='--', lw=2)
    ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1])
    ax.set_xticks([0.5, 3.5, 6.5, 9.5])
    ax.set_xticklabels([task_phase_labels[p] for p in phases],
                       rotation=45)
    ax.set_ylabel('Match with trajectory side')
    ax.set_xlabel('')
    ax.legend().remove()
    sns.despine()
    plt.tight_layout()

    plot_name = 'barplot_match_with_both_{}_events_coord_{}.{}'.format(
        replay_direction, coordination_area,
        plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
    #plt.close(fig=f)



    pos_patch = mpatches.Patch(edgecolor=sns.xkcd_rgb['grey'],
                               facecolor='white', linewidth=2.5,
                               label='Previous trial side')
    neg_patch = mpatches.Patch(edgecolor=sns.xkcd_rgb['grey'],
                               facecolor=sns.xkcd_rgb['grey'], linewidth=2.5,
                               label='Current trial side')
    f, ax = plt.subplots(1, 1, figsize=[3, 1])
    ax.legend(handles=[pos_patch, neg_patch], frameon=False)
    ax.axis('off')
    plt.tight_layout()

    plot_name = 'barplot_match_trial_side_legend.{}'.format(plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

