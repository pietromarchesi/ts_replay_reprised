import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import quantities as pq
from constants import *
import itertools
from utils import *
from numpy import median
from statsmodels.stats.proportion import proportions_ztest
from plotting_style import *

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

group_iti_and_return = True

contrasts                 = ['coordination_perirhinal',
                             'coordination_barrel',
                             'coordination_both']

#contrasts = ['local_remote']
# PLOTTING PARAMETERS
#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'jul4_DecSet2_smooth2_morelages'

restrict_to_direction = None

n_bootstraps = 300
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, 'RE_coordination')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------


df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return)

df['phase'] = df['phase'].replace(to_replace='img', value='itiimg')
df['phase'] = df['phase'].replace(to_replace='iti', value='itiimg')


if group_iti_and_return :
    phases = ['iti', 'run', 'reward']
else :
    phases = ['iti', 'return', 'reward', 'run']

# --- PREPARE DATA -------------------------------------------------------------



# --- GET COORDINATION WITH SPIKES ---------------------------------------------

dp = add_coordination_per_event_add_spike_times(df.copy(), ratemap_area='Perirhinal',
                                ratemap_settings=ratemap_settings,
                                data_version=data_version)


db = add_coordination_per_event_add_spike_times(df.copy(), ratemap_area='Barrel',
                                ratemap_settings=ratemap_settings,
                                data_version=data_version)

cols_to_use = db.columns.difference(dp.columns)

dfb = dp.merge(db[cols_to_use], left_index=True, right_index=True,
               how='inner')

dfb['coordination_both'] = ['coordinated' if x == y == 'coordinated'
                            else 'uncoordinated' for x, y in
                            zip(dfb['coordination_perirhinal'],
                                dfb['coordination_barrel'])]

dg = pd.DataFrame(columns=['area', 'perc_time'])

for i, row in dfb.iterrows():

    if row['coordination_both'] == 'coordinated':

        re_duration = row['end_time_in_ms'] - row['start_time_in_ms']
        spike_times = row['coord_spikes_perirhinal'].rescale(pq.ms) - row['start_time_in_ms'] *pq.ms
        perc_times = 100 * spike_times / re_duration
        for t in perc_times[0:1]:
            dg.loc[dg.shape[0], :] = ['Perirhinal', t.item()]

        re_duration = row['end_time_in_ms'] - row['start_time_in_ms']
        spike_times = row['coord_spikes_barrel'].rescale(pq.ms) - row['start_time_in_ms'] *pq.ms
        perc_times = 100 * spike_times / re_duration
        for t in perc_times[0:1]:
            dg.loc[dg.shape[0], :] = ['Barrel', t.item()]


dg['perc_time'] = pd.to_numeric(dg['perc_time'])

f, ax = plt.subplots(figsize=[2, 3])
sns.barplot(data=dg, x='area', y='perc_time',
            order=['Perirhinal', 'Barrel'], palette=area_palette)
ax.set_ylabel('Fraction of time after RE start\nof first coordinated spike')
ax.set_xticklabels(['PRH', 'S1BF'])
ax.set_xlabel('')
sns.despine()
plt.tight_layout()

f, ax = plt.subplots(figsize=[4, 4])
sns.distplot(dg[dg['area'] == 'Perirhinal']['perc_time'],
             hist=True, kde=False, norm_hist=True, color=area_palette_2['PRH'])
sns.distplot(dg[dg['area'] == 'Barrel']['perc_time'],
             hist=True, kde=False, norm_hist=True, color=area_palette_2['S1BF'])
ax.set_xlabel('Spike position in RE')
sns.despine()
plt.tight_layout()
#sns.distplot(data=dg, y='perc_time', hue='area')




# --- GET COORDINATION WITH SPIKES ON RANDOMIZED INTERVALS ---------------------

dp = add_coordination_per_event_add_spike_times(df.copy(), ratemap_area='Perirhinal',
                                ratemap_settings=ratemap_settings,
                                data_version=data_version,
                                randomize_time=True)


db = add_coordination_per_event_add_spike_times(df.copy(), ratemap_area='Barrel',
                                ratemap_settings=ratemap_settings,
                                data_version=data_version,
                                randomize_time=True)

# cols_to_use = db.columns.difference(dp.columns)

# dfb = dp.merge(db[cols_to_use], left_index=True, right_index=True,
#                how='inner')
#
# dfb['coordination_both'] = ['coordinated' if x == y == 'coordinated'
#                             else 'uncoordinated' for x, y in
#                             zip(dfb['coordination_perirhinal'],
#                                 dfb['coordination_barrel'])]

dg = pd.DataFrame(columns=['area', 'perc_time'])

for i, row in dp.iterrows():

    if row['coordination_perirhinal'] == 'coordinated':

        re_duration = row['end_time_in_ms'] - row['start_time_in_ms']
        spike_times = row['coord_spikes_perirhinal'].rescale(pq.ms) - row['start_time_in_ms'] *pq.ms
        perc_times = 100 * spike_times / re_duration
        for t in perc_times[0:1]:
            dg.loc[dg.shape[0], :] = ['Perirhinal', t.item()]


for i, row in db.iterrows() :

    if row['coordination_barrel'] == 'coordinated' :

        re_duration = row['end_time_in_ms'] - row['start_time_in_ms']
        spike_times = row['coord_spikes_barrel'].rescale(pq.ms) - row['start_time_in_ms'] *pq.ms
        perc_times = 100 * spike_times / re_duration
        for t in perc_times[0:1]:
            dg.loc[dg.shape[0], :] = ['Barrel', t.item()]


dg['perc_time'] = pd.to_numeric(dg['perc_time'])

f, ax = plt.subplots(figsize=[2, 3])
sns.barplot(data=dg, x='area', y='perc_time',
            order=['Perirhinal', 'Barrel'], palette=area_palette)
ax.set_ylabel('Fraction of time after RE start\nof first coordinated spike [RANDOMIZED]')
ax.set_xticklabels(['PRH', 'S1BF'])
ax.set_xlabel('')
sns.despine()
plt.tight_layout()

f, ax = plt.subplots(figsize=[4, 4])
sns.distplot(dg[dg['area'] == 'Perirhinal']['perc_time'],
             hist=True, kde=False, norm_hist=True, color=area_palette_2['PRH'])
sns.distplot(dg[dg['area'] == 'Barrel']['perc_time'],
             hist=True, kde=False, norm_hist=True, color=area_palette_2['S1BF'])
ax.set_xlabel('Spike position in RE [RANDOMIZED]')
sns.despine()
plt.tight_layout()
#sns.distplot(data=dg, y='perc_time', hue='area')