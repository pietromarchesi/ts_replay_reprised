import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from session_selection import sessions_HC_replay
from utils import *

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'
plot_settings      = 'base'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]

sessions           = [2,3,4,6,7,8,9,10,11,28,29,30]

epochs             = ['task']


# FILTER PBES PARAMETERS

ratemap_settings = 'jul4_DecSet2_smooth2_morelages'


shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
p_vals = [0.05, 0.05]
min_pbe_duration_in_ms = 50


# PLOTTING PARAMETERS
n_events = 3
save_plots = True
plot_format = 'svg'
dpi = 400


plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'RE_coordination_examples')


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

pbes_dfs, sig_dfs= [], []
rds = {}

for sess_ind in sessions:
    for epoch in epochs:

        pbes = load_decoded_PBEs(sess_ind, PBEs_settings_name, PBEs_area,
                                 data_version, dec_settings_name, dec_area)

        pbes_dfs.append(pbes['pbes'])
        sig_dfs.append(pbes['sig_df'])

        for pbe_id, det in zip(pbes['pbes'].index, pbes['detectors']):
            rds[pbe_id] = det

df = pd.concat(pbes_dfs)
sig_df = pd.concat(sig_dfs)

df = df[df['event_duration_in_ms'] >= min_pbe_duration_in_ms]

# --- FILTER PBES -------------------------------------------------------------

sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types,
                                        p_vals=p_vals)
df = df[np.isin(df.index, sel_pbes_sig)]

# --- GET GOOD EVENTS ---------------------------------------------------------


right_events_positive = ['6_task_434',
                         '6_task_1453',
                         '29_task_718',
                         '7_task_850',
                         '29_task_515',
                         '7_task_1258']


right_events_negative = ['6_task_1188',
                        '29_task_915',
                         '6_task_945',
                         '6_task_821']

left_events_positive = ['7_task_934',
                        '10_task_1299',
                        '2_task_2033',
                        '6_task_707',
                        '10_task_632',
                        '11_task_714']

left_events_negative = ['3_task_680',
                        '6_task_581',
                        '10_task_421',
                        '6_task_1527']

event_side = ['right_pos'] * 6 + ['right_neg'] * 4 + ['left_pos'] * 6 + ['left_neg'] * 4

ids = right_events_positive + right_events_negative + left_events_positive + left_events_negative


df_sel = df.loc[ids]

# --- GET COORDINATION ---------------------------------------------------------

dp = add_coordination_per_event_add_spike_times(df_sel.copy(), ratemap_area='Perirhinal',
                                ratemap_settings=ratemap_settings,
                                data_version=data_version)


db = add_coordination_per_event_add_spike_times(df_sel.copy(), ratemap_area='Barrel',
                                ratemap_settings=ratemap_settings,
                                data_version=data_version)

cols_to_use = db.columns.difference(dp.columns)

dfb = dp.merge(db[cols_to_use], left_index=True, right_index=True,
               how='inner')

dfb['coordination_both'] = ['coordinated' if x == y == 'coordinated'
                            else 'uncoordinated' for x, y in
                            zip(dfb['coordination_perirhinal'],
                                dfb['coordination_barrel'])]

# --- GET MIN AND MAX SPIKES AND PROBA -----------------------------------------

# max_spikes_list = []
# max_proba_list = []
# for ev_type in dict_events.keys():
#     ids = dict_events[ev_type]
#     ids = ids[0:n_events]
#     for row, id in enumerate(ids):
#         pbe = rds[id]
#         max_spikes_list.append(pbe.spikes.max())
#         max_proba_list.append(pbe.y_pred_proba.max())
#
# vmax_spikes = np.max(max_spikes_list)
# vmax_proba = np.round(np.max(max_proba_list), 1)

from matplotlib.colors import LinearSegmentedColormap
colors = ['white', area_palette_2['PRH']]  # R -> G -> B
n_bins = 100  # Discretizes the interpolation into bins
cmap_name = 'pr_cmap'
cmap_pr = LinearSegmentedColormap.from_list(cmap_name, colors, N=n_bins)


colors = ['white', area_palette_2['S1BF']]  # R -> G -> B
n_bins = 100  # Discretizes the interpolation into bins
cmap_name = 'br_cmap'
cmap_br = LinearSegmentedColormap.from_list(cmap_name, colors, N=n_bins)

vmax_spikes = 5
vmax_prob = 0.5
vmin_spikes = 0
vmin_proba = 0

df = dfb.copy()

for i, row in df.iterrows():

    if row['coordination_both'] == 'coordinated':

        f, ax = plt.subplots(3, 1, gridspec_kw={'height_ratios' : [4, 1, 1]},
                             sharex=False, figsize=[2, 4])
        ax[0].get_xticks()

        pbe = rds[i]
        pbe.binsize_in_ms = 10

        im = pbe._plot_binned_spikes(ax[0], plot_cbar=False,
                                vmin=vmin_spikes, vmax=vmax_spikes)

        ticklabels = (ax[0].get_xticks()[1:] * 10).astype(int)
        ax[0].set_xticklabels(ticklabels)
        #ax.set_xlabel('Time [ms]')
        times = row['coord_spikes_perirhinal'].rescale(pq.ms) - row['start_time_in_ms'] *pq.ms
        for t in times:
            ax[1].axvline(t, c=area_palette_2['PRH'], lw=3)

        times = row['coord_spikes_barrel'].rescale(pq.ms) - row['start_time_in_ms'] *pq.ms
        for t in times:
            ax[2].axvline(t, c=area_palette_2['S1BF'], lw=3)

        tmax = 10 * (pbe.time_bins[-1] + 1)
        print(tmax)
        real_tmax = row['end_time_in_ms'] - row['start_time_in_ms']
        print(real_tmax)

        ax[0].set_xlim([0, pbe.time_bins[-1] + 1])
        ax[1].set_xlim([0, tmax])
        ax[2].set_xlim([0, tmax])

        for axx in ax:
            axx.set_yticks([])
            axx.set_yticklabels([])

        # ax[0].set_xticklabels([])
        # ax[1].set_xticklabels([])

        ax[0].set_xlabel('')
        ax[1].set_xlabel('')
        ax[2].set_xlabel('Time [ms]')

        ax[0].set_ylabel('CA1 neurons\n')
        ax[1].set_ylabel('Spikes from\ncoordinated\nPRH neuron')
        ax[2].set_ylabel('Spikes from\ncoordinated\nS1BF neuron')

        sns.despine(top=True, right=True, offset=0, trim=False, left=True)
        plt.subplots_adjust(wspace=0.2, hspace=0.2)
        plt.tight_layout()

        plot_name = 'pbe_panel_{}_combined_vline.{}'.format(i, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

