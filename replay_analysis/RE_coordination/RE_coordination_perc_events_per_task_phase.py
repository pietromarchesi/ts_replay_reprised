import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import quantities as pq
import matplotlib.patches as mpatches
from scipy.ndimage import gaussian_filter1d
import itertools
from utils import *
from numpy import median
from statsmodels.stats.proportion import proportions_ztest
from plotting_style import *
from constants import *

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

group_iti_and_return = True

contrasts                 = ['coordination_perirhinal',
                             'coordination_barrel',
                             'coordination_both']

#contrasts = ['local_remote']
# PLOTTING PARAMETERS
#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'jul4_DecSet2_smooth2_morelages'

restrict_to_direction = None

min_spikes = 3

n_bootstraps = 300
save_plots = False
plot_format = 'svg'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms = 50
same_sessions = True

SPEED_THRESHOLD = 8

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- set up paths -------------------------------------------------------------
if SPEED_THRESHOLD is not None:
    PLOTS_FOLDER = os.path.join(PLOTS_FOLDER, 'speed{}'.format(SPEED_THRESHOLD))

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'RE_coordination')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df, df_not_sig = load_replay(sessions=sessions,
                             PBEs_settings_name=PBEs_settings_name,
                             PBEs_area=PBEs_area,
                             data_version=data_version,
                             dec_settings_name=dec_settings_name,
                             dec_area=dec_area,
                             min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                             shuffle_types=shuffle_types,
                             shuffle_p_vals=shuffle_p_vals,
                             group_iti_and_return=group_iti_and_return,
                             return_not_sig=True,
                             max_average_speed=SPEED_THRESHOLD,
                             NEWSPEED=True)


df['phase'] = df['phase'].replace(to_replace='img', value='itiimg')
df['phase'] = df['phase'].replace(to_replace='iti', value='itiimg')


# --- PREPARE DATA -------------------------------------------------------------

if group_iti_and_return :
    phases = ['iti', 'run', 'reward']
else :
    phases = ['iti', 'return', 'reward', 'run']


df_mod_1 = add_coordination_per_event(df.copy(), ratemap_area='Perirhinal',
                                ratemap_settings=ratemap_settings,
                                data_version=data_version,
                                min_spikes=min_spikes)


df_mod_2 = add_coordination_per_event(df.copy(), ratemap_area='Barrel',
                                ratemap_settings=ratemap_settings,
                                data_version=data_version,
                                min_spikes=min_spikes)

df_mod_3 = add_coordination_both_areas(df.copy(),
                                ratemap_settings=ratemap_settings,
                                data_version=data_version,
                                min_spikes=min_spikes)

if same_sessions:
    selsess = df_mod_3['sess_ind'].unique()
    df_mod_1 = df_mod_1[np.isin(df_mod_1['sess_ind'], selsess)]
    df_mod_2 = df_mod_2[np.isin(df_mod_2['sess_ind'], selsess)]

    assert set(df_mod_1['sess_ind']) == set(df_mod_2['sess_ind'])  == set(df_mod_3['sess_ind'])


df_mod_1 = df_mod_1.rename({'coordination_perirhinal' : 'coordination'}, axis=1)
df_mod_2 = df_mod_2.rename({'coordination_barrel' : 'coordination'}, axis=1)
df_mod_3 = df_mod_3.rename({'coordination_both' : 'coordination'}, axis=1)

contrast = 'coordination'

name1, name2 = np.sort(df_mod_1[contrast].unique())


if group_iti_and_return :
    phases = ['itiimg', 'run', 'reward']
else :
    phases = ['itiimg', 'return', 'reward', 'run']

if restrict_to_direction is not None:
    df_mod_1 = df_mod_1[df_mod_1['replay_direction'] == restrict_to_direction]
    df_mod_2 = df_mod_2[df_mod_2['replay_direction'] == restrict_to_direction]
    df_mod_3 = df_mod_3[df_mod_3['replay_direction'] == restrict_to_direction]


df_mod_1['area_coord'] = 'PRH'
df_mod_2['area_coord'] = 'S1BF'
df_mod_3['area_coord'] = 'Both'

df_mod = pd.concat([df_mod_1, df_mod_2, df_mod_3])

#df_mod = df_mod_3

# --- BOOTSTRAP ----------------------------------------------------------------

dg = pd.DataFrame(columns=['phase', 'area_coord', 'perc_centr', 'ncentrif', 'ntot'])
for phase in ['all'] + phases:
    for area_coord in ['PRH', 'S1BF', 'Both']:

        for n in range(n_bootstraps):

            indx = np.random.choice(np.arange(df_mod.shape[0]), size=df_mod.shape[0], replace=True)
            df_mod_boot = df_mod.iloc[indx]
            if phase == 'all':
                dk = df_mod_boot[df_mod_boot['area_coord'] == area_coord].copy()
            else:
                dk = df_mod_boot[(df_mod_boot['area_coord'] == area_coord) & (df_mod_boot['phase'] == phase)].copy()
            if dk.shape[0] > 5:
                npos = dk[dk[contrast] == name1].shape[0]
                nneg = dk[dk[contrast] == name2].shape[0]
                perc = (100 * npos / (npos+nneg))
                ntot = npos+nneg
                dg.loc[dg.shape[0], :] = [phase, area_coord ,perc, npos, ntot]
            else:
                pass

dg['perc_centr'] = pd.to_numeric(dg['perc_centr'])
dg['ncentrif'] = pd.to_numeric(dg['ncentrif'])
dg['ntot'] = pd.to_numeric(dg['ntot'])


# # --- COMPUTE CONFIDENCE INTERVALS ---------------------------------------------
# y = []
# err = []
# for phase in ['all'] + phases:
#     for area_coord in ['PRH', 'S1BF', 'Both']:
#         #print('\n{} {}'.format(match_with, phase))
#         stats = dg[(dg['area_coord'] == area_coord) & (dg['phase'] == phase)]['perc_centr']
#         stats = stats / 100
#         obs_val = stats.median()
#         y.append(obs_val)
#         alpha = 0.95
#         p = ((1.0 - alpha) / 2.0) * 100
#         lower = max(0.0, np.percentile(stats, p))
#         p = (alpha + ((1.0 - alpha) / 2.0)) * 100
#         upper = min(1.0, np.percentile(stats, p))
#         err.append([obs_val - lower, upper - obs_val])
#         print('{} {}: {:.1f} {}% CI {:.1f} and {:.1f}'.format(
#              area_coord, phase, obs_val * 100, alpha * 100, lower * 100, upper * 100))
#
# y = np.array(y)*100
# err = np.array(err) * 100
#
# # --- PLOT BARS ----------------------------------------------------------------
#
# f, ax = plt.subplots(1, 1, figsize=[2.2, 3.2])
#
# ax.bar(x=[0, 1, 2,
#           4, 5, 6,
#           8, 9, 10,
#           12, 13, 14],
#        height=y,
#        yerr=np.array(err).T,
#        linewidth=2.2,
#        ecolor='0.2')
#
# ax.legend().remove()
# ax.set_yticks([0, 20, 40, 60, 80, 100])
# ax.set_ylim([0, 100])
# ax.set_xticks([1, 5, 9, 13])
#
# inx = np.array([0, 3, 6, 9])
# for i, patch in enumerate(ax.patches):
#     if np.isin(i, inx):
#         patch.set_facecolor(area_palette_2['PRH'])
#     if np.isin(i, inx+1):
#         patch.set_facecolor(area_palette_2['S1BF'])
#     if np.isin(i, inx+2):
#         patch.set_facecolor(area_palette_2['Both'])
#
# sns.despine()
# ax.set_ylabel('Coordinated replay events (% of total)')
# ax.set_xticklabels([task_phase_labels_nobreak_v2[l] for l in ['all'] + phases],
#                    rotation=45)
# ax.set_xlabel('')
# plt.tight_layout()
#
# plot_name = 'perc_{}_events_per_task_phase_v2_all_areas_min_spikes_{}_ss{}.{}'.format(contrast, min_spikes, same_sessions, plot_format)
# f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- COMPUTE CONFIDENCE INTERVALS PRH ONLY ------------------------------------
y = []
err = []
for phase in ['all'] + phases:
    for area_coord in ['PRH']:
        #print('\n{} {}'.format(match_with, phase))
        stats = dg[(dg['area_coord'] == area_coord) & (dg['phase'] == phase)]['perc_centr']
        stats = stats / 100
        obs_val = stats.median()
        y.append(obs_val)
        alpha = 0.95
        p = ((1.0 - alpha) / 2.0) * 100
        lower = max(0.0, np.percentile(stats, p))
        p = (alpha + ((1.0 - alpha) / 2.0)) * 100
        upper = min(1.0, np.percentile(stats, p))
        err.append([obs_val - lower, upper - obs_val])
        print('{} {}: {:.1f} {}% CI {:.1f} and {:.1f}'.format(
             area_coord, phase, obs_val * 100, alpha * 100, lower * 100, upper * 100))

y = np.array(y)*100
err = np.array(err) * 100

# --- PLOT BARS PRH ------------------------------------------------------------

f, ax = plt.subplots(1, 1, figsize=[2, 3.2])

ax.bar(x=[0, 1, 2, 3],
       height=y,
       yerr=np.array(err).T,
       linewidth=2.2,
       ecolor='0.2', color=area_palette_2['PRH'], alpha=0.8)

ax.legend().remove()
ax.set_yticks([0, 20, 40, 60, 80, 100])
ax.set_ylim([0, 100])
ax.set_xticks([0, 1, 2, 3])


sns.despine()
ax.set_ylabel('Coordinated replay events (% of total)')
ax.set_xticklabels([task_phase_labels_nobreak_v2[l] for l in ['all'] + phases],
                   rotation=45)
ax.set_xlabel('')
plt.tight_layout()

plot_name = 'perc_{}_events_per_task_phase_v2_ONLYPRH_min_spikes_{}_ss{}.{}'.format(contrast, min_spikes, same_sessions, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- PLOT LEGEND --------------------------------------------------------------

pr_patch = mpatches.Patch(facecolor=area_palette_2['PRH'],
                       edgecolor='white', linewidth=2.5,
                       label='Coordinated with PRH')
br_patch = mpatches.Patch(facecolor=area_palette_2['S1BF'],
                          edgecolor='white', linewidth=2.5,
                       label='Coordinated with S1BF')
bo_patch = mpatches.Patch(facecolor=area_palette_2['Both'],
                          edgecolor='white', linewidth=2.5,
                       label='Widely coordinated')

f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=[pr_patch, br_patch, bo_patch], frameon=False)
ax.axis('off')
plt.tight_layout()

plot_name = 'replay_{}_legend_all_areas.{}'.format(contrast, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- Z-TEST STATISTICS --------------------------------------------------------

dg = pd.DataFrame(columns=['phase', 'area_coord', 'perc_centr', 'ncentrif', 'ntot'])
for phase in ['all'] + phases:
    for area_coord in ['PRH', 'S1BF', 'Both']:
        if phase == 'all':
            dk = df_mod[df_mod['area_coord'] == area_coord].copy()
        else:
            dk = df_mod[(df_mod['area_coord'] == area_coord) & (df_mod['phase'] == phase)].copy()
        if dk.shape[0] > 5:
            npos = dk[dk[contrast] == name1].shape[0]
            nneg = dk[dk[contrast] == name2].shape[0]
            perc = (100 * npos / (npos+nneg))
            ntot = npos+nneg
            dg.loc[dg.shape[0], :] = [phase, area_coord ,perc, npos, ntot]
        else:
            pass


phases = ['all', 'itiimg', 'run', 'reward']
combos = list(itertools.combinations(phases,2))
n_tests = len(combos)

for area_coord in ['PRH', 'S1BF', 'Both']:
    print("\nSTATISTICS FOR COORD WITH {}".format(area_coord))
    dg_sel = dg[dg['area_coord'] == area_coord]
    for ph1, ph2 in combos :
        ncentrif1 = dg_sel[dg_sel['phase'] == ph1]['ncentrif'].values[0]
        nsamp1 = dg_sel[dg_sel['phase'] == ph1]['ntot'].values[0]

        ncentrif2 = dg_sel[dg_sel['phase'] == ph2]['ncentrif'].values[0]
        nsamp2 = dg_sel[dg_sel['phase'] == ph2]['ntot'].values[0]

        z, pval = proportions_ztest(count=[ncentrif1, ncentrif2],
                                    nobs=[nsamp1, nsamp2])
        is_sig = pval < (0.05 / n_tests)
        adjusted_p = pval
        print('{} vs {} z-test p={:.10f} - {}'.format(ph1, ph2, adjusted_p, is_sig))
        print('{:.1f}% vs {:.1f}%'.format(100*ncentrif1/nsamp1, 100*ncentrif2/nsamp2))


