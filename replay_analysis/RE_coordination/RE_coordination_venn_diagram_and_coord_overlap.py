import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from plotting_style import *
from utils import *
from matplotlib_venn import venn2

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

ratemap_areas = ['Both']

group_iti_and_return = True

# PLOTTING PARAMETERS
#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'jul4_DecSet2_smooth2_morelages'

n_bootstraps = 300
save_plots = False
plot_format = 'svg'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms    = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'RE_coordination')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return)


df_pre = df.copy()


# --- MAKE VENN DIAGRAM PER PHASE ----------------------------------------------


#phases = ['all', 'itiimg', 'reward', 'run']
phases = ['all']
df = add_coordination_both_areas(df.copy(),
                                ratemap_settings=ratemap_settings,
                                data_version=data_version)

df['phase'] = df['phase'].replace(to_replace='img', value='itiimg')
df['phase'] = df['phase'].replace(to_replace='iti', value='itiimg')

for phase in phases:

    if phase != 'all':
        df_sel = df[df['phase'] == phase].copy()
    else:
        df_sel = df.copy()

    dc = {'PRH' : 0, 'S1BF' : 0, 'Both' : 0, 'Total': 0}
    for i, row in df_sel.iterrows():
        if row['coordination_perirhinal']=='coordinated' and row['coordination_barrel']=='coordinated' :
            dc['Both'] += 1
        elif row['coordination_perirhinal']=='coordinated' and row['coordination_barrel']=='uncoordinated':
            dc['PRH'] += 1
        elif row['coordination_perirhinal']=='uncoordinated' and row['coordination_barrel']=='coordinated':
            dc['S1BF'] += 1
        dc['Total'] += 1


    f, ax = plt.subplots(1, 1, figsize=[2, 2])
    v2 = venn2(subsets = {'10': dc['PRH'],
                          '01': dc['S1BF'],
                          '11': dc['Both']},
               set_labels=('', ''), ax=ax)

    v2.get_patch_by_id('10').set_color(area_palette['Perirhinal'])
    v2.get_patch_by_id('01').set_color(area_palette['Barrel'])
    v2.get_patch_by_id('11').set_color(sns.xkcd_rgb['grey'])

    v2.get_patch_by_id('10').set_edgecolor('none')
    v2.get_patch_by_id('01').set_edgecolor('none')
    v2.get_patch_by_id('11').set_edgecolor('none')

    tot = dc['PRH'] + dc['S1BF'] + dc['Both']

    v2.get_label_by_id('10').set_text('%s\n%d\n(%.0f%%)' % ('PRH', dc['PRH'],
                                                            np.divide(dc['PRH'], tot)*100))

    v2.get_label_by_id('01').set_text('%s\n%d\n(%.0f%%)' % ('S1BF', dc['S1BF'],
                                                            np.divide(dc['S1BF'], tot)*100))

    v2.get_label_by_id('11').set_text('%s\n%d\n(%.0f%%)' % ('Both', dc['Both'],
                                                            np.divide(dc['Both'], tot)*100))

    for text in v2.subset_labels:
        text.set_fontsize(10)

    plt.show()

    plot_name = 'coordination_venn_diagram_{}.{}'.format(phase, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)





#  --- BOOTSTRAP FOR OVERLAP OF COORDINATION ACROSS PHASES ---------------------

dk = pd.DataFrame(columns=['phase', 'perc'])
for phase in phases:

    if phase != 'all' :
        df_sel = df[df['phase'] == phase].copy()
    else :
        df_sel = df.copy()

    for n in range(n_bootstraps) :
        indx = np.random.choice(np.arange(df_sel.shape[0]),
                                size=df_sel.shape[0], replace=True)
        df_sel_boot = df_sel.iloc[indx]

        dc = {'PRH' : 0, 'S1BF' : 0, 'Both' : 0, 'Total': 0}
        for i, row in df_sel_boot.iterrows():
            if row['coordination_perirhinal']=='coordinated' and row['coordination_barrel']=='coordinated' :
                dc['Both'] += 1
            elif row['coordination_perirhinal']=='coordinated' and row['coordination_barrel']=='uncoordinated':
                dc['PRH'] += 1
            elif row['coordination_perirhinal']=='uncoordinated' and row['coordination_barrel']=='coordinated':
                dc['S1BF'] += 1
            dc['Total'] += 1
        dk.loc[dk.shape[0], :] = [phase, dc['Both']/dc['Total']*100]


# --- COMPUTE CONFIDENCE INTERVALS ---------------------------------------------

y = []
err = []
for phase in phases:
    #print('\n{} {}'.format(match_with, phase))
    stats = dk[dk['phase'] == phase]['perc']
    stats = stats / 100
    obs_val = stats.median()
    y.append(obs_val)
    alpha = 0.95
    p = ((1.0 - alpha) / 2.0) * 100
    lower = max(0.0, np.percentile(stats, p))
    p = (alpha + ((1.0 - alpha) / 2.0)) * 100
    upper = min(1.0, np.percentile(stats, p))
    err.append([obs_val - lower, upper - obs_val])
    print('{}: {:.1f} {}% CI {:.1f} and {:.1f}'.format(
         phase, obs_val * 100, alpha * 100, lower * 100, upper * 100))

y = np.array(y)*100
err = np.array(err) * 100

# --- PLOT BARS ----------------------------------------------------------------

# f, ax = plt.subplots(1, 1, figsize=[2.5, 4])
#
# ax.bar(x=[0, 1, 2, 3],
#        height=y,
#        yerr=np.array(err).T,
#        linewidth=2.5,
#        ecolor='0.2')
#
# ax.legend().remove()
# ax.set_yticks([0, 20, 40, 60, 80, 100])
# ax.set_ylim([0, 100])
#
# for i, patch in enumerate(ax.patches):
#     patch.set_edgecolor(area_palette_2['Both'])
#
#     patch.set_facecolor('white')
#
# ax.set_xticks([0, 1, 2, 3])
# ax.set_xticklabels([task_phase_labels_nobreak_v2[l] for l in phases],
#                    rotation=45)
# ax.set_xlabel('')
# ax.set_ylabel('% of replay events\ncoordinated across all areas')
# sns.despine()
# plt.tight_layout()
#
# plot_name = 'perc_events_coordinated_both_areas_per_task_phase.{}'.format(plot_format)
# f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



