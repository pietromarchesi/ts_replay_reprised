import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
import matplotlib.patches as mpatches
import itertools
from utils import *
from statsmodels.stats.proportion import proportions_ztest
from constants import *
from scipy.stats import wilcoxon

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

group_iti_and_return = True
coordination_area = 'Perirhinal'


# PLOTTING PARAMETERS
#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'dec6'

phases = ['all', 'iti', 'img', 'run', 'reward']
restrict_to_direction = None

n_bootstraps = 300
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms    = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, 'RE_coordination_trial_outcome')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return)


if coordination_area == 'Perirhinal' or coordination_area == 'Barrel':
    df = add_coordination_per_event(df.copy(), ratemap_area=coordination_area,
                                    ratemap_settings=ratemap_settings,
                                    data_version=data_version)
elif coordination_area == 'Both':
    df = add_coordination_both_areas(df.copy(),
                                    ratemap_settings=ratemap_settings,
                                    data_version=data_version)

col_name = 'coordination_{}'.format(coordination_area.lower())
col_vals = ['coordinated', 'uncoordinated']

df = add_correct_incorrect(df=df, data_version='dec16')

# --- STATISTICS WITH Z-TEST -----------------------------------------------


df[df['current_trial_outcome'] == 'correct'][col_name].value_counts()
df[df['current_trial_outcome'] == 'incorrect'][col_name].value_counts()

#df_sel = df[np.isin(df['phase'], ['reward', 'run'])]

df_sel = df[(df['replay_direction'] == 'negative')]

val_counts_1 = df_sel[df_sel[col_name] == 'coordinated']['current_trial_outcome'].value_counts()
val_counts_2 = df_sel[df_sel[col_name] == 'uncoordinated']['current_trial_outcome'].value_counts()

n1 = val_counts_1['correct']
tot1 = val_counts_1['correct'] + val_counts_1['incorrect']

n2 = val_counts_2['correct']
tot2 = val_counts_2['correct'] + val_counts_2['incorrect']

print('% of correct events in coordinated: {:.1f}'.format(100*n1/tot1))
print('% of correct events in NOT coordinated: {:.1f}'.format(100*n2/tot2))

z, pval = proportions_ztest(count=[n1, n2], nobs=[tot1, tot2])
print('z-test pval={:.1g}'.format(pval))



# --- STATISTICS WITH Z-TEST -----------------------------------------------

df_sel = df[(df['replay_direction'] == 'negative')]

val_counts_1 = df_sel[df_sel['current_trial_outcome'] == 'correct'][col_name].value_counts()
val_counts_2 = df_sel[df_sel['current_trial_outcome'] == 'incorrect'][col_name].value_counts()

n1 = val_counts_1['coordinated']
tot1 = val_counts_1['coordinated'] + val_counts_1['uncoordinated']

n2 = val_counts_2['coordinated']
tot2 = val_counts_2['coordinated'] + val_counts_2['uncoordinated']

print('% of coordinated events in correct: {:.1f}'.format(100*n1/tot1))
print('% of coordinated events in NOT correct: {:.1f}'.format(100*n2/tot2))

z, pval = proportions_ztest(count=[n1, n2], nobs=[tot1, tot2])
print('z-test pval={:.1g}'.format(pval))