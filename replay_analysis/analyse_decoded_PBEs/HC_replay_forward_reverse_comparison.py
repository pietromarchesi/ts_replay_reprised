import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from plotting_style import *
from utils import load_bin_centers
from utils import prepare_bins
from scipy.stats import wilcoxon, mannwhitneyu
from utils import *
import itertools
import scipy
from numpy import median
from utils import bin_occurrences_for_chi_square_test, linearize_trajectory
from statsmodels.stats.proportion import proportions_ztest


data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

# PLOTTING PARAMETERS

save_plots = False
plot_format = 'svg'
dpi = 400
dot_scaling = 1200

phases = phases_plus_all
# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'HC_replay')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

# df, df_not_sig = load_replay(sessions=sessions,
#                              PBEs_settings_name=PBEs_settings_name,
#                              PBEs_area=PBEs_area,
#                              data_version=data_version,
#                              dec_settings_name=dec_settings_name,
#                              dec_area=dec_area,
#                              min_pbe_duration_in_ms=min_pbe_duration_in_ms,
#                              shuffle_types=shuffle_types,
#                              shuffle_p_vals=shuffle_p_vals,
#                              group_iti_and_return=group_iti_and_return,
#                              return_not_sig=True,
#                              max_average_speed=SPEED_THRESHOLD,
#                              NEWSPEED=True)

df = pd.read_csv(os.path.join(REPLAY_FOLDER, 'df_final_selection.csv'))



# --- LOAD BIN CENTERS --------------------------------------------------------
sess_ind = 10
F = loadmat(DATA_FOLDER + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]
bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, session)
bin_centers, bin_labels = prepare_bins(bin_centers_combined)

# --- POSITIVE NEGATIVE EVENTS PER TASK PHASES ACROSS SESSIONS -----------------

dg = pd.DataFrame(columns=['sess_ind', 'phase', 'perc_pos', 'npos', 'ntot'])
for phase in phases:
    for sess_ind in sessions:
        if phase == 'all':
            df_sel = df[(df['sess_ind'] == sess_ind) & (df['epoch'] == 'task')]
        else:
            df_sel = df[(df['sess_ind'] == sess_ind) & (df['epoch'] == 'task')
                        & (df['phase'] == phase)]
        if df_sel.shape[0] >= 5:
            npos = df_sel[df_sel['replay_direction'] == 'positive'].shape[0]
            nneg = df_sel[df_sel['replay_direction'] == 'negative'].shape[0]
            perc = (100 * npos / (npos+nneg))
            ntot = npos+nneg
            dg.loc[dg.shape[0], :] = [sess_ind, phase, perc, npos, ntot]

dg['perc_pos'] = pd.to_numeric(dg['perc_pos'])
dg['npos'] = pd.to_numeric(dg['npos'])
dg['ntot'] = pd.to_numeric(dg['ntot'])


print('\nPERCENTAGE FORWARD EVENTS PER TASK PHASE, WILCOXON')
n_tests = len(['all'] + phases)
for phase in ['all'] + phases:
    m = dg.groupby('phase')['perc_pos'].quantile(q=0.5).loc[phase]
    q1 = dg.groupby('phase')['perc_pos'].quantile(q=0.25).loc[phase]
    q2 = dg.groupby('phase')['perc_pos'].quantile(q=0.75).loc[phase]
    print('\n{} - % forward events for events: {:.0f}%, {:.0f}% - {:.0f}%'.format(phase, m, q1, q2))

    x = dg[dg['phase'] == phase]['perc_pos']
    stat, wilp = wilcoxon(x-50)
    adjusted_p = wilp * n_tests
    is_sig = adjusted_p < 0.05
    print('    percentage > 50%: p={:.1g}, significant={}'.format(adjusted_p, is_sig))


print('\nPercentage of forward events different per task phase')
allphases = ['all'] + phases
combos = list(itertools.combinations(allphases, 2))

for ph1, ph2 in combos:
    x = dg[dg['phase'] == ph1]['perc_pos']
    y = dg[dg['phase'] == ph2]['perc_pos']
    stat, pval = mannwhitneyu(x, y)
    is_sig = pval < (0.05 / len(combos))
    adjusted_p = pval * len(combos)
    print('{} vs {} mannwhitney p={:.3f} - {}'.format(ph1, ph2, adjusted_p, is_sig))


f, ax = plt.subplots(1, 1, figsize=slightly_vertical_panel_small)

sns.barplot(x=['all'] + phases, y=[100, 100, 100, 100, 100],
            edgecolor=replay_direction_palette['negative'],
            facecolor='white', linewidth=3.5)

g = sns.barplot(data=dg, x='phase', y='perc_pos', estimator=median,
                edgecolor=replay_direction_palette['positive'],
                facecolor='white', linewidth=3.5, errcolor='0.2')


sns.despine()
ax.set_ylabel('% of replay events')
ax.set_xticklabels([task_phase_labels_nobreak[l._text] for l in ax.get_xticklabels()],
                   rotation=45)
ax.set_xlabel('')
plt.tight_layout()
ax.axhline(50, c=maze_color, ls='--', lw=2)

plot_name = 'perc_posneg_events_per_task_phase.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#plt.close(fig=f)




# --- STATISTICS WITH Z-TEST -----------------------------------------------

# dfxy = df.copy()
# dg = pd.DataFrame(columns=['phase', 'perc_pos', 'npos', 'ntot'])
# for phase in ['all'] + phases:
#     if phase == 'all':
#         dk = dfxy[(dfxy['epoch'] == 'task')]
#     else:
#         dk = dfxy[(dfxy['epoch'] == 'task') & (dfxy['phase'] == phase)]
#     if dk.shape[0] > 5:
#         npos = dk[dk['replay_direction'] == 'positive'].shape[0]
#         nneg = dk[dk['replay_direction'] == 'negative'].shape[0]
#         perc = (100 * npos / (npos+nneg))
#         ntot = npos+nneg
#         dg.loc[dg.shape[0], :] = [phase, perc, npos, ntot]
#
# dg['perc_pos'] = pd.to_numeric(dg['perc_pos'])
# dg['npos'] = pd.to_numeric(dg['npos'])
# dg['ntot'] = pd.to_numeric(dg['ntot'])
#
#
# allphases = ['all'] + phases
# combos = list(itertools.combinations(allphases, 2))
# n_tests = len(allphases)
#
# print('\nTASK PHASES DIFFERENT THAN 50% Z-TEST')
# for ph1 in ['all'] + phases:
#     if ph1 == 'all':
#         ncentrif = dg['npos'].values[0]
#         nsamp = dg['ntot'].values[0]
#     else:
#         ncentrif = dg[dg['phase'] == ph1]['npos'].values[0]
#         nsamp = dg[dg['phase'] == ph1]['ntot'].values[0]
#
#     ncentrif2 = int(nsamp / 2)
#
#     z, pval = proportions_ztest(count=[ncentrif, ncentrif2], nobs=[nsamp, nsamp])
#     is_sig = pval < (0.05 / n_tests)
#     adjusted_p = pval * n_tests
#     print('{} different from 50%, {} of {}, z-test p={:.3f} - {}'.format(ph1, ncentrif, nsamp, adjusted_p, is_sig))
#
#
# n_tests =  len(combos)
# print('\nTASK PHASES COMPARISON Z-TEST')
# for ph1, ph2 in combos:
#     ncentrif1 = dg[dg['phase'] == ph1]['npos'].values[0]
#     nsamp1 = dg[dg['phase'] == ph1]['ntot'].values[0]
#
#     ncentrif2 = dg[dg['phase'] == ph2]['npos'].values[0]
#     nsamp2 = dg[dg['phase'] == ph2]['ntot'].values[0]
#
#     z, pval = proportions_ztest(count=[ncentrif1, ncentrif2], nobs=[nsamp1, nsamp2])
#     is_sig = pval < (0.05 / n_tests)
#     adjusted_p = pval * n_tests
#     print('{} vs {} z-test p={:.3f} - {}'.format(ph1, ph2, adjusted_p, is_sig))
#





# # --- BARPLOT PERCENTAGE OF FORWARD/REVERSE EVENTS ACROSS SESSIONS -------------
# f, ax = plt.subplots(1, 1, figsize=slightly_vertical_panel)
#
# sns.barplot(x=['all'] + phases, y=[100, 100, 100, 100, 100],
#             edgecolor=replay_direction_palette['negative'],
#             facecolor='white', linewidth=3.5)
#
# g = sns.barplot(data=dg, x='phase', y='perc_pos', estimator=median,
#                 edgecolor=replay_direction_palette['positive'],
#                 facecolor='white', linewidth=3.5, errcolor='0.2')
#
#
# sns.despine()
# ax.set_ylabel('% of replay events')
# ax.set_xticklabels([task_phase_labels_nobreak[l._text] for l in ax.get_xticklabels()],
#                    rotation=45)
# ax.set_xlabel('')
# plt.tight_layout()
# ax.axhline(50, c=maze_color, ls='--', lw=2)
#
# plot_name = 'perc_posneg_events_per_task_phase.{}'.format(plot_format)
# f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
# plt.close(fig=f)


# --- METRIC BOXPLOTS FOR FORWARD VS REVERSE EVENTS ----------------------------

statistics = ['replay_score', 'dist_traversed_cm', 'percentage_active',
              'sharpness', 'event_duration_in_ms', 'max_jump_dist',
              'compression_factor']

# cols = ['replay_score',
#         'dist_traversed_cm',
#         'mean_jump_dist', 'sharpness', 'n_active']

for statistic in statistics:
    df_pos = df[(df['epoch'] == 'task') & (df['replay_direction'] == 'positive')]
    df_neg = df[(df['epoch'] == 'task') & (df['replay_direction'] == 'negative')]

    x = df_pos[statistic].values
    y = df_neg[statistic].values
    st,pval = mannwhitneyu(x, y)
    sig = pval < (0.05 / len(statistics))
    print('{}, medians: {:.2f} vs {:.2f}: {}, p_val = {:.3e}, significant: {}'.format(statistic,
          np.median(x), np.median(y), st, pval, sig))


    f, ax = plt.subplots(1, 1, figsize=[2, 4])
    sns.boxplot(data=df[df['epoch'] == 'task'], y=statistic,color='white',
                fliersize=2,x='replay_direction', order=['positive', 'negative'])

    plt.setp(ax.artists[0], edgecolor=replay_direction_palette['positive'], facecolor='w')
    plt.setp(ax.artists[1], edgecolor=replay_direction_palette['negative'], facecolor='w')

    plt.setp(ax.lines[0:6], color=replay_direction_palette['positive'])
    plt.setp(ax.lines[6:], color=replay_direction_palette['negative'])
    ax.set_ylabel(replay_metric_labels[statistic])
    ax.set_xticklabels([replay_direction_labels_break['positive'],
                        replay_direction_labels_break['negative']], rotation=45)
    ax.set_xlabel('')
    sns.despine()
    plt.tight_layout()

    plot_name = 'replay_direction_{}_boxplot.{}'.format(statistic, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- PBE LOCATION, START AND END OF TRAJ. FOR FORWARD / BACKWARD -------------

for event in ['pbe_loc_bin', 'dec_start_bin', 'dec_end_bin']:

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
    plot_maze(ax, c='grey')
    mark_locations(ax, color=maze_color)

    for replay_direction in ['positive', 'negative']:

        df_sel = df[(df['epoch'] == 'task') & (df['replay_direction'] == replay_direction)]

        bins = merge_duplicate_left_right_bins(np.array(df_sel[event]))
        bin_counts = pd.value_counts(bins, normalize=True)
        for bin, counts in bin_counts.iteritems():
            #print(bin, counts)
            x, y = get_position_bin(bin, bin_centers, bin_labels)
            if replay_direction == 'positive':
                x = x - 4
            if replay_direction == 'negative':
                x = x + 3
            ax.scatter(x, y, s=counts * dot_scaling,
                       c=replay_direction_palette[replay_direction],
                       zorder=10)
        ax.axis('off')
        #plt.tight_layout()
        #ax.set_title(event)
        plot_name = 'difference_in_representation_of_{}_forward_vs_backward.{}'.format(event,
                                                                       plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- PBE LOCATION, START AND END OF TRAJ. FOR FORWARD / BACKWARD -------------

for phase in phases:
    for event in ['dec_start_bin', 'dec_end_bin']:

        f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
        plot_maze(ax, c='grey')
        mark_locations(ax, color=maze_color)

        for replay_direction in ['positive', 'negative']:

            df_sel = df[(df['epoch'] == 'task') &
                        (df['replay_direction'] == replay_direction) &
                        (df['phase'] == phase)]

            bins = merge_duplicate_left_right_bins(np.array(df_sel[event]))
            bin_counts = pd.value_counts(bins, normalize=True)
            for bin, counts in bin_counts.iteritems():
                #print(bin, counts)
                x, y = get_position_bin(bin, bin_centers, bin_labels)
                if replay_direction == 'positive':
                    x = x - 4
                if replay_direction == 'negative':
                    x = x + 3
                ax.scatter(x, y, s=counts * dot_scaling,
                           c=replay_direction_palette[replay_direction],
                           zorder=10)
            ax.axis('off')
            #plt.tight_layout()
            #ax.set_title(event)
            plot_name = 'difference_in_representation_of_{}_forward_vs_backward_{}.{}'.format(event, phase,
                                                                           plot_format)
            f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# --- ALL DECODED LOCATIONS BY REPLAY DIRECTION --------------------------------

f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

for replay_direction in ['positive', 'negative'] :
    df_sel = df[(df['epoch'] == 'task') & (df['replay_direction'] == replay_direction)]
    traj = np.hstack(df_sel['trajectory'])
    traj = merge_duplicate_left_right_bins(traj)
    bin_counts = pd.value_counts(traj, normalize=True)

    for bin, counts in bin_counts.iteritems() :
        # print(bin, counts)
        x, y = get_position_bin(bin, bin_centers, bin_labels)

        if replay_direction == 'positive' :
            x = x - 4
        if replay_direction == 'negative' :
            x = x + 3
        ax.scatter(x, y, s=counts * dot_scaling,
                   c=replay_direction_palette[replay_direction],
                   zorder=10)
        ax.scatter(x, y, s=counts * 1000, c='grey')
    plot_maze(ax, c=maze_color)
    mark_locations(ax, color=maze_color)
    plt.tight_layout()
    ax.axis('off')

plot_name = 'locations_representation_in_decoded_traj_positive_negative.{}'.format(
    plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- DISTRIBUTIONS OF DECODED TRAJECTORIES FOR FORWARD VS REVERSE -------------

bin_counts_per_direction = {}
for direction in ['positive', 'negative']:

    df_sel = df[(df['epoch'] == 'task') & (df['replay_direction'] == direction)]
    #print(df_sel.shape[0])

    bins = np.hstack(df_sel['trajectory'])
    #bins = np.hstack(df_sel['dec_start_bin'])
    # TODO CHECK THIS, replacement or not?
    #bins = np.random.choice(bins, 400, replace=False) #downsampling should be required
    # but we try it anyways
    #bin_freq = bin_occurrences_for_chi_square_test(bins)

    bin_freq = pd.value_counts(bins).sort_index().to_list()
    bin_counts_per_direction[direction] = bin_freq


rs = scipy.stats.chisquare(f_obs=bin_counts_per_direction['positive'],
                           f_exp=bin_counts_per_direction['negative'])
print('Difference in spatial distribution '
      'between positive and negative: p={:.2e}'.format(rs[1]))


#x = np.arange(8)
y = bin_counts_per_direction['positive']
y2 = bin_counts_per_direction['negative']
x = np.arange(y.__len__())

f, ax = plt.subplots(1, 1)
plt.bar(x, y)
plt.bar(x+0.2, y2)

