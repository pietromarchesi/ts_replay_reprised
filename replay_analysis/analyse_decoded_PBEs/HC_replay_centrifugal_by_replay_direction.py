import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
import matplotlib.patches as mpatches
import itertools
from utils import *
from statsmodels.stats.proportion import proportions_ztest
from constants import *

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']
group_iti_and_return = True

# PLOTTING PARAMETERS
#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'dec6'
phases = ['all', 'iti', 'img', 'run', 'reward']

n_bootstraps = 300


save_plots = False
plot_format = 'png'
dpi = 400

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, 'HC_replay')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

contrast = 'radial_direction'
col_name = 'replay_direction'
col_vals = ['positive', 'negative']

df = load_replay(sessions=sessions,
                 PBEs_settings_name=PBEs_settings_name,
                 PBEs_area=PBEs_area,
                 data_version=data_version,
                 dec_settings_name=dec_settings_name,
                 dec_area=dec_area,
                 min_pbe_duration_in_ms=min_pbe_duration_in_ms,
                 shuffle_types=shuffle_types,
                 shuffle_p_vals=shuffle_p_vals,
                 group_iti_and_return=group_iti_and_return)


df_mod = add_radial_direction(df.copy())

name1, name2 = np.sort(df_mod[contrast].unique())


# --- STATS WITH Z-TEST ------------------------------------------------

df_sel = df_mod[df_mod['epoch'] == 'task']

dg = pd.DataFrame( columns=['phase', 'replay_direction', 'perc_centr', 'ncentrif', 'ntot'])
for phase in phases:
    for dir in col_vals:
        if phase == 'all' :
            dk = df_sel[df_sel[col_name] == dir].copy()
        else :
            dk = df_sel[(df_sel[col_name] == dir) & (df_sel['phase'] == phase)].copy()

        if dk.shape[0] > 5:
            npos = dk[dk[contrast] == name1].shape[0]
            nneg = dk[dk[contrast] == name2].shape[0]
            perc = (100 * npos / (npos + nneg))
            ntot = npos + nneg
            dg.loc[dg.shape[0], :] = [phase, dir, perc, npos, ntot]
        else :
            pass


n_tests = len(phases)
for phase in phases:
    d1 = dg[(dg['replay_direction'] == 'positive') & (dg['phase'] == phase)]
    d2 = dg[(dg['replay_direction'] == 'negative') & (dg['phase'] == phase)]

    if d1.shape[0] > 0 and d2.shape[0] > 0:
        ncentrif1 = d1['ncentrif'].values[0]
        nsamp1 = d1['ntot'].values[0]

        ncentrif2 = d2['ncentrif'].values[0]
        nsamp2 = d2['ntot'].values[0]

        z, pval = proportions_ztest(count=[ncentrif1, ncentrif2], nobs=[nsamp1, nsamp2])
        is_sig = pval < (0.05 / n_tests)
        adjusted_p = pval * n_tests
        #print('{} different from 50%, {} of {}, z-test p={:.3f} - {}'.format(val, ncentrif, nsamp, adjusted_p, is_sig))
        print('{} - {} - coord different from uncoord - {:.4f} {}'.format(
            phase, contrast, adjusted_p, is_sig))


# --- BOOTSTRAP --------------------------------------------------------

dg = pd.DataFrame( columns=['phase', 'replay_direction', 'perc_centr', 'ncentrif', 'ntot'])
for phase in phases:
    for dir in col_vals:
        for n in range(n_bootstraps) :
            indx = np.random.choice(np.arange(df_mod.shape[0]), size=df_mod.shape[0], replace=True)
            df_sel_boot = df_sel.iloc[indx]

            if phase == 'all' :
                dk = df_sel_boot[df_sel_boot[col_name] == dir].copy()
            else :
                dk = df_sel_boot[(df_sel_boot[col_name] == dir) & (df_sel_boot['phase'] == phase)].copy()

            if dk.shape[0] > 5 :
                npos = dk[dk[contrast] == name1].shape[0]
                nneg = dk[dk[contrast] == name2].shape[0]
                perc = (100 * npos / (npos + nneg))
                ntot = npos + nneg
                dg.loc[dg.shape[0], :] = [phase, dir, perc, npos, ntot]
            else :
                pass

dg['perc_centr'] = pd.to_numeric(dg['perc_centr'])
dg['ncentrif'] = pd.to_numeric(dg['ncentrif'])
dg['ntot'] = pd.to_numeric(dg['ntot'])


# --- COMPUTE CONFIDENCE INTERVALS -------------------------------------
y = []
err = []
for phase in phases:
    for dir in col_vals:
        stats = dg[(dg['replay_direction'] == dir) & (dg['phase'] == phase)]['perc_centr']
        stats = stats / 100
        obs_val = stats.median()
        y.append(obs_val)
        alpha = 0.95
        p = ((1.0 - alpha) / 2.0) * 100
        lower = max(0.0, np.percentile(stats, p))
        p = (alpha + ((1.0 - alpha) / 2.0)) * 100
        upper = min(1.0, np.percentile(stats, p))
        err.append([obs_val - lower, upper - obs_val])
        print('{} {}: {:.1f} {}% CI {:.1f} and {:.1f}'.format(
            dir, phase, obs_val * 100, alpha * 100, lower * 100,
                        upper * 100))
y = np.array(y) * 100
err = np.array(err) * 100


# --- PLOT BARS --------------------------------------------------------
f, ax = plt.subplots(1, 1, figsize=[2.5, 4])

ax.bar(x=[0, 1,
          3, 4,
          6, 7,
          9, 10,
          12, 13],
       height=y,
       yerr=np.array(err).T,
       linewidth=2.5,
       edgecolor='0.2')

ax.legend().remove()
ax.set_yticks([0, 20, 40, 60, 80, 100])
ax.set_ylim([0, 100])
ax.set_xticks([0.5, 3.5, 6.5, 9.5, 12.5])

inx = np.array([0, 2, 4, 6, 8])
for i, patch in enumerate(ax.patches) :

    if np.isin(i, inx) :
        patch.set_edgecolor(replay_direction_palette['positive'])
        patch.set_facecolor('white')

    if np.isin(i, inx + 1) :
        patch.set_edgecolor(replay_direction_palette['negative'])
        patch.set_facecolor('white')

ax.axhline(50, c=maze_color, ls='--', lw=2)

sns.despine()
ax.set_ylabel('% of centrifugal events')

ax.set_xticklabels([task_phase_labels_nobreak_v2[l] for l in phases], rotation=45)
ax.set_xlabel('')
plt.tight_layout()

plot_name = 'perc_centrif_events_per_replay_direction.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#plt.close(fig=f)



# --- PLOT LEGEND ------------------------------------------------------
# pos_patch = mpatches.Patch(edgecolor=coordinated_palette['coordinated'],
#                        facecolor='white', linewidth=2.5,
#                        label='Coordinated with both areas')
# neg_patch = mpatches.Patch(edgecolor=coordinated_palette['uncoordinated'],
#                        facecolor='white', linewidth=2.5,
#                        label='Not coordinated with both areas')
# f, ax = plt.subplots(1, 1, figsize=[3, 1])
# ax.legend(handles=[pos_patch, neg_patch], frameon=False)
# ax.axis('off')
# plt.tight_layout()

# plot_name = 'replay_{}_legend_4.{}'.format(contrast, plot_format)
# f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



