import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from utils import load_bin_centers
from utils import prepare_bins
from utils import plot_maze, mark_locations
from utils import get_position_bin
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from utils import is_on_central_arm, merge_duplicate_left_right_bins
from utils import add_ripples_to_pbes_df
from session_selection import sessions_HC_replay
from utils import load_decoded_PBEs
from utils import get_time_spent_per_task_phase
import quantities as pq
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
from scipy.ndimage import gaussian_filter1d
import itertools
from utils import *



data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'
ratemap_settings   = 'dec6'


#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]

group_iti_and_return = True

# PLOTTING PARAMETERS

save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, 'HC_replay_correct_incorrect')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

# df, df_not_sig = load_replay(sessions=sessions,
#                              PBEs_settings_name=PBEs_settings_name,
#                              PBEs_area=PBEs_area,
#                              data_version=data_version,
#                              dec_settings_name=dec_settings_name,
#                              dec_area=dec_area,
#                              min_pbe_duration_in_ms=min_pbe_duration_in_ms,
#                              shuffle_types=shuffle_types,
#                              shuffle_p_vals=shuffle_p_vals,
#                              group_iti_and_return=group_iti_and_return,
#                              return_not_sig=True)


df = pd.read_csv(os.path.join(REPLAY_FOLDER, 'df_final_selection.csv'))


if group_iti_and_return:
    phases = ['iti', 'img', 'run', 'reward']
else:
    phases = ['iti', 'img', 'return', 'reward', 'run']


df = add_correct_incorrect(df, data_version=data_version)


# --- LOAD BIN CENTERS --------------------------------------------------------
sess_ind = 10
F = loadmat(DATA_FOLDER + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]
bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, session)
bin_centers, bin_labels = prepare_bins(bin_centers_combined)






# --- ALL PHASES COMBINED ------------------------------------------------------

# for phase in phases:
#     for trial_outcome_col in ['previous_trial_outcome', 'current_trial_outcome']:

#df['phase'] = df['phase'].replace(to_replace='run', value='reward')

#df['phase'] = df['phase'].replace(to_replace='img', value='reward')


for phase in ['reward']:
    for trial_outcome_col in ['current_trial_outcome']:

        rs = pd.DataFrame(columns=['sess_ind', 'phase', 'outcome', 'replay_rate'])

        for sess_ind in df['sess_ind'].unique():

            timespent = get_time_spent_per_task_phase_outcome(sess_ind,
                                                      data_version=data_version,
                                                      speed_threshold=12)
            df_sel = df[(df['epoch'] == 'task') & (df['sess_ind'] == sess_ind)]

            for outc in ['correct', 'incorrect'] :

                if phase == 'all':
                    nreplay = df_sel[df_sel[trial_outcome_col] == outc].shape[0]
                else :
                    nreplay = df_sel[(df_sel['phase'] == phase) &
                                     (df_sel[trial_outcome_col] == outc)].shape[0]

                replayrate = nreplay / timespent[outc][phase].rescale(pq.min)
                rs.loc[rs.shape[0], :] = [sess_ind, phase, outc, replayrate]

        rs['replay_rate'] = pd.to_numeric([i.item() for i in rs['replay_rate']])

        f, ax = plt.subplots(1, 1, figsize=[1.5, 3])
        sns.barplot(x='outcome', y='replay_rate', data=rs,
                    edgecolor=sns.xkcd_rgb['dark grey'], facecolor='white', errcolor='0.2',
                    linewidth=1.5, order=['correct', 'incorrect'])
        ax.set_ylabel('CA1 replay events per minute\nduring {}'.format(task_phase_labels[phase]))
        ax.set_xlabel('')
        ax.set_xticklabels(['Correct', 'Incorrect'], rotation=30)

        for i in [0]:
            plt.setp(ax.patches[i], edgecolor=outcome_palette['correct'], facecolor='w',
                     linewidth=3)
        for i in [1]:
            plt.setp(ax.patches[i], edgecolor=outcome_palette['incorrect'],facecolor='w',
                     linewidth=3)

        ax.legend().remove()
        sns.despine()
        plt.tight_layout()

        plot_name = 'replay_rate_per_task_phase_coordinated_{}_corr_{}.{}'.format(phase, trial_outcome_col, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

        plt.close(fig=f)


        # --- STATISTICS ---------------------------------------------------------------


        df1 = rs[(rs['phase'] == phase) & (rs['outcome'] == 'correct')]
        df2 = rs[(rs['phase'] == phase) & (rs['outcome'] == 'incorrect')]

        ind = ~np.logical_or(np.isnan(df1['replay_rate']).values, np.isnan(df2['replay_rate']).values)
        df1 = df1[ind]
        df2 = df2[ind]

        np.testing.assert_array_equal(df1['sess_ind'], df2['sess_ind'])
        #print(df1[col].hasnans, df2[col].hasnans)
        x = df1['replay_rate'].values
        y = df2['replay_rate'].values
        #print(np.median(x), np.median(y))
        st,pval = wilcoxon(x, y)
        adjusted_p = pval
        print('{}, {}, {}, medians: {:.2f} vs {:.2f}: p_val = {:.1g}, sig: {}'.format('replay_rate',
              phase,  trial_outcome_col, np.median(x), np.median(y), adjusted_p, adjusted_p<0.05))
        # df1 = rs[(rs['phase'] == 'img')]
        # df2 = rs[(rs['phase'] == 'run')]
        # df3 = rs[(rs['phase'] == 'iti')]


