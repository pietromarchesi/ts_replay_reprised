import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from utils import *
from utils import plot_maze, mark_locations
from utils import get_position_bin
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from utils import is_on_central_arm, merge_duplicate_left_right_bins
from utils import add_ripples_to_pbes_df
from session_selection import sessions_HC_replay
from utils import load_decoded_PBEs
from utils import get_time_spent_per_task_phase
import quantities as pq
import matplotlib.patches as mpatches
from scipy.ndimage import gaussian_filter1d
import itertools
from utils import load_replay
from numpy import median
from statsmodels.stats.proportion import proportions_ztest

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
epochs             = ['task']
phases             = phases_plus_all


# PLOTTING PARAMETERS

save_plots = False
plot_format = 'svg'
dpi = 400
dot_scaling = 1200

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'HC_replay')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

# df, df_not_sig = load_replay(sessions=sessions,
#                              PBEs_settings_name=PBEs_settings_name,
#                              PBEs_area=PBEs_area,
#                              data_version=data_version,
#                              dec_settings_name=dec_settings_name,
#                              dec_area=dec_area,
#                              min_pbe_duration_in_ms=min_pbe_duration_in_ms,
#                              shuffle_types=shuffle_types,
#                              shuffle_p_vals=shuffle_p_vals,
#                              group_iti_and_return=group_iti_and_return,
#                              return_not_sig=True,
#                              max_average_speed=SPEED_THRESHOLD,
#                              NEWSPEED=True,
#                              exclude_run_phase=True)

df = pd.read_csv(os.path.join(REPLAY_FOLDER, 'df_final_selection.csv'))


# --- LOAD BIN CENTERS --------------------------------------------------------
sess_ind = 10
F = loadmat(DATA_FOLDER + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]
bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, session)
bin_centers, bin_labels = prepare_bins(bin_centers_combined)


for direction in ['all']:#['all', 'positive', 'negative']:

    # ---- MEAN ACROSS SESSIONS ------------------------------------------------

    print('\n DIRECTION = {}'.format(direction))

    df_sel = df[df['epoch'] == 'task']
    if direction != 'all':
        df_sel = df_sel[df_sel['replay_direction'] == direction]
    dfxy = add_radial_direction(df_sel)


    dg = pd.DataFrame(columns=['sess_ind', 'phase', 'perc_centr', 'ntot'])
    for phase in phases:
        for sess_ind in sessions:
            if phase == 'all':
                dk = dfxy[(dfxy['sess_ind'] == sess_ind) & (dfxy['epoch'] == 'task')]
            else:
                dk = dfxy[(dfxy['sess_ind'] == sess_ind) & (dfxy['epoch'] == 'task')
                              & (dfxy['phase'] == phase)]
            if dk.shape[0] >= 5:
                npos = dk[dk['radial_direction'] == 'centrifugal'].shape[0]
                nneg = dk[dk['radial_direction'] == 'centripetal'].shape[0]
                perc = (100 * npos / (npos+nneg))
                ntot = npos+nneg
                dg.loc[dg.shape[0], :] = [sess_ind, phase, perc, ntot]

    dg['perc_centr'] = pd.to_numeric(dg['perc_centr'])


    f, ax = plt.subplots(1, 1, figsize=slightly_vertical_panel_small)


    sns.barplot(x=phases, y=[100] * len(phases),
                edgecolor=radial_direction_palette['centripetal'],
                facecolor='white', linewidth=3.5)

    g = sns.barplot(data=dg, x='phase', y='perc_centr', estimator=median,
                    edgecolor=radial_direction_palette['centrifugal'],
                    facecolor='white', linewidth=3.5, errcolor='0.2')

    sns.despine()
    ax.set_ylabel('% of replay events')
    ax.set_xticklabels([task_phase_labels_nobreak[l._text] for l in ax.get_xticklabels()],
                       rotation=45)
    ax.set_xlabel('')
    ax.axhline(50, c=maze_color, ls='--', lw=2)
    plt.tight_layout()

    plot_name = 'perc_centrifugal_events_per_task_phase_{}.{}'.format(direction, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
    #plt.close(fig=f)


    # --- MEAN ACROSS SESSIONS STATISTICS --------------------------------------
    print('\nTASK PHASES MEDIAN AND INTERQUARTILE WITH WILCOXON')
    for phase in phases:
        m = dg.groupby('phase')['perc_centr'].quantile(q=0.5).loc[phase]
        q1 = dg.groupby('phase')['perc_centr'].quantile(q=0.25).loc[phase]
        q2 = dg.groupby('phase')['perc_centr'].quantile(q=0.75).loc[phase]
        x = dg[dg['phase'] == phase]['perc_centr']
        stat, p = wilcoxon(x-50)
        adjusted_p = p * len(phases)
        sig = adjusted_p < 0.05
        print('% centrifugal events {}: {:.2g}, {:.2g} - {:.2g}, p={:.1g} - sig = {}(Wilcoxon)'
              ''.format(phase, m, q1, q2, adjusted_p, sig))

    combos = list(itertools.combinations(phases, 2))

    print('\nTASK PHASES COMPARISON')
    for ph1, ph2 in combos:
        x = dg[dg['phase'] == ph1]['perc_centr']
        y = dg[dg['phase'] == ph2]['perc_centr']
        stat, pval = mannwhitneyu(x, y)
        is_sig = pval < (0.05 / len(combos))
        adjusted_p = pval * len(combos)
        print('{} vs {} Mann-Whitney U-test p={:.3f} - {}'.format(ph1, ph2, adjusted_p, is_sig))


    # --- STATISTICS WITH Z-TEST -----------------------------------------------

    # dg = pd.DataFrame(columns=['phase', 'perc_centr', 'ncentrif', 'ntot'])
    # for phase in ['all'] + phases:
    #     if phase == 'all':
    #         dk = dfxy[(dfxy['epoch'] == 'task')]
    #     else:
    #         dk = dfxy[(dfxy['epoch'] == 'task') & (dfxy['phase'] == phase)]
    #     if dk.shape[0] > 5:
    #         npos = dk[dk['radial_direction'] == 'centrifugal'].shape[0]
    #         nneg = dk[dk['radial_direction'] == 'centripetal'].shape[0]
    #         perc = (100 * npos / (npos+nneg))
    #         ntot = npos+nneg
    #         dg.loc[dg.shape[0], :] = [phase, perc, npos, ntot]
    #
    # dg['perc_centr'] = pd.to_numeric(dg['perc_centr'])
    # dg['ncentrif'] = pd.to_numeric(dg['ncentrif'])
    # dg['ntot'] = pd.to_numeric(dg['ntot'])
    #
    # allphases = ['all'] + phases
    # combos = list(itertools.combinations(allphases, 2))
    # n_tests = len(allphases)
    #
    # print('\nTASK PHASES DIFFERENT THAN 50% Z-TEST')
    # for ph1 in ['all'] + phases:
    #     if ph1 == 'all':
    #         ncentrif = dg['ncentrif'].values[0]
    #         nsamp = dg['ntot'].values[0]
    #     else:
    #         ncentrif = dg[dg['phase'] == ph1]['ncentrif'].values[0]
    #         nsamp = dg[dg['phase'] == ph1]['ntot'].values[0]
    #
    #     ncentrif2 = int(nsamp / 2)
    #
    #     z, pval = proportions_ztest(count=[ncentrif, ncentrif2], nobs=[nsamp, nsamp])
    #     is_sig = pval < (0.05 / n_tests)
    #     adjusted_p = pval * n_tests
    #     print('{} different from 50%, {} of {}, z-test p={:.3f} - {}'.format(ph1, ncentrif, nsamp, adjusted_p, is_sig))
    #
    # n_tests =  len(combos)
    # print('\nTASK PHASES COMPARISON Z-TEST')
    # for ph1, ph2 in combos:
    #     ncentrif1 = dg[dg['phase'] == ph1]['ncentrif'].values[0]
    #     nsamp1 = dg[dg['phase'] == ph1]['ntot'].values[0]
    #
    #     ncentrif2 = dg[dg['phase'] == ph2]['ncentrif'].values[0]
    #     nsamp2 = dg[dg['phase'] == ph2]['ntot'].values[0]
    #
    #     z, pval = proportions_ztest(count=[ncentrif1, ncentrif2], nobs=[nsamp1, nsamp2])
    #     is_sig = pval < (0.05 / n_tests)
    #     adjusted_p = pval * n_tests
    #     print('{} vs {} z-test p={:.3f} - {}'.format(ph1, ph2, adjusted_p, is_sig))


    # from numpy import median
    # f, ax = plt.subplots(1, 1, figsize=slightly_vertical_panel)
    #
    # sns.barplot(x=['all'] + phases, y=[100] * len(['all']+phases),
    #             edgecolor=radial_direction_palette['centripetal'],
    #             facecolor='white', linewidth=3.5)
    #
    # g = sns.barplot(data=dg, x='phase', y='perc_centr', estimator=median,
    #                 edgecolor=radial_direction_palette['centrifugal'],
    #                 facecolor='white', linewidth=3.5, errcolor='0.2')
    #
    #
    # sns.despine()
    # ax.set_ylabel('% of replay events')
    # ax.set_xticklabels([task_phase_labels_nobreak[l._text] for l in ax.get_xticklabels()],
    #                    rotation=45)
    # ax.set_xlabel('')
    # plt.tight_layout()
    #
    # plot_name = 'perc_centrifugal_events_per_task_phase_{}_v2.{}'.format(direction, plot_format)
    # f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
    # #plt.close(fig=f)




pos_patch = mpatches.Patch(edgecolor=radial_direction_palette['centrifugal'],
                           facecolor='white', linewidth=2.5,
                           label='Centrifugal')
neg_patch = mpatches.Patch(edgecolor=radial_direction_palette['centripetal'],
                           facecolor='white', linewidth=2.5,
                           label='Centripetal')
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=[pos_patch, neg_patch], frameon=False)
ax.axis('off')
plt.tight_layout()

plot_name = 'replay_radial_direction_legend_4.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)