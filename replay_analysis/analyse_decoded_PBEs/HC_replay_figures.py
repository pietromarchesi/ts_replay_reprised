import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from utils import load_bin_centers
from utils import prepare_bins
from utils import plot_maze, mark_locations
from utils import get_position_bin
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from utils import is_on_central_arm, merge_duplicate_left_right_bins
from utils import add_ripples_to_pbes_df
from session_selection import sessions_HC_replay
from utils import load_decoded_PBEs
from utils import get_time_spent_per_task_phase
import quantities as pq
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
from scipy.ndimage import gaussian_filter1d
import itertools
from utils import *


data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]

group_iti_and_return = True



# PLOTTING PARAMETERS
save_plots = False
plot_format = 'svg'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms = 50

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'HC_replay')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

# df, df_not_sig = load_replay(sessions=sessions,
#                              PBEs_settings_name=PBEs_settings_name,
#                              PBEs_area=PBEs_area,
#                              data_version=data_version,
#                              dec_settings_name=dec_settings_name,
#                              dec_area=dec_area,
#                              min_pbe_duration_in_ms=min_pbe_duration_in_ms,
#                              shuffle_types=shuffle_types,
#                              shuffle_p_vals=shuffle_p_vals,
#                              group_iti_and_return=group_iti_and_return,
#                              return_not_sig=True,
#                              max_average_speed=SPEED_THRESHOLD,
#                              NEWSPEED=True,
#                              correct_incorrect=True,
#                              exclude_run_phase=True)
# print(df.shape[0])
#
# df.to_csv(os.path.join(REPLAY_FOLDER, 'df_final_selection.csv'))

df = pd.read_csv(os.path.join(REPLAY_FOLDER, 'df_final_selection.csv'))

start = df['dec_start_bin'] # THIS IS DIFFERENT
end = df['dec_end_bin'] # THIS IS DIFFERENT

# ma_start = df['maze_arm_dec_start']
# ma_end = df['maze_arm_dec_end']
# df['local_remote'] = ['local' if a==b and a==c else 'remote' for a, b, c in
#                        zip(ma_occ, ma_start, ma_end)]

rew_loc = [15, 16, 17, 18, 19, -15, -16, -17, -18, -19]
df['traj_start_reward'] = ['yes' if np.isin(s, rew_loc) else 'no' for s in start]
df['traj_end_reward'] = ['yes' if np.isin(e, rew_loc) else 'no' for e in end]
df['traj_start_end_reward'] = ['yes' if a=='yes' or b=='yes' else 'no' for a, b in
                               zip(df['traj_start_reward'], df['traj_end_reward'])]




# --- AVERAGE COMPRESSION FACTOR -----------------------------------------------

av_cf = df[(df['epoch'] == 'task')]['compression_factor'].mean()
print('Average compression factor: {}'.format(av_cf))


compression_factor = df[(df['epoch'] == 'task')]['compression_factor']
median = compression_factor.quantile(0.5)
q1 = compression_factor.quantile(0.25)
q2 = compression_factor.quantile(0.75)
print('{:.1f} [{:.1f}, {:.1f}]'.format(median, q1, q2))


# --- OVERLAP WITH RIPPLES -----------------------------------------------------

swr_overlap = df['has_ripple'].sum() / df.shape[0]
print('OVERLAP WITH RIPPLES: {}'.format(100*np.round(swr_overlap, 2)))

# --- LOAD BIN CENTERS ---------------------------------------------------------
sess_ind = 10
F = loadmat(DATA_FOLDER + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]
bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, session)
bin_centers, bin_labels = prepare_bins(bin_centers_combined)


# --- EVENTS PER ANIMAL TABLE --------------------------------------------------


print(df['animal'].value_counts())
print(df.groupby(['sess_ind'])['animal'].value_counts())

print(df.groupby(['animal'])['sess_ind'].unique())

# --- EVENTS PER PHASE ---------------------------------------------------------


f, ax = plt.subplots(1, 1, figsize=[2.5, 4])
sns.countplot(data=df, x='phase', order=phases, color='white', edgecolor='k')

ax.set_ylabel('# of replay events')
ax.set_xlabel('')
ax.set_xticklabels([task_phase_labels_nobreak[l._text] for l in ax.get_xticklabels()],
    rotation=30)
sns.despine()
plt.tight_layout()

plot_name = 'number_of_events_per_phase.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- STATISTICS ---------------------------------------------------------------

n_sig_events = df.shape[0]
n_not_sig_events = df_not_sig.shape[0]

n_totale_events = n_sig_events + n_not_sig_events

print('{} events detected, of which {} ({}%) were significant'.format(
    n_totale_events, n_sig_events, np.round(100*n_sig_events/n_totale_events, 2)))

df['phase'].value_counts()


# --- MAKE LEGENDS ------------------------------------------------------------


colors = [replay_direction_palette['positive'],
          replay_direction_palette['negative']]
texts = [replay_direction_labels['positive'], replay_direction_labels['negative']]
patches = [ plt.plot([],[], marker="o", ms=10, ls="", mec=None, color=colors[i],
            label="{:s}".format(texts[i]) )[0]  for i in range(len(texts)) ]
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=patches, frameon=False)
ax.axis('off')
plt.tight_layout()
plot_name = 'replay_direction_legend_1.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


colors = [replay_direction_palette['positive'],
          replay_direction_palette['negative']]
texts = [replay_direction_labels['positive'], replay_direction_labels['negative']]
patches = [ plt.plot([],[], marker="", ms=10, ls="-", mec=None, color=colors[i],
            label="{:s}".format(texts[i]) )[0]  for i in range(len(texts)) ]
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=patches, frameon=False)
ax.axis('off')
plt.tight_layout()
plot_name = 'replay_direction_legend_1.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



colors = [has_ripple_palette['True'],
          has_ripple_palette['False']]
texts = ['SWR-associated replay events', 'No-SWR replay events']
patches = [ plt.plot([],[], marker="", ms=10, ls="-", mec=None, color=colors[i],
            label="{:s}".format(texts[i]) )[0]  for i in range(len(texts)) ]
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=patches, frameon=False)
ax.axis('off')
plt.tight_layout()
plot_name = 'replay_ripple_legend_1.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# time shift for perirhinal
colors = [area_palette['Perirhinal']]
texts = ["All replay events"]
patches = [ plt.plot([],[], marker="o", ms=10, ls="", mec=None, color=colors[i],
            label="{:s}".format(texts[i]) )[0]  for i in range(len(texts)) ]
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=patches, frameon=False)
ax.axis('off')
plt.tight_layout()
plot_name = 'global_time_shift_legend_1.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# time shift for perirhinal
colors = [area_palette['Perirhinal']]
texts = ["All replay events"]
patches = [ plt.plot([],[], marker="", ms=10, ls="-", mec=None, color=colors[i],
            label="{:s}".format(texts[i]) )[0]  for i in range(len(texts)) ]
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=patches, frameon=False)
ax.axis('off')
plt.tight_layout()
plot_name = 'global_time_shift_legend_1.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




pos_patch = mpatches.Patch(color=replay_direction_palette['positive'],
                           label='{} replay'.format(replay_direction_labels['positive']))
neg_patch = mpatches.Patch(color=replay_direction_palette['negative'],
                           label='{} replay'.format(replay_direction_labels['negative']))
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=[pos_patch, neg_patch], frameon=False)
ax.axis('off')
plt.tight_layout()

plot_name = 'replay_direction_legend_3.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



pos_patch = mpatches.Patch(edgecolor=replay_direction_palette['positive'],
                           facecolor='white', linewidth=2.5,
                           label=replay_direction_labels['positive'])
neg_patch = mpatches.Patch(edgecolor=replay_direction_palette['negative'],
                           facecolor='white', linewidth=2.5,
                           label=replay_direction_labels['negative'])
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=[pos_patch, neg_patch], frameon=False)
ax.axis('off')
plt.tight_layout()

plot_name = 'replay_direction_legend_4.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


colors = [beginning_end_palette['dec_start_bin'],
          beginning_end_palette['dec_end_bin']]
texts = ["Replay trajectory start", "Replay trajectory end"]
patches = [ plt.plot([],[], marker="o", ms=10, ls="", mec=None, color=colors[i],
            label="{:s}".format(texts[i]) )[0]  for i in range(len(texts)) ]

colors = [animal_loc_color]
texts = ["Animal location during replay"]
patches = patches + [plt.plot([],[], marker="o", ms=20, ls="",
                     markeredgecolor="w", color=colors[i], alpha=0.2,
                     label="{:s}".format(texts[i]) )[0]  for i in range(len(texts)) ]

f, ax = plt.subplots(figsize=[8, 1])
ax.legend(handles=patches, frameon=False, mode = "expand", ncol = 3)
ax.axis('off')
plt.tight_layout()
plot_name = 'start_end_legend_1.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



pos_patch = mpatches.Patch(color=beginning_end_palette['dec_start_bin'],
                           label='Trajectory start')
neg_patch = mpatches.Patch(color=beginning_end_palette['dec_end_bin'],
                           label='Trajectory end')
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=[pos_patch, neg_patch], frameon=False)
ax.axis('off')
plt.tight_layout()

plot_name = 'start_end_legend_2.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- CHI SQUARED TEST FOR SPATIAL DISTRIBUTION --------------------------------

bin_counts_per_phase = {}
for task_phase in phases:


    df_sel = df[(df['epoch'] == 'task') & (df['phase'] == task_phase)]
    #print(df_sel.shape[0])

    #bins = np.hstack(df_sel['trajectory'])
    bins = df_sel['dec_start_bin']
    # TODO CHECK THIS, replacement or not?
    #bins = np.random.choice(bins, 400, replace=True) #downsampling should be required
    # but we try it anyways
    bin_freq = bin_occurrences_for_chi_square_test(bins)
    bin_counts_per_phase[task_phase] = bin_freq


import scipy.stats
import itertools
for phase1, phase2 in list(itertools.combinations(phases,2)):

    rs = scipy.stats.chisquare(f_obs=bin_counts_per_phase[phase1],
                               f_exp=bin_counts_per_phase[phase2])
    print('Difference in spatial distribution between {} and {}: p={:.2e}'.format(
        phase1, phase2, rs[1]))

x = np.arange(8)
y = bin_counts_per_phase['img']
y2 = bin_counts_per_phase['reward']

f, ax = plt.subplots(1, 1)
plt.bar(x, y)
plt.bar(x+0.2, y2)



# --- START AND END OF TRAJ. FOR DIFFERENT PHASES -----------------------------


for task_phase in phases:

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

    for event in ['dec_start_bin', 'dec_end_bin']:

        df_sel = df[(df['epoch'] == 'task') & (df['phase'] == task_phase)
                    & (df['replay_direction'] == 'positive')]

        bins = merge_duplicate_left_right_bins(np.array(df_sel[event]))
        bin_counts = pd.value_counts(bins, normalize=True)

        for bin, counts in bin_counts.iteritems():
            #print(bin, counts)
            x, y = get_position_bin(bin, bin_centers, bin_labels)
            if event == 'dec_start_bin':
                x = x - 5
            elif event == 'dec_end_bin':
                x = x + 5
            ax.scatter(x, y, s=counts * dot_scaling, c=[beginning_end_palette[event]],
                       zorder=10)
    plot_maze(ax, c=maze_color)
    mark_locations(ax, color=maze_color)

    for loc in circle_locations[task_phase]:
        x, y = get_position_bin(loc, bin_centers, bin_labels)
        circle = plt.Circle((x, y), circle_size[task_phase], edgecolor='w',
                            facecolor=animal_loc_color, linewidth=0,
                            zorder=10, alpha=0.2)
        ax.add_artist(circle)
    plt.tight_layout()
    ax.axis('off')
        #ax.set_title(task_phase)

    plot_name = 'start_and_end_traj_{}.{}'.format(task_phase, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- DISTANCE BETWEEN BIN OF PBE AND START DEC BIN ---------------------------

df_sel = df[df['epoch'] == 'task']

dist_dict = {}
for xlab, ylab, lab in [('x_pos_dec_start', 'y_pos_dec_start', 'Beginning'),
                   ('x_pos_dec_end', 'y_pos_dec_end', 'End')]:
    pbe_loc = np.vstack((df_sel['x_pos'].values, df_sel['y_pos'])).T
    dec_start = np.vstack((df_sel[xlab].values, df_sel[ylab])).T

    #dec_start = dec_start[np.random.permutation(np.arange(dec_start.shape[0]))]

    distance = np.linalg.norm((pbe_loc-dec_start), axis=1)
    dist_dict[lab] = distance

t, p = wilcoxon(dist_dict['Beginning'], dist_dict['End'])
print('distance of pbe location to beginning/end of trajectory is different'
      ' p={:.3}'.format(p))




# --- PLOT EXAMPLE TRAJECTORIES ON THE MAZE ------------------------------------
trajectory_cmap = plt.cm.winter_r


# PLOT TRAJECTORY LEGEND LINE
f, ax = plt.subplots(1, 1, figsize=[4.5, 1])
x = np.arange(0, 100)
y = np.repeat(0, x.shape[0])

N = y.shape[0]
for i in range(N) :
    colors = trajectory_cmap(int(255 * i / N))
    ax.plot(x[i:i+2], y[i:i+2], color=colors, lw=3)

c_start = trajectory_cmap(int(255 * 0 / N))
c_end = trajectory_cmap(int((255 * N - 1) / N))
ax.scatter(x[0], y[0], c=c_start, zorder=10, s=20)
ax.scatter(x[-1], y[-1], c=c_end, zorder=10, s=20)
ax.text(x[0]-30, y[0]+0.05, 'Replay trajectory start')
ax.text(x[-1]-30, y[0]+0.05, 'Replay trajectory end')
plt.tight_layout()
ax.axis('off')
plot_name = 'trajectories_example_line.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
plt.close(fig=f)

trajectory_cmap = plt.cm.winter_r
#df_sel = df[(df['epoch'] == 'task') & (df['replay_direction'] == 'positive')]

df_sel_1 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'iti') & (df['replay_direction'] == 'positive')]

df_sel_2 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'img') & (df['replay_direction'] == 'positive')]

df_sel_3 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'run') & (df['replay_direction'] == 'positive')]

df_sel_4 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'reward') & (df['replay_direction'] == 'positive')]


df_sel_5 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'iti')]

df_sel_6 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'img')]

df_sel_7 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'run')]

df_sel_8 = df[(df['epoch'] == 'task')
            & (df['phase'] == 'reward')]


dfs = [df_sel_1, df_sel_2, df_sel_3, df_sel_4,
       df_sel_5, df_sel_6, df_sel_7, df_sel_8]
labels = ['forward_during_iti', 'forward_during_img',
          'forward_during_run', 'forward_during_reward',
          'during_iti', 'during_img',
          'during_run', 'during_reward']

n_traj = 8
offsets = np.linspace(-4, 4, n_traj)
for df_sel, label in zip(dfs, labels):
    trajectories=  np.random.choice(df_sel['trajectory'].values, min(n_traj, df_sel.shape[0]),
                                    replace=False)

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

    for i, trajectory in enumerate(trajectories):
        xy_pos = np.vstack([get_position_bin(bin, bin_centers, bin_labels) for bin in trajectory])

        # xy_pos[1:-1, 0] += np.random.normal(loc=0, scale=3)
        # xy_pos[1:-1, 1] += np.random.normal(loc=0, scale=2)
        # xy_pos[:, 0] += np.random.uniform(low=-5, high=5)
        # xy_pos[:, 1] += np.random.uniform(low=-5, high=5)

        xy_pos[:, 0] += offsets[i]
        xy_pos[:, 1] += offsets[i]

        xy_pos[:, 0] = gaussian_filter1d(xy_pos[:, 0], sigma=1)
        xy_pos[:, 1] = gaussian_filter1d(xy_pos[:, 1], sigma=1)

        N = xy_pos.shape[0]-1
        for i in range(N):
            colors = trajectory_cmap(int(255 * i / N))
            ax.plot(xy_pos[i:i+2, 0], xy_pos[i:i+2, 1],color=colors)
        #ax.plot(xy_pos[:, 0], xy_pos[:, 1], c='blue', alpha=0.6)
        c_start = trajectory_cmap(int(255 * 0 / N))
        c_end = trajectory_cmap(int((255 * N - 1) / N))
        ax.scatter(*xy_pos[0, :], c=c_start, zorder=10, s=12)
        ax.scatter(*xy_pos[-1, :], c=c_end, zorder=10, s=12)

    plot_maze(ax, c=maze_color)
    mark_locations(ax, color=maze_color)
    ax.axis('off')
    plt.tight_layout()

    plot_name = 'trajectories_{}.{}'.format(label, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
    plt.close(fig=f)




# --- REPLAY RATE -------------------------------------------------------------

# TODO was using speed threshold 12
max_speed_rate = 12

rs = pd.DataFrame(columns=['sess_ind', 'phase', 'replay_rate'])

for sess_ind in df['sess_ind'].unique():

    timespent = get_time_spent_per_task_phase(sess_ind,
                                              data_version=data_version,
                                              speed_threshold=max_speed_rate)
    df_sel = df[(df['epoch'] == 'task') & (df['sess_ind'] == sess_ind)]
    #df_sel = df_sel[df_sel['animal'] == 'Ramachandran']

    for phase in ['all'] + phases:

        if phase == 'all':
            nreplay = df_sel.shape[0]
        else:
            nreplay = df_sel[df_sel['phase'] == phase].shape[0]

        replayrate = nreplay / timespent[phase].rescale(pq.min)

        rs.loc[rs.shape[0], :] = [sess_ind, phase, replayrate]

rs['replay_rate'] = pd.to_numeric([i.item() for i in rs['replay_rate']])

f, ax = plt.subplots(1, 1, figsize=slightly_vertical_panel_small)
sns.barplot(x='phase', y='replay_rate', data=rs,
            edgecolor=sns.xkcd_rgb['dark grey'], facecolor='white', errcolor='0.2',
            linewidth=1.5, order=['all']+phases, )
ax.set_ylabel('CA1 replay events per minute')
ax.set_xlabel('')
ax.set_xticklabels([task_phase_labels[l._text] for l in ax.get_xticklabels()],
                   rotation=45)
# plt.setp(ax.artists, edgecolor=sns.xkcd_rgb['dark grey'], facecolor='w')
# plt.setp(ax.lines, color=sns.xkcd_rgb['dark grey'])
sns.despine()
plt.tight_layout()

plot_name = 'replay_rate_per_task_phase_maxspeed_{}.{}'.format(max_speed_rate, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

plt.close(fig=f)


# --- REPLAY RATE STATISTICS ---------------------------------------------------

# combos = [('iti', 'img'),
#           ('iti', 'reward'),
#           ('iti', 'run'),
#           ('run', 'reward'),
#           ('run', 'img'),
#           ('reward', 'img')]

combos = [('iti', 'img'),
          ('iti', 'reward'),
          ('reward', 'img')]

for phase1, phase2 in combos:

    df1 = rs[(rs['phase'] == phase1)]
    df2 = rs[(rs['phase'] == phase2)]
    np.testing.assert_array_equal(df1['sess_ind'], df2['sess_ind'])
    #print(df1[col].hasnans, df2[col].hasnans)
    x = df1['replay_rate'].values
    y = df2['replay_rate'].values
    #print(np.median(x), np.median(y))
    st,pval = wilcoxon(x, y)
    adjusted_p = pval * len(combos)

    st, pval = mannwhitneyu(x, y)
    adjusted_p = pval * len(combos)

    # print('{}, {} vs {} medians: {:.2f} vs {:.2f}: p_val = {:.1g}, sig: {}'.format('replay_rate',
    #       phase1, phase2, np.median(x), np.median(y), adjusted_p, adjusted_p<0.05))

    print('{}, {} vs {} averages: {:.2f} vs {:.2f}: p_val = {:.1g}, sig: {}'.format('replay_rate',
          phase1, phase2, np.mean(x), np.mean(y), adjusted_p, adjusted_p<0.05))
    # df1 = rs[(rs['phase'] == 'img')]
    # df2 = rs[(rs['phase'] == 'run')]
    # df3 = rs[(rs['phase'] == 'iti')]





# --- REPLAY RATE CORRECT INCORRECT --------------------------------------------

for trial_outcome_col in ['current_trial_outcome']:

    max_speed_rate = 10
    rs = pd.DataFrame(columns=['sess_ind', 'phase', 'outcome', 'replay_rate'])

    for sess_ind in df['sess_ind'].unique():

        timespent = get_time_spent_per_task_phase_outcome(sess_ind,
                                                  data_version=data_version,
                                                  speed_threshold=max_speed_rate)
        df_sel = df[(df['epoch'] == 'task')
                    & (df['sess_ind'] == sess_ind)]

        for phase in phases:
            for outc in ['correct', 'incorrect']:
                if phase == 'all':
                    nreplay = df_sel[df_sel[trial_outcome_col] == outc].shape[0]
                else:
                    nreplay = df_sel[(df_sel['phase'] == phase) &
                                     (df_sel[trial_outcome_col] == outc)].shape[0]

                replayrate = nreplay / timespent[outc][phase].rescale(pq.min)

                rs.loc[rs.shape[0], :] = [sess_ind, phase, outc, replayrate]

    rs['replay_rate'] = pd.to_numeric([i.item() for i in rs['replay_rate']])

    rs = rs[rs['replay_rate'] < 40]

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
    sns.barplot(x='phase', y='replay_rate', data=rs,
                edgecolor=sns.xkcd_rgb['dark grey'], facecolor='white', errcolor='0.2',
                linewidth=1.5, order=phases, hue='outcome', hue_order=['correct', 'incorrect'])
    sns.swarmplot(x='phase', y='replay_rate', data=rs,
                edgecolor=sns.xkcd_rgb['dark grey'],
                  order=phases, hue='outcome', hue_order=['correct', 'incorrect'],
                  palette=outcome_palette, dodge=True)


    ax.set_ylabel('CA1 replay events per minute')
    ax.set_xlabel('')
    ax.set_xticklabels([task_phase_labels[l._text] for l in ax.get_xticklabels()],
                       rotation=30)

    for i in [0, 1, 2]:
        plt.setp(ax.patches[i], edgecolor=outcome_palette['correct'], facecolor='w',
                 linewidth=3)
    for i in [3, 4, 5]:
        plt.setp(ax.patches[i], edgecolor=outcome_palette['incorrect'],facecolor='w',
                 linewidth=3)

    ax.legend().remove()
    sns.despine()
    plt.tight_layout()

    plot_name = 'replay_rate_per_task_phase_split_corr_{}_maxspeed_{}.{}'.format(trial_outcome_col, max_speed_rate, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

    #plt.close(fig=f)


    # --- STATISTICS ---------------------------------------------------------------

    for phase in phases:

        df1 = rs[(rs['phase'] == phase) & (rs['outcome'] == 'correct')]
        df2 = rs[(rs['phase'] == phase) & (rs['outcome'] == 'incorrect')]

        ind = ~np.logical_or(np.isnan(df1['replay_rate']).values, np.isnan(df2['replay_rate']).values)
        df1 = df1[ind]
        df2 = df2[ind]

        np.testing.assert_array_equal(df1['sess_ind'], df2['sess_ind'])
        #print(df1[col].hasnans, df2[col].hasnans)
        x = df1['replay_rate'].values
        y = df2['replay_rate'].values
        #print(np.median(x), np.median(y))
        st,pval = wilcoxon(x, y)
        st, pval = mannwhitneyu(x, y)

        adjusted_p = pval * len(phases)
        print('{}, {}, {} vs {} means: {:.2f} vs {:.2f}: p_val = {:.1g}, sig: {}'.format('replay_rate',
              phase, 'correct', 'incorrect', np.mean(x), np.mean(y), adjusted_p, adjusted_p<0.05))
        # df1 = rs[(rs['phase'] == 'img')]
        # df2 = rs[(rs['phase'] == 'run')]
        # df3 = rs[(rs['phase'] == 'iti')]








# --- REPLAY METRICS -----------------------------------------------------------


cols = ['replay_score', 'event_duration_in_ms',
        'dist_traversed_cm', 'compression_factor', 'max_jump_dist',
        'mean_jump_dist', 'sharpness', 'n_active']

combos = list(itertools.combinations(phases, 2))

for col in cols:
    print('\n')

    for phase1, phase2 in combos:
        df1 = df[(df['epoch'] == 'task') & (df['phase'] == phase1)]
        df2 = df[(df['epoch'] == 'task') & (df['phase'] == phase2)]

        #print(df1[col].hasnans, df2[col].hasnans)
        x = df1[col].values
        y = df2[col].values
        #print(np.median(x), np.median(y))
        st,pval = mannwhitneyu(x, y)
        adjusted_p = pval * len(combos)
        print('{}, {} vs {} medians: {:.1f} vs {:.1f}: {}, p_val = {:.1g}, sig: {}'.format(col,
              phase1, phase2, np.median(x), np.median(y), st, adjusted_p, adjusted_p<0.05))


df_sel = df[df['epoch'] == 'task']

cols = ['replay_score', 'event_duration_in_ms',
        'dist_traversed_cm', 'compression_factor', 'max_jump_dist',
        'mean_jump_dist', 'sharpness', 'n_active']

for col in cols:

    f, ax = plt.subplots(1, 1, figsize=[2.5, 4])
    sns.boxplot(data=df_sel, x='phase', y=col, color='white',
                fliersize=2, order=phases)

    sns.swarmplot(data=df_sel, x='phase', y=col, color=sns.xkcd_rgb['dark grey'],
                 order=phases, s=2)
    plt.setp(ax.artists, edgecolor=sns.xkcd_rgb['dark grey'], facecolor='w')
    plt.setp(ax.lines, color=sns.xkcd_rgb['dark grey'])

    if col == 'compression_factor':
        ax.set_yscale("log")
        ax.set_ylim(1, 100)

    #sns.swarmplot(data=df_sel, x='phase', y=col, color=sns.xkcd_rgb['dark grey'])

    ax.set_ylabel(replay_metric_labels[col])
    ax.set_xlabel('')
    ax.set_xticklabels([task_phase_labels_nobreak[l._text] for l in ax.get_xticklabels()],
                       rotation=30)
    sns.despine()
    plt.tight_layout()

    plot_name = 'replay_{}_per_task_phase.{}'.format(col, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

    #plt.close(fig=f)


# --- PLOT JUST THE EMPTY MAZE -------------------------------------------------

f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)

plot_maze(ax, c=maze_color)
mark_locations(ax, color=maze_color)
# ax.set_title('{}-{}'.format(phase1, phase2))
plt.tight_layout()
ax.axis('off')
plot_name = 'just_the_maze.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


f, ax = plt.subplots(1, 1, figsize=[2, 2])

plot_maze(ax, c=maze_color)
mark_locations(ax, color=maze_color)
# ax.set_title('{}-{}'.format(phase1, phase2))
plt.tight_layout()
ax.axis('off')
plot_name = 'just_the_small_maze.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
