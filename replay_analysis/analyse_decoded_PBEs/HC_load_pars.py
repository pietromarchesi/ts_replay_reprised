import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from utils import load_bin_centers
from utils import prepare_bins
from utils import plot_maze, mark_locations
from utils import get_position_bin
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from utils import is_on_central_arm, merge_duplicate_left_right_bins
from utils import add_ripples_to_pbes_df
from session_selection import sessions_HC_replay
from utils import load_decoded_PBEs
from utils import get_time_spent_per_task_phase
import quantities as pq
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
from scipy.ndimage import gaussian_filter1d
import itertools
from utils import *



data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

ratemap_settings   = 'dec6'


#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]


# --- LOAD DATA ---------------------------------------------------------------


pbes = load_decoded_PBEs(sessions[0], PBEs_settings_name, PBEs_area,
                         data_version, dec_settings_name, dec_area,
                         load_light=True)
decode_pars = pbes['decode_pars']
print(decode_pars)



pbes = load_PBEs(sessions[0], PBEs_settings_name, PBEs_area, data_version=data_version)
detection_pars = pbes['detection_pars']
print(detection_pars)


# --- LOAD RATEMAPS DATA ------------------------------------------------------------

dfs = []
ratemap_area = 'Perirhinal'
res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)
file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
df = res['df']
pars = res['pars']
shift_amounts = pars['shift_amounts']
smoothing_sigma = pars['smooth_sigma']
