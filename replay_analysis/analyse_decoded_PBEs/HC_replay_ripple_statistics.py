import os
import pickle
from loadmat import loadmat
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from utils import load_bin_centers
from utils import prepare_bins
from utils import plot_maze, mark_locations
from utils import get_position_bin
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from utils import is_on_central_arm, merge_duplicate_left_right_bins
from session_selection import sessions_HC_replay
from utils import load_decoded_PBEs
from utils import get_time_spent_per_task_phase
from utils import load_replay
import quantities as pq
import matplotlib.patches as mpatches
from scipy.ndimage import gaussian_filter1d
import itertools
from utils import bin_occurrences_for_chi_square_test, linearize_trajectory

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]
epochs             = ['task']

group_iti_and_return = True

# PLOTTING PARAMETERS

plot_settings = 'only_two_shufs'
save_plots = False
plot_format = 'png'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms    = 50

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
shuffle_p_vals = [0.05, 0.05]

# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'hippocampal_replay',
                             'pbe_setting_{}_{}_{}_dec_setting_{}_{}_plot_setting_{}'.format(PBEs_settings_name, PBEs_area,
                              data_version, dec_settings_name, dec_area, plot_settings))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------


# df = load_replay(sessions=sessions,
#                  PBEs_settings_name=PBEs_settings_name,
#                  PBEs_area=PBEs_area,
#                  data_version=data_version,
#                  dec_settings_name=dec_settings_name,
#                  dec_area=dec_area,
#                  min_pbe_duration_in_ms=min_pbe_duration_in_ms,
#                  shuffle_types=shuffle_types,
#                  shuffle_p_vals=shuffle_p_vals,
#                  group_iti_and_return=group_iti_and_return)

df = pd.read_csv(os.path.join(REPLAY_FOLDER, 'df_final_selection.csv'))

# --- LOAD BIN CENTERS --------------------------------------------------------
sess_ind = 10
F = loadmat(DATA_FOLDER + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]
bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, session)
bin_centers, bin_labels = prepare_bins(bin_centers_combined)

# --- STATS --------------------------------------------------------------------

# perc_rip_sig = 100 * df['has_ripple'].sum() / df.shape[0]
# perc_rip_not_sig = 100 * df_not_sig['has_ripple'].sum() / df_not_sig.shape[0]
#
# print('Percentage of significant events with ripple: {}'.format(perc_rip_sig))
# print('Percentage of non-significant events with ripple: {}'.format(perc_rip_not_sig))



# --- PERCENTAGE OF RIPPLE/NO RIPPLE AVERAGED ACROSS SESSIONS -----------------
df_sel = df[df['epoch'] == 'task']

pos, neg = [], []
for sess_ind in df_sel['sess_ind'].unique():

    dfx = df_sel[df_sel['sess_ind'] == sess_ind]

    npos = dfx[dfx['has_ripple']].shape[0]
    nneg = dfx[~dfx['has_ripple']].shape[0]

    perc_pos = npos / (npos+nneg)
    perc_neg = nneg / (npos+nneg)
    pos.append(perc_pos)
    neg.append(perc_neg)

dfy = pd.DataFrame(columns=['Ripple events', 'percentage'])
dfy['replay_direction'] = ['positive'] * len(pos) + ['negative'] * len(neg)
dfy['percentage'] = pos + neg

stat, p_val = wilcoxon(pos, neg)
print('Difference between % of forward and backward events: p={}'.format(p_val))

f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
sns.barplot(data=dfy, x='replay_direction', y='percentage')
ax.set_xticklabels(['Ripple\npresent', 'No\nripple'])
ax.set_ylabel('Percentage')
ax.set_xlabel('')
#ax.legend(frameon=False, loc='upper right')
sns.despine()
plt.tight_layout()



# --- RIPPLES ----------------------------------------------------------------
df_sel = df[df['epoch'] == 'task']

phases = ['iti', 'img', 'run', 'reward']
dg = pd.DataFrame(columns=['sess_ind', 'phase', 'direction', 'perc_ripple'])
for sess_ind in df_sel['sess_ind'].unique():
    for phase in phases:
        for replay_direction in ['positive', 'negative']:
            dfx = df_sel[(df_sel['sess_ind'] == sess_ind) &
                         (df_sel['phase'] == phase) &
                         (df_sel['replay_direction'] == replay_direction)]

            if dfx.shape[0] >= 8:
                nrip = dfx[dfx['has_ripple']].shape[0]
                nnorip = dfx[~dfx['has_ripple']].shape[0]

                perc_rip = 100 * nrip / (nrip+nnorip)

                dg.loc[dg.shape[0], :] = [sess_ind, phase,replay_direction, perc_rip]


f, ax = plt.subplots(1, 1, figsize=[3, 4])
sns.barplot(data=dg, x='phase', y='perc_ripple', hue='direction',
            order=phases,
            palette=replay_direction_palette,
            linewidth=3.5, errcolor='0.2')
ax.set_ylabel('% of replay events\nco-occurring with SWRs')
ax.set_xticklabels([task_phase_labels_nobreak[l._text] for l in ax.get_xticklabels()],
                   rotation=45)
ax.legend()
ax.get_legend().remove()
ax.set_xlabel('')
plt.tight_layout()
sns.despine()
plot_name = 'perc_ripples_across_phases.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


pos_patch = mpatches.Patch(edgecolor=replay_direction_palette['positive'],
                           facecolor=replay_direction_palette['positive'],
                           label='Task-direction replay')
neg_patch = mpatches.Patch(edgecolor=replay_direction_palette['negative'],
                           facecolor=replay_direction_palette['negative'],
                           label='Opposite-direction replay')
f, ax = plt.subplots(1, 1, figsize=[3, 1])
ax.legend(handles=[pos_patch, neg_patch], frameon=False)
ax.axis('off')
plt.tight_layout()

plot_name = 'perc_ripples_across_phases_legend.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- PBE LOCATION, START AND END OF TRAJ. FOR EVENTS W/WO RIPPLES -------------

for event in ['pbe_loc_bin', 'dec_start_bin', 'dec_end_bin']:

    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
    plot_maze(ax, c=maze_color)
    mark_locations(ax, color=maze_color)

    for has_ripple in [True, False]:

        df_sel = df[(df['epoch'] == 'task') & (df['has_ripple'] == has_ripple)]
        bins = merge_duplicate_left_right_bins(np.array(df_sel[event]))
        bin_counts = pd.value_counts(bins, normalize=True)

        for bin, counts in bin_counts.iteritems():
            #print(bin, counts)
            x, y = get_position_bin(bin, bin_centers, bin_labels)
            if has_ripple:
                x = x - 4
            else:
                x = x + 3
            ax.scatter(x, y, s=counts * dot_scaling,
                       c=has_ripple_palette[has_ripple],
                       zorder=10)
        ax.axis('off')
        #plt.tight_layout()
        #ax.set_title(event)
        plot_name = 'difference_in_representation_of_{}_ripple_vs_no_ripple.{}'.format(event,
                                                                       plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- STATISTICAL COMPARISON OF EVENTS W AND WO RIPPLES -----------------------

statistics = ['replay_score', 'dist_traversed_cm', 'percentage_active',
              'sharpness', 'event_duration_in_ms', 'max_jump_dist',
              'compression_factor']

# cols = ['replay_score',
#         'dist_traversed_cm',
#         'mean_jump_dist', 'sharpness', 'n_active']

for statistic in statistics:
    df_pos = df[(df['epoch'] == 'task') & (df['has_ripple'])]
    df_neg = df[(df['epoch'] == 'task') & (~df['has_ripple'])]

    x = df_pos[statistic].values
    y = df_neg[statistic].values
    st,pval = mannwhitneyu(x, y)
    sig = pval < 0.05 / len(statistic)
    print('{}, medians: {:.2f} vs {:.2f}: {}, p_val = {:.3e}, significant: {}'.format(statistic,
          np.median(x), np.median(y), st, pval, sig))


    f, ax = plt.subplots(1, 1, figsize=[2, 4])
    sns.boxplot(data=df[df['epoch'] == 'task'], y=statistic,color='white',
                fliersize=2,
                x='has_ripple', order=[True, False])

    plt.setp(ax.artists, edgecolor=sns.xkcd_rgb['dark grey'], facecolor='w')
    plt.setp(ax.lines, color=sns.xkcd_rgb['dark grey'])
    ax.set_ylabel(replay_metric_labels[statistic])
    ax.set_xticklabels(['SWR\npresent', 'No SWR'], rotation=45)
    ax.set_xlabel('')
    sns.despine()
    plt.tight_layout()

    plot_name = 'ripple_{}_boxplot.{}'.format(statistic, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

# counts = df[df['has_ripple']]['replay_direction'].value_counts()
# counts2 = df[~df['has_ripple']]['replay_direction'].value_counts()
#
# # see how events w/wo ripples are distributed across task phases
# for col in ['phase']:
#     group1 = df.groupby(['has_ripple', col]).agg({col:'count'})
#     group2 = df.groupby(['has_ripple']).agg({col:'count'})
#     perc_col_per_set = group1.div(group2, level='has_ripple') * 100
#     print(perc_col_per_set)