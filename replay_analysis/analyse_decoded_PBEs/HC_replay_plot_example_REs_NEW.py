import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from session_selection import sessions_HC_replay
from utils import load_decoded_PBEs

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]

sessions           = [0,1,2,3,4,6,7,8,9,10,11,28,29,30]

epochs             = ['task']


# FILTER PBES PARAMETERS
phase = None  # 'iti', 'img', 'run', 'return' (or list)
epoch = None # 'task', 'pre_sleep', 'post_sleep' (or list)

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
p_vals = [0.05, 0.05]
min_pbe_duration_in_ms = 50


# PLOTTING PARAMETERS
n_events = 3
save_plots = True
plot_format = 'svg'
dpi = 400

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'all_final_selection')


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

pbes_dfs, sig_dfs= [], []
rds = {}

for sess_ind in sessions:
    for epoch in epochs:

        pbes = load_decoded_PBEs(sess_ind, PBEs_settings_name, PBEs_area,
                                 data_version, dec_settings_name, dec_area)

        pbes_dfs.append(pbes['pbes'])
        sig_dfs.append(pbes['sig_df'])

        for pbe_id, det in zip(pbes['pbes'].index, pbes['detectors']):
            rds[pbe_id] = det

# df = pd.concat(pbes_dfs)
# sig_df = pd.concat(sig_dfs)
#
# df = df[df['event_duration_in_ms'] >= min_pbe_duration_in_ms]
#
# # --- FILTER PBES -------------------------------------------------------------
#
# sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types,
#                                         p_vals=p_vals)
# df = df[np.isin(df.index, sel_pbes_sig)]

# --- GET GOOD EVENTS ---------------------------------------------------------

# df_sort = df.sort_values(by=['replay_score', 'average_speed'],
#                          ascending=False)


right_events_positive = ['6_task_434',
                         '6_task_1453',
                         '29_task_718']


right_events_negative = ['6_task_1188',
                        '29_task_915',
                         '6_task_945']

left_events_positive = ['7_task_934',
                        '10_task_1299',
                        '2_task_2033']

left_events_negative = ['3_task_680',
                        '6_task_581',
                        '10_task_421']

main_fig_events = np.hstack([right_events_positive,
                             right_events_negative,
                             left_events_positive,
                             left_events_negative])

dict_events = {'right_pos' : right_events_positive,
               'right_neg' : right_events_negative,
               'left_pos' : left_events_positive,
               'left_neg' : left_events_negative}


# ---------

max_spikes_list = []
max_proba_list = []
for ev_type in dict_events.keys():
    ids = dict_events[ev_type]
    ids = ids[0:n_events]
    for row, id in enumerate(ids):
        pbe = rds[id]
        max_spikes_list.append(pbe.spikes.max())
        max_proba_list.append(pbe.y_pred_proba.max())

vmax_spikes = np.max(max_spikes_list)
vmax_proba = np.round(np.max(max_proba_list), 1)
#vmax_spikes = 5
#vmax_prob = 0.5
vmin_spikes = 0
vmin_proba = 0

fontsize = 13
# for ev_type in dict_events.keys():
#     ids = dict_events[ev_type]
#     ids = ids[0:n_events]
#     figsize=[tiny_panel_side*2, 1.6*n_events]


selected = ['28_task_71',
            '7_task_1289', # no line
            '9_task_546',
            '7_task_850',
            '6_task_707',
            '10_task_632',
            '11_task_714',
            '6_task_1527',
            '29_task_515',
            '7_task_1258',
            '6_task_821',
            '6_task_434',  # two lines
            '7_task_817',
            '29_task_915',
            '2_task_2033',
            '7_task_984',
            '10_task_1360',
            '10_task_421',
            '29_task_718',
            '30_task_963',
            '6_task_945',
            '10_task_632',
            '7_task_934',
            '6_task_707',
            '9_task_1974',
            '6_task_1016',
            '10_task_1062',
            '9_task_1020',
            '29_task_732',
            '6_task_636',
            '6_task_1124',
            '9_task_933',
            '29_task_571',
            '9_task_1465',
            '6_task_382',
            '6_task_1564',
            '7_task_739',
            '29_task_894',
            '3_task_1168',
            '29_task_913',
            '11_task_865',
            '4_task_56',
            '10_task_1003',
            '10_task_933',
            '7_task_718',
            '10_task_344',
            '2_task_1388',
            '7_task_944',
            '6_task_1408',
            '9_task_2016',
            '29_task_442',
            '9_task_890',
            '29_task_869',
            '3_task_760',
            '2_task_1449',
            '10_task_937',
            '9_task_356',
            '11_task_734',
            '11_task_1472',
            '9_task_2015',
            '11_task_1439',
            '6_task_1369',
            '9_task_1790',
            '10_task_955',
            '30_task_1022',
            '6_task_1188',
            '10_task_914',
            '9_task_618',
            '6_task_788',
            '30_task_292',
            '6_task_1235',
            '28_task_297',
            '10_task_1117',
            '9_task_1627',
            '3_task_1601',
            '4_task_235',
            '7_task_468',
            '10_task_842',
            '7_task_925',
            '10_task_1179',
            '9_task_1806',
            '9_task_1819',
            '9_task_1580',
            '9_task_1726',
            '9_task_207',
            '6_task_796',
            '9_task_261',]




df = pd.read_csv(os.path.join(REPLAY_FOLDER, 'df_final_selection.csv'))

start = df['dec_start_bin'] # THIS IS DIFFERENT
end = df['dec_end_bin'] # THIS IS DIFFERENT

# ma_start = df['maze_arm_dec_start']
# ma_end = df['maze_arm_dec_end']
# df['local_remote'] = ['local' if a==b and a==c else 'remote' for a, b, c in
#                        zip(ma_occ, ma_start, ma_end)]

rew_loc = [15, 16, 17, 18, 19, -15, -16, -17, -18, -19]
df['traj_start_reward'] = ['yes' if np.isin(s, rew_loc) else 'no' for s in start]
df['traj_end_reward'] = ['yes' if np.isin(e, rew_loc) else 'no' for e in end]
df['traj_start_end_reward'] = ['yes' if a=='yes' or b=='yes' else 'no' for a, b in
                               zip(df['traj_start_reward'], df['traj_end_reward'])]


#selected_checked = [s for s in selected if np.isin(s, df['Unnamed: 0'])]

selected_checked = df['Unnamed: 0']


for id in ['6_task_318']:

    if np.isin(id, selected_checked):
        checked = True


    replay_direction = df[df['Unnamed: 0'] == id]['replay_direction'].iloc[0]
    pbe_side = df[df['Unnamed: 0'] == id]['pbe_side'].iloc[0]


    line_color = replay_direction_palette[replay_direction]

    ids = [id]

    f, ax = plt.subplots(1, 2, figsize=[4, 2])

    for row, id in enumerate(ids):
        try:

            pbe = rds[id]
        except KeyError:
            print('NOT FOUND')
        pbe.binsize_in_ms = 10

        im = pbe._plot_binned_spikes(ax[0], plot_cbar=False,
                                vmin=vmin_spikes, vmax=vmax_spikes)

        im2 = pbe._plot_decoded_pbe(ax[1], plot_cbar=False,
                              vmin=vmin_proba, vmax=vmax_proba, line_color=line_color)

        if id == '6_task_434' or id == '3_task_680' or '6_task_318':
            y = np.interp(x=pbe.time_bins,
                          xp=[pbe.time_bins[0], pbe.time_bins[-1]],
                          fp=[pbe.start_location+30, pbe.end_location+30])
            ax[1].plot(pbe.time_bins, y, c=line_color, ls=':', lw=3)
            #raise ValueError

        if pbe_side == 'right':
            ax[1].set_yticks([0, reward_bin])
            ax[1].set_yticklabels(['IMG', 'REW'])
            ax[1].yaxis.tick_right()
            for loc in [0, reward_bin] :
                ax[1].axhline(loc, c=maze_color, zorder=10, ls='--')

        elif pbe_side == 'left':
            ax[1].set_yticks([0, -reward_bin])
            ax[1].set_yticklabels(['IMG', 'REW'])
            ax[1].yaxis.tick_right()
            for loc in [0, -reward_bin] :
                ax[1].axhline(loc, c=maze_color, zorder=10, ls='--')

        for ii in [0, 1]:
            ax[ii].set_xlabel('')
            ax[ii].set_ylabel('')

        ax[0].set_yticks([])
    ax[0].set_ylabel('CA1 units', fontdict={'fontsize':fontsize})
    ax[1].set_ylabel('Location', fontdict={'fontsize':fontsize})
    # add a big axis, hide frame
    # f.add_subplot(111, frameon=False)
    # plt.tick_params(labelcolor='none', top=False, bottom=False, left=False,
    #                 right=False)
    # plt.xlabel('Time [ms]')
    f.text(0.5, 0.0, 'Time [ms]', ha='center', fontdict={'fontsize':fontsize})

    #plt.ylabel('CA1 units')

    sns.despine(top=True, right=True, offset=0, trim=False)
    plt.subplots_adjust(wspace=0.22, hspace=0.22)
    plt.tight_layout()

    if checked:
        plot_name = 'pbe_panel_id_{}_checked.{}'.format(id, plot_format)
    else:
        plot_name = 'pbe_panel_id_{}.{}'.format(id, plot_format)

    f.savefig(os.path.join(plot_folder, plot_name), bbox_inches='tight', dpi=dpi)
    plt.close()

