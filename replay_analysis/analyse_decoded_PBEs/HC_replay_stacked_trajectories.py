import os
import pickle
from loadmat import loadmat
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from utils import load_bin_centers
from utils import prepare_bins
from utils import plot_maze, mark_locations
from utils import get_position_bin
from sklearn.metrics import confusion_matrix
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
from scipy.ndimage import gaussian_filter1d
import itertools
from utils import *

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]

group_iti_and_return = True

# PLOTTING PARAMETERS

settings_name = 'reviewer_pars'
save_plots = False
plot_format = 'svg'
dpi = 400
dot_scaling = 1200
min_pbe_duration_in_ms = 50

max_n_traj = 300
max_n_traj = 200



# --- SET UP PLOT PATHS -------------------------------------------------------

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'Figure3')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

# dfo, df_not_sig = load_replay(sessions=sessions,
#                              PBEs_settings_name=PBEs_settings_name,
#                              PBEs_area=PBEs_area,
#                              data_version=data_version,
#                              dec_settings_name=dec_settings_name,
#                              dec_area=dec_area,
#                              min_pbe_duration_in_ms=min_pbe_duration_in_ms,
#                              shuffle_types=shuffle_types,
#                              shuffle_p_vals=shuffle_p_vals,
#                              group_iti_and_return=group_iti_and_return,
#                              return_not_sig=True,
#                              max_average_speed=SPEED_THRESHOLD,
#                              NEWSPEED=True,
#                              correct_incorrect=True,
#                              exclude_run_phase=True)


df = pd.read_csv(os.path.join(REPLAY_FOLDER, 'df_final_selection.csv'))

# recast from strings to arrays
restored_trajectories = []
for traj in df['trajectory']:
    restraj = [int(s.strip('[]\n')) for s in traj.split(' ') if len(s.strip('[]\n')) > 0]

    restored_trajectories.append(restraj)
df['trajectory'] = restored_trajectories

# for i in range(df.shape[0]):
#     try:
#         np.testing.assert_array_equal(dfo['trajectory'][i], df['trajectory'][i])
#     except:
#         print(i)


# --- LOAD BIN CENTERS ---------------------------------------------------------
sess_ind = 10
F = loadmat(DATA_FOLDER + '/F/F_units/F_All.mat')['F']
rat, day, session = F[sess_ind].dir.split('/')[-3:]
bin_centers_combined = load_bin_centers(DATA_FOLDER, rat, day, session)
bin_centers, bin_labels = prepare_bins(bin_centers_combined)


# --- MAKE LEGEND --------------------------------------------------------------
colors = [beginning_end_palette['dec_start_bin'],
          beginning_end_palette['dec_end_bin']]
texts = ["Trajectory start", "Trajectory end"]
patches = [ plt.plot([],[], marker="o", ms=10, ls="", mec=None, color=colors[i],
            label="{:s}".format(texts[i]) )[0]  for i in range(len(texts)) ]



# LEGEND FOR STACKS

colors = [beginning_end_palette['dec_start_bin'],
          beginning_end_palette['dec_end_bin']]
texts = ["Replay trajectory start", "Replay trajectory end"]
patches = [ plt.plot([],[], marker="o", ms=8, ls="", mec=None, color=colors[i],
            label="{:s}".format(texts[i]) )[0]  for i in range(len(texts)) ]

patches = patches +  [mpatches.Patch(facecolor=animal_loc_color, lw=0, alpha=0.5,
                      label='Animal location during replay')]

# Create the figure
f, ax = plt.subplots(figsize=[8, 1])
ax.legend(handles=patches, frameon=False,
          mode="expand", ncol=3)
ax.axis('off')
plt.tight_layout()
plt.show()
plot_name = 'stack_legend.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



def check_linerized_traj(traj, side, direction):
    start = traj[0]
    end = traj[-1]
    if direction == 'positive' and side == 'right' :
        if not start < end :
            raise ValueError
    elif direction == 'positive' and side == 'left' :
        if not start > end :
            raise ValueError
    elif direction == 'negative' and side == 'right' :
        if not start > end :
            raise ValueError
    elif direction == 'negative' and side == 'left' :
        if not start < end :
            raise ValueError


# --- STACKED TRAJECTORIES SPLIT BY SIDE NO KDE --------------------------------

if False:
    for current_trial_side in ['left', 'right'] :
        for phase in phases:

            # --- PREPARE DATA ---------------------------
            df_sel = df[df['epoch'] == 'task']
            df_sel = df_sel[df_sel['phase'] == phase]
            df_sel = df_sel[df_sel['current_trial_side'] == current_trial_side]

            if df_sel.shape[0] > max_n_traj:
                    ind = np.random.choice(np.arange(df_sel.shape[0]), max_n_traj, replace=False)
                    df_sel = df_sel.iloc[ind]

            new_trajs = []
            for i in range(df_sel.shape[0]):
                row = df_sel.iloc[i]
                traj = row['trajectory']
                loc = row['pbe_loc_bin']
                side = row['pbe_side']
                direction = row['replay_direction']
                traj = linearize_trajectory(traj, side, direction)
                check_linerized_traj(traj, side, direction)
                new_trajs.append(traj)


            df_sel['trajectory'] = new_trajs
            df_sel['new_dec_start_bin'] = [t[0] for t in df_sel['trajectory']]
            df_sel['new_dec_end_bin'] = [t[-1] for t in df_sel['trajectory']]

            df1 = df_sel[df_sel['replay_direction'] == 'negative']
            df2 = df_sel[df_sel['replay_direction'] == 'positive']
            df1 = df1.sort_values(by=['new_dec_start_bin', 'new_dec_end_bin'])
            df2 = df2.sort_values(by=['new_dec_start_bin', 'new_dec_end_bin'])
            df_sel = pd.concat((df1, df2))

            # --- PLOT -----------------------------------
            f, ax = plt.subplots(1, 1, figsize=[2.5, 3.5])

            ytext = df_sel[df_sel['replay_direction'] == 'negative'].shape[0]

            if phase == 'iti' or phase == 'img' :
                ax.axvspan(*location_bands[phase], alpha=0.5, color=animal_loc_color, lw=0)
            elif phase == 'run' or phase == 'reward' :
                if current_trial_side == 'left' :
                    ax.axvspan(*location_bands[phase][0], alpha=0.5,  color=animal_loc_color, lw=0)
                elif current_trial_side == 'right' :
                    ax.axvspan(*location_bands[phase][1], alpha=0.5,  color=animal_loc_color, lw=0)

            ba = int(df_sel.shape[0] / 40)
            ax.plot([-31, -31], [1, ytext-ba], lw=4.5,
                       c=replay_direction_palette['negative'])
            ax.plot([-31, -31], [ytext+ba, df_sel.shape[0]-1], lw=4.5,
                       c=replay_direction_palette['positive'])

            for i in range(df_sel.shape[0]):
                row = df_sel.iloc[i]
                traj = row['trajectory']
                loc = row['pbe_loc_bin']
                side = row['pbe_side']
                direction = row['replay_direction']

                if traj[-1] > traj[0]:
                    traj = np.arange(traj[0], traj[-1]+0.1, 0.5)
                elif traj[-1] < traj[0]:
                    #print('rev')
                    traj = np.arange(traj[0], traj[-1]-0.1, -0.5)
                N = traj.shape[0]
                for j in range(traj.shape[0]-1):
                    #print([traj[j], traj[j+1]])
                    colors = trajectory_cmap(int(255 * j / N))
                    ax.plot([traj[j], traj[j+1]], [i, i], color=colors, zorder=100)

                ax.scatter(traj[0], i, c=trajectory_cmap(0), s=6, zorder=101)
                ax.scatter(traj[-1], i, c=trajectory_cmap(255), s=6, zorder=101)
                # ax[1].scatter(loc, i, facecolor=animal_loc_color, marker='.', zorder=100, alpha=0.8,
                #               edgecolor='w', lw=0, s=40)

            ax.set_yticks([], [])
            ax.tick_params(axis=u'both', which=u'both',length=0)
            ax.set_xlabel('Linearized location', labelpad=15)
            ax.legend(frameon=False)
            ax.get_legend().remove()
            #ax2.get_legend().remove()
            ax.set_xlim([-31, 31])
            ax.set_xticks([-29, -reward_bin, 0, reward_bin, 29])
            ax.set_xticklabels(['IMG', 'REW', 'IMG', 'REW', 'IMG'], fontsize=8)
            sns.despine(left=True, ax=ax, trim=True)
            for loc in [0, reward_bin, -reward_bin]:
                ax.axvline(loc, c=maze_color, zorder=-10, ls='--')
            plt.tight_layout()

            plot_name = 'trajectory_stack_{}_{}.{}'.format(phase, current_trial_side, plot_format)
            f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- STACKED TRAJECTORIES SPLIT BY SIDE WITH KDE ------------------------------
for current_trial_side in ['left', 'right'] :
    for phase in phases:

        df_sel = df[df['epoch'] == 'task']
        df_sel = df_sel[df_sel['phase'] == phase]
        df_sel = df_sel[df_sel['current_trial_side'] == current_trial_side]

        if df_sel.shape[0] > max_n_traj:
            ind = np.random.choice(np.arange(df_sel.shape[0]), max_n_traj, replace=False)
            df_sel = df_sel.iloc[ind]

        new_trajs = []
        for i in range(df_sel.shape[0]):
            row = df_sel.iloc[i]
            traj = row['trajectory']
            loc = row['pbe_loc_bin']
            side = row['pbe_side']
            direction = row['replay_direction']
            traj = linearize_trajectory(traj, side, direction)
            check_linerized_traj(traj, side, direction)
            new_trajs.append(traj)

        df_sel['trajectory'] = new_trajs
        df_sel['new_dec_start_bin'] = [t[0] for t in df_sel['trajectory']]
        df_sel['new_dec_end_bin'] = [t[-1] for t in df_sel['trajectory']]

        df1 = df_sel[df_sel['replay_direction'] == 'negative']
        df2 = df_sel[df_sel['replay_direction'] == 'positive']
        df1 = df1.sort_values(by=['new_dec_start_bin', 'new_dec_end_bin'])
        df2 = df2.sort_values(by=['new_dec_start_bin', 'new_dec_end_bin'])
        df_sel = pd.concat((df1, df2))



        f, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios' : [1, 7]},
                             sharex=True, figsize=[2.5, 3.5])

        ax2 = ax[0].twinx()
        bw = 0.6
        # if phase == 'run' or phase == 'img':
        #     bw = 0.2

        sns.kdeplot(df_sel['new_dec_start_bin'], ax=ax[0], color=trajectory_cmap(0),
                    label='Start of replay trajectory', bw=bw, cut=100, clip=[-29, 29],
                    kernel='gau')

        sns.kdeplot(df_sel['new_dec_end_bin'], ax=ax[0], color=trajectory_cmap(255),
                    label='End of replay trajectory', bw=bw, cut=100, clip=[-29, 29])


        ytext = df_sel[df_sel['replay_direction'] == 'negative'].shape[0]

        ba = int(df_sel.shape[0] / 40)
        ax[1].plot([-31, -31], [1, ytext-ba], lw=4.5,
                   c=replay_direction_palette['negative'])
        ax[1].plot([-31, -31], [ytext+ba, df_sel.shape[0]-1], lw=4.5,
                   c=replay_direction_palette['positive'])

        if phase == 'iti' or phase == 'img' :
            ax[1].axvspan(*location_bands[phase], alpha=0.5, color=animal_loc_color, lw=0)
        elif phase == 'run' or phase == 'reward' :
            if current_trial_side == 'left' :
                ax[1].axvspan(*location_bands[phase][0], alpha=0.5, color=animal_loc_color, lw=0)
            elif current_trial_side == 'right' :
                ax[1].axvspan(*location_bands[phase][1], alpha=0.5, color=animal_loc_color, lw=0)

        for i in range(df_sel.shape[0]):
            row = df_sel.iloc[i]
            traj = row['trajectory']
            loc = row['pbe_loc_bin']
            side = row['pbe_side']
            direction = row['replay_direction']

            if traj[-1] > traj[0]:
                traj = np.arange(traj[0], traj[-1]+0.1, 0.5)
            elif traj[-1] < traj[0]:
                #print('rev')
                traj = np.arange(traj[0], traj[-1]-0.1, -0.5)
            N = traj.shape[0]
            for j in range(traj.shape[0]-1):
                #print([traj[j], traj[j+1]])
                colors = trajectory_cmap(int(255 * j / N))
                ax[1].plot([traj[j], traj[j+1]], [i, i], color=colors, zorder=100,
                           lw=1.2)

            ax[1].scatter(traj[0], i, c=trajectory_cmap(0), s=4, zorder=101)
            ax[1].scatter(traj[-1], i, c=trajectory_cmap(255), s=4, zorder=101)
            # ax[1].scatter(loc, i, facecolor=animal_loc_color, marker='.', zorder=100, alpha=0.8,
            #               edgecolor='w', lw=0, s=40)
        sns.despine(left=True, bottom=True, ax=ax[0])
        sns.despine(left=True, bottom=True, ax=ax2)

        for axx in ax:
            axx.set_yticks([], [])
        ax2.set_yticks([], [])
        ax[0].tick_params(axis=u'both', which=u'both',length=0)
        ax[1].set_xlabel('Linearized location', labelpad=15, fontdict={'fontsize':13})
        ax[0].legend(frameon=False)
        leg = ax[0].get_legend()
        ax[0].get_legend().remove()
        #ax2.get_legend().remove()
        ax[1].set_xlim([-31, 31])
        ax[1].set_xticks([-29, -reward_bin, 0, reward_bin, 29])
        ax[1].set_xticklabels(['IMG', 'REW', 'IMG', 'REW', 'IMG'], fontsize=11)
        sns.despine(left=True, ax=ax[1], trim=True)
        for loc in [0, reward_bin, -reward_bin]:
            ax[1].axvline(loc, c=maze_color, zorder=-10, ls='--')
        plt.tight_layout()

        plot_name = 'trajectory_stack_with_kde_{}_{}_{}.{}'.format(phase, current_trial_side, settings_name, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)





# --- STACKED TRAJECTORIES BOTH SIDES COMBINED ---------------------------------

if False:
    max_n_traj = 300


    for phase in phases:
        df_sel = df[df['epoch'] == 'task']
        df_sel = df_sel[df_sel['phase'] == phase]

        #df_sel = df_sel[~df_sel['pbe_loc_bin'].isna()]

        if df_sel.shape[0] > max_n_traj:
                ind = np.random.choice(np.arange(df_sel.shape[0]), max_n_traj, replace=False)
                df_sel = df_sel.iloc[ind]

        new_trajs = []
        for i in range(df_sel.shape[0]):
            row = df_sel.iloc[i]
            traj = row['trajectory']
            loc = row['pbe_loc_bin']
            side = row['pbe_side']
            direction = row['replay_direction']
            traj = linearize_trajectory(traj, side, direction)
            new_trajs.append(traj)
        df_sel['trajectory'] = new_trajs
        df_sel['new_dec_start_bin'] = [t[0] for t in df_sel['trajectory']]
        df_sel['new_dec_end_bin'] = [t[-1] for t in df_sel['trajectory']]


        df1 = df_sel[df_sel['replay_direction'] == 'negative']
        df2 = df_sel[df_sel['replay_direction'] == 'positive']
        df1 = df1.sort_values(by=['new_dec_start_bin', 'new_dec_end_bin'])
        df2 = df2.sort_values(by=['new_dec_start_bin', 'new_dec_end_bin'])
        df_sel = pd.concat((df1, df2))

        f, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios' : [1, 7]},
                             sharex=True, figsize=[2.5, 6.5])

        ax2 = ax[0].twinx()  # instantiate a second axes that shares the same x-axis

        # if phase == 'img':
        #     bw_loc = 0.5
        # else:
        #     bw_loc = 0.1
        # sns.kdeplot(df_sel['pbe_loc_bin'], ax=ax2, c=animal_loc_color,
        #             label='Animal location during REs', bw=bw_loc,
        #             cut=100, clip=[-29, 29])

        bw = 0.1
        sns.kdeplot(df_sel['new_dec_start_bin'], ax=ax[0], c=trajectory_cmap(0),
                    label='Start of replay trajectory', bw=bw, cut=100, clip=[-29, 29])

        sns.kdeplot(df_sel['new_dec_end_bin'], ax=ax[0], c=trajectory_cmap(255),
                    label='End of replay trajectory', bw=bw, cut=100, clip=[-29, 29])


        ytext = df_sel[df_sel['replay_direction'] == 'negative'].shape[0]

        ba = int(df_sel.shape[0] / 40)
        ax[1].plot([-31, -31], [1, ytext-ba], lw=4.5,
                   c=replay_direction_palette['negative'])
        ax[1].plot([-31, -31], [ytext+ba, df_sel.shape[0]-1], lw=4.5,
                   c=replay_direction_palette['positive'])

        if phase == 'iti' or phase == 'img':
            ax[1].axvspan(*location_bands[phase], alpha=0.5, color=animal_loc_color, lw=0)
        elif phase == 'run' or phase == 'reward':
            ax[1].axvspan(*location_bands[phase][0], alpha=0.5, color=animal_loc_color, lw=0)
            ax[1].axvspan(*location_bands[phase][1], alpha=0.5, color=animal_loc_color, lw=0)

        for i in range(df_sel.shape[0]):
            row = df_sel.iloc[i]
            traj = row['trajectory']
            loc = row['pbe_loc_bin']
            side = row['pbe_side']
            direction = row['replay_direction']

            if traj[-1] > traj[0]:
                traj = np.arange(traj[0], traj[-1]+0.1, 0.5)
            elif traj[-1] < traj[0]:
                #print('rev')
                traj = np.arange(traj[0], traj[-1]-0.1, -0.5)
            N = traj.shape[0]
            for j in range(traj.shape[0]-1):
                #print([traj[j], traj[j+1]])
                colors = trajectory_cmap(int(255 * j / N))
                ax[1].plot([traj[j], traj[j+1]], [i, i], color=colors, zorder=100)

            ax[1].scatter(traj[0], i, c=trajectory_cmap(0), s=6, zorder=101)
            ax[1].scatter(traj[-1], i, c=trajectory_cmap(255), s=6, zorder=101)
            # ax[1].scatter(loc, i, facecolor=animal_loc_color, marker='.', zorder=100, alpha=0.8,
            #               edgecolor='w', lw=0, s=40)
        sns.despine(left=True, bottom=True, ax=ax[0])
        sns.despine(left=True, bottom=True, ax=ax2)

        for axx in ax:
            axx.set_yticks([], [])
        ax2.set_yticks([], [])
        ax[0].tick_params(axis=u'both', which=u'both',length=0)
        ax[1].set_xlabel('Linearized location', labelpad=15)
        ax[0].legend(frameon=False)
        leg = ax[0].get_legend()
        ax[0].get_legend().remove()
        #ax2.get_legend().remove()
        ax[1].set_xlim([-31, 31])
        ax[1].set_xticks([-29, -reward_bin, 0, reward_bin, 29])
        ax[1].set_xticklabels(['IMG', 'REW', 'IMG', 'REW', 'IMG'], fontsize=8)
        sns.despine(left=True, ax=ax[1], trim=True)
        for loc in [0, reward_bin, -reward_bin]:
            ax[1].axvline(loc, c=maze_color, zorder=-10, ls='--')
        plt.tight_layout()

        plot_name = 'trajectory_stack_{}.{}'.format(phase, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



