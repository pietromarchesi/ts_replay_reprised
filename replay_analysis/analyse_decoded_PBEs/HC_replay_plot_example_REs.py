import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from session_selection import sessions_HC_replay
from utils import load_decoded_PBEs

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet2'
dec_area           = 'Hippocampus'

#sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,27,28,29,30]

sessions           = [2,3,4,6,7,8,9,10,11,28,29,30]

epochs             = ['task']


# FILTER PBES PARAMETERS
phase = None  # 'iti', 'img', 'run', 'return' (or list)
epoch = None # 'task', 'pre_sleep', 'post_sleep' (or list)

shuffle_types = ['column_cycle_shuffle', 'place_field_rotation_shuffle']
p_vals = [0.05, 0.05]
min_pbe_duration_in_ms = 50


# PLOTTING PARAMETERS
n_events = 3
save_plots = True
plot_format = 'svg'
dpi = 400

plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'HC_replay_example_events')


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# --- LOAD DATA ---------------------------------------------------------------

pbes_dfs, sig_dfs= [], []
rds = {}

for sess_ind in sessions:
    for epoch in epochs:

        pbes = load_decoded_PBEs(sess_ind, PBEs_settings_name, PBEs_area,
                                 data_version, dec_settings_name, dec_area)

        pbes_dfs.append(pbes['pbes'])
        sig_dfs.append(pbes['sig_df'])

        for pbe_id, det in zip(pbes['pbes'].index, pbes['detectors']):
            rds[pbe_id] = det

df = pd.concat(pbes_dfs)
sig_df = pd.concat(sig_dfs)

df = df[df['event_duration_in_ms'] >= min_pbe_duration_in_ms]

# --- FILTER PBES -------------------------------------------------------------

sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types,
                                        p_vals=p_vals)
df = df[np.isin(df.index, sel_pbes_sig)]

# --- GET GOOD EVENTS ---------------------------------------------------------

df_sort = df.sort_values(by=['replay_score', 'average_speed'],
                         ascending=False)


right_events_positive = ['6_task_434',
                         '6_task_1453',
                         '29_task_718',
                         '7_task_850',
                         '29_task_515',
                         '7_task_1258']

right_events_negative = ['6_task_1188',
                        '29_task_915',
                         '6_task_945',
                         '6_task_821']

left_events_positive = ['7_task_934',
                        '10_task_1299',
                        '2_task_2033',
                        '6_task_707',
                        '10_task_632',
                        '11_task_714']

left_events_negative = ['3_task_680',
                        '6_task_581',
                        '10_task_421',
                        '6_task_1527']


dict_events = {'right_pos' : right_events_positive,
               'right_neg' : right_events_negative,
               'left_pos' : left_events_positive,
               'left_neg' : left_events_negative}



# --- GET MIN AND MAX SPIKES AND PROBA -----------------------------------------

max_spikes_list = []
max_proba_list = []
for ev_type in dict_events.keys():
    ids = dict_events[ev_type]
    ids = ids[0:n_events]
    for row, id in enumerate(ids):
        pbe = rds[id]
        max_spikes_list.append(pbe.spikes.max())
        max_proba_list.append(pbe.y_pred_proba.max())

vmax_spikes = np.max(max_spikes_list)
vmax_proba = np.round(np.max(max_proba_list), 1)
#vmax_spikes = 5
#vmax_prob = 0.5
vmin_spikes = 0
vmin_proba = 0

fontsize = 13
for ev_type in dict_events.keys():
    ids = dict_events[ev_type]
    ids = ids[0:n_events]
    figsize=[tiny_panel_side*2, 1.6*n_events]

    if np.isin(ev_type, ['right_pos', 'left_pos']):
        line_color = replay_direction_palette['positive']
    else:
        line_color = replay_direction_palette['negative']


    f, ax = plt.subplots(n_events, 2, figsize=figsize)

    for row, id in enumerate(ids):
        pbe = rds[id]
        pbe.binsize_in_ms = 10

        im = pbe._plot_binned_spikes(ax[row, 0], plot_cbar=False,
                                vmin=vmin_spikes, vmax=vmax_spikes)

        im2 = pbe._plot_decoded_pbe(ax[row, 1], plot_cbar=False,
                              vmin=vmin_proba, vmax=vmax_proba, line_color=line_color)

        if id == '6_task_434' or id == '3_task_680':
            y = np.interp(x=pbe.time_bins,
                          xp=[pbe.time_bins[0], pbe.time_bins[-1]],
                          fp=[pbe.start_location+30, pbe.end_location+30])
            ax[row, 1].plot(pbe.time_bins, y, c=line_color, ls=':', lw=3)
            #raise ValueError
        if np.isin(ev_type, ['right_pos', 'right_neg']):
            ax[row, 1].set_yticks([0, reward_bin])
            ax[row, 1].set_yticklabels(['IMG', 'REW'])
            ax[row, 1].yaxis.tick_right()
            for loc in [0, reward_bin] :
                ax[row, 1].axhline(loc, c=maze_color, zorder=10, ls='--')

        elif np.isin(ev_type, ['left_pos', 'left_neg']):
            ax[row, 1].set_yticks([0, -reward_bin])
            ax[row, 1].set_yticklabels(['IMG', 'REW'])
            ax[row, 1].yaxis.tick_right()
            for loc in [0, -reward_bin] :
                ax[row, 1].axhline(loc, c=maze_color, zorder=10, ls='--')

        for ii in [0, 1]:
            ax[row, ii].set_xlabel('')
            ax[row, ii].set_ylabel('')

        ax[row, 0].set_yticks([])
    ax[1, 0].set_ylabel('CA1 units', fontdict={'fontsize':fontsize})
    ax[1, 1].set_ylabel('Location', fontdict={'fontsize':fontsize})
    # add a big axis, hide frame
    # f.add_subplot(111, frameon=False)
    # plt.tick_params(labelcolor='none', top=False, bottom=False, left=False,
    #                 right=False)
    # plt.xlabel('Time [ms]')
    f.text(0.5, 0.0, 'Time [ms]', ha='center', fontdict={'fontsize':fontsize})

    #plt.ylabel('CA1 units')

    sns.despine(top=True, right=True, offset=0, trim=False)
    plt.subplots_adjust(wspace=0.22, hspace=0.22)
    plt.tight_layout()

    plot_name = 'pbe_panel_{}_combined.{}'.format(ev_type, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), bbox_inches='tight', dpi=dpi)



# --- PLOT COLOR BARS FOR SPIKES AND PROBA -------------------------------------

vmax = vmax_spikes
vmin = vmin_spikes


f1, axbo = plt.subplots(1, 1, figsize=[1,1])
f, ax = plt.subplots(1, 1, figsize=[2,2])
cmap = plt.get_cmap('Greys', vmax+1)
sorting = pbe.dec.ratemap.argmax(axis=1).argsort()
sorted_spikes = pbe.spikes[:, sorting]
cax = axbo.imshow(sorted_spikes.T, origin='lower', cmap=cmap,
                aspect='auto', vmin=vmin-0.5, vmax=vmax+0.5)
cbar = plt.colorbar(cax, ax=ax, pad=0.2,
                    ticks=np.arange(vmin, vmax+0.5).astype(int),
                    orientation='vertical', aspect=10)
cbar.set_label('# of spikes', size=matplotlib.rcParams['font.size'] - 1)
#cbar.ax.tick_params(length=0)
ax.axis('off')
plt.tight_layout(rect=[0, 0, .8, 0.8])

plot_name = 'pbe_panel_colorbar_spikes.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


f, ax = plt.subplots(1, 1, figsize=[2,2])

cbar = plt.colorbar(im2, ax=ax, pad=0.2, cmap='Blues',
                    ticks=np.arange(0, vmax_proba+0.1, 0.2),
                    orientation='vertical', aspect=10)
cbar.set_label('Probability', size=matplotlib.rcParams['font.size'] - 1)

ax.axis('off')
plt.tight_layout(rect=[0, 0, .8, 0.8])
plot_name = 'pbe_panel_colorbar_proba.{}'.format(plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



