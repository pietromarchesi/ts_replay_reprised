#!/usr/bin/env bash

#SBATCH --job-name=pbe_dkw
#SBATCH --output=pbe_dkw%A_%a.out
#SBATCH --error=pbe_dkw%A_%a.err
##SBATCH --array=0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30
#SBATCH --array=12,27,28,29,30
###SBATCH --array=0,1
#SBATCH --partition=cpu
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=4000
#SBATCH --exclude=csn-cpu[1-5]
##SBATCH --exclusive

data_version=$1
pbes_settings=$2
dec_settings_name=$3

source ~/venvs/basic36/bin/activate

python ../decode_PBEs_DKW.py --sess_ind $SLURM_ARRAY_TASK_ID \
                        --data_version $data_version \
                        --PBEs_settings $pbes_settings \
                        --dec_settings_name $dec_settings_name \
