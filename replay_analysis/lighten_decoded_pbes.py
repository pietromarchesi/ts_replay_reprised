import os
import pickle
from loadmat import loadmat
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from constants import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from plotting_style import *
from utils import load_bin_centers
from utils import prepare_bins
from utils import plot_maze, mark_locations
from utils import get_position_bin
from sklearn.metrics import confusion_matrix
from scipy.stats import wilcoxon, mannwhitneyu
from utils import is_on_central_arm, merge_duplicate_left_right_bins
from utils import add_ripples_to_pbes_df
from session_selection import sessions_HC_replay
from utils import load_decoded_PBEs

data_version       = 'dec16'
PBEs_settings_name = 'dec19k20'
PBEs_area          = 'Hippocampus'
dec_settings_name  = 'DecSet6'
dec_area           = 'Hippocampus'

sessions           = [0,1,2,3,4,5,6,7,8,9,10,11,12,27,28,29,30]
epochs             = ['task']


# --- LOAD DATA ---------------------------------------------------------------

pbes_dfs, sig_dfs = [], []

for sess_ind in sessions:
    print(sess_ind)
    for epoch in epochs:
        try:
            pbes = load_decoded_PBEs(sess_ind, PBEs_settings_name, PBEs_area,
                                     data_version, dec_settings_name, dec_area)

            output_folder = os.path.join(REPLAY_FOLDER, 'results', 'decoded_PBEs',
                                         'pbe_setting_{}_{}_{}_dec_setting_{}_{}'.format(
                                             PBEs_settings_name, PBEs_area,
                                             data_version, dec_settings_name,
                                             dec_area))
            file_name = 'decoded_PBEs_sess_{:02}_{}_{}_light.pkl'.format(sess_ind, dec_area, epoch)
            PBEs_file = os.path.join(output_folder, file_name)

            pbes_light = {'pbes' : pbes['pbes'], 'sig_df' : pbes['sig_df'],
                          'decode_pars' : pbes['decode_pars']}
            pickle.dump(pbes_light, open(PBEs_file, 'wb'))
        except FileNotFoundError:
            print('No decoded PBEs for session {}'.format(sess_ind))
        # pbes_dfs.append(pbes['pbes'])
        # sig_dfs.append(pbes['sig_df'])

# df = pd.concat(pbes_dfs)
# sig_df = pd.concat(sig_dfs)