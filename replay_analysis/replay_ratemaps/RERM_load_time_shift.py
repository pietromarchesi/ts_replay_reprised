import os
from constants import *
import pickle
import statsmodels.stats.descriptivestats
from statsmodels.stats.multitest import multipletests
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
from plotting_style import *
from sklearn.preprocessing import MinMaxScaler
from scipy.ndimage import gaussian_filter1d
from utils import *


#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
#ratemap_settings = 'jul4_DecSet2_smooth2_morelages'
#ratemap_settings = 'dec6'
#ratemap_settings = 'newspeedFIXED_THETA_MANUAL2'  #newspeedFIXED_THETA_MANUAL_THETAFALSE #'newspeedFIXED_8_oct19'
ratemap_settings = 'jan9_final' #'newspeed_celldID_multicomp'

ratemap_settings = 'jan9_final_rip'

ratemap_settings = 'jan9_final_correct'

ratemap_area = 'Perirhinal'

save_plots = True
plot_format = 'svg'
dpi = 400

stat_test = 'sign_test' #'sign_test', 't-test'
sign_alpha = 0.05
p_val_correction = 'fdr_bh'
variables = ['corrcoef_debiased']  # ['corrcoef_obs', 'corrcoef_debiased']
test_values = [0] # [0, 0]
nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
cols_to_check = ['phase', 'direction', 'has_ripple', 'null_method']
subselect_sessions = None

null_dist_method_for_plot_both_shuff = 'rotate_ratemap'

estimator = np.median

subselect_shifts = False
min_shift = -8
max_shift = 8

# --- SET UP PATHS ------------------------------------------------------------
plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'RERM', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# --- LOAD RESULTS ------------------------------------------------------------
res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)
file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
df = res['df']
pars = res['pars']
shift_amounts = pars['shift_amounts']
smoothing_sigma = pars['smooth_sigma']


if subselect_sessions is not None:
    df = df[np.isin(df['sess_ind'], subselect_sessions)]

if subselect_shifts:
    df = df[df['shift_amount'] >= min_shift]
    df = df[df['shift_amount'] <= max_shift]
    shift_amounts = np.arange(min_shift, max_shift+1)

    #shift_amounts = np.array([-8, -6, -4, -2, 0, 2, 4, 6, 8])


# --- STATISTICS NO SHIFT ------------------------------------------------------

variables = ['percentile_rank']
test_values = [50] # [0, 0]

for variable, test_value in zip(variables, test_values):
    for null_method in nulldistmet_plots:
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'all') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_method)]

        ccd = dfx[dfx['shift_amount'] == 0][variable]


        if stat_test == 'sign_test' :
            #m, p_val = statsmodels.stats.descriptivestats.sign_test(ccd, test_value)
            m, p_val = scipy.stats.wilcoxon(ccd - test_value)
        elif stat_test == 't-test' :
            t, p_val = scipy.stats.ttest_1samp(ccd, test_value)

        q50, q25, q75 = ccd.quantile([0.5, 0.25, 0.75])
        print('{} {}, median and interquartile range:  {:.1f}, {:.1f} to {:.1f}'.format(variable, null_method,
                                                    q50, q25, q75))
        print('{} different from {} with {} distribution: p={:.3}'.format(
            variable, test_value, null_method, p_val
        ))



# --- COMPARING BEFORE AND AFTER ZERO ------------------------------------------

print('\n\nnComparing the average debiased correlation coefficient per unit '
      'in the xx ms before and after 0 for Perirhinal')

df['unit_id'] = ['{}_{}'.format(r['sess_ind'], r['unit']) for i, r in
                 df.iterrows()]

for variable in variables:

    for direction in ['all'] :
        print('\nDirection: {}'.format(direction))
        print('\n\n-> 100 ms before and after')
        for null_distribution_method in nulldistmet_plots:
            times_before = np.arange(-10, 0)
            times_after = np.arange(1, 10+1)
            dfx = df[(df['phase'] == 'all') &
                     (df['direction'] == direction) &
                     (df['has_ripple'] == 'all') &
                     (df['null_method'] == null_distribution_method)]

            dfx = dfx.drop(['ratemap_real', 'ratemap_event', 'phase',
                            'direction', 'null_method','has_ripple'], axis=1)

            df1 = dfx[np.isin(dfx['shift_amount'], times_before)].groupby('unit_id').mean().reset_index()
            df2 = dfx[np.isin(dfx['shift_amount'], times_after)].groupby('unit_id').mean().reset_index()
            np.testing.assert_array_equal(df1['unit_id'], df2['unit_id'])

            print('\ntesting {} with {}'.format(variable, null_distribution_method))
            v1 = (df1[variable].quantile(q=0.5),
                       df1[variable].quantile(q=0.25),
                       df1[variable].quantile(q=0.75))

            v2 = (df2[variable].quantile(q=0.5),
                       df2[variable].quantile(q=0.25),
                       df2[variable].quantile(q=0.75))

            print('Before: {:.2f}, {:.2f}-{:.2f}'
                  '\nAfter: {:.2f}, {:.2f}-{:.2f}'.format(v1[0], v1[1], v1[2], v2[0], v2[1], v2[2]))

            t, p = scipy.stats.wilcoxon(df1[variable], df2[variable])
            print('p = {:.1g}, Wilcoxon signed-rank test, significant: ----> {}'.format(p, p<0.05/2))



# --- GLOBAL SHIFT COMBINED ----------------------------------------------------

variables = ['corrcoef_debiased']

for variable, test_value in zip(variables, test_values):

    # Create the significance mask using both shuffles
    masks = {}
    for null_distribution_method in nulldistmet_plots:
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'all') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_distribution_method)]

        for col in cols_to_check:
            assert dfx[col].unique().shape[0] == 1

        mask, pvals_corr = get_significance_mask(dfx, correction=p_val_correction,
                                                 variable=variable,
                                                 stat_test=stat_test,
                                                 test_value=test_value,
                                                 sign_alpha=sign_alpha)
        masks[null_distribution_method] = mask
    #mask = masks[null_dist_method_for_plot_both_shuff]
    mask = np.logical_and(masks[nulldistmet_plots[0]], masks[nulldistmet_plots[1]])


    # get the values to plot based on one shuffle
    dfx = df[(df['phase'] == 'all') &
             (df['direction'] == 'all') &
             (df['has_ripple'] == 'all') &
             (df['null_method'] == null_dist_method_for_plot_both_shuff)]

    f, ax = plt.subplots(1, 1, figsize=smaller_but_wider_panel_size)

    line_color = make_rgb_transparent(area_palette[ratemap_area], (1, 1, 1), 0.9)
    line_color = area_palette[ratemap_area]

    sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax,
                 markers=True, color=line_color, ci=95,
                 markersize=5, estimator=estimator, n_boot=1000)

    if ratemap_area == 'Perirhinal':
        if null_dist_method_for_plot_both_shuff == 'rotate_spikes':
            ax.set_ylim([-0.15, 0.1])
            ax.set_yticks([-0.1, 0, 0.1])

        if null_dist_method_for_plot_both_shuff == 'rotate_ratemap':
            ax.set_ylim([-0.05, 0.25])
            ax.set_yticks([0, 0.1, 0.2])

    elif ratemap_area == 'Barrel':
        if null_dist_method_for_plot_both_shuff == 'rotate_spikes':
            ax.set_ylim([-0.18, 0.03])
        if null_dist_method_for_plot_both_shuff == 'rotate_ratemap' :
            ax.set_ylim([-0.1, 0.15])
    #
    # elif ratemap_area == 'Barrel':
    #     if null_dist_method_for_plot_both_shuff == 'rotate_spikes':
    #         ax.set_ylim([ax.get_ylim()[0], 0.2])
    #     if null_dist_method_for_plot_both_shuff == 'rotate_ratemap':
    #         ax.set_ylim([-0.03, 0.28])

    ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))
    height = ax.get_ylim()[0] + 0.01

    signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
    ax.scatter(shift_amounts, signif_line, lw=1, alpha=1, c=line_color,
               marker = 'D', s = 10)
    ax.set_ylabel('Median {} replay\ncoordination strength ($r_d$)'.format(ratemap_area_sigla[ratemap_area]))
    ax.set_xlabel('Time shift [ms]')
    ax.set_xticks([-10, -5, 0, 5, 10])
    #ax.set_yticks([0, 0.1, 0.2])
    ax.set_xticklabels([-100, -50, 0, 50, 100])
    sns.despine()


    ax.axhline(test_value, c='grey', ls='--', lw=2, zorder=-1)
    #ax.get_legend().remove()
    plt.tight_layout()

    if save_plots:
        plot_name = 'ratemap_shift_BOTHSHUF_{}_{}_{}.' \
                    '{}'.format(ratemap_area, variable, null_dist_method_for_plot_both_shuff, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- GLOBAL SHIFT SPLIT BY REPLAY DIRECTION BOTH SHUFFLES ---------------------

for variable, test_value in zip(variables, test_values):

    # make the masks using both shuffles
    masks = {'positive' : [],
             'negative' : []}
             #'difference' : []}

    for null_distribution_method in nulldistmet_plots:

        for direction in ['positive', 'negative']:
            dfx = df[(df['phase'] == 'all') &
                     (df['direction'] == direction) &
                     (df['has_ripple'] == 'all') &
                     (df['null_method'] == null_distribution_method)]

            for col in cols_to_check:
                assert dfx[col].unique().shape[0] == 1

            mask, pvals_corr = get_significance_mask(dfx, correction=p_val_correction,
                                                     variable=variable,
                                                     stat_test=stat_test,
                                                     test_value=test_value,
                                                     sign_alpha=sign_alpha)

            masks[direction].append(mask)


    # find the difference mask using again both shuffles
    # SKIP THE MASK OF THE DIFFERENCE
    # for null_distribution_method in nulldistmet_plots:
    #
    #     df_split = {}
    #     for direction in ['positive', 'negative']:
    #         dfx = df[(df['phase'] == 'all') &
    #                  (df['direction'] == direction) &
    #                  (df['has_ripple'] == 'all') &
    #                  (df['null_method'] == null_distribution_method)]
    #         df_split[direction] = dfx
    #
    #     mask, pvals_corr = get_significance_mask_split(df_split['positive'],
    #                                                    df_split['negative'],
    #                                              correction=p_val_correction,
    #                                              variable=variable,
    #                                              stat_test=stat_test,
    #                                              sign_alpha=sign_alpha)
    #     masks['difference'].append(mask)

    # combine masks
    masks['positive'] = np.logical_and(masks['positive'][0], masks['positive'][1])
    masks['negative'] = np.logical_and(masks['negative'][0], masks['negative'][1])

    #masks['difference'] = np.logical_and(masks['difference'][0], masks['difference'][1])


    f, ax = plt.subplots(1, 1, figsize=smaller_but_wider_panel_size)

    for direction in ['negative', 'positive'] :
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == direction) &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_dist_method_for_plot_both_shuff)]
        sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax,
                     color=replay_direction_palette[direction], label=direction,
                     markers=False, style='null_method', markersize=5,
                     estimator=estimator)

    if ratemap_area == 'Perirhinal':

        if null_dist_method_for_plot_both_shuff == 'rotate_spikes':
            ax.set_ylim([-0.15, 0.1])
            ax.set_yticks([-0.1, 0, 0.1])


        if null_dist_method_for_plot_both_shuff == 'rotate_ratemap':
            ax.set_ylim([-0.05, 0.25])
            ax.set_yticks([0, 0.1, 0.2])

    elif ratemap_area == 'Barrel':
        if null_dist_method_for_plot_both_shuff == 'rotate_spikes':
            ax.set_ylim([-0.18, 0.03])
        if null_dist_method_for_plot_both_shuff == 'rotate_ratemap':
            ax.set_ylim([-0.1, 0.15])

    #ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))

    for i, (dir, mask) in enumerate(masks.items()):
        height = ax.get_ylim()[0] + 0.01 + 0.008 * i
        print(height)
        signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
        ax.scatter(shift_amounts, signif_line, color=replay_direction_palette[dir],
                lw=1,alpha=1, marker='D', s=10)

    sns.despine()
    ax.legend(frameon=False)
    ax.set_ylabel('Median {} replay\ncoordination strength ($r_d$)'.format(ratemap_area_sigla[ratemap_area]))
    ax.set_xlabel('Time shift [ms]')
    ax.set_xticks([-10, -5, 0, 5, 10])
    ax.set_xticklabels([-100, -50, 0, 50, 100])
    ax.get_legend().remove()
    ax.axhline(test_value, c='grey', ls='--', lw=2, zorder=-1)
    plt.tight_layout()

    if save_plots:
        plot_name = 'ratemap_shift_by_replay_direction_BOTHSHUF_{}_{}_{}.' \
                    '{}'.format(ratemap_area, variable, null_dist_method_for_plot_both_shuff, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# --- GLOBAL SHIFT SPLIT BY REPLAY DIRECTION BOTH SHUFFLES ---------------------

if ratemap_settings == 'jan9_final_rip':

    for variable, test_value in zip(variables, test_values):

        # make the masks using both shuffles
        masks = {'True' : [],
                 'False' : []}
                 #'difference' : []}

        # the column was cast to object
        df['has_ripple'] = np.array(df['has_ripple']).astype(str)

        for null_distribution_method in nulldistmet_plots:

            for hasripple in ['True', 'False']:
                dfx = df[(df['phase'] == 'all') &
                         (df['direction'] == 'all') &
                         (df['has_ripple'] == hasripple) &
                         (df['null_method'] == null_distribution_method)]

                for col in cols_to_check:
                    assert dfx[col].unique().shape[0] == 1

                mask, pvals_corr = get_significance_mask(dfx, correction=p_val_correction,
                                                         variable=variable,
                                                         stat_test=stat_test,
                                                         test_value=test_value,
                                                         sign_alpha=sign_alpha)

                masks[hasripple].append(mask)


        # find the difference mask using again both shuffles
        # SKIP THE MASK OF THE DIFFERENCE
        # for null_distribution_method in nulldistmet_plots:
        #
        #     df_split = {}
        #     for direction in ['positive', 'negative']:
        #         dfx = df[(df['phase'] == 'all') &
        #                  (df['direction'] == direction) &
        #                  (df['has_ripple'] == 'all') &
        #                  (df['null_method'] == null_distribution_method)]
        #         df_split[direction] = dfx
        #
        #     mask, pvals_corr = get_significance_mask_split(df_split['positive'],
        #                                                    df_split['negative'],
        #                                              correction=p_val_correction,
        #                                              variable=variable,
        #                                              stat_test=stat_test,
        #                                              sign_alpha=sign_alpha)
        #     masks['difference'].append(mask)

        # combine masks
        masks['True'] = np.logical_and(masks['True'][0], masks['True'][1])
        masks['False'] = np.logical_and(masks['False'][0], masks['False'][1])

        #masks['difference'] = np.logical_and(masks['difference'][0], masks['difference'][1])

        f, ax = plt.subplots(1, 1, figsize=smaller_but_wider_panel_size)

        for hasripple in ['True', 'False'] :
            dfx = df[(df['phase'] == 'all') &
                     (df['direction'] == 'all') &
                     (df['has_ripple'] == hasripple) &
                     (df['null_method'] == null_dist_method_for_plot_both_shuff)]
            sns.lineplot(data=dfx, x='shift_amount', y=variable, ax=ax,
                         color=has_ripple_palette[hasripple], label=hasripple,
                         markers=False, style='null_method', markersize=5,
                         estimator=estimator)

        if ratemap_area == 'Perirhinal':

            if null_dist_method_for_plot_both_shuff == 'rotate_spikes':
                ax.set_ylim([-0.15, 0.1])
                ax.set_yticks([-0.1, 0, 0.1])


            if null_dist_method_for_plot_both_shuff == 'rotate_ratemap':
                ax.set_ylim([-0.05, 0.25])
                ax.set_yticks([0, 0.1, 0.2])

        elif ratemap_area == 'Barrel':
            if null_dist_method_for_plot_both_shuff == 'rotate_spikes':
                ax.set_ylim([-0.18, 0.03])
            if null_dist_method_for_plot_both_shuff == 'rotate_ratemap':
                ax.set_ylim([-0.1, 0.15])

        #ax.set_ylabel('{}\n{}'.format(variable, null_distribution_method))

        for i, (dir, mask) in enumerate(masks.items()):
            height = ax.get_ylim()[0] + 0.01 + 0.008 * i
            print(height)
            signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
            ax.scatter(shift_amounts, signif_line, color=has_ripple_palette[dir],
                    lw=1,alpha=1, marker='D', s=10)

        sns.despine()
        ax.legend(frameon=False)
        ax.set_ylabel('Median {} replay\ncoordination strength ($r_d$)'.format(ratemap_area_sigla[ratemap_area]))
        ax.set_xlabel('Time shift [ms]')
        ax.set_xticks([-10, -5, 0, 5, 10])
        ax.set_xticklabels([-100, -50, 0, 50, 100])
        ax.get_legend().remove()
        ax.axhline(test_value, c='grey', ls='--', lw=2, zorder=-1)
        plt.tight_layout()

        if save_plots:
            plot_name = 'ratemap_shift_by_ripple_BOTHSHUF_{}_{}_{}.' \
                        '{}'.format(ratemap_area, variable, null_dist_method_for_plot_both_shuff, plot_format)
            f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




