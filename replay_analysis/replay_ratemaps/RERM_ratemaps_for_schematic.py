import os
from constants import *
import pickle
import statsmodels.stats.descriptivestats
from statsmodels.stats.multitest import multipletests
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
from plotting_style import *
from sklearn.preprocessing import MinMaxScaler
from scipy.ndimage import gaussian_filter1d
import matplotlib.patches as mpatches
from utils import *

ratemap_settings = 'may11_DecSet2_smooth2_morelages'
#ratemap_settings = 'jul4_DecSet2_smooth2_morelages'
#ratemap_settings = 'feb10_DecSet26_taskphase'
ratemap_areas = ['Perirhinal', 'Barrel']

save_plots = True
plot_format = 'svg'
dpi = 400

test_values = [50, 0] # [0, 0]
nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
subselect_sessions = None
subselect_shifts = False
min_shift = -8
max_shift = 8

variable = 'corrcoef_obs'

# SWARMS
p_val_thr = 0.05
bonferroni_correct = False
null_dist_method_for_plot_both_shuff = 'rotate_spikes'


# RATEMAPS
smooth_ratemaps = True
p_val_thr_rateplots = 0.05
minmax_scale_plots = True
bins = np.arange(-29, 30)
n_rows = 1
n_cols = 2
n_plots = n_rows * n_cols
shift_amount = 0
shift_amount_ratemaps = 0



# --- SET UP PATHS ------------------------------------------------------------
plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'RERM', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# --- LOAD RESULTS ------------------------------------------------------------

dfs = []
for ratemap_area in ratemap_areas:
    res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)
    file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
    res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
    df = res['df']
    pars = res['pars']
    shift_amounts = pars['shift_amounts']
    smoothing_sigma = pars['smooth_sigma']


    if subselect_sessions is not None:
        df = df[np.isin(df['sess_ind'], subselect_sessions)]

    if subselect_shifts:
        df = df[df['shift_amount'] >= min_shift]
        df = df[df['shift_amount'] <= max_shift]
        shift_amounts = np.arange(min_shift, max_shift+1)

    df['area'] = ratemap_area
    dfs.append(df)

df = pd.concat(dfs)
df['unit_id'] = ['{}_{}_{}'.format(r['sess_ind'], r['unit'], r['area']) for i, r in df.iterrows()]
    #shift_amounts = np.array([-8, -6, -4, -2, 0, 2, 4, 6, 8])


len(df[df['area'] == 'Perirhinal']['sess_ind'].unique())
len(df[df['area'] == 'Barrel']['sess_ind'].unique())


# --- MAKE RATEMAP LEGEND ------------------------------------------------------








for ratemap_area in ratemap_areas:

    sig_ids = []
    n_total = []
    for null_distribution_method in nulldistmet_plots :
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'all') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_distribution_method) &
                 (df['shift_amount'] == 0) &
                 (df['area'] == ratemap_area)]

        if bonferroni_correct:
            p = p_val_thr / dfx.shape[0]
        else:
            p = p_val_thr

        #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
        unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
        sig_ids.append(unit_ids)
        n_total.append(dfx.shape[0])

    sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

    dfx = df[(df['phase'] == 'all') &
             (df['direction'] == 'all') &
             (df['has_ripple'] == 'all') &
             (df['null_method'] == null_dist_method_for_plot_both_shuff) &
             (df['shift_amount'] == shift_amount_ratemaps) &
             (np.isin(df['unit_id'], sig_both))]


    dfx = dfx.nlargest(n=n_plots, columns=variable)
    dfx = dfx.reset_index()
    n_neurons = dfx.shape[0]

    palette = real_event_palette[ratemap_area]



    f, axes = plt.subplots(n_rows, n_cols, figsize=[n_cols*1.6, n_rows * 1.1],
                           sharex=False)

    if n_rows == 1:
        axes = axes[np.newaxis, :]

    for kk, row in dfx.iterrows():

        real_ratemap = row['ratemap_real']
        event_ratemap = row['ratemap_event']

        try:
            ax = axes.flatten()[kk]
        except TypeError:
            ax = axes

        ax.set_yticks([])

        for i, rm in enumerate([real_ratemap, event_ratemap]):
            curve = rm

            if smooth_ratemaps:
                curve  = gaussian_filter1d(curve, smoothing_sigma)

            if minmax_scale_plots:
                mm = MinMaxScaler()
                curve = mm.fit_transform(curve.reshape(-1, 1))[:, 0]
            if i == 0:
                line = ax.plot(bins, curve, color=palette[i], lw=2)
                fillcolor = line[0].get_color()
                ax.fill_between(bins, curve,
                                color=fillcolor, alpha=0.3,
                                zorder=int(10 + 2 * n_neurons - 2 * kk - 1))
                ax.set_title('c = {:.03}'.format(row[variable]),
                             fontsize=10)

    axes.flatten()[0].set_xticks([-29, -reward_bin, 0, reward_bin, 29])
    axes.flatten()[0].set_xticklabels(['', 'REW', 'IMG', 'REW', ''], fontsize=8)
    #axes.flatten()[0].set_xticks([-29, -15, 0, 15, 29])
    #axes.flatten()[0].set_xticklabels([-29, '', 'Location', '', 29])
    #axes.flatten()[0].set_xlabel('Location')
    sns.despine(left=True,offset=2, ax=axes.flatten()[0], trim=True)

    for kk in range(1, n_plots):
        print(kk)
        axes.flatten()[kk].axis('off')

    plt.tight_layout()


    #plt.rcParams['savefig.dpi'] = dpi

    plot_name = 'sig_replay_ratemaps_{}_{}_real.{}'.format(ratemap_area, ratemap_settings, plot_format)
    plt.draw()
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)








    sig_ids = []
    n_total = []
    for null_distribution_method in nulldistmet_plots :
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'all') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_distribution_method) &
                 (df['shift_amount'] == 0) &
                 (df['area'] == ratemap_area)]

        if bonferroni_correct:
            p = p_val_thr / dfx.shape[0]
        else:
            p = p_val_thr

        #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
        unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
        sig_ids.append(unit_ids)
        n_total.append(dfx.shape[0])

    sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

    dfx = df[(df['phase'] == 'all') &
             (df['direction'] == 'all') &
             (df['has_ripple'] == 'all') &
             (df['null_method'] == null_dist_method_for_plot_both_shuff) &
             (df['shift_amount'] == shift_amount_ratemaps) &
             (np.isin(df['unit_id'], sig_both))]


    dfx = dfx.nlargest(n=n_plots, columns=variable)
    dfx = dfx.reset_index()
    n_neurons = dfx.shape[0]

    palette = real_event_palette[ratemap_area]



    f, axes = plt.subplots(n_rows, n_cols, figsize=[n_cols*1.6, n_rows * 1.1],
                           sharex=False)

    if n_rows == 1:
        axes = axes[np.newaxis, :]

    for kk, row in dfx.iterrows():

        real_ratemap = row['ratemap_real']
        event_ratemap = row['ratemap_event']

        try:
            ax = axes.flatten()[kk]
        except TypeError:
            ax = axes

        ax.set_yticks([])

        for i, rm in enumerate([real_ratemap, event_ratemap]):
            curve = rm

            if smooth_ratemaps:
                curve  = gaussian_filter1d(curve, smoothing_sigma)

            if minmax_scale_plots:
                mm = MinMaxScaler()
                curve = mm.fit_transform(curve.reshape(-1, 1))[:, 0]

            if i == 1:
                line = ax.plot(bins, curve, color=palette[i], lw=2)
                fillcolor = line[0].get_color()
                ax.fill_between(bins, curve,
                                color=fillcolor, alpha=0.3,
                                zorder=int(10 + 2 * n_neurons - 2 * kk - 1))
                ax.set_title('c = {:.03}'.format(row[variable]),
                             fontsize=10)

    axes.flatten()[0].set_xticks([-29, -reward_bin, 0, reward_bin, 29])
    axes.flatten()[0].set_xticklabels(['', 'REW', 'IMG', 'REW', ''], fontsize=8)
    #axes.flatten()[0].set_xticks([-29, -15, 0, 15, 29])
    #axes.flatten()[0].set_xticklabels([-29, '', 'Location', '', 29])
    #axes.flatten()[0].set_xlabel('Location')
    sns.despine(left=True,offset=2, ax=axes.flatten()[0], trim=True)

    for kk in range(1, n_plots):
        print(kk)
        axes.flatten()[kk].axis('off')

    plt.tight_layout()


    #plt.rcParams['savefig.dpi'] = dpi

    plot_name = 'sig_replay_ratemaps_{}_{}_replay.{}'.format(ratemap_area, ratemap_settings, plot_format)
    plt.draw()
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)