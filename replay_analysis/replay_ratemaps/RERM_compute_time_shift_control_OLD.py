import matplotlib.pyplot as plt
from constants import *
from utils import *
import quantities as pq
from plotting_style import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from replaydetector.replayratemaps import ReplayRatemaps
import copy
import statsmodels.stats.descriptivestats
from utils import add_ripples_to_pbes_df
from utils import get_RSI
from session_selection import ratemap_sessions

"""
This is a control where we compute for all the session/phase/direction/ripple
combinations with at least n min_pbe_events_per_group, and in case
there are more events than min_pbe_events_per_group we subselect to 
min_pbe_events_per_group
"""



data_version              = 'dec16'
PBEs_settings_name        = 'dec19k20'
dec_settings_name         = 'DecSet2'
ratemap_settings          = 'CONTROL_jul4_DecSet2_smooth2_morelages'
ratemap_areas             = ['Barrel', 'Perirhinal']
PBEs_area                 = 'Hippocampus'
dec_area                  = 'Hippocampus'



# filter pbes parameters
shuffle_types             = ['column_cycle_shuffle', 'place_field_rotation_shuffle',
                             'unit_identity_shuffle']
shuffle_p_vals            = [0.05, 0.05, 1]
speed_thr_real_rm         = 3
time_bin_size_task_in_ms  = 200
min_pbe_duration_in_ms    = 50
min_pbe_events_per_group  = 10
smooth_ratemaps           = True
smooth_sigma              = 2
correlation_method        = 'pearson'
null_distribution_methods = ['rotate_ratemap', 'rotate_spikes']#['rotate_ratemap', 'rotate_spikes']
n_surrogates              = 100
shift_amounts             = np.arange(-10, 11)
group_iti_and_return      = True

# phase_direction_ripples   = [('all', 'all', 'all'),
#                              ('all', 'positive', 'all'),
#                              ('all', 'negative', 'all'),
#                              ('all', 'all', True),
#                              ('all', 'all', False),
#                              ('iti', 'all', 'all'),
#                              ('img', 'all', 'all'),
#                              ('reward', 'all', 'all'),
#                              ('iti', 'positive', 'all'),
#                              ('iti', 'negative', 'all'),
#                              ('reward', 'positive', 'all'),
#                              ('reward', 'negative', 'all')]

phase_direction_ripples   = [('iti', 'all', 'all'),
                             ('img', 'all', 'all'),
                             ('run', 'all', 'all'),
                             ('reward', 'all', 'all')]



# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

output_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(output_folder):
    os.makedirs(output_folder, exist_ok=True)

# --- RUN ---------------------------------------------------------------------

for ratemap_area in ratemap_areas:

    dfs = []
    #df_n_events = pd.DataFrame(columns=['all', 'pos', 'neg'])

    sessions = ratemap_sessions[ratemap_area]

    # TODO da rimuovere
    sessions = sessions[np.isin(sessions, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 27,
                                           28, 29, 30])]

    for sess_ind in sessions:
        print(sess_ind)
        #pass
        # --- LOAD TASK DATA ------------------------------------------------------
        # Needed to build the ratemaps of the area during normal locomotion
        session_data = unpack_session(sess_ind, data_folder=DATA_FOLDER,
                                      area=ratemap_area,
                                      binsize_in_ms=time_bin_size_task_in_ms,
                                      data_version=data_version)

        speed_real = session_data['speed'].flatten()
        binned_spikes_task = session_data['spikes']
        binned_position_task = session_data['bin']

        binned_spikes_task = binned_spikes_task[speed_real >= speed_thr_real_rm, :]
        binned_position_task = binned_position_task[speed_real >= speed_thr_real_rm]

        trains = session_data['trains']
        n_trains = len(trains)
        # --- COMUPTE UNIT PROPERTIES ---------------------------------------------

        rsi = get_RSI(sess_ind, ratemap_area)

        # --- LOAD PBEs -----------------------------------------------------------


        pbes_data = load_decoded_PBEs(sess_ind=sess_ind,
                                       PBEs_settings_name=PBEs_settings_name,
                                       PBEs_area=PBEs_area,
                                       data_version=data_version,
                                       dec_settings_name=dec_settings_name,
                                       dec_area=dec_area,
                                       load_light=True)
        pbes_df_all = pbes_data['pbes']

        if group_iti_and_return :
            pbes_df_all['phase'] = pbes_df_all['phase'].replace(to_replace='return', value='iti')

        pbes_df_all = add_ripples_to_pbes_df(pbes_df_all)

        sig_df = pbes_data['sig_df']

        bin_size_pbe_in_ms = pbes_data['decode_pars']['bin_size_pbe_in_ms']
        slide_by_pbe_in_ms = pbes_data['decode_pars']['slide_by_pbe_in_ms']
        sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types,
                                                p_vals=shuffle_p_vals)
        pbes_df_all = pbes_df_all[np.isin(pbes_df_all.index, sel_pbes_sig)]
        pbes_df_all = pbes_df_all[pbes_df_all['event_duration_in_ms'] >= min_pbe_duration_in_ms]


        min_events = []
        for task_phase, replay_direction, has_ripple in phase_direction_ripples :
            pbes_df_sel = select_pbes(pbes_df_all=pbes_df_all,
                                      task_phase=task_phase,
                                      replay_direction=replay_direction,
                                      has_ripple=has_ripple)
            print('{} {} {} {}'.format(task_phase, replay_direction, has_ripple,
                                       pbes_df_sel.shape[0]))
            min_events.append(pbes_df_sel.shape[0])


        #df_n_events.loc[sess_ind, :] = [min_events[0], min_events[1],
                                        #min_events[2]]

        # if min(min_events) < min_pbe_events_per_group:
        #     print('Skipping session {}'.format(sess_ind))
        #     continue


        for (task_phase, replay_direction, has_ripple), minev in zip(phase_direction_ripples, min_events):
            print(task_phase, minev)

            if minev < min_pbe_events_per_group:
                print('Skipping {} {} {} of session {}'.format(task_phase, replay_direction, has_ripple, sess_ind))

                continue

            print('Running for session {} task phase: {}, replay direction: {} and with ripple: '
                  '{}'.format(sess_ind, task_phase, replay_direction, has_ripple))

            pbes_df = select_pbes(pbes_df_all=pbes_df_all,
                                  task_phase=task_phase,
                                  replay_direction=replay_direction,
                                  has_ripple=has_ripple)

            # TODO HERE WE RANDOMLY SUBSELECT
            pbes_df = pbes_df.iloc[np.random.choice(np.arange(pbes_df.shape[0]), min_pbe_events_per_group)]



            PBEs_times = [(row['start_time_in_ms'] * pq.ms, row['end_time_in_ms'] * pq.ms)
                          for i, row in pbes_df.iterrows()]

            binned_position_replay = pbes_df['trajectory'].tolist()

            binned_pbes = bin_PBEs(trains, PBEs_times,
                                   bin_size_in_ms=bin_size_pbe_in_ms,
                                   sliding_binning=True,
                                   slide_by_in_ms=slide_by_pbe_in_ms,
                                   pyramidal_only=False)

            binned_spikes_replay = binned_pbes['PBEs_binned']

            for sa in shift_amounts:

                if sa > 0:
                    shifted_bs_replay = [s[sa:] for s in binned_spikes_replay]
                    shifted_pos_replay = [p[:-sa] for p in binned_position_replay]

                elif sa < 0:
                    shifted_bs_replay = [s[:sa] for s in binned_spikes_replay]
                    shifted_pos_replay = [p[-sa:] for p in binned_position_replay]

                elif sa == 0:
                    shifted_bs_replay = copy.copy(binned_spikes_replay)
                    shifted_pos_replay = copy.copy(binned_position_replay)

                # --- MAKE SURE BINNED SPIKES DON'T HAVE ZERO SHAPE ---------------
                shifted_bs_replay_sel, shifted_pos_replay_sel = [], []

                for bs, bp in zip(shifted_bs_replay, shifted_pos_replay):
                    if bs.shape[0] > 0 and bs.shape[1] > 0:
                        shifted_bs_replay_sel.append(bs)
                        shifted_pos_replay_sel.append(bp)

                shifted_bs_replay = shifted_bs_replay_sel
                shifted_pos_replay = shifted_pos_replay_sel

                #assert len(shifted_bs_replay) >= min_pbe_events_per_group

                # --- BUILD RATEMAPS ----------------------------------------------

                rr = ReplayRatemaps(bootstrap=False,
                                    unit_labels=range(len(trains)))

                rr.build_real_ratemaps(binned_spikes_task=binned_spikes_task,
                                       binned_position_task=binned_position_task,
                                       time_bin_size_task_in_ms=time_bin_size_task_in_ms)

                rr.build_event_ratemaps(binned_spikes_replay=shifted_bs_replay,
                                        binned_position_replay=shifted_pos_replay,
                                        time_bin_size_replay_in_ms=bin_size_pbe_in_ms)

                for null_distribution_method in null_distribution_methods:
                    corr = rr.correlate_ratemaps(null_distribution_method=null_distribution_method,
                                                 n_surrogates=n_surrogates,
                                                 correlation_method=correlation_method,
                                                 smooth=smooth_ratemaps, sigma=smooth_sigma)



                    co = corr['corrcoef_obs']
                    cd = corr['corrcoef_debiased']
                    pr = corr['percentile_rank']
                    pv = corr['p_values']
                    sh = np.repeat(sa, co.shape[0])
                    si = np.repeat(sess_ind, co.shape[0])
                    un = np.arange(co.shape[0])
                    ph = np.repeat(task_phase, co.shape[0]).astype(str)
                    di = np.repeat(replay_direction, co.shape[0]).astype(str)
                    hr = np.repeat(has_ripple, co.shape[0]).astype(str)
                    nm = np.repeat(null_distribution_method, co.shape[0]).astype(str)

                    columns = ['sess_ind', 'unit', 'shift_amount', 'phase',
                               'direction', 'null_method', 'rsi', 'corrcoef_obs',
                               'corrcoef_debiased', 'percentile_rank', 'p_values']

                    dfx = pd.DataFrame(columns=columns)
                    dfx['sess_ind'] = si
                    dfx['unit'] = un
                    dfx['shift_amount'] = sh
                    dfx['phase'] = ph
                    dfx['direction'] = di
                    dfx['has_ripple'] = hr
                    dfx['null_method'] = nm
                    dfx['rsi'] = rsi
                    dfx['corrcoef_obs'] = co
                    dfx['corrcoef_debiased'] = cd
                    dfx['percentile_rank'] = pr
                    dfx['p_values'] = pv

                    dfx['ratemap_real'] = pd.Series([i for i in rr.ratemap['real']])
                    dfx['ratemap_event'] = pd.Series([i for i in rr.ratemap['event']])


                    dfs.append(dfx)


    df = pd.concat(dfs)



    pars = {'data_version' : data_version,
            'ratemap_settings' : ratemap_settings,
            'ratemap_area' : ratemap_area,
            'PBEs_area' : PBEs_area,
            'dec_area' : dec_area,
            'PBEs_settings_name' : PBEs_settings_name,
            'dec_settings_name' : dec_settings_name,
            'shuffle_types' : shuffle_types,
            'shuffle_p_vals' : shuffle_p_vals,
            'smooth_ratemaps' : smooth_ratemaps,
            'smooth_sigma' : smooth_sigma,
            'speed_thr_real_rm' : speed_thr_real_rm,
            'time_bin_size_task_in_ms' : time_bin_size_task_in_ms,
            'phase_direction_ripples' : phase_direction_ripples,
            'null_distribution_methods' : null_distribution_methods,
            'n_surrogates' : n_surrogates,
            'shift_amounts' : shift_amounts}




    output = {'pars' : pars,
              'df' : df}

    file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
    full_path = os.path.join(output_folder, file_name)
    print('Saving output to {}'.format(full_path))
    pickle.dump(output, open(full_path, 'wb'))
