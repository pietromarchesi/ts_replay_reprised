import os
from constants import *
import pickle
import statsmodels.stats.descriptivestats
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
from plotting_style import *
from sklearn.preprocessing import MinMaxScaler
from scipy.ndimage import gaussian_filter1d
from utils import *
from scipy.stats import mannwhitneyu, wilcoxon

#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
ratemap_settings = 'jul4_DecSet2_smooth2_morelages'

ratemap_settings = 'newspeedFIXED_8_oct19'
#ratemap_settings = 'dec6_control_2_eq'
#ratemap_settings = 'dec6'

ratemap_area = 'Perirhinal'


contrasts = ['replay_direction',
             'radial_direction',
             'ahead_behind',
             'local_remote',
             'joint_phase',
             'replay_score',
             'event_duration']

contrasts                 = ['replay_direction',
                             'radial_direction',
                             'ahead_behind',
                             'local_remote',
                             'current_trial_outcome',
                             'previous_trial_outcome',]

contrasts = ['replay_direction',
             'local_remote',
             'joint_phase']


#contrasts = ['replay_direction']
             #'current_trial_outcome'


# contrasts = ['replay_direction']


save_plots = True
plot_format = 'svg'
dpi = 400

stat_test = 'sign_test' #'sign_test', 't-test'
sign_alpha = 0.01
p_val_correction = 'fdr_bh'
variable = 'corrcoef_debiased'  # ['corrcoef_obs', 'corrcoef_debiased']
test_value = 0 # [0, 0]
nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
subselect_sessions = None
null_dist_method_for_plot_both_shuff = 'rotate_ratemap'
estimator_barplot = 'mean'

# PEAK TIME DISTRIBUTION
p_val_thr_peak_time_dist = 0.00001

subselect_shifts = False
min_shift = -8
max_shift = 8


# --- SET UP PATHS ------------------------------------------------------------
if ratemap_settings == 'newspeedFIXED_8_oct19':
    plot_folder = os.path.join(PLOTS_FOLDER, 'speed8', plot_format, 'RERM',
                           null_dist_method_for_plot_both_shuff)
else:
    plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'RERM', ratemap_settings,
                           null_dist_method_for_plot_both_shuff)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# for contrast in contrasts:
#     # --- LOAD RESULTS ------------------------------------------------------------
#     res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps_contrasts', ratemap_settings)
#     file_name = 'replayratemaps_{}_{}_{}.pkl'.format(ratemap_settings, ratemap_area, contrast)
#     res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
#     df = res['df']
#     pars = res['pars']
#     shift_amounts = pars['shift_amounts']
#     smoothing_sigma = pars['smooth_sigma']
#
#
#     if subselect_sessions is not None:
#         df = df[np.isin(df['sess_ind'], subselect_sessions)]
#
#     if subselect_shifts:
#         df = df[df['shift_amount'] >= min_shift]
#         df = df[df['shift_amount'] <= max_shift]
#         shift_amounts = np.arange(min_shift, max_shift+1)
#
#     df['contrast'] = df['contrast'].replace('np', 'no')
#
#     assert df['contrast'].unique().shape[0] == 2
#
#     name1, name2 = np.sort(df['contrast'].unique())
#
#     palette = {name1 : sns.xkcd_rgb['forest green'],
#                name2 : sns.xkcd_rgb['ultramarine blue'],
#                'difference' : sns.xkcd_rgb['grey']}
#
#     df1 = df[df['contrast'] == name1]
#     df2 = df[df['contrast'] == name2]
#
#
#     # --- MAKE TIME SHIFT PLOT -------------------------------------------------
#
#     # make the masks using both shuffles
#     masks = {name1 : [],
#              name2 : [],
#              'difference' : []}
#
#     for null_distribution_method in nulldistmet_plots:
#         df_split = {}
#         for dfz, name in zip([df1, df2], [name1, name2]):
#             #print(dfz)
#             dfx = dfz[(dfz['null_method'] == null_distribution_method)]
#             mask, pvals_corr = get_significance_mask(dfx, correction=p_val_correction,
#                                                      variable=variable,
#                                                      stat_test=stat_test,
#                                                      test_value=test_value,
#                                                      sign_alpha=sign_alpha)
#             masks[name].append(mask)
#             df_split[name] = dfx
#
#         # TODO if the units are the same I can use paired test!
#         mask, pvals_corr = get_significance_mask_split(df_split[name1],
#                                                        df_split[name2],
#                                                  correction=p_val_correction,
#                                                  variable=variable,
#                                                  stat_test=stat_test,
#                                                  sign_alpha=sign_alpha)
#         masks['difference'].append(mask)
#     # combine masks
#     masks[name1] = np.logical_and(masks[name1][0], masks[name1][1])
#     masks[name2] = np.logical_and(masks[name2][0], masks[name2][1])
#     masks['difference'] = np.logical_and(masks['difference'][0], masks['difference'][1])
#
#     f, ax = plt.subplots(1, 1, figsize=[5, 3])
#     dfx1 = df1[df1['null_method'] == null_dist_method_for_plot_both_shuff]
#     dfx2 = df2[df2['null_method'] == null_dist_method_for_plot_both_shuff]
#
#     for dfplot, name in zip([dfx1, dfx2], [name1, name2]):
#         sns.lineplot(data=dfplot, x='shift_amount', y=variable, ax=ax,
#                      color=palette[name], label=name, estimator=np.median,
#                      markers=True, style='null_method', markersize=5)
#     for i, (name, mask) in enumerate(masks.items()):
#         height = ax.get_ylim()[0] + 0.02 * i
#         signif_line = np.ma.masked_where(~mask, np.repeat(height, len(shift_amounts)))
#         ax.scatter(shift_amounts, signif_line, color=palette[name],
#                 lw=1,alpha=1)
#
#     colors = [palette[name1], palette[name2]]
#     texts = [name1, name2]
#     patches = [plt.plot([], [], marker="o", ms=10, ls="", mec=None, color=colors[i],
#                  label="{:s}".format(texts[i]))[0] for i in range(len(texts))]
#     ax.legend(handles=patches, frameon=False,
#               bbox_to_anchor=(1.04, 1), loc="upper left")
#
#     ax.set_ylabel('{} replay coordination strength $(r_d)$'.format(ratemap_area_sigla[ratemap_area]))
#     ax.set_xlabel('Time shift [ms]')
#     ax.set_xticks([-10, -5, 0, 5, 10])
#     ax.set_xticklabels([-100, -50, 0, 50, 100])
#     #ax.get_legend().remove()
#     ax.axhline(test_value, c='grey', ls='--', lw=2, zorder=-1)
#     sns.despine()
#     plt.tight_layout()
#
#     if save_plots:
#         plot_name = 'ratemap_shift_by_{}_BOTHSHUF_{}_{}_{}.' \
#                     '{}'.format(ratemap_area, contrast, variable, null_dist_method_for_plot_both_shuff, plot_format)
#         f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#



# --- PLOT BARS ------------------------------------------------------------

f, axes = plt.subplots(1, 3, figsize=[1.8*1.7, 3], sharey=True)


for ax, contrast in zip(axes, contrasts):
    # --- LOAD RESULTS ------------------------------------------------------------
    res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps_contrasts', ratemap_settings)
    file_name = 'replayratemaps_{}_{}_{}.pkl'.format(ratemap_settings,
                                                     ratemap_area, contrast)
    res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
    df = res['df']
    pars = res['pars']
    shift_amounts = pars['shift_amounts']
    smoothing_sigma = pars['smooth_sigma']

    if estimator_barplot == 'median':
        func = np.median
    elif estimator_barplot == 'mean':
        func = np.mean

    dfx = df[df['null_method'] == null_dist_method_for_plot_both_shuff]
    dfx = dfx[dfx['shift_amount'] == 0]

    #dfx = dfx[dfx[variable] >= -0.05]

    sns.barplot(data=dfx, x='contrast', y=variable, ax=ax,
                order=contrast_labels[contrast].keys(),
                estimator=func)
    labls = [contrast_labels[contrast][t._text] for t in ax.get_xticklabels()]

    plt.setp(ax.patches[0], edgecolor=sns.xkcd_rgb['dark grey'], facecolor='w',
             linewidth=1.5)
    plt.setp(ax.patches[1], edgecolor=sns.xkcd_rgb['dark grey'],
             facecolor=sns.xkcd_rgb['grey'], linewidth=1.5)

    plt.setp(ax.lines, color=sns.xkcd_rgb['dark grey'])

    ax.set_xticklabels(labls, rotation=45)
    if contrast == 'joint_phase':
        ax.tick_params(axis='x', which='major', labelsize=8)
    #
    # if variable == 'percentile_rank':
    #     ax.set_yticks([0, 20, 40, 60])
    #     ax.set_ylim([0, 75])
    # else:
    #     if null_dist_method_for_plot_both_shuff == 'rotate_spikes':
    #         ax.set_yticks([-0.05, 0, 0.05, 0.1])
    #         ax.set_ylim([-0.08, 0.13])
    #     elif null_dist_method_for_plot_both_shuff == 'rotate_ratemap':
    #         ax.set_yticks([-0, 0.05, 0.1, 0.15, 0.2])
    #         ax.set_ylim([-0.02, 0.22])
    ax.set_xlabel('')
    #if contrast == 'replay_direction':


    # --- BARPLOT STATISTICS ---------------------------------------------------
    name1 = list(contrast_labels[contrast].keys())[0]
    name2 = list(contrast_labels[contrast].keys())[1]
    df1 = dfx[dfx['contrast'] == name1]
    df2 = dfx[dfx['contrast'] == name2]

    x = df1[variable]
    y = df2[variable]

    # This checks whether the data is actually paired
    # if df1['sess_ind'].equals(df2['sess_ind']) and df1['unit'].equals(df2['unit']):
    #     stat, pval = wilcoxon(x, y)
    #     print('{} vs {}: p={:.1g}, Wilcoxon signed-rank test'.format(name1, name2, pval))
    #
    # else:
    #     stat, pval = mannwhitneyu(x, y)
    #     print('{} vs {}: p={:.1g}, Mann-Whitney U-test'.format(name1, name2, pval))

    # let's use mann whitney as we cannot be sure, it will just be less powerful
    stat, pval = mannwhitneyu(x, y)
    print('\n{} vs {}: p={:.1g}, Mann-Whitney U-test'.format(name1, name2, pval))

    is_sig_05 = pval<0.05/len(contrasts)
    is_sig_01 = pval<0.01/len(contrasts)
    is_sig_001 = pval<0.001/len(contrasts)

    print('significant Bonferroni corrected for {} tests: '
          '\n0.05 {}\n0.01 {}\n0.001 {}'.format(len(contrasts), is_sig_05, is_sig_01, is_sig_001))


axes[1].set_ylabel('')
axes[2].set_ylabel('')
axes[0].set_ylabel('{} replay coordination\nstrength ($r_d$)'.format(ratemap_area_sigla[ratemap_area]))
sns.despine()
plt.tight_layout()

if save_plots:
    plot_name = 'barplot_at_shift_0_{}_{}_{}_estimator_{}.' \
                '{}'.format(ratemap_area, null_dist_method_for_plot_both_shuff, variable, estimator_barplot, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


