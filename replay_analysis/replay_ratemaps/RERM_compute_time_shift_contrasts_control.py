import matplotlib.pyplot as plt
from constants import *
from utils import *
import quantities as pq
from plotting_style import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from replaydetector.replayratemaps import ReplayRatemaps
import copy
import statsmodels.stats.descriptivestats
from utils import *
from session_selection import ratemap_sessions

data_version              = 'dec16'
PBEs_settings_name        = 'dec19k20'
dec_settings_name         = 'DecSet2'
#ratemap_settings          = 'jul4_DecSet2_smooth2_morelages'

#ratemap_settings          = 'dec6_control_2_eq'

ratemap_settings          = 'newspeedFIXED_8_oct19_controlEQ'


ratemap_areas             = ['Perirhinal']
PBEs_area                 = 'Hippocampus'
dec_area                  = 'Hippocampus'


# filter pbes parameters
shuffle_types             = ['column_cycle_shuffle', 'place_field_rotation_shuffle',
                             'unit_identity_shuffle']
shuffle_p_vals            = [0.05, 0.05, 1]
speed_thr_real_rm         = 3
time_bin_size_task_in_ms  = 200
min_pbe_duration_in_ms    = 50
min_pbe_events_per_group  = 20
smooth_ratemaps           = True
smooth_sigma              = 2
correlation_method        = 'pearson'
null_distribution_methods = ['rotate_ratemap', 'rotate_spikes']#['rotate_ratemap', 'rotate_spikes']
n_surrogates              = 100
shift_amounts             = np.arange(-10, 11)

new_speed                 = True
speed_threshold_pbes      = 8
time_before_in_ms         = 0
time_after_in_ms          = 0
position_sigma            = 10
binsize_interp_pos        = 50

group_iti_and_return      = True

contrasts                 = ['replay_direction',
                             'radial_direction',
                             #'ahead_behind',
                             'local_remote',
                             'current_trial_outcome']
                             #'previous_trial_outcome',]

contrasts                 = ['replay_direction',
                             'local_remote',
                             'joint_phase']

#contrasts = ['replay_direction']


# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(REPLAY_FOLDER, 'plots', 'replay_ratemaps', ratemap_settings)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

output_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps_contrasts', ratemap_settings)

if not os.path.isdir(output_folder):
    os.makedirs(output_folder, exist_ok=True)

# --- RUN ---------------------------------------------------------------------
for contrast in contrasts:
    for ratemap_area in ratemap_areas:

        dfs = []
        #df_n_events = pd.DataFrame(columns=['all', 'pos', 'neg'])

        sessions = ratemap_sessions[ratemap_area]

        # TODO da rimuovere
        sessions = sessions[np.isin(sessions, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                                               10, 11, 27, 28, 29, 30])]

        for sess_ind in sessions:
            print('\n {} \n'.format(sess_ind))
            #pass
            # --- LOAD TASK DATA ------------------------------------------------------
            # Needed to build the ratemaps of the area during normal locomotion
            session_data = unpack_session(sess_ind, data_folder=DATA_FOLDER,
                                          area=ratemap_area,
                                          binsize_in_ms=time_bin_size_task_in_ms,
                                          data_version=data_version)

            speed_real = session_data['speed'].flatten()
            binned_spikes_task = session_data['spikes']
            binned_position_task = session_data['bin']

            binned_spikes_task = binned_spikes_task[speed_real >= speed_thr_real_rm, :]
            binned_position_task = binned_position_task[speed_real >= speed_thr_real_rm]

            trains = session_data['trains']
            n_trains = len(trains)
            # --- COMUPTE UNIT PROPERTIES ---------------------------------------------

            rsi = get_RSI(sess_ind, ratemap_area)

            # --- LOAD PBEs -----------------------------------------------------------

            pbes_data = load_decoded_PBEs(sess_ind=sess_ind,
                                           PBEs_settings_name=PBEs_settings_name,
                                           PBEs_area=PBEs_area,
                                           data_version=data_version,
                                           dec_settings_name=dec_settings_name,
                                           dec_area=dec_area,
                                           load_light=True)
            pbes_df_all = pbes_data['pbes']

            if group_iti_and_return :
                pbes_df_all['phase'] = pbes_df_all['phase'].replace(to_replace='return', value='iti')

            if new_speed :
                pbes_df_all = add_new_speed(pbes_df_all,
                                            time_before_in_ms=time_before_in_ms,
                                            time_after_in_ms=time_after_in_ms,
                                            binsize_interp_pos=binsize_interp_pos,
                                            position_sigma=position_sigma,
                                            data_version=data_version,
                                            column_name='average_speed')

            pbes_df_all = pbes_df_all[pbes_df_all['average_speed'] <= speed_threshold_pbes]


            pbes_df_all = add_ripples_to_pbes_df(pbes_df_all)
            sig_df = pbes_data['sig_df']

            bin_size_pbe_in_ms = pbes_data['decode_pars']['bin_size_pbe_in_ms']
            slide_by_pbe_in_ms = pbes_data['decode_pars']['slide_by_pbe_in_ms']
            sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types,
                                                    p_vals=shuffle_p_vals)
            pbes_df_all = pbes_df_all[np.isin(pbes_df_all.index, sel_pbes_sig)]
            pbes_df_all = pbes_df_all[pbes_df_all['event_duration_in_ms'] >= min_pbe_duration_in_ms]

            if contrast == 'radial_direction':
                pbes_df_all = add_radial_direction(pbes_df_all)

            if contrast == 'ahead_behind':
                pbes_df_all = add_ahead_behind(pbes_df_all)

            if contrast == 'local_remote':
                pbes_df_all = add_local_remote(pbes_df_all)

            if contrast == 'replay_score':
                pbes_df_all = add_binned_replay_score(pbes_df_all)

            if contrast == 'event_duration':
                pbes_df_all = add_binned_event_duration(pbes_df_all)

            if contrast == 'current_trial_outcome' or contrast == 'previous_trial_outcome' :
                pbes_df_all = add_correct_incorrect(pbes_df_all.copy(), data_version)

            # --- SPLIT FOR CONTRAST ---

            pbes_dfs, names = get_pbes_contrast(pbes_df_all, contrast)

            min_events = []
            for pbes_df_sel, name in zip(pbes_dfs, names):
                print('{} - {}, {}'.format(contrast, name, pbes_df_sel.shape[0]))
                min_events.append(pbes_df_sel.shape[0])

            if min(min_events) < min_pbe_events_per_group:
                print('SKIPPING')
                continue

            for pbes_df, name, minev in zip(pbes_dfs, names, min_events):

                print('EQUALIZING UNITS')
                pbes_df = pbes_df.iloc[np.random.choice(np.arange(pbes_df.shape[0]), min(min_events))]
                print('RUNNING for {} - {}, {}'.format(contrast, name, pbes_df.shape[0]))


                PBEs_times = [(row['start_time_in_ms'] * pq.ms, row['end_time_in_ms'] * pq.ms)
                              for i, row in pbes_df.iterrows()]

                binned_position_replay = pbes_df['trajectory'].tolist()

                binned_pbes = bin_PBEs(trains, PBEs_times,
                                       bin_size_in_ms=bin_size_pbe_in_ms,
                                       sliding_binning=True,
                                       slide_by_in_ms=slide_by_pbe_in_ms,
                                       pyramidal_only=False)

                binned_spikes_replay = binned_pbes['PBEs_binned']

                for sa in shift_amounts:

                    if sa > 0:
                        shifted_bs_replay = [s[sa:] for s in binned_spikes_replay]
                        shifted_pos_replay = [p[:-sa] for p in binned_position_replay]

                    elif sa < 0:
                        shifted_bs_replay = [s[:sa] for s in binned_spikes_replay]
                        shifted_pos_replay = [p[-sa:] for p in binned_position_replay]

                    elif sa == 0:
                        shifted_bs_replay = copy.copy(binned_spikes_replay)
                        shifted_pos_replay = copy.copy(binned_position_replay)

                    # --- MAKE SURE BINNED SPIKES DON'T HAVE ZERO SHAPE ---------------
                    shifted_bs_replay_sel, shifted_pos_replay_sel = [], []

                    for bs, bp in zip(shifted_bs_replay, shifted_pos_replay):
                        if bs.shape[0] > 0 and bs.shape[1] > 0:
                            shifted_bs_replay_sel.append(bs)
                            shifted_pos_replay_sel.append(bp)

                    shifted_bs_replay = shifted_bs_replay_sel
                    shifted_pos_replay = shifted_pos_replay_sel

                    #assert len(shifted_bs_replay) >= min_pbe_events_per_group

                    # --- BUILD RATEMAPS ----------------------------------------------

                    rr = ReplayRatemaps(bootstrap=False,
                                        unit_labels=range(len(trains)))

                    rr.build_real_ratemaps(binned_spikes_task=binned_spikes_task,
                                           binned_position_task=binned_position_task,
                                           time_bin_size_task_in_ms=time_bin_size_task_in_ms)

                    rr.build_event_ratemaps(binned_spikes_replay=shifted_bs_replay,
                                            binned_position_replay=shifted_pos_replay,
                                            time_bin_size_replay_in_ms=bin_size_pbe_in_ms)

                    for null_distribution_method in null_distribution_methods:
                        corr = rr.correlate_ratemaps(null_distribution_method=null_distribution_method,
                                                     n_surrogates=n_surrogates,
                                                     correlation_method=correlation_method,
                                                     smooth=smooth_ratemaps, sigma=smooth_sigma)



                        co = corr['corrcoef_obs']
                        cd = corr['corrcoef_debiased']
                        pr = corr['percentile_rank']
                        pv = corr['p_values']
                        sh = np.repeat(sa, co.shape[0])
                        si = np.repeat(sess_ind, co.shape[0])
                        un = np.arange(co.shape[0])
                        ct = np.repeat(name, co.shape[0]).astype(str)
                        nm = np.repeat(null_distribution_method, co.shape[0]).astype(str)

                        columns = ['sess_ind', 'unit', 'shift_amount',
                                   'null_method', 'rsi', 'corrcoef_obs',
                                   'corrcoef_debiased', 'percentile_rank', 'p_values']

                        dfx = pd.DataFrame(columns=columns)
                        dfx['sess_ind'] = si
                        dfx['unit'] = un
                        dfx['shift_amount'] = sh
                        dfx['contrast'] = ct
                        dfx['null_method'] = nm
                        dfx['rsi'] = rsi
                        dfx['corrcoef_obs'] = co
                        dfx['corrcoef_debiased'] = cd
                        dfx['percentile_rank'] = pr
                        dfx['p_values'] = pv
                        dfx['ratemap_real'] = pd.Series([i for i in rr.ratemap['real']])
                        dfx['ratemap_event'] = pd.Series([i for i in rr.ratemap['event']])


                        dfs.append(dfx)


        df = pd.concat(dfs)

        pars = {'data_version' : data_version,
                'ratemap_settings' : ratemap_settings,
                'ratemap_area' : ratemap_area,
                'PBEs_area' : PBEs_area,
                'dec_area' : dec_area,
                'PBEs_settings_name' : PBEs_settings_name,
                'dec_settings_name' : dec_settings_name,
                'shuffle_types' : shuffle_types,
                'shuffle_p_vals' : shuffle_p_vals,
                'smooth_ratemaps' : smooth_ratemaps,
                'smooth_sigma' : smooth_sigma,
                'speed_thr_real_rm' : speed_thr_real_rm,
                'time_bin_size_task_in_ms' : time_bin_size_task_in_ms,
                'null_distribution_methods' : null_distribution_methods,
                'n_surrogates' : n_surrogates,
                'shift_amounts' : shift_amounts}


        output = {'pars' : pars,
                  'df' : df}

        file_name = 'replayratemaps_{}_{}_{}.pkl'.format(ratemap_settings, ratemap_area, contrast)
        full_path = os.path.join(output_folder, file_name)
        print('Saving output to {}'.format(full_path))
        pickle.dump(output, open(full_path, 'wb'))
