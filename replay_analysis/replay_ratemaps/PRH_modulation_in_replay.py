import matplotlib.pyplot as plt
from constants import *
from utils import *
import quantities as pq
from plotting_style import *
from replaydetector.replay_utils import filter_pbes, filter_pbes_significance
from replaydetector.replayratemaps import ReplayRatemaps
import copy
import statsmodels.stats.descriptivestats
from utils import *
from session_selection import ratemap_sessions

data_version              = 'dec16'
PBEs_settings_name        = 'dec19k20'
dec_settings_name         = 'DecSet2'

cortical_areas            = ['Barrel', 'Perirhinal']
hippocampus               = 'Hippocampus'
dec_area                  = 'Hippocampus'

# filter pbes parameters
shuffle_types             = ['column_cycle_shuffle', 'place_field_rotation_shuffle',
                             'unit_identity_shuffle']
shuffle_p_vals            = [0.0025, 0.0025, 0.0025]

time_bin_size_task_in_ms  = 200
min_pbe_duration_in_ms    = 50
min_pbe_events_per_group  = 5

only_remote_events        = False
only_local_events         = False
group_iti_and_return      = True

new_speed                 = True
speed_threshold_pbes      = 3
time_before_in_ms         = 0
time_after_in_ms          = 0
position_sigma            = 10
binsize_interp_pos        = 50

theta_power               = False

phase_direction_ripples   = [('all', 'all', 'all'),
                             ('all', 'positive', 'all'),
                             ('all', 'negative', 'all')]

phase_direction_ripples   = [('all', 'all', 'all'),
                             ('all', 'positive', 'all'),
                             ('all', 'negative', 'all')]


phase_direction_ripples   = [('iti', 'all', 'all'),
                             ('img', 'all', 'all'),
                             ('run', 'all', 'all'),
                             ('reward', 'all', 'all')]

phase_direction_ripples   = [('all', 'all', True),
                             ('all', 'all', False)]

phase_direction_ripples   = [('iti', 'positive', 'all'),
                             ('img', 'positive', 'all'),
                             ('run', 'positive', 'all'),
                             ('reward', 'positive', 'all'),
                             ('iti', 'negative', 'all'),
                             ('img', 'negative', 'all'),
                             ('run', 'negative', 'all'),
                             ('reward', 'negative', 'all')]


# --- RUN ---------------------------------------------------------------------

if theta_power :

    from constants import excluded_ids

columns = ['sess_ind', 'unit', 'area', 'task_phase',
           'replay_direction', 'ripple', 'n_spikes',
           'pbe_duration_in_s', 'firing_rate_Hz']

df = pd.DataFrame(columns=columns)


for ratemap_area in cortical_areas:

    sessions = ratemap_sessions[ratemap_area]

    # TODO da rimuovere
    sessions = sessions[np.isin(sessions, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 27, 28, 29, 30])]

    for sess_ind in sessions:
        print('\n SESSION {} \n'.format(sess_ind))
        #pass
        # --- LOAD TASK DATA ------------------------------------------------------
        session_data = unpack_session(sess_ind, data_folder=DATA_FOLDER,
                                      area=ratemap_area,
                                      binsize_in_ms=time_bin_size_task_in_ms,
                                      data_version=data_version)

        cortical_trains = session_data['trains']

        # --- LOAD REPLAY ------------------------------------------------------

        pbes_data = load_decoded_PBEs(sess_ind=sess_ind,
                                      PBEs_settings_name=PBEs_settings_name,
                                      PBEs_area=hippocampus,
                                      data_version=data_version,
                                      dec_settings_name=dec_settings_name,
                                      dec_area=dec_area,
                                      load_light=True)

        pbes_df_all = pbes_data['pbes']

        if new_speed:
            pbes_df_all = add_new_speed(pbes_df_all, time_before_in_ms=time_before_in_ms,
                                        time_after_in_ms=time_after_in_ms,
                                        binsize_interp_pos=binsize_interp_pos,
                                        position_sigma=position_sigma,
                                        data_version=data_version,
                                        column_name='average_speed')

        # if theta_power:
        #     # pbes_df_all = pd.merge(pbes_df_all, theta[['theta_power', 'sig_theta']], left_index=True,
        #     #          right_index=True, how='inner')
        #
        #     # pbes_df_all = pbes_df_all[pbes_df_all['sig_theta'] == False]
        #
        #     excluded_mask = pbes_df_all.index.isin(excluded_ids)
        #
        #     pbes_df_all = pbes_df_all[~excluded_mask]

        if group_iti_and_return :
            pbes_df_all['phase'] = pbes_df_all['phase'].replace(to_replace='return', value='iti')

        pbes_df_all = add_ripples_to_pbes_df(pbes_df_all)

        sig_df = pbes_data['sig_df']

        bin_size_pbe_in_ms = pbes_data['decode_pars']['bin_size_pbe_in_ms']
        slide_by_pbe_in_ms = pbes_data['decode_pars']['slide_by_pbe_in_ms']
        sel_pbes_sig = filter_pbes_significance(sig_df, shuffles=shuffle_types,
                                                p_vals=shuffle_p_vals)
        pbes_df_all = pbes_df_all[np.isin(pbes_df_all.index, sel_pbes_sig)]
        pbes_df_all = pbes_df_all[pbes_df_all['event_duration_in_ms'] >= min_pbe_duration_in_ms]

        pbes_df_all = pbes_df_all[pbes_df_all['average_speed'] <= speed_threshold_pbes]



        for task_phase, replay_direction, has_ripple in phase_direction_ripples:

            print('\nRunning for session {} task phase: {}, replay direction: {} and with ripple: '
                  '{}'.format(sess_ind, task_phase, replay_direction, has_ripple))

            # --- SELECT REPLAY FOR GIVEN SUBSET -------------------------------
            pbes_df = select_pbes(pbes_df_all=pbes_df_all,
                                  task_phase=task_phase,
                                  replay_direction=replay_direction,
                                  has_ripple=has_ripple)

            if pbes_df.shape[0] < min_pbe_events_per_group:
                print('Skipping {} {} {} of session {}'.format(task_phase, replay_direction, has_ripple, sess_ind))
                continue

            print('#pbes: {}'.format(pbes_df.shape[0]))

            PBEs_times = [(row['start_time_in_ms'] * pq.ms, row['end_time_in_ms'] * pq.ms)
                          for i, row in pbes_df.iterrows()]

            # --- BIN CORTICAL SPIKES USING REPLAY TIMES -----------------------

            for unit_ind, train in enumerate(cortical_trains):

                unit_id  = '{}_{}_{}'.format(sess_ind, ratemap_area, unit_ind)

                for t_start, t_stop in PBEs_times:

                    n_spikes = train[np.logical_and(train>=t_start, train<=t_stop)].shape[0]

                    pbe_duration_in_s = (t_stop - t_start).rescale(pq.s).item()

                    firing_rate = n_spikes / pbe_duration_in_s

                    row = [sess_ind, unit_id, ratemap_area, task_phase,
                           replay_direction, has_ripple, n_spikes,
                           pbe_duration_in_s, firing_rate]

                    df.loc[df.shape[0], :] = row


for col in ['n_spikes', 'pbe_duration_in_s', 'firing_rate_Hz']:
    df[col] = pd.to_numeric(df[col])


file_name = 'firing_rate_replay.pkl'.format()
full_path = os.path.join('/Users/pietro/data/ts_replay_reprised/results', file_name)
print('Saving output to {}'.format(full_path))
pickle.dump(df, open(full_path, 'wb'))



plot_folder = os.path.join(PLOTS_FOLDER, 'firing_rate_replay')
if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)





f, ax = plt.subplots(1, 2, figsize=[8, 5])
ax[0].set_title('PER')
ax[1].set_title('S1BF')
df1 = df[df['area'] == 'Perirhinal']
df2 = df[df['area'] == 'Barrel']

df1x = df1.groupby(['unit', 'area',  'task_phase', 'replay_direction']).mean().reset_index()
df2x = df2.groupby(['unit', 'area',  'task_phase', 'replay_direction']).mean().reset_index()

sns.barplot(data=df1x, x='task_phase', y='firing_rate_Hz',
            hue='replay_direction', ax=ax[0])
sns.barplot(data=df2x, x='task_phase', y='firing_rate_Hz',
            hue='replay_direction', ax=ax[1])
sns.despine()
plt.tight_layout()

plot_name = 'firing_rate_replay_barplot_phase_direction.png'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)


f, ax = plt.subplots(1, 2, figsize=[8, 5])
ax[0].set_title('PER')
ax[1].set_title('S1BF')
df1 = df[df['area'] == 'Perirhinal']
df2 = df[df['area'] == 'Barrel']

df1x = df1.groupby(['unit', 'area',  'task_phase', 'replay_direction']).mean().reset_index()
df2x = df2.groupby(['unit', 'area',  'task_phase', 'replay_direction']).mean().reset_index()

sns.swarmplot(data=df1x, x='task_phase', y='firing_rate_Hz',
            hue='replay_direction', ax=ax[0],  size=3, dodge=True)
sns.swarmplot(data=df2x, x='task_phase', y='firing_rate_Hz',
            hue='replay_direction', ax=ax[1], size=3, dodge=True)
sns.despine()
plt.tight_layout()

plot_name = 'firing_rate_replay_swarmplot_phase_direction.png'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)







f, ax = plt.subplots(1, 2, figsize=[9, 5])
ax[0].set_title('PER')
ax[1].set_title('S1BF')
df1 = df[(df['area'] == 'Perirhinal') & (df['replay_direction'] == 'all')]
df2 = df[(df['area'] == 'Barrel') & (df['replay_direction'] == 'all')]

df1x = df1.groupby(['unit', 'area',  'task_phase']).mean().reset_index()
df2x = df2.groupby(['unit', 'area',  'task_phase']).mean().reset_index()


sns.barplot(data=df1x, x='task_phase', y='firing_rate_Hz', ax=ax[0])
sns.barplot(data=df2x, x='task_phase', y='firing_rate_Hz', ax=ax[1])

sns.despine()
plt.tight_layout()

plot_name = 'firing_rate_replay_bars_phase.png'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)



f, ax = plt.subplots(1, 2, figsize=[9, 5])
ax[0].set_title('PER')
ax[1].set_title('S1BF')
df1 = df[(df['area'] == 'Perirhinal') & (df['replay_direction'] == 'all')]
df2 = df[(df['area'] == 'Barrel') & (df['replay_direction'] == 'all')]

df1x = df1.groupby(['unit', 'area',  'task_phase']).mean().reset_index()
df2x = df2.groupby(['unit', 'area',  'task_phase']).mean().reset_index()

sns.swarmplot(data=df1x, x='task_phase', y='firing_rate_Hz', ax=ax[0], size=3)
sns.swarmplot(data=df2x, x='task_phase', y='firing_rate_Hz', ax=ax[1], size=3)

sns.despine()
plt.tight_layout()

plot_name = 'firing_rate_replay_swarm_phase.png'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)





f, ax = plt.subplots(1, 2, figsize=[9, 5])
ax[0].set_title('PER')
ax[1].set_title('S1BF')
df1 = df[(df['area'] == 'Perirhinal') & (df['replay_direction'] == 'all')]
df2 = df[(df['area'] == 'Barrel') & (df['replay_direction'] == 'all')]

df1x = df1.groupby(['unit', 'area',  'ripple']).mean().reset_index()
df2x = df2.groupby(['unit', 'area',  'ripple']).mean().reset_index()

sns.swarmplot(data=df1x, x='ripple', y='firing_rate_Hz', ax=ax[0], size=3)
sns.swarmplot(data=df2x, x='ripple', y='firing_rate_Hz', ax=ax[1], size=3)

sns.despine()
plt.tight_layout()

plot_name = 'firing_rate_replay_swarm_ripple.png'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)


f, ax = plt.subplots(1, 2, figsize=[9, 5])
ax[0].set_title('PER')
ax[1].set_title('S1BF')
df1 = df[(df['area'] == 'Perirhinal') & (df['replay_direction'] == 'all')]
df2 = df[(df['area'] == 'Barrel') & (df['replay_direction'] == 'all')]

df1x = df1.groupby(['unit', 'area',  'ripple']).mean().reset_index()
df2x = df2.groupby(['unit', 'area',  'ripple']).mean().reset_index()

sns.barplot(data=df1x, x='ripple', y='firing_rate_Hz', ax=ax[0])
sns.barplot(data=df2x, x='ripple', y='firing_rate_Hz', ax=ax[1])

sns.despine()
plt.tight_layout()

plot_name = 'firing_rate_replay_bar_ripple.png'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)











f, ax = plt.subplots(1, 2, figsize=[7, 5])
ax[0].set_title('PER')
ax[1].set_title('S1BF')
df1 = df[(df['area'] == 'Perirhinal') & (df['task_phase'] == 'all')]
df2 = df[(df['area'] == 'Barrel') & (df['task_phase'] == 'all')]

df1x = df1.groupby(['unit', 'area',  'replay_direction']).mean().reset_index()
df2x = df2.groupby(['unit', 'area',  'replay_direction']).mean().reset_index()

sns.swarmplot(data=df1x, x='replay_direction', y='firing_rate_Hz', ax=ax[0], size=4)
sns.swarmplot(data=df2x, x='replay_direction', y='firing_rate_Hz', ax=ax[1], size=4)

sns.despine()
plt.tight_layout()

plot_name = 'firing_rate_replay_swarm_direction.png'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)






# df = pd.concat(dfs)
#
# pars = {'data_version' : data_version,
#         'ratemap_settings' : ratemap_settings,
#         'ratemap_area' : ratemap_area,
#         'PBEs_area' : PBEs_area,
#         'dec_area' : dec_area,
#         'speed_threshold_pbes' : speed_threshold_pbes,
#         'new_speed' : new_speed,
#         'time_before_in_ms' : time_before_in_ms,
#         'time_after_in_ms' : time_after_in_ms,
#         'position_sigma' : position_sigma,
#         'binsize_interp_pos' : binsize_interp_pos,
#         'PBEs_settings_name' : PBEs_settings_name,
#         'dec_settings_name' : dec_settings_name,
#         'shuffle_types' : shuffle_types,
#         'shuffle_p_vals' : shuffle_p_vals,
#         'smooth_ratemaps' : smooth_ratemaps,
#         'smooth_sigma' : smooth_sigma,
#         'speed_thr_real_rm' : speed_thr_real_rm,
#         'time_bin_size_task_in_ms' : time_bin_size_task_in_ms,
#         'phase_direction_ripples' : phase_direction_ripples,
#         'null_distribution_methods' : null_distribution_methods,
#         'n_surrogates' : n_surrogates,
#         'shift_amounts' : shift_amounts,
#         'only_remote_events' : only_remote_events,
#         'only_local_events' : only_local_events}
#
# output = {'pars' : pars,
#           'df' : df}
#
#
# file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
# full_path = os.path.join(output_folder, file_name)
# print('Saving output to {}'.format(full_path))
# pickle.dump(output, open(full_path, 'wb'))
