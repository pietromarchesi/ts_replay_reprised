import os
from constants import *
import pickle
import statsmodels.stats.descriptivestats
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
from plotting_style import *
from utils import *
from scipy.stats import mannwhitneyu, wilcoxon
import matplotlib.patches as mpatches
from matplotlib.colors import LinearSegmentedColormap

ratemap_settings = 'dec6'
#ratemap_settings = 'jul4_DecSet2_smooth2_morelages'


save_plots = True
plot_format = 'png'
dpi = 400

p_val_thr = 0.05
bonferroni_correct = False

stat_test = 'sign_test' #'sign_test', 't-test'
sign_alpha = 0.01
p_val_correction = 'fdr_bh'
variable = 'corrcoef_debiased'  # ['corrcoef_obs', 'corrcoef_debiased']
test_value = 0 # [0, 0]
nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
subselect_sessions = None
null_dist_method_for_plot_both_shuff = 'rotate_spikes'

contrasts = ['replay_direction',
             'joint_phase',
             'local_remote']


# --- SET UP PATHS ------------------------------------------------------------
if ratemap_settings == 'newspeedFIXED_8_oct19':
    plot_folder = os.path.join(PLOTS_FOLDER, 'speed8', plot_format, 'RERM',
                           null_dist_method_for_plot_both_shuff)
else:
    plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'RERM', ratemap_settings,
                           null_dist_method_for_plot_both_shuff)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)


ratemap_areas = ['Perirhinal', 'Barrel']


# --- LOAD RESULTS COMPUTE FOR ALL EVENTS --------------------------------------

dfs = []
for ratemap_area in ratemap_areas :
    res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps',
                              ratemap_settings)
    file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings,
                                                  ratemap_area)
    res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
    df = res['df']
    pars = res['pars']
    shift_amounts = pars['shift_amounts']
    smoothing_sigma = pars['smooth_sigma']

    df['area'] = ratemap_area
    dfs.append(df)

df = pd.concat(dfs)
df['unit_id'] = ['{}_{}_{}'.format(r['sess_ind'], r['unit'], r['area']) for i, r
                 in df.iterrows()]


ds = {'Perirhinal' : None, 'Barrel' : None}
for ratemap_area in ratemap_areas:

    sig_ids = []
    n_total = []
    for null_distribution_method in nulldistmet_plots :
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'all') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_distribution_method) &
                 (df['shift_amount'] == 0) &
                 (df['area'] == ratemap_area)]

        if bonferroni_correct:
            p = p_val_thr / dfx.shape[0]
        else:
            p = p_val_thr

        unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
        sig_ids.append(unit_ids)
        n_total.append(dfx.shape[0])
    sig_both = set(sig_ids[0]).intersection(set(sig_ids[1]))
    ds[ratemap_area] = sig_both



# --- LOAD THE CONTRAST RATEMAPS -----------------------------------------------


for contrast in contrasts:
    # --- LOAD RESULTS ------------------------------------------------------------

    dfs = []
    for ratemap_area in ratemap_areas:
        res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps_contrasts', ratemap_settings)
        file_name = 'replayratemaps_{}_{}_{}.pkl'.format(ratemap_settings, ratemap_area, contrast)
        res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
        df = res['df']
        df['area'] = ratemap_area
        pars = res['pars']
        dfs.append(df)

    df = pd.concat(dfs)
    df = df[df['shift_amount'] == 0]
    df['unit_id'] = ['{}_{}_{}'.format(r['sess_ind'], r['unit'], r['area']) for
                     i, r in df.iterrows()]

    df = df[np.isin(df['unit_id'], list(ds['Perirhinal'])+list(ds['Barrel']))]

    df = df[df['null_method'] == null_dist_method_for_plot_both_shuff]
    assert df['contrast'].unique().shape[0] == 2
    name1, name2 = np.sort(df['contrast'].unique())


    # --- STATISTICS -----------------------------------------------------------
    df1 = df[df['contrast'] == name1]
    df2 = df[df['contrast'] == name2]

    for area in ratemap_areas:

        xx = df1[df1['area'] == area]['corrcoef_debiased']
        yy = df2[df2['area'] == area]['corrcoef_debiased']
        #
        # try:
        #     np.testing.assert_array_equal(df1['unit_id'], df2['unit_id'])
        #     stat, p = wilcoxon(xx, yy)
        #     print('{}, {} -  corrcoef debiased of sig. units:  p={:.1g},'
        #           ' Wilcoxon signed-rank test,'.format(area, contrast, p))
        # except AssertionError:
        stat, p = mannwhitneyu(xx, yy)
        print('{}, {} -  corrcoef debiased of sig. units:  p={:.1g},'
              ' Mann-Whitney U-test,'.format(area, contrast, p))


    # --- PLOT ONLY BARREL -----------------------------------------------------
    for ratemap_area in ratemap_areas:
        dfx = df[df['area'] == ratemap_area]

        f, ax = plt.subplots(1, 1, figsize=[1.8, 3])

        sns.barplot(data=dfx, x='contrast', y=variable, ax=ax,
                    order=contrast_labels[contrast].keys())
        labls = [contrast_labels[contrast][t._text] for t in ax.get_xticklabels()]

        plt.setp(ax.patches[0], edgecolor=sns.xkcd_rgb['dark grey'], facecolor='w',
                 linewidth=2)
        plt.setp(ax.patches[1], edgecolor=sns.xkcd_rgb['dark grey'],
                 facecolor=sns.xkcd_rgb['grey'], linewidth=2)

        plt.setp(ax.lines, color=sns.xkcd_rgb['dark grey'])

        ax.set_xticklabels(labls, rotation=45)

        if variable == 'percentile_rank':
            ax.set_yticks([0, 20, 40, 60])
            ax.set_ylim([0, 75])
        else:
            #ax.set_yticks([-0.05, 0, 0.05, 0.1])
            ax.set_ylim([-0.05, 0.48])
        ax.set_xlabel('')
        #if contrast == 'replay_direction':
        ax.set_ylabel('{} replay\ncoordination strength'.format(ratemap_area_sigla[ratemap_area]))
        sns.despine()
        plt.tight_layout()

        plot_name = 'contrasts_coordinated_units_{}_{}.{}'.format(ratemap_area, contrast, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



    # --- PLOT BOTH COMBINED ---------------------------------------------------
    f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
    sns.barplot(data=df, x='area', y='corrcoef_debiased', hue='contrast',
                order=['Perirhinal', 'Barrel'], dodge=True)

    plt.setp(ax.patches[0], edgecolor=sns.xkcd_rgb['dark grey'], facecolor='w', linewidth=2)
    plt.setp(ax.patches[2], edgecolor=sns.xkcd_rgb['dark grey'], facecolor=sns.xkcd_rgb['grey'], linewidth=2)
    plt.setp(ax.patches[1], edgecolor=sns.xkcd_rgb['dark grey'], facecolor='w', linewidth=2)
    plt.setp(ax.patches[3], edgecolor=sns.xkcd_rgb['dark grey'], facecolor=sns.xkcd_rgb['grey'], linewidth=2)
    plt.setp(ax.lines, color=sns.xkcd_rgb['dark grey'])

    ax.set_xticklabels(['PRH', 'S1BF'])

    ax.set_ylabel(variable_label['corrcoef_debiased'] + '\n of coordinated units')
    ax.set_xlabel('')
    ax.get_legend().remove()
    sns.despine()
    plt.tight_layout()

    plot_name = 'contrasts_coordinated_units_{}.{}'.format(contrast, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


    # MAKE LEGEND
    pos_patch = mpatches.Patch(edgecolor=sns.xkcd_rgb['dark grey'],
                               facecolor='white', linewidth=2,
                               label=contrast_labels[contrast][name1])
    neg_patch = mpatches.Patch(edgecolor=sns.xkcd_rgb['dark grey'],
                               facecolor=sns.xkcd_rgb['grey'], linewidth=2,
                               label=contrast_labels[contrast][name2])
    f, ax = plt.subplots(1, 1, figsize=[3, 1])
    ax.legend(handles=[pos_patch, neg_patch], frameon=False)
    ax.axis('off')
    plt.tight_layout()

    plot_name = 'contrasts_coordinated_units_{}_legend.{}'.format(contrast, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

    # COLOR BY AREA AND DOUBLE TICKS
    # colors = ['white', area_palette_2['PRH']]  # R -> G -> B
    # n_bins = 100  # Discretizes the interpolation into bins
    # cmap_name = 'pr_cmap'
    # cmap_pr = LinearSegmentedColormap.from_list(cmap_name, colors, N=n_bins)
    #
    # colors = ['white', area_palette_2['S1BF']]  # R -> G -> B
    # n_bins = 100  # Discretizes the interpolation into bins
    # cmap_name = 'br_cmap'
    # cmap_br = LinearSegmentedColormap.from_list(cmap_name, colors, N=n_bins)

    # plt.setp(ax.patches[0], edgecolor=cmap_pr(100), facecolor='w', linewidth=2)
    # plt.setp(ax.patches[2], edgecolor=cmap_pr(50), facecolor='w', linewidth=2)
    # plt.setp(ax.patches[1], edgecolor=cmap_br(100), facecolor='w', linewidth=2)
    # plt.setp(ax.patches[3], edgecolor=cmap_br(50), facecolor='w', linewidth=2)

    # ax.get_xticks()
    # am = 0.25
    # ax.set_xticks([0-am, 0, 0+am, 1-am, 1, 1+am])
    # ax.set_xticklabels([contrast_labels[contrast][name1],
    #                     '\nPHR',
    #                     contrast_labels[contrast][name2],
    #                     contrast_labels[contrast][name1],
    #                     '\nS1BF',
    #                     contrast_labels[contrast][name2]])


