import os
from constants import *
import pickle
import statsmodels.stats.descriptivestats
from statsmodels.stats.multitest import multipletests
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
from plotting_style import *
from sklearn.preprocessing import MinMaxScaler
from scipy.ndimage import gaussian_filter1d
import matplotlib.patches as mpatches
from utils import *
from statsmodels.stats.proportion import proportions_ztest
from scipy.stats import mannwhitneyu, wilcoxon


#ratemap_settings = 'may11_DecSet2_smooth2_morelages'
#ratemap_settings = 'jul4_DecSet2_smooth2_morelages'
#ratemap_settings = 'mar17_local_startend'
#ratemap_settings = 'dec6'
ratemap_settings = 'newspeedFIXED_8_oct19'
ratemap_settings = 'newspeedFIXED_THETA_48_5'
ratemap_settings = 'newspeedFIXED_THETA_MANUAL'
ratemap_settings = 'newspeedFIXED_THETA_MANUAL2'
ratemap_settings = 'newspeed_celldID_multicomp' #'newspeed_celldID_multicomp'
ratemap_settings = 'newspeed_celldID'
ratemap_settings = 'jan9_final_rip'
ratemap_settings = 'jan9_final'


ratemap_areas = ['Perirhinal']

save_plots = True
plot_format = 'svg'
dpi = 400

test_values = [50, 0] # [0, 0]
nulldistmet_plots = ['rotate_ratemap', 'rotate_spikes']
subselect_sessions = None
subselect_shifts = False
min_shift = -8
max_shift = 8

variable_for_ratemaps = 'corrcoef_obs'

# SWARMS
p_val_thr = 0.05
bonferroni_correct = False
null_dist_method_for_plot_both_shuff = 'rotate_spikes'


# RATEMAPS
smooth_ratemaps = True
p_val_thr_rateplots = 0.05
minmax_scale_plots = True
bins = np.arange(-29, 30)
n_rows = 4
n_cols = 4
n_plots = n_rows * n_cols
shift_amount = 0
shift_amount_ratemaps = 0



# --- SET UP PATHS ------------------------------------------------------------
plot_folder = os.path.join(PLOTS_FOLDER, plot_format, 'RERM', ratemap_settings,
                           null_dist_method_for_plot_both_shuff)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# --- LOAD RESULTS ------------------------------------------------------------

dfs = []
for ratemap_area in ratemap_areas:
    res_folder = os.path.join(REPLAY_FOLDER, 'results', 'replay_ratemaps', ratemap_settings)
    file_name = 'replayratemaps_{}_{}.pkl'.format(ratemap_settings, ratemap_area)
    res = pickle.load(open(os.path.join(res_folder, file_name), 'rb'))
    df = res['df']
    pars = res['pars']
    shift_amounts = pars['shift_amounts']
    smoothing_sigma = pars['smooth_sigma']


    if subselect_sessions is not None:
        df = df[np.isin(df['sess_ind'], subselect_sessions)]

    if subselect_shifts:
        df = df[df['shift_amount'] >= min_shift]
        df = df[df['shift_amount'] <= max_shift]
        shift_amounts = np.arange(min_shift, max_shift+1)

    df['area'] = ratemap_area
    dfs.append(df)

df = pd.concat(dfs)
df['unit_id'] = ['{}_{}_{}'.format(r['sess_ind'], r['unit'], r['area']) for i, r in df.iterrows()]


if ratemap_settings == 'jan9_final_rip':
    rip = np.array(df['has_ripple']).astype(str)
    df = df.drop(columns=['has_ripple'])
    df['has_ripple'] = rip


    print('# OF PER CELLS:')
    for hasrip in ['True', 'False'] :
        print('\n hasrip: {}'.format(hasrip))
        print(df[df['has_ripple'] == hasrip]['unit_id'].unique().__len__())
        print(df[df['has_ripple'] == hasrip]['sess_ind'].unique().__len__())

    # SELECT ONLY WITH RIPPLES
    df = df[df['has_ripple'] == 'True']
    df['has_ripple'] = 'all'



print('# OF PER CELLS:')
for direction in ['all', 'positive', 'negative']:
    print('\n direction: {}'.format(direction))
    print(df[df['direction'] == direction]['unit_id'].unique().__len__())

print(df['unit_id'].unique().__len__())

#shift_amounts = np.array([-8, -6, -4, -2, 0, 2, 4, 6, 8])


# len(df[df['area'] == 'Perirhinal']['sess_ind'].unique())
# len(df[df['area'] == 'Barrel']['sess_ind'].unique())

# --- STATISTICS ---------------------------------------------------------------

# is the proportion of coordinated cells greater than chance?
ds = {'Perirhinal' : {}, 'Barrel' : {}}
for ratemap_area in ratemap_areas:

    sig_ids = []
    n_total = []
    for null_distribution_method in nulldistmet_plots :

        dfx = df[(df['phase'] == 'all') &
                     (df['direction'] == 'all') &
                     (df['has_ripple'] == 'all') &
                     (df['null_method'] == null_distribution_method) &
                     (df['shift_amount'] == 0) &
                     (df['area'] == ratemap_area)]

        if bonferroni_correct:
            p = p_val_thr / dfx.shape[0]
        else:
            p = p_val_thr

        unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
        sig_ids.append(unit_ids)
        n_total.append(dfx.shape[0])
    sig_both = set(sig_ids[0]).intersection(set(sig_ids[1]))
    n_tot = n_total[0]
    n_sig = len(sig_both)
    n_not_sig = n_tot - n_sig
    binom_p = scipy.stats.binom_test(n_sig, n=n_tot, p=0.05)
    print('{}, both methods: {} out of {} significant units, ({}%) - p = {:.1g}'.format(
        ratemap_area, n_sig, n_tot, round(100 * n_sig / n_tot, 1), binom_p))
    ds[ratemap_area] = {'n_sig' : n_sig, 'n_tot' : n_tot}



# Is the proportion of coordinated cells different in the two areas?
if len(ratemap_areas) == 2:
    z, pval = proportions_ztest(count=[ds['Barrel']['n_sig'], ds['Perirhinal']['n_sig']],
                                nobs=[ds['Barrel']['n_tot'], ds['Perirhinal']['n_tot']])

    print('\n\nProportion of coordinated cells is different in PRH vs S1BF'
          '\nz_test of proportions : p={:.1f}'.format(pval))


dfx = df[(df['phase'] == 'all') &
         (df['direction'] == 'all') &
         (df['has_ripple'] == 'all') &
         (df['null_method'] == null_dist_method_for_plot_both_shuff) &
         (df['shift_amount'] == 0)]

xx = dfx[dfx['area'] == 'Perirhinal']['corrcoef_debiased']

print('Corrcoef debiased PHR: {:.2f}, {:.2f} - {:.2f}'.format(xx.quantile(q=0.5),
                                                            xx.quantile(q=0.25),

                                                            xx.quantile(q=0.75)))

stat, p_PHR = wilcoxon(xx-0)
print('Corrcoef debiased for PRH different from 0, Wilcoxon signed-rank test, p={:.1g}'.format(p_PHR))


if len(ratemap_areas) == 2:
    yy = dfx[dfx['area'] == 'Barrel']['corrcoef_debiased']

    print('Corrcoef debiased S1BF: {:.2f}, {:.2f} - {:.2f}'.format(yy.quantile(q=0.5),
                                                                 yy.quantile(q=0.25),
                                                                 yy.quantile(q=0.75)))

    stat, p_S1BF = wilcoxon(yy-0)
    print('Corrcoef debiased for S1BF different from 0, Wilcoxon signed-rank test, p={:.1g}'.format(p_S1BF))


    stat, p = mannwhitneyu(xx, yy)
    print('Corrcoef debiased PRH vs S1BF, Mann-Whitney U-test, p={:.1g}'.format(p))


# --- MAKE RATEMAP LEGEND ------------------------------------------------------


for ratemap_area in ratemap_areas:
    edgecolor1 = real_event_palette[ratemap_area][0]
    facecolor1 = make_rgb_transparent(real_event_palette[ratemap_area][0], (1, 1, 1), 0.3)

    edgecolor2 = real_event_palette[ratemap_area][1]
    facecolor2 = make_rgb_transparent(real_event_palette[ratemap_area][1], (1, 1, 1), 0.3)


    pos_patch = mpatches.Patch(edgecolor=edgecolor1,
                               facecolor=facecolor1, linewidth=2.5,
                               label='Real rate map')
    neg_patch = mpatches.Patch(edgecolor=edgecolor2,
                               facecolor=facecolor2, linewidth=2.5,
                               label='Replay rate map')
    f, ax = plt.subplots(1, 1, figsize=[3, 1])
    leg = ax.legend(handles=[pos_patch, neg_patch], frameon=False,
                    title=ratemap_area_sigla[ratemap_area])
    leg.set_title(ratemap_area_sigla[ratemap_area], prop={'size' : 12})

    leg._legend_box.align = "left"
    ax.axis('off')
    plt.tight_layout()

    plot_name = 'legend_ratemaps_{}.{}'.format(ratemap_area, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# --- PLOT RATEMAPS COMBINED SHUFFLES ------------------------------------------

for ratemap_area in ratemap_areas:

    sig_ids = []
    n_total = []
    for null_distribution_method in nulldistmet_plots :
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'all') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_distribution_method) &
                 (df['shift_amount'] == 0) &
                 (df['area'] == ratemap_area)]

        if bonferroni_correct:
            p = p_val_thr / dfx.shape[0]
        else:
            p = p_val_thr

        #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
        unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
        sig_ids.append(unit_ids)
        n_total.append(dfx.shape[0])
    sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

    dfx = df[(df['phase'] == 'all') &
             (df['direction'] == 'all') &
             (df['has_ripple'] == 'all') &
             (df['null_method'] == null_dist_method_for_plot_both_shuff) &
             (df['shift_amount'] == shift_amount_ratemaps) &
             (np.isin(df['unit_id'], sig_both))]


    dfx = dfx.nlargest(n=n_plots, columns=variable_for_ratemaps)
    dfx = dfx.reset_index()
    n_neurons = dfx.shape[0]

    palette = real_event_palette[ratemap_area]



    f, axes = plt.subplots(n_rows, n_cols, figsize=[n_cols*1.6, n_rows * 1.1],
                           sharex=False)

    if n_rows == 1:
        axes = axes[np.newaxis, :]

    for kk, row in dfx.iterrows():

        real_ratemap = row['ratemap_real']
        event_ratemap = row['ratemap_event']

        try:
            ax = axes.flatten()[kk]
        except TypeError:
            ax = axes

        ax.set_yticks([])

        for i, rm in enumerate([real_ratemap, event_ratemap]):
            curve = rm

            if smooth_ratemaps:
                curve  = gaussian_filter1d(curve, smoothing_sigma)

            if minmax_scale_plots:
                mm = MinMaxScaler()
                curve = mm.fit_transform(curve.reshape(-1, 1))[:, 0]
            line = ax.plot(bins, curve, color=palette[i], lw=2)
            fillcolor = line[0].get_color()
            ax.fill_between(bins, curve,
                            color=fillcolor, alpha=0.3,
                            zorder=int(10 + 2 * n_neurons - 2 * kk - 1))
            ax.set_title('r = {:.03}'.format(row[variable_for_ratemaps]),
                         fontsize=10)

    axes.flatten()[0].set_xticks([-29, -reward_bin, 0, reward_bin, 29])
    axes.flatten()[0].set_xticklabels(['', 'REW', 'IMG', 'REW', ''], fontsize=8)
    #axes.flatten()[0].set_xticks([-29, -15, 0, 15, 29])
    #axes.flatten()[0].set_xticklabels([-29, '', 'Location', '', 29])
    #axes.flatten()[0].set_xlabel('Location')
    sns.despine(left=True,offset=2, ax=axes.flatten()[0], trim=True)

    for kk in range(1, n_plots):
        print(kk)
        axes.flatten()[kk].axis('off')

    plt.tight_layout()


    #plt.rcParams['savefig.dpi'] = dpi

    plot_name = 'sig_replay_ratemaps_{}_{}.{}'.format(ratemap_area, ratemap_settings, plot_format)
    plt.draw()
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- SWARMPLOTS COMBINE SHUFFLES ----------------------------------------------

sig_ids = []
n_total = []
for null_distribution_method in nulldistmet_plots :
    dfx = df[(df['phase'] == 'all') &
             (df['direction'] == 'all') &
             (df['has_ripple'] == 'all') &
             (df['null_method'] == null_distribution_method) &
             (df['shift_amount'] == 0)]

    if bonferroni_correct:
        p = p_val_thr / dfx.shape[0]
    else:
        p = p_val_thr

    #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
    unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
    sig_ids.append(unit_ids)
    n_total.append(dfx.shape[0])

sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

dfx = df[(df['phase'] == 'all') &
         (df['direction'] == 'all') &
         (df['has_ripple'] == 'all') &
         (df['null_method'] == null_dist_method_for_plot_both_shuff) &
         (df['shift_amount'] == 0)]

dfx['is_sig'] = ['Coordinated' if f else 'Not coordinated' for f in np.isin(dfx['unit_id'], sig_both)]


dfx = dfx[dfx['area'] == 'Perirhinal']


# ----------
f, ax = plt.subplots(1, 1, figsize=[1, 3])

sns.violinplot(x='area', y='corrcoef_debiased', data=dfx,
               color='white', inner='quartile', lw=8, edgecolor='red')

sns.swarmplot(x='area', y='corrcoef_debiased', data=dfx, hue='is_sig', size=4,
              palette=[sns.xkcd_rgb['dark grey'], sns.xkcd_rgb['silver']],
              hue_order=['Coordinated', 'Not coordinated'],
              alpha=0.9)
ax.set_ylabel('Replay coordination\nstrength ($r_d$)')


ax.collections[0].set_edgecolor(area_palette['Perirhinal'])
ax.collections[0].set_linewidth(3.5)
ax.collections[0].set_alpha(0.7)

#ax.collections[1].set_edgecolor(area_palette['Barrel'])

for i in range(0, 3) :
    ax.lines[i].set_color(area_palette['Perirhinal'])
    # ax.lines[i].set_alpha(0.7)
    ax.lines[i].set_linewidth(3.5)
    ax.lines[i].set_alpha(0.7)

# for i in range(3, 6) :
#     ax.lines[i].set_color(area_palette['Barrel'])
#     # ax.lines[i].set_alpha(0.7)
#     ax.lines[i].set_linewidth(2.5)
ax.lines[1].set_linestyle('-')

#ax.set_title(null_distribution_method)
sns.despine(bottom=True)
ax.set_ylabel('PER replay coordination\nstrength ($r_d$)')
ax.set_xlabel('')
ax.set_xticks([])

if null_dist_method_for_plot_both_shuff == 'rotate_ratemap':
    ax.set_ylim([-1, 1.15])

else:
    ax.set_ylim([-1, 1])
ax.set_yticks([-1, -0.5, 0, 0.5, 1])
ax.get_legend().remove()
plt.tight_layout()


if save_plots :
    plot_name = 'sig_swarm_comb_shuffle.{}'.format(plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- DONUT COMBINE SHUFFLES ---------------------------------------------------

f, axes = plt.subplots(1, 2, figsize=[6, 3])

for ax, ratemap_area in zip(axes, ratemap_areas):

    sig_ids = []
    n_total = []
    for null_distribution_method in nulldistmet_plots :
        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'all') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_distribution_method) &
                 (df['shift_amount'] == 0) &
                 (df['area'] == ratemap_area)]

        if bonferroni_correct:
            p = p_val_thr / dfx.shape[0]
        else:
            p = p_val_thr

        #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
        unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
        sig_ids.append(unit_ids)
        n_total.append(dfx.shape[0])

    sig_both = set(sig_ids[0]).intersection(set(sig_ids[1]))

    n_tot = n_total[0]
    n_sig = len(sig_both)
    n_not_sig = n_tot - n_sig

    binom_p = scipy.stats.binom_test(n_sig, n=n_tot, p=0.05)
    print('{}, both methods: {} out of {} significant units, ({}%) - p = {:.0e}'.format(
        ratemap_area, n_sig, n_tot, round(100 * n_sig / n_tot, 1), binom_p))

    size_of_groups = [n_sig, n_not_sig]
    labels = ['{}%'.format(round(100 * n_sig / n_tot, 1)),
              '{}%'.format(round(100 * n_not_sig / n_tot, 1))]

    circle_color = make_rgb_transparent(area_palette[ratemap_area], (1, 1, 1), 0.7)
    sig_info_colors_list = [sns.xkcd_rgb['dark grey'], circle_color]

    ax.pie(size_of_groups, colors=sig_info_colors_list, labels=labels,
           wedgeprops=dict(width=0.5), textprops={'fontsize' : 14})
    ax.axis('equal')
    #ax.set_title('{}: {} out of {}\ncoordinated units'.format(ratemap_area_sigla[ratemap_area], n_sig, n_tot))
    ax.set_title('{}'.format(ratemap_area_sigla[ratemap_area]), fontsize=16)
    #ax.set_title('{}: {} out of {} units\nbinomial p={:.3f}'.format(ratemap_area, n_sig, n_tot, binom_p))
    plt.tight_layout()

if save_plots :
    plot_name = 'sig_donut_comb_shuffle.{}'.format(plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# --- Corr debiased in positive versus negative --------------------------------
# Take the units which are significant with all events, and look at
# their correlation coefficient in positive versus negative.
if False:
    dfs = []

    for ratemap_area in ratemap_areas:

        sig_ids = []
        for null_distribution_method in nulldistmet_plots :
            dfx = df[(df['phase'] == 'all') &
                     (df['direction'] == 'all') &
                     (df['has_ripple'] == 'all') &
                     (df['null_method'] == null_distribution_method) &
                     (df['shift_amount'] == 0) &
                     (df['area'] == ratemap_area)]

            if bonferroni_correct:
                p = p_val_thr / dfx.shape[0]
            else:
                p = p_val_thr
            #dfx['is_sig'] = ['Coordinated' if pv <= p else 'Not coordinated' for pv in dfx['p_values']]
            unit_ids = dfx[dfx['p_values'] <= p]['unit_id'].values
            sig_ids.append(unit_ids)

        sig_both = list(set(sig_ids[0]).intersection(set(sig_ids[1])))

        dfx = df[(df['phase'] == 'all') &
                 (df['direction'] == 'negative') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_dist_method_for_plot_both_shuff) &
                 (df['shift_amount'] == 0) &
                 (df['area'] == ratemap_area) &
                 (np.isin(df['unit_id'], sig_both))]

        dfx2 = df[(df['phase'] == 'all') &
                 (df['direction'] == 'positive') &
                 (df['has_ripple'] == 'all') &
                 (df['null_method'] == null_dist_method_for_plot_both_shuff) &
                 (df['shift_amount'] == 0) &
                 (df['area'] == ratemap_area) &
                 (np.isin(df['unit_id'], sig_both))]

        df3 = pd.concat([dfx, dfx2])
        dfs.append(df3)

        nhig = (dfx2['corrcoef_debiased'] > dfx['corrcoef_debiased']).sum()
        ntot = dfx.shape[0]
        binom_p = scipy.stats.binom_test(nhig, n=ntot, p=0.5)
        print('{} - {} units out of {} ({}%), p={:.2f} for which the debiased correlation is higher in pos'
              ' than neg events'.format(ratemap_area, nhig, ntot, np.round(100*nhig/ntot, 2), binom_p))
        t, p = scipy.stats.wilcoxon(dfx2['corrcoef_debiased'], dfx['corrcoef_debiased'])
        print('wilcoxon p={:.2f}'.format(p))



dg = pd.concat(dfs)

f, ax = plt.subplots(1, 1, figsize=smaller_panel_size)
sns.boxplot(data=dg, x='area', y='corrcoef_debiased', hue='direction',
              order=['Perirhinal', 'Barrel'], dodge=True,
              color='white')
plt.setp(ax.artists, edgecolor=sns.xkcd_rgb['grey'], facecolor='w')
plt.setp(ax.lines, color=sns.xkcd_rgb['grey'])

#
# ax.collections[0].set_edgecolor(violin_color)
# ax.collections[1].set_edgecolor(violin_color)
# for i in range(0, 6) :
#     ax.lines[i].set_color(violin_color)
#     # ax.lines[i].set_alpha(0.7)
#     ax.lines[i].set_linewidth(2.5)
# ax.lines[1].set_linestyle('-')
# ax.lines[4].set_linestyle('-')

sns.swarmplot(data=dg, x='area', y='corrcoef_debiased', hue='direction',
              order=['Perirhinal', 'Barrel'], dodge=True,
              palette=replay_direction_palette)
ax.set_ylabel(variable_label['corrcoef_debiased']+'\n of significant units')
ax.set_xlabel('')
ax.get_legend().remove()
sns.despine()
plt.tight_layout()


if save_plots :
    plot_name = 'corrcoef_posneg_of_sig_units_{}.{}'.format(null_dist_method_for_plot_both_shuff, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


